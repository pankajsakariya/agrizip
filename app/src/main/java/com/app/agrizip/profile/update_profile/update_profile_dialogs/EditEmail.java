package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/26/2016 at 5:39 PM.
 */

public class EditEmail extends CustomDialog
{

    @BindView(R.id.et_dialogEditEmail)
    EditText etDialogEditEmail;

    public EditEmail(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit, SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_edit_email);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                saveNewEmailAddress();
            }
        });

        etDialogEditEmail.setText(singleDetailView.tvSingleDetailValue.getText());

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

    }
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void saveNewEmailAddress()
    {
        boolean valideEmail = isValidEmail(etDialogEditEmail.getText().toString());
        if(valideEmail){
            userProfileDetailGetForEdit.getData()
                    .setEmailAddress(etDialogEditEmail.getText().toString());

            singleDetailView
                    .tvSingleDetailValue.setText(etDialogEditEmail.getText().toString());
            dismiss();
        }else{
            Toast.makeText(getContext(),"Please Enter Valid Email",Toast.LENGTH_LONG).show();
        }





        /*RetrofitCallbacks<ResponseBody> onUpdateEmail = new RetrofitCallbacks<ResponseBody>(getContext())
        {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    EditEmail.this.dismiss();
                }
            }
        };

        API.getInstance().updateUserEmail(etDialogEditEmail.getText().toString(),
                App.getInstance().getUser().getId(), onUpdateEmail);*/

    }

    @Override
    public void onChangePrivacy(String id)
    {
        userProfileDetailGetForEdit.getData().setGenderVisibleTo(id);
    }
}

