package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import dialogs.PrivacyChangeListener;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/27/2016 at 4:51 PM.
 */

public class EditCurrentCity extends CustomDialog implements PrivacyChangeListener
{
    @BindView(R.id.et_dialogEditCurrentCity)
    EditText etDialogEditCurrentCity;

    public EditCurrentCity(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit, SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_edit_current_city);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        etDialogEditCurrentCity.setText(singleDetailView.tvSingleDetailValue.getText());

        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (etDialogEditCurrentCity.getText().toString().trim().length() > 0)
                {
                    userProfileDetailGetForEdit.getData().setCity(etDialogEditCurrentCity.getText().toString());

                    singleDetailView.tvSingleDetailValue.setText(etDialogEditCurrentCity.getText().toString());
                } else
                {
                    Toast.makeText(getContext(), "Enter City", Toast.LENGTH_LONG).show();
                }
                dismiss();
            }
        });
        try{

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onChangePrivacy(String id)
    {
        userProfileDetailGetForEdit.getData().setCityVisibleTo(id);
    }
}
