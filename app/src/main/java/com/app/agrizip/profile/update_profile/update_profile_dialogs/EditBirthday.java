package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import dialogs.PrivacyChangeListener;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/27/2016 at 3:56 PM.
 */

public class EditBirthday extends CustomDialog implements DatePickerDialog.OnDateSetListener,
        View.OnClickListener, PrivacyChangeListener
{

    final Activity activity;
    @BindView(R.id.tv_dialogBirthDay)
    TextView tvDialogBirthDay;
    @BindView(R.id.tv_dialogBirthMonth)
    TextView tvDialogBirthMonth;
    @BindView(R.id.tv_dialogBirthYear)
    TextView tvDialogBirthYear;
    String birthDate = "";
    String temp_brithdate="";
    public EditBirthday(Activity activity, Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit,
                        SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_birthday);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
         temp_brithdate = singleDetailView.tvSingleDetailValue.getText().toString();
       /* DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date startDate;
        String newDateString = null;
        try {
            startDate = df.parse(temp_brithdate);
           newDateString  = df.format(startDate);
            singleDetailView.tvSingleDetailValue.setText(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/


         if(temp_brithdate!=null){
             if(temp_brithdate.length() < 11 )
             {
        String[] datePart = temp_brithdate.split("/");
        tvDialogBirthDay.setText(datePart[0]);
        tvDialogBirthMonth.setText(datePart[1]);
        tvDialogBirthYear.setText(datePart[2]);
    }}
        tvDialogBirthYear.setOnClickListener(this);
        tvDialogBirthDay.setOnClickListener(this);
        tvDialogBirthMonth.setOnClickListener(this);

        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });


        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
      //    if( birthdatevalidation()){
                if(birthDate.equals(""))
                {
                    birthDate=temp_brithdate;
                    userProfileDetailGetForEdit.getData().setDob(birthDate);

                    singleDetailView
                            .tvSingleDetailValue.setText(birthDate);

                    dismiss();
                }
                else {
                    userProfileDetailGetForEdit.getData().setDob(birthDate);

                    singleDetailView
                            .tvSingleDetailValue.setText(birthDate);

                    dismiss();
                }

       //    }else{
               //Toast.makeText(activity,"Input Valid BirthDate",Toast.LENGTH_LONG).show();
        ////   }

            }
        });

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
    {
        tvDialogBirthDay.setText(dayOfMonth + "");
        tvDialogBirthMonth.setText((monthOfYear + 1) + "");
        tvDialogBirthYear.setText(year + "");
        birthDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
    }

    @Override
    public void onClick(View view)
    {
        super.onClick(view);
        if (view.getId() == R.id.tv_dialogBirthDay || view.getId() == R.id.tv_dialogBirthMonth
                || view.getId() == R.id.tv_dialogBirthYear)
        {
            DatePickerDialog dpd;
            Calendar now = Calendar.getInstance();
            try {
                dpd = DatePickerDialog.newInstance(
                        this,Integer.valueOf(tvDialogBirthYear.getText().toString()),Integer.valueOf(tvDialogBirthMonth.getText().toString())-1,Integer.valueOf(tvDialogBirthDay.getText().toString())


                );

            }catch (Exception ex)
            {
                dpd=DatePickerDialog.newInstance(this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
            }


            dpd.setMaxDate(now);
            dpd.show(activity.getFragmentManager(), "Datepickerdialog");
        }

    }

    @Override
    public void onChangePrivacy(String id)
    {
        userProfileDetailGetForEdit.getData().setDOBVisibleTo(id);
    }
}
