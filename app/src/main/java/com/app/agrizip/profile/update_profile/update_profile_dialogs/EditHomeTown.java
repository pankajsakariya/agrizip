package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/27/2016 at 4:52 PM.
 */

public class EditHomeTown extends CustomDialog
{

    @BindView(R.id.et_dialogEditHomeTown)
    EditText etDialogEditHomeTown;

    public EditHomeTown(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit, SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_edit_home_town);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);

        etDialogEditHomeTown.setText(singleDetailView.tvSingleDetailValue.getText());


        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                if(etDialogEditHomeTown.getText().toString().trim().length() >0){
                    userProfileDetailGetForEdit.getData()
                            .setHometown(etDialogEditHomeTown.getText().toString());

                    singleDetailView.tvSingleDetailValue.setText(etDialogEditHomeTown.getText().toString());
                    dismiss();
                }else{
                    Toast.makeText(getContext(),"Enter HomeTown",Toast.LENGTH_LONG).show();
                }

            }
        });

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

    }


    @Override
    public void onChangePrivacy(String id)
    {
        userProfileDetailGetForEdit.getData().setHometownVisibleTo(id);
    }
}
