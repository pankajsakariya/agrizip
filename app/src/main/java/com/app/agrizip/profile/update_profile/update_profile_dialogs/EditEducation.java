package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import java.util.Calendar;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import dialogs.PrivacyChangeListener;
import extras.Utils;
import viewmodels.SingleDetailModel;
import views.SingleDetailView;
import views.SingleProfileDetailView;

/**
 * Created by rutvik on 12/27/2016 at 6:29 PM.
 */

public class EditEducation extends CustomDialog implements PrivacyChangeListener
{


    @BindView(R.id.et_dialogEducationCollege)
    EditText etDialogEducationCollege;
    @BindView(R.id.et_dialogEducationCity)
    EditText etDialogEducationCity;
    @BindView(R.id.et_dialogEducationDesc)
    EditText etDialogEducationDesc;


    @BindView(R.id.tv_dialogEducationFromDay)
    TextView tvDialogEducationFromDay;
    @BindView(R.id.tv_dialogEducationFromMonth)
    TextView tvDialogEducationFromMonth;
    @BindView(R.id.tv_dialogEducationFromYear)
    TextView tvDialogEducationFromYear;

    @BindView(R.id.tv_dialogEducationToDay)
    TextView tvDialogEducationToDay;
    @BindView(R.id.tv_dialogEducationToMonth)
    TextView tvDialogEducationToMonth;
    @BindView(R.id.tv_dialogEducationToYear)
    TextView tvDialogEducationToYear;

    @BindView(R.id.cb_dialogEducationGraduate)
    CheckBox cbDialogEducationGraduate;
    SingleProfileDetailView singleProfileDetailView;
    private String fromDate = "";
    private String toDate = "";
    private String EducationFromDate="";
    private String EductionToDate="";
    private UserProfileDetailGetForEdit.DataBean.EducationBean
            newEducationBean = new UserProfileDetailGetForEdit.DataBean.EducationBean();
    private UserProfileDetailGetForEdit.DataBean.EducationBean
            existingEducationBean = null;

    public EditEducation(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit,
                         SingleProfileDetailView singleProfileDetailView,
                         SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);

        this.singleProfileDetailView = singleProfileDetailView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_education);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);

        tvDialogEducationFromDay.setOnClickListener(this);
        tvDialogEducationFromMonth.setOnClickListener(this);
        tvDialogEducationFromYear.setOnClickListener(this);

        tvDialogEducationToDay.setOnClickListener(this);
        tvDialogEducationToMonth.setOnClickListener(this);
        tvDialogEducationToYear.setOnClickListener(this);

        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                newEducationBean.setEducationId(id);
                newEducationBean.setInstituteName(etDialogEducationCollege.getText().toString());
                newEducationBean.setCity(etDialogEducationCity.getText().toString());
                newEducationBean.setDescription(etDialogEducationDesc.getText().toString());
                if(fromDate.equals("") && toDate.equals(""))
                {

                    fromDate=EducationFromDate;
                    toDate=EductionToDate;
                    newEducationBean.setTimePeriodFrom(fromDate);
                    newEducationBean.setTimePeriodTo(toDate);

                 }else if(fromDate.equals(""))
                 {
                    fromDate=EducationFromDate;
                    newEducationBean.setTimePeriodFrom(fromDate);
                     newEducationBean.setTimePeriodTo(toDate);
                }
                else if(toDate.equals(""))
                {
                    toDate=EductionToDate;
                    newEducationBean.setTimePeriodTo(toDate);
                    newEducationBean.setTimePeriodFrom(fromDate);
                }
                 else {
                    newEducationBean.setTimePeriodFrom(fromDate);
                    newEducationBean.setTimePeriodTo(toDate);
                }

                newEducationBean.setGraduated(cbDialogEducationGraduate.isChecked());
                if (singleProfileDetailView != null)
                {
                userProfileDetailGetForEdit.getData().getEducation().add(newEducationBean);

                SingleDetailModel singleDetailModel =
                        new SingleDetailModel(newEducationBean.getEducationId(),
                                etDialogEducationCollege.getText().toString(),
                                R.drawable.ic_camera_alt_black_18dp);

                singleDetailModel.setReadOnly(false);
                singleDetailModel.setEditSingleDetailListener(singleProfileDetailView.getSingleProfileDetailModel());


                    singleProfileDetailView.addNewSingleDetail(singleDetailModel);
                }else {

                        Log.i("Eduction ID**********", id);
                        for (UserProfileDetailGetForEdit.DataBean.EducationBean Education :
                                userProfileDetailGetForEdit.getData().getEducation())
                        {
                            Log.i("ITERATING EductionID***", Education.getEducationId());

                            if (newEducationBean.getEducationId().equals(Education.getEducationId()))
                            {
                                Education.setEducationId(newEducationBean.getEducationId());
                                Education.setInstituteName(newEducationBean.getInstituteName());
                                Education.setCity(newEducationBean.getCity());
                                Education.setDescription(newEducationBean.getDescription());
                                Education.setGraduated(newEducationBean.isGraduated());
                                Education.setTimePeriodFrom(newEducationBean.getTimePeriodFrom());
                                Education.setTimePeriodTo(newEducationBean.getTimePeriodTo());
                            }
                        }
                        singleDetailView.tvSingleDetailValue.setText(newEducationBean.getInstituteName());

                    }

                dismiss();
            }
        });

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());


        cbDialogEducationGraduate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
            {
                if (existingEducationBean != null)
                {
                    existingEducationBean.setGraduated(isChecked);
                } else
                {
                    newEducationBean.setGraduated(isChecked);
                }
            }
        });

        if (singleDetailView != null)
        {
            fillDataFromExistingModel();
        }

    }


    @Override
    public void onChangePrivacy(String id)
    {
        if (existingEducationBean != null)
        {
            existingEducationBean.setEducationVisibleTo(id);
        } else
        {
            newEducationBean.setEducationVisibleTo(id);
        }
    }


    private void fillDataFromExistingModel()
    {

        for (UserProfileDetailGetForEdit.DataBean.EducationBean education :
                userProfileDetailGetForEdit.getData().getEducation())
        {
            if (education.getInstituteName().equals(singleDetailView.getSingleDetailModel()
                    .getTextValue()))
            {
                existingEducationBean = education;
            }
        }

        if (existingEducationBean != null)
        {
            etDialogEducationCollege.setText(existingEducationBean.getInstituteName());
            etDialogEducationCity.setText(existingEducationBean.getCity());
            etDialogEducationDesc.setText(existingEducationBean.getDescription());
            cbDialogEducationGraduate.setChecked(existingEducationBean.isGraduated());
            EducationFromDate=existingEducationBean.getTimePeriodFrom();

            EductionToDate=existingEducationBean.getTimePeriodTo();

            String FromEduDate;
            String ToEduDate;
            if(EducationFromDate.contains("T"))
            {
                FromEduDate= Utils.changeDateFormat(EducationFromDate,"yyyy-MM-dd'T'HH:mm:ss","MM/dd/yyyy");
            }else {
                FromEduDate=EducationFromDate;
            }
            if(FromEduDate.length() < 11){
                String[] datePart = FromEduDate.split("/");
                tvDialogEducationFromDay.setText(datePart[0]);
                tvDialogEducationFromMonth.setText(datePart[1]);
                tvDialogEducationFromYear.setText(datePart[2]);
            }
            if(EducationFromDate.contains("T"))
            {
                ToEduDate= Utils.changeDateFormat(EductionToDate,"yyyy-MM-dd'T'HH:mm:ss","MM/dd/yyyy");
            }else {
                ToEduDate=EductionToDate;
            }
            if(ToEduDate.length()<11)
            {
                String[] datePart = ToEduDate.split("/");
                tvDialogEducationToDay.setText(datePart[0]);
                tvDialogEducationToMonth.setText(datePart[1]);
                tvDialogEducationToYear.setText(datePart[2]);
            }

        }

    }


    @Override
    public void onClick(View view)
    {
        super.onClick(view);
        if (view.getId() == R.id.tv_dialogEducationFromDay || view.getId() == R.id.tv_dialogEducationFromMonth
                || view.getId() == R.id.tv_dialogEducationFromYear)
        {
            Calendar now = Calendar.getInstance();
            android.app.DatePickerDialog dpd;
            try {
                dpd = new android.app.DatePickerDialog(getContext(), new android.app.DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth)
                    {
                                tvDialogEducationFromDay.setText(dayOfMonth + "");
                                tvDialogEducationFromMonth.setText((monthOfYear + 1) + "");
                                tvDialogEducationFromYear.setText(year + "");
                                fromDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            }
                        },  Integer.valueOf(tvDialogEducationFromYear.getText().toString()),Integer.valueOf(tvDialogEducationFromMonth.getText().toString())-1,Integer.valueOf(tvDialogEducationFromDay.getText().toString())

            );
            } catch (Exception ex) {
                dpd = new android.app.DatePickerDialog(getContext(), new android.app.DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth)
                    {
                                tvDialogEducationFromDay.setText(dayOfMonth + "");
                                tvDialogEducationFromMonth.setText((monthOfYear + 1) + "");
                                tvDialogEducationFromYear.setText(year + "");
                                fromDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)

                );
            }

                dpd.show();

        } else if (view.getId() == R.id.tv_dialogEducationToDay || view.getId() == R.id.tv_dialogEducationToMonth
                || view.getId() == R.id.tv_dialogEducationToYear)
        {
            Calendar now = Calendar.getInstance();
            android.app.DatePickerDialog dpd;
                    try{
                        dpd = new android.app.DatePickerDialog(getContext(), new android.app.DatePickerDialog.OnDateSetListener()
                        {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth)
                            {
                                        tvDialogEducationToDay.setText(dayOfMonth + "");
                                        tvDialogEducationToMonth.setText((monthOfYear + 1) + "");
                                        tvDialogEducationToYear.setText(year + "");
                                        toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                    }
                                },Integer.valueOf(tvDialogEducationToYear.getText().toString()),Integer.valueOf(tvDialogEducationToMonth.getText().toString())-1,Integer.valueOf(tvDialogEducationToDay.getText().toString())
                        );
                    }catch (Exception ex) {
                        dpd = new android.app.DatePickerDialog(getContext(), new android.app.DatePickerDialog.OnDateSetListener()
                        {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth)
                            {

                                tvDialogEducationToDay.setText(dayOfMonth + "");
                                    tvDialogEducationToMonth.setText((monthOfYear + 1) + "");
                                    tvDialogEducationToYear.setText(year + "");
                                    toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                }
                            },
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    }

                        dpd.show();

        }

    }

}
