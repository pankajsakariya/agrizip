package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import java.util.Calendar;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import extras.Utils;
import viewmodels.SingleDetailModel;
import views.SingleDetailView;
import views.SingleProfileDetailView;

/**
 * Created by rutvik on 12/27/2016 at 6:29 PM.
 */

public class EditWork extends CustomDialog
{

    @BindView(R.id.et_dialogWorkWhere)
    EditText etDialogWorkWhere;
    @BindView(R.id.et_dialogWorkCity)
    EditText etDialogWorkCity;
    @BindView(R.id.et_dialogWorkDesc)
    EditText etDialogWorkDesc;
    @BindView(R.id.et_dialogWorkPosition)
    EditText etDialogWorkPosition;
    @BindView(R.id.tv_dialogWorkFromDay)
    TextView tvDialogWorkFromDay;
    @BindView(R.id.tv_dialogWorkFromMonth)
    TextView tvDialogWorkFromMonth;
    @BindView(R.id.tv_dialogWorkFromYear)
    TextView tvDialogWorkFromYear;

    @BindView(R.id.tv_dialogWorkToDay)
    TextView tvDialogWorkToDay;
    @BindView(R.id.tv_dialogWorkToMonth)
    TextView tvDialogWorkToMonth;
    @BindView(R.id.tv_dialogWorkToYear)
    TextView tvDialogWorkToYear;
    SingleProfileDetailView singleProfileDetailView;
    private String fromDate = "";
    private String toDate = "";
    private String WorkFromDate = "";
    private String WorkToDate = "";
    private UserProfileDetailGetForEdit.DataBean.WorkBean
            newWorkBean = new UserProfileDetailGetForEdit.DataBean.WorkBean();
    private UserProfileDetailGetForEdit.DataBean.WorkBean
            existingWorkBean = null;

    public EditWork(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit,
                    SingleProfileDetailView singleProfileDetailView,
                    SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
        this.singleProfileDetailView = singleProfileDetailView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_work);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);


        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        tvDialogWorkFromDay.setOnClickListener(this);
        tvDialogWorkFromMonth.setOnClickListener(this);
        tvDialogWorkFromYear.setOnClickListener(this);

        tvDialogWorkToDay.setOnClickListener(this);
        tvDialogWorkToMonth.setOnClickListener(this);
        tvDialogWorkToYear.setOnClickListener(this);

        btnDialogCancel
                .setOnClickListener(new View.OnClickListener()

                                    {
                                        @Override
                                        public void onClick(View view)
                                        {
                                            dismiss();
                                        }
                                    }

                );

        btnDialogSave
                .setOnClickListener(new View.OnClickListener()

                                    {
                                        @Override
                                        public void onClick(View view)
                                        {
                                            newWorkBean.setWorkId(id);
                                            newWorkBean.setCompany(etDialogWorkWhere.getText().toString());
                                            newWorkBean.setCity(etDialogWorkCity.getText().toString());
                                            newWorkBean.setDescription(etDialogWorkDesc.getText().toString());
                                            newWorkBean.setPosition(etDialogWorkPosition.getText().toString());

                                            if (fromDate.equals("") && toDate.equals(""))
                                            {

                                                fromDate = WorkFromDate;
                                                toDate = WorkToDate;
                                                newWorkBean.setTimePeriodFrom(fromDate);
                                                newWorkBean.setTimePeriodTo(toDate);

                                            } else if (fromDate.equals(""))
                                            {
                                                fromDate = WorkFromDate;
                                                newWorkBean.setTimePeriodFrom(fromDate);
                                                newWorkBean.setTimePeriodTo(toDate);
                                            } else if (toDate.equals(""))
                                            {
                                                toDate = WorkToDate;
                                                newWorkBean.setTimePeriodFrom(fromDate);
                                                newWorkBean.setTimePeriodTo(toDate);
                                            } else
                                            {
                                                newWorkBean.setTimePeriodFrom(fromDate);
                                                newWorkBean.setTimePeriodTo(toDate);
                                            }

                                            if (singleProfileDetailView != null)
                                            {
                                                userProfileDetailGetForEdit.getData().getWork().add(newWorkBean);

                                                SingleDetailModel singleDetailModel =
                                                        new SingleDetailModel(newWorkBean.getWorkId(),
                                                                etDialogWorkWhere.getText().toString(),
                                                                R.drawable.ic_camera_alt_black_18dp);

                                                singleDetailModel.setReadOnly(false);
                                                singleDetailModel
                                                        .setEditSingleDetailListener(singleProfileDetailView.getSingleProfileDetailModel());


                                                singleProfileDetailView.addNewSingleDetail(singleDetailModel);
                                            } else
                                            {
                                                Log.i("WORK ID**********", id);
                                                for (UserProfileDetailGetForEdit.DataBean.WorkBean work :
                                                        userProfileDetailGetForEdit.getData().getWork())
                                                {
                                                    Log.i("ITERATING WORK ID***", work.getWorkId());

                                                    if (newWorkBean.getWorkId().equals(work.getWorkId()))
                                                    {
                                                        work.setWorkId(newWorkBean.getWorkId());
                                                        work.setCity(newWorkBean.getCity());
                                                        work.setCompany(newWorkBean.getCompany());
                                                        work.setDescription(newWorkBean.getDescription());
                                                        work.setPosition(newWorkBean.getPosition());
                                                        work.setTimePeriodFrom(newWorkBean.getTimePeriodFrom());
                                                        work.setTimePeriodTo(newWorkBean.getTimePeriodTo());
                                                    }
                                                }
                                                singleDetailView.tvSingleDetailValue.setText(newWorkBean.getCompany());
                                            }

                                            dismiss();
                                        }
                                    }

                );

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

        if (singleDetailView != null)
        {
            fillDataFromExistingModel();
        }

    }

    private void fillDataFromExistingModel()
    {

        for (UserProfileDetailGetForEdit.DataBean.WorkBean work :
                userProfileDetailGetForEdit.getData().getWork())
        {
            if (work.getCompany().contains(singleDetailView.getSingleDetailModel()
                    .getTextValue()))
            {
                existingWorkBean = work;
            }
        }

        if (existingWorkBean != null)
        {
            etDialogWorkWhere.setText(existingWorkBean.getCompany());
            etDialogWorkPosition.setText(existingWorkBean.getPosition());
            etDialogWorkCity.setText(existingWorkBean.getCity());
            etDialogWorkDesc.setText(existingWorkBean.getDescription());
            WorkFromDate = existingWorkBean.getTimePeriodFrom();
            WorkToDate = existingWorkBean.getTimePeriodTo();


            String FromWork, ToWork;


            if (WorkFromDate.contains("T"))
            {
                FromWork = Utils.changeDateFormat(WorkFromDate, "yyyy-MM-dd'T'HH:mm:ss", "MM/dd/yyyy");
            } else
            {
                FromWork = WorkFromDate;
            }
            if (FromWork.length() < 11)
            {
                String[] datePart = FromWork.split("/");
                tvDialogWorkFromDay.setText(datePart[0]);
                tvDialogWorkFromMonth.setText(datePart[1]);
                tvDialogWorkFromYear.setText(datePart[2]);
            }
            if (WorkToDate.contains("T"))
            {
                ToWork = Utils.changeDateFormat(WorkToDate, "yyyy-MM-dd'T'HH:mm:ss", "MM/dd/yyyy");
            } else
            {
                ToWork = WorkToDate;
            }
            if (ToWork.length() < 11)
            {
                String[] datePart = ToWork.split("/");
                tvDialogWorkToDay.setText(datePart[0]);
                tvDialogWorkToMonth.setText(datePart[1]);
                tvDialogWorkToYear.setText(datePart[2]);
            }

        }

    }


    @Override
    public void onChangePrivacy(String id)
    {
        if (existingWorkBean != null)
        {
            existingWorkBean.setWorkVisibleTo(id);
        } else
        {
            newWorkBean.setWorkVisibleTo(id);
        }
    }


    @Override
    public void onClick(View view)
    {
        super.onClick(view);
        if (view.getId() == R.id.tv_dialogWorkFromDay || view.getId() == R.id.tv_dialogWorkFromMonth
                || view.getId() == R.id.tv_dialogWorkFromYear)
        {
            Calendar now = Calendar.getInstance();
            android.app.DatePickerDialog dpd;

            dpd = new android.app.DatePickerDialog(getContext(), new android.app.DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth)
                {
                    tvDialogWorkFromDay.setText(dayOfMonth + "");
                    tvDialogWorkFromMonth.setText((monthOfYear + 1) + "");
                    tvDialogWorkFromYear.setText(year + "");
                    fromDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                }
            }, now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
            dpd.show();

        } else if (view.getId() == R.id.tv_dialogWorkToDay || view.getId() == R.id.tv_dialogWorkToMonth
                || view.getId() == R.id.tv_dialogWorkToYear)
        {
            Calendar now = Calendar.getInstance();
            android.app.DatePickerDialog dpd;

            dpd = new android.app.DatePickerDialog(getContext(), new android.app.DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth)
                {
                    tvDialogWorkToDay.setText(dayOfMonth + "");
                    tvDialogWorkToMonth.setText((monthOfYear + 1) + "");
                    tvDialogWorkToYear.setText(year + "");
                    toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                }
            }, now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));

            dpd.show();

        }


    }

}
