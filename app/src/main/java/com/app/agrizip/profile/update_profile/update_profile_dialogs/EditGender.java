package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/27/2016 at 4:50 PM.
 */

public class EditGender extends CustomDialog implements RadioGroup.OnCheckedChangeListener
{

    @BindView(R.id.rg_dialogEditGender)
    RadioGroup rgDialogEditGender;

    @BindView(R.id.rb_dialogGenderFemale)
    RadioButton rbDialogGenderFemale;

    @BindView(R.id.rb_dialogGenderMale)
    RadioButton rbDialogGenderMale;
    int flag=0;
    public EditGender(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit, SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_edit_gender);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        if (userProfileDetailGetForEdit.getData().getGender() != null)
        {
            if (userProfileDetailGetForEdit.getData().getGender().equalsIgnoreCase("male"))
            {
                rbDialogGenderMale.setChecked(true);
            } else if (userProfileDetailGetForEdit.getData().getGender().equalsIgnoreCase("female"))
            {
                rbDialogGenderFemale.setChecked(true);
            }
        }

        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {


                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(flag==1)
                {
                    userProfileDetailGetForEdit.getData().setGender("Male");
                    singleDetailView.tvSingleDetailValue.setText("Male");
                }
                else if(flag==2)
                {
                    userProfileDetailGetForEdit.getData().setGender("Female");
                    singleDetailView.tvSingleDetailValue.setText("Female");
                }
                dismiss();
            }
        });

        rgDialogEditGender.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedRadioButton)
    {
        if (checkedRadioButton == R.id.rb_dialogGenderMale)
        {
            flag=1;

        } else if (checkedRadioButton == R.id.rb_dialogGenderFemale)
        {
            flag=2;

        }

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

    }

    @Override
    public void onChangePrivacy(String id)
    {
        userProfileDetailGetForEdit.getData().setGenderVisibleTo(id);
    }
}
