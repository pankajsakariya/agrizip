package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import views.SingleDetailView;

/**
 * Created by rutvik on 2/27/2017 at 1:53 PM.
 */

public class EditName extends CustomDialog
{

    @BindView(R.id.et_dialogEditName)
    EditText etDialogEditName;

    public EditName(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit, SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_edit_name);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                saveName();
            }
        });

        etDialogEditName.setText(singleDetailView.tvSingleDetailValue.getText());

    }

    private void saveName()
    {
        if(etDialogEditName.getText().toString().trim().length() == 0){
            Toast.makeText(getContext(),"Please Enter Name",Toast.LENGTH_LONG).show();
        }else{
            userProfileDetailGetForEdit.getData()
                    .setFullName(etDialogEditName.getText().toString());

            singleDetailView
                    .tvSingleDetailValue.setText(etDialogEditName.getText().toString());

            dismiss();
        }


    }

    @Override
    public void onChangePrivacy(String visibilityId)
    {
    }
}
