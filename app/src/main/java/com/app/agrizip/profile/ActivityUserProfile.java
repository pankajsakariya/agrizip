package com.app.agrizip.profile;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.followers.activity.ActivityFollowers;
import com.app.agrizip.friends.friend_search.ActivitySearchUser;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.home.adapter.MasterAdapter;
import com.app.agrizip.home.fragment.FragmentNewsFeed;
import com.app.agrizip.post.post_comments.dailog.CommentsDialog;
import com.app.agrizip.profile.update_profile.UpdateProfileActivity;
import com.google.gson.Gson;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FriendList;
import apimodels.SinglePost;
import apimodels.StatusPostList;
import apimodels.UpdateProfilePic;
import apimodels.UserInfo;
import apimodels.UserProfileDetailsInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Response;
import viewholders.VHSinglePost;
import viewmodels.SingleDetailModel;
import viewmodels.SingleProfileDetailModel;

public class ActivityUserProfile extends AppCompatActivity
        implements SinglePost.PostCallbacksListener,
        SwipeRefreshLayout.OnRefreshListener, UserInfo.UserEventCallbacks,CommentsDialog.CommentCountChangeListener{

    private static final String TAG = App.APP_TAG + ActivityUserProfile.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_userProfile)
    RecyclerView rvUserDetails;

    @BindView(R.id.et_searchUser)
    EditText etSearchUser;

    String newStatusPostId;

    private OnPostPhotoUploadedSuccessfully onPostPhotoUploadedSuccessfully;


    MasterAdapter adapter;
    boolean updateProfilePic = false;
    final Handler mHandler = new Handler();

    RetrofitCallbacks<UserProfileDetailsInfo> userProfileDetailsInfoCallback;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    boolean busyLoadingData = false;
    int pageNo = 1;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    LinearLayoutManager llm;
    CommentsDialog commentsDialog;
    @BindView(R.id.srl_refreshUserProfile)
    SwipeRefreshLayout srlRefreshUserProfile;
    private UserProfileDetailsInfo userProfileDetailsInfo = null;
    private String userId = "";
    private boolean isSelf = false;
    private boolean isShowingComments = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ButterKnife.bind(this);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        onPostPhotoUploadedSuccessfully = new OnPostPhotoUploadedSuccessfully();

        try{
            this.registerReceiver(onPostPhotoUploadedSuccessfully,new IntentFilter(Constants.ON_POST_PHOTO_UPLOADED_SUCCESSFULLY));
        }catch (Exception e){
            e.printStackTrace();
        }
        rvUserDetails.setHasFixedSize(true);

        llm = new LinearLayoutManager(this);

        rvUserDetails.setLayoutManager(llm);

        rvUserDetails.addOnScrollListener(new OnScrollRecyclerView());

        adapter = new MasterAdapter(this);

        rvUserDetails.setAdapter(adapter);

        userId = getIntent().getStringExtra(Constants.USER_ID);
        isSelf = getIntent().getBooleanExtra(Constants.IS_SEEING_SELF_PROFILE, false);

        if (!userId.isEmpty()) {
            getUserProfileDetails();
        }

        etSearchUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityUserProfile.this, ActivitySearchUser.class));
            }
        });

        App.getInstance().profileNavigationList.add(userId);
        srlRefreshUserProfile.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshUserProfile.setOnRefreshListener(this);

    }



    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver();
    }
    public void unregisterReceiver()
    {
        try{
        if (onPostPhotoUploadedSuccessfully != null)
        {
            this.unregisterReceiver(onPostPhotoUploadedSuccessfully);
        }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void getUserProfileDetails() {

        userProfileDetailsInfoCallback = new RetrofitCallbacks<UserProfileDetailsInfo>(this) {

            @Override
            public void onResponse(Call<UserProfileDetailsInfo> call, Response<UserProfileDetailsInfo> response) {
                super.onResponse(call, response);
                if (srlRefreshUserProfile.isRefreshing()) {
                    srlRefreshUserProfile.setRefreshing(false);
                }
                if (response.isSuccessful()) {

                    flLoading.setVisibility(View.GONE);

                    ActivityUserProfile.this.userProfileDetailsInfo = response.body();


                    if (userProfileDetailsInfo != null) {
                        adapter.clear();
                        setupAdapter();
                    }

                } else {
                    Toast.makeText(ActivityUserProfile.this, R.string.something_went_erong, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserProfileDetailsInfo> call, Throwable t) {
                super.onFailure(call, t);
                if (srlRefreshUserProfile.isRefreshing()) {
                    srlRefreshUserProfile.setRefreshing(false);
                }
            }
        };

        API.getInstance().getUserProfileInfo(userId, userProfileDetailsInfoCallback);

    }

    @Override
    public void onBackPressed() {
        if (UpdateProfileActivity.Update_profile_detail) {
            updateProfilePic = false;
            Intent intent = new Intent(this, ActivityHome.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent);
            //startActivity(new Intent(this, ActivityHome.class));
            UpdateProfileActivity.Update_profile_detail = false;
        }
        if(updateProfilePic){
            updateProfilePic = false;
            Intent intent = new Intent(this, ActivityHome.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent);
        }
        super.onBackPressed();
    }

    private void setupAdapter() {
        adapter.addBigProfileViewWithDetails("big_profile_view", userProfileDetailsInfo.getData());

        adapter.addUserProfileExtraStrip("user_profile_extra_strip", userProfileDetailsInfo.getData());

        setupProfileDetails();

        if (isSelf) {
            App.getInstance().getUser().setShowNeedHelpInWhatsOnYourMind(true);
            adapter.addWhatsOnYourMind("whats_on_your_mind", App.getInstance().getUser());
        }

        adapter.notifyDataSetChanged();

        getUserFeeds();
    }

    private void setupProfileDetails() {

        final UserProfileDetailsInfo.DataBean info = userProfileDetailsInfo.getData();

        // This is Parent
        SingleProfileDetailModel profileDetails;

        if (info.getUserId().equals(App.getInstance().getUser().getId())) {
            profileDetails =
                    new SingleProfileDetailModel(this, "Profile Details",
                            R.drawable.ic_profile_detail_512,
                            false, userProfileDetailsInfo.getData());
        } else {
            profileDetails =
                    new SingleProfileDetailModel(this, "Profile Details",
                            R.drawable.name_white_32, true,
                            userProfileDetailsInfo.getData());
        }



        if (info.getWork() != null) {
            if (info.getWork().size() > 0) {
                for (UserProfileDetailsInfo.DataBean.WorkBean work : info.getWork()) {
                    //Make Child Detail
                    final SingleDetailModel singleDetailModel = new SingleDetailModel("work", work.getPosition() + " at " + work.getCompany(),
                            R.drawable.work27);
                    singleDetailModel.setDetailVisible(3);
                    // ADD CHILD TO PARENT
                    profileDetails
                            .addSingleDetailModel(singleDetailModel);
                }
            }
        }

        if (info.getEducation() != null) {
            if (info.getEducation().size() > 0) {
                for (UserProfileDetailsInfo.DataBean.EducationBean education : info.getEducation()) {

                    //MAKE CHILD DETAIL
                    final SingleDetailModel singleDetailModel = new SingleDetailModel("education", education.getInstituteName(),
                            R.drawable.student27);
                    singleDetailModel.setDetailVisible(3);

                    //ADD CHILD TO PARENT
                    profileDetails
                            .addSingleDetailModel(singleDetailModel);
                }
            }
        }

        if(info.isIsCityShow()){
            //MAKE CHILD DETAIL
            final SingleDetailModel city = new SingleDetailModel("current_city", "Lives in " + info.getCity() + ", "
                    + info.getCountryName(),
                    R.drawable.location227);

            city.setDetailVisible(3);

            // ADD CHILD TO PARENT
            profileDetails
                    .addSingleDetailModel(city);

        }


        //RELATIONSHIP STATUS
        if (info.isIsRelationshipShow()) {

            // MAKE CHILD DETAIL
            final SingleDetailModel relationship = new SingleDetailModel("relationship_status",info.getRelationshipStatus(),
                    R.drawable.relationship27);
            relationship.setDetailVisible(3);


            //ADD CHILD TO PARENT
            profileDetails
                    .addSingleDetailModel(relationship);
        }

        System.out.println("Hello");

/*
        final SingleDetailModel followedBy = new SingleDetailModel("", "Followed By " + info.getFollowCount() + " People",
                R.drawable.ic_info_black_18dp);
        followedBy.setDetailVisible(3);

        profileDetails
                .addSingleDetailModel(followedBy);
*/

        profileDetails.setShowFriendsAndPhotos(true, userProfileDetailsInfo.getData().getFriends());


        adapter.addSingleProfileDetail("profileDetails", profileDetails);
    }


    private void getUserFeeds() {

        API.getInstance().getStatusPostListForUser(userId,
                pageNo,
                new RetrofitCallbacks<StatusPostList>(this) {
                    @Override
                    public void onResponse(Call<StatusPostList> call, Response<StatusPostList> response) {
                        try {
                            super.onResponse(call, response);
                            if (response.isSuccessful()) {
                                for (SinglePost singlePost : response.body().getData()) {
                                    singlePost.setUserProfileDetailsInfo(userProfileDetailsInfo.getData());
                                    singlePost.setPostCallbacksListener(ActivityUserProfile.this);
                                    adapter.addSinglePost(singlePost.getStatusId(), singlePost);
                                }
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<StatusPostList> call, Throwable t) {
                        super.onFailure(call, t);
                    }
                });

    }



    @Override
    protected void onDestroy() {
        App.getInstance().getUser().setShowNeedHelpInWhatsOnYourMind(false);
        App.getInstance().profileNavigationList.remove(userId);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (UpdateProfileActivity.Update_profile_detail) {
                updateProfilePic = false;
                Intent intent = new Intent(this, ActivityHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
                //startActivity(new Intent(this, ActivityHome.class));
                UpdateProfileActivity.Update_profile_detail = false;
            }
            if(updateProfilePic){
                updateProfilePic = false;
                Intent intent = new Intent(this, ActivityHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
            }
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.CHANGE_PROFILE_PIC) {
            if(data != null){
            final ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                String imagePath = images.get(0).getPath();
            Constants.USERPROFILE_IMAGE = imagePath;

                String userJSON = PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(Constants.PREF_USER, "");
                if (!TextUtils.isEmpty(userJSON)) {
                    UserInfo.User user = new Gson().fromJson(userJSON, UserInfo.User.class);
                    user.setUserImage(imagePath);
                    App.getInstance().setUser(user);
                }
                updateProfilePic = true;
            adapter.notifyDataSetChanged();
            updateUserProfilePicture(Utils.convertImageFileToBase64(imagePath));
            }

            /*Uri selectedImage = data.getData();

                Cursor cursor = getContentResolver().query(selectedImage,null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    String document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                    cursor.close();


                    cursor = getContentResolver().query(
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                        cursor.close();
                        // Change Image code Here
                        Constants.USERPROFILE_IMAGE = path;
                        //App.getInstance().saveUserImage(path);
                        String userJSON = PreferenceManager.getDefaultSharedPreferences(this)
                                .getString(Constants.PREF_USER, "");
                        if (!TextUtils.isEmpty(userJSON)) {
                            UserInfo.User user = new Gson().fromJson(userJSON, UserInfo.User.class);
                            user.setUserImage(path);
                            App.getInstance().setUser(user);
                        }
                        adapter.notifyDataSetChanged();
                      //  updateUserProfilePicture(Utils.convertImageFileToBase64(path));}
                }


            }*/


        }
        if(requestCode == 444){
            int commentcount = data.getIntExtra("COMMENT_COUNT",0);
            int likecount =data.getIntExtra("LIKE_COUNT",0);
            int sharecount = data.getIntExtra("SHARE_COUNT",0);
            System.out.println("Hello");
            adapter.updateModel(likecount,commentcount,sharecount, VHSinglePost.temp_comment_count);
        }
    }

    private void updateUserProfilePicture(String base64String) {

        final UpdateProfilePic newProfilePic = new UpdateProfilePic();

        newProfilePic.setUserId(App.getInstance().getUser().getId());
        newProfilePic.setImagestr(base64String);

        final RetrofitCallbacks<UpdateProfilePic.UpdatedProfilePicDetails> onChangeUserProfilePicture =
                new RetrofitCallbacks<UpdateProfilePic.UpdatedProfilePicDetails>(this) {
                    @Override
                    public void onResponse(Call<UpdateProfilePic.UpdatedProfilePicDetails> call, Response<UpdateProfilePic.UpdatedProfilePicDetails> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            Toast.makeText(ActivityUserProfile.this,
                                    "Profile Picture Changed Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().changeProfilePicture(newProfilePic
                , onChangeUserProfilePicture);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(onPostPhotoUploadedSuccessfully == null){
            onPostPhotoUploadedSuccessfully = new OnPostPhotoUploadedSuccessfully();

            try{
                this.registerReceiver(onPostPhotoUploadedSuccessfully,new IntentFilter(Constants.ON_POST_PHOTO_UPLOADED_SUCCESSFULLY));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        App.getInstance().getUser().setUserEventCallbacks(this);
    }

    @Override
    public void onClickComments(String postId,int count) {
        isShowingComments = true;
        commentsDialog = new CommentsDialog(this, postId,String.valueOf(count));
        commentsDialog.mListenerCount = this;
        commentsDialog.show();
    }

    public boolean isShowingComments() {
        return isShowingComments;
    }


    public void hideCommentsFragment() {
        isShowingComments = false;
        commentsDialog.dismiss();
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        getUserProfileDetails();
    }

    @Override
    public void onStatusPostSuccessful(SinglePost singlePost) {
        singlePost.setUserProfileDetailsInfo(userProfileDetailsInfo.getData());
        adapter.addNewSinglePost(singlePost.getStatusId(), singlePost);
        adapter.notifyItemInserted(1);
    }

    @Override
    public void onStatusPostEdit(SinglePost singlePost) {

    }

    @Override
    public void onStatusDeleted(String statusId) {
        adapter.removePostById(statusId);
    }

    @Override
    public void OnChangeCommentCount(int count, int position) {
        adapter.changeCommentcount(count,position);
    }


    class OnScrollRecyclerView extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (!busyLoadingData) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        pageNo++;
                        getUserFeeds();
                    }
                }
            }
        }
    }

    class OnPostPhotoUploadedSuccessfully extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String postId = intent.getStringExtra(Constants.POST_ID);
            if (postId != null)
            {
                mHandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        newStatusPostId = postId;
                        getUserProfileDetails();
                       // addNotification("Images uploaded successfully!", false);
                    }
                });
            }
        }
    }

    private void addNotification(String contentText, boolean isOngoing)
    {
        Notification.Builder m_notificationBuilder = new Notification.Builder(this)
                .setContentTitle("Agrizip")
                .setContentText(contentText)
                .setOngoing(isOngoing)
                .setSmallIcon(R.drawable.ic_camera_alt_black_18dp);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // send the notification
        notificationManager.notify(1066, m_notificationBuilder.build());

    }

}
