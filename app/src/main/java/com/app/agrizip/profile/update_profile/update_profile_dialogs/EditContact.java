package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/27/2016 at 3:13 PM.
 */

public class EditContact extends CustomDialog
{

    @BindView(R.id.et_dialogEditContact)
    EditText etDialogEditContact;
    private String mobileNumber;
    boolean flag;

    public EditContact(Context context, String id, UserProfileDetailGetForEdit userProfileDetailGetForEdit,
                       SingleDetailView singleDetailView, MasterAdapter adapter)
    {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_contact);

        ButterKnife.bind(this);
         setCanceledOnTouchOutside(false);

        etDialogEditContact.setText(singleDetailView.tvSingleDetailValue.getText().toString());
        mobileNumber=etDialogEditContact.getText().toString();

        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

               if(etDialogEditContact.getText().toString().equals("")){

                   singleDetailView.tvSingleDetailValue.setText(singleDetailView.tvSingleDetailValue.getText().toString());
                   dismiss();

               }
               else if(mobileNumber.equals(singleDetailView.tvSingleDetailValue.getText().toString()))
                {
                    singleDetailView.tvSingleDetailValue.setText(singleDetailView.tvSingleDetailValue.getText().toString());
                    //Toast.makeText(getContext(),"Mobile Number Same as Previous one",Toast.LENGTH_LONG).show();
                    dismiss();

                }else{
                   userProfileDetailGetForEdit.getData()
                           .setMobileNumber(etDialogEditContact.getText().toString());
                   singleDetailView.tvSingleDetailValue.setText(etDialogEditContact.getText().toString());



                   dismiss();
                }




            }
        });

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

    }


    @Override
    public void onChangePrivacy(String id)
    {
        userProfileDetailGetForEdit.getData().setMobileVisibleTo(id);
    }
}
