package com.app.agrizip.profile.update_profile;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.home.adapter.MasterAdapter;
import com.app.agrizip.profile.ActivityUserProfile;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditBirthday;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditContact;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditCurrentCity;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditEducation;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditEmail;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditGender;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditHomeTown;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditName;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditRelationship;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditWork;
import com.google.gson.Gson;

import java.util.ArrayList;

import api.API;
import api.RetrofitCallbacks;
import apimodels.UserInfo;
import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import viewmodels.SingleDetailModel;
import viewmodels.SingleProfileDetailModel;
import views.SingleDetailView;
import views.SingleProfileDetailView;

public class UpdateProfileActivity extends AppCompatActivity
        implements SingleDetailModel.EditSingleDetailListener,
        SingleProfileDetailModel.AddSingleDetailListener {

    private static final String TAG = App.APP_TAG + UpdateProfileActivity.class.getSimpleName();
    final ArrayList<SingleProfileDetailModel> singleProfileDetailModelArrayList = new ArrayList<>();
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    String tempDate;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.rv_editProfileDetails)
    RecyclerView rvEditProfileDetails;
    MasterAdapter adapter;
    UserProfileDetailGetForEdit userProfileDetailGetForEdit;
    RetrofitCallbacks userProfileDetailsInfoCallback;
    TryUpdatingUserProfile tryUpdatingUserProfile;

    @BindView(R.id.fl_loading)
    FrameLayout flLoading;

    @BindView(R.id.btn_saveEditProfileDetails)
    Button btnSaveEditProfileDetails;
    String WHERE_COME_FROM;
    public static boolean Update_profile_detail = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_details);

        ButterKnife.bind(this);

        if (mToolbar != null) {
            if (tvToolbarTitle != null) {
                tvToolbarTitle.setText("Edit Profile Details");
            }
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
        WHERE_COME_FROM = this.getIntent().getStringExtra("COME_FROM");
        System.out.println("!! " + WHERE_COME_FROM);
        rvEditProfileDetails.setLayoutManager(new LinearLayoutManager(this));
        rvEditProfileDetails.setHasFixedSize(true);

        adapter = new MasterAdapter(this);

        rvEditProfileDetails.setAdapter(adapter);

        getUserProfileDetails();

    }

    private void getUserProfileDetails() {

        userProfileDetailsInfoCallback = new RetrofitCallbacks<UserProfileDetailGetForEdit>(this) {

            @Override
            public void onResponse(Call<UserProfileDetailGetForEdit> call, Response<UserProfileDetailGetForEdit> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {

                    btnSaveEditProfileDetails.setOnClickListener(new TryUpdatingUserProfile());

                    flLoading.setVisibility(View.GONE);

                    UpdateProfileActivity.this.userProfileDetailGetForEdit = response.body();

                    if (userProfileDetailGetForEdit != null) {
                        setupProfileDetails();
                    }

                }
            }
        };

        API.getInstance().getUserProfileDetailForEdit(App.getInstance().
                getUser().getId(), userProfileDetailsInfoCallback);

    }

    private void setupProfileDetails() {

        final UserProfileDetailGetForEdit.DataBean info = userProfileDetailGetForEdit.getData();


        /*NAME*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel name = new SingleProfileDetailModel(this, "Name",
                R.drawable.ic_people_white_18dp, true,
                userProfileDetailGetForEdit.getData());

        //MAKE CHILD DETAILS
        SingleDetailModel singleNameDetail =
                new SingleDetailModel("name", info.getFullName(), R.drawable.ic_info_black_18dp);
        singleNameDetail.setReadOnly(false);
        singleNameDetail.setEditSingleDetailListener(this);
        singleNameDetail.setDetailVisible(3);

        //ADD CHILD DETAILS TO PARENT DETAILS
        name.addSingleDetailModel(singleNameDetail);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(name);
        adapter.addSingleProfileDetail("name", name);
        /////////////////////////////////////////////////////////


        /*EMAIL*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel email = new SingleProfileDetailModel(this, "Email",
                R.drawable.ic_mail_white_18dp, true,
                userProfileDetailGetForEdit.getData());

        //MAKE CHILD DETAILS
        String tempEmail;
        if (info.getEmailAddress().equals("")) {
            tempEmail = "Update Your Email";
        } else {
            tempEmail = info.getEmailAddress();
        }
        SingleDetailModel singleEmailDetail =
                new SingleDetailModel("email", tempEmail, R.drawable.ic_info_black_18dp);

        singleEmailDetail.setReadOnly(false);
        singleEmailDetail.setEditSingleDetailListener(this);
        singleEmailDetail.setDetailVisible(3);

        //ADD CHILD DETAILS TO PARENT DETAILS
        email.addSingleDetailModel(singleEmailDetail);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(email);
        adapter.addSingleProfileDetail("email", email);
        /////////////////////////////////////////////////////////


        /*MOBILE*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel mobile = new SingleProfileDetailModel(this, "Mobile",
                R.drawable.ic_phone_iphone_white_18dp, true,
                userProfileDetailGetForEdit.getData());

        //MAKE CHILD DETAILS
        SingleDetailModel singleMobileDetail =
                new SingleDetailModel("mobile", info.getMobileNumber(), R.drawable.ic_info_black_18dp);
        singleMobileDetail.setReadOnly(false);
        singleMobileDetail.setEditSingleDetailListener(this);
        singleMobileDetail.setDetailVisible(3);

        //ADD CHILD DETAILS TO PARENT DETAILS
        mobile.addSingleDetailModel(singleMobileDetail);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(mobile);
        adapter.addSingleProfileDetail("mobile", mobile);
        /////////////////////////////////////////////////////////


        /*Birthday*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel birthday = new SingleProfileDetailModel(this, "Birthday",
                R.drawable.ic_cake_white_18dp, true,
                userProfileDetailGetForEdit.getData());

        //MAKE CHILD DETAILS

        if (info.getDob().contains("T")) {
            tempDate = Utils.changeDateFormat(info.getDob(), "yyyy-MM-dd'T'HH:mm:ss", "dd/MM/yyyy");
        } else {
            tempDate = info.getDob();
        }
        SingleDetailModel singleBirthDayDetail =
                new SingleDetailModel("birthday", tempDate, R.drawable.ic_info_black_18dp);
        singleBirthDayDetail.setReadOnly(false);
        singleBirthDayDetail.setEditSingleDetailListener(this);
        singleBirthDayDetail.setDetailVisible(3);


      //  userProfileDetailGetForEdit.getData().setDob(tempDate);

        //ADD CHILD DETAILS TO PARENT DETAILS
        birthday.addSingleDetailModel(singleBirthDayDetail);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(birthday);
        adapter.addSingleProfileDetail("birthday", birthday);
        /////////////////////////////////////////////////////////


        /*GENDER*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel gender = new SingleProfileDetailModel(this, "Gender",
                R.drawable.ic_friend_request_white512, true,
                userProfileDetailGetForEdit.getData());

        //MAKE CHILD DETAILS
        SingleDetailModel genderDetail =
                new SingleDetailModel("gender", info.getGender(), R.drawable.ic_info_black_18dp);
        genderDetail.setReadOnly(false);
        genderDetail.setEditSingleDetailListener(this);
        genderDetail.setDetailVisible(3);

        //ADD CHILD DETAILS TO PARENT DETAILS
        gender.addSingleDetailModel(genderDetail);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(gender);
        adapter.addSingleProfileDetail("gender", gender);
        /////////////////////////////////////////////////////////



        /*WORK DETAILS*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel work = new SingleProfileDetailModel(this, "Work",
                R.drawable.ic_business_center_white_18dp, true,
                userProfileDetailGetForEdit.getData());


        if (userProfileDetailGetForEdit.getData().getWork() != null) {
            for (UserProfileDetailGetForEdit.DataBean.WorkBean singleWork : info.getWork()) {
                //MAKE CHILD DETAILS
                Log.d("WorkBean=====", singleWork.getDescription());

                //Log.d("WorkBean=====", singleWork.getDescription());

                SingleDetailModel workDetails =
                        new SingleDetailModel(singleWork.getWorkId(), "Work", singleWork.getCompany(),
                                null);
                workDetails.setReadOnly(false);
                workDetails.setEditSingleDetailListener(work);

                //ADD CHILD DETAILS TO PARENT DETAILS
                work.addSingleDetailModel(workDetails);
            }
            work.setUserProfileDetailGetForEdit(userProfileDetailGetForEdit);
        }
        work.setAddSingleDetailListener("Add Work", this);


        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(work);
        adapter.addSingleProfileDetail("work", work);
        /////////////////////////////////////////////////////////


        /*EDUCATION DETAILS*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel education = new SingleProfileDetailModel(this, "Education",
                R.drawable.ic_school_white_18dp, true,
                userProfileDetailGetForEdit.getData());

        if (userProfileDetailGetForEdit.getData().getEducation() != null) {
            for (UserProfileDetailGetForEdit.DataBean.EducationBean singleEducation : info.getEducation()) {

                //MAKE CHILD DETAILS

                SingleDetailModel educationDetail =
                        new SingleDetailModel(singleEducation.getEducationId(), "Education", singleEducation.getInstituteName(),
                                null);
                educationDetail.setReadOnly(false);
                educationDetail.setEditSingleDetailListener(education);

                //ADD CHILD DETAILS TO PARENT DETAILS
                education.addSingleDetailModel(educationDetail);
            }
            education.setUserProfileDetailGetForEdit(userProfileDetailGetForEdit);
        }
        education.setAddSingleDetailListener("Add Education", this);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(education);
        adapter.addSingleProfileDetail("education", education);
        /////////////////////////////////////////////////////////



        /*Current City*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel currentCity = new SingleProfileDetailModel(this, "Current City",
                R.drawable.ic_location_on_white_18dp, true,
                userProfileDetailGetForEdit.getData());

        if(info.getCity().length() > 0){

        }else{
            info.setCity("Lives In");
        }
        //MAKE CHILD DETAILS
        SingleDetailModel singleCurrentCity;

        singleCurrentCity =
                new SingleDetailModel("current_city", info.getCity(),
                        null);


        singleCurrentCity.setReadOnly(false);
        singleCurrentCity.setEditSingleDetailListener(this);
        singleCurrentCity.setDetailVisible(userProfileDetailGetForEdit.getData()
                .isCityShow() ? 1 : 2);

        //ADD CHILD DETAILS TO PARENT DETAILS
        currentCity.addSingleDetailModel(singleCurrentCity);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(currentCity);
        adapter.addSingleProfileDetail("current_city", currentCity);
        /////////////////////////////////////////////////////////



        /*Home Town*/
        //MAKE PARENT DETAIL////////////////////////////////////
        SingleProfileDetailModel homeTown = new SingleProfileDetailModel(this, "Home Town",
                R.drawable.ic_home_white_18dp, true,
                userProfileDetailGetForEdit.getData());

        if(info.getHometown().length() > 0){

        }else{
            info.setHometown("From");
        }
        //MAKE CHILD DETAILS
        SingleDetailModel singleHomeTown;

       /* singleHomeTown =
                new SingleDetailModel("home_town", info.getHometown()
                        + info.getCity(),
                        null);*/
        singleHomeTown =
                new SingleDetailModel("home_town", info.getHometown(),
                        null);
        singleHomeTown.setReadOnly(false);
        singleHomeTown.setEditSingleDetailListener(this);
        singleHomeTown.setDetailVisible(userProfileDetailGetForEdit.getData()
                .isHometownShow() ? 1 : 2);

        //ADD CHILD DETAILS TO PARENT DETAILS
        homeTown.addSingleDetailModel(singleHomeTown);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(homeTown);
        adapter.addSingleProfileDetail("home_town", homeTown);
        /////////////////////////////////////////////////////////



        /*Relationship Status*/
        //MAKE PARENT DETAIL////////////////////////////////////
        Log.i(TAG, "CREATING RELATIONSHIP STATUS DETAILS");
        SingleProfileDetailModel relationshipStatus = new SingleProfileDetailModel(this, "Relationship",
                R.drawable.ic_relationship_icon, true,
                userProfileDetailGetForEdit.getData());

        //MAKE CHILD DETAILS
        String tempRelation;
        if (info.getRelationshipStatus().equals("")) {
            tempRelation = "Update Your Relationship Status";
        } else {
            tempRelation = info.getRelationshipStatus();
        }
        SingleDetailModel singleRelationshipStatus =
                new SingleDetailModel("relationship_status", tempRelation,
                        null);
        singleRelationshipStatus.setReadOnly(false);
        singleRelationshipStatus.setEditSingleDetailListener(this);
        singleRelationshipStatus.setDetailVisible(userProfileDetailGetForEdit.getData()
                .isIsRelationshipShow() ? 1 : 2);

        //ADD CHILD DETAILS TO PARENT DETAILS
        relationshipStatus.addSingleDetailModel(singleRelationshipStatus);

        //ADD PARENT DETAILS TO ADAPTER
        singleProfileDetailModelArrayList.add(relationshipStatus);
        adapter.addSingleProfileDetail("relationship_status", relationshipStatus);
        /////////////////////////////////////////////////////////


        /**Followers
         //MAKE PARENT DETAIL////////////////////////////////////
         SingleProfileDetailModel followers = new SingleProfileDetailModel("Followers",
         R.drawable.ic_group_white_18dp, true,
         userProfileDetailGetForEdit.getData());

         //MAKE CHILD DETAILS
         SingleDetailModel singleFollowerDetail =
         new SingleDetailModel("followers", "Followed By " + info.getFollowUpCount() + " People",
         null);
         singleFollowerDetail.setReadOnly(true);
         singleFollowerDetail.setEditSingleDetailListener(this);
         singleFollowerDetail.setDetailVisible(userProfileDetailGetForEdit.getData()
         .isIsFollowUpShow() ? 1 : 2);

         //ADD CHILD DETAILS TO PARENT DETAILS
         followers.addSingleDetailModel(singleFollowerDetail);

         //ADD PARENT DETAILS TO ADAPTER
         singleProfileDetailModelArrayList.add(followers);
         adapter.addSingleProfileDetail("followers", followers);
         /////////////////////////////////////////////////////////*/


        //addSaveButton();

        adapter.notifyDataSetChanged();

    }


    private void addSaveButton() {
        tryUpdatingUserProfile = new TryUpdatingUserProfile() {

            @Override
            public void onClick(View view) {
                super.onClick(view);
                //Toast.makeText(ActivityEditProfileDetails.this, "I WAS CLICKED", Toast.LENGTH_SHORT).show();
            }
        };
        adapter.addSaveProfileDetailButton("save_user_profile_details", tryUpdatingUserProfile);
    }

    @Override
    public void onEditDetail(String id, final String value, final SingleDetailView singleDetailView) {
        switch (id) {
            case "name":

                EditName editName = new EditName(this,
                        "name",
                        userProfileDetailGetForEdit,
                        singleDetailView, adapter);
                editName.show();

                break;

            case "email":

                EditEmail editEmail = new EditEmail(this,
                        "email",
                        userProfileDetailGetForEdit,
                        singleDetailView, adapter);
                editEmail.show();

                break;

            case "mobile":

                EditContact editContact = new EditContact(this,
                        "mobile",
                        userProfileDetailGetForEdit,
                        singleDetailView, adapter);
                editContact.show();

                break;

            case "birthday":

                EditBirthday editBirthday = new EditBirthday(this, this,
                        "birthday",
                        userProfileDetailGetForEdit,
                        singleDetailView, adapter);
                editBirthday.show();

                break;

            case "gender":

                EditGender editGender = new EditGender(this,
                        "gender",
                        userProfileDetailGetForEdit,
                        singleDetailView, adapter);
                editGender.show();

                break;

            case "current_city":

                EditCurrentCity editCurrentCity = new EditCurrentCity(this,
                        "current_city",
                        userProfileDetailGetForEdit,
                        singleDetailView, adapter);
                editCurrentCity.show();

                break;

            case "relationship_status":

                EditRelationship editRelationship = new EditRelationship(this,
                        "relationship_status",
                        userProfileDetailGetForEdit,
                        singleDetailView, adapter);
                editRelationship.show();

                break;


            case "home_town":

                EditHomeTown editHomeTown = new EditHomeTown(this, "home_town",
                        userProfileDetailGetForEdit, singleDetailView,
                        adapter);

                editHomeTown.show();

                break;

        }
    }

    @Override
    public void onEditVisibility(String id, SingleDetailView singleDetailView) {
        switch (id) {
            case "current_city":

                userProfileDetailGetForEdit.getData().setCityShow(singleDetailView
                        .getSingleDetailModel().isDetailVisible() == 1);

                break;

            case "relationship_status":

                userProfileDetailGetForEdit.getData().setIsRelationshipShow(singleDetailView
                        .getSingleDetailModel().isDetailVisible() == 1);

                break;

            case "work":

                break;

            case "education":

                break;
            case "birthday":

                break;

            case "home_town":

                userProfileDetailGetForEdit.getData().setHometownShow(singleDetailView
                        .getSingleDetailModel().isDetailVisible() == 1);

                break;


            case "followers":

                userProfileDetailGetForEdit.getData().setIsFollowUpShow(singleDetailView
                        .getSingleDetailModel().isDetailVisible() == 1);

                break;
        }
    }


    /**
     * private void showEditBox(EditText input, String title, String text, DialogInterface.OnClickListener okClicked)
     * {
     * AlertDialog.Builder builder = new AlertDialog.Builder(this);
     * builder.setTitle(title);
     * <p>
     * // Set up the input
     * input.setText(text);
     * // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
     * input.setInputType(InputType.TYPE_CLASS_TEXT);
     * builder.setView(input);
     * <p>
     * // Set up the buttons
     * builder.setPositiveButton("OK", okClicked);
     * builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
     * {
     *
     * @Override public void onClick(DialogInterface dialog, int which)
     * {
     * dialog.cancel();
     * }
     * });
     * <p>
     * builder.show();
     * }
     */


    @Override
    public void onAddDetailClicked(final SingleProfileDetailView singleProfileDetailView) {

        switch (singleProfileDetailView.getTvAddNewDetailLabel().getText().toString()) {
            case "Add Work":

                EditWork editWork = new EditWork(this, "Work",
                        userProfileDetailGetForEdit, singleProfileDetailView, null,
                        adapter);

                editWork.show();

                break;

            case "Add Education":
                EditEducation editEducation = new EditEducation(this, "Education",
                        userProfileDetailGetForEdit, singleProfileDetailView, null,
                        adapter);

                editEducation.show();

                break;
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (WHERE_COME_FROM.equalsIgnoreCase("HOME_SCREEN")) {
                //  startActivity(new Intent(UpdateProfileActivity.this, ActivityHome.class));
            }
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    class TryUpdatingUserProfile implements View.OnClickListener {

        final RetrofitCallbacks<UserInfo> callback =
                new RetrofitCallbacks<UserInfo>(UpdateProfileActivity.this) {

                    @Override
                    public void onResponse(Call<UserInfo> call,
                                           Response<UserInfo> response) {
                        super.onResponse(call, response);
                        Utils.hideProgressDialog();
                        if (response.isSuccessful()) {
                            if(response.body().getResponseStatus() == 1) {
                                Toast.makeText(UpdateProfileActivity.this,
                                        "Details Updated Successfully", Toast.LENGTH_SHORT).show();

                                String userJSON = PreferenceManager
                                        .getDefaultSharedPreferences(UpdateProfileActivity.this)
                                        .getString(Constants.PREF_USER, "");
                                if (!TextUtils.isEmpty(userJSON)) {
                                    UserInfo.User user = new Gson().fromJson(userJSON, UserInfo.User.class);
                                    user.setFullName(userProfileDetailGetForEdit.getData().getFullName());
                                    Constants.USER_NAME = userProfileDetailGetForEdit.getData().getFullName();
                                    App.getInstance().setUser(user);
                                    App.getInstance().getUser()
                                            .setFullName(userProfileDetailGetForEdit.getData().getFullName());
                                }

                                if (WHERE_COME_FROM.equalsIgnoreCase("HOME_SCREEN")) {
                                    Update_profile_detail = true;
                                    Intent intent = new Intent(UpdateProfileActivity.this, ActivityHome.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    startActivity(intent);
                                    // startActivity(new Intent(UpdateProfileActivity.this, ActivityHome.class));

                                    finish();
                                } else {
                                    Update_profile_detail = true;
                                    Intent intent = new Intent(UpdateProfileActivity.this, ActivityUserProfile.class);
                                    intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                                    intent.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    //   startActivity(new Intent(UpdateProfileActivity.this,ActivityUserProfile.class));
                                    finish();
                                }

                            }
                            else{
                                Toast.makeText(UpdateProfileActivity.this,
                                          response.body().getErrorMessage().toString(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(UpdateProfileActivity.this,
                                    "Failed To Update Details", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {
                        super.onFailure(call, t);
                        Utils.hideProgressDialog();
                        Toast.makeText(UpdateProfileActivity.this, "Try again later", Toast.LENGTH_SHORT).show();
                    }
                };

        @Override
        public void onClick(View view) {


            //Toast.makeText(ActivityEditProfileDetails.this, "BUTTON CLICKED", Toast.LENGTH_SHORT).show();
            if(userProfileDetailGetForEdit.getData().getDob() == null){
                userProfileDetailGetForEdit.getData().setDob(tempDate);
            }
            if(userProfileDetailGetForEdit.getData().getDob().contains("T")){
                //userProfileDetailGetForEdit.getData().setDob(tempDate);
              //  tempDate = Utils.changeDateFormat(tempDate, "mm/dd/yyyy", "dd/mm/yyyy");
                userProfileDetailGetForEdit.getData().setDob(tempDate);
            }

            String dob = Utils.changeDateFormat(userProfileDetailGetForEdit.getData().getDob(),"dd/mm/yyyy","yyyy-mm-dd");
            userProfileDetailGetForEdit.getData().setDob(dob);
            Utils.showProgressDialog(UpdateProfileActivity.this, "Please Wait...",
                    "Updating profile details...",
                    false, false, null);
            API.getInstance().updateUserProfileDetail(userProfileDetailGetForEdit.getData(), callback);
        }
    }
}
