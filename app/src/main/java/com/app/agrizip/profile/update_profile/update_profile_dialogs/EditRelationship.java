package com.app.agrizip.profile.update_profile.update_profile_dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.app.agrizip.R;
import com.app.agrizip.home.adapter.MasterAdapter;

import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import extras.Constants;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/28/2016 at 4:35 PM.
 */

public class EditRelationship extends CustomDialog {

    @BindView(R.id.spin_dialogEditRelationship)
    Spinner spinDialogEditRelationship;

    String selectedRelationship = "";

    public EditRelationship(Context context, String id, final UserProfileDetailGetForEdit userProfileDetailGetForEdit, SingleDetailView singleDetailView, MasterAdapter adapter) {
        super(context, id, userProfileDetailGetForEdit, singleDetailView, adapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_relationship);

        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnDialogSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singleDetailView.tvSingleDetailValue.setText(selectedRelationship);
                dismiss();
            }
        });

        final ArrayAdapter<String> relationshipAdapter = Constants.getRelationshipAdapter(context);
        relationshipAdapter.setDropDownViewResource(R.layout.spinner_item_text);
        spinDialogEditRelationship.setAdapter(relationshipAdapter);

        if (userProfileDetailGetForEdit.getData().getRelationshipStatus() != null) {
            spinDialogEditRelationship.setSelection(relationshipAdapter.getPosition(
                    userProfileDetailGetForEdit.getData().getRelationshipStatus()
            ));
        }

        spinDialogEditRelationship.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedRelationship = adapterView.getSelectedItem().toString();
                userProfileDetailGetForEdit.getData()
                        .setRelationshipStatus(selectedRelationship);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onChangePrivacy(String id) {
        userProfileDetailGetForEdit.getData().setRelationshipVisibleTo(id);
    }
}
