package com.app.agrizip.profile.my_friend_list_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;

import apimodels.UserProfileDetailsInfo;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 12/15/2016 at 1:17 PM.
 */

public class SingleSmallFriendView extends LinearLayout
{

    final UserProfileDetailsInfo.DataBean.PhotoBean friendsBean;
    final Context context;
    @BindView(R.id.iv_friendSmallImage)
    ImageView ivFriendSmallImage;
    @BindView(R.id.tv_smallFriendName)
    TextView tvSmallFriendName;

    public SingleSmallFriendView(final Context context,
                                 final UserProfileDetailsInfo.DataBean.PhotoBean friendsBean)
    {
        super(context);

        this.friendsBean = friendsBean;

        this.context = context;

        this.setOrientation(HORIZONTAL);

        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        View v = LayoutInflater.from(context)
                .inflate(R.layout.single_small_friend_view, null, false);

        setPadding(0, 0, 8, 0);

        this.addView(v);

        ButterKnife.bind(this);

        Glide.with(context).load(this.friendsBean.getImagePath())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(ivFriendSmallImage);

        /*tvSmallFriendName.setText(this.friendsBean.getFullName());

        this.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(context, friendsBean.getFullName() + " was clicked!", Toast.LENGTH_SHORT).show();
            }
        });*/

    }
}
