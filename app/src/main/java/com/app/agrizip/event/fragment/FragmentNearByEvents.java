package com.app.agrizip.event.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.event.activity.ViewPostedEvent;
import com.app.agrizip.event.adapter.EventListAdapter;
import com.app.agrizip.post.post_simple.ItemTouchEvent;

import java.util.ArrayList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.EventList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/18/2017 at 5:12 PM.
 */

public class FragmentNearByEvents extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{

    @BindView(R.id.rv_NearByEvents)
    RecyclerView rvNearByEvents;
    @BindView(R.id.srl_refreshNearByEventList)
    SwipeRefreshLayout srlRefreshNearByEventList;
    EventListAdapter eventListAdapter;
    @BindView(R.id.tv_eventApiResponse)
    TextView tvEventApiResponse;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    EventList.DataBean eventLists;
    List<EventList.DataBean> event=new ArrayList<>();
    private Context context;

    public static FragmentNearByEvents newInstance(Context context)
    {
        FragmentNearByEvents fragmentNearByEvents = new FragmentNearByEvents();
        fragmentNearByEvents.context = context;
        return fragmentNearByEvents;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_near_by_events, container, false);

        ButterKnife.bind(this, view);

        eventListAdapter = new EventListAdapter(getActivity());

        rvNearByEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvNearByEvents.setHasFixedSize(true);
        rvNearByEvents.setAdapter(eventListAdapter);
        srlRefreshNearByEventList.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshNearByEventList.setOnRefreshListener(this);

        getNearByEvents();
      /*  rvNearByEvents.addOnItemTouchListener(new ItemTouchEvent(getContext(), rvNearByEvents, new ItemTouchEvent.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                eventLists=event.get(position);
                Intent viewSingleEvent=new Intent(getActivity(), ViewPostedEvent.class);
                viewSingleEvent.putExtra("UserImage", eventLists.getEventImage());
                viewSingleEvent.putExtra("EventName", eventLists.getEventName());
                viewSingleEvent.putExtra("Host", eventLists.getHost());
                viewSingleEvent.putExtra("Address", eventLists.getAddress());
                viewSingleEvent.putExtra("Contact", eventLists.getContactPerson());
                viewSingleEvent.putExtra("Time", eventLists.getTime());
                viewSingleEvent.putExtra("UserName",eventLists.getUserName());
                viewSingleEvent.putExtra("Date", eventLists.getDate());
                viewSingleEvent.putExtra("Desc", eventLists.getDescription());
                startActivity(viewSingleEvent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        return view;
    }

    private void getNearByEvents()
    {
        final RetrofitCallbacks<EventList> onGetEventListCallback =
                new RetrofitCallbacks<EventList>(getActivity())
                {
                    @Override
                    public void onResponse(Call<EventList> call, Response<EventList> response)
                    {
                        super.onResponse(call, response);
                        if (srlRefreshNearByEventList.isRefreshing())
                        {
                            srlRefreshNearByEventList.setRefreshing(false);
                        }
                        if (response.isSuccessful())
                        {
                            flLoading.setVisibility(View.GONE);
                            if (response.body().getData() != null)
                            {
                                if (response.body().getData().size() > 0)
                                {
                                    tvEventApiResponse.setVisibility(View.GONE);
                                    eventListAdapter.clear();
                                    for (EventList.DataBean singleEvent : response.body().getData())
                                    {
                                        eventListAdapter.addNewEvent(singleEvent);
                                        event.add(singleEvent);
                                    }
                                } else
                                {
                                    if (eventListAdapter.getItemCount() == 0)
                                    {
                                        tvEventApiResponse.setVisibility(View.VISIBLE);
                                        tvEventApiResponse.setText(response.body().getMessage());
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<EventList> call, Throwable t)
                    {
                        super.onFailure(call, t);
                        if (srlRefreshNearByEventList.isRefreshing())
                        {
                            srlRefreshNearByEventList.setRefreshing(false);
                        }
                    }
                };

        API.getInstance().getNearByEventList(23.021323, 72.554634, onGetEventListCallback);


    }

    @Override
    public void onRefresh()
    {
        getNearByEvents();
    }
}
