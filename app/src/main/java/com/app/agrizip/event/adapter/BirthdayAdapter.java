package com.app.agrizip.event.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.BirthdayList;
import apimodels.GetSinglePostDetails;
import apimodels.PostImageList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by S-PC on 15/03/2017.
 */

public class BirthdayAdapter extends RecyclerView.Adapter<BirthdayAdapter.VHBirthday> {

    List<BirthdayList.DataBean> birthdayLists;
    private final Context context;
    public BirthdayAdapter(final Context context) {
        this.context = context;
        birthdayLists = new LinkedList<>();
    }


    public class VHBirthday extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_birthday_image)
        PorterShapeImageView ivbirthday_image;
        @BindView(R.id.tv_birthday_username)
        TextView tvbirthday_username;
        @BindView(R.id.tv_birthday_date)
        TextView tvbirthday_date;
        @BindView(R.id.tv_birthday_age)
        TextView tvbirthday_age;
        @BindView(R.id.ib_birthday_wish)
        Button ibbirthday_wish;

        public VHBirthday(View view) {

            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void addNewEvent(BirthdayList.DataBean birthdayList)
    {
        birthdayLists.add(birthdayList);
        notifyItemInserted(birthdayLists.size());
    }
    @Override
    public VHBirthday onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewcard= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_event_birthday_row,parent,false);
        return new VHBirthday(viewcard);
    }

    @Override
    public void onBindViewHolder(final VHBirthday holder, int position) {
        final BirthdayList.DataBean birthdayModel=birthdayLists.get(position);
        Glide.with(context)
                .load(birthdayModel.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(holder.ivbirthday_image);


            holder.tvbirthday_username.setText(birthdayModel.getFullName());
        String tempDate;
        if(birthdayModel.getDob().contains("T"))
        {
            tempDate= Utils.changeDateFormat(birthdayModel.getDob(),"yyyy-MM-dd'T'HH:mm:ss","EEE, MMM yy");
        }else {
            tempDate=birthdayModel.getDob();
        }
            holder.tvbirthday_date.setText(tempDate);
         Calendar c = Calendar.getInstance();
         SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
         String formattedDate = df.format(c.getTime());
            holder.tvbirthday_age.setText(formattedDate);

        holder.ibbirthday_wish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context,"Post Id is :" + birthdayModel.getPostID() ,Toast.LENGTH_LONG).show();
                getBirthdayPostByPostId(birthdayModel.getPostID());

            }
        });

    }

    public void getBirthdayPostByPostId(String postId){
        final RetrofitCallbacks<GetSinglePostDetails> onGetPostDetailsCallback =
                new RetrofitCallbacks<GetSinglePostDetails>(context) {

                    @Override
                    public void onResponse(Call<GetSinglePostDetails> call, Response<GetSinglePostDetails> response) {
                        super.onResponse(call, response);


                        if (response.isSuccessful()) {

                           if(response.body().getResponseStatus() == 1){
                               ArrayList<PostImageList.DataBean> images = new ArrayList<>();
                               final PostImageList.DataBean d = new PostImageList.DataBean();
                               d.setImageId(response.body().getData().get(0).getImages().get(0).getStatusImageId());
                               d.setImagePath(response.body().getData().get(0).getImages().get(0).getImagestr());
                               d.setStatusId(response.body().getData().get(0).getImages().get(0).getStatusID());
                               images.add(d);
                               Intent i = new Intent(context, ActivityFullPhotoView.class);

                               i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, images);
                               i.putExtra("PostUserId",response.body().getData().get(0).getUserID());
                               i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, 0);
                               i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);
                              context.startActivity(i);
                           }

                           }




                        else {



                        }
                    }

                    @Override
                    public void onFailure(Call<GetSinglePostDetails> call, Throwable t) {
                        super.onFailure(call, t);

                    }
                };
        API.getInstance().getStatusPostById(postId, App.getInstance().getUser().getId(),onGetPostDetailsCallback);

    }


    public void clear()
    {
        birthdayLists.clear();
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return birthdayLists.size();
    }
}
