package com.app.agrizip.event.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;

import apimodels.EventList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Utils;

public class


ViewPostedEvent extends AppCompatActivity {
    EventList.DataBean SingleEvent;
    @BindView(R.id.iv_singleEventCoverPic)
    ImageView ivSingleEventCoverPic;
    @BindView(R.id.tv_singleEventName)
    TextView tvSingleEventName;
    @BindView(R.id.tv_singleEventHost)
    TextView tvSingleEventHost;
    @BindView(R.id.tv_singleEventAddress)
    TextView tvSingleEventAddress;
    @BindView(R.id.tv_singleEventContactPerson)
    TextView tvSingleEventContactPerson;
    @BindView(R.id.tv_singleEventTime)
    TextView tvSingleEventTime;
    @BindView(R.id.tv_singleEventDay)
    TextView tvSingleEventDay;
    @BindView(R.id.tv_singleEventMonth)
    TextView tvSingleEventMonth;
    @BindView(R.id.tv_singleEventYear)
    TextView tvSingleEventYear;
    @BindView(R.id.tv_event_description)
    TextView tvEvent_description;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;
    String getDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_posted_event);
        ButterKnife.bind(this);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);

            if (tvToolbarTitle != null) {
                tvToolbarTitle.setText("View Event");
            }

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        getDate = this.getIntent().getStringExtra("Date");
        String[] date = new String[3];

        date[0] = "N/A";
        date[1] = "N/A";
        date[2] = "N/A";
//        String tempdate = Utils.convertMillsToDate(Long.valueOf(singleEvent.getDate()));

        if (getDate.contains("-")) {
            date = getDate.split("-");
        } else if (getDate.contains("/")) {
            date = getDate.split("/");
        }

        if (date[2].contains("T")) {
            date[2] = date[2].substring(0, date[2].split("T").length);
        }

        try {
            String month = Utils.month[Integer.parseInt(date[1]) - 1];
            tvSingleEventMonth.setText(month);
        } catch (ArrayIndexOutOfBoundsException e) {
            if (date[1].contains("T")) {
                date[1] = date[1].substring(0, date[1].split("T").length);
            }
            tvSingleEventDay.setText(date[1]);
            try {
                String month = Utils.month[Integer.parseInt(date[1]) - 1];
                tvSingleEventMonth.setText(month);
            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.printStackTrace();
                Log.i("DateError", "ERROR HANDLING MONTH");
            }
        }
        tvSingleEventDay.setText(date[2]);
        Log.e("date", "" + date);
        try {
            String month = Utils.month[Integer.parseInt(date[1]) - 1];
            tvSingleEventMonth.setText(month);
        } catch (ArrayIndexOutOfBoundsException e) {
            tvSingleEventDay.setText(date[1]);
            try {
                String month = Utils.month[Integer.parseInt(date[1]) - 1];
                tvSingleEventMonth.setText(month);
            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.printStackTrace();
                Log.i("DateError", "ERROR HANDLING MONTH");
            }
        }
        tvSingleEventYear.setText(date[0]);


        try {
            String EventImage = getIntent().getStringExtra("UserImage");
            //   SingleEvent= (EventList.DataBean) this.getIntent().getParcelableArrayListExtra("SingleEvent");
            //  ivSingleEventCoverPic.setImageResource(this.getIntent().getStringExtra("UserImage")));
            Glide.with(getApplicationContext())
                    .load(EventImage)
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    .crossFade()
                    .into(ivSingleEventCoverPic);
            tvSingleEventName.setText(this.getIntent().getStringExtra("EventName"));
            tvSingleEventHost.setText(this.getIntent().getStringExtra("Host"));
            tvSingleEventAddress.setText(Html.fromHtml("<font color='#000000'> <b>Address</b></font>: ") + this.getIntent().getStringExtra("Address"));
            tvSingleEventContactPerson.setText(this.getIntent().getStringExtra("UserName") + " - +91" + this.getIntent().getStringExtra("Contact"));
            tvSingleEventTime.setText(this.getIntent().getStringExtra("Time"));
            tvEvent_description.setText(this.getIntent().getStringExtra("Desc"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
