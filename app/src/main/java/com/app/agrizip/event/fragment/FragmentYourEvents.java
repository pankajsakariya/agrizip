package com.app.agrizip.event.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.event.activity.ViewPostedEvent;
import com.app.agrizip.event.adapter.EventListAdapter;
import com.app.agrizip.post.post_simple.ItemTouchEvent;

import java.util.ArrayList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.EventList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/18/2017 at 4:52 PM.
 */

public class FragmentYourEvents extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_events)
    RecyclerView rvEvents;
    @BindView(R.id.srl_refreshEventList)
    SwipeRefreshLayout srlRefreshEventList;
    EventListAdapter eventListAdapter;
    @BindView(R.id.tv_eventApiResponse)
    TextView tvEventApiResponse;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    private Context context;
    private FragmentYourEvents.OnPostEventSuccessfully onPostEventSuccessfully;
    final Handler mHandler = new Handler();
    String newStatusPostId;
     EventList.DataBean eventLists;
    List<EventList.DataBean> event=new ArrayList<>();
    public static FragmentYourEvents newInstance(Context context) {
        FragmentYourEvents fragmentYourEvents = new FragmentYourEvents();
        fragmentYourEvents.context = context;
        return fragmentYourEvents;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_your_events, container, false);
        ButterKnife.bind(this, view);
        getEvents();
        eventListAdapter = new EventListAdapter(getActivity());

        rvEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvEvents.setHasFixedSize(true);
        rvEvents.setAdapter(eventListAdapter);
        srlRefreshEventList.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshEventList.setOnRefreshListener(this);

        onPostEventSuccessfully = new OnPostEventSuccessfully();

           getActivity().registerReceiver(onPostEventSuccessfully,
            new IntentFilter(Constants.ON_POST_PHOTO_UPLOADED_SUCCESSFULLY));

       /* rvEvents.addOnItemTouchListener(new ItemTouchEvent(getContext(), rvEvents, new ItemTouchEvent.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                    eventLists=event.get(position);
                Intent viewSingleEvent=new Intent(getActivity(), ViewPostedEvent.class);
                viewSingleEvent.putExtra("UserImage", eventLists.getEventImage());
                viewSingleEvent.putExtra("EventName", eventLists.getEventName());
                viewSingleEvent.putExtra("Host", eventLists.getHost());
                viewSingleEvent.putExtra("Address", eventLists.getAddress());
                viewSingleEvent.putExtra("UserName",eventLists.getUserName());
                viewSingleEvent.putExtra("Contact", eventLists.getContactPerson());
                viewSingleEvent.putExtra("Time", eventLists.getTime());
                viewSingleEvent.putExtra("Date", eventLists.getDate());
                viewSingleEvent.putExtra("Desc", eventLists.getDescription());
                startActivity(viewSingleEvent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

*/
        return view;
    }

    private void getEvents() {
        final String userId = App.getInstance().getUser().getId();

        final RetrofitCallbacks<EventList> onGetEventListCallback =
                new RetrofitCallbacks<EventList>(getActivity()) {
                    @Override
                    public void onResponse(Call<EventList> call, Response<EventList> response) {
                        super.onResponse(call, response);
                        if (srlRefreshEventList.isRefreshing()) {
                            srlRefreshEventList.setRefreshing(false);
                        }
                        if (response.isSuccessful()) {
                            flLoading.setVisibility(View.GONE);
                            if (response.body().getData() != null) {
                                if (response.body().getData().size() > 0) {
                                    tvEventApiResponse.setVisibility(View.GONE);
                                    eventListAdapter.clear();
                                    for (EventList.DataBean singleEvent : response.body().getData()) {

                                        eventListAdapter.addNewEvent(singleEvent);
                                        event.add(singleEvent);
                                    }
                                    eventListAdapter.notifyDataSetChanged();

                                } else {
                                    if (eventListAdapter.getItemCount() == 0) {
                                        tvEventApiResponse.setVisibility(View.VISIBLE);
                                        tvEventApiResponse.setText(response.body().getMessage());
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<EventList> call, Throwable t) {
                        super.onFailure(call, t);
                        if (srlRefreshEventList.isRefreshing()) {
                            srlRefreshEventList.setRefreshing(false);
                        }
                    }
                };

        API.getInstance().getEventList(userId, 1, onGetEventListCallback);

    }

    public void unregisterReceiver() {
        if (onPostEventSuccessfully != null) {
            getActivity().unregisterReceiver(onPostEventSuccessfully);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getEvents();
    }

    @Override
    public void onRefresh() {
        getEvents();
    }

    public class OnPostEventSuccessfully extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String postId = intent.getStringExtra(Constants.POST_ID);
            if (postId != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        newStatusPostId = postId;
                        getEvents();
                    }
                });
            }

        }
    }
}
