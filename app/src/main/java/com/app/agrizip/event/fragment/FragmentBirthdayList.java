package com.app.agrizip.event.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.event.adapter.BirthdayAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.BirthdayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/18/2017 at 5:20 PM.
 */

public class FragmentBirthdayList extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    @BindView(R.id.rv_BirthDayEvents)
    RecyclerView rv_BirthDayEvents;
    @BindView(R.id.srl_refreshBirthdayList)
    SwipeRefreshLayout srl_refreshBirthdayList;
    BirthdayAdapter BirthdayListAdapter;
    @BindView(R.id.tv_eventApiResponse1)
    TextView tvEventApiResponse1;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    private Context context;

    public static FragmentBirthdayList newInstance(Context context)
    {
        FragmentBirthdayList fragmentBirthdayList = new FragmentBirthdayList();
        fragmentBirthdayList.context = context;
        return fragmentBirthdayList;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_birthday_list, container, false);

        ButterKnife.bind(this, view);
        BirthdayListAdapter = new BirthdayAdapter(getActivity());
        rv_BirthDayEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_BirthDayEvents.setHasFixedSize(true);
        rv_BirthDayEvents.setAdapter(BirthdayListAdapter);
        srl_refreshBirthdayList.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srl_refreshBirthdayList.setOnRefreshListener(this);
       getBirthdayList();

        return view;
    }



    private void getBirthdayList()
    {
        final String userId = App.getInstance().getUser().getId();
        final RetrofitCallbacks<BirthdayList> onGetBirthdayList =
                new RetrofitCallbacks<BirthdayList>(getActivity())
                {

                    @Override
                    public void onResponse(Call<BirthdayList> call, Response<BirthdayList> response)
                    {
                        super.onResponse(call, response);
                        if (srl_refreshBirthdayList.isRefreshing())
                        {
                            srl_refreshBirthdayList.setRefreshing(false);
                        }
                        if (response.isSuccessful())
                        {
                            flLoading.setVisibility(View.GONE);
                            if (response.body().getData() != null) {
                                if (response.body().getData().size() > 0) {
                                    tvEventApiResponse1.setVisibility(View.GONE);
                                    BirthdayListAdapter.clear();
                                    for (BirthdayList.DataBean singleBirthday : response.body().getData()) {

                                        BirthdayListAdapter.addNewEvent(singleBirthday);

                                    }
                                } else {
                                    if (BirthdayListAdapter.getItemCount() == 0) {
                                        tvEventApiResponse1.setVisibility(View.VISIBLE);
                                        tvEventApiResponse1.setText(response.body().getMessage());
                                    }
                                }
                            }


                        }
                    }
                    @Override
                    public void onFailure(Call<BirthdayList> call, Throwable t) {
                        super.onFailure(call, t);
                        if (srl_refreshBirthdayList.isRefreshing()) {
                            srl_refreshBirthdayList.setRefreshing(false);
                        }
                    }

                };

        API.getInstance().getBirthdayList(userId, onGetBirthdayList);
    }

    @Override
    public void onRefresh() {
       // getBirthdayList();
    }
}
