package com.app.agrizip.event.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.event.fragment.FragmentYourEvents;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CreateEvent;
import apimodels.EventList;
import apimodels.EventResponse;
import apimodels.PostStatusResponse;
import apimodels.SinglePost;
import apimodels.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityCreateEvent extends AppCompatActivity implements Validator.ValidationListener,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final int SELECT_PHOTO = 1424;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle2)
    TextView tvToolbarTitle;

    @BindView(R.id.iv_eventCoverPic)
    ImageView ivEventCoverPic;

    @BindView(R.id.fab_selectEventCoverPic)
    FloatingActionButton fabSelectEventCoverPic;

    @NotEmpty
    @BindView(R.id.et_eventAddress)
    EditText etEventAddress;

    @NotEmpty
    @BindView(R.id.et_eventName)
    EditText etEventName;

    @NotEmpty
    @BindView(R.id.et_eventHost)
    EditText etEventHost;

    @NotEmpty
    @BindView(R.id.et_eventContactPerson)
    EditText etEventContactPerson;

    @NotEmpty
    @BindView(R.id.et_eventLocation)
    EditText etEventLocation;

    @NotEmpty
    @BindView(R.id.et_eventDate)
    EditText etEventDate;

    @NotEmpty
    @BindView(R.id.et_eventTime)
    EditText etEventTime;

    @BindView(R.id.et_eventDescription)
    EditText etEventDescription;

    Validator validator;

    Call<EventResponse> postingEvent;

    DatePickerDialog dpd;

    TimePickerDialog tpd;

    String coverPicBase64String = "";

    String selectedLocation;

    double selectedLocationLat, selectedLocationLng;
    private FragmentYourEvents fragmentYourEvents;
    SinglePost post = new SinglePost();
    Uri imageUri;
    UserInfo.User user;
    boolean postingStatus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        ButterKnife.bind(this);
        user = ((App) getApplication()).getUser();
        validator = new Validator(this);
        validator.setValidationListener(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(R.string.create_event);
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        etEventDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean inFocus) {
                if (inFocus) {
                    Calendar today = Calendar.getInstance();
                    dpd = DatePickerDialog.newInstance(ActivityCreateEvent.this, today.get(Calendar.YEAR),
                            today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH));
                    dpd.setMinDate(today);
                    dpd.show(getFragmentManager(), "DATE");
                }
            }
        });

        etEventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar today = Calendar.getInstance();
                DatePickerDialog.newInstance(ActivityCreateEvent.this, today.get(Calendar.YEAR),
                        today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH));
                dpd.setMinDate(today);
                dpd.show(getFragmentManager(), "DATE");
            }
        });


        etEventTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean inFocus) {
                if (inFocus) {
                    Calendar today = Calendar.getInstance();
                    tpd = TimePickerDialog.newInstance(ActivityCreateEvent.this, today.get(Calendar.HOUR),
                            today.get(Calendar.MINUTE), false);
                    tpd.setMinTime(today.get(Calendar.HOUR), today.get(Calendar.MINUTE),
                            today.get(Calendar.SECOND));
                    tpd.show(getFragmentManager(), "TIME");
                }
            }
        });

        etEventTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar today = Calendar.getInstance();
                TimePickerDialog.newInstance(ActivityCreateEvent.this, today.get(Calendar.HOUR),
                        today.get(Calendar.MINUTE), false);
                tpd.setMinTime(today.get(Calendar.HOUR), today.get(Calendar.MINUTE),
                        today.get(Calendar.SECOND));
                tpd.show(getFragmentManager(), "TIME");
            }
        });

        etEventLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivityForResult(new Intent(ActivityCreateEvent.this, ActivityCheckin.class),
//                        Constants.CHECKIN_ACTIVITY);
                CheckInPlace();
            }
        });

        etEventLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean inFocus) {
                if (inFocus) {
//                    startActivityForResult(new Intent(ActivityCreateEvent.this, ActivityCheckin.class),
//                            Constants.CHECKIN_ACTIVITY);
                    CheckInPlace();
                }
            }
        });


    }

    public void CheckInPlace() {
        try {
            Intent intent =
                    new PlaceAutocomplete
                            .IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(ActivityCreateEvent.this);
            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_create_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_post) {
            validator.validate();
        }
        return super.onOptionsItemSelected(item);
    }

    private void promptUserForPostingEvent() {
        new AlertDialog.Builder(this)
                .setTitle("Create Event")
                .setMessage("Are you sure you want to create this event?")
                .setPositiveButton("CREATE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Utils.showProgressDialog(ActivityCreateEvent.this,
                                "Please Wait...", "Creating new Event", true, false, new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialogInterface) {
                                        if (postingEvent != null) {
                                            postingEvent.cancel();
                                            Utils.hideProgressDialog();
                                        }
                                    }
                                });
                        tryPostingNewEvent();
                    }
                }).setNegativeButton("CANCEL", null)
                .show();
    }

    private void tryPostingNewEvent() {

        String temp_eventdate = Utils.changeDateFormat(etEventDate.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd");
        SinglePost.EventsDetailsBean eventsDetailsBean = new SinglePost.EventsDetailsBean();
        post.setUserID(App.getInstance().getUser().getId());
        post.setUserName(App.getInstance().getUser().getFullName());
        post.setIsEventPost(true);
        post.setSharedOwner("");
        post.setStatus("");
        post.setUserImage(App.getInstance().getUser().getUserImage());
        eventsDetailsBean.setEventName(etEventName.getText().toString());
        eventsDetailsBean.setHost(etEventHost.getText().toString());
        eventsDetailsBean.setContactPerson(etEventContactPerson.getText().toString());
        eventsDetailsBean.setDate(temp_eventdate);
        eventsDetailsBean.setTime(etEventTime.getText().toString());

        eventsDetailsBean.setAddress(etEventAddress.getText().toString());
        eventsDetailsBean.setDescription(etEventDescription.getText().toString());
        eventsDetailsBean.setLocation(etEventLocation.getText().toString());
        //eventsDetailsBean.setLocation(etEventLocation.getText().toString().trim());
        if (coverPicBase64String.length() > 0) {
            eventsDetailsBean.setEventImage(imageUri.toString());
        } else {
            eventsDetailsBean.setEventImage("");
        }
        eventsDetailsBean.setUserId(App.getInstance().getUser().getId());

        post.setEventsDetails(eventsDetailsBean);

        final CreateEvent newEvent = new CreateEvent();
        newEvent.setEventName(etEventName.getText().toString());
        newEvent.setHost(etEventHost.getText().toString());
        newEvent.setContactPerson(etEventContactPerson.getText().toString());

        Log.e("dateformate===", "" + temp_eventdate);
        newEvent.setDate(temp_eventdate);
        newEvent.setTime(etEventTime.getText().toString());
        //newEvent.setLocation(selectedLocation);
        newEvent.setLocation(etEventLocation.getText().toString().trim());

        newEvent.setLatitude(selectedLocationLat);
        newEvent.setLatitude(selectedLocationLng);
        newEvent.setAddress(etEventAddress.getText().toString());
        newEvent.setDescription(etEventDescription.getText().toString());
        newEvent.setEventImage(coverPicBase64String);
        newEvent.setUserId(App.getInstance().getUser().getId());


        final RetrofitCallbacks<EventResponse> onCreateEventCallback =
                new RetrofitCallbacks<EventResponse>(this) {
                    @Override
                    public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                        super.onResponse(call, response);
                        Utils.hideProgressDialog();

                        if (response.isSuccessful()) {
                          //  post.setStatusId(response.body().getData().getE());
                           // post.setStatusId(response.body().getData().get(0).getEventId());
                            post.setStatusId(response.body().getData().getEventId());
                            user.getUserEventCallbacks().onStatusPostSuccessful(post);
                            Toast.makeText(ActivityCreateEvent.this, "Event Created Successfully!", Toast.LENGTH_SHORT).show();

                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<EventResponse> call, Throwable t) {
                        super.onFailure(call, t);
                        Utils.hideProgressDialog();
                    }
                };

        postingEvent = API.getInstance().createNewEvent(newEvent, onCreateEventCallback);

    }

    @Override
    protected void onDestroy() {
        if (fragmentYourEvents != null) {
            fragmentYourEvents.unregisterReceiver();
        }
        super.onDestroy();
    }

    @Override
    public void onValidationSucceeded() {
        promptUserForPostingEvent();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
        etEventDate.setText(date);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        if (view.getIsCurrentlyAmOrPm() == TimePickerDialog.AM) {
            etEventTime.setText(hourOfDay + ":" + minute + " AM");
        } else if (view.getIsCurrentlyAmOrPm() == TimePickerDialog.PM) {
            etEventTime.setText(hourOfDay + ":" + minute + " PM");
        }
    }


    @OnClick(R.id.fab_selectEventCoverPic)
    public void pickEventCoverPic() {
        fabSelectEventCoverPic.setEnabled(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fabSelectEventCoverPic.setEnabled(true);
            }
        }, 2500);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    try {
                        imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        coverPicBase64String = Utils.getEncoded64ImageStringFromBitmap(selectedImage);
                        Glide.with(this)
                                .load(imageUri)
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(ivEventCoverPic);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case 1:

                if (requestCode == 1) {
                    if (resultCode == RESULT_OK) {
                        // retrive the data by using getPlace() method.
                        Place place = PlaceAutocomplete.getPlace(this, data);
                        Log.e("Tag", "Place: " + place.getAddress() + place.getPhoneNumber());

                        etEventLocation.setText(place.getName());

                    } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                        Status status = PlaceAutocomplete.getStatus(this, data);
                        // TODO: Handle the error.
                        Log.e("Tag", status.getStatusMessage());

                    } else if (resultCode == RESULT_CANCELED) {
                        // The user canceled the operation.
                    }
                }
                /*if (data != null) {
                    final String place = data.getStringExtra(Constants.CHECKIN_PLACE);
                    final double lat = data.getDoubleExtra(Constants.CHECKIN_LAT, 0);
                    final double lng = data.getDoubleExtra(Constants.CHECKIN_LNG, 0);
                    if (place != null) {
                        selectedLocation = place;
                        etEventLocation.setText(selectedLocation);
                        selectedLocationLat = lat;
                        selectedLocationLng = lng;
                    }
                }*/
                break;
        }

    }


}
