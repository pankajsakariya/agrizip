package com.app.agrizip.event.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import apimodels.EventList;
import viewholders.VHSingelEventItem;

/**
 * Created by rutvik on 1/18/2017 at 1:07 PM.
 */

public class EventListAdapter extends RecyclerView.Adapter
{
    private final Context context;

    private final List<EventList.DataBean> eventList;


    public EventListAdapter(final Context context)
    {
        this.context = context;
        eventList = new LinkedList<>();
    }

    public void addNewEvent(EventList.DataBean singleEventItem)
    {
        eventList.add(singleEventItem);
        notifyItemInserted(eventList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingelEventItem.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSingelEventItem.bind((VHSingelEventItem) holder, (EventList.DataBean) eventList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return eventList.size();
    }

    public void clear()
    {
        eventList.clear();
        notifyDataSetChanged();
    }

}
