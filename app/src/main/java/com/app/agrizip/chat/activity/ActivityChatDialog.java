package com.app.agrizip.chat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.app.agrizip.app.App;
import com.app.agrizip.chat.adapter.ChatAdapter;
import com.google.gson.Gson;
import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.chat.QBChat;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.chat.utils.DialogUtils;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;
import com.quickblox.messages.model.QBPushType;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


import apimodels.NotificationGetPostModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.Utils;
import fcm.NotificationHandler;
import quickblox.AttachmentPreviewAdapter;
import quickblox.AttachmentPreviewAdapterView;
import quickblox.ChatHelper;
import quickblox.ErrorUtils;
import quickblox.ImagePickHelper;
import quickblox.OnImagePickedListener;
import quickblox.PaginationHistoryListener;
import quickblox.QbChatDialogMessageListenerImp;
import quickblox.QbDialogHolder;
import quickblox.QbDialogUtils;
import quickblox.Toaster;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class ActivityChatDialog extends ActivityChatDialogBase implements OnImagePickedListener {
    public static final String EXTRA_DIALOG_ID = "dialogId";
    public static final String EXTRA_QBUSERID_ID = "qbUserId";
    public static final String EXTRA_ISFROMNOTIFICATION = "isFromNotification";
    private static final String TAG = App.APP_TAG + ActivityChatDialog.class.getSimpleName();
    private static final int REQUEST_CODE_ATTACHMENT = 721;
    private static final int REQUEST_CODE_SELECT_PEOPLE = 752;
    private static final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";
    private final ConnectionListener chatConnectionListener = new ConnectionListener() {
        @Override
        public void connected(XMPPConnection xmppConnection) {

        }

        @Override
        public void authenticated(XMPPConnection xmppConnection, boolean b) {

        }

        @Override
        public void connectionClosed() {

        }

        @Override
        public void connectionClosedOnError(Exception e) {

        }

        @Override
        public void reconnectionSuccessful() {

        }

        @Override
        public void reconnectingIn(int i) {

        }

        @Override
        public void reconnectionFailed(Exception e) {

        }
    };


    ArrayList<QBUser> qbUserArrayList = new ArrayList<>();
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.iv_sendChatMessage)
    ImageView ivSendChatMessage;
    @BindView(R.id.et_newChatMessageBody)
    EditText etNewChatMessageBody;
    @BindView(R.id.ll_newChatMessageContainer)
    LinearLayout llNewChatMessageContainer;
    @BindView(R.id.list_chat_messages)
    StickyListHeadersListView messagesListView;
    @BindView(R.id.divider_chat_attachments)
    View dividerChatAttachments;
    @BindView(R.id.layout_attachment_preview_container)
    LinearLayout layoutAttachmentPreviewContainer;
    @BindView(R.id.activity_chat_dialog)
    RelativeLayout activityChatDialog;
    @BindView(R.id.adapter_view_attachment_preview)
    AttachmentPreviewAdapterView adapterViewAttachmentPreview;
    private ChatAdapter chatAdapter;
    private QBChatDialog qbChatDialog;
    private ArrayList<QBChatMessage> unShownMessages;
    private int skipPagination = 0;
    private ChatMessageListener chatMessageListener;
    private AttachmentPreviewAdapter attachmentPreviewAdapter;
    public static final String PROPERTY_OCCUPANTS_IDS = "occupants_ids";
    public static final String PROPERTY_DIALOG_TYPE = "dialog_type";
    public static final String PROPERTY_DIALOG_NAME = "dialog_name";
    public static final String PROPERTY_NOTIFICATION_TYPE = "notification_type";
    private QBSystemMessagesManager systemMessagesManager;
    public static int chatRecivedUserId = 0;
    String qbUserName="";
    public static void startForResult(Activity activity, int code, QBChatDialog dialogId,String qbUserName) {
        Intent intent = new Intent(activity, ActivityChatDialog.class);
        intent.putExtra(ActivityChatDialog.EXTRA_DIALOG_ID, dialogId);
        intent.putExtra("UserName",qbUserName);
        intent.putExtra(ActivityChatDialog.EXTRA_ISFROMNOTIFICATION, 0);

        activity.startActivityForResult(intent, code);
    }

    public static Intent startForNotificationResult(Context context, int qbUserId) {
        Intent intent = new Intent(context, ActivityChatDialog.class);
        intent.putExtra(ActivityChatDialog.EXTRA_QBUSERID_ID, qbUserId);
        intent.putExtra(ActivityChatDialog.EXTRA_ISFROMNOTIFICATION, 1);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_dialog);

        ButterKnife.bind(this);

        Log.v(TAG, "onCreate ChatActivity on Thread ID = " + Thread.currentThread().getId());

        if((int) getIntent().getSerializableExtra(EXTRA_ISFROMNOTIFICATION) == 0)
        {
            qbChatDialog = (QBChatDialog) getIntent().getSerializableExtra(EXTRA_DIALOG_ID);
             qbUserName =(String)getIntent().getSerializableExtra("UserName");
            initView(qbUserName);
        }
        else
        {
            int  qbUserId = (int) getIntent().getSerializableExtra(EXTRA_QBUSERID_ID);
             qbUserName = getIntent().getStringExtra("UserName");
            Log.v(TAG, "qbUserId = " + qbUserId);
            notificationChatInit(qbUserId,qbUserName);
        }


    }

    private  void initView(String qbUserName)
    {

        chatRecivedUserId = qbChatDialog.getOccupants().get(0);
        Log.v(TAG, "deserialized dialog = " + qbChatDialog);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(qbUserName);
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        qbChatDialog.initForChat(QBChatService.getInstance());
        chatMessageListener = new ChatMessageListener();
        qbChatDialog.addMessageListener(chatMessageListener);
        attachmentPreviewAdapter = new AttachmentPreviewAdapter(this,
                new AttachmentPreviewAdapter.OnAttachmentCountChangedListener() {
                    @Override
                    public void onAttachmentCountChanged(int count) {
                        layoutAttachmentPreviewContainer.setVisibility(count == 0 ? View.GONE : View.VISIBLE);
                    }
                },
                new AttachmentPreviewAdapter.OnAttachmentUploadErrorListener() {
                    @Override
                    public void onAttachmentUploadError(QBResponseException e) {
                        showErrorSnackbar(0, e, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onAttachmentsClick(v);
                            }
                        });
                    }
                });

        adapterViewAttachmentPreview.setAdapter(attachmentPreviewAdapter);
        Utils.showProgressDialog(this, "Please Wait...", "Loading Chat...", true, false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                ActivityChatDialog.this.finish();
            }
        });

        if(ActivityFullPhotoView.getAbsulutePath != null) {
            if (ActivityFullPhotoView.getAbsulutePath.length() > 0) {
                attachmentPreviewAdapter.add(ActivityFullPhotoView.getAbsulutePath);
                ActivityFullPhotoView.getAbsulutePath = null;
            }
        }
    }


    protected Snackbar showErrorSnackbar(@StringRes int resId, Exception e,
                                         View.OnClickListener clickListener) {
        return ErrorUtils.showSnackbar(activityChatDialog, resId, e,
                R.string.dlg_retry, clickListener);
    }

    public void onAttachmentsClick(View view) {
        new ImagePickHelper().pickAnImage(this, REQUEST_CODE_ATTACHMENT);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        if (qbChatDialog != null) {
            outState.putString(EXTRA_DIALOG_ID, qbChatDialog.getDialogId());
        }
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (qbChatDialog == null) {
            qbChatDialog = QbDialogHolder.getInstance().getChatDialogById(savedInstanceState.getString(EXTRA_DIALOG_ID));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ChatHelper.getInstance().addConnectionListener(chatConnectionListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ChatHelper.getInstance().removeConnectionListener(chatConnectionListener);
    }

    @Override
    public void onBackPressed() {
        releaseChat();
        sendDialogId();

        super.onBackPressed();
    }

    @Override
    public void onSessionCreated(boolean success) {
        if (success) {
            initChat();
            Log.i(TAG, "onSessionCreated: SUCCESS");
        } else {
            Log.i(TAG, "onSessionCreated: FAILED");
        }
    }

    private void releaseChat() {
        qbChatDialog.removeMessageListrener(chatMessageListener);
        if (!QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
            //leaveGroupDialog();
        }
    }

    private void sendDialogId() {
        Intent result = new Intent();
        result.putExtra(EXTRA_DIALOG_ID, qbChatDialog.getDialogId());
        setResult(RESULT_OK, result);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatRecivedUserId = 0;
    }

    private void sendChatMessage(String text, QBAttachment attachment) {
        String strNotificationMessage = text;
        QBChatMessage chatMessage = new QBChatMessage();
        if (attachment != null) {
            chatMessage.addAttachment(attachment);
        } else {
            chatMessage.setBody(text);
        }
        chatMessage.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");
        chatMessage.setDateSent(System.currentTimeMillis() / 1000);
        chatMessage.setMarkable(true);


        if (!QBDialogType.PRIVATE.equals(qbChatDialog.getType()) && !qbChatDialog.isJoined()){
            Toaster.shortToast("You're still joining a group chat, please wait a bit");
            return;
        }

        try {
            qbChatDialog.sendMessage(chatMessage);

            if (QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
                showMessage(chatMessage);
            }

            if (attachment != null) {
                attachmentPreviewAdapter.remove(attachment);
            } else {

                etNewChatMessageBody.setText("");
            }
        } catch (SmackException.NotConnectedException e) {
            Log.w(TAG, e);
            Toaster.shortToast("Can't send a message, You are not connected to chat");
        }

        if(qbChatDialog != null)
        {

            if (attachment != null) {
                strNotificationMessage = (getResources().getString(R.string.chat_attachment_message,qbChatDialog.getName()));
                sendNotifaction(qbChatDialog.getOccupants().get(0),qbChatDialog.getOccupants().get(1),strNotificationMessage);
            } else {
                sendNotifaction(qbChatDialog.getOccupants().get(0),qbChatDialog.getOccupants().get(1),text);
            }
        }

    }

    void sendNotifaction(int SenderId, int ReceiverId,String message)
    {
        // recipients
        StringifyArrayList<Integer> userIds = new StringifyArrayList<Integer>();
       // userIds.add(QBSessionManager.getInstance().getSessionParameters().getUserId());
        userIds.add(ReceiverId);

        QBEvent event = new QBEvent();
        event.setUserIds(userIds);
        event.setEnvironment(QBEnvironment.PRODUCTION);
        event.setNotificationType(QBNotificationType.PUSH);

        NotificationGetPostModel objobjNotificationGetPostModel = new NotificationGetPostModel();
        objobjNotificationGetPostModel.setUser_id(SenderId);
        objobjNotificationGetPostModel.setMessage(message);
        objobjNotificationGetPostModel.setNotificationType(Constants.NotificationType.ChatNewMessage);
        objobjNotificationGetPostModel.setUserContactNumber(App.getInstance().getUser().getMobileNumber());
        String strNotificationSent = new Gson().toJson(objobjNotificationGetPostModel);
        Log.i(TAG, strNotificationSent);
        event.setMessage(strNotificationSent);


        QBPushNotifications.createEvent(event).performAsync(new QBEntityCallback<QBEvent>() {

            @Override
            public void onSuccess(QBEvent qbEvent, Bundle bundle) {
                Log.w(TAG, String.valueOf(qbEvent));
            }
            @Override
            public void onError(QBResponseException e) {
                Log.w(TAG, String.valueOf(e));
            }

        });
    }


    private void initChat() {
        switch (qbChatDialog.getType()) {
            case GROUP:
                break;
            case PUBLIC_GROUP:
                //joinGroupChat();
                break;

            case PRIVATE:
                Log.i(TAG, "initChat: PRIVATE CHAT");
                loadDialogUsers();
                break;

            default:
                //Toaster.shortToast(String.format("%s %s", getString(R.string.chat_unsupported_type), qbChatDialog.getType().name()));
                finish();
                break;
        }
    }


    private void updateDialog(final ArrayList<QBUser> selectedUsers) {
        ChatHelper.getInstance().updateDialogUsers(qbChatDialog, selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        qbChatDialog = dialog;
                        loadDialogUsers();
                    }

                    @Override
                    public void onError(QBResponseException e) {
                    }
                }
        );
    }

    private void loadDialogUsers() {
        ChatHelper.getInstance().getUsersFromDialog(qbChatDialog, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> users, Bundle bundle) {
                Log.i(TAG, "loadDialogUsers SUCCESSFULLY!!!!!!!");
                //loadUsersFromQb(getIntent().getStringExtra(Constants.MOBILE_NO));
                //qbUserArrayList.add(users);
                //updateDialog(qbUserArrayList);
                loadChatHistory();
            }

            @Override
            public void onError(QBResponseException e) {
                e.printStackTrace();
            }
        });
    }

    @OnClick(R.id.iv_sendChatMessage)
    public void sendMessage() {
       // sendChatMessage(etNewChatMessageBody.getText().toString(), null);
        int totalAttachmentsCount = attachmentPreviewAdapter.getCount();
        Collection<QBAttachment> uploadedAttachments = attachmentPreviewAdapter.getUploadedAttachments();
        if (!uploadedAttachments.isEmpty()) {
            if (uploadedAttachments.size() == totalAttachmentsCount) {
                for (QBAttachment attachment : uploadedAttachments) {
                    sendChatMessage(null, attachment);
                }
            } else {
                Toaster.shortToast(R.string.chat_wait_for_attachments_to_upload);
            }
        }
        String text = etNewChatMessageBody.getText().toString().trim();
        if (!TextUtils.isEmpty(text)) {
            sendChatMessage(text, null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showMessage(QBChatMessage message) {
        if (chatAdapter != null) {
            chatAdapter.add(message);
            scrollMessageListDown();
        } else {
            if (unShownMessages == null) {
                unShownMessages = new ArrayList<>();
            }
            unShownMessages.add(message);
        }
    }

    private void scrollMessageListDown() {
        messagesListView.setSelection(messagesListView.getCount() - 1);
    }

    private void loadChatHistory() {
        ChatHelper.getInstance().loadChatHistory(qbChatDialog, skipPagination, new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {
                Log.i(TAG, "LOADING CHAT HISTORY onSuccess");
                // The newest messages should be in the end of list,
                // so we need to reverse list to show messages in the right order
                Collections.reverse(messages);
                if (chatAdapter == null) {
                    chatAdapter = new ChatAdapter(ActivityChatDialog.this, qbChatDialog, messages);
                    chatAdapter.setPaginationHistoryListener(new PaginationHistoryListener() {
                        @Override
                        public void downloadMore() {
                            loadChatHistory();
                        }
                    });
                    chatAdapter.setOnItemInfoExpandedListener(new ChatAdapter.OnItemInfoExpandedListener() {
                        @Override
                        public void onItemInfoExpanded(final int position) {
                            if (isLastItem(position)) {
                                // HACK need to allow info textview visibility change so posting it via handler
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        messagesListView.setSelection(position);
                                    }
                                });
                            } else {
                                messagesListView.smoothScrollToPosition(position);
                            }
                        }

                        private boolean isLastItem(int position) {
                            return position == chatAdapter.getCount() - 1;
                        }
                    });
                    if (unShownMessages != null && !unShownMessages.isEmpty()) {
                        List<QBChatMessage> chatList = chatAdapter.getList();
                        for (QBChatMessage message : unShownMessages) {
                            if (!chatList.contains(message)) {
                                chatAdapter.add(message);
                            }
                        }
                    }
                    messagesListView.setAdapter(chatAdapter);
                    messagesListView.setAreHeadersSticky(false);
                    messagesListView.setDivider(null);
                    //progressBar.setVisibility(View.GONE);
                } else {
                    chatAdapter.addList(messages);
                    messagesListView.setSelection(messages.size());
                }
                Utils.hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.i(TAG, "LOADING CHAT HISTORY onError: " + e.getMessage());
                //progressBar.setVisibility(View.GONE);
                skipPagination -= ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
                //snackbar = showErrorSnackbar(R.string.connection_error, e, null);
            }
        });
        skipPagination += ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
    }


    private void loadUsersFromQb(String userContactNumber) {
        QBUsers.getUserByLogin(userContactNumber).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {

            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    @Override
    public void onImagePicked(int requestCode, File file) {
        switch (requestCode) {
            case REQUEST_CODE_ATTACHMENT:
                attachmentPreviewAdapter.add(file);
                break;
        }
    }

    @Override
    public void onImagePickError(int requestCode, Exception e) {

    }

    @Override
    public void onImagePickClosed(int requestCode) {

    }


    public class ChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            Log.i(TAG, "MESSAGE BODY: " + qbChatMessage.getBody() +
                    " FROM SENDER ID: " + qbChatMessage.getSenderId());
            showMessage(qbChatMessage);
        }
    }
//
//    private void signInQB() {
//        if (!checkSignIn()) {
//            QBUser qbUser = CoreConfigUtils.getUserFromConfig(Consts.SAMPLE_CONFIG_FILE_NAME);
//
//            QBUsers.signIn(qbUser).performAsync(new QBEntityCallback<QBUser>() {
//                @Override
//                public void onSuccess(QBUser qbUser, Bundle bundle) {
//                    proceedToTheNextActivity();
//                }
//
//                @Override
//                public void onError(QBResponseException e) {
//                    showSnackbarError(null, R.string.splash_create_session_error, e, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            signInQB();
//                        }
//                    });
//                }
//            });
//        } else {
//            proceedToTheNextActivityWithDelay();
//        }
//    }

    protected boolean checkSignIn() {
        return QBSessionManager.getInstance().getSessionParameters() != null;
    }

    public void notificationChatInit(int qbUserId,String UserName)
    {

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(UserName);
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        ArrayList<Integer> occupantIdsList = new ArrayList<>();
        occupantIdsList.add(qbUserId);
        occupantIdsList.add(App.getInstance().getUser().getQuickBloxUser().getId());

        qbChatDialog = DialogUtils.buildPrivateDialog(qbUserId);
        qbChatDialog.setOccupantsIds(occupantIdsList);
        qbChatDialog.setUserId(App.getInstance().getUser().getQuickBloxUser().getId());
        Log.i(TAG, "qbChatDialog: " + qbChatDialog.hashCode());

        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();


        QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>()
        {
            @Override
            public void onSuccess(QBChatDialog result, Bundle params)
            {
                Log.i(TAG, "result: " + result.hashCode());
                qbChatDialog = result;
                Log.i(TAG, "CHAT DIALOG WAS CREATED SUCCESSFULLY!!!");


                Log.i(TAG, "getRecipientId: " + result.getRecipientId());
                Log.i(TAG, "getDialogId: " + result.getDialogId());
                Log.i(TAG, "getName: " + result.getName());
                Log.i(TAG, "getId: " + result.getId());
                Log.i(TAG, "getUserId: " + result.getUserId());
                Log.i(TAG, "getOccupants: " + result.getOccupants());
                for (Integer id : result.getOccupants())
                {
                    Log.i(TAG, "occupant: " + id);
                }

                sendSystemMessageAboutCreatingDialog(systemMessagesManager, result);
                initView(qbUserName);

            }

            @Override
            public void onError(QBResponseException responseException)
            {
                Utils.hideProgressDialog();
                Log.i(TAG, "ERROR CREATING CHAT DIALOG");
            }
        });
    }

    public void sendSystemMessageAboutCreatingDialog(QBSystemMessagesManager systemMessagesManager, QBChatDialog dialog)
    {
        QBChatMessage systemMessageCreatingDialog = buildSystemMessageAboutCreatingGroupDialog(dialog);

        for (Integer recipientId : dialog.getOccupants())
        {
            if (QBChatService.getInstance().getUser() == null)
            {
                Log.i(TAG, "QB USER IS NULL");
                return;
            }
            if (!recipientId.equals(QBChatService.getInstance().getUser().getId()))
            {
                systemMessageCreatingDialog.setRecipientId(recipientId);
                try
                {
                    systemMessagesManager.sendSystemMessage(systemMessageCreatingDialog);
                } catch (SmackException.NotConnectedException e)
                {
                    e.printStackTrace();
                }
            }
        }

    }

    private static QBChatMessage buildSystemMessageAboutCreatingGroupDialog(QBChatDialog dialog)
    {
        QBChatMessage qbChatMessage = new QBChatMessage();
        qbChatMessage.setDialogId(dialog.getDialogId());
        qbChatMessage.setProperty(PROPERTY_OCCUPANTS_IDS, QbDialogUtils.getOccupantsIdsStringFromList(dialog.getOccupants()));
        qbChatMessage.setProperty(PROPERTY_DIALOG_TYPE, String.valueOf(dialog.getType().getCode()));
        qbChatMessage.setProperty(PROPERTY_DIALOG_NAME, String.valueOf(dialog.getName()));
        qbChatMessage.setProperty(PROPERTY_NOTIFICATION_TYPE, "creating_dialog");

        return qbChatMessage;
    }

}
