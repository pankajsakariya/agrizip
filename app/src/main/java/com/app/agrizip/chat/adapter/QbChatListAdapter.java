package com.app.agrizip.chat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.quickblox.chat.model.QBPresence;

import java.util.ArrayList;
import java.util.List;

import apimodels.FriendList;
import viewholders.VHSingleChatListUser;

/**
 * Created by rutvik on 1/27/2017 at 3:50 PM.
 */

public class QbChatListAdapter extends RecyclerView.Adapter
{

    final Context context;

    final List<FriendList.DataBean> friendList;

    public QbChatListAdapter(final Context context)
    {
        this.context = context;
        friendList = new ArrayList<>();
    }

    public void addSingleFriend(FriendList.DataBean singleFriend)
    {
        boolean contains = false;
        if (friendList.size() > 0)
        {
            for (FriendList.DataBean s : friendList)
            {
                if (s.getMobileNumber().equals(singleFriend.getMobileNumber()))
                {
                    contains = true;
                }
            }
        }
        if (!contains)
        {
            friendList.add(singleFriend);
            notifyItemInserted(friendList.size());
        }
    }

    public void removeFriend(FriendList.DataBean friend)
    {
        final int index = friendList.indexOf(friend);
        friendList.remove(index);
        notifyItemRemoved(index);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingleChatListUser.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSingleChatListUser.bind((VHSingleChatListUser) holder, friendList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return friendList.size();
    }

    public void clear()
    {
        friendList.clear();
        notifyDataSetChanged();
    }

    public void notifyPresenceChanged(QBPresence qbPresence)
    {
        for (FriendList.DataBean singleUser : friendList)
        {
            if (singleUser.getOnPresenceChangeListener() != null)
            {
                singleUser.getOnPresenceChangeListener().onPresenceChanged(qbPresence);
            }
        }
    }
}
