package com.app.agrizip.chat.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.utils.DialogUtils;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

import org.jivesoftware.smack.SmackException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import quickblox.ChatHelper;
import quickblox.DialogsManager;
import quickblox.QbChatDialogMessageListenerImp;
import quickblox.QbDialogUtils;

public class ActivityChat extends AppCompatActivity implements DialogsManager.ManagingDialogsCallbacks
{

    public static final String PROPERTY_OCCUPANTS_IDS = "occupants_ids";
    public static final String PROPERTY_DIALOG_TYPE = "dialog_type";
    public static final String PROPERTY_DIALOG_NAME = "dialog_name";
    public static final String PROPERTY_NOTIFICATION_TYPE = "notification_type";
    private static final String TAG = App.APP_TAG + ActivityChat.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    QBChatDialog qbChatDialog;

    private QBSystemMessagesManager systemMessagesManager;
    private QBIncomingMessagesManager incomingMessagesManager;

    private QBChatDialogMessageListener allDialogsMessagesListener;

    private DialogsManager dialogsManager;

    private SystemMessagesListener systemMessagesListener;

    private static QBChatMessage buildSystemMessageAboutCreatingGroupDialog(QBChatDialog dialog)
    {
        QBChatMessage qbChatMessage = new QBChatMessage();
        qbChatMessage.setDialogId(dialog.getDialogId());
        qbChatMessage.setProperty(PROPERTY_OCCUPANTS_IDS, QbDialogUtils.getOccupantsIdsStringFromList(dialog.getOccupants()));
        qbChatMessage.setProperty(PROPERTY_DIALOG_TYPE, String.valueOf(dialog.getType().getCode()));
        qbChatMessage.setProperty(PROPERTY_DIALOG_NAME, String.valueOf(dialog.getName()));
        qbChatMessage.setProperty(PROPERTY_NOTIFICATION_TYPE, "creating_dialog");

        return qbChatMessage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);

        dialogsManager = new DialogsManager();


        ArrayList<Integer> occupantIdsList = new ArrayList<>();
        occupantIdsList.add(23198823);
        occupantIdsList.add(23195125);

        qbChatDialog = DialogUtils.buildPrivateDialog(23198823);
        qbChatDialog.setOccupantsIds(occupantIdsList);
        qbChatDialog.setUserId(23195125);
        Log.i(TAG, "qbChatDialog: " + qbChatDialog.hashCode());

        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();


        QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>()
        {
            @Override
            public void onSuccess(QBChatDialog result, Bundle params)
            {
                Log.i(TAG, "result: " + result.hashCode());
                qbChatDialog = result;
                Log.i(TAG, "CHAT DIALOG WAS CREATED SUCCESSFULLY!!!");


                Log.i(TAG, "getRecipientId: " + result.getRecipientId());
                Log.i(TAG, "getDialogId: " + result.getDialogId());
                Log.i(TAG, "getName: " + result.getName());
                Log.i(TAG, "getId: " + result.getId());
                Log.i(TAG, "getUserId: " + result.getUserId());
                Log.i(TAG, "getOccupants: " + result.getOccupants());
                for (Integer id : result.getOccupants())
                {
                    Log.i(TAG, "occupant: " + id);
                }

                sendSystemMessageAboutCreatingDialog(systemMessagesManager, result);

                /*ActivityChatDialog
                        .startForResult(ActivityChat.this, 0, result);*/
            }

            @Override
            public void onError(QBResponseException responseException)
            {
                Log.i(TAG, "ERROR CREATING CHAT DIALOG");
            }
        });

    }

    private void registerQbChatListeners()
    {
        incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

        if (incomingMessagesManager != null)
        {
            incomingMessagesManager.addDialogMessageListener(allDialogsMessagesListener != null
                    ? allDialogsMessagesListener : new AllDialogsMessageListener());
        }

        if (systemMessagesManager != null)
        {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : new SystemMessagesListener());
        }

        dialogsManager.addManagingDialogsCallbackListener(this);

    }

    private void unregisterQbChatListeners()
    {
        if (incomingMessagesManager != null)
        {
            incomingMessagesManager.removeDialogMessageListrener(allDialogsMessagesListener);
        }

        if (systemMessagesManager != null)
        {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }

        dialogsManager.removeManagingDialogsCallbackListener(this);
    }

    public void sendSystemMessageAboutCreatingDialog(QBSystemMessagesManager systemMessagesManager, QBChatDialog dialog)
    {
        QBChatMessage systemMessageCreatingDialog = buildSystemMessageAboutCreatingGroupDialog(dialog);

        for (Integer recipientId : dialog.getOccupants())
        {
            if (QBChatService.getInstance().getUser() == null)
            {
                Log.i(TAG, "QB USER IS NULL");
                return;
            }
            if (!recipientId.equals(QBChatService.getInstance().getUser().getId()))
            {
                systemMessageCreatingDialog.setRecipientId(recipientId);
                try
                {
                    systemMessagesManager.sendSystemMessage(systemMessageCreatingDialog);
                } catch (SmackException.NotConnectedException e)
                {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onDialogCreated(QBChatDialog chatDialog)
    {

    }

    @Override
    public void onDialogUpdated(String chatDialog)
    {

    }

    @Override
    public void onNewDialogLoaded(QBChatDialog chatDialog)
    {

    }

    private class SystemMessagesListener implements QBSystemMessageListener
    {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage)
        {
            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }

        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage)
        {

        }
    }

    private class AllDialogsMessageListener extends QbChatDialogMessageListenerImp
    {
        @Override
        public void processMessage(final String dialogId, final QBChatMessage qbChatMessage, Integer senderId)
        {
            if (!senderId.equals(ChatHelper.getCurrentUser().getId()))
            {
                dialogsManager.onGlobalMessageReceived(dialogId, qbChatMessage);
            }
        }
    }


}
