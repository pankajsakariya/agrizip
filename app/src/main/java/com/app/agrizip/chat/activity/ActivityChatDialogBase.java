package com.app.agrizip.chat.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.app.agrizip.app.App;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import quickblox.ChatHelper;
import quickblox.QbAuthUtils;
import quickblox.QbSessionStateCallback;

/**
 * Created by rutvik on 1/25/2017 at 4:35 PM.
 */

public abstract class ActivityChatDialogBase extends AppCompatActivity implements QbSessionStateCallback
{

    private static final String TAG = App.APP_TAG + ActivityChatDialogBase.class.getSimpleName();
    private static final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
    protected boolean isAppSessionActive;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        boolean wasAppRestored = savedInstanceState != null;
        boolean isQbSessionActive = QbAuthUtils.isSessionActive();
        final boolean needToRestoreSession = wasAppRestored || !isQbSessionActive;
        Log.v(TAG, "wasAppRestored = " + wasAppRestored);
        Log.v(TAG, "isQbSessionActive = " + isQbSessionActive);

        // Triggering callback via Handler#post() method
        // to let child's code in onCreate() to execute first
        mainThreadHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                if (needToRestoreSession)
                {
                    recreateChatSession();
                    isAppSessionActive = false;
                } else
                {
                    onSessionCreated(true);
                    isAppSessionActive = true;
                }
            }
        });
    }

    private void recreateChatSession()
    {
        Log.d(TAG, "Need to recreate chat session");

        QBUser user = App.getInstance().getUser().getQuickBloxUser();
        if (user == null)
        {
            throw new RuntimeException("User is null, can't restore session");
        }

        reloginToChat(user);
    }

    private void reloginToChat(final QBUser user)
    {

        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>()
        {
            @Override
            public void onSuccess(Void result, Bundle bundle)
            {
                Log.v(TAG, "Chat login onSuccess()");
                isAppSessionActive = true;
                onSessionCreated(true);
            }

            @Override
            public void onError(QBResponseException e)
            {
                isAppSessionActive = false;
                Log.w(TAG, "Chat login onError(): " + e);
                onSessionCreated(false);
            }
        });
    }

}
