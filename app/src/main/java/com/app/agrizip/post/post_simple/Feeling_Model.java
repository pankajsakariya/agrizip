package com.app.agrizip.post.post_simple;

import android.graphics.Bitmap;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Created by S-PC on 10/03/2017.
 */

public class Feeling_Model extends ArrayList<Parcelable> {


        int icon;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getFeelingName() {
        return feelingName;
    }

    public void setFeelingName(String feelingName) {
        this.feelingName = feelingName;
    }

    String feelingName;

        public Feeling_Model(){

        }
        public Feeling_Model(int icon,String feelingName){
            this.icon=icon;
            this.feelingName=feelingName;
        }


}
