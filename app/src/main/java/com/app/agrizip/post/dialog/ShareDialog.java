package com.app.agrizip.post.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.app.agrizip.R;

/**
 * Created by rutvik on 2/11/2017 at 2:20 PM.
 */

public class ShareDialog extends Dialog
{
    public ShareDialog(Context context)
    {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_share_post);


    }

}
