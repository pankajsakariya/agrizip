package com.app.agrizip.post.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;

import java.io.IOException;

import api.API;
import api.RetrofitCallbacks;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/27/2017 at 3:10 PM.
 */

public class DialogHideDeletePost extends Dialog
{

    private static final String TAG = App.APP_TAG + DialogHideDeletePost.class.getSimpleName();
    final String postId;
    @BindView(R.id.tv_postHideDeleteMessage)
    TextView tvPostHideDeleteMessage;
    @BindView(R.id.btn_postHideDeleteSave)
    Button btnPostHideDeleteSave;
    @BindView(R.id.btn_postHideDeleteCancel)
    Button btnPostHideDeleteCancel;
    Purpose purpose;

    public DialogHideDeletePost(Context context, final String postId, Purpose purpose)
    {
        super(context);
        this.postId = postId;
        this.purpose = purpose;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_hide_post);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        ButterKnife.bind(this);

        if (purpose == Purpose.DELETE_POST)
        {
            tvPostHideDeleteMessage.setText("Are you Sure You Want To Delete This Post?");
        } else if (purpose == Purpose.HIDE_POST)
        {
            tvPostHideDeleteMessage.setText("Are you Sure You Want To Hide This Post?");
        }

    }


    @OnClick(R.id.btn_postHideDeleteSave)
    public void performApiCall()
    {
        if (purpose == Purpose.DELETE_POST)
        {
            deletePost();
        } else if (purpose == Purpose.HIDE_POST)
        {
            hidePost();
        }
    }

    @OnClick(R.id.btn_postHideDeleteCancel)
    public void dismissThisDialogBox()
    {
        dismiss();
    }


    private void deletePost()
    {
        API.getInstance().deletePost(postId, new RetrofitCallbacks<ResponseBody>(getContext())
        {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    dismiss();
                    App.getInstance().getUser().getUserEventCallbacks().onStatusDeleted(postId);
                    try
                    {
                        Log.i(TAG, "RESPONSE: " + response.body().string());
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                } else
                {
                    try
                    {
                        Log.i(TAG, "RESPONSE: " + response.errorBody().string());
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }

        });
    }

    private void hidePost()
    {
        final RetrofitCallbacks<ResponseBody> onHidePostCallback =
                new RetrofitCallbacks<ResponseBody>(getContext())
                {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
                    {
                        super.onResponse(call, response);
                        if (response.isSuccessful())
                        {
                            App.getInstance().getUser().getUserEventCallbacks().onStatusDeleted(postId);
                            dismissThisDialogBox();
                            Toast.makeText(getContext(), "Post was made hidden", Toast.LENGTH_SHORT).show();
                        } else
                        {
                            Toast.makeText(getContext(), "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().hideFeedPost(App.getInstance().getUser().getId(),
                postId, onHidePostCallback);
    }


    public static enum Purpose
    {
        HIDE_POST(0), DELETE_POST(1);

        int value;

        Purpose(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return value;
        }
    }

}
