package com.app.agrizip.post.post_comments.dailog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.app.agrizip.app.App;
import com.app.agrizip.home.fragment.FragmentNewsFeed;
import com.app.agrizip.post.post_comments.adapter.CommentsAdapter;
import com.app.agrizip.post.post_comments.fragment.FragmentComments;
import com.bumptech.glide.util.Util;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import viewholders.VHSinglePost;

/**
 * Created by rutvik on 2/2/2017 at 5:21 PM.v
 */

public class CommentsDialog extends Dialog implements CommentsAdapter.changeCommentcount{

    public static final int REQUEST_POST_IMAGE_AS_COMMENT = 3542;
    private static final String TAG = App.APP_TAG + FragmentComments.class.getSimpleName();
    final CommentOnPost newComment = new CommentOnPost();
    @BindView(R.id.iv_addImageToNewComment)
    ImageView ivAddImageToNewComment;
    @BindView(R.id.iv_postNewComment)
    ImageView ivPostNewComment;
    @BindView(R.id.et_newCommentBody)
    EditText etNewCommentBody;
    @BindView(R.id.tv_commentCount)
    TextView tvCommentCount;
    @BindView(R.id.rv_comments)
    RecyclerView rvComments;
    @BindView(R.id.tv_comment)
    TextView tv_comment;
     String count ;
    CommentsAdapter adapter;
    ProgressBar pb;
    ProgressDialog pd;
    boolean tempVeriable ;
    public CommentCountChangeListener mListenerCount = null;
    final RetrofitCallbacks<ResponseBody> onPostComment = new RetrofitCallbacks<ResponseBody>(getContext()) {

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            super.onResponse(call, response);
            pb.setVisibility(View.GONE);
            pd.dismiss();
            if (response.isSuccessful()) {
                try {
                    final JsonElement jsonElement;
                    jsonElement = new Gson().fromJson(response.body().string(), JsonElement.class);
                    JsonObject root = jsonElement.getAsJsonObject();
                    CommentsOnPost.DataBean newComment = new Gson().fromJson(root.getAsJsonObject("Data"), CommentsOnPost.DataBean.class);
                    newComment.setUserImage(App.getInstance().getUser().getUserImage());
                    newComment.setFullName(App.getInstance().getUser().getFullName());

                    Date date = new Date(Calendar.getInstance().getTimeInMillis() - TimeZone.getDefault().getRawOffset());
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    String dateFormatted = formatter.format(date);

                    newComment.setCreateDate(dateFormatted);
                    adapter.addCommentModel(newComment);
                    etNewCommentBody.setText("");
                    int intCount = Integer.parseInt(count);
                    intCount++;
                    count = String.valueOf(intCount);
                    tvCommentCount.setText (count);
                   /* if(mListenerCount !=null){
                        mListenerCount.OnChangeCommentCount(Integer.parseInt(count),VHSinglePost.temp_comment_count);
                    }*/
                    getCommentsOnThisPost();
                    Toast.makeText(getContext(), "Comment posted successfully", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    pd.dismiss();
                }

            }
        }
    };
    Call<ResponseBody> call;
    String commentImage = "";
    String postId;

    Activity owner;

    public CommentsDialog(Context context, String postId,String CommentCount) {
        super(context);
        this.postId = postId;
        this.count = CommentCount;
        tempVeriable = true;

        // chances of context not being an activity is very low, but better to check.
        owner = (context instanceof Activity) ? (Activity) context : null;
    }

    public CommentsDialog(Context context, String postId) {
        super(context);
        this.postId = postId;

        // chances of context not being an activity is very low, but better to check.
        owner = (context instanceof Activity) ? (Activity) context : null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_comments);

        pb = (ProgressBar) findViewById(R.id.pb_loadingFeeds);
        pd = new ProgressDialog(getContext());
        pd.setMessage("Please Wait..");
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        ButterKnife.bind(this);

        rvComments.setLayoutManager(new LinearLayoutManager(getContext()));
        rvComments.setHasFixedSize(true);

        rvComments.setItemAnimator(new DefaultItemAnimator());

        adapter = new CommentsAdapter(getContext());
        CommentsAdapter.changeCommentcount = this;

        rvComments.setAdapter(adapter);

        getCommentsOnThisPost();
        if (tvCommentCount.equals("0")) {
            tv_comment.setVisibility(View.VISIBLE);
        }

    }


    private void getCommentsOnThisPost() {
        if (call != null) {
            call.cancel();
        }
        RetrofitCallbacks<CommentsOnPost> onGetCommentList =
                new RetrofitCallbacks<CommentsOnPost>(getContext()) {

                    @Override
                    public void onResponse(Call<CommentsOnPost> call, Response<CommentsOnPost> response) {
                        super.onResponse(call, response);
                        pb.setVisibility(View.GONE);

                        if (response.isSuccessful()) {
                            adapter.clear();
                            if (response.body().getData().size() > 0) {
                                tv_comment.setVisibility(View.GONE);
                            } else {
                                tv_comment.setVisibility(View.VISIBLE);
                            }
                            for (CommentsOnPost.DataBean singleComment : response.body().getData()) {
                                adapter.addCommentModel(singleComment);

                            }
                            rvComments.getLayoutManager().scrollToPosition(adapter.getItemCount());
                            Utils.hideKeyboard(getContext(), getCurrentFocus());
                            if(tempVeriable){
                                tvCommentCount.setText(count);
                                tempVeriable = false;
                            }else{
                                count = tvCommentCount.getText().toString();
                            }
                          //  tvCommentCount.setText(adapter.getItemCount() + "");
                           // count = tvCommentCount.getText().toString();
                             if(mListenerCount !=null){
                                 if(owner.toString().contains("FullPhoto")){
                                     mListenerCount.OnChangeCommentCount(Integer.parseInt(count),0);
                                 }else{
                                     mListenerCount.OnChangeCommentCount(Integer.parseInt(count),VHSinglePost.temp_comment_count);
                                 }
                        //mListenerCount.OnChangeCommentCount(Integer.parseInt(count),VHSinglePost.temp_comment_count);
                    }
                       /*     if (count.equals("0")) {
                                tv_comment.setVisibility(View.VISIBLE);
                            }*/
                        }
                       /* else if (count.equals("0")) {
                            tv_comment.setVisibility(View.VISIBLE);
                        }*/
                    }
                };

        API.getInstance().getCommentListOfPost(postId,App.getInstance().getUser().getId(),onGetCommentList);

    }


    @OnClick(R.id.iv_addImageToNewComment)
    public void postNewImageOnComment() {
        ImagePicker.create(owner)
                .single() // single mode
                .showCamera(true) // show camera or not (true by default)
                .start(REQUEST_POST_IMAGE_AS_COMMENT); // start image picker activity with request code
    }

    @OnClick(R.id.iv_postNewComment)
    public void postingNewComment() {
        if (etNewCommentBody.getText().toString().trim().isEmpty()) {
            Toast.makeText(getContext(), "Please enter comment!", Toast.LENGTH_SHORT).show();
            return;
        }

    pd.show();
        final String commentBody = etNewCommentBody.getText().toString();

        etNewCommentBody.setText("");

        newComment.setComment(commentBody);
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(postId);
        newComment.setImagestr("");
        tvCommentCount.setText(count);

        if (call != null) {
            call.cancel();
        }
        call = API.getInstance().commentOnPost(newComment, onPostComment);

    }

    @Override
    public void onBackPressed() {

  //      Toast.makeText(getContext(),"Position" + VHSinglePost.temp_comment_count+ " Total Commnet " + count,Toast.LENGTH_LONG).show();
      /*  ActivityFullPhotoView.commentCount = count;
        Toast.makeText(getContext(),ActivityFullPhotoView.commentCount,Toast.LENGTH_LONG).show();*/
      //  new FragmentNewsFeed().OnChangeCommentCount(1,1);

        super.onBackPressed();

      //  new ActivityFullPhotoView().commentset(count);

    }

    public void postImageAsComment(Image image) {
        Log.i(TAG, "Posting image as comment");
        newComment.setComment(etNewCommentBody.getText().toString());
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(postId);
        newComment.setImagestr(Utils.convertImageFileToBase64(image.getPath()));
            pd.show();
        API.getInstance().commentOnPost(newComment, onPostComment);
    }

    @Override
    public void OnUpdateCommentCount() {
        int tempcount = Integer.parseInt(tvCommentCount.getText().toString());
        tempcount++;
        count = String.valueOf(tempcount);
        tvCommentCount.setText(count);
        if(mListenerCount !=null){
            if(owner.toString().contains("FullPhoto")){
                mListenerCount.OnChangeCommentCount(Integer.parseInt(count),0);
            }else{
                mListenerCount.OnChangeCommentCount(Integer.parseInt(count),VHSinglePost.temp_comment_count);
            }
            //mListenerCount.OnChangeCommentCount(Integer.parseInt(count),VHSinglePost.temp_comment_count);
        }

    }

    public interface CommentCountChangeListener{
        void OnChangeCommentCount(int count,int position);
    }

}
