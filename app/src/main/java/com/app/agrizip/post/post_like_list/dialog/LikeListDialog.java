package com.app.agrizip.post.post_like_list.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.post.post_like_list.adapter.LikeListAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.LikeListResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/11/2017 at 5:43 PM.
 */

public class LikeListDialog extends Dialog
{

    private final String postId;
    @BindView(R.id.rv_likeList)
    RecyclerView rvLikeList;
    @BindView(R.id.tv_likeListCount)
    TextView tvLikeListCount;
    LikeListAdapter adapter;
    private LikeListResponse likeListResponse;

    public LikeListDialog(Context context, String postId)
    {
        super(context);
        this.postId = postId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_like_list);


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        ButterKnife.bind(this);

        API.getInstance().getStatusPostLikesList(postId, App.getInstance().getUser().getId(),
                new RetrofitCallbacks<LikeListResponse>(getContext())
                {

                    @Override
                    public void onResponse(Call<LikeListResponse> call, Response<LikeListResponse> response)
                    {
                        super.onResponse(call, response);
                        if (response.isSuccessful())
                        {
                            likeListResponse = response.body();
                            tvLikeListCount.setText(likeListResponse.getData().size() + "");

                            setupRecyclerView();

                        }
                    }
                });
    }

    private void setupRecyclerView()
    {
        rvLikeList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvLikeList.setHasFixedSize(true);

        adapter = new LikeListAdapter(getContext());

        rvLikeList.setAdapter(adapter);

        if (likeListResponse != null)
        {
            if (likeListResponse.getData().size() > 0)
            {
                for (LikeListResponse.DataBean likeListItem : likeListResponse.getData())
                {
                    adapter.addLikeListItem(likeListItem);
                }
            }
        }

    }

}
