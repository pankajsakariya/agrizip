package com.app.agrizip.post.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.app.agrizip.R;
import com.app.agrizip.need_help.activity.ActivityNeedJob;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;

import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;

/**
 * Created by rutvik on 3/1/2017 at 5:56 PM.
 */

public class DialogNeedHelp extends Dialog
{

    @BindView(R.id.ll_needAdvise)
    LinearLayout llNeedAdvise;

    @BindView(R.id.ll_needBuy)
    LinearLayout llNeedBuy;

    @BindView(R.id.ll_needSale)
    LinearLayout llNeedSale;

    @BindView(R.id.ll_needJob)
    LinearLayout llNeedJob;

    @BindView(R.id.ll_jobOptionContainer)
    LinearLayout llJobOptionContainer;

    @BindView(R.id.btn_wantJob)
    Button btnWantJob;

    @BindView(R.id.btn_wantEmployee)
    Button btnWantEmployee;

    @BindView(R.id.ib_needJob)
    ImageButton ibNeedJob;
    int code ;
    public  boolean come_from_UserProfile =false;
    public DialogNeedHelp(Context context,int code)
    {
        super(context);
        this.code = code;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_need_help);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        ButterKnife.bind(this);

        final Intent needJobIntent = new Intent(getContext(), ActivityNeedJob.class);

        final Intent intent = new Intent(getContext(), ActivityPostToAgrizip.class);
        intent.putExtra(Constants.NEED_HELP, true);

        llNeedAdvise.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(code == 0){
                come_from_UserProfile = false;
            }else{
                come_from_UserProfile = true;
            }
                intent.putExtra("COME_FROM_PROFLE",come_from_UserProfile);
                intent.putExtra(Constants.HELP_TYPE, Constants.ADVISE);
                getContext().startActivity(intent);
                dismiss();
            }
        });

        llNeedBuy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(code == 0){
                    come_from_UserProfile = false;
                }else{
                    come_from_UserProfile = true;
                }
                intent.putExtra("COME_FROM_PROFLE",come_from_UserProfile);
                intent.putExtra(Constants.HELP_TYPE, Constants.BUY);
                getContext().startActivity(intent);
                dismiss();
            }
        });

        llNeedSale.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(code == 0){
                    come_from_UserProfile = false;
                }else{
                    come_from_UserProfile = true;
                }
                intent.putExtra("COME_FROM_PROFLE",come_from_UserProfile);
                intent.putExtra(Constants.HELP_TYPE, Constants.SELL);
                getContext().startActivity(intent);
                dismiss();
            }
        });

        llNeedJob.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(code == 0){
                    come_from_UserProfile = false;
                }else{
                    come_from_UserProfile = true;
                }
                ibNeedJob.setImageResource(R.drawable.check);
                llJobOptionContainer.setVisibility(View.VISIBLE);
            }
        });

        btnWantJob.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(code == 0){
                    come_from_UserProfile = false;
                }else{
                    come_from_UserProfile = true;
                }
                needJobIntent.putExtra("COME_FROM_PROFLE",come_from_UserProfile);
                needJobIntent.putExtra(Constants.NEED_JOB, true);
                getContext().startActivity(needJobIntent);
                dismiss();
            }
        });

        btnWantEmployee.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                needJobIntent.putExtra("COME_FROM_PROFLE",come_from_UserProfile);
                needJobIntent.putExtra(Constants.NEED_JOB, false);
                getContext().startActivity(needJobIntent);
                dismiss();
            }
        });

    }
}
