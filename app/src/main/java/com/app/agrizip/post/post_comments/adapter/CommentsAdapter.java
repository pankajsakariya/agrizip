package com.app.agrizip.post.post_comments.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.post.post_comments.dailog.CommentsDialog;

import java.util.ArrayList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.Comment;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import viewholders.VHSingleComment;

/**
 * Created by rutvik on 12/16/2016 at 11:59 AM.
 */

public class CommentsAdapter extends RecyclerView.Adapter implements Comment.CommentOperationsListener
{

    final Context context;

    List<Comment> modelList;

    public static changeCommentcount changeCommentcount;

    public CommentsAdapter(Context context)
    {
        this.context = context;
        modelList = new ArrayList<>();
    }

    public void addCommentModel(Comment model)
    {
        modelList.add(model);
//        CommentsDialog.tvCommentCount.setText(modelList.size());
        notifyItemInserted(modelList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingleComment.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        modelList.get(position).setCommentOperationsListener(this);
        VHSingleComment.bind((VHSingleComment) holder, modelList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return modelList.size();
    }


    public void clear()
    {
        modelList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onDeleteComment(final Comment model)
    {
        RetrofitCallbacks<ResponseBody> onRemoveCommentCallback = new RetrofitCallbacks<ResponseBody>(context)
        {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    final int position = modelList.indexOf(model);
                    modelList.remove(model);
                    notifyItemRemoved(position);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t)
            {
                super.onFailure(call, t);
                Toast.makeText(context, R.string.try_again_later, Toast.LENGTH_SHORT).show();
            }
        };

        API.getInstance().removeCommentOnPost(model.getStatusDetailID(), onRemoveCommentCallback);
    }

    @Override
    public void onLikeUnlikeComment(final Comment model)
    {

        final boolean likeStatus = !model.isLiked();

        RetrofitCallbacks<ResponseBody> onLikeUnlikeCallback = new RetrofitCallbacks<ResponseBody>(context)
        {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    model.setLiked(likeStatus);
                    if (likeStatus)
                    {
                        model.setCommentLikeCount(model.getCommentLikeCount() + 1);
                    } else
                    {
                        if(model.getCommentLikeCount() == 0){

                        }else{
                            model.setCommentLikeCount(model.getCommentLikeCount() - 1);
                        }

                    }
                    notifyDataSetChanged();
                }
            }
        };

        final CommentOnPost likeComment = new CommentOnPost();
        likeComment.setStatusID(model.getStatusID());
        likeComment.setUserID(App.getInstance().getUser().getId());
        likeComment.setStatusDetailID(model.getStatusDetailID());
        likeComment.setStatusID(model.getStatusID());
        likeComment.setIsLike(likeStatus);

        API.getInstance().likeUnlikeOnComment(likeComment, onLikeUnlikeCallback);
    }

    @Override
    public void onReplied(final Comment singleComment,
                          final Comment.CommentReply mReply)
    {
        final CommentsOnPost.DataBean.CommentReplyDetailBean reply =
                (CommentsOnPost.DataBean.CommentReplyDetailBean) mReply;

        final RetrofitCallbacks<ResponseBody> onReplyCallback = new RetrofitCallbacks<ResponseBody>(context)
        {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    if (singleComment.getCommentReplyDetailForPost() == null)
                    {
                        singleComment.setCommentReplyDetailForPost(new ArrayList<CommentsOnPost.DataBean.CommentReplyDetailBean>());
                    }
                    if(changeCommentcount !=null){
                       changeCommentcount.OnUpdateCommentCount();
                    }
                    singleComment.getCommentReplyDetailForPost().add(reply);
                    notifyDataSetChanged();
                }
            }
        };


        final CommentOnPost commentOnPost = new CommentOnPost();
        commentOnPost.setIsComment(true);
        commentOnPost.setStatusID(singleComment.getStatusID());
        commentOnPost.setUserID(App.getInstance().getUser().getId()
        );
        commentOnPost.setStatusDetailID(singleComment.getStatusDetailID());
        commentOnPost.setComment(reply.getReplyComment());

        API.getInstance().replyOnComment(commentOnPost, onReplyCallback);
    }

    public interface changeCommentcount{
        void OnUpdateCommentCount();
    }


}
