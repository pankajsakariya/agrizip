package com.app.agrizip.post.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.Calendar;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FollowersList;
import apimodels.FriendList;
import apimodels.UnfollowUser;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/27/2017 at 3:10 PM.
 */

public class DialogFollowerFriend extends Dialog {

    private static final String TAG = App.APP_TAG + DialogFollowerFriend.class.getSimpleName();
    final String postId;
    @BindView(R.id.btn_unfollow)
    Button btnPostHideDeleteSave;
    @BindView(R.id.btn_cancle)
    Button btnPostHideDeleteCancel;
    Purpose purpose;
    FollowersList.DataBean singleFollower;
    @BindView(R.id.iv_feedUserPic)
    PorterShapeImageView iv_feedUserPic;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_title)
    TextView tv_title;
    FriendList.DataBean thisFriend;

    public changetextFollow mChangeFolloetext;


    public DialogFollowerFriend(Context context, final String postId, Purpose purpose, FriendList.DataBean thisFriend) {
        super(context);
        this.thisFriend = thisFriend;
        this.postId = postId;
        this.purpose = purpose;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.custom_dialog_follower);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes((WindowManager.LayoutParams) params);
        ButterKnife.bind(this);
        tv_name.setText(thisFriend.getFullName());

                if(thisFriend.isIsFollow()){
                    btnPostHideDeleteSave.setText("UnFollow");
                    tv_title.setText("Are you Sure To Unfollow ?");
                }else{
                    tv_title.setText("Are you Sure To Follow ?");
                    btnPostHideDeleteSave.setText("Follow");

                }
        Glide.with(getContext())
                .load(thisFriend.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(iv_feedUserPic);

    }

    @OnClick(R.id.btn_unfollow)
    public void performApiCall() {
      //  Toast.makeText(getContext(), "Unfollow", Toast.LENGTH_LONG).show();
        if(btnPostHideDeleteSave.getText().toString().equalsIgnoreCase("UnFollow")){
            unfollow();
        }else{
            FollowFriend();
        }

        dismiss();
    }

    @OnClick(R.id.btn_cancle)
    public void dismissThisDialogBox() {
        // Toast.makeText(getContext(),"Cancel",Toast.LENGTH_LONG).show();
        dismiss();
    }
    private void FollowFriend(){
        final UnfollowUser followUser = new UnfollowUser();
        followUser.setIsFollow(true);
        followUser.setFollowUserId(thisFriend.getFriendUserId());
        followUser.setUserId(App.getInstance().getUser().getId());
        followUser.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        API.getInstance().followUser(followUser,
                new RetrofitCallbacks<ResponseBody>(getContext()) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            Constants.UPDATE_FLLOWCOUNT = true;
                            if (thisFriend.getFriendActionListener() != null) {
                                //   thisFriend.getFriendActionListener().onUnfollow(thisFriend);
                            }
                            if(mChangeFolloetext !=null){
                                mChangeFolloetext.OnChnageText(true);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        super.onFailure(call, t);
                    }
                });
    }

    private void unfollow() {
        final UnfollowUser unfollowUser = new UnfollowUser();
        unfollowUser.setIsFollow(false);
        unfollowUser.setFollowUserId(thisFriend.getFriendUserId());
        unfollowUser.setUserId(App.getInstance().getUser().getId());
        unfollowUser.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");


        final RetrofitCallbacks<ResponseBody> onUnfollow = new RetrofitCallbacks<ResponseBody>(getContext()) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    Constants.UPDATE_FLLOWCOUNT = true;
                    if (thisFriend.getFriendActionListener() != null) {
                     //   thisFriend.getFriendActionListener().onUnfollow(thisFriend);

                    }
                    if(mChangeFolloetext !=null){
                        mChangeFolloetext.OnChnageText(false);
                    }
                } else {
                    Toast.makeText(getContext(), "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        };

        API.getInstance().unfollowUser(unfollowUser, onUnfollow);

    }


    private void unfollowUser() {
        UnfollowUser unfollowUser = new UnfollowUser();
        unfollowUser.setFollowUserId(singleFollower.getFollowUserId());
        unfollowUser.setIsFollow(false);
        unfollowUser.setFollowupId(singleFollower.getFollowupId());
        unfollowUser.setUserId(App.getInstance().getUser().getId());

        API.getInstance().unfollowUser(unfollowUser,
                new RetrofitCallbacks<ResponseBody>(getContext()) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                          //  Toast.makeText(getContext(), "Unfollowed Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        super.onFailure(call, t);
                    }
                });
    }

    public static enum Purpose {
        HIDE_POST(0);

        int value;

        Purpose(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

   public interface changetextFollow{

       void OnChnageText(boolean isUnFollow );
   }

}
