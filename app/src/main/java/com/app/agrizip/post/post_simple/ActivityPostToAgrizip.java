package com.app.agrizip.post.post_simple;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.friends.friend_tag.ActivityTagFriends;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.post.dialog.DialogDiscardPost;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import api.API;
import api.RetrofitCallbacks;
import apimodels.EditedPost;
import apimodels.GetSinglePostDetails;
import apimodels.PostStatusResponse;
import apimodels.SinglePost;
import apimodels.UserInfo;
import apimodels.UserPrivacyDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import services.StatusImageUploadingService;
import viewholders.SelectedImage;

import static extras.Constants.CHECKIN_ACTIVITY;
import static extras.Constants.REQUEST_CODE_IMAGE_PICKER;
import static extras.Constants.TAG_FRIEND_ACTIVITY;

public class ActivityPostToAgrizip extends AppCompatActivity
        implements SelectedImage.SelectedImageOperations, DialogInterface.OnClickListener {

    public static final String POST_CAPTURED_IMAGE = "POST_CAPTURED_IMAGE";
    public static final String POST_CAPTURED_IMAGE_URL = "POST_CAPTURED_IMAGE_URL";
    private static final String TAG = App.APP_TAG + ActivityPostToAgrizip.class.getSimpleName();
    final Map<String, Bitmap> selectedImageBitmaps = new LinkedHashMap<>();
    final Map<String, String> imageList = new LinkedHashMap<>();
    final ArrayList<String> taggedFriendIds = new ArrayList<>();
    final ArrayList<String> taggedFriendNames = new ArrayList<>();
    //Map<Integer, SinglePost.ImagesBean> imagesBeanList;
    final Handler mHandler = new Handler();
    UserInfo.User user;
    SinglePost post;
    boolean isCheckin = false;
    String checkinPlace;
    SelectedImage selectedImage;
    ArrayList<SinglePost.TagpeopleBean> taggedPeoples = new ArrayList<>();
    @BindView(R.id.ll_selectedImages)
    LinearLayout llSelectedImages;
    @BindView(R.id.ll_selectedImagesContainer)
    LinearLayout llSelectedImagesContainer;
    @BindView(R.id.tv_removeAllSelectedPhotos)
    TextView tvRemoveAllSelectedPhotos;
    @BindView(R.id.tv_postStatusUserName)
    TextView tvUserFullName;
    @BindView(R.id.tv_checkinPlace)
    TextView tvCheckinPlace;
    @BindView(R.id.tv_at)
    TextView tvAt;
    @BindView(R.id.et_postNewStatus)
    EditText etStatusMessage;
    @BindView(R.id.iv_postStatusUserPic)
    PorterShapeImageView ivPostStatusUserPic;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_postPrivacy)
    TextView tvPostPrivacy;

    @BindView(R.id.tv_toolbarTitle2)
    TextView tvToolbarTitle;

    @BindView(R.id.et_needHelpTitle)
    EditText etNeedHelpTitle;

    AlertDialog promptPrivacyDialog;

    ArrayAdapter<UserPrivacyDetails.DataBean> privacyAdapter;
    ArrayList<Image> galleryImage = new ArrayList<>();
    ArrayList<String> tempImageStore = new ArrayList<>();
    ArrayList<SinglePost.ImagesBean> imagesToBePosted = new ArrayList<>();

    boolean needHelp, postingStaus = false;

    int helpType;

    Bitmap capturedImage;

    boolean isEditingPost;
    String editPostId;
    int feelingIconId;
    String feeling_Name;
    ImageView iv_feeling, iv_feelingIcon;
    TextView tv_feeling_Name;
    ProgressDialog pd;

    SinglePost postToBeEdited;
    @BindView(R.id.tv_feeling)
    TextView tvFeeling;
    @BindView(R.id.tv_feelingName)
    TextView tvFeelingName;

    TextView tvTagFriend;

    @BindView(R.id.tv_checkin)
    ImageView tv_checkin;

    @BindView(R.id.tv_tagFriend)
    ImageView tv_tagFriend;

    @BindView(R.id.tv_postPhotosVideos)
    ImageView tv_postPhotosVideos;
    boolean come_from_UserProfile = false;


    final RetrofitCallbacks<GetSinglePostDetails> onGetPostDetailsCallback =
            new RetrofitCallbacks<GetSinglePostDetails>(this) {

                @Override
                public void onResponse(Call<GetSinglePostDetails> call, Response<GetSinglePostDetails> response) {
                    super.onResponse(call, response);

                    Utils.hideProgressDialog();

                    if (response.isSuccessful()) {
                        postToBeEdited = response.body().getData().get(0);
                        setDetailsIntoView();
                    } else {
                        Toast.makeText(ActivityPostToAgrizip.this, "try again later", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<GetSinglePostDetails> call, Throwable t) {
                    super.onFailure(call, t);
                    Toast.makeText(ActivityPostToAgrizip.this, "try again later", Toast.LENGTH_SHORT).show();
                    finish();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_to_agrizip);

        ButterKnife.bind(this);
        iv_feeling = (ImageView) findViewById(R.id.iv_feeling);
        iv_feelingIcon = (ImageView) findViewById(R.id.iv_feelingIcon);
        tv_feeling_Name = (TextView) findViewById(R.id.tv_feeling_Name);

        iv_feeling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeelingsActivity();
            }
        });

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);

            if (tvToolbarTitle != null) {
                tvToolbarTitle.setText(R.string.post_to_agrizip);
            }
            come_from_UserProfile = getIntent().getBooleanExtra("COME_FROM_PROFLE", false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        needHelp = getIntent().getBooleanExtra(Constants.NEED_HELP, false);

        if (getIntent().getStringExtra(Constants.POST_ID) != null) {
            editPostId = getIntent().getStringExtra(Constants.POST_ID);
            isEditingPost = true;
            getPostForEdit();
            tv_postPhotosVideos.setVisibility(View.GONE);
        }

        helpType = getIntent().getIntExtra(Constants.HELP_TYPE, 0);

        if (needHelp) {
            iv_feeling.setVisibility(View.GONE);
            tv_checkin.setVisibility(View.GONE);
            tv_tagFriend.setVisibility(View.GONE);
            tv_postPhotosVideos.setVisibility(View.VISIBLE);

            if (isEditingPost) {
                etNeedHelpTitle.setInputType(InputType.TYPE_NULL);

                etNeedHelpTitle.setVisibility(View.VISIBLE);
                iv_feeling.setVisibility(View.GONE);
                tv_checkin.setVisibility(View.GONE);
                tv_tagFriend.setVisibility(View.GONE);
                tv_postPhotosVideos.setVisibility(View.GONE);

                etNeedHelpTitle.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Toast.makeText(ActivityPostToAgrizip.this, "You can not edit Title",
                                Toast.LENGTH_SHORT).show();
                        return false;
                    }
                });

            } else {
                etNeedHelpTitle.setVisibility(View.VISIBLE);
            }

            etStatusMessage.setHint("Write Description Here...");

            if (helpType == Constants.ADVISE) {
                etNeedHelpTitle.setHint("Advise Title");

                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(R.string.need_a_help_for_advice);
                }

            } else if (helpType == Constants.BUY) {
                etNeedHelpTitle.setHint("Buy Title");

                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(R.string.need_a_help_for_buy);
                }

            } else if (helpType == Constants.SELL) {
                etNeedHelpTitle.setHint("Sale Title");

                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(R.string.need_a_help_for_sale);
                }

            }
        }


        user = ((App) getApplication()).getUser();

        tvUserFullName.setText(user.getFullName());

        post = new SinglePost();

        selectedImage = new SelectedImage(this,this);

        if (Constants.USERPROFILE_IMAGE.length() > 0) {
            Glide.with(this).load(Constants.USERPROFILE_IMAGE).asBitmap()
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    // .crossFade()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivPostStatusUserPic.setImageBitmap(resource);
                        }
                    });
        } else {
            Glide.with(this).load(user.getUserImage()).asBitmap()
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    // .crossFade()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivPostStatusUserPic.setImageBitmap(resource);
                        }
                    });
        }

        findViewById(R.id.tv_postPhotosVideos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImagePicker();

            }
        });

        findViewById(R.id.tv_checkin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //                Intent checkin = new Intent(ActivityPostToAgrizip.this, ActivityCheckin.class);
                //                startActivityForResult(checkin, CHECKIN_ACTIVITY);
                CheckInPlace();

            }
        });

        findViewById(R.id.tv_tagFriend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tagFriend = new Intent(ActivityPostToAgrizip.this, ActivityTagFriends.class);
                tagFriend.putExtra("COME_FROM_POST", "1");
                tagFriend.putExtra(Constants.TAG_FRIEND_LIST, taggedFriendIds);
                tagFriend.putExtra(Constants.TAG_FRIEND_NAME_LIST, taggedFriendNames);
                startActivityForResult(tagFriend, TAG_FRIEND_ACTIVITY);
            }
        });

        tvRemoveAllSelectedPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedImage.removeAll();
               //
                // imagesToBePosted.clear();
                imageList.clear();
                imagesToBePosted.clear();
            }
        });


        findViewById(R.id.tv_singleTaggedFriendName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tagFriend = new Intent(ActivityPostToAgrizip.this, ActivityTagFriends.class);
                tagFriend.putExtra("COME_FROM_POST", "1");
                tagFriend.putExtra(Constants.TAG_FRIEND_LIST, taggedFriendIds);
                tagFriend.putExtra(Constants.TAG_FRIEND_NAME_LIST, taggedFriendNames);
                startActivityForResult(tagFriend, TAG_FRIEND_ACTIVITY);
            }
        });

        findViewById(R.id.tv_otherTaggedFriends).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tagFriend = new Intent(ActivityPostToAgrizip.this, ActivityTagFriends.class);
                tagFriend.putExtra("COME_FROM_POST", "1");
                tagFriend.putExtra(Constants.TAG_FRIEND_LIST, taggedFriendIds);
                tagFriend.putExtra(Constants.TAG_FRIEND_NAME_LIST, taggedFriendNames);
                startActivityForResult(tagFriend, TAG_FRIEND_ACTIVITY);
            }
        });


        findViewById(R.id.tv_checkinPlace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent checkin = new Intent(ActivityPostToAgrizip.this, ActivityCheckin.class);
                startActivityForResult(checkin, CHECKIN_ACTIVITY);*/
                CheckInPlace();
            }
        });
        // Resolve File Camera issue in home page MN
        if(getIntent().getStringExtra(POST_CAPTURED_IMAGE_URL) != null && getIntent().getStringExtra(POST_CAPTURED_IMAGE_URL) != "")
        {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals(getIntent().getStringExtra(POST_CAPTURED_IMAGE_URL))) {
                    f = temp;
                    break;
                }
            }
            try {
                Bitmap bitmap;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                        bitmapOptions);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,50,stream);
                capturedImage = bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {

            capturedImage = getIntent().getParcelableExtra(POST_CAPTURED_IMAGE);
        }


        try {

            String imageKey = this.getIntent().getStringExtra("ImageKey");
            Log.d("imageKey=", imageKey);
            galleryImage = getIntent().getParcelableArrayListExtra("GalleryImage");


            for (int i = 0; i < galleryImage.size(); i++) {
                final Image singleImage = galleryImage.get(i);
                imageList.put(singleImage.getPath(), singleImage.getPath());
                selectedImageBitmaps.put(singleImage.getPath(), null);
                final SinglePost.ImagesBean ib = new SinglePost.ImagesBean();
                ib.setImagestr(singleImage.getPath());
                imagesToBePosted.add(ib);
               /* selectedImage.addSingleImage(singleImage.getPath(),
                        Utils.convertImageToBitmap(singleImage.getPath()));
*/
                selectedImage.addSingleImage(singleImage.getPath(),
                        null);

            }


            llSelectedImages.removeAllViews();
            llSelectedImages.addView(selectedImage.getView());

            if (selectedImageBitmaps.size() > 0) {
                llSelectedImagesContainer.setVisibility(View.VISIBLE);
            } else {
                llSelectedImagesContainer.setVisibility(View.GONE);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        if (capturedImage != null) {
            llSelectedImagesContainer.setVisibility(View.VISIBLE);

            Uri tempUri = Utils.getImageUri(getApplicationContext(), capturedImage);
            String filePath="";
            // CALL THIS METHOD TO GET THE ACTUAL PATH
          /*  if(ActivityHome.imageUri.toString().length() > 0){
                 filePath = Utils.getRealPathFromURI(this, ActivityHome.imageUri);
            }else{
                 filePath = Utils.getRealPathFromURI(this, tempUri);
            }*/

            filePath = Utils.getRealPathFromURI(this, tempUri);


            Log.i(TAG, "filePath: " + filePath);

            imageList.put(filePath, filePath);
            selectedImageBitmaps.put(filePath, capturedImage);
           // selectedImageBitmaps.put(filePath, Utils.convertImageToBitmap(filePath));

            selectedImage.addSingleImage(filePath, capturedImage);
            //selectedImage.addSingleImage(filePath, Utils.convertImageToBitmap(filePath));

            llSelectedImages.addView(selectedImage.getView());

        }

        preparePrivacyAdapter();

        if (getIntent().getBooleanExtra(Constants.IS_POST_TYPE_CHECKIN, false)) {
            // Intent checkin = new Intent(ActivityPostToAgrizip.this, ActivityCheckin.class);
            // startActivityForResult(checkin, CHECKIN_ACTIVITY);
            CheckInPlace();
        }

    }

    public void CheckInPlace() {
        isCheckin = false;
        checkinPlace = null;
        try {
            Intent intent =
                    new PlaceAutocomplete
                            .IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(ActivityPostToAgrizip.this);
            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }


    private void openImagePicker() {
        try {
           // mToolbar.setVisibility(View.GONE);
            ImagePicker.create(this)

                    .folderMode(false) // folder mode (false by default)
                    .folderTitle("Select Photos") // folder selection title
                    .imageTitle("Tap to select") // image selection title
                    .multi() // multi mode (default mode)
                    .limit(20) // max images can be selected (999 by default)
                    .showCamera(true) // show camera or not (true by default)
                    //.imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                    .start(REQUEST_CODE_IMAGE_PICKER); // start image picker activity with request code

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_post_to_agrizip, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        checkIfPostInProgress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            checkIfPostInProgress();
        }
        if (item.getItemId() == R.id.action_post) {
            Utils.hideKeyboard(this, this.getCurrentFocus());
            if (postingStaus == false) {
                postingStaus = true;
                if (isEditingPost) {
                    tryPostingEditedPost();
                    return true;
                }
                tryPostingFeed();

            } else {
                Toast.makeText(this, "Wait Your Post is Uploading..", Toast.LENGTH_LONG).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void tryPostingEditedPost() {
        Utils.showProgressDialog(this, "Please Wait...", "Posting status...", false, false, null);

        final EditedPost editedPost = new EditedPost();
        editedPost.setStatus(etStatusMessage.getText().toString());
        editedPost.setStatusId(postToBeEdited.getStatusId());

        postToBeEdited.setStatus(etStatusMessage.getText().toString());

        final RetrofitCallbacks<ResponseBody> onEditedPostCallback =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {

                            user.getUserEventCallbacks().onStatusPostEdit(postToBeEdited);
                            Toast.makeText(ActivityPostToAgrizip.this, "Post Edited Successfully",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Utils.hideProgressDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        super.onFailure(call, t);
                        Utils.hideProgressDialog();
                    }
                };

        API.getInstance().updateFeedPost(editedPost, onEditedPostCallback);
    }

    private void tryPostingFeed() {

        if (etStatusMessage.getText().toString().trim().isEmpty() &&
                imageList.size() == 0 && checkinPlace == null) {
            postingStaus = false;
            Toast.makeText(this, "Please write something or add images or make check-in to post status.", Toast.LENGTH_SHORT).show();
            return;
        }

        Utils.showProgressDialog(this, "Please Wait...", "Posting status...", false, false, null);

        post.setUserID(user.getId());
        post.setUserName(user.getFullName());
        post.setIsCheckIn(isCheckin);
        post.setUserImage(App.getInstance().getUser().getUserImage());
        if (isCheckin) {
            post.setPlace(checkinPlace);
        }
        // DateTime.Now.ToString("yyyy-MM-ddThh:mm:sszzz");
        //String timeStamp = new SimpleDateFormat("yyyy-MM-ddTHH:mm:sszzz").format(Calendar.getInstance().getTime());

       /* SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        TimeZone tz = Calendar.getInstance().getTimeZone();

        sf.setTimeZone(tz);

        String date = sf.format(new Date(Calendar.getInstance().getTimeInMillis()));*/

        Date date = new Date(Calendar.getInstance().getTimeInMillis() - TimeZone.getDefault().getRawOffset());
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String dateFormatted = formatter.format(date);
        post.setCreateDate(dateFormatted);
        //post.setCreateDate("Just now");
        post.setCreateDatetime(dateFormatted);
        post.setStatus(etStatusMessage.getText().toString().trim());
        post.setHelp(false);
        post.setSharedOwner("");
        if (taggedFriendIds.size() > 0) {
            for (String friendId : taggedFriendIds) {
                final SinglePost.TagpeopleBean tag = new SinglePost.TagpeopleBean();
                tag.setTaggedTo(friendId);
                tag.setTaggedBy(user.getId());
                tag.setIsDelete(false);
                taggedPeoples.add(tag);
            }

            post.setTagpeople(taggedPeoples);
        } else {
            post.setTagpeople(new ArrayList<SinglePost.TagpeopleBean>());
        }
        if(imageList.size() > 0){
            SinglePost.ImagesBean si;
         for(int i=0 ;i< imageList.size(); i++){
            si = new SinglePost.ImagesBean();
             si.setId("dhsk");
            // si.setImagestr(imageList.);
         }
        }
        post.setImages(imagesToBePosted);
      //  post.setImages();

        if (needHelp) {
            post.setHelp(true);
            post.setHelpTitle(etNeedHelpTitle.getText().toString());
            post.setHelpType(helpType);
        }

        API.getInstance().createNewStatusPost(post,
                new RetrofitCallbacks<PostStatusResponse>(ActivityPostToAgrizip.this) {

                    @Override
                    public void onResponse(Call<PostStatusResponse> call, Response<PostStatusResponse> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            if (response.body().getResponseStatus() == 1) {
                                Utils.hideProgressDialog();
                                Log.i(TAG, "STATUS MESSAGE: " + response.body().getData().getStatus());

                                tryUploadingImages(response.body().getData().getStatusId());

                                post.setStatusId(response.body().getData().getStatusId());
                                SinglePost.TagDetails tagDetails;
                                if (response.body().getData().getTagpeople().size() > 0) {
                                    List<SinglePost.TagDetails> tagDetailsList = new ArrayList<SinglePost.TagDetails>();
                                    for (int i = 0; i < response.body().getData().getTagpeople().size(); i++) {
                                        tagDetails = new SinglePost.TagDetails();
                                        tagDetails.setFullName(response.body().getData().getTagpeople().get(i).getTaggedName());

                                        tagDetailsList.add(i, tagDetails);

                                    }


                                    post.setTagDetails(tagDetailsList);
                                }


                                user.getUserEventCallbacks().onStatusPostSuccessful(post);
                                if (come_from_UserProfile) {
                                    if (response.body().getData().getImages().size() > 0) {

                                        Utils.showProgressDialog(ActivityPostToAgrizip.this, " Wait...", "Uploading Photo...", true, false, new DialogInterface.OnCancelListener() {
                                            @Override
                                            public void onCancel(DialogInterface dialogInterface) {
                                                finish();
                                            }
                                        });
                                        gotoUserProfile(5000);
                                    } else {
                                        gotoUserProfile(2000);
                                    }


                                   /* Intent intent = new Intent(ActivityPostToAgrizip.this, ActivityUserProfile.class);
                                    intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                                    intent.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    //   startActivity(new Intent(UpdateProfileActivity.this,ActivityUserProfile.class));
                                    finish();*/
                                } else {
                                    ActivityPostToAgrizip.this.finish();
                                }

                            }
                        } else {
                            Utils.hideProgressDialog();

                        }
                    }


                    @Override
                    public void onFailure(Call<PostStatusResponse> call, Throwable t) {
                        super.onFailure(call, t);
                        Utils.hideProgressDialog();
                        Toast.makeText(ActivityPostToAgrizip.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }

        );


    }

    public void gotoUserProfile(long milisecount) {
        CountDownTimer timer;
        timer = new CountDownTimer(milisecount, 20) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {

                    Utils.hideProgressDialog();
                    Intent intent = new Intent(ActivityPostToAgrizip.this, ActivityUserProfile.class);
                    intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                    intent.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();


                    //getNewActiveRide(context);
                } catch (Exception e) {
                    //  Log.e("Error", "Error: " + e.toString());
                }
            }
        }.start();

        //   startActivity(new Intent(UpdateProfileActivity.this,ActivityUserProfile.class));

    }

    private void tryUploadingImages(final String statusId) {

        /*if (imagesBeanList.values().size() > 0)
        {
            for (SinglePost.ImagesBean image : imagesBeanList.values())
            {
                imageList.add(image.getImagestr());
            }*/

        if (imageList.size() > 0) {
            Intent i = new Intent(ActivityPostToAgrizip.this, StatusImageUploadingService.class);
            i.putStringArrayListExtra(Constants.BASE_64_IMAGE_STRING_LIST, new ArrayList<>(imageList.values()));
            i.putExtra(Constants.STATUS_ID, statusId);
            startService(i);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 123) {
            if (resultCode == RESULT_OK) {
                feelingIconId = data.getIntExtra("feelingIcon", 0);
                feeling_Name = data.getStringExtra("feelingName");
                post.setFeelingName(feeling_Name);
                post.setFeelingValue(feelingIconId + "");
            } else {
                feelingIconId = 0;
                feeling_Name = null;
                post.setFeelingName("");
                post.setFeelingValue("");
            }
            setupCheckinTextAndTagText();
        } else {
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case TAG_FRIEND_ACTIVITY:

                        if (data != null) {
                            taggedFriendIds.clear();
                            taggedFriendNames.clear();

                            final ArrayList<String> taggedFriendsIds = data
                                    .getStringArrayListExtra(Constants.TAG_FRIEND_LIST);

                            Log.i(TAG, "ON RESULT TAG FRIEND ID LIST SIZE: " + taggedFriendsIds.size());

                            if (taggedFriendsIds != null) {
                                for (String id : taggedFriendsIds) {
                                    Log.i(TAG, "ON RESULT TAG FRIEND ID: " + id);
                                    taggedFriendIds.add(id);
                                }
                            }

                            final ArrayList<String> taggedFriendsNames = data
                                    .getStringArrayListExtra(Constants.TAG_FRIEND_NAME_LIST);

                            Log.i(TAG, "ON RESULT TAG FRIEND LIST SIZE: " + taggedFriendNames.size());

                            if (taggedFriendsNames != null) {
                                for (String name : taggedFriendsNames) {
                                    Log.i(TAG, "ON RESULT TAG FRIEND NAME: " + name);
                                    taggedFriendNames.add(name);
                                }
                            }

                            setupCheckinTextAndTagText();
                        }

                        break;

                    case CHECKIN_ACTIVITY:
                        if (data != null) {
                            final String place = data.getStringExtra(Constants.CHECKIN_PLACE);
                            if (place != null) {
                                checkinPlace = place;
                                isCheckin = true;
                                setupCheckinTextAndTagText();
                            }
                        }

                        break;
                    case 1:
                        if (requestCode == 1) {
                            if (resultCode == RESULT_OK) {
                                // retrive the data by using getPlace() method.
                                Place place = PlaceAutocomplete.getPlace(this, data);
                                if (place != null) {
                                    checkinPlace = place.getName().toString();
                                    isCheckin = true;
                                }
                            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                                Status status = PlaceAutocomplete.getStatus(this, data);
                                // TODO: Handle the error.
                                Log.e("Tag", status.getStatusMessage());
                                checkinPlace = null;
                                isCheckin = false;
                            } else if (resultCode == RESULT_CANCELED) {
                                checkinPlace = null;
                                isCheckin = false;
                            }
                        }
                        setupCheckinTextAndTagText();
                        break;
                    case REQUEST_CODE_IMAGE_PICKER:


                        if (data != null) {
                            final ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);


                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected void onPreExecute() {
                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            pd = new ProgressDialog(ActivityPostToAgrizip.this);
                                            pd.setTitle("Please Wait...");
                                            pd.setMessage("Adding Images...");
                                            pd.setIndeterminate(true);
                                            pd.show();
                                        }
                                    });

                                }

                                @Override
                                protected Void doInBackground(Void... voids) {

                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            for (int i = 0; i < images.size(); i++) {
                                                final Image singleImage = images.get(i);
                                                imageList.put(singleImage.getPath(), singleImage.getPath());
                                                try {
                                                    selectedImageBitmaps.put(singleImage.getPath(), null);
                                                    final SinglePost.ImagesBean ib = new SinglePost.ImagesBean();
                                                    ib.setImagestr(singleImage.getPath());
                                                    imagesToBePosted.add(ib);
                                                    tempImageStore.add(singleImage.getPath());

                                                    selectedImage.addSingleImage(singleImage.getPath(),
                                                            null);
                                                } catch (OutOfMemoryError e) {
                                                    Toast.makeText(ActivityPostToAgrizip.this, "Images is to Large Size reduse size and upload all.", Toast.LENGTH_LONG).show();
                                                    selectedImageBitmaps.clear();
                                                    imagesToBePosted.clear();
                                                    selectedImage.removeAll();
                                                    tempImageStore.clear();
                                                    imageList.clear();
                                                    //llSelectedImagesContainer.setVisibility(View.GONE);
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                }

                                            }


                                        }
                                    });
                                    return null;
                                }


                                @Override
                                protected void onPostExecute(Void aVoid) {

                                    llSelectedImages.removeAllViews();
                                    llSelectedImages.addView(selectedImage.getView());

                                    if (selectedImageBitmaps.size() > 0) {
                                        llSelectedImagesContainer.setVisibility(View.VISIBLE);
                                    } else {
                                        llSelectedImagesContainer.setVisibility(View.GONE);
                                    }
                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            pd.dismiss();
                                        }
                                    });
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }

                        break;
                }
            }
        }

    }

    @Override
    protected void onStop() {
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
        super.onStop();
    }

    @Override
    public void removeSelectedImage(String imagePath) {
        int index =  tempImageStore.indexOf(imagePath);
        try {

            selectedImageBitmaps.remove(imagePath);
            //  imagesToBePosted.remove(imagesToBePosted.indexOf(imagePath));

            tempImageStore.remove(imagePath);
            imagesToBePosted.remove(index);
            imageList.remove(imagePath);

        }catch (IndexOutOfBoundsException e1){
        //    imagesToBePosted.remove(index);
            imageList.remove(imagePath);

        }
        catch (Exception e){
            e.printStackTrace();
        }


        if (selectedImageBitmaps.isEmpty() && imageList.isEmpty()) {
            llSelectedImagesContainer.setVisibility(View.GONE);
        }
        Log.i(TAG, "SELECTED IMAGE BITMAP SIZE: " + selectedImageBitmaps.size());
        Log.i(TAG, "imageList SIZE: " + imageList.size());
    }

    @Override
    public void onRemoveAllImages() {
        selectedImageBitmaps.clear();
        imagesToBePosted.clear();
        imageList.clear();
        llSelectedImagesContainer.setVisibility(View.GONE);
    }

    private void setupCheckinTextAndTagText() {
        if (isCheckin || taggedFriendNames.size() > 0) {
            tvUserFullName.setText(user.getFullName() + " - ");
        }

        if (feeling_Name != null) {
            if (!feeling_Name.isEmpty()) {
                tvFeeling.setVisibility(View.VISIBLE);
                tvFeelingName.setVisibility(View.VISIBLE);
                tvFeelingName.setText(feeling_Name);
            } else {
                tvFeeling.setVisibility(View.GONE);
                tvFeelingName.setVisibility(View.GONE);
                tvFeelingName.setText("");
            }
        } else {
            tvFeeling.setVisibility(View.GONE);
            tvFeelingName.setVisibility(View.GONE);
            tvFeelingName.setText("");
        }

        final TextView tvWith = (TextView) findViewById(R.id.tv_with);
        final TextView tvSingleTaggedFriendName = (TextView) findViewById(R.id.tv_singleTaggedFriendName);
        final TextView tvAnd = (TextView) findViewById(R.id.tv_and);
        final TextView tvOtherTaggedFriends = (TextView) findViewById(R.id.tv_otherTaggedFriends);
        final TextView tvAt = (TextView) findViewById(R.id.tv_at);
        final TextView tvCheckinPlace = (TextView) findViewById(R.id.tv_checkinPlace);

        if (isCheckin) {
            tvAt.setVisibility(View.VISIBLE);
            tvCheckinPlace.setVisibility(View.VISIBLE);
            tvCheckinPlace.setText(checkinPlace);
        } else {
            tvAt.setVisibility(View.GONE);
            tvCheckinPlace.setVisibility(View.GONE);
            tvCheckinPlace.setText("");
        }

        if (taggedFriendNames.size() != 0) {
            if (taggedFriendNames.size() == 1) {
                tvWith.setVisibility(View.VISIBLE);
                tvSingleTaggedFriendName.setVisibility(View.VISIBLE);
                tvSingleTaggedFriendName.setText(taggedFriendNames.get(0));
                tvAnd.setVisibility(View.GONE);
                tvOtherTaggedFriends.setVisibility(View.GONE);
            } else if (taggedFriendNames.size() == 2) {
                tvWith.setVisibility(View.VISIBLE);
                tvSingleTaggedFriendName.setVisibility(View.VISIBLE);
                tvSingleTaggedFriendName.setText(taggedFriendNames.get(0));
                tvAnd.setVisibility(View.VISIBLE);
                tvOtherTaggedFriends.setVisibility(View.VISIBLE);
                tvOtherTaggedFriends.setText(taggedFriendNames.get(1));
            } else if (taggedFriendNames.size() > 2) {
                tvWith.setVisibility(View.VISIBLE);
                tvSingleTaggedFriendName.setVisibility(View.VISIBLE);
                tvSingleTaggedFriendName.setText(taggedFriendNames.get(0));
                tvAnd.setVisibility(View.VISIBLE);
                tvOtherTaggedFriends.setVisibility(View.VISIBLE);
                final int count = taggedFriendNames.size() - 1;
                tvOtherTaggedFriends.setText(count + " Other");
            }
        } else {
            tvWith.setVisibility(View.GONE);
            tvSingleTaggedFriendName.setVisibility(View.GONE);
            tvAnd.setVisibility(View.GONE);
            tvOtherTaggedFriends.setVisibility(View.GONE);
            tvUserFullName.setText(user.getFullName());
        }

    }

    public void checkIfPostInProgress() {
        if (etStatusMessage.getText().toString().trim().length() > 0 ||
                isCheckin || taggedFriendIds.size() > 0 || imageList.size() > 0) {

            new DialogDiscardPost(this, this).show();
            /**new AlertDialog.Builder(this)
             .setTitle("Discard post?")
             .setMessage("Are you sure you want to discard this post?")
             .setPositiveButton("DISCARD POST", new DialogInterface.OnClickListener()
             {
             @Override public void onClick(DialogInterface dialogInterface, int i)
             {
             ActivityPostToAgrizip.this.finish();
             }
             })
             .setNegativeButton("CANCEL", new DialogInterface.OnClickListener()
             {
             @Override public void onClick(DialogInterface dialogInterface, int i)
             {
             dialogInterface.dismiss();
             }
             })
             .show();*/

        } else {
            finish();
        }
    }

    private void preparePrivacyAdapter() {
        if (App.getInstance().getUser().getUserPrivacyDetails() == null) {
            App.getInstance().getPrivacyDetails(new App.GetPrivacyListListener() {
                @Override
                public void onGetPrivacyList() {
                    preparePrivacyAdapter();
                }
            });
            return;
        }

        final List<UserPrivacyDetails.DataBean> privacyList =
                App.getInstance().getUser().getUserPrivacyDetails().getData();

        tvPostPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());
        post.setVisibleTo(privacyList.get(privacyList.size() - 1).getId());

        privacyAdapter
                = new ArrayAdapter<UserPrivacyDetails.DataBean>(this, android.R.layout.simple_list_item_1, privacyList) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(ActivityPostToAgrizip.this).inflate(
                            android.R.layout.simple_list_item_1, parent, false);
                }
                TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

                tv.setText(privacyList.get(position).getUserTypeName());

                return convertView;

            }
        };

    }

    @OnClick(R.id.tv_postPrivacy)
    public void promptUserForSelectingPostPrivacy() {
        if (App.getInstance().getUser().getUserPrivacyDetails() == null) {
            return;
        }
        LayoutInflater inflater = this.getLayoutInflater();
        View titleView = inflater.inflate(R.layout.custom_dialog, null);
        promptPrivacyDialog = new AlertDialog.Builder(this)
                .setTitle("Select Post Privacy")
                .setCustomTitle(titleView)
                .setSingleChoiceItems(privacyAdapter, 0, this)
                .show();

    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        tvPostPrivacy.setText(privacyAdapter.getItem(i).getUserTypeName());
        post.setVisibleTo(privacyAdapter.getItem(i).getId());
        if (promptPrivacyDialog.isShowing()) {
            promptPrivacyDialog.dismiss();
        }
    }

    private void setDetailsIntoView() {
        etNeedHelpTitle.setText(postToBeEdited.getHelpTitle());
        etStatusMessage.setText(postToBeEdited.getStatus());

        feeling_Name = postToBeEdited.getFeelingName();
        if (postToBeEdited.getFeelingValue().length() > 0)
            feelingIconId = Integer.parseInt(postToBeEdited.getFeelingValue());

        isCheckin = postToBeEdited.isIsCheckIn();
        if (isCheckin) {
            checkinPlace = postToBeEdited.getPlace();
        }
        if (postToBeEdited.getTagDetails() != null) {
            for (SinglePost.TagDetails td : postToBeEdited.getTagDetails()) {
                taggedFriendIds.add(td.getTagFriendId());
                taggedFriendNames.add(td.getFullName());
            }
        }

        setupCheckinTextAndTagText();
    }


    public void getPostForEdit() {
        Utils.showProgressDialog(this, "Please Wait...", "Getting post...", true, false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
            }
        });

        API.getInstance().getStatusPostById(editPostId, App.getInstance().getUser().getId(), onGetPostDetailsCallback);
    }

    @Override
    public void finish() {
        Utils.hideProgressDialog();
        super.finish();
    }

    @OnClick(R.id.tv_feelingName)
    void openFeelingsActivity() {
        Intent feelingActivity = new Intent(ActivityPostToAgrizip.this, Feeling_Activity.class);
        startActivityForResult(feelingActivity, 123);
    }

}
