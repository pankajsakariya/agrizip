package com.app.agrizip.post.post_simple;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.agrizip.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by S-PC on 10/03/2017.
 */

public class Feeling_Adapter extends RecyclerView.Adapter<Feeling_Adapter.ViewHolder> {

    List<Feeling_Model> feelingLists=new ArrayList<>();


    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView thumbnail,iv_backgroundImage;
        public TextView feelingname;
        public ViewHolder(View view) {
            super(view);
            iv_backgroundImage=(ImageView)view.findViewById(R.id.iv_backgroundImage);
            thumbnail = (ImageView) view.findViewById(R.id.iv_thumbnail);
            feelingname=(TextView)view.findViewById(R.id.tv_feelingname);

        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewcard= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feeling_cardview,parent,false);
        return new ViewHolder(viewcard);
    }
    public Feeling_Adapter(List<Feeling_Model> feelingLists)
    {
        this.feelingLists=feelingLists;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Feeling_Model FeelingModel=feelingLists.get(position);
        holder.thumbnail.setImageResource(FeelingModel.getIcon());
        holder.feelingname.setText(FeelingModel.getFeelingName());

    }

    @Override
    public int getItemCount() {
        return feelingLists.size();
    }
}
