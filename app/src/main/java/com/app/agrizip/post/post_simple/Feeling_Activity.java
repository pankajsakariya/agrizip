package com.app.agrizip.post.post_simple;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.agrizip.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Feeling_Activity extends AppCompatActivity
{
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;
    Feeling_Model feelingModel;
    boolean feelingSelected = false;
    private List<Feeling_Model> feeling_lists = new ArrayList<>();
    private RecyclerView feeling_recyclerView;
    private Feeling_Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feelingscreen);
        ButterKnife.bind(this);
        if (mToolbar != null)
        {
            setSupportActionBar(mToolbar);

            if (tvToolbarTitle != null)
            {
                tvToolbarTitle.setText("Feeling");
            }

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        feeling_recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        final RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        mAdapter = new Feeling_Adapter(feeling_lists);

        feeling_recyclerView.setLayoutManager(mLayoutManager);

        feeling_recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        feeling_recyclerView.setItemAnimator(new DefaultItemAnimator());
        feeling_recyclerView.setAdapter(mAdapter);

        setFeelingIcon();

        feeling_recyclerView.addOnItemTouchListener(new ItemTouchEvent(getApplicationContext(), feeling_recyclerView, new ItemTouchEvent.ClickListener()
        {
            @Override
            public void onClick(View view, int position)
            {
                feelingSelected = true;
                feelingModel = feeling_lists.get(position);
                Intent PostToAgrizip = new Intent(Feeling_Activity.this, ActivityPostToAgrizip.class);
                PostToAgrizip.putExtra("feelingIcon", feelingModel.getIcon());
                PostToAgrizip.putExtra("feelingName", feelingModel.getFeelingName());
                setResult(RESULT_OK, PostToAgrizip);
                finish();
            }

            @Override
            public void onLongClick(View view, int position)
            {

            }
        }));
    }

    private void setFeelingIcon()
    {


        Feeling_Model Flists = new Feeling_Model(R.drawable.emoji_1f600, "Happy");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f602, "Very Happy ");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f634, "Hopeless");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f621, "Very Angry");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f624, "Angry");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f616, "Distress");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f60a, "Joyful");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f60c, "Shy");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f60d, "In love");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f60e, "Cool");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f60f, "Naughty");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f61c, "Crazy");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f62a, "Lazy");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f62d, "Crying");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f62e, "Shock");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f637, "Contagious");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f607, "Praise");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f613, "Exhausted");
        feeling_lists.add(Flists);
        Flists = new Feeling_Model(R.drawable.emoji_1f629, "Hurt");
        feeling_lists.add(Flists);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void finish()
    {
        if (!feelingSelected)
        {
            setResult(RESULT_CANCELED, null);
        }
        super.finish();
    }

    private int dpToPx(int dp)
    {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration
    {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge)
        {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
        {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge)
            {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount)
                { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else
            {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount)
                {
                    outRect.top = spacing; // item top
                }
            }
        }

    }
}
