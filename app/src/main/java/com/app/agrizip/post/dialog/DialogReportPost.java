package com.app.agrizip.post.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.ReportPost;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/3/2017 at 1:53 PM.
 */

public class DialogReportPost extends Dialog implements Validator.ValidationListener
{

    String postId;

    @NotEmpty
    @BindView(R.id.et_reportPostTitle)
    EditText etReportPostTitle;

    @NotEmpty
    @BindView(R.id.et_reportPostDescription)
    EditText etReportPostDescription;

    @BindView(R.id.btn_dialogSave)
    Button btnSave;

    @BindView(R.id.btn_dialogCancel)
    Button btnCancel;

    Validator validator;

    public DialogReportPost(Context context, String postId)
    {
        super(context);
        this.postId = postId;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_report_post);


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                validator.validate();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

    }


    private void promptUserForReportPost()
    {
        new AlertDialog.Builder(getContext())
                .setTitle("Report Post")
                .setMessage("Are you sure you want to report this post?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Report", new DialogInterface.OnClickListener()
                {

                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        reportPost();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.dismiss();
                    }
                }).show();
    }


    private void reportPost()
    {
        final RetrofitCallbacks<ResponseBody> onReportPostCallback =
                new RetrofitCallbacks<ResponseBody>(getContext())
                {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
                    {
                        super.onResponse(call, response);
                        if (response.isSuccessful())
                        {
                            Toast.makeText(getContext(), "Report sent successfully", Toast.LENGTH_SHORT).show();
                            dismiss();
                        } else
                        {
                            Toast.makeText(getContext(), "something went wrong try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        final ReportPost reportPost = new ReportPost();
        reportPost.setPostId(postId);
        reportPost.setCreatedate(Calendar.getInstance().getTimeInMillis() + "");
        reportPost.setIsPostRemove(false);
        reportPost.setUserId(App.getInstance().getUser().getId());
        reportPost.setReportTitle(etReportPostTitle.getText().toString());
        reportPost.setReportDetail(etReportPostDescription.getText().toString());

        API.getInstance().reportFeedPost(reportPost, onReportPostCallback);
    }

    @Override
    public void onValidationSucceeded()
    {
        promptUserForReportPost();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors)
    {
        for (ValidationError error : errors)
        {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText)
            {
                ((EditText) view).setError(message);
            } else
            {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
