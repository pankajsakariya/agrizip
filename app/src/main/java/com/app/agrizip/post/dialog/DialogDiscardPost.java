package com.app.agrizip.post.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.app.agrizip.R;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 3/2/2017 at 9:10 PM.
 */

public class DialogDiscardPost extends Dialog {

    ActivityPostToAgrizip postActivity;

    @BindView(R.id.btn_dialogCancel)
    Button btnDialogCancel;

    @BindView(R.id.btn_dialogDiscardPost)
    Button btnDialogDiscardPost;

    public DialogDiscardPost(Context context, ActivityPostToAgrizip postActivity) {
        super(context);
        this.postActivity = postActivity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_discard_post);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        ButterKnife.bind(this);

        btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnDialogDiscardPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                postActivity.finish();
            }
        });

    }
}
