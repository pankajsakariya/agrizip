package com.app.agrizip.post.post_like_list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apimodels.LikeListResponse;
import viewholders.VHSIngleLikeListItem;

/**
 * Created by rutvik on 2/11/2017 at 6:10 PM.
 */

public class LikeListAdapter extends RecyclerView.Adapter
{

    private final Context context;

    private final List<LikeListResponse.DataBean> dataBeanList;

    public LikeListAdapter(Context context)
    {
        this.context = context;
        this.dataBeanList = new ArrayList<>();
    }


    public void addLikeListItem(LikeListResponse.DataBean likeListItem)
    {
        dataBeanList.add(likeListItem);
        notifyItemInserted(dataBeanList.size());
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSIngleLikeListItem.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSIngleLikeListItem.bind((VHSIngleLikeListItem) holder, dataBeanList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return dataBeanList.size();
    }
}
