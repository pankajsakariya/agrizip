package com.app.agrizip.post.post_comments.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.post.post_comments.adapter.CommentsAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.IOException;
import java.util.Calendar;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 12/16/2016 at 11:56 AM.
 */

public class FragmentComments extends Fragment
{

    public static final int REQUEST_CODE_PICKER = 3422;
    private static final String TAG = App.APP_TAG + FragmentComments.class.getSimpleName();
    final CommentOnPost newComment = new CommentOnPost();
    @BindView(R.id.iv_addImageToNewComment)
    ImageView ivAddImageToNewComment;
    @BindView(R.id.iv_postNewComment)
    ImageView ivPostNewComment;
    @BindView(R.id.et_newCommentBody)
    EditText etNewCommentBody;
    @BindView(R.id.tv_commentCount)
    TextView tvCommentCount;
    @BindView(R.id.rv_comments)
    RecyclerView rvComments;
    CommentsAdapter adapter;
    final RetrofitCallbacks<ResponseBody> onPostComment = new RetrofitCallbacks<ResponseBody>(getActivity())
    {

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
        {
            super.onResponse(call, response);
            if (response.isSuccessful())
            {
                try
                {
                    final JsonElement jsonElement;
                    jsonElement = new Gson().fromJson(response.body().string(), JsonElement.class);
                    JsonObject root = jsonElement.getAsJsonObject();
                    CommentsOnPost.DataBean newComment = new Gson().fromJson(root.getAsJsonObject("Data"), CommentsOnPost.DataBean.class);
                    newComment.setUserImage(App.getInstance().getUser().getUserImage());
                    newComment.setFullName(App.getInstance().getUser().getFullName());
                    newComment.setCreateDate(Utils.convertMillsToDate(Calendar.getInstance().getTimeInMillis()));
                    adapter.addCommentModel(newComment);
                    etNewCommentBody.setText("");
                    Toast.makeText(getActivity(), "Comment posted successfully", Toast.LENGTH_SHORT).show();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
    };
    String commentImage = "";
    String postId;

    public static FragmentComments newInstance(String postId)
    {
        FragmentComments fragment = new FragmentComments();
        fragment.postId = postId;
        Bundle b = new Bundle();
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_comments, container, false);

        ButterKnife.bind(this, rootView);

        rvComments.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvComments.setHasFixedSize(true);

        rvComments.setItemAnimator(new DefaultItemAnimator());

        adapter = new CommentsAdapter(getActivity());

        rvComments.setAdapter(adapter);

        getCommentsOnThisPost();

        return rootView;
    }

    private void getCommentsOnThisPost()
    {

        RetrofitCallbacks<CommentsOnPost> onGetCommentList = new RetrofitCallbacks<CommentsOnPost>(getActivity())
        {

            @Override
            public void onResponse(Call<CommentsOnPost> call, Response<CommentsOnPost> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    adapter.clear();
                    for (CommentsOnPost.DataBean singleComment : response.body().getData())
                    {
                        adapter.addCommentModel(singleComment);
                    }
                    tvCommentCount.setText(adapter.getItemCount() + "");
                }
            }
        };

        API.getInstance().getCommentListOfPost(postId, App.getInstance().getUser().getId(),onGetCommentList);

    }

    @OnClick(R.id.iv_addImageToNewComment)
    public void postNewImageOnComment()
    {
        ImagePicker.create(getActivity())
                .single() // single mode
                .showCamera(true) // show camera or not (true by default)
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    @OnClick(R.id.iv_postNewComment)
    public void postingNewComment()
    {
        if (etNewCommentBody.getText().toString().trim().isEmpty())
        {
            Toast.makeText(getActivity(), "Please enter comment!", Toast.LENGTH_SHORT).show();
            return;
        }

        newComment.setComment(etNewCommentBody.getText().toString());
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(postId);
        newComment.setImagestr("");

        API.getInstance().commentOnPost(newComment, onPostComment);

    }

    public void postImageAsComment(Image image)
    {
        Log.i(TAG, "Posting image as comment");
        newComment.setComment(etNewCommentBody.getText().toString());
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(postId);
        newComment.setImagestr(Utils.convertImageFileToBase64(image.getPath()));

        API.getInstance().commentOnPost(newComment, onPostComment);
    }

}


