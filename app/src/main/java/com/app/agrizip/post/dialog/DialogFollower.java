package com.app.agrizip.post.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.followers.activity.ActivityFollowers;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.io.IOException;
import java.util.Calendar;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FollowersList;
import apimodels.FriendList;
import apimodels.UnfollowUser;
import apimodels.UserProfileDetailsInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/27/2017 at 3:10 PM.
 */

public class DialogFollower extends Dialog {

    private static final String TAG = App.APP_TAG + DialogFollower.class.getSimpleName();
    final String postId;
    @BindView(R.id.btn_unfollow)
    Button btnPostHideDeleteSave;
    @BindView(R.id.btn_cancle)
    Button btnPostHideDeleteCancel;
    Purpose purpose;
    FollowersList.DataBean singleFollower;
    @BindView(R.id.iv_feedUserPic)
    PorterShapeImageView iv_feedUserPic;
    @BindView(R.id.tv_name)
    TextView tv_name;
    int position =0;
    Context context;

    public DialogFollower(Context context, final String postId, Purpose purpose,FollowersList.DataBean singleFollower,int position) {
        super(context);
        this.singleFollower = singleFollower;
        this.postId = postId;
        this.purpose = purpose;
        this.position = position;
        this.context = context;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.custom_dialog_follower);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes((WindowManager.LayoutParams) params);
        ButterKnife.bind(this);
        tv_name.setText(singleFollower.getFullName());
        Glide.with(getContext())
                .load(singleFollower.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(iv_feedUserPic);

    }

    @OnClick(R.id.btn_unfollow)
    public void performApiCall() {
      //  Toast.makeText(getContext(),"Unfollow",Toast.LENGTH_LONG).show();
        unfollowUser();
        dismiss();

    }

    @OnClick(R.id.btn_cancle)
    public void dismissThisDialogBox() {
      // Toast.makeText(getContext(),"Cancel",Toast.LENGTH_LONG).show();
        dismiss();
    }

    private void deletePost() {
        API.getInstance().deletePost(postId, new RetrofitCallbacks<ResponseBody>(getContext()) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    dismiss();
                    App.getInstance().getUser().getUserEventCallbacks().onStatusDeleted(postId);
                    try {
                        Log.i(TAG, "RESPONSE: " + response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Log.i(TAG, "RESPONSE: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        });
    }

    private void hidePost() {
        final RetrofitCallbacks<ResponseBody> onHidePostCallback =
                new RetrofitCallbacks<ResponseBody>(getContext()) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            App.getInstance().getUser().getUserEventCallbacks().onStatusDeleted(postId);
                            dismissThisDialogBox();
                            Toast.makeText(getContext(), "Post was made hidden", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().hideFeedPost(App.getInstance().getUser().getId(),
                postId, onHidePostCallback);
    }


    private void unfollowUser() {
       /* final RetrofitCallbacks<ResponseBody> onUnfollow = new RetrofitCallbacks<ResponseBody>(getContext()) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
               if(response.isSuccessful()){
                    if (singleFollower.getFriendunfollowActionListener() != null) {
                        singleFollower.getFriendunfollowActionListener().onUnfollow(singleFollower);
                    }
                }
                 else {
                    Toast.makeText(getContext(), "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        };

        final UnfollowUser unfollowUser = new UnfollowUser();
        unfollowUser.setIsFollow(false);
        unfollowUser.setFollowUserId(singleFollower.getFollowUserId());
        unfollowUser.setUserId(App.getInstance().getUser().getId());
        unfollowUser.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        API.getInstance().unfollowUser(unfollowUser, onUnfollow);
*/
        final UserProfileDetailsInfo userProfileDetailsInfo = new UserProfileDetailsInfo();

        final RetrofitCallbacks<ResponseBody> onUnfollow = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    System.out.println("Hello");
                    if (singleFollower.getFriendunfollowActionListener() != null) {
                        singleFollower.getFriendunfollowActionListener().onUnfollow(singleFollower,position);
                        Constants.UPDATE_FLLOWCOUNT = true;
                       // userProfileDetailsInfo.getOnUpdateFolowerCount().onChangeFollowerCount(5, ActivityFollowers.FlowCountpos);
                      //  btnSingleFriendExtraOptions.setText("Follow");
                    }
                } else {
                    Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                super.onFailure(call, t);
                System.out.println("Error " + t.toString());
            }
        };

        final UnfollowUser unfollowUser = new UnfollowUser();
        unfollowUser.setIsFollow(false);
        unfollowUser.setFollowUserId(singleFollower.getFollowUserId());
        unfollowUser.setUserId(App.getInstance().getUser().getId());
        unfollowUser.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        API.getInstance().unfollowUser(unfollowUser, onUnfollow);

    }

    public static enum Purpose {
        HIDE_POST(0);

        int value;

        Purpose(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }



}
