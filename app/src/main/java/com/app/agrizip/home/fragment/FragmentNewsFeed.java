package com.app.agrizip.home.fragment;

import android.app.Instrumentation;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.event.activity.ActivityCreateEvent;
import com.app.agrizip.event.activity.ActivityViewEvents;
import com.app.agrizip.home.adapter.MasterAdapter;
import com.app.agrizip.need_help.activity.ActivityNeedJob;
import com.app.agrizip.post.dialog.DialogNeedHelp;
import com.app.agrizip.post.post_comments.dailog.CommentsDialog;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;
import com.app.agrizip.profile.ActivityUserProfile;
import com.app.agrizip.profile.update_profile.UpdateProfileActivity;

import java.util.ArrayList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.SinglePost;
import apimodels.StatusPostList;
import apimodels.UserInfo;
import apimodels.UserProfileDetailsInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import fragments.BaseFragment;
import retrofit2.Call;
import retrofit2.Response;
import viewholders.VHSinglePost;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Rakshit on 18-11-2016 at 17:52.
 */

public class FragmentNewsFeed extends BaseFragment
        implements UserInfo.UserEventCallbacks, SwipeRefreshLayout.OnRefreshListener,
        SinglePost.PostCallbacksListener, DialogInterface.OnClickListener,CommentsDialog.CommentCountChangeListener,VHSinglePost.followUnfollowTextChange
{

    private static final String TAG = App.APP_TAG + FragmentNewsFeed.class.getSimpleName();
    final Handler mHandler = new Handler();
    public CommentsDialog commentsDialog;
    //List<SinglePost> singlePostList = new LinkedList<>();
    @BindView(R.id.fl_commentsFragmentContainer)
    FrameLayout flCommentsFragmentContainer;
    @BindView(R.id.rv_newsFeedView)
    RecyclerView rvNewsFeedView;
    @BindView(R.id.tv_needHelp)
    TextView txNeedHelp;
    @BindView(R.id.tv_event)
    TextView tvEvent;
   public static int pageNo = 1;

    public static boolean firstTime= false;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    LinearLayoutManager llm;
    //StaggeredGridLayoutManager llm;
    //final List<SinglePost> singlePostList = new LinkedList<>();
    public static MasterAdapter adapter;
    ProgressBar pb;
    SwipeRefreshLayout srlFeedPullToRefresh;
    RetrofitCallbacks userProfileDetailsInfoCallback;
    ArrayAdapter<String> promptHelpOptionsAdapter;
    AlertDialog promptHelpOptionsDialog;
    boolean busyLoadingData = false;
    String newStatusPostId;
    private boolean isShowingComments = false;
    private UserProfileDetailsInfo userProfileDetailsInfo = null;
    private OnPostPhotoUploadedSuccessfully onPostPhotoUploadedSuccessfully;

    public static boolean fromRefresh = false;
    public static FragmentNewsFeed newInstance(int index)
    {
        FragmentNewsFeed fragment = new FragmentNewsFeed();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_news_feed, container, false);

        ButterKnife.bind(this, rootView);

        rvNewsFeedView.setHasFixedSize(true);

        /*rvNewsFeedView.setItemViewCacheSize(20);
        rvNewsFeedView.setDrawingCacheEnabled(true);
        rvNewsFeedView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);*/

        pb = (ProgressBar) rootView.findViewById(R.id.pb_loadingFeeds);

        /**rvNewsFeedView.addOnScrollListener(new RecyclerView.OnScrollListener()
         {
         @Override public void onScrollStateChanged(RecyclerView recyclerView, int newState)
         {
         super.onScrollStateChanged(recyclerView, newState);
         final Picasso picasso = Picasso.with(getActivity());
         if (newState == RecyclerView.SCROLL_STATE_IDLE)
         {
         picasso.resumeTag("resume_tag");
         } else
         {
         picasso.pauseTag("resume_tag");
         }

         }
         });*/


        llm = new LinearLayoutManager(getActivity());

      //  llm = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);

        rvNewsFeedView.setLayoutManager(llm);

        rvNewsFeedView.setItemAnimator(new DefaultItemAnimator());

        rvNewsFeedView.addOnScrollListener(new OnScrollRecyclerView());

        adapter = new MasterAdapter(getActivity());

        rvNewsFeedView.setAdapter(adapter);

        try
        {
            App.getInstance().getUser().setUserEventCallbacks(this);

        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }

        VHSinglePost.mFollowtextchange = FragmentNewsFeed.this;
        App.getInstance().getUser().setShowNeedHelpInWhatsOnYourMind(false);
        adapter.addWhatsOnYourMind("whats_on_your_mind", ((App) getActivity().getApplication()).getUser());

        srlFeedPullToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_feedPullToRefresh);
        srlFeedPullToRefresh.setOnRefreshListener(this);
        srlFeedPullToRefresh.setEnabled(false);
        srlFeedPullToRefresh.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... voids)
            {
                if (userProfileDetailsInfo == null){

                    getUserProfileDetails();
                  //  getUserFeeds();
                }else{

                    getUserFeeds();
                }

                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        getUserFeeds();
        preparePromptHelpOptionsAdapter();

        onPostPhotoUploadedSuccessfully = new OnPostPhotoUploadedSuccessfully();
        try {
            getActivity().registerReceiver(onPostPhotoUploadedSuccessfully,
                    new IntentFilter(Constants.ON_POST_PHOTO_UPLOADED_SUCCESSFULLY));
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }


    private void getUserFeeds()


    {
        if(!fromRefresh){
            if(pageNo == 1) {
                pb.setVisibility(View.VISIBLE);
            }
        }


        busyLoadingData = true;

        API.getInstance().getStatusPostList(((App) getActivity().getApplication()).getUser().getId(),
                pageNo,
                new RetrofitCallbacks<StatusPostList>(getActivity())
                {

                    @Override
                    public void onResponse(Call<StatusPostList> call, Response<StatusPostList> response)
                    {
                        super.onResponse(call, response);
                        busyLoadingData = false;
                        pb.setVisibility(View.GONE);
                        UpdateProfileActivity.Update_profile_detail = false;
                        stopPullToRefresh();



                        try
                        {

                            if (response.isSuccessful())
                            {
                                for (SinglePost singlePost : response.body().getData())
                                {
                                    if (singlePost.getUserID().equals(App.getInstance().getUser().getId()))
                                    {
                                        singlePost.setUserProfileDetailsInfo(userProfileDetailsInfo.getData());
                                    }
                                    singlePost.setPostCallbacksListener(FragmentNewsFeed.this);

                                    adapter.addSinglePost(singlePost.getStatusId(), singlePost);
                                }
                                fromRefresh = false;
                                adapter.removeProgressBar();
                            }
                        } catch (NullPointerException e)
                        {
                            e.printStackTrace();
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<StatusPostList> call, Throwable t)
                    {
                        super.onFailure(call, t);
                        busyLoadingData = false;
                        pb.setVisibility(View.GONE);
                        stopPullToRefresh();
                    }
                });


    }



    @Override
    public void onStatusPostSuccessful(SinglePost singlePost)
    {

        singlePost.setPostCallbacksListener(this);
        singlePost.setUserProfileDetailsInfo(userProfileDetailsInfo.getData());
        adapter.addNewSinglePost(singlePost.getStatusId(), singlePost);
        mHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                adapter.notifyItemInserted(1);
            }
        });
    }

    @Override
    public void onStatusPostEdit(SinglePost singlePost) {

        singlePost.setPostCallbacksListener(this);
        singlePost.setUserProfileDetailsInfo(userProfileDetailsInfo.getData());
        adapter.replaceSinglePost(singlePost.getStatusId(), singlePost);
        mHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                adapter.notifyDataSetChanged();
            }
        });



    }

    @Override
    public void onStatusDeleted(String statusId)
    {
        adapter.removePostById(statusId);
    }

    @Override
    public void onRefresh()
    {
        pageNo = 1;
        fromRefresh = true;
        MasterAdapter.clearFlag = true;
        /*
        adapter.clearAllPost();
        adapter.notifyDataSetChanged();

        adapter.addWhatsOnYourMind("whats_on_your_mind", ((App) getActivity().getApplication()).getUser());
        */
        new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... voids)
            {
                if (userProfileDetailsInfo == null)
                {

                    getUserProfileDetails();
                } else
                {

                  //  getUserFeeds();
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //setAdapter();
        getUserFeeds();
    }

    private void stopPullToRefresh()
    {
        srlFeedPullToRefresh.setEnabled(true);

        if (srlFeedPullToRefresh.isRefreshing())
        {
            srlFeedPullToRefresh.setRefreshing(false);
        }
    }

    private void getUserProfileDetails()
    {

        userProfileDetailsInfoCallback = new RetrofitCallbacks<UserProfileDetailsInfo>(getActivity())
        {

            @Override
            public void onResponse(Call<UserProfileDetailsInfo> call, Response<UserProfileDetailsInfo> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {

                    FragmentNewsFeed.this.userProfileDetailsInfo = response.body();

                    if (userProfileDetailsInfo != null)

                    {
                       // getUserFeeds();
                    }

                } else
                {
                    //pb.setVisibility(View.GONE);
                    stopPullToRefresh();
                }
            }

            @Override
            public void onFailure(Call<UserProfileDetailsInfo> call, Throwable t)
            {
                super.onFailure(call, t);
                pb.setVisibility(View.GONE);
                stopPullToRefresh();
            }
        };

        API.getInstance().getUserProfileInfo(App.getInstance().getUser().getId(), userProfileDetailsInfoCallback);

    }

    @Override
    public void onClickComments(String postId,int count)
    {
        isShowingComments = true;
        commentsDialog = new CommentsDialog(getActivity(), postId, String.valueOf(count));
        commentsDialog.mListenerCount = this;
        commentsDialog.show();

        /**flCommentsFragmentContainer.setVisibility(View.VISIBLE);
         fragmentComments = FragmentComments.newInstance(postId);
         getActivity()
         .getSupportFragmentManager()
         .beginTransaction()
         .add(R.id.fl_commentsFragmentContainer, fragmentComments).commit();*/
    }

    public boolean isShowingComments()
    {
        return isShowingComments;
    }

    public void hideCommentsFragment()
    {
        isShowingComments = false;
        commentsDialog.dismiss();
        /*flCommentsFragmentContainer.setVisibility(View.GONE);
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .remove(fragmentComments).commit();*/
    }


    private void preparePromptHelpOptionsAdapter()
    {
        final List<String> helpOptionList = new ArrayList<>();

        helpOptionList.add("Advise");
        helpOptionList.add("Buy");
        helpOptionList.add("Sale");
        helpOptionList.add("Job");

        promptHelpOptionsAdapter
                = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, helpOptionList)
        {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                if (convertView == null)
                {
                    convertView = LayoutInflater.from(getActivity()).inflate(
                            android.R.layout.simple_list_item_1, parent, false);
                }
                TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

                tv.setText(helpOptionList.get(position));

                return convertView;

            }
        };

    }


    @OnClick(R.id.tv_needHelp)
    public void promptHelpOptions()
    {

        if(adapter.getItemCount() > 1) {
            final DialogNeedHelp needHelp = new DialogNeedHelp(getActivity(), 0);
            needHelp.show();
        }else{
            Toast.makeText(getActivity(),"Please Wait NewsFeed is Loading. ",Toast.LENGTH_LONG).show();

        }

        /**promptHelpOptionsDialog = new AlertDialog.Builder(getActivity())
         .setTitle("Select Help Type")
         .setSingleChoiceItems(promptHelpOptionsAdapter, 0, this)
         .show();*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 444){
            int commentcount = data.getIntExtra("COMMENT_COUNT",0);
            int likecount =data.getIntExtra("LIKE_COUNT",0);
            int sharecount = data.getIntExtra("SHARE_COUNT",0);
            System.out.println("Hello");
            adapter.updateModel(likecount,commentcount,sharecount,VHSinglePost.temp_comment_count);
           // adapter.changeCommentcount(commentcount,VHSinglePost.temp_comment_count);

        }
    }



    @Override
    public void onClick(DialogInterface dialogInterface, int selectedIndex)
    {

        App.getInstance().getUser().setShowNeedHelpInWhatsOnYourMind(false);
        Intent intent = new Intent(getActivity(), ActivityPostToAgrizip.class);
        intent.putExtra(Constants.NEED_HELP, true);

        dialogInterface.dismiss();

        switch (selectedIndex)
        {

            case 0:
                intent.putExtra(Constants.HELP_TYPE, Constants.ADVISE);
                startActivity(intent);
                break;

            case 1:
                intent.putExtra(Constants.HELP_TYPE, Constants.BUY);
                startActivity(intent);
                break;

            case 2:
                intent.putExtra(Constants.HELP_TYPE, Constants.SELL);
                startActivity(intent);
                break;

            case 3:

                dialogInterface.dismiss();
                final List<String> jobType = new ArrayList<>();

                jobType.add("I WANT JOB");
                jobType.add("I WANT EMPLOYEE");

                new AlertDialog.Builder(getActivity())
                        .setTitle("Job")
                        .setSingleChoiceItems(new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_list_item_1, jobType), 0, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int i)
                            {
                                final Intent intent = new Intent(getActivity(), ActivityNeedJob.class);
                                if (i == 0)
                                {
                                    intent.putExtra(Constants.NEED_JOB, true);
                                } else if (i == 1)
                                {
                                    intent.putExtra(Constants.NEED_JOB, false);
                                }
                                dialog.dismiss();
                                startActivity(intent);
                            }
                        })
                        .show();
                break;

        }
    }

    @OnClick(R.id.tv_event)
    public void showEvents()
    {
        if(adapter.getItemCount() > 1){
        //startActivity(new Intent(getActivity(), ActivityViewEvents.class));
        startActivity(new Intent(getActivity(), ActivityCreateEvent.class));
        }else{
            Toast.makeText(getActivity(),"Please Wait NewsFeed is Loading. ",Toast.LENGTH_LONG).show();

        }
    }

    public void unregisterReceiver()
    {
        if (onPostPhotoUploadedSuccessfully != null)
        {
            getActivity().unregisterReceiver(onPostPhotoUploadedSuccessfully);
        }
    }

    @Override
    public void OnChangeCommentCount(int count,int position) {

        adapter.changeCommentcount(count,position);
    }

    @Override
    public void OnFollowTextChange(boolean isFollow,String UserId) {
        adapter.changeFollowText(isFollow,UserId);
    }


    class OnScrollRecyclerView extends RecyclerView.OnScrollListener
    {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            if (dy > 0)
            {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();

             //   int[] firstVisibleItems = null;
               // firstVisibleItems = llm.findFirstVisibleItemPositions(firstVisibleItems);
                /*if(firstVisibleItems != null && firstVisibleItems.length > 0) {
                    pastVisibleItems = firstVisibleItems[0];
                }*/

                if (!busyLoadingData)
                {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount)
                    {
                        pageNo++;
                        adapter.addProgressBar();
                        getUserFeeds();
                    }
                }
            }
        }
    }

    class OnPostPhotoUploadedSuccessfully extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String postId = intent.getStringExtra(Constants.POST_ID);
            if (postId != null)
            {
                mHandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        newStatusPostId = postId;
                        getUserFeeds();


                    }
                });
            }
        }
    }



}
