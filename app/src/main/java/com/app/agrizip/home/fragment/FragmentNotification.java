package com.app.agrizip.home.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.home.adapter.NotificationAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.NotificationList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Rakshit on 18-11-2016 at 17:53.
 */

public class FragmentNotification extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{

    @BindView(R.id.srl_refreshNotification)
    SwipeRefreshLayout srlRefreshNotification;

    @BindView(R.id.rv_notification)
    RecyclerView rvNotification;

    @BindView(R.id.fl_emptyPendingRequest)
    FrameLayout flEmptyPendingRequest;
    @BindView(R.id.pb_loadingNotification)
    ProgressBar pb;



    NotificationAdapter adapter;

    int pageNo = 1;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    LinearLayoutManager llm;
    boolean busyLoadingData = false;


    public static FragmentNotification newInstance(int index)
    {
        FragmentNotification fragment = new FragmentNotification();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);

        ButterKnife.bind(this, rootView);

        llm = new LinearLayoutManager(getActivity());

        rvNotification.setLayoutManager(llm);
        rvNotification.setHasFixedSize(true);

        rvNotification.addOnScrollListener(new OnScrollRecyclerView());

        adapter = new NotificationAdapter(getActivity());

        rvNotification.setAdapter(adapter);
        srlRefreshNotification.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshNotification.setOnRefreshListener(this);

       // getNotificationList();

        return rootView;
    }


    public void getNotificationList()

    {
        pb.setVisibility(View.VISIBLE);
        final RetrofitCallbacks<NotificationList> onGetUserNotifications =
                new RetrofitCallbacks<NotificationList>(getActivity())
                {
                    @Override
                    public void onResponse(Call<NotificationList> call, Response<NotificationList> response)
                    {

                        super.onResponse(call, response);
                        pb.setVisibility(View.GONE);
                        if (srlRefreshNotification.isRefreshing())
                        {
                            srlRefreshNotification.setRefreshing(false);
                        }

                        if (response.isSuccessful())
                        {
                            for (NotificationList.DataBean notificationItem : response.body().getData())
                            {

                                adapter.addNotificationItem(notificationItem);

                            }
                            if (adapter.getItemCount() > 0)
                            {
                                flEmptyPendingRequest.setVisibility(View.GONE);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<NotificationList> call, Throwable t)
                    {
                        super.onFailure(call, t);
                        pb.setVisibility(View.VISIBLE);
                        if (srlRefreshNotification.isRefreshing())
                        {
                            srlRefreshNotification.setRefreshing(false);
                        }
                    }
                };

        API.getInstance().getNotificationByUserId(App.getInstance().getUser().getId(), pageNo,
                onGetUserNotifications);
    }


    @Override
    public void onRefresh()
    {
        pageNo = 1;
        getNotificationList();
    }


    class OnScrollRecyclerView extends RecyclerView.OnScrollListener
    {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            if (dy > 0)
            {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (!busyLoadingData)
                {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount)
                    {
                        pageNo++;
                        getNotificationList();
                    }
                }
            }
        }
    }

}
