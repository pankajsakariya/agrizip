package com.app.agrizip.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.agrizip.app.App;
import com.app.agrizip.home.fragment.FragmentNewsFeed;

import org.apache.commons.collections4.map.ListOrderedMap;

import java.util.LinkedList;
import java.util.List;

import apimodels.SinglePost;
import apimodels.UserInfo;
import apimodels.UserProfileDetailsInfo;
import components.RecyclerViewComponent;
import viewholders.VHBigProfileViewWithDetails;
import viewholders.VHSinglePost;
import viewholders.VHSingleProfileDetail;
import viewholders.VHSingleProgressBar;
import viewholders.VHSingleSaveProfileButton;
import viewholders.VHUserProfileExtraStrip;
import viewholders.VHWhatsOnYourMind;
import viewmodels.SingleProfileDetailModel;

import static components.RecyclerViewComponent.BIG_PROFILE_PIC_WITH_DETAILS;
import static components.RecyclerViewComponent.LOADING_PROGRESS_BAR;
import static components.RecyclerViewComponent.SAVE_USER_PROFILE_DETAILS_BUTTON;
import static components.RecyclerViewComponent.SINGLE_POST;
import static components.RecyclerViewComponent.SINGLE_PROFILE_DETAIL;
import static components.RecyclerViewComponent.USER_PROFILE_EXTRA_STRIP;
import static components.RecyclerViewComponent.WHATS_ON_YOUR_MIND;

/**
 * Created by rutvik on 11/22/2016 at 10:58 AM.
 */

public class MasterAdapter extends RecyclerView.Adapter {

    private static final String TAG = App.APP_TAG + MasterAdapter.class.getSimpleName();

    private final Context context;

    private final ListOrderedMap<String, RecyclerViewComponent> recyclerViewComponentList;

    private final List<String> idList;

    //private List<String> statusIds = new ArrayList<>();

    String recentlyUploadedPost;
    private boolean isShowingProgressBar;

   public static boolean clearFlag = true;

    public MasterAdapter(final Context context) {
        this.context = context;
        recyclerViewComponentList = new ListOrderedMap<>();
        idList = new LinkedList<>();
    }


    public void addBigProfileViewWithDetails(String id, UserProfileDetailsInfo.DataBean model) {
        idList.add(id);
        recyclerViewComponentList
                .put(id, new RecyclerViewComponent(BIG_PROFILE_PIC_WITH_DETAILS,
                        model));
        notifyItemInserted(recyclerViewComponentList.size());
    }

    public void addUserProfileExtraStrip(String id, UserProfileDetailsInfo.DataBean userInfo) {
        idList.add(id);
        recyclerViewComponentList
                .put(id, new RecyclerViewComponent(USER_PROFILE_EXTRA_STRIP,
                        userInfo));
        notifyItemInserted(recyclerViewComponentList.size());
    }


    public void addWhatsOnYourMind(String id, UserInfo.User user) {
        idList.add(id);
        recyclerViewComponentList
                .put(id, new RecyclerViewComponent(WHATS_ON_YOUR_MIND,
                        user));
        notifyItemInserted(recyclerViewComponentList.size());
    }

    public void updateModel(int likecount,int commentcount,int sharecount,int position){
        SinglePost objSinglePost =  (SinglePost) recyclerViewComponentList.get(idList.get(position)).getModel();
        objSinglePost.setLikeCounts(likecount);
        objSinglePost.setShareCounts(sharecount);
        objSinglePost.setCommentCounts(commentcount);
         notifyItemChanged(position);
    }
    public void changeLikeCount(int count,int position){
        SinglePost objSinglePost =  (SinglePost) recyclerViewComponentList.get(idList.get(position)).getModel();
        objSinglePost.setLikeCounts(count);
        notifyItemChanged(position);
    }
    public void changeShareCount(int count,int position){
        SinglePost objSinglePost =  (SinglePost) recyclerViewComponentList.get(idList.get(position)).getModel();
        objSinglePost.setShareCounts(count);
        notifyItemChanged(position);
    }

    public void changeCommentcount(int count,int position){
        SinglePost objSinglePost =  (SinglePost) recyclerViewComponentList.get(idList.get(position)).getModel();
        objSinglePost.setCommentCounts(count);
        notifyItemChanged(position);
    }

    public void changeFollowText(boolean isfollow,String UserId){
        for(int i=1;i<idList.size();i++){
            SinglePost objSinglePost =  (SinglePost) recyclerViewComponentList.get(idList.get(i)).getModel();
           if(UserId.equalsIgnoreCase(objSinglePost.getUserID())){
               objSinglePost.setFollow(isfollow);
               notifyItemChanged(i);
           }


        }
    }

    public void ChangeFollowerCout(int count, int position){
        UserProfileDetailsInfo.DataBean obj = (UserProfileDetailsInfo.DataBean)recyclerViewComponentList.get(idList.get(position)).getModel();
        obj.setFollowCount(String.valueOf(count));
        notifyItemChanged(position);

    }

    public void addSinglePost(String id, SinglePost singlePost) {
        boolean flag =false;

      /*  if (!idList.contains(id)) {
            if(FragmentNewsFeed.pageNo == 1 && FragmentNewsFeed.fromRefresh == true){
               // clear();

                idList.add(1,id);
                flag = true;

            }else{
                idList.add(id);

            }

        }
        recyclerViewComponentList.put(id, new RecyclerViewComponent(SINGLE_POST,
                singlePost));

        notifyItemInserted(recyclerViewComponentList.size());
        if(flag){
            notifyDataSetChanged();
        }


        Log.i(TAG, "idList SIZE: " + idList.size());
        Log.i(TAG, "recyclerViewComponentList SIZE: " + recyclerViewComponentList.size());

        if (recentlyUploadedPost != null) {
            notifyItemChanged(idList.indexOf(recentlyUploadedPost));
        }
        */


        //OLD LOGIC
        if(FragmentNewsFeed.pageNo == 1 && FragmentNewsFeed.fromRefresh == true){
            if(clearFlag) {
                clear();
                //     adapter.addWhatsOnYourMind("whats_on_your_mind", ((App) getActivity().getApplication()).getUser());

                addWhatsOnYourMind("whats_on_your_mind", ((App) context.getApplicationContext()).getUser());
                notifyDataSetChanged();
                clearFlag = false;
            }

        }
        //else {
            if (!idList.contains(id)) {
                idList.add(id);
            }
            recyclerViewComponentList.put(id, new RecyclerViewComponent(SINGLE_POST,
                    singlePost));

            notifyItemInserted(recyclerViewComponentList.size());

            Log.i(TAG, "idList SIZE: " + idList.size());
            Log.i(TAG, "recyclerViewComponentList SIZE: " + recyclerViewComponentList.size());

            if (recentlyUploadedPost != null) {
                notifyItemChanged(idList.indexOf(recentlyUploadedPost));
            }
       // }

    }

    public void addNewSinglePost(String id, SinglePost singlePost) {
        recentlyUploadedPost = id;

        idList.add(1, id);

        recyclerViewComponentList.put(1, id, new RecyclerViewComponent(SINGLE_POST,
                singlePost));

        /**final List<RecyclerViewComponent> lValue = recyclerViewComponentList.valueList();
         lValue.add(new RecyclerViewComponent(SINGLE_POST,
         singlePost));

         final List<String> lKeys = recyclerViewComponentList.keyList();
         lKeys.add(id);

         recyclerViewComponentList.key*/

        Log.i(TAG, "idList SIZE: " + idList.size());
        Log.i(TAG, "recyclerViewComponentList SIZE: " + recyclerViewComponentList.size());

        notifyItemInserted(idList.size());
        //addSinglePost();
    }

    public void replaceSinglePost(String id, SinglePost singlePost) {
        recentlyUploadedPost = id;


        if (recyclerViewComponentList.containsKey(id)) {
            recyclerViewComponentList.put(id, new RecyclerViewComponent(SINGLE_POST, singlePost));
        }




        Log.i(TAG,"idList SIZE: "+idList.size());
        Log.i(TAG,"recyclerViewComponentList SIZE: "+recyclerViewComponentList.size());

    notifyItemInserted(idList.size());
    //addSinglePost();
}

    public void removePostById(String statusId) {
        int index = idList.indexOf(statusId);
        idList.remove(statusId);
        recyclerViewComponentList.remove(statusId);
        notifyItemRemoved(index);
    }

    public void addSingleProfileDetail(String id, SingleProfileDetailModel singleProfileDetailModel) {


        idList.add(id);
        recyclerViewComponentList.put(id, new RecyclerViewComponent(RecyclerViewComponent.SINGLE_PROFILE_DETAIL,
                singleProfileDetailModel));
    }

    public void notifyChange(String id) {
        Log.i(TAG, "NOTIFYING ITEM WITH ID: " + id);
        Log.i(TAG, "DATA CHANGE AT INDEX: " + idList.indexOf(id));
        notifyItemChanged(idList.indexOf(id));
    }

    public void addSaveProfileDetailButton(String id, View.OnClickListener onClickListener) {
        idList.add(id);
        recyclerViewComponentList.put(id,
                new RecyclerViewComponent(RecyclerViewComponent.SAVE_USER_PROFILE_DETAILS_BUTTON,
                        onClickListener));
    }

    public void addProgressBar() {
        if (!isShowingProgressBar) {
            isShowingProgressBar = true;
            idList.add("progress_bar");
            recyclerViewComponentList.put("progress_bar",
                    new RecyclerViewComponent(RecyclerViewComponent.LOADING_PROGRESS_BAR, null));
            notifyItemInserted(idList.size());
        }
    }

    public void removeProgressBar() {
        if (isShowingProgressBar) {
            isShowingProgressBar = false;
            final int index = idList.indexOf("progress_bar");
            idList.remove("progress_bar");
            recyclerViewComponentList.remove("progress_bar");

            notifyItemRemoved(index);
        }

    }

    /*public void clearAllPost()
    {
        recyclerViewComponentList.clear();
    }*/

    @Override
    public int getItemViewType(int position) {
        return recyclerViewComponentList.get(idList.get(position)).getViewType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case BIG_PROFILE_PIC_WITH_DETAILS:
                return VHBigProfileViewWithDetails.create(context, parent);

            case USER_PROFILE_EXTRA_STRIP:
                return VHUserProfileExtraStrip.create(context, parent);

            case WHATS_ON_YOUR_MIND:
                return VHWhatsOnYourMind.create(context, parent);

            case SINGLE_POST:
                return VHSinglePost.create(context, parent);

            case SINGLE_PROFILE_DETAIL:
                return VHSingleProfileDetail.create(context, parent);

            case SAVE_USER_PROFILE_DETAILS_BUTTON:
                return VHSingleSaveProfileButton.create(context, parent);

            case LOADING_PROGRESS_BAR:
                return VHSingleProgressBar.create(context, parent);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case BIG_PROFILE_PIC_WITH_DETAILS:
                VHBigProfileViewWithDetails.bind((VHBigProfileViewWithDetails) holder,
                        (UserProfileDetailsInfo.DataBean) recyclerViewComponentList.get(idList.get(position)).getModel());
                break;

            case USER_PROFILE_EXTRA_STRIP:
                VHUserProfileExtraStrip.bind((VHUserProfileExtraStrip) holder,
                        (UserProfileDetailsInfo.DataBean)
                                recyclerViewComponentList.get(idList.get(position)).getModel());
                break;

            case WHATS_ON_YOUR_MIND:
                VHWhatsOnYourMind.bind((VHWhatsOnYourMind) holder,
                        (UserInfo.User) recyclerViewComponentList.get(idList.get(position)).getModel());
                break;

            case SINGLE_POST:
                VHSinglePost.bind((VHSinglePost) holder,
                        (SinglePost) recyclerViewComponentList.get(idList.get(position)).getModel());
                break;

            case SINGLE_PROFILE_DETAIL:
                VHSingleProfileDetail.bind((VHSingleProfileDetail) holder,
                        (SingleProfileDetailModel) recyclerViewComponentList
                                .get(idList.get(position)).getModel());
                break;

            case SAVE_USER_PROFILE_DETAILS_BUTTON:
                VHSingleSaveProfileButton.bind((VHSingleSaveProfileButton) holder,
                        (View.OnClickListener) recyclerViewComponentList.get(idList.get(position)).getModel());
                break;

            case LOADING_PROGRESS_BAR:
                VHSingleProgressBar.bind((VHSingleProgressBar) holder);
                break;

        }

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    public void notifyItem(String postId) {
        notifyItemChanged(idList.indexOf(postId));
        if (recentlyUploadedPost != null) {
            recentlyUploadedPost = null;
        }
    }

    public void clear() {
        idList.clear();
        recyclerViewComponentList.clear();

    }
}
