package com.app.agrizip.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apimodels.NotificationList;
import viewholders.VHNotificationItem;

/**
 * Created by rutvik on 2/22/2017 at 3:52 PM.
 */

public class NotificationAdapter extends RecyclerView.Adapter
{

    private final Context context;
    private final List<NotificationList.DataBean> notificationList;

    public NotificationAdapter(Context context)
    {
        this.context = context;
        notificationList = new ArrayList<>();
    }

    public void addNotificationItem(NotificationList.DataBean notificationItem)
    {
      if(!checkData(notificationItem)){
          notificationList.add(notificationItem);
          notifyItemInserted(notificationList.size());
      }

    }
    private boolean checkData(NotificationList.DataBean notificationItem){
       boolean Id_available = false;
        for(int i=0;i<notificationList.size();i++){
            if(notificationItem.getNotificationId() == notificationList.get(i).getNotificationId()){
                Id_available = true;

            }
        }
        return Id_available;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHNotificationItem.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHNotificationItem.bind((VHNotificationItem) holder, notificationList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return notificationList.size();
    }
}
