package com.app.agrizip.home.activity;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.activities.BaseActivity;
import com.app.agrizip.album.ActivityAlbum;
import com.app.agrizip.app.App;
import com.app.agrizip.block.ActivityBlockList;
import com.app.agrizip.event.activity.ActivityViewEvents;
import com.app.agrizip.friends.friend_list.ActivityFriendList;
import com.app.agrizip.friends.friend_request.FragmentRequests;
import com.app.agrizip.friends.friend_search.adapter.UserSearchAdapter;
import com.app.agrizip.home.fragment.FragmentChats;
import com.app.agrizip.home.fragment.FragmentNewsFeed;
import com.app.agrizip.home.fragment.FragmentNotification;
import com.app.agrizip.need_help.activity.ActivityNeedHelpList;
import com.app.agrizip.post.post_comments.dailog.CommentsDialog;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;
import com.app.agrizip.profile.ActivityUserProfile;
import com.app.agrizip.profile.update_profile.UpdateProfileActivity;
import com.app.agrizip.qr_code.activity.ActivityQrCode;
import com.app.agrizip.settings.ActivitySettings;
import com.app.agrizip.start.activity.ActivitySignUp;
import com.app.agrizip.stories.activity.StoryListActivity;
import com.app.agrizip.support.ActivitySupportInbox;
import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import api.API;
import api.ApiClient;
import api.RetrofitCallbacks;
import apimodels.UserSearchResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.Utils;
import fragments.FragmentMore;
import retrofit2.Call;
import retrofit2.Response;

import static android.R.attr.bitmap;
import static android.R.attr.itemBackground;
import static extras.Constants.REQUEST_CODE_IMAGE_PICKER;

public class ActivityHome extends BaseActivity implements View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener, TextWatcher {

    private static final String TAG = App.APP_TAG + ActivityHome.class.getSimpleName();

    private static final int CAMERA_REQUEST = 1888;

    //private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

    /**
     * @BindView(R.id.bottom_navigation) AHBottomNavigation bottomNavigation;
     */
    @BindView(R.id.et_searchUser)
    EditText etSearch;

    @BindView(R.id.rv_userSearchResult)
    RecyclerView rvUserSearchResult;

    @BindView(R.id.lly_search_user)
    LinearLayout lly_search_user;

    @BindView(R.id.fl_emptyViewSearchFriend)
    FrameLayout flEmptyViewSearchFriend;

    UserSearchAdapter adapter;

    //SearchUser searchUser;

    Call<UserSearchResponse> call;


    @BindView(R.id.rl_showNewsFeed)
    RelativeLayout rlShowNewsFeeds;

    @BindView(R.id.rl_showPendingFriendRequest)
    RelativeLayout rlShowPendingFriendRequest;

    @BindView(R.id.bottom_navigation)
    RelativeLayout bottom_navigation;

    @BindView(R.id.rl_showNotifications)
    RelativeLayout rlShowNotifications;

    @BindView(R.id.rl_showChats)
    RelativeLayout rlShowChats;

    @BindView(R.id.rl_postPicture)
    RelativeLayout rlPostPicture;

    @BindView(R.id.lly_search)
    LinearLayout lly_search;

    @BindView(R.id.iv_toggleMore)
    ImageView toggleMore;

    @BindView(R.id.toolbar_title)
    ImageView toolbar_title;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.iv_showNewsFeed_select)
    ImageView iv_showNewsFeed_select;

    @BindView(R.id.iv_showPendingFriendRequest_select)
    ImageView iv_showPendingFriendRequest_select;

    @BindView(R.id.iv_showNotificationsn_select)
    ImageView iv_showNotificationsn_select;

    @BindView(R.id.iv_showChats_select)
    ImageView iv_showChats_select;

    @BindView(R.id.iv_showChats)
    ImageView iv_showChats;

    @BindView(R.id.iv_showNotificationsn)
    ImageView iv_showNotificationsn;

    @BindView(R.id.iv_showPendingFriendRequest)
    ImageView iv_showPendingFriendRequest;

    @BindView(R.id.iv_showNewsFeed)
    ImageView iv_showNewsFeed;

    List<Fragment> fragmentList = new ArrayList<>();
    boolean isShowingMore = false;

    @BindView(R.id.fab_quickPost)
    FloatingActionButton fabQuickPost;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.fab_scanQrCode)
    FloatingActionButton fabScanQrCode;

    @BindView(R.id.fab_menu)
    FloatingActionMenu fabMenu;

    @BindView(R.id.fragment_container)
    FrameLayout fragment_container;

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.lly_chat)
    LinearLayout lly_chat;
    private NavigationMenuView navMenuView;
    boolean isOnHome = true;
    boolean search = true;
    private FragmentManager fragmentManager;
    private FragmentNewsFeed fragmentNewsFeed;
    private FragmentRequests fragmentRequests;
    private FragmentChats fragmentChats;
    private FragmentNotification fragmentNotification;
    private FragmentMore fragmentMore;

   public static Uri  imageUri;
    public String stringUri="";
    File file;
    Uri fileUri;
    final int RC_TAKE_PHOTO = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        /** **/

//        if (!PreferenceManager.getDefaultSharedPreferences(this)
//                .getBoolean(Constants.IS_PROFILE_SET, false))
//        {
//            Intent i = new Intent(this, ActivitySetupProfile.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(i);
//        }

        ButterKnife.bind(this);

        rlShowNewsFeeds.setBackgroundResource(R.color.white);
        iv_showNewsFeed_select.setVisibility(View.VISIBLE);
        iv_showNewsFeed.setVisibility(View.GONE);

        initUI();

    }

    private void initUI() {
        App.getInstance().getPrivacyDetails(null);

        fragmentManager = getSupportFragmentManager();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        //  NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        navMenuView.addItemDecoration(new DividerItemDecoration(ActivityHome.this, DividerItemDecoration.VERTICAL));

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                if (Constants.USERPROFILE_IMAGE.length() > 0) {
                    if (App.getInstance().getUser().getUserImage() != null) {
                        if (!App.getInstance().getUser().getUserImage().isEmpty()) {
                            Glide.with(ActivityHome.this).load(Constants.USERPROFILE_IMAGE)
                                    .placeholder(R.drawable.user_default)
                                    .error(R.drawable.user_default)
                                    .crossFade()
                                    .into((PorterShapeImageView) navigationView
                                            .getHeaderView(0).findViewById(R.id.iv_drawerUserImage));

                        }
                    }
                }
                if (Constants.USER_NAME.length() > 0) {
                    ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_drawerUserFullName))
                            .setText(Constants.USER_NAME);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.END | Gravity.RIGHT);
                Intent i = new Intent(ActivityHome.this, ActivityUserProfile.class);
                i.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
                startActivity(i);
            }
        });

        if (App.getInstance().getUser().getUserImage() != null) {
            if (!App.getInstance().getUser().getUserImage().isEmpty()) {
                Glide.with(this).load(App.getInstance().getUser().getUserImage())
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .into((PorterShapeImageView) navigationView
                                .getHeaderView(0).findViewById(R.id.iv_drawerUserImage));
            }
        }
        iv_back = (ImageView) findViewById(R.id.iv_back);
        toolbar_title = (ImageView) findViewById(R.id.toolbar_title);
        lly_search = (LinearLayout) findViewById(R.id.lly_search);
        lly_search_user = (LinearLayout) findViewById(R.id.lly_search_user);
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_drawerUserFullName))
                .setText(App.getInstance().getUser().getFullName());

        rlPostPicture.setOnClickListener(this);
        rlShowChats.setOnClickListener(this);
        rlShowNewsFeeds.setOnClickListener(this);
        rlShowPendingFriendRequest.setOnClickListener(this);
        rlShowNotifications.setOnClickListener(this);

        toggleMore.setOnClickListener(this);

        /**bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FFFFFF"));

         AHBottomNavigationItem itemNewFeed = new AHBottomNavigationItem("News Feed", R.mipmap.ic_launcher);
         AHBottomNavigationItem itemRequest = new AHBottomNavigationItem("Requests", R.mipmap.ic_launcher);
         AHBottomNavigationItem itemChats = new AHBottomNavigationItem("Chats", R.mipmap.ic_launcher);
         AHBottomNavigationItem itemNotification = new AHBottomNavigationItem("Notification", R.mipmap.ic_launcher);
         AHBottomNavigationItem itemMore = new AHBottomNavigationItem("More", R.mipmap.ic_launcher);

         bottomNavigationItems.add(itemNewFeed);
         bottomNavigationItems.add(itemRequest);
         bottomNavigationItems.add(itemChats);
         bottomNavigationItems.add(itemNotification);
         bottomNavigationItems.add(itemMore);
         bottomNavigation.setBehaviorTranslationEnabled(true);

         bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

         bottomNavigation.addItems(bottomNavigationItems);
         bottomNavigation.setAccentColor(getResources().getColor(R.color.app_green));
         bottomNavigation.setInactiveColor(Color.parseColor("#4D4D4D"));
         bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#BED9EE"));

         bottomNavigation.setCurrentItem(0);*/

        fragmentNewsFeed = FragmentNewsFeed.newInstance(0);
        fragmentRequests = FragmentRequests.newInstance(1);
        fragmentChats = FragmentChats.newInstance(2);
        fragmentNotification = FragmentNotification.newInstance(3);
        fragmentMore = FragmentMore.newInstance(4);

        fragmentList.add(fragmentNewsFeed);
        fragmentList.add(fragmentRequests);
        fragmentList.add(fragmentChats);
        fragmentList.add(fragmentNotification);
        fragmentList.add(fragmentMore);

        fragmentManager.beginTransaction()
                .add(R.id.fragment_container, fragmentNewsFeed)
                .add(R.id.fragment_container, fragmentRequests)
                .add(R.id.fragment_container, fragmentChats)
                .add(R.id.fragment_container, fragmentNotification)
                //.add(R.id.fragment_moreContainer, fragmentMore)
                .commitAllowingStateLoss();

        showFragment(fragmentNewsFeed, 0);
        etSearch = (EditText) findViewById(R.id.et_searchUser);
        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                lly_search.setVisibility(View.VISIBLE);
                fragment_container.setVisibility(View.GONE);
                bottom_navigation.setVisibility(View.GONE);
                toggleMore.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.GONE);
                iv_back.setVisibility(View.VISIBLE);
                tv_title.setVisibility(View.GONE);
                lly_chat.setVisibility(View.GONE);
                fabMenu.setVisibility(View.GONE);
                rvUserSearchResult.setLayoutManager(new LinearLayoutManager(ActivityHome.this));

                rvUserSearchResult.setHasFixedSize(true);

                adapter = new UserSearchAdapter(ActivityHome.this);
                // When user press back then came to home screen
                search = false;
                isOnHome = false;
                rvUserSearchResult.setAdapter(adapter);

                etSearch.addTextChangedListener(ActivityHome.this);
                //   backpress();
                return false;
            }
        });
        /*etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(ActivityHome.this, ActivitySearchUser.class));
                lly_search.setVisibility(View.VISIBLE);
                fragment_container.setVisibility(View.GONE);
                bottom_navigation.setVisibility(View.GONE);
                toggleMore.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.GONE);
                iv_back.setVisibility(View.VISIBLE);
                rvUserSearchResult.setLayoutManager(new LinearLayoutManager(ActivityHome.this));

                rvUserSearchResult.setHasFixedSize(true);

                adapter = new UserSearchAdapter(ActivityHome.this);

                rvUserSearchResult.setAdapter(adapter);

                etSearch.addTextChangedListener(ActivityHome.this);
                //   backpress();
            }
        });*/
       /* lly_search_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(ActivityHome.this, ActivitySearchUser.class));
                lly_search.setVisibility(View.VISIBLE);
                fragment_container.setVisibility(View.GONE);
                bottom_navigation.setVisibility(View.GONE);
                toggleMore.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.GONE);
                iv_back.setVisibility(View.VISIBLE);
                rvUserSearchResult.setLayoutManager(new LinearLayoutManager(ActivityHome.this));

                rvUserSearchResult.setHasFixedSize(true);

                adapter = new UserSearchAdapter(ActivityHome.this);

                rvUserSearchResult.setAdapter(adapter);

                etSearch.addTextChangedListener(ActivityHome.this);
                //   backpress();

            }
        });*/
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
                etSearch.clearFocus();
                lly_search.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.VISIBLE);
                iv_back.setVisibility(View.GONE);
                fragment_container.setVisibility(View.VISIBLE);
                bottom_navigation.setVisibility(View.VISIBLE);
                toggleMore.setVisibility(View.VISIBLE);
                fabMenu.setVisibility(View.VISIBLE);
                lly_chat.setVisibility(View.GONE);
                showFragment(fragmentNewsFeed, 0);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

            }
        });
        toolbar_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent storyActivity = new Intent(ActivityHome.this, StoryListActivity.class);
                startActivity(storyActivity);
            }
        });
    }

    public void showFragment(Fragment fragmentToShow, int code) {
        //findViewById(R.id.fragment_moreContainer).setVisibility(View.GONE);
        final FragmentTransaction t = fragmentManager.beginTransaction();
        if (code == 0) {
            for (Fragment fragment : fragmentList) {
                if (!fragment.equals(fragmentToShow)) {
                    t.hide(fragment);
                } else {
                    t.show(fragment);
                }
            }
        }
        if (code == 1) {
            fragmentToShow = fragmentList.get(0);
            for (Fragment fragment : fragmentList) {


                if (!fragment.equals(fragmentToShow)) {
                    t.hide(fragment);
                } else {
                    t.show(fragment);
                }
            }
            rlShowNewsFeeds.setBackgroundResource(R.color.white);
            rlShowPendingFriendRequest.setBackgroundResource(R.color.new_color);
            rlShowNotifications.setBackgroundResource(R.color.new_color);
            rlShowChats.setBackgroundResource(R.color.new_color);
            iv_showNewsFeed_select.setVisibility(View.VISIBLE);
            fragmentChats.setMenuVisibility(false);
            fragmentNotification.setMenuVisibility(false);
            fragmentRequests.setMenuVisibility(false);
            iv_showNotificationsn_select.setVisibility(View.GONE);
            iv_showPendingFriendRequest_select.setVisibility(View.GONE);
            lly_chat.setVisibility(View.GONE);
            toggleMore.setVisibility(View.VISIBLE);
            tv_title.setVisibility(View.GONE);
            toolbar_title.setVisibility(View.VISIBLE);
            iv_showChats_select.setVisibility(View.GONE);
            iv_showChats.setVisibility(View.VISIBLE);
            iv_showPendingFriendRequest.setVisibility(View.VISIBLE);
            iv_showNotificationsn.setVisibility(View.VISIBLE);
            iv_showNewsFeed.setVisibility(View.GONE);
            isOnHome = true;
            //t.show(fragmentToShow);
        }

        if (code == 2) {

            fragmentToShow = fragmentList.get(1);
            for (Fragment fragment : fragmentList) {


                if (!fragment.equals(fragmentToShow)) {
                    t.hide(fragment);
                } else {
                    t.show(fragment);
                }
            }
            rlShowNewsFeeds.setBackgroundResource(R.color.new_color);
            rlShowPendingFriendRequest.setBackgroundResource(R.color.white);
            rlShowNotifications.setBackgroundResource(R.color.new_color);
            rlShowChats.setBackgroundResource(R.color.new_color);
            iv_showNewsFeed_select.setVisibility(View.GONE);
            tv_title.setVisibility(View.GONE);
            fragmentChats.setMenuVisibility(false);
            toggleMore.setVisibility(View.VISIBLE);
            fragmentNotification.setMenuVisibility(false);
            lly_chat.setVisibility(View.GONE);
            fragmentNewsFeed.setMenuVisibility(false);
            iv_showNotificationsn_select.setVisibility(View.GONE);
            iv_showPendingFriendRequest_select.setVisibility(View.VISIBLE);
            toolbar_title.setVisibility(View.VISIBLE);
            iv_showChats_select.setVisibility(View.GONE);
            iv_showChats.setVisibility(View.VISIBLE);
            iv_showPendingFriendRequest.setVisibility(View.GONE);
            iv_showNotificationsn.setVisibility(View.VISIBLE);
            iv_showNewsFeed.setVisibility(View.VISIBLE);
            isOnHome = false;
        }

        if(code == 3){
            fragmentToShow = fragmentList.get(2);
            for (Fragment fragment : fragmentList) {


                if (!fragment.equals(fragmentToShow)) {
                    t.hide(fragment);
                } else {
                    t.show(fragment);
                }
            }
            App.getInstance().addRosterListener(fragmentChats);
            rlShowNewsFeeds.setBackgroundResource(R.color.new_color);
            rlShowPendingFriendRequest.setBackgroundResource(R.color.new_color);
            rlShowNotifications.setBackgroundResource(R.color.new_color);
            rlShowChats.setBackgroundResource(R.color.white);
            iv_showNewsFeed_select.setVisibility(View.GONE);
            fragmentNewsFeed.setMenuVisibility(false);
            fragmentNotification.setMenuVisibility(false);
            fragmentRequests.setMenuVisibility(false);
            fragmentChats.setMenuVisibility(true);
            iv_showNotificationsn_select.setVisibility(View.GONE);
            iv_showPendingFriendRequest_select.setVisibility(View.GONE);
            iv_showChats_select.setVisibility(View.VISIBLE);
            iv_showChats.setVisibility(View.GONE);
            toggleMore.setVisibility(View.VISIBLE);
            toolbar_title.setVisibility(View.GONE);
            tv_title.setVisibility(View.VISIBLE);
            //lly_chat.setVisibility(View.VISIBLE);
            iv_showPendingFriendRequest.setVisibility(View.VISIBLE);
            iv_showNotificationsn.setVisibility(View.VISIBLE);
            iv_showNewsFeed.setVisibility(View.VISIBLE);
            isOnHome = false;
        }
        t.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {

        if (fabMenu.isOpened()) {
            fabMenu.close(true);
            return;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (isOnHome == false) {
            if (search == false) {
                etSearch.setText("");
                etSearch.clearFocus();
                lly_search.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.VISIBLE);
                iv_back.setVisibility(View.GONE);
                fragment_container.setVisibility(View.VISIBLE);
                bottom_navigation.setVisibility(View.VISIBLE);
                toggleMore.setVisibility(View.VISIBLE);
                fabMenu.setVisibility(View.VISIBLE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                search = true;

            } else {
                rlShowNewsFeeds.callOnClick();
            }

        } else if (drawer.isDrawerOpen(GravityCompat.END | Gravity.RIGHT)) {
            drawer.closeDrawer(GravityCompat.END | Gravity.RIGHT);

        } else {
            promptUserForExitApp();
        }


        //  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END | Gravity.RIGHT)) {
            drawer.closeDrawer(GravityCompat.END | Gravity.RIGHT);
        } else if (fragmentNewsFeed.isShowingComments()) {
            fragmentNewsFeed.hideCommentsFragment();
        } else {
            Log.d("HomeBackPress", "No Changed");
        }
    }

    private void promptUserForExitApp() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.exit_title)
                .setMessage(R.string.exit_msg)
                .setIcon(R.drawable.ic_exit_green_24dp)
                .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                       /* PreferenceManager.getDefaultSharedPreferences(ActivityHome.this)
                                .edit()
                                .putString(Constants.USER_ID, "")
                                .putString(Constants.PREF_USER, "")
                                .apply();*/

                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    private void promptUserForClosingApp() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.logout_title)
                .setMessage(R.string.logout_msg)
                .setIcon(R.drawable.ic_exit_green_24dp)
                .setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        PreferenceManager.getDefaultSharedPreferences(ActivityHome.this)
                                .edit()
                                .putString(Constants.USER_ID, "")
                                .putString(Constants.PREF_USER, "")
                                .apply();

                        Constants.USERPROFILE_IMAGE = "";
                        Intent signUp = new Intent(ActivityHome.this, ActivitySignUp.class);
                        startActivity(signUp);
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();

    }

    @Override
    public void onClick(View view) {
        closeFabMenu();
        switch (view.getId()) {
            case R.id.rl_showNewsFeed:
                showFragment(fragmentNewsFeed, 0);
                rlShowNewsFeeds.setBackgroundResource(R.color.white);
                rlShowPendingFriendRequest.setBackgroundResource(R.color.new_color);
                rlShowNotifications.setBackgroundResource(R.color.new_color);
                rlShowChats.setBackgroundResource(R.color.new_color);
                iv_showNewsFeed_select.setVisibility(View.VISIBLE);
                fragmentChats.setMenuVisibility(false);
                fragmentNotification.setMenuVisibility(false);
                fragmentRequests.setMenuVisibility(false);
                iv_showNotificationsn_select.setVisibility(View.GONE);
                iv_showPendingFriendRequest_select.setVisibility(View.GONE);
                lly_chat.setVisibility(View.GONE);
                toggleMore.setVisibility(View.VISIBLE);
                tv_title.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.VISIBLE);
                iv_showChats_select.setVisibility(View.GONE);
                iv_showChats.setVisibility(View.VISIBLE);
                iv_showPendingFriendRequest.setVisibility(View.VISIBLE);
                iv_showNotificationsn.setVisibility(View.VISIBLE);
                iv_showNewsFeed.setVisibility(View.GONE);
                isOnHome = true;
                break;

            case R.id.rl_showPendingFriendRequest:
                fragmentRequests.getPendingFriendRequests();
                showFragment(fragmentRequests, 0);
                rlShowNewsFeeds.setBackgroundResource(R.color.new_color);
                rlShowPendingFriendRequest.setBackgroundResource(R.color.white);
                rlShowNotifications.setBackgroundResource(R.color.new_color);
                rlShowChats.setBackgroundResource(R.color.new_color);
                iv_showNewsFeed_select.setVisibility(View.GONE);
                tv_title.setVisibility(View.GONE);
                fragmentChats.setMenuVisibility(false);
                toggleMore.setVisibility(View.VISIBLE);
                fragmentNotification.setMenuVisibility(false);
                lly_chat.setVisibility(View.GONE);
                fragmentNewsFeed.setMenuVisibility(false);
                iv_showNotificationsn_select.setVisibility(View.GONE);
                iv_showPendingFriendRequest_select.setVisibility(View.VISIBLE);
                toolbar_title.setVisibility(View.VISIBLE);
                iv_showChats_select.setVisibility(View.GONE);
                iv_showChats.setVisibility(View.VISIBLE);
                iv_showPendingFriendRequest.setVisibility(View.GONE);
                iv_showNotificationsn.setVisibility(View.VISIBLE);
                iv_showNewsFeed.setVisibility(View.VISIBLE);
                isOnHome = false;
                break;

            case R.id.rl_postPicture:
                selectImage();
           /*     Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);*/
                break;

            case R.id.rl_showNotifications:
                fragmentNotification.getNotificationList();
                showFragment(fragmentNotification, 0);
                rlShowNewsFeeds.setBackgroundResource(R.color.new_color);
                tv_title.setVisibility(View.GONE);
                rlShowPendingFriendRequest.setBackgroundResource(R.color.new_color);
                rlShowNotifications.setBackgroundResource(R.color.white);
                rlShowChats.setBackgroundResource(R.color.new_color);
                iv_showNewsFeed_select.setVisibility(View.GONE);
                toggleMore.setVisibility(View.VISIBLE);
                fragmentChats.setMenuVisibility(false);
                fragmentNewsFeed.setMenuVisibility(false);
                fragmentRequests.setMenuVisibility(false);
                iv_showNotificationsn_select.setVisibility(View.VISIBLE);
                iv_showPendingFriendRequest_select.setVisibility(View.GONE);
                lly_chat.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.VISIBLE);
                iv_showChats_select.setVisibility(View.GONE);
                iv_showChats.setVisibility(View.VISIBLE);
                iv_showPendingFriendRequest.setVisibility(View.VISIBLE);
                iv_showNotificationsn.setVisibility(View.GONE);
                iv_showNewsFeed.setVisibility(View.VISIBLE);
                isOnHome = false;
                break;

            case R.id.rl_showChats:
                /*Intent chatActivity = new Intent(this, ActivityChat.class);
                startActivity(chatActivity);*/
                fragmentChats.getFriendList();
                showFragment(fragmentChats, 0);
                // fragmentChats.getFriendList();
                App.getInstance().addRosterListener(fragmentChats);
                rlShowNewsFeeds.setBackgroundResource(R.color.new_color);
                rlShowPendingFriendRequest.setBackgroundResource(R.color.new_color);
                rlShowNotifications.setBackgroundResource(R.color.new_color);
                rlShowChats.setBackgroundResource(R.color.white);
                iv_showNewsFeed_select.setVisibility(View.GONE);
                fragmentNewsFeed.setMenuVisibility(false);
                fragmentNotification.setMenuVisibility(false);
                fragmentRequests.setMenuVisibility(false);
                fragmentChats.setMenuVisibility(true);
                iv_showNotificationsn_select.setVisibility(View.GONE);
                iv_showPendingFriendRequest_select.setVisibility(View.GONE);
                iv_showChats_select.setVisibility(View.VISIBLE);
                iv_showChats.setVisibility(View.GONE);
                toggleMore.setVisibility(View.VISIBLE);
                toolbar_title.setVisibility(View.GONE);
                tv_title.setVisibility(View.VISIBLE);
                //lly_chat.setVisibility(View.VISIBLE);
                iv_showPendingFriendRequest.setVisibility(View.VISIBLE);
                iv_showNotificationsn.setVisibility(View.VISIBLE);
                iv_showNewsFeed.setVisibility(View.VISIBLE);
                isOnHome = false;
                break;

            case R.id.iv_toggleMore:
                drawer.openDrawer(GravityCompat.END | Gravity.RIGHT);
                break;
        }
    }

    private void closeFabMenu() {
        if (fabMenu.isOpened()) {
            fabMenu.close(true);
            return;
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END | Gravity.RIGHT);

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_profileUpdate:
                Intent profileintent = new Intent(this, UpdateProfileActivity.class);
                profileintent.putExtra("COME_FROM", "HOME_SCREEN");
                startActivity(profileintent);
                // finish();
                //startActivity(new Intent(this, UpdateProfileActivity.class));
                break;

            case R.id.nav_friends:

                Intent in = new Intent(this, ActivityFriendList.class);
                in.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                startActivity(in);
                break;

            case R.id.nav_photos:
                Intent i = new Intent(this, ActivityAlbum.class);
                i.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                startActivity(i);
                break;

            case R.id.action_needHelpList:
                Intent intent = new Intent(this, ActivityNeedHelpList.class);
                startActivity(intent);
                break;

            case R.id.nav_events:
                Intent eventsActivity = new Intent(this, ActivityViewEvents.class);
                startActivity(eventsActivity);
                break;

            case R.id.nav_story:
                Intent storyActivity = new Intent(this, StoryListActivity.class);
                startActivity(storyActivity);
                break;

            case R.id.nav_supportInbox:
                Intent supportInbox = new Intent(this, ActivitySupportInbox.class);
                startActivity(supportInbox);
                break;

            case R.id.nav_block:
                Intent blockActivity = new Intent(this, ActivityBlockList.class);
                startActivity(blockActivity);
                break;
            /**case R.id.nav_privacy:
             startActivity(new Intent(this,
             ActivityPrivacySettings.class));
             break;*/

            case R.id.nav_settings:
                Intent settingsActivity = new Intent(this, ActivitySettings.class);
                startActivity(settingsActivity);
                break;

         /*   case R.id.nav_change_language:

                break;
*/
            case R.id.nav_logout:
                promptUserForClosingApp();
                break;
        }
        return true;
    }

    private void setNavMenuItemColor(MenuItem item, int color) {
        SpannableString span = new SpannableString(item.getTitle());
        span.setSpan(new BackgroundColorSpan(color), 0, span.length(), 0);
        item.setTitle(span);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        LayoutInflater inflater = this.getLayoutInflater();
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityHome.this);
        builder.setTitle("Add Photo!");
        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    // cameraIntent();
                    takePhoto();
                } else if (items[item].equals("Choose from Gallery")) {
                    openImagePicker();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openImagePicker() {

        ImagePicker.create(this)
                .folderMode(false) // folder mode (false by default)
                .folderTitle("Select Photos") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .multi() // multi mode (default mode)
                .limit(20) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .start(REQUEST_CODE_IMAGE_PICKER); // start image picker activity with request code


    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST);
    }
    // Resolve File Camera issue in home page MN
    private void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, RC_TAKE_PHOTO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "On Activity result of home activity REQUEST CODE: " + requestCode);

        if (requestCode == RC_TAKE_PHOTO && resultCode == RESULT_OK) {
            onCaptureImageResult();
        }
        else
        {
            switch (requestCode) {
                case CommentsDialog.REQUEST_POST_IMAGE_AS_COMMENT:
                    if (resultCode == RESULT_OK && data != null) {
                        Log.i(TAG, "REQUEST_POST_IMAGE_AS_COMMENT: " + CommentsDialog.REQUEST_POST_IMAGE_AS_COMMENT);
                        ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                        for (Image singleImage : images) {
                            Log.i(TAG, "for singel image");
                            if (fragmentNewsFeed.commentsDialog != null) {
                                fragmentNewsFeed.commentsDialog.postImageAsComment(singleImage);
                            }
                        }
                    }
                    break;

                case CAMERA_REQUEST:
                    temp1();
                    if (data != null) {
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case REQUEST_CODE_IMAGE_PICKER:
                    if (data != null) {
                        final ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                        Intent i = new Intent(this, ActivityPostToAgrizip.class);
                        i.putExtra("ImageKey", "setImage");
                        i.putParcelableArrayListExtra("GalleryImage", images);
                        startActivity(i);
                    }
                    break;

            }
            if (requestCode == 444) {
                boolean chatvisible = data.getBooleanExtra("chatScreen",false);

                if(chatvisible){
                    fragmentChats.getFriendList();
                    showFragment(fragmentChats,3);
                }else{
                    fragmentNewsFeed.onActivityResult(requestCode, resultCode, data);
                }
                //fragmentNewsFeed.onActivityResult(requestCode, resultCode, data);
            }

        }

        // fragmentNewsFeed.onActivityResult(requestCode,resultCode,data);
    }

    private void onCaptureImageResult() {

        Intent i1 = new Intent(this, ActivityPostToAgrizip.class);
        i1.putExtra(ActivityPostToAgrizip.POST_CAPTURED_IMAGE_URL, "temp.jpg");
        startActivity(i1);

    }

    public void temp1(){
       // String filePath = Utils.getRealPathFromURI(this, ActivityHome.imageUri);

        try{
        Bitmap photo1 = Utils.convertImageToBitmap(stringUri);
        Intent int1 = new Intent(ActivityHome.this, ActivityPostToAgrizip.class);
        int1.putExtra(ActivityPostToAgrizip.POST_CAPTURED_IMAGE, photo1);
      //  startActivity(int1);
        }catch (Exception e){
            e.printStackTrace();
        }
      //  finish();
    }

    @OnClick(R.id.fab_scanQrCode)
    public void openQrCodeActivity() {
        startActivity(new Intent(this, ActivityQrCode.class));
    }

    @OnClick(R.id.fab_quickPost)
    public void openPostActivity() {
        startActivity(new Intent(this, ActivityPostToAgrizip.class));
    }

    @Override
    protected void onStop() {
        closeFabMenu();
        super.onStop();
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onDestroy() {
        if (fragmentNewsFeed != null) {
            fragmentNewsFeed.unregisterReceiver();
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void finish() {
        if (ApiClient.getOkHttpClient() != null) {
            ApiClient.getOkHttpClient().dispatcher().cancelAll();
        }
        super.finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        if (charSequence.toString().length() > 1) {
            searchUsers(charSequence.toString());

            /*if (searchUser != null)
            {
                searchUser.cancel(true);
                searchUser = null;
            } else
            {
                searchUser = new SearchUser(this, charSequence.toString());
                searchUser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }*/
        } else {
            adapter.clear();
            toggleEmptyView();
        }
    }

    private void searchUsers(String keyword) {
        if (call != null) {
            call.cancel();
        }

        call = API.getInstance().searchUser(App.getInstance().getUser().getId(), keyword, new RetrofitCallbacks<UserSearchResponse>(this) {

            @Override
            public void onResponse(Call<UserSearchResponse> call, Response<UserSearchResponse> response) {
                super.onResponse(call, response);

                if (response.isSuccessful()) {
                    adapter.clear();
                    for (UserSearchResponse.DataBean singleSearchedUser : response.body().getData()) {
                        adapter.addSearchedUser(singleSearchedUser);
                    }
                    toggleEmptyView();
                }

            }

            @Override
            public void onFailure(Call<UserSearchResponse> call, Throwable t) {
                super.onFailure(call, t);
            }
        });

    }


    private void toggleEmptyView() {
        if (adapter.getItemCount() > 0) {
            flEmptyViewSearchFriend.setVisibility(View.GONE);
        } else {
            flEmptyViewSearchFriend.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fragmentNewsFeed != null) {
            App.getInstance().getUser().setUserEventCallbacks(fragmentNewsFeed);
        }
    }

}
