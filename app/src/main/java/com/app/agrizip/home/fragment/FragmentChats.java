package com.app.agrizip.home.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.chat.activity.ActivityChatDialog;
import com.app.agrizip.chat.adapter.QbChatListAdapter;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.chat.utils.DialogUtils;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;

import java.util.ArrayList;
import java.util.Collection;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FriendList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Utils;
import quickblox.QbDialogUtils;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Rakshit on 18-11-2016 at 17:53.
 */

public class FragmentChats extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        FriendList.DataBean.ChatOperationsListener, QBRosterListener, App.RosterReadyListener
{

    public static final String PROPERTY_OCCUPANTS_IDS = "occupants_ids";
    public static final String PROPERTY_DIALOG_TYPE = "dialog_type";
    public static final String PROPERTY_DIALOG_NAME = "dialog_name";
    public static final String PROPERTY_NOTIFICATION_TYPE = "notification_type";
    private static final String TAG = App.APP_TAG + FragmentChats.class.getSimpleName();
    @BindView(R.id.rv_chatList)
    RecyclerView rvFriendList;

    @BindView(R.id.ll_loadingChat)
    LinearLayout llLoadingChat;

    @BindView(R.id.ll_noChatContacts)
    LinearLayout llNoChatContacts;

    @BindView(R.id.srl_refreshChatList)
    SwipeRefreshLayout srlRefreshFriendList;

    QbChatListAdapter adapter;

    boolean busyLoadingData = false;

    int pageNo = 1;

    FriendList friendList;

    int pastVisibleItems, visibleItemCount, totalItemCount;

    LinearLayoutManager llm;

    QBChatDialog qbChatDialog;

    private QBSystemMessagesManager systemMessagesManager;
    private boolean listIsSet;

    public static FragmentChats newInstance(int index)
    {
        FragmentChats fragment = new FragmentChats();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    private static QBChatMessage buildSystemMessageAboutCreatingGroupDialog(QBChatDialog dialog)
    {
        QBChatMessage qbChatMessage = new QBChatMessage();
        qbChatMessage.setDialogId(dialog.getDialogId());
        qbChatMessage.setProperty(PROPERTY_OCCUPANTS_IDS, QbDialogUtils.getOccupantsIdsStringFromList(dialog.getOccupants()));
        qbChatMessage.setProperty(PROPERTY_DIALOG_TYPE, String.valueOf(dialog.getType().getCode()));
        qbChatMessage.setProperty(PROPERTY_DIALOG_NAME, String.valueOf(dialog.getName()));
        qbChatMessage.setProperty(PROPERTY_NOTIFICATION_TYPE, "creating_dialog");

        return qbChatMessage;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        ButterKnife.bind(this, rootView);

        adapter = new QbChatListAdapter(getActivity());

        llm = new LinearLayoutManager(getActivity());

        rvFriendList.setLayoutManager(llm);
        rvFriendList.setHasFixedSize(true);
        rvFriendList.setAdapter(adapter);
        rvFriendList.setItemAnimator(new DefaultItemAnimator());

        rvFriendList.addOnScrollListener(new OnScrollRecyclerView());
        srlRefreshFriendList.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshFriendList.setOnRefreshListener(this);

        return rootView;
    }

    @Override
    public void onRefresh()
    {
        if (!busyLoadingData)
        {
            pageNo = 1;
            getFriendList();
        }
    }


    public void getFriendList()
    {

        App.getInstance().setRosterReadyListener(this);

        final RetrofitCallbacks<FriendList> onGetFriendList =
                new RetrofitCallbacks<FriendList>(getActivity())
                {
                    @Override
                    public void onResponse(Call<FriendList> call, Response<FriendList> response)
                    {
                        super.onResponse(call, response);
                        if (response.isSuccessful())
                        {
                            if (adapter.getItemCount() > 0)
                            {
                                llLoadingChat.setVisibility(View.GONE);
                                rvFriendList.setVisibility(View.VISIBLE);
                                llNoChatContacts.setVisibility(View.GONE);
                            }
                            busyLoadingData = false;
                            if (adapter.getItemCount() == 0 && pageNo == 1)
                            {
                                adapter.clear();
                                friendList = response.body();
                                llLoadingChat.setVisibility(View.GONE);
                                rvFriendList.setVisibility(View.GONE);
                                llNoChatContacts.setVisibility(View.VISIBLE);
                            } else
                            {
                                if (response.body().getData().size() > 0)
                                {
                                    listIsSet = false;
                                    friendList.getData().addAll(response.body().getData());
                                }
                                else{

                                }
                            }

                            if (App.getInstance().getRoster() == null)
                            {
                                while (App.getInstance().getRoster() != null)
                                {

                                }
                            }

                            if (App.getInstance().getRoster() != null && !listIsSet)
                            {
                                listIsSet = true;

                                for (FriendList.DataBean singleFriend : response.body().getData())
                                {
                                    singleFriend.setChatOperationsListener(FragmentChats.this);
                                    adapter.addSingleFriend(singleFriend);
                                }
                                llLoadingChat.setVisibility(View.GONE);
                                rvFriendList.setVisibility(View.VISIBLE);
                                llNoChatContacts.setVisibility(View.GONE);
                            }

                            if (srlRefreshFriendList.isRefreshing())
                            {
                                srlRefreshFriendList.setRefreshing(false);
                            }
                        }}


                    @Override
                    public void onFailure(Call<FriendList> call, Throwable t)
                    {
                        super.onFailure(call, t);
                        llLoadingChat.setVisibility(View.GONE);
                        rvFriendList.setVisibility(View.GONE);
                        llNoChatContacts.setVisibility(View.VISIBLE);
                    }
                };


        if (!busyLoadingData)
        {
            busyLoadingData = true;

            API.getInstance().getFriendList(App.getInstance().getUser().getId(), pageNo,
                    onGetFriendList);

        }

    }

    @Override
    public void initChat(QBUser qbUser, final FriendList.DataBean user)
    {
        if (qbUser == null)
        {
            Toast.makeText(getActivity(), "User not found on quick blox", Toast.LENGTH_SHORT).show();
            return;
        }else{

            Utils.showProgressDialog(getActivity(), "Please Wait...", "Loading Chat", false, false, null);
        }

        ArrayList<Integer> occupantIdsList = new ArrayList<>();
        occupantIdsList.add(qbUser.getId());
        occupantIdsList.add(App.getInstance().getUser().getQuickBloxUser().getId());

        qbChatDialog = DialogUtils.buildPrivateDialog(qbUser.getId());
        qbChatDialog.setOccupantsIds(occupantIdsList);
        qbChatDialog.setUserId(App.getInstance().getUser().getQuickBloxUser().getId());
        Log.i(TAG, "qbChatDialog: " + qbChatDialog.hashCode());

        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();


        QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>()
        {
            @Override
            public void onSuccess(QBChatDialog result, Bundle params)
            {
                Log.i(TAG, "result: " + result.hashCode());
                qbChatDialog = result;
                Log.i(TAG, "CHAT DIALOG WAS CREATED SUCCESSFULLY!!!");


                Log.i(TAG, "getRecipientId: " + result.getRecipientId());
                Log.i(TAG, "getDialogId: " + result.getDialogId());
                Log.i(TAG, "getName: " + result.getName());
                Log.i(TAG, "getId: " + result.getId());
                Log.i(TAG, "getUserId: " + result.getUserId());
                Log.i(TAG, "getOccupants: " + result.getOccupants());
                for (Integer id : result.getOccupants())
                {
                    Log.i(TAG, "occupant: " + id);
                }

                sendSystemMessageAboutCreatingDialog(systemMessagesManager, result);

                Utils.hideProgressDialog();
                ActivityChatDialog
                        .startForResult(getActivity(), 0, result,user.getFullName());
            }

            @Override
            public void onError(QBResponseException responseException)
            {
                Utils.hideProgressDialog();
                Log.i(TAG, "ERROR CREATING CHAT DIALOG");
            }
        });
    }

    public void sendSystemMessageAboutCreatingDialog(QBSystemMessagesManager systemMessagesManager, QBChatDialog dialog)
    {
        QBChatMessage systemMessageCreatingDialog = buildSystemMessageAboutCreatingGroupDialog(dialog);

        for (Integer recipientId : dialog.getOccupants())
        {
            if (QBChatService.getInstance().getUser() == null)
            {
                Log.i(TAG, "QB USER IS NULL");
                return;
            }
            if (!recipientId.equals(QBChatService.getInstance().getUser().getId()))
            {
                systemMessageCreatingDialog.setRecipientId(recipientId);
                try
                {
                    systemMessagesManager.sendSystemMessage(systemMessageCreatingDialog);
                } catch (SmackException.NotConnectedException e)
                {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void entriesDeleted(Collection<Integer> collection)
    {

    }

    @Override
    public void entriesAdded(Collection<Integer> collection)
    {

    }

    @Override
    public void entriesUpdated(Collection<Integer> collection)
    {

    }

    @Override
    public void presenceChanged(QBPresence qbPresence)
    {
        Log.i(TAG, "*******************************presenceChanged: USER ID: " + qbPresence.getUserId());
        adapter.notifyPresenceChanged(qbPresence);
    }

    @Override
    public void onRosterReady()
    {
        getFriendList();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    class OnScrollRecyclerView extends RecyclerView.OnScrollListener
    {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            if (dy > 0)
            {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (!busyLoadingData)
                {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount)
                    {
                        pageNo++;
                        getFriendList();
                    }
                }
            }
        }
    }

}
