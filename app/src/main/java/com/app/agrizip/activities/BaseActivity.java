package com.app.agrizip.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.app.agrizip.app.App;

/**
 * Created by rutvik on 2/23/2017 at 2:56 PM.
 */

public abstract class BaseActivity extends AppCompatActivity
{

    private static final String TAG = App.APP_TAG + BaseActivity.class.getSimpleName();

    @CallSuper
    @Override
    protected void onStart()
    {
        super.onStart();

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected())
        {

        } else
        {
            Toast.makeText(this, "You are not connected to the internet", Toast.LENGTH_LONG);
        }

    }

}
