package com.app.agrizip.qr_code.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.friends.friend_list.ActivityFriendList;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import api.API;
import api.RetrofitCallbacks;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/1/2017 at 1:17 PM.
 */

public class FragmentScanQrCode extends Fragment
{

    final Handler mHandler = new Handler();
    @BindView(R.id.CameraView)
    SurfaceView cameraView;
    BarcodeDetector barcode;
    CameraSource cameraSource;
    SurfaceHolder surfaceHolder;

    Call<ResponseBody> addFriendViaQrCode;

    public static FragmentScanQrCode newInstance()
    {
        return new FragmentScanQrCode();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_scan_qr_code, container, false);
        ButterKnife.bind(this, view);

        cameraView.setZOrderMediaOverlay(true);
        surfaceHolder = cameraView.getHolder();
        barcode = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        if (!barcode.isOperational())
        {
            Toast.makeText(getActivity().getApplicationContext(), "Sorry can't Found detector", Toast.LENGTH_LONG).show();
        }
        cameraSource = new CameraSource.Builder(getActivity(), barcode)
                .setFacing(CameraSource.CAMERA_FACING_BACK)

                .setRequestedFps(24)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(170, 170)
                .build();

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback()
        {
            @Override
            public void surfaceCreated(SurfaceHolder holder)
            {

                if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                {

                }
                try
                {
                    cameraSource.start(cameraView.getHolder());
                } catch (IOException e)
                {
                    e.printStackTrace();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
            {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder)
            {

            }
        });

        barcode.setProcessor(new Detector.Processor<Barcode>()
        {
            @Override
            public void release()
            {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections)
            {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() > 0)
                {
                    final Barcode barcode = barcodes.valueAt(0);

                    mHandler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                           //Toast.makeText(getActivity(), "Hol", Toast.LENGTH_SHORT).show();
                        }
                    });

                    mHandler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {

                            if (addFriendViaQrCode != null)
                            {
                                addFriendViaQrCode.cancel();
                            }

                            addFriendViaQrCode = API.getInstance()
                                    .scanQrCode(App.getInstance().getUser().getId(),
                                            barcode.displayValue, new RetrofitCallbacks<ResponseBody>(getActivity())
                                            {

                                                @Override
                                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
                                                {
                                                    super.onResponse(call, response);
                                                    if (response.isSuccessful())
                                                    {
                                                        Toast.makeText(getActivity(),
                                                                "Friend Added Successfully", Toast.LENGTH_SHORT).show();

                                                        Intent intent = new Intent(getActivity(), ActivityFriendList.class);
                                                        intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                                                        getActivity().startActivity(intent);

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseBody> call, Throwable t)
                                                {
                                                    super.onFailure(call, t);
                                                }
                                            });
                        }
                    });

                    try
                    {
                        Thread.sleep(5000);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }

                    //FragmentScanQrCode.this.barcode.release();

                    /**Intent intent=new Intent();
                     intent.putExtra("barcode",barcodes.valueAt(0));
                     setResult(RESULT_OK,intent);
                     finish();*/
                }
            }
        });

        return view;
    }
}
