package com.app.agrizip.qr_code.fragment;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 2/1/2017 at 1:27 PM.
 */

public class FragmentShowQrCode extends Fragment
{

    @BindView(R.id.iv_qrCode)
    ImageView ivQrCode;

    public static FragmentShowQrCode newInstance()
    {
        return new FragmentShowQrCode();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_show_qr_code, container, false);
        ButterKnife.bind(this, view);

        generateQrCode();

        return view;
    }

    private void generateQrCode()
    {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try
        {
            BitMatrix bitMatrix = multiFormatWriter.encode(App.getInstance().getUser().getId(),
                    BarcodeFormat.QR_CODE, 170, 170);
            //  Bitmap ImageBitmap = Bitmap.createBitmap(180, 40, Bitmap.Config.ARGB_8888);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();

            //   Bitmap myLogo = BitmapFactory.decodeResource(getResources(), R.drawable.google_logo);

            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            //    Bitmap  bitMerged = mergeBitmaps(bitmap,myLogo);
            for (int i = 0; i < 170; i++)
            {//width
                for (int j = 0; j < 170; j++)
                {//height
                    bitmap.setPixel(i, j, bitMatrix.get(i, j) ?
                            getResources().getColor(R.color.new_color) : Color.WHITE);
                }
            }

            ivQrCode.setImageBitmap(bitmap);

        } catch (WriterException e)
        {
            e.printStackTrace();
        }
    }


}
