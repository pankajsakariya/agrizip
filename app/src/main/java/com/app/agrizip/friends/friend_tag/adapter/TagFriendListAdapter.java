package com.app.agrizip.friends.friend_tag.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apimodels.FriendList;

/**
 * Created by rutvik on 11/30/2016 at 6:04 PM.
 */

public class TagFriendListAdapter extends RecyclerView.Adapter
{

    final Context context;
    final TagFriendListener tagFriendListener;

    final List<FriendList.DataBean> tagFriendList;

    public TagFriendListAdapter(Context context, TagFriendListener tagFriendListener)
    {
        this.context = context;
        this.tagFriendListener = tagFriendListener;
        tagFriendList = new ArrayList<>();
    }

    public void addFriend(FriendList.DataBean singleTagFriend)
    {
        tagFriendList.add(singleTagFriend);
        notifyItemInserted(tagFriendList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingleTagFriend.create(context, parent, tagFriendListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSingleTagFriend.bind((VHSingleTagFriend) holder, tagFriendList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return tagFriendList.size();
    }


    public void clear()
    {
        tagFriendList.clear();
        notifyDataSetChanged();
    }

    public interface TagFriendListener
    {

        void onSelectedFriendForTagging(final String friendId, final String friendName);

        void onDeselectedFriendForTagging(final String friendId, final String friendName);

    }

}
