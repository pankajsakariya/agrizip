package com.app.agrizip.friends.friend_list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apimodels.FriendList;

/**
 * Created by rutvik on 1/2/2017 at 11:52 AM.
 */

public class FriendListAdapter extends RecyclerView.Adapter
{

    final Context context;

    final List<FriendList.DataBean> friendList;

    public FriendListAdapter(final Context context)
    {
        this.context = context;
        friendList = new ArrayList<>();
    }

    public void addSingleFriend(FriendList.DataBean singleFriend)
    {
        friendList.add(singleFriend);
        notifyItemInserted(friendList.size());
    }

    public void removeFriend(FriendList.DataBean friend)
    {
        final int index = friendList.indexOf(friend);
        friendList.remove(index);
        notifyItemRemoved(index);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingleFriend.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSingleFriend.bind((VHSingleFriend) holder, friendList.get(position), position);
    }

    @Override
    public int getItemCount()
    {
        return friendList.size();
    }

    public void clear()
    {
        friendList.clear();
        notifyDataSetChanged();
    }
}
