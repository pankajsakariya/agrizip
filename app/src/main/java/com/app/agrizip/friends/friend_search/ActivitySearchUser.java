package com.app.agrizip.friends.friend_search;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.friends.friend_search.adapter.UserSearchAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.UserSearchResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class ActivitySearchUser extends AppCompatActivity implements TextWatcher
{
    private static final String TAG = App.APP_TAG + ActivitySearchUser.class.getSimpleName();

    @BindView(R.id.et_searchUser)
    EditText etSearch;

    @BindView(R.id.rv_userSearchResult)
    RecyclerView rvUserSearchResult;

    @BindView(R.id.fl_emptyViewSearchFriend)
    FrameLayout flEmptyViewSearchFriend;

    UserSearchAdapter adapter;

    //SearchUser searchUser;

    Call<UserSearchResponse> call;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

        ButterKnife.bind(this);

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null)
            {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        rvUserSearchResult.setLayoutManager(new LinearLayoutManager(this));

        rvUserSearchResult.setHasFixedSize(true);

        adapter = new UserSearchAdapter(this);

        rvUserSearchResult.setAdapter(adapter);

        etSearch.addTextChangedListener(this);

    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        if (charSequence.toString().length() > 2)
        {
            searchUsers(charSequence.toString());

            /*if (searchUser != null)
            {
                searchUser.cancel(true);
                searchUser = null;
            } else
            {
                searchUser = new SearchUser(this, charSequence.toString());
                searchUser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }*/
        } else
        {
            adapter.clear();
            toggleEmptyView();
        }
    }

    @Override
    public void afterTextChanged(Editable editable)
    {

    }


    private void searchUsers(String keyword)
    {
        if (call != null)
        {
            call.cancel();
        }

        call = API.getInstance().searchUser(App.getInstance().getUser().getId(), keyword, new RetrofitCallbacks<UserSearchResponse>(this)
        {

            @Override
            public void onResponse(Call<UserSearchResponse> call, Response<UserSearchResponse> response)
            {
                super.onResponse(call, response);

                if (response.isSuccessful())
                {
                    adapter.clear();
                    for (UserSearchResponse.DataBean singleSearchedUser : response.body().getData())
                    {
                        adapter.addSearchedUser(singleSearchedUser);
                    }
                    toggleEmptyView();
                }

            }


            @Override
            public void onFailure(Call<UserSearchResponse> call, Throwable t)
            {
                super.onFailure(call, t);
            }
        });

    }


    /*class SearchUser extends AsyncTask<Void, Void, UserSearchResponse>
    {

        final String keyword;

        final Context context;

        public SearchUser(Context context, String keyword)
        {
            this.keyword = keyword;
            this.context = context;
        }

        @Override
        protected UserSearchResponse doInBackground(Void... voids)
        {

            return API.getInstance().searchUser(keyword, new RetrofitCallbacks<UserSearchResponse>(context)
            {

                @Override
                public void onResponse(Call<UserSearchResponse> call, Response<UserSearchResponse> response)
                {
                    super.onResponse(call, response);
                }


                @Override
                public void onFailure(Call<UserSearchResponse> call, Throwable t)
                {
                    super.onFailure(call, t);
                }
            });

        }

        @Override
        protected void onPostExecute(UserSearchResponse userSearchResponse)
        {
            if (userSearchResponse != null)
            {
                adapter.clear();
                for (UserSearchResponse.DataBean singleSearchedUser : userSearchResponse.getData())
                {
                    adapter.addSearchedUser(singleSearchedUser);
                }
            }
        }
    }*/


    private void toggleEmptyView()
    {
        if (adapter.getItemCount() > 0)
        {
            flEmptyViewSearchFriend.setVisibility(View.GONE);
        } else
        {
            flEmptyViewSearchFriend.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
