package com.app.agrizip.friends.friend_request;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.friends.friend_request.adapter.PendingRequestsAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.PendingRequests;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Rakshit on 18-11-2016 at 17:53.
 */

public class FragmentRequests extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = App.APP_TAG + FragmentRequests.class.getSimpleName();

    PendingRequestsAdapter adapter;

    @BindView(R.id.rv_pendingFriendRequest)
    RecyclerView rvPendingFriendRequest;

    @BindView(R.id.fl_emptyPendingRequest)
    FrameLayout flEmptyPendingRequest;

    @BindView(R.id.srl_refreshPendingRequest)
    SwipeRefreshLayout srlRefreshPendingRequest;

    @BindView(R.id.lly_name)
    LinearLayout lly_name;

    int pageNo = 1;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    LinearLayoutManager llm;

    private boolean loading = false;

    public static FragmentRequests newInstance(int index) {
        FragmentRequests fragment = new FragmentRequests();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_requests, container, false);

        ButterKnife.bind(this, rootView);

        llm = new LinearLayoutManager(getActivity());

        rvPendingFriendRequest.setLayoutManager(llm);

        rvPendingFriendRequest.setHasFixedSize(true);

        adapter = new PendingRequestsAdapter(getActivity());

        rvPendingFriendRequest.setAdapter(adapter);

        rvPendingFriendRequest.addOnScrollListener(new OnScrollRecyclerView());
        srlRefreshPendingRequest.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshPendingRequest.setOnRefreshListener(this);

        return rootView;
    }

    public void getPendingFriendRequests() {

        loading = true;

        Log.i(TAG, "GETTING PENDING REQUEST FOR PAGE NO: " + pageNo);

        final String userId = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString(Constants.USER_ID, "");

        if (!userId.isEmpty()) {
            API.getInstance().getPendingFriendRequest(userId, pageNo,
                    new RetrofitCallbacks<PendingRequests>(getActivity()) {
                        @Override
                        public void onResponse(Call<PendingRequests> call, Response<PendingRequests> response) {
                            super.onResponse(call, response);
                            if (response.isSuccessful()) {
                                if (pageNo == 1) {
                                    adapter.clear();
                                }
                                for (PendingRequests.DataBean request : response.body().getData()) {
                                    adapter.addSearchedUser(request);
                                }
                                if (adapter.getItemCount() > 0) {
                                    flEmptyPendingRequest.setVisibility(View.GONE);
                                    lly_name.setVisibility(View.VISIBLE);
                                }
                                if (adapter.getItemCount() == 0) {
                                    lly_name.setVisibility(View.GONE);
                                }
                            }

                            loading = false;
                            if (srlRefreshPendingRequest.isRefreshing()) {
                                srlRefreshPendingRequest.setRefreshing(false);
                            }
                        }

                        @Override
                        public void onFailure(Call<PendingRequests> call, Throwable t) {
                            super.onFailure(call, t);
                            loading = false;
                            if (srlRefreshPendingRequest.isRefreshing()) {
                                srlRefreshPendingRequest.setRefreshing(false);
                            }
                        }
                    });
        }

    }

    @Override
    public void onRefresh() {
        if (!loading) {
            getPendingFriendRequests();
        }
    }

    class OnScrollRecyclerView extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (!loading) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        pageNo++;
                        getPendingFriendRequests();
                    }
                }

            }
        }

    }

}
