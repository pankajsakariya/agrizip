package com.app.agrizip.friends.friend_tag;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.friends.friend_tag.adapter.TagFriendListAdapter;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;
import com.google.android.gms.vision.text.Text;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FriendList;
import apimodels.FriendTagId;
import apimodels.SinglePost;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityTagFriends extends AppCompatActivity implements TagFriendListAdapter.TagFriendListener, TextWatcher {

    private static final String TAG = App.APP_TAG + ActivityTagFriends.class.getSimpleName();
    final ArrayList<String> selectedFriendIds = new ArrayList<>();
    final ArrayList<String> selectedFriendNames = new ArrayList<>();
    final List<FriendList.DataBean> allFriends = new LinkedList<>();
    final List<FriendList.DataBean> searchedUsers = new LinkedList<>();
    String sedt_searchuser;
    @BindView(R.id.et_searchUser)
    EditText etSearchUser;

    @BindView(R.id.tv_nosearch)
    TextView tv_nosearch;

    @BindView(R.id.tv_noFriendsFound)
    TextView tv_noFriendsFound;

    @BindView(R.id.rv_tagFriendList)
    RecyclerView rvTagFriendList;
    TagFriendListAdapter adapter;
    int pageNo = 1;

    int pastVisibleItems, visibleItemCount, totalItemCount;
    LinearLayoutManager llm;
    String userId;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private boolean loading = true;
    String Come_from_post;
    String PostId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_friends);

        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }
        tv_noFriendsFound = (TextView) findViewById(R.id.tv_noFriendsFound);
        tv_nosearch = (TextView) findViewById(R.id.tv_nosearch);
        etSearchUser.addTextChangedListener(this);
        sedt_searchuser = etSearchUser.getText().toString();
        Log.e("serach,", "" + sedt_searchuser);
        Come_from_post = getIntent().getStringExtra("COME_FROM_POST");
        if(Come_from_post.equalsIgnoreCase("1")){
            selectedFriendIds.addAll(getIntent().getStringArrayListExtra(Constants.TAG_FRIEND_LIST));
            selectedFriendNames.addAll(getIntent().getStringArrayListExtra(Constants.TAG_FRIEND_NAME_LIST));
        }else{
            PostId = getIntent().getStringExtra("POST_ID");
            selectedFriendIds.addAll(getIntent().getStringArrayListExtra(Constants.TAG_FRIEND_LIST));
            selectedFriendNames.addAll(getIntent().getStringArrayListExtra(Constants.TAG_FRIEND_NAME_LIST));

        }


        llm = new LinearLayoutManager(this);

        rvTagFriendList.setLayoutManager(llm);
        rvTagFriendList.setHasFixedSize(true);

        adapter = new TagFriendListAdapter(this, this);

        rvTagFriendList.setAdapter(adapter);

        userId = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(Constants.USER_ID, "");
        if (rvTagFriendList.isActivated()) {
            tv_nosearch.setVisibility(View.GONE);
            // tv_noFriendsFound.setVisibility(View.VISIBLE);
        }
        if (!userId.isEmpty()) {
            getFriendList();
        }

    }

    private void getFriendList() {
        API.getInstance().getFriendList(userId, pageNo, new RetrofitCallbacks<FriendList>(this) {

            @Override
            public void onResponse(Call<FriendList> call, Response<FriendList> response) {
                super.onResponse(call, response);
                findViewById(R.id.pb_loadingFriendsForTagging).setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (FriendList.DataBean singleTagFriend : response.body().getData()) {
                        if (selectedFriendIds.contains(singleTagFriend.getFriendUserId())) {
                            singleTagFriend.setIsChecked(true);
                        }
                        allFriends.add(singleTagFriend);
                        adapter.addFriend(singleTagFriend);

                    }
                } else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                    findViewById(R.id.tv_noFriendsFound).setVisibility(View.VISIBLE);
                }
            }

        });

    }


    @Override
    public void onSelectedFriendForTagging(final String friendId, final String friendName) {
        if (!selectedFriendIds.contains(friendId)) {
            Log.i(TAG, "ADDING: " + friendName + " WITH ID: " + friendId);
            selectedFriendIds.add(friendId);
            selectedFriendNames.add(friendName);
        }
    }

    @Override
    public void onDeselectedFriendForTagging(final String friendId, final String friendName) {
        if (selectedFriendIds.contains(friendId)) {
            Log.i(TAG, "REMOVING: " + friendName + " WITH ID: " + friendId);
            selectedFriendIds.remove(friendId);
            selectedFriendNames.remove(friendName);

        }
    }


    @Override
    public void finish() {
        // if (selectedFriendIds.size() > 0) {

        Intent i = new Intent();
        i.putStringArrayListExtra(Constants.TAG_FRIEND_LIST, selectedFriendIds);
        i.putStringArrayListExtra(Constants.TAG_FRIEND_NAME_LIST, selectedFriendNames);
        setResult(Activity.RESULT_OK, i);
        // }

        super.finish();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        tv_nosearch.setVisibility(View.GONE);
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().length() > 0) {
            searchedUsers.clear();
            adapter.clear();
            for (FriendList.DataBean singleFriend : allFriends) {
                if (singleFriend.getFullName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                    searchedUsers.add(singleFriend);
                    tv_nosearch.setVisibility(View.GONE);
                } else if (!sedt_searchuser.equals(searchedUsers)) {
                    tv_nosearch.setVisibility(View.VISIBLE);
                    tv_nosearch.setText("Sorry we did not Found any Friend with this " + sedt_searchuser + "KeyWord");
                }
            }

            for (FriendList.DataBean singleFriend : searchedUsers) {
                adapter.addFriend(singleFriend);

            }

        } else {
            adapter.clear();
            for (FriendList.DataBean singleFriend : allFriends) {
                adapter.addFriend(singleFriend);

            }
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_tag_friends, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // finish();
            //onBackPressed();
            super.finish();
        }
        if (item.getItemId() == R.id.action_done) {
            if(Come_from_post.equalsIgnoreCase("1")){
                finish();
            }else{
              TagFriend();
                super.finish();
            }



        }
        return super.onOptionsItemSelected(item);
    }


    public void TagFriend(){
        if(selectedFriendIds.size() > 0){
            List<FriendTagId.FriendUserID> tagIds = new ArrayList<>();
            FriendTagId friendTagId = new FriendTagId();
            for (String friendId : selectedFriendIds) {
              final FriendTagId.FriendUserID tag = new FriendTagId.FriendUserID();
                tag.setFriendUserID(friendId);
                tagIds.add(tag);

            }
            friendTagId.setUserID(App.getInstance().getUser().getId());
            friendTagId.setStatusId(PostId);
            friendTagId.setFriendUserID(tagIds);

            final RetrofitCallbacks<ResponseBody> AddTagintoPost = new RetrofitCallbacks<ResponseBody>(this) {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    super.onResponse(call, response);
                    if (response.isSuccessful()) {


                    } else {
                       //Toast.makeText(Ac, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                    }
                }
            };

            API.getInstance().AddTagintoPost(friendTagId,AddTagintoPost);

        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem settingsMenuItem = menu.findItem(R.id.action_done);
        SpannableString s = new SpannableString(settingsMenuItem.getTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            s.setSpan(new ForegroundColorSpan(getColor(R.color.white)), 0, s.length(), 0);
        }
        settingsMenuItem.setTitle(s);

        return super.onPrepareOptionsMenu(menu);
    }

    class OnScrollRecyclerView extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (loading) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        loading = false;
                        pageNo++;
                        getFriendList();
                    }
                }

            }
        }

    }
}
