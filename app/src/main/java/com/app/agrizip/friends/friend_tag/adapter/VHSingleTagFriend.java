package com.app.agrizip.friends.friend_tag.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.squareup.picasso.Picasso;

import apimodels.FriendList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 11/30/2016 at 6:19 PM.
 */

public class VHSingleTagFriend extends RecyclerView.ViewHolder
{

    final Context context;

    final TagFriendListAdapter.TagFriendListener tagFriendListener;

    @BindView(R.id.ll_singleTagFrnds)
    LinearLayout ll_singleTagFrnds;
    @BindView(R.id.cb_selectForTag)
    CheckBox cbSelectedFriend;
    @BindView(R.id.tv_tagFriendName)
    TextView tvFullName;
    @BindView(R.id.iv_tagFriendPic)
    PorterShapeImageView ivUserPic;
    private FriendList.DataBean singleTagFriend;

    public VHSingleTagFriend(Context context, View itemView, final TagFriendListAdapter.TagFriendListener tagFriendListener)
    {
        super(itemView);
        this.context = context;
        this.tagFriendListener = tagFriendListener;

        ButterKnife.bind(this, itemView);
        cbSelectedFriend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){

                    tagFriendListener.onDeselectedFriendForTagging(singleTagFriend.getFriendUserId(),
                            singleTagFriend.getFullName());
                }else{

                    tagFriendListener.onSelectedFriendForTagging(singleTagFriend.getFriendUserId(),
                            singleTagFriend.getFullName());
                }

            }
        });
       ll_singleTagFrnds.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               boolean is_frd_tagged = cbSelectedFriend.isChecked();
               /*if (is_frd_tagged)
               {
                   singleTagFriend.setIsChecked(is_frd_tagged);
                   tagFriendListener.onSelectedFriendForTagging(singleTagFriend.getFriendUserId(),
                           singleTagFriend.getFullName());
               } else
               {
                   singleTagFriend.setIsChecked(is_frd_tagged);
                   tagFriendListener.onDeselectedFriendForTagging(singleTagFriend.getFriendUserId(),
                           singleTagFriend.getFullName());
               }*/
               if(is_frd_tagged){
                   cbSelectedFriend.setChecked(false);
                   singleTagFriend.setIsChecked(false);
                   tagFriendListener.onDeselectedFriendForTagging(singleTagFriend.getFriendUserId(),
                           singleTagFriend.getFullName());
               }else{
                   cbSelectedFriend.setChecked(true);
                   singleTagFriend.setIsChecked(true);
                   tagFriendListener.onSelectedFriendForTagging(singleTagFriend.getFriendUserId(),
                           singleTagFriend.getFullName());
               }
           }
       });

       /* cbSelectedFriend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
            {
                singleTagFriend.setIsChecked(isChecked);
                if (isChecked)
                {
                    tagFriendListener.onSelectedFriendForTagging(singleTagFriend.getFriendUserId(),
                            singleTagFriend.getFullName());
                } else
                {
                    tagFriendListener.onDeselectedFriendForTagging(singleTagFriend.getFriendUserId(),
                            singleTagFriend.getFullName());
                }
            }
        });
*/
    }

    public static VHSingleTagFriend create(final Context context, final ViewGroup parent, TagFriendListAdapter.TagFriendListener tagFriendListener)
    {
        return new VHSingleTagFriend(context, LayoutInflater.from(context)
                .inflate(R.layout.single_tag_friend, parent, false), tagFriendListener);
    }

    public static void bind(final VHSingleTagFriend vh, FriendList.DataBean singleTagFriend)
    {
        vh.singleTagFriend = singleTagFriend;

        vh.tvFullName.setText(vh.singleTagFriend.getFullName());
        vh.cbSelectedFriend.setChecked(vh.singleTagFriend.getIsChecked());

        Picasso.with(vh.context).load(vh.singleTagFriend.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)

                .into(vh.ivUserPic);

    }


    /**private void loadImage(final String base64String)
     {
     new VHSingleTagFriend.BitmapWorkerTask(ivUserPic, base64String)
     .execute(0);
     }


     class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap>
     {
     private final WeakReference<ImageView> imageViewReference;
     private int data = 0;

     private final String base64String;

     public BitmapWorkerTask(ImageView imageView, final String base64String)
     {
     // Use a WeakReference to ensure the ImageView can be garbage collected
     imageViewReference = new WeakReference<ImageView>(imageView);
     this.base64String = base64String;
     }

     // Decode image in background.
     @Override protected Bitmap doInBackground(Integer... params)
     {
     data = params[0];
     if (base64String != null)
     {
     return Utils.getBitmapFromBase64(base64String);
     }
     return null;
     }

     // Once complete, see if ImageView is still around and set bitmap.
     @Override protected void onPostExecute(Bitmap bitmap)
     {
     if (imageViewReference != null && bitmap != null)
     {
     final ImageView imageView = imageViewReference.get();
     if (imageView != null)
     {
     imageView.setImageBitmap(bitmap);
     }
     }
     }
     }*/


}
