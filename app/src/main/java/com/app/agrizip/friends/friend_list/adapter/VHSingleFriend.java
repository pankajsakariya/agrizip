package com.app.agrizip.friends.friend_list.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.followers.activity.ActivityFollowers;
import com.app.agrizip.friends.friend_list.ActivityFriendList;
import com.app.agrizip.post.dialog.DialogFollowerFriend;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import api.API;
import api.RetrofitCallbacks;
import apimodels.BlockUser;
import apimodels.FriendList;
import apimodels.UnfollowUser;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/2/2017 at 11:56 AM.
 */

public class VHSingleFriend extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener, View.OnClickListener, MenuBuilder.Callback ,DialogFollowerFriend.changetextFollow{

    final Context context;
    @BindView(R.id.iv_SingleFriendImage)
    PorterShapeImageView ivSingleFriendImage;
    @BindView(R.id.tv_singleFriendName)
    TextView tvSingleFriendName;
    @BindView(R.id.tv_singleFriendMutualCount)
    TextView tvSingleFriendMutualCount;
    @BindView(R.id.btn_singleFriendExtraOptions)
    Button btnSingleFriendExtraOptions;
    FriendList.DataBean thisFriend;

    //PopupMenu popup;
    MenuPopupHelper popup1;
    MenuPopupHelper popup2;
    MenuBuilder menuBuilder,menuBuilder1;
    int position;

    public VHSingleFriend(Context context, View itemView) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);
      //  menuBuilder = new MenuBuilder()

        menuBuilder = new MenuBuilder(context);
        menuBuilder1 =new MenuBuilder(context);
        new MenuInflater(context).inflate(R.menu.friend_extra_options, menuBuilder);
        new MenuInflater(context).inflate(R.menu.friend_extra_options1, menuBuilder1);
        // popup = new PopupMenu(context, btnSingleFriendExtraOptions);

        popup2 = new MenuPopupHelper(context, menuBuilder1, btnSingleFriendExtraOptions);
        popup2.setForceShowIcon(true);
        menuBuilder1.setCallback(this);
        popup1 = new MenuPopupHelper(context, menuBuilder, btnSingleFriendExtraOptions);
        popup1.setForceShowIcon(true);

        menuBuilder.setCallback(this);
        //popup.getMenuInflater().inflate(R.menu.friend_extra_options, popup.getMenu());

        // popup.setOnMenuItemClickListener(this);

        btnSingleFriendExtraOptions.setOnClickListener(this);

        tvSingleFriendName.setOnClickListener(this);

        ivSingleFriendImage.setOnClickListener(this);

    }

    public static VHSingleFriend create(final Context context, final ViewGroup parent) {
        return new VHSingleFriend(context, LayoutInflater.from(context)
                .inflate(R.layout.single_friend_row_item, parent, false));
    }

    public static void bind(final VHSingleFriend vh, FriendList.DataBean thisFriend, int position) {
        vh.thisFriend = thisFriend;
        vh.position = position;
        if (thisFriend.getMutualFriendCount() > 0) {
            vh.tvSingleFriendMutualCount.setText(thisFriend.getMutualFriendCount() + " " +
                    vh.context.getResources().getString(R.string.mutual_friend));
        } else {
            vh.tvSingleFriendMutualCount.setText("No" + " " +
                    vh.context.getResources().getString(R.string.mutual_friend));
        }


        vh.tvSingleFriendName.setText(thisFriend.getFullName());

        Glide.with(vh.context).load(thisFriend.getUserImage()).asBitmap()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)

                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        vh.ivSingleFriendImage.setImageBitmap(resource);
                    }
                });

    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
       /* switch (item.getItemId()) {
            case R.id.action_unfollow:
                promptUserForUnfollow();
                break;

            case R.id.action_unfriend:
                promptUserForUnfriend();
                break;

            case R.id.action_block:
                promptUserForBlocking();
                break;

        }*/
        return true;
    }

    private void promptUserForUnBlocking() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView title = (TextView)titleView.findViewById(R.id.tv_title);
        title.setText("Unblock");

        new AlertDialog.Builder(context)
                .setTitle("Unblock")
                .setCustomTitle(titleView)
                .setMessage("Are you sure you want to unblock " + thisFriend.getFullName())
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        unblock();
                    }
                })
                .setNegativeButton("CANCEL", null)
                .show();
    }

    private void promptUserForBlocking() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView title = (TextView)titleView.findViewById(R.id.tv_title);
        title.setText("Block");

        new AlertDialog.Builder(context)
                .setTitle("Block")
                .setCustomTitle(titleView)
                .setMessage("Are you sure you want to block " + thisFriend.getFullName())
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        block();
                    }
                })
                .setNegativeButton("CANCEL", null)
                .show();
    }

    private void unblock() {
        final RetrofitCallbacks<ResponseBody> onBlock = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {

                    if (thisFriend.getFriendActionListener() != null) {
                        thisFriend.getFriendActionListener().onBlockFriend(thisFriend);
                        btnSingleFriendExtraOptions.setText("Friends");
                    }
                } else {
                    Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        };

        final BlockUser blockUser = new BlockUser();
        blockUser.setBlockUserID(thisFriend.getFriendUserId());
        blockUser.setUserID(App.getInstance().getUser().getId());
        blockUser.setBlockMessage("This user is Unblocked");


        //API.getInstance().blockUser(blockUser, onBlock);
        API.getInstance().unblockUser(blockUser, onBlock);
    }

    private void block() {

        final RetrofitCallbacks<ResponseBody> onBlock = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    if (thisFriend.getFriendActionListener() != null) {
                        Constants.UPDATE_FLLOWCOUNT = true;
                        thisFriend.getFriendActionListener().onBlockFriend(thisFriend);
                        btnSingleFriendExtraOptions.setText("Unblock");
                    }
                } else {
                    Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        };

        final BlockUser blockUser = new BlockUser();
        blockUser.setBlockUserID(thisFriend.getFriendUserId());
        blockUser.setUserID(App.getInstance().getUser().getId());
        blockUser.setBlockMessage("This user is blocked");


        API.getInstance().blockUser(blockUser, onBlock);

    }

    private void promptUserForUnfollow() {


       /* new AlertDialog.Builder(context)
                .setTitle("Unfollow")
                .setMessage("Are you sure you want to unfollow " + thisFriend.getFullName())
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        unfollow();
                    }
                })
                .setNegativeButton("CANCEL", null)
                .show();*/
    }

    private void promptUserForUnfriend() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView title = (TextView)titleView.findViewById(R.id.tv_title);
        title.setText("Unfriend");


        new AlertDialog.Builder(context)
                .setTitle("Unfriend")
                .setCustomTitle(titleView)
                .setMessage("Are you sure you want to unfriend " + thisFriend.getFullName())
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        unfriend();
                    }
                })
                .setNegativeButton("CANCEL", null)
                .show();

    }

    private void unfollow() {

        final RetrofitCallbacks<ResponseBody> onUnfollow = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    if (thisFriend.getFriendActionListener() != null) {
                        thisFriend.getFriendActionListener().onUnfollow(thisFriend);
                        btnSingleFriendExtraOptions.setText("Follow");
                    }
                } else {
                    Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        };

        final UnfollowUser unfollowUser = new UnfollowUser();
        unfollowUser.setIsFollow(false);
        unfollowUser.setFollowUserId(thisFriend.getUserId());
        unfollowUser.setUserId(App.getInstance().getUser().getId());
        unfollowUser.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        API.getInstance().unfollowUser(unfollowUser, onUnfollow);

    }

    private void unfriend() {

        final RetrofitCallbacks<ResponseBody> onUnfriend = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    if (thisFriend.getFriendActionListener() != null) {
                        Constants.UPDATE_FLLOWCOUNT = true;
                        thisFriend.getFriendActionListener().onUnfriend(thisFriend);
                    }
                } else {
                    Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        };

        API.getInstance().unfriend(App.getInstance().getUser().getId(),
                thisFriend.getFriendUserId(), onUnfriend);

    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_singleFriendExtraOptions) {
            if (App.getInstance().getUser().getId().equalsIgnoreCase(ActivityFriendList.userId)) {
                if (btnSingleFriendExtraOptions.getText().toString().equalsIgnoreCase("Unblock")) {
                    promptUserForUnBlocking();
                } else {
                    if(thisFriend.isIsFollow()){
                        popup1.show();
                    }else {
                        popup2.show();
                    }

                }
            } else {

            }
        } else if (view.getId() == R.id.tv_singleProfileDetailTitle || view.getId() == R.id.iv_SingleFriendImage)
        {
            Intent i = new Intent(context, ActivityUserProfile.class);

            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
            i.putExtra(Constants.USER_ID, thisFriend.getFriendUserId());

            if (App.getInstance().profileNavigationList.size() > 0) {
                if (!App.getInstance().profileNavigationList.get(App.getInstance()
                        .profileNavigationList.size() - 1).equals(thisFriend.getFriendUserId())) {
                    context.startActivity(i);
                }
            } else {
                context.startActivity(i);
            }
        }
        else if(view.getId() == R.id.tv_singleFriendName){
            Intent i = new Intent(context, ActivityUserProfile.class);

            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
            i.putExtra(Constants.USER_ID, thisFriend.getFriendUserId());

            if (App.getInstance().profileNavigationList.size() > 0) {
                if (!App.getInstance().profileNavigationList.get(App.getInstance()
                        .profileNavigationList.size() - 1).equals(thisFriend.getFriendUserId())) {
                    context.startActivity(i);
                }
            } else {
                context.startActivity(i);
            }
        }
    }

    @Override
    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_unfollow:

                if (App.getInstance().getUser().getId().equalsIgnoreCase(ActivityFriendList.userId)) {
                    final DialogFollowerFriend UnFollowFriend =
                            new DialogFollowerFriend(context, thisFriend.getFriendUserId(), DialogFollowerFriend.Purpose.HIDE_POST, thisFriend);
                    UnFollowFriend.mChangeFolloetext = this;
                    UnFollowFriend.show();
                } else {

                }

                //   promptUserForUnfollow();
                break;
            case R.id.action_follow:
                if (App.getInstance().getUser().getId().equalsIgnoreCase(ActivityFriendList.userId)) {
                    final DialogFollowerFriend UnFollowFriend =
                            new DialogFollowerFriend(context, thisFriend.getFriendUserId(), DialogFollowerFriend.Purpose.HIDE_POST, thisFriend);
                    UnFollowFriend.mChangeFolloetext = this;
                    UnFollowFriend.show();
                } else {

                }
                break;

            case R.id.action_unfriend:
                promptUserForUnfriend();
                break;

            case R.id.action_block:
                promptUserForBlocking();
                break;
        }
        return false;
    }

    @Override
    public void onMenuModeChange(MenuBuilder menu) {

    }

    @Override
    public void OnChnageText(boolean isUnFollow) {
        thisFriend.setIsFollow(isUnFollow);
    }
}
