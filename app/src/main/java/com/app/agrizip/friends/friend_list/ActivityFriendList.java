package com.app.agrizip.friends.friend_list;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.friends.friend_list.adapter.FriendListAdapter;
import com.app.agrizip.post.dialog.DialogFollowerFriend;
import com.app.agrizip.profile.ActivityUserProfile;

import java.util.ArrayList;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FriendList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityFriendList extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        FriendList.FriendActionListener {
    @BindView(R.id.rv_friendList)
    RecyclerView rvFriendList;
    @BindView(R.id.fl_emptyPendingRequest)
    FrameLayout fl_emptyPendingRequest;
    @BindView(R.id.srl_refreshFriendList)
    SwipeRefreshLayout srlRefreshFriendList;

    FriendListAdapter adapter;

    public static ArrayList<String> userIdList = new ArrayList<>();

    boolean busyLoadingData = false;

    int pageNo = 1;

    int pastVisibleItems, visibleItemCount, totalItemCount;

    LinearLayoutManager llm;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    public static String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        ButterKnife.bind(this);

        userId = getIntent().getStringExtra(Constants.USER_ID);

         userIdList.add(userId);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(R.string.friends);
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        adapter = new FriendListAdapter(this);

        llm = new LinearLayoutManager(this);

        rvFriendList.setLayoutManager(llm);
        rvFriendList.setHasFixedSize(true);
        rvFriendList.setAdapter(adapter);
        rvFriendList.setItemAnimator(new DefaultItemAnimator());

        rvFriendList.addOnScrollListener(new OnScrollRecyclerView());
        srlRefreshFriendList.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshFriendList.setOnRefreshListener(this);

        getFriendList();
    }

    @Override
    public void onRefresh() {
        if (!busyLoadingData) {
            pageNo = 1;
            getFriendList();
        }
    }

    private void getFriendList() {

        final RetrofitCallbacks<FriendList> onGetFriendList =
                new RetrofitCallbacks<FriendList>(this) {

                    @Override
                    public void onResponse(Call<FriendList> call, Response<FriendList> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            busyLoadingData = false;
                            if (adapter.getItemCount() > 0 && pageNo == 1) {
                                adapter.clear();
                            }
                            for (FriendList.DataBean singleFriend : response.body().getData()) {
                                singleFriend.setFriendActionListener(ActivityFriendList.this);

                                adapter.addSingleFriend(singleFriend);
                            }
                            if (adapter.getItemCount() > 0) {
                                fl_emptyPendingRequest.setVisibility(View.GONE);
                            }
                            if (srlRefreshFriendList.isRefreshing()) {
                                srlRefreshFriendList.setRefreshing(false);
                            }
                        }
                    }
                };


        if (!busyLoadingData) {
            busyLoadingData = true;

            API.getInstance().getFriendList(userIdList.get(userIdList.size() - 1), pageNo,
                    onGetFriendList);

        }

    }

    @Override
    public void onBackPressed() {
        if(userIdList !=null){
            if(userIdList.size() > 0){
                userIdList.remove(userIdList.size() - 1);
            }
        }
        if(Constants.UPDATE_FLLOWCOUNT){
            Intent intent = new Intent(ActivityFriendList.this, ActivityUserProfile.class);
            intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
            intent.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Constants.UPDATE_FLLOWCOUNT = false;
        }else{
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if(userIdList !=null){
                if(userIdList.size() > 0){
                    userIdList.remove(userIdList.size() - 1);
                }
            }

            if(Constants.UPDATE_FLLOWCOUNT){
                Intent intent = new Intent(ActivityFriendList.this, ActivityUserProfile.class);
                intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                intent.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Constants.UPDATE_FLLOWCOUNT = false;
            }else{
                finish();
            }

        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onUnfriend(FriendList.DataBean friend) {
        adapter.removeFriend(friend);
        if(adapter.getItemCount() == 0){
            fl_emptyPendingRequest.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onUnfollow(FriendList.DataBean friend) {
        adapter.removeFriend(friend);

    }

    @Override
    public void onBlockFriend(FriendList.DataBean blockedFriend) {
        adapter.removeFriend(blockedFriend);
        if(adapter.getItemCount() == 0){
            fl_emptyPendingRequest.setVisibility(View.VISIBLE);
        }
    }


    class OnScrollRecyclerView extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (!busyLoadingData) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        pageNo++;
                        getFriendList();
                    }
                }
            }
        }
    }

}
