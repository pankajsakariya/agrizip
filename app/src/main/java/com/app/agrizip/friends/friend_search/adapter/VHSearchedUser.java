package com.app.agrizip.friends.friend_search.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import api.API;
import api.RetrofitCallbacks;
import apimodels.UserSearchResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 11/28/2016 at 4:52 PM.
 */

public class VHSearchedUser extends RecyclerView.ViewHolder implements View.OnClickListener {

    final Context context;

    @BindView(R.id.iv_friendRequestPic)
    PorterShapeImageView ivUserPic;

    @BindView(R.id.tv_requestFrom)
    TextView tvUserName;

    @BindView(R.id.tv_mutualFriends)
    TextView tvMutualFriend;

    @BindView(R.id.btn_addFriend)
    ImageView btnAddFriend;
    @BindView(R.id.btn_cancleFriend)
    Button btn_cancleFriend;
    String friendUserId;

    private VHSearchedUser(final Context context, View itemView) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

        tvUserName.setOnClickListener(this);
        ivUserPic.setOnClickListener(this);

        btnAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String userId = App.getInstance().getUser().getId();

                if (!userId.isEmpty()) {
                    API.getInstance().sendFriendRequest(userId, friendUserId,
                            new RetrofitCallbacks<ResponseBody>(context) {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    super.onResponse(call, response);
                                    if (response.isSuccessful()) {
                                       // btn_cancleFriend.setVisibility(View.VISIBLE);
                                        btnAddFriend.setVisibility(View.GONE);
                                     //   btn_cancleFriend.setText("Cancel Request");
                                        try {
                                            System.out.println(response.body().string());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    super.onFailure(call, t);
                                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });

    }

    public static VHSearchedUser create(final Context context, final ViewGroup parent) {
        return new VHSearchedUser(context, LayoutInflater.from(context)
                .inflate(R.layout.single_searched_user, parent, false));
    }

    public static void bind(final VHSearchedUser vh, final UserSearchResponse.DataBean model) {


        vh.tvUserName.setText(model.getFullName());
        vh.friendUserId = model.getId();

        if (model.getMutualFriend() != null) {
            if (model.getMutualFriend().size() > 0) {
                vh.tvMutualFriend.setText(model.getMutualfriendcount() + " Mutual Friend");
            } else {
                vh.tvMutualFriend.setText("");
            }
        } else {
            vh.tvMutualFriend.setText("");
        }

        if (model.getIsFriend().equals(Constants.UserType.SELF)) {
            vh.tvUserName.setText("You");
            vh.btnAddFriend.setVisibility(View.GONE);
        } else if (model.getIsFriend().equals(Constants.UserType.FRIEND)) {
            vh.btnAddFriend.setVisibility(View.GONE);
        } else if (model.getIsFriend().equals(Constants.UserType.NOT_A_FRIEND)) {
            vh.btnAddFriend.setVisibility(View.VISIBLE);
        } else if (model.getIsFriend().equals(Constants.UserType.FRIEND_REQUEST_ALREADY_SENT)) {
            vh.tvUserName.setText(model.getFullName() + " request pending");
            //vh.tvUserName.setText(model.getFullName() + " waiting for confirmation");
            vh.btnAddFriend.setVisibility(View.GONE);
        }

        Picasso.with(vh.context).load(model.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)

                .into(vh.ivUserPic);

        /**new AsyncTask<Void, Void, Bitmap>()
         {
         @Override protected Bitmap doInBackground(Void... voids)
         {
         if (model.getUserImage() != null)
         {
         return Utils.getBitmapFromBase64(model.getUserImage());
         }
         return null;
         }

         @Override protected void onPostExecute(Bitmap bitmap)
         {
         if (bitmap != null)
         {
         vh.ivUserPic.setImageBitmap(bitmap);
         }
         }
         }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);*/

    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(context, ActivityUserProfile.class);
        i.putExtra(Constants.USER_ID, friendUserId);
        if (friendUserId.equals(App.getInstance().getUser().getId())) {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
        } else {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
        }
        context.startActivity(i);
    }

    /**private void loadImage(final String base64String)
     {
     new BitmapWorkerTask(ivUserPic, base64String)
     .execute(0);
     }*/


    /**class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap>
     {
     private final WeakReference<ImageView> imageViewReference;
     private int data = 0;

     private final String base64String;

     public BitmapWorkerTask(ImageView imageView, final String base64String)
     {
     // Use a WeakReference to ensure the ImageView can be garbage collected
     imageViewReference = new WeakReference<ImageView>(imageView);
     this.base64String = base64String;
     }

     // Decode image in background.
     @Override protected Bitmap doInBackground(Integer... params)
     {
     data = params[0];
     if (base64String != null)
     {
     return Utils.getBitmapFromBase64(base64String);
     }
     return null;
     }

     // Once complete, see if ImageView is still around and set bitmap.
     @Override protected void onPostExecute(Bitmap bitmap)
     {
     if (imageViewReference != null && bitmap != null)
     {
     final ImageView imageView = imageViewReference.get();
     if (imageView != null)
     {
     imageView.setImageBitmap(bitmap);
     }
     }
     }
     }
     */

}
