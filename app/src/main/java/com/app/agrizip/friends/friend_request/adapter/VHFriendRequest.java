package com.app.agrizip.friends.friend_request.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import api.API;
import api.RetrofitCallbacks;
import apimodels.BlockUser;
import apimodels.ConfirmRejectFriendRequest;
import apimodels.PendingRequests;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 11/29/2016 at 6:33 PM.
 */

public class VHFriendRequest extends RecyclerView.ViewHolder {

    final Context context;

    @BindView(R.id.iv_friendRequesterImage)
    PorterShapeImageView ivUserPic;

    @BindView(R.id.tv_friendRequestFrom)
    TextView tvUserName;

    @BindView(R.id.btn_blockFriendRequest)
    TextView btn_blockFriendRequest;

    @BindView(R.id.tv_friendsMutualFriend)
    TextView tvMutualFriend;

    @BindView(R.id.btn_confirmFriendRequest)
    TextView btnConfirm;
    @BindView(R.id.tv_friendRequestFrom1)
    TextView tv_friendRequestFrom1;
    @BindView(R.id.btn_rejectFriendRequest)
    TextView btnReject;

    @BindView(R.id.lly_acc_rej)
    LinearLayout lly_acc_rej;
    @BindView(R.id.lly_name)
    LinearLayout lly_name;
    String friendUserId;

    PendingRequests.DataBean model;

    public VHFriendRequest(final Context context, View itemView) {
        super(itemView);

        this.context = context;

        ButterKnife.bind(this, itemView);

        btn_blockFriendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userId = App.getInstance().getUser().getId();
                final RetrofitCallbacks<ResponseBody> onBlock = new RetrofitCallbacks<ResponseBody>(context) {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            tv_friendRequestFrom1.setText("You Block friend request from " + tv_friendRequestFrom1.getText());
                            tv_friendRequestFrom1.setVisibility(View.VISIBLE);

                            lly_name.setVisibility(View.INVISIBLE);
                            tvMutualFriend.setVisibility(View.INVISIBLE);
                            lly_acc_rej.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

                final BlockUser blockUser = new BlockUser();
                blockUser.setBlockUserID(friendUserId);
                blockUser.setUserID(App.getInstance().getUser().getId());
                blockUser.setBlockMessage("This user is blocked");


                API.getInstance().blockUser(blockUser, onBlock);

            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String userId = App.getInstance().getUser().getId();

                if (!userId.isEmpty()) {
                    final ConfirmRejectFriendRequest rejectFriendRequest = new ConfirmRejectFriendRequest();
                    rejectFriendRequest.setFriendUserId(friendUserId);
                    rejectFriendRequest.setUserId(userId);
                    rejectFriendRequest.setId(model.getId());

                    API.getInstance().rejectFriendRequest(rejectFriendRequest,
                            new RetrofitCallbacks<ResponseBody>(context) {

                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    super.onResponse(call, response);

                                    if (response.isSuccessful()) {
                                        try {
                                            System.out.println(response.body().string());
                                            //   tvUserName.setText("You rejected friend request from " + tvUserName.getText());
                                            tv_friendRequestFrom1.setText("You rejected friend request from " + tv_friendRequestFrom1.getText());
                                            tv_friendRequestFrom1.setVisibility(View.VISIBLE);

                                            lly_name.setVisibility(View.INVISIBLE);
                                            tvMutualFriend.setVisibility(View.INVISIBLE);
                                            lly_acc_rej.setVisibility(View.INVISIBLE);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String userId = App.getInstance().getUser().getId();

                if (!userId.isEmpty()) {
                    final ConfirmRejectFriendRequest confirmRejectFriendRequest = new ConfirmRejectFriendRequest();
                    confirmRejectFriendRequest.setFriendUserId(friendUserId);
                    confirmRejectFriendRequest.setUserId(userId);
                    confirmRejectFriendRequest.setId(model.getId());

                    API.getInstance().confirmFriendRequest(confirmRejectFriendRequest,
                            new RetrofitCallbacks<ResponseBody>(context) {

                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    super.onResponse(call, response);

                                    if (response.isSuccessful()) {
                                        try {
                                            System.out.println(response.body().string());
                                            //tvUserName.setText(tvUserName.getText() + " is now friend with you.");
                                            tv_friendRequestFrom1.setText(tv_friendRequestFrom1.getText() + " is now friend with you.");
                                            //  btnReject.setVisibility(View.INVISIBLE);
                                            //  btnConfirm.setVisibility(View.INVISIBLE);
                                            tv_friendRequestFrom1.setVisibility(View.VISIBLE);
                                            lly_name.setVisibility(View.INVISIBLE);
                                            lly_acc_rej.setVisibility(View.INVISIBLE);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                }
            }
        });

    }

    public static VHFriendRequest create(final Context context, final ViewGroup parent) {
        return new VHFriendRequest(context, LayoutInflater.from(context)
                .inflate(R.layout.single_friend_request, parent, false));
    }

    public static void bind(final VHFriendRequest vh, final PendingRequests.DataBean model) {

        vh.model = model;
        vh.tvMutualFriend.setVisibility(View.VISIBLE);
        vh.lly_acc_rej.setVisibility(View.VISIBLE);
        if(model.getMutualFriendCount() > 0){
            vh.tvMutualFriend.setText(model.getMutualFriendCount() +"Mutual Friend");
        }else {
            vh.tvMutualFriend.setText("No Mutual Friend");
        }

        vh.tvUserName.setText(model.getFullName());
        vh.tv_friendRequestFrom1.setText(model.getFullName());
        vh.friendUserId = model.getUserId();


        Glide.with(vh.context)
                .load(model.getUserImage()).asBitmap()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)

                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        vh.ivUserPic.setImageBitmap(resource);
                    }
                });
        vh.tvMutualFriend.setVisibility(View.VISIBLE);
        vh.lly_acc_rej.setVisibility(View.VISIBLE);


    }
    @OnClick(R.id.iv_friendRequesterImage)
    public void ClickonImage(){
        Intent i = new Intent(context, ActivityUserProfile.class);
        i.putExtra(Constants.USER_ID, model.getUserId());
        if (model.getUserId().equals(App.getInstance().getUser().getId())) {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
        } else {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
        }
        context.startActivity(i);
    }


}
