package com.app.agrizip.friends.friend_search.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import apimodels.UserSearchResponse;

/**
 * Created by rutvik on 11/28/2016 at 4:53 PM.
 */

public class UserSearchAdapter extends RecyclerView.Adapter
{

    final Context context;

    List<UserSearchResponse.DataBean> modelList;

    public UserSearchAdapter(final Context context)
    {
        this.context = context;
        modelList = new LinkedList<>();
    }

    public void addSearchedUser(UserSearchResponse.DataBean searchedUser)
    {
        modelList.add(searchedUser);
        notifyItemInserted(modelList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSearchedUser.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSearchedUser.bind((VHSearchedUser) holder, modelList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return modelList.size();
    }

    public void clear()
    {
        modelList.clear();
        notifyDataSetChanged();
    }

}
