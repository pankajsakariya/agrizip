package com.app.agrizip.friends.friend_request.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import apimodels.PendingRequests;

/**
 * Created by rutvik on 11/29/2016 at 6:31 PM.
 */

public class PendingRequestsAdapter extends RecyclerView.Adapter
{
    final Context context;

    List<PendingRequests.DataBean> modelList;

    public PendingRequestsAdapter(final Context context)
    {
        this.context = context;
        modelList = new LinkedList<>();
    }

    public void addSearchedUser(PendingRequests.DataBean singleFriendRequest)
    {
        modelList.add(singleFriendRequest);
        notifyItemInserted(modelList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHFriendRequest.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHFriendRequest.bind((VHFriendRequest) holder, modelList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return modelList.size();
    }


    public void clear()
    {
        modelList.clear();
        notifyDataSetChanged();
    }

}
