package com.app.agrizip.app;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.multidex.BuildConfig;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.app.agrizip.R;
import com.bumptech.glide.request.target.ViewTarget;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.QBPushManager;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import api.API;
import api.RetrofitCallbacks;
import apimodels.UserInfo;
import apimodels.UserPrivacyDetails;
import extras.Constants;
import io.fabric.sdk.android.Fabric;
import quickblox.QBCallbacks;
import quickblox.QuickBlox;
import quickblox.Toaster;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by rutvik on 11/17/2016 at 4:10 PM.
 */

public class App extends Application implements QBCallbacks.onLoginCallback.LoginListener
{

    public static final String APP_TAG = "AGZ ";
    public static File dir;
    private static App instance;
    public List<String> profileNavigationList = new LinkedList<>();
    private UserInfo.User user;

    private static QBRoster roster;

    private QBRosterListener rosterListener;

    private RosterReadyListener rosterReadyListener;

    public static App getInstance()
    {
        return instance;
    }

    public static boolean hasNetwork()
    {
        return instance.checkIfHasNetwork();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        dir = getCacheDir();

        ViewTarget.setTagId(R.id.glide_tag);

        if (BuildConfig.DEBUG)
        {
            Timber.plant(new Timber.DebugTree());
        }
        initializeQuickBlox();

    }



    private void initializeQuickBlox()
    {
        QBSettings.getInstance().init(getApplicationContext(),
                Constants.QuickBlox.APP_ID, Constants.QuickBlox.AUTH_KEY,
                Constants.QuickBlox.AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(Constants.QuickBlox.ACCOUNT_KEY);
        QBSettings.getInstance().setEnablePushNotification(true);

        QBChatService.setDebugEnabled(true);
        QBChatService.setDefaultAutoSendPresenceInterval(100);

        QBChatService.ConfigurationBuilder chatServiceConfigurationBuilder = new QBChatService.ConfigurationBuilder();
        chatServiceConfigurationBuilder.setSocketTimeout(60); //Sets chat socket's read timeout in seconds
        chatServiceConfigurationBuilder.setKeepAlive(true); //Sets connection socket's keepAlive option.
        chatServiceConfigurationBuilder.setUseTls(true); //Sets the TLS security mode used when making the connection. By default TLS is disabled.
        QBChatService.setConfigurationBuilder(chatServiceConfigurationBuilder);

        initPushManager();
    }

    private void initPushManager() {
       QBPushManager.getInstance().addListener(new QBPushManager.QBSubscribeListener() {
            @Override
            public void onSubscriptionCreated() {
               // Toaster.shortToast("Subscription Created");
                Log.d(APP_TAG, "SubscriptionCreated");
            }

            @Override
            public void onSubscriptionError(Exception e, int resultCode) {
                Log.d(APP_TAG, "SubscriptionError" + e.getLocalizedMessage());
                if (resultCode >= 0) {
                    String error = GoogleApiAvailability.getInstance().getErrorString(resultCode);
                    Log.d(APP_TAG, "SubscriptionError playServicesAbility: " + error);
                }
                Toaster.shortToast(e.getLocalizedMessage());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public UserInfo.User getUser()
    {
        if (user == null)
        {
            final String userJSON = PreferenceManager.getDefaultSharedPreferences(this)
                    .getString(Constants.PREF_USER, "");
            if (!userJSON.isEmpty())
            {
                setUser(new Gson().fromJson(userJSON, UserInfo.User.class));
            }
        }
        return user;
    }

    public void setUser(UserInfo.User user)
    {
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putString(Constants.USER_ID, user.getId())
                .putString(Constants.PREF_USER, new Gson().toJson(user))
                .apply();

        QuickBlox.loginToQuickBlox(user.getMobileNumber(), user.getId(),
                new QBCallbacks.onLoginCallback(this));

        getPrivacyDetails(null);
        this.user = user;
    }

    public boolean checkIfHasNetwork()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void getPrivacyDetails(final GetPrivacyListListener getPrivacyListListener)
    {

        Timber.i("Getting privacy details from server");

        final RetrofitCallbacks<UserPrivacyDetails> callback = new RetrofitCallbacks<UserPrivacyDetails>(this)
        {
            @Override
            public void onResponse(Call<UserPrivacyDetails> call, Response<UserPrivacyDetails> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    user.setUserPrivacyDetails(response.body());
                    if (getPrivacyListListener != null)
                    {
                        getPrivacyListListener.onGetPrivacyList();
                    }
                }
            }
        };

        API.getInstance().getPrivacyDetails(callback);
    }

    @Override
    public void unauthorized(QBResponseException e)
    {
        QuickBlox.registerNewUser(user.getMobileNumber(), user.getId(), user.getFullName(),
                new QBEntityCallback<QBUser>()
                {
                    @Override
                    public void onSuccess(QBUser qbUser, Bundle bundle)
                    {
                        Log.i(APP_TAG, "NEW USER WAS CREATED SUCCESSFULLY!!!");
                        App.this.user.setQuickBloxUser(qbUser);
                        loginToQbChatService(qbUser);
                    }

                    @Override
                    public void onError(QBResponseException e)
                    {
                        Log.i(APP_TAG, "CREATE USER RESPONSE CODE: " + e.getHttpStatusCode());
                        Log.i(APP_TAG, "CREATE USER MESSAGE: " + e.getMessage());

                    }
                });
    }

    @Override
    public void loginSuccessful(final QBUser qbUser, Bundle bundle)
    {
        Log.i(APP_TAG, "loginSuccessful: id:" + qbUser.getId());
        App.this.user.setQuickBloxUser(qbUser);
        loginToQbChatService(qbUser);
        Log.i(APP_TAG, "USER WAS LOGGED IN SUCCESSFULLY!!!");
    }


    private void loginToQbChatService(final QBUser qbUser)
    {
        qbUser.setPassword(user.getId());
        new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... voids)
            {
                try
                {
                    QBChatService.getInstance().login(qbUser);
                    initRoster();
                } catch (XMPPException e)
                {
                    e.printStackTrace();
                } catch (IOException e)
                {
                    e.printStackTrace();
                } catch (SmackException e)
                {
                    e.printStackTrace();
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void initRoster()
    {
        roster = QBChatService.getInstance().getRoster(QBRoster.SubscriptionMode.mutual, new QBSubscriptionListener()
        {
            @Override
            public void subscriptionRequested(int id)
            {
                Log.i(APP_TAG, "subscriptionRequested: FROM: " + id);
                if (roster != null)
                {
                    try
                    {
                        roster.confirmSubscription(id);
                    } catch (SmackException.NotConnectedException e)
                    {
                        e.printStackTrace();
                    } catch (SmackException.NotLoggedInException e)
                    {
                        e.printStackTrace();
                    } catch (XMPPException e)
                    {
                        e.printStackTrace();
                    } catch (SmackException.NoResponseException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });

        sendPresenceOnline();

    }


    private void sendPresenceOnline()
    {
        if (roster != null)
        {
            if (rosterReadyListener != null)
            {
                rosterReadyListener.onRosterReady();
            }

            QBPresence presence = new QBPresence(QBPresence.Type.online, "Hey there I am using AgriZip",
                    1, QBPresence.Mode.available);

            try
            {
                roster.sendPresence(presence);
                Log.i(APP_TAG, "SENDING PRESENCE!!!");
            } catch (SmackException.NotConnectedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public QBRoster getRoster()
    {
        if(roster == null)
        {
            initRoster();
            return roster;
        }
        else {
            return roster;
        }
    }

    public void addRosterListener(QBRosterListener rosterListener)
    {
        Log.i(APP_TAG, "inside addRosterListener: ");
        if (this.rosterListener == null)
        {
            Log.i(APP_TAG, "roster listener is null setting new RosterListener: ");

            this.rosterListener = rosterListener;
            if (roster != null)
            {
                roster.addRosterListener(rosterListener);
            }
        }
    }

    public RosterReadyListener getRosterReadyListener()
    {
        return rosterReadyListener;
    }

    public void setRosterReadyListener(RosterReadyListener rosterReadyListener)
    {
        this.rosterReadyListener = rosterReadyListener;
    }

    public interface GetPrivacyListListener
    {
        void onGetPrivacyList();
    }

    public interface RosterReadyListener
    {
        void onRosterReady();
    }

}
