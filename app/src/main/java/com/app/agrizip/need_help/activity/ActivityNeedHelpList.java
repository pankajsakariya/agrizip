package com.app.agrizip.need_help.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.fragment.FragmentAdvise;
import com.app.agrizip.need_help.fragment.FragmentBuy;
import com.app.agrizip.need_help.fragment.FragmentListJobs;
import com.app.agrizip.need_help.fragment.FragmentSale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityNeedHelpList extends AppCompatActivity implements TabLayout.OnTabSelectedListener
{

    private static final String TAG = App.APP_TAG + ActivityNeedHelpList.class.getSimpleName();

    @BindView(R.id.tl_helpTabs)
    TabLayout tlHelpTabs;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.fl_helpModuleContainer)
    FrameLayout flHelpModuleContainer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_need_help_list)
    RelativeLayout activityNeedHelpList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_help_list);

        ButterKnife.bind(this);

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null)
            {
                if (tvToolbarTitle != null)
                {
                    tvToolbarTitle.setText("Need Help List");
                }

                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        tlHelpTabs.addTab(tlHelpTabs.newTab().setText("Advise"));
        tlHelpTabs.addTab(tlHelpTabs.newTab().setText("Buy"));
        tlHelpTabs.addTab(tlHelpTabs.newTab().setText("Sale"));
        tlHelpTabs.addTab(tlHelpTabs.newTab().setText("Job"));

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_helpModuleContainer, FragmentAdvise.newInstance(this))
                .commitNow();

        tlHelpTabs.addOnTabSelectedListener(this);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab)
    {
        final int tabPosition = tab.getPosition();

        switch (tabPosition)
        {
            case 0:
                Log.i(TAG, "FIRST TAB WAS SELECTED!!!");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_helpModuleContainer, FragmentAdvise.newInstance(this))
                        .commitAllowingStateLoss();
                break;

            case 1:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_helpModuleContainer, FragmentBuy.newInstance(this))
                        .commitAllowingStateLoss();
                break;

            case 2:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_helpModuleContainer, FragmentSale.newInstance(this))
                        .commitAllowingStateLoss();
                break;

            case 3:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_helpModuleContainer, FragmentListJobs.newInstance(this))
                        .commitAllowingStateLoss();
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab)
    {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab)
    {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
