package com.app.agrizip.need_help.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.activity.ActivityNeedHelpFullView;
import com.app.agrizip.need_help.adapter.HelpListAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AdvisePostList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/17/2017 at 6:10 PM.
 */

public class FragmentBuy extends Fragment implements AdvisePostList.HideHelpListener,TextWatcher,ActivityNeedHelpFullView.updatecomment
{

    @BindView(R.id.rv_buy)
    RecyclerView rvBuy;
    @BindView(R.id.rv_Searchbuy)
    RecyclerView rv_Searchbuy;

    @BindView(R.id.ll_loadingJobs)
    LinearLayout llLoadingJobs;
    @BindView(R.id.tv_buyApiResponse)
    TextView tv_buyApiResponse;
    @BindView(R.id.et_searchBuy)
    EditText etsearchBuy;

    HelpListAdapter buyListAdapter,buyListAdapter1;

    private Context context;

    public static FragmentBuy newInstance(final Context context)
    {
        FragmentBuy fragmentBuy = new FragmentBuy();
        fragmentBuy.context = context;

        return fragmentBuy;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_buy, container, false);

        ButterKnife.bind(this, view);

        buyListAdapter = new HelpListAdapter(getActivity());
        buyListAdapter1 = new HelpListAdapter(getActivity());

        rvBuy.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBuy.setHasFixedSize(true);
        rvBuy.setAdapter(buyListAdapter);
        rv_Searchbuy.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_Searchbuy.setHasFixedSize(true);
        rv_Searchbuy.setAdapter(buyListAdapter1);
        etsearchBuy.addTextChangedListener(FragmentBuy.this);
        ActivityNeedHelpFullView.msetComment =this;
        getBuyList();

        return view;
    }

    private void getBuyList()
    {
        final RetrofitCallbacks<AdvisePostList> callback =
                new RetrofitCallbacks<AdvisePostList>(getActivity())
                {

                    @Override
                    public void onResponse(Call<AdvisePostList> call, Response<AdvisePostList> response)
                    {
                        super.onResponse(call, response);
                        llLoadingJobs.setVisibility(View.GONE);
                        if (response.isSuccessful())
                        {
                            for (AdvisePostList.DataBean singleBuyPost : response.body().getData())
                            {
                                if(response.body().getData().size() > 0){
                                    tv_buyApiResponse.setVisibility(View.GONE);
                                }
                                singleBuyPost.setHideHelpListener(FragmentBuy.this);
                                singleBuyPost.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.BUY);
                                buyListAdapter.addAdviseListItem(singleBuyPost);
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<AdvisePostList> call, Throwable t) {
                        super.onFailure(call, t);
                        llLoadingJobs.setVisibility(View.GONE);
                    }
                };

        API.getInstance().getBuyPostList(App.getInstance().getUser().getId(), 1, callback);

    }

    @Override
    public void onhideHelp(AdvisePostList.DataBean singleHelp, int position,int type) {
        buyListAdapter.removeHelp(singleHelp,position);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().length() > 3) {
            searchBuy(charSequence.toString());
        }
        if (charSequence.toString().length() == 0) {
           rv_Searchbuy.setVisibility(View.GONE);
            rvBuy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void searchBuy(String keyword) {

        Context context = getActivity();
         String userid = App.getInstance().getUser().getId();
        API.getInstance().getSearchPostBuyList(userid,keyword,1, new RetrofitCallbacks<AdvisePostList>(context) {

            @Override
            public void onResponse(Call<AdvisePostList> call, Response<AdvisePostList> response) {
                super.onResponse(call, response);

                if (response.isSuccessful()) {
                    buyListAdapter1.clear();
                    if(response.body().getData().size() > 0){
                        rvBuy.setVisibility(View.GONE);
                        rv_Searchbuy.setVisibility(View.VISIBLE);
                        tv_buyApiResponse.setVisibility(View.GONE);
                    //    rvAdvise.setVisibility(View.GONE);
                      //  rv_searchAdvise.setVisibility(View.VISIBLE);
                        for (AdvisePostList.DataBean singleAdvice : response.body().getData()) {

                            // singleAdvice.setHideHelpListener(FragmentAdvise.this);
                            singleAdvice.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.ADVISE);
                            //adviseListAdapter.addAdviseListItem(singleAdvice);
                        //    adviseListAdapter1.addAdviseListItem(singleAdvice);
                            buyListAdapter1.addAdviseListItem(singleAdvice);
                        }
                    }else{
                        rvBuy.setVisibility(View.GONE);
                        rv_Searchbuy.setVisibility(View.GONE);
                        tv_buyApiResponse.setVisibility(View.VISIBLE);
                    }
                   /* adviseListAdapter.clear();
                    for (AdvisePostList.DataBean singleAdvice : response.body().getData()) {
                        if (response.body().getData().size() > 0) {
                            tv_buyApiResponse.setVisibility(View.GONE);
                        }
                       // singleAdvice.setHideHelpListener(FragmentAdvise.this);
                        singleAdvice.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.ADVISE);
                        adviseListAdapter.addAdviseListItem(singleAdvice);
                    }*/
                }

            }

            @Override
            public void onFailure(Call<AdvisePostList> call, Throwable t) {
                super.onFailure(call, t);
            }
        });

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            ActivityNeedHelpFullView.msetComment = this;
        }
    }

    @Override
    public void setCommentInModel(int count, int position) {
        buyListAdapter.changeCommnetCount(position,count);
    }
}
