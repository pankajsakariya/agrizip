package com.app.agrizip.need_help.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.agrizip.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 1/16/2017 at 5:31 PM.
 */

public class FragmentListJobs extends Fragment {

    @BindView(R.id.tl_jobTabs)
    TabLayout tlJobTabs;
    @BindView(R.id.vp_jobs)
    ViewPager vpJobs;
    private Context context;

    public static FragmentListJobs newInstance(final Context context) {
        final FragmentListJobs fragmentListJobs = new FragmentListJobs();

        fragmentListJobs.context = context;

        return fragmentListJobs;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_jobs, container, false);

        ButterKnife.bind(this, view);

        setupViewPager(vpJobs);

        tlJobTabs.setupWithViewPager(vpJobs);

        return view;
    }

    private void setupViewPager(ViewPager vpPhotosAndAlbums) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(FragmentRequireJob.newInstance(getActivity()), "Require Job");
        adapter.addFragment(FragmentRequireEmployee.newInstance(getActivity()), "Require Employee");
        vpPhotosAndAlbums.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
