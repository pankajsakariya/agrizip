package com.app.agrizip.need_help.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.activity.ActivityNeedHelpFullView;
import com.app.agrizip.need_help.adapter.HelpListAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AdvisePostList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/17/2017 at 6:13 PM.
 */

public class FragmentSale extends Fragment implements AdvisePostList.HideHelpListener,TextWatcher,ActivityNeedHelpFullView.updatecomment{

    @BindView(R.id.rv_sale)
    RecyclerView rvSale;
    @BindView(R.id.rv_Searchsale)
    RecyclerView rv_Searchsale;
    @BindView(R.id.ll_loadingJobs)
    LinearLayout llLoadingJobs;
    @BindView(R.id.tv_saleApiResponse)
    TextView tv_saleApiResponse;
    @BindView(R.id.et_searchsale)
    EditText etsearchsale;
    HelpListAdapter saleListAdapter,saleListAdapter1;


    private Context context;

    public static FragmentSale newInstance(final Context context) {
        FragmentSale fragmentSale = new FragmentSale();
        fragmentSale.context = context;
        return fragmentSale;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sale, container, false);

        ButterKnife.bind(this, view);

        saleListAdapter = new HelpListAdapter(getActivity());
        saleListAdapter1 = new HelpListAdapter(getActivity());

        rvSale.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSale.setHasFixedSize(true);
        rvSale.setAdapter(saleListAdapter);
        rv_Searchsale.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_Searchsale.setHasFixedSize(true);
        rv_Searchsale.setAdapter(saleListAdapter1);
        etsearchsale.addTextChangedListener(FragmentSale.this);
        ActivityNeedHelpFullView.msetComment =this;
        getSaleList();

        return view;
    }

    private void getSaleList() {
        final RetrofitCallbacks<AdvisePostList> callback =
                new RetrofitCallbacks<AdvisePostList>(getActivity()) {

                    @Override
                    public void onResponse(Call<AdvisePostList> call, Response<AdvisePostList> response) {
                        super.onResponse(call, response);
                        llLoadingJobs.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            for (AdvisePostList.DataBean single : response.body().getData()) {
                                if (response.body().getData().size() > 0) {
                                    tv_saleApiResponse.setVisibility(View.GONE);
                                }
                                single.setHideHelpListener(FragmentSale.this);
                                single.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.SALE);
                                saleListAdapter.addAdviseListItem(single);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdvisePostList> call, Throwable t) {
                        super.onFailure(call, t);
                        llLoadingJobs.setVisibility(View.GONE);
                    }
                };

        API.getInstance().getSalePostList(App.getInstance().getUser().getId(), 1, callback);
    }

    @Override
    public void onhideHelp(AdvisePostList.DataBean singleHelp, int position, int type) {
        saleListAdapter.removeHelp(singleHelp,position);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().length() > 3) {
            searchSale(charSequence.toString());
        }
        if (charSequence.toString().length() == 0) {
        //    searchSale(charSequence.toString());
            rv_Searchsale.setVisibility(View.GONE);
            rvSale.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
    private void searchSale(String keyword) {
        String userid = App.getInstance().getUser().getId();
        Context context = getActivity();
        API.getInstance().getSearchPostSaleList(userid,keyword,1, new RetrofitCallbacks<AdvisePostList>(context) {

            @Override
            public void onResponse(Call<AdvisePostList> call, Response<AdvisePostList> response) {
                super.onResponse(call, response);

                if (response.isSuccessful()) {
                    saleListAdapter1.clear();
                    // adviseListAdapter1.clear();
                    //  searchHelpListAdapter.clear();
                    if(response.body().getData().size() > 0){
                            rvSale.setVisibility(View.GONE);
                         rv_Searchsale.setVisibility(View.VISIBLE);
                        tv_saleApiResponse.setVisibility(View.GONE);
                        for (AdvisePostList.DataBean singleAdvice : response.body().getData()) {

                            // singleAdvice.setHideHelpListener(FragmentAdvise.this);
                            singleAdvice.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.ADVISE);
                            //adviseListAdapter.addAdviseListItem(singleAdvice);
                                saleListAdapter1.addAdviseListItem(singleAdvice);
                        }
                    }else{
                        rvSale.setVisibility(View.GONE);
                        rv_Searchsale.setVisibility(View.GONE);
                        tv_saleApiResponse.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onFailure(Call<AdvisePostList> call, Throwable t) {
                super.onFailure(call, t);
            }
        });

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            ActivityNeedHelpFullView.msetComment = this;
        }
    }

    @Override
    public void setCommentInModel(int count, int position) {
        saleListAdapter.changeCommnetCount(position,count);
    }
}
