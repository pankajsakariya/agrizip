package com.app.agrizip.need_help.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CreateJobOrEmployee;
import apimodels.JobResponse;
import apimodels.PostStatusResponse;
import apimodels.RequireJobList;
import apimodels.SinglePost;
import apimodels.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Response;
import viewholders.SelectedImage;

import static extras.Constants.REQUEST_CODE_IMAGE_PICKER;

public class ActivityNeedJob extends AppCompatActivity implements Validator.ValidationListener, AdapterView.OnItemSelectedListener, SelectedImage.SelectedImageOperations {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle2)
    TextView tvToolbarTitle;

    @BindView(R.id.ll_needEmployeeField2)
    LinearLayout llNeedEmployeeField2;

    @BindView(R.id.sv_jobCompanyScreen)
    ScrollView svJobCompanyScreen;

    @NotEmpty
    @BindView(R.id.et_ne_city)
    EditText etNeCity;

    @BindView(R.id.et_ne_companyName)
    EditText etNeCompanyName;

    @NotEmpty
    @BindView(R.id.et_ne_district)
    EditText etNeDistrict;

    @NotEmpty
    @BindView(R.id.et_ne_education)
    EditText etNeEducation;

    @BindView(R.id.et_ne_experience)
    EditText etNeExperience;

    @NotEmpty
    @BindView(R.id.et_ne_jobTitle)
    EditText etNeJobTitle;


    @NotEmpty(message = "Mobile No cannot be empty")
    @Length(min = 10, message = "Mobile No should be of 10 digits", trim = true)
    @BindView(R.id.et_ne_mobile)
    EditText etNeMobile;
    SelectedImage selectedImage;
    @NotEmpty
    @BindView(R.id.et_ne_street)
    EditText etNeStreet;
    boolean postingStaus = false;

    /*   @NotEmpty
       @BindView(R.id.et_ne_partOrFullTime)
       EditText etNePartOrFullTime;*/
    @BindView(R.id.sp_full_part_time)
    Spinner sp_full_part_time;
    @NotEmpty
    @BindView(R.id.et_ne_state)
    EditText etNeState;
    final Handler mHandler = new Handler();
    @BindView(R.id.ll_selectedImages)
    LinearLayout llSelectedImages;
    @BindView(R.id.ll_selectedImagesContainer)
    LinearLayout llSelectedImagesContainer;

    @BindView(R.id.btn_uploadResume)
    Button btn_uploadResume;

    @BindView(R.id.et_ne_vacancy)
    EditText etNeVacancy;
    @BindView(R.id.iv_selected_resume)
    ImageView iv_selected_resume;


    @NotEmpty
    @BindView(R.id.et_ne_zipCode)
    EditText etNeZipCode;
    ProgressDialog pd;

    Bitmap bitmap;
    UserInfo.User user;
    private static final String TAG = App.APP_TAG + ActivityPostToAgrizip.class.getSimpleName();
    final Map<String, Bitmap> selectedImageBitmaps = new LinkedHashMap<>();
    final Map<String, String> imageList = new LinkedHashMap<>();
    ArrayList<SinglePost.ImagesBean> imagesToBePosted = new ArrayList<>();
    String resumeImage64BaseFormat = null;
    @NotEmpty
    @Email
    @BindView(R.id.et_ne_email)
    EditText etNeEmail;


    boolean needAJob;
    String img_path;
    Validator validator;
    CreateJobOrEmployee job = new CreateJobOrEmployee();
    SinglePost post = new SinglePost();
    boolean come_from_UserProfile = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_job);

        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);
        selectedImage = new SelectedImage(this, this);
        needAJob = getIntent().getBooleanExtra(Constants.NEED_JOB, false);
        user = ((App) getApplication()).getUser();
        come_from_UserProfile = getIntent().getBooleanExtra("COME_FROM_PROFLE", false);
        if (toolbar != null) {
            if (needAJob) {
                tvToolbarTitle.setText("Need Help For Job");
                llNeedEmployeeField2.setVisibility(View.GONE);
                etNeCompanyName.setVisibility(View.GONE);
            } else {
                tvToolbarTitle.setText("Need Help For Company");
                llNeedEmployeeField2.setVisibility(View.VISIBLE);
                etNeCompanyName.setVisibility(View.VISIBLE);
                btn_uploadResume.setVisibility(View.GONE);
            }
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }

        }

        sp_full_part_time.setOnItemSelectedListener(this);
        List<String> categories = new ArrayList<String>();
        categories.add("Full-Time");
        categories.add("Part-Time");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinner_item_text);

        // attaching data adapter to spinner
        sp_full_part_time.setAdapter(dataAdapter);

        btn_uploadResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImagePicker();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            System.out.println("!! " + requestCode);
            System.out.println("!! " + data);
            if (data != null) {
                final ArrayList<Image> images1 = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                iv_selected_resume.setVisibility(View.VISIBLE);
                img_path = images1.get(0).getPath();
                try{
                    bitmap = Utils.convertImageToBitmap(images1.get(0).getPath());

                    resumeImage64BaseFormat = Utils.getEncoded64ImageStringFromBitmap(bitmap);
                    iv_selected_resume.setImageBitmap(bitmap);

                }catch (OutOfMemoryError e1){
                    e1.printStackTrace();
                    Toast.makeText(this,"Image is Large Size Choose another.",Toast.LENGTH_LONG).show();
                }

                catch (Exception e){
                    e.printStackTrace();
                }

/*

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected void onPreExecute() {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                pd = new ProgressDialog(ActivityNeedJob.this);
                                pd.setTitle("Please Wait...");
                                pd.setMessage("Adding Images...");
                                pd.setIndeterminate(true);
                                pd.show();
                            }
                        });

                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        for (int i = 0; i < images1.size(); i++) {
                            final Image singleImage = images1.get(i);
                            imageList.put(singleImage.getPath(), singleImage.getPath());
                            selectedImageBitmaps.put(singleImage.getPath(), Utils.convertImageToBitmap(singleImage.getPath()));
                            resumeImage64BaseFormat = Utils.getEncoded64ImageStringFromBitmap(bitmap);
                            final SinglePost.ImagesBean ib = new SinglePost.ImagesBean();
                            ib.setImagestr(singleImage.getPath());
                            imagesToBePosted.add(ib);
                            selectedImage.addSingleImage(singleImage.getPath(),
                                    Utils.convertImageToBitmap(singleImage.getPath()));

                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {

                        llSelectedImages.removeAllViews();
                        llSelectedImages.addView(selectedImage.getView());


                  */
/*  if (selectedImageBitmaps.size() > 0) {
                        llSelectedImagesContainer.setVisibility(View.VISIBLE);
                    } else {
                        llSelectedImagesContainer.setVisibility(View.GONE);
                    }*//*

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                pd.dismiss();
                            }
                        });
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

*/

                if (resultCode == REQUEST_CODE_IMAGE_PICKER) {
                    switch (resultCode) {
                        case REQUEST_CODE_IMAGE_PICKER:
                            System.out.println("!!12 " + data);
                            if (data != null) {
                                System.out.println("!! " + data);
                                final ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

                                iv_selected_resume.setImageBitmap(Utils.convertImageToBitmap(images.get(0).getPath()));

                                new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected void onPreExecute() {
                                        mHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                pd = new ProgressDialog(ActivityNeedJob.this);
                                                pd.setTitle("Please Wait...");
                                                pd.setMessage("Adding Images...");
                                                pd.setIndeterminate(true);
                                                pd.show();
                                            }
                                        });

                                    }

                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        for (int i = 0; i < images.size(); i++) {
                                            final Image singleImage = images.get(i);
                                            imageList.put(singleImage.getPath(), singleImage.getPath());
                                            selectedImageBitmaps.put(singleImage.getPath(), Utils.convertImageToBitmap(singleImage.getPath()));
                                            final SinglePost.ImagesBean ib = new SinglePost.ImagesBean();
                                            ib.setImagestr(singleImage.getPath());
                                            imagesToBePosted.add(ib);
                                            selectedImage.addSingleImage(singleImage.getPath(),
                                                    Utils.convertImageToBitmap(singleImage.getPath()));

                                        }

                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void aVoid) {

                                        llSelectedImages.removeAllViews();
                                        llSelectedImages.addView(selectedImage.getView());


                                        if (selectedImageBitmaps.size() > 0) {
                                            llSelectedImagesContainer.setVisibility(View.VISIBLE);
                                        } else {
                                            llSelectedImagesContainer.setVisibility(View.GONE);
                                        }
                                        mHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                pd.dismiss();
                                            }
                                        });
                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }

                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openImagePicker() {

        ImagePicker.create(this)
                .folderMode(false) // folder mode (false by default)
                .folderTitle("Select Photos") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .multi() // multi mode (default mode)
                .limit(1) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .start(REQUEST_CODE_IMAGE_PICKER); // start image picker activity with request code


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_post_to_agrizip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_post) {
            validator.validate();
        }
        return super.onOptionsItemSelected(item);
    }

    private void tryPostingNewJobOrEmployeeRequest() {
        pd = new ProgressDialog(ActivityNeedJob.this);
        pd.setTitle("Please Wait...");
        pd.show();
        // post.setUserID(App.getInstance().getUser().getId());
        SinglePost.JobDetailsBean jobDetailsBean = new SinglePost.JobDetailsBean();
        post.setUserID(App.getInstance().getUser().getId());
        post.setIsJobPost(true);
        //jobDetailsBean.setIsWantJob(true);
        jobDetailsBean.setJobTitle(etNeJobTitle.getText().toString());
        jobDetailsBean.setEmail(etNeEmail.getText().toString());
        jobDetailsBean.setEducation(etNeEducation.getText().toString());
        jobDetailsBean.setJobDuration(sp_full_part_time.getSelectedItem().toString());
        jobDetailsBean.setCity(etNeCity.getText().toString());
        jobDetailsBean.setStreet(etNeStreet.getText().toString());
        jobDetailsBean.setState(etNeState.getText().toString());
        jobDetailsBean.setDistrict(etNeDistrict.getText().toString());
        jobDetailsBean.setZipCode(etNeZipCode.getText().toString());
        if (!needAJob) {
            jobDetailsBean.setIsWantJob(false);
            jobDetailsBean.setIsWantEmployee(true);
            jobDetailsBean.setCompanyName(etNeCompanyName.getText().toString());
            jobDetailsBean.setVacancy(etNeVacancy.getText().toString());
            jobDetailsBean.setExperience(etNeExperience.getText().toString());
            jobDetailsBean.setMobile(etNeMobile.getText().toString());


        } else {
            jobDetailsBean.setIsWantJob(true);
            jobDetailsBean.setIsWantEmployee(false);
            jobDetailsBean.setMobile(etNeMobile.getText().toString());


        }
        if (resumeImage64BaseFormat != null) {
            jobDetailsBean.setResume(img_path);
            // job.setResume(resumeImage64BaseFormat);
        } else {
            jobDetailsBean.setResume("");

        }
        post.setJobDetails(jobDetailsBean);
        post.setSharedOwner("");
        post.setStatus("");
        post.setUserName(App.getInstance().getUser().getFullName());
        post.setUserImage(App.getInstance().getUser().getUserImage());
     /*   post.getJobDetails().setIsWantJob(true);
        post.getJobDetails().setJobTitle(etNeJobTitle.getText().toString());
        post.getJobDetails().setEmail(etNeEmail.getText().toString());
        post.getJobDetails().setMobile(etNeMobile.getText().toString());
        post.getJobDetails().setEducation(etNeEducation.getText().toString());
        post.getJobDetails().setJobDuration(sp_full_part_time.getSelectedItem().toString());
        post.getJobDetails().setCity(etNeCity.getText().toString());
        post.getJobDetails().setStreet(etNeStreet.getText().toString());
        post.getJobDetails().setState(etNeState.getText().toString());
        post.getJobDetails().setDistrict(etNeDistrict.getText().toString());
        post.getJobDetails().setZipCode(etNeZipCode.getText().toString());
*/

        job.setUserId(App.getInstance().getUser().getId());
        job.setJobTitle(etNeJobTitle.getText().toString());
        job.setEmail(etNeEmail.getText().toString());
        job.setMobile(etNeMobile.getText().toString());
        job.setEducation(etNeEducation.getText().toString());
        // job.setJobDuration(etNePartOrFullTime.getText().toString());
        job.setJobDuration(sp_full_part_time.getSelectedItem().toString());
        job.setStreet(etNeStreet.getText().toString());
        job.setCity(etNeCity.getText().toString());
        job.setDistrict(etNeDistrict.getText().toString());
        job.setState(etNeState.getText().toString());
        job.setZipCode(etNeZipCode.getText().toString());
        if (resumeImage64BaseFormat != null) {
            job.setResume(resumeImage64BaseFormat);
        }
        if (!needAJob) {
            job.setIsWantEmployee(true);
            job.setIsWantJob(false);
            if (etNeCompanyName.getText().toString().isEmpty() ||
                    etNeVacancy.getText().toString().isEmpty() ||
                    etNeExperience.getText().toString().isEmpty()) {

                if (etNeCompanyName.getText().toString().isEmpty()) {
                    etNeCompanyName.setError("This field is required");

                } else {
                    etNeCompanyName.setError(null);
                    // job.setIsWantEmployee(false);
                    //job.setIsWantJob(true);
                }

                if (etNeVacancy.getText().toString().isEmpty()) {
                    etNeVacancy.setError("This field is required");
                } else {
                    etNeVacancy.setError(null);
                }

                if (etNeExperience.getText().toString().isEmpty()) {
                    etNeExperience.setError("This field is required");
                } else {
                    etNeExperience.setError(null);
                }

                return;
            }

            job.setCompanyName(etNeCompanyName.getText().toString());
            job.setVacancy(etNeVacancy.getText().toString());
            job.setExperience(etNeExperience.getText().toString());

        } else {
            job.setIsWantEmployee(false);
            job.setIsWantJob(true);
        }

     /*   API.getInstance().createNewStatusPost(post,
                new RetrofitCallbacks<PostStatusResponse>(ActivityNeedJob.this){
                    @Override
                    public void onResponse(Call<PostStatusResponse> call, Response<PostStatusResponse> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            if (response.body().getResponseStatus() == 1) {
                                Toast.makeText(ActivityNeedJob.this, "Job posted successfully", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PostStatusResponse> call, Throwable t) {


                    }
                } );
*/
  /*      final RetrofitCallbacks<ResponseBody> onCreateJonCallback =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            if (needAJob) {
                                System.out.println("!! " + response.body());
                               if (response.body().getResponseStatus() == 1) {
                            } else {
                                Toast.makeText(ActivityNeedJob.this, "Employee request posted successfully", Toast.LENGTH_SHORT).show();
                            }
                            finish();
                        } else {
                            Toast.makeText(ActivityNeedJob.this, "something went wrong try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                };*/
        final RetrofitCallbacks<JobResponse> onCreateJonCallback =
                new RetrofitCallbacks<JobResponse>(this) {
                    @Override
                    public void onResponse(Call<JobResponse> call, Response<JobResponse> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {

                            if (needAJob) {
                                pd.dismiss();
                                System.out.println("!! " + response.body());
                              //  post.setStatusId(response.body().getData().get(0).getJobId());
                                post.setStatusId(response.body().getData().getJobId());
                                user.getUserEventCallbacks().onStatusPostSuccessful(post);

                                Toast.makeText(ActivityNeedJob.this, "Job posted successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                pd.dismiss();
                              post.setStatusId(response.body().getData().getJobId());
                                user.getUserEventCallbacks().onStatusPostSuccessful(post);
                                Toast.makeText(ActivityNeedJob.this, "Employee request posted successfully", Toast.LENGTH_SHORT).show();
                            }
                            finish();


                        } else {
                            pd.dismiss();
                            postingStaus = false;
                            Toast.makeText(ActivityNeedJob.this, "something went wrong try again later", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JobResponse> call, Throwable t) {
                        super.onFailure(call, t);
                        postingStaus = false;
                    }
                };
        API.getInstance().createJobOrEmployee(job, onCreateJonCallback);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();


        // Showing selected spinner item
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onValidationSucceeded() {
        if(postingStaus == false){
            postingStaus = true;
        tryPostingNewJobOrEmployeeRequest();
        }else{
            Toast.makeText(this,"Wait Your JobPost is Uploading..",Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void removeSelectedImage(String imagePath) {
        selectedImageBitmaps.remove(imagePath);
        imageList.remove(imagePath);
        if (selectedImageBitmaps.isEmpty() && imageList.isEmpty()) {
            llSelectedImagesContainer.setVisibility(View.GONE);
        }
        Log.i(TAG, "SELECTED IMAGE BITMAP SIZE: " + selectedImageBitmaps.size());
        Log.i(TAG, "imageList SIZE: " + imageList.size());
    }

    @Override
    public void onRemoveAllImages() {
        selectedImageBitmaps.clear();
        imageList.clear();
        llSelectedImagesContainer.setVisibility(View.GONE);
    }
}
