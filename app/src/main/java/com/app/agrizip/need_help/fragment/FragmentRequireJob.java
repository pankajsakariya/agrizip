package com.app.agrizip.need_help.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.activity.ActivityJobOrEmployeeFullView;
import com.app.agrizip.need_help.adapter.HelpListAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.RequireJobList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/16/2017 at 5:52 PM.
 */

public class FragmentRequireJob extends Fragment implements RequireJobList.JobEmployeeHidePost, ActivityJobOrEmployeeFullView.setJobCount {

    @BindView(R.id.rv_jobs)
    RecyclerView rvJobs;

    HelpListAdapter jobListAdapter;
    @BindView(R.id.ll_loadingJobs)
    LinearLayout llLoadingJobs;
    private Context context;

    @BindView(R.id.tv_buyApiResponse)
    TextView tv_buyApiResponse;

    public static FragmentRequireJob newInstance(final Context context) {
        FragmentRequireJob fragmentRequireJob = new FragmentRequireJob();

        fragmentRequireJob.context = context;

        return fragmentRequireJob;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_require_job, container, false);

        ButterKnife.bind(this, view);

        jobListAdapter = new HelpListAdapter(getActivity());

        rvJobs.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvJobs.setHasFixedSize(true);
        rvJobs.setAdapter(jobListAdapter);

        ActivityJobOrEmployeeFullView.mjobcount = this;
        getRequireJobList();

        return view;
    }


    private void getRequireJobList() {
        final RetrofitCallbacks<RequireJobList> onGetJobListCallback =
                new RetrofitCallbacks<RequireJobList>(getActivity()) {
                    @Override
                    public void onResponse(Call<RequireJobList> call, Response<RequireJobList> response) {
                        super.onResponse(call, response);
                        llLoadingJobs.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            for (RequireJobList.DataBean singleJob : response.body().getData()) {
                                if (response.body().getData().size() > 0) {
                                    tv_buyApiResponse.setVisibility(View.GONE);
                                }
                                singleJob.setJobEmployeeHidePost(FragmentRequireJob.this);
                                jobListAdapter.addRequireJob(singleJob);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RequireJobList> call, Throwable t) {
                        super.onFailure(call, t);
                        llLoadingJobs.setVisibility(View.GONE);
                    }
                };

        API.getInstance().getRequireJobList(App.getInstance().getUser().getId(), 1, onGetJobListCallback);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            ActivityJobOrEmployeeFullView.mjobcount = this;
        }
    }

    @Override
    public void OnHideJobEmployeePost(int position) {
        jobListAdapter.removeJobEmployeePost(position,0);
    }

    @Override
    public void OnsetnewCommentCount(int count, int position) {
        jobListAdapter.changeJobCommentCount(position,count);
    }
}
