package com.app.agrizip.need_help.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.post.post_comments.adapter.CommentsAdapter;
import com.app.agrizip.post.post_like_list.dialog.LikeListDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AdvisePostList;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import apimodels.LikeListResponse;
import apimodels.LikeUnlikePost;
import apimodels.LikeUnlikeResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.MyLinearLayoutManager;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityNeedHelpFullView extends AppCompatActivity implements CommentsAdapter.changeCommentcount {

    private static final String TAG = App.APP_TAG + ActivityNeedHelpFullView.class.getSimpleName();

    @BindView(R.id.iv_helpFvUserPic)
    ImageView ivHelpFvUserPic;

    @BindView(R.id.iv_helpFvImage)
    ImageView ivHelpFvImage;

    @BindView(R.id.tv_helpFvContent)
    TextView tvHelpFvContent;

    @BindView(R.id.tv_helpFvRequestTitle)
    TextView tvHelpFvRequestTitle;

    @BindView(R.id.tv_feedCommentCount2)
    TextView tv_feedCommentCount2;

    @BindView(R.id.tv_helpFvTime)
    TextView tvHelpFvTime;

    @BindView(R.id.tv_helpFvUserLocation)
    TextView tvHelpFvUserLocation;

    @BindView(R.id.rv_helpComments)
    RecyclerView rvHelpComments;

    @BindView(R.id.tv_helpCommentCount)
    TextView tvHelpCommentCount;
    @BindView(R.id.tv_feedViewCount)
    TextView tv_feedViewCount;
    @BindView(R.id.iv_postNewComment_help)
    ImageView ivPostNewComment;

    @BindView(R.id.et_newCommentBody)
    EditText etNewCommentBody;

    @BindView(R.id.pb_commentLoding)
    RelativeLayout pb_commentLoding;


    @BindView(R.id.iv_addImageToNewComment)
    ImageView ivAddImageToNewComment;

    @BindView(R.id.tv_helpFvContent_select)
    TextView tv_helpFvContent_select;

    @BindView(R.id.tv_feedLikeCount_advice)
    TextView tv_feedLikeCount_help;

    @BindView(R.id.tv_likehelp)
    TextView tv_likehelp;


    @BindView(R.id.ll_postLikeAndShareContainer_help)
    LinearLayout ll_postLikeAndShareContainer_help;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    CommentsAdapter commentsAdapter;
    AdvisePostList.DataBean model;
    int replycount = 0;
    int Views = 0;

    public static updatecomment msetComment;

    public static final int REQUEST_POST_IMAGE_AS_COMMENT = 3542;
    ProgressDialog pd;
    String count;
    boolean isLike = true;
    Activity owner;
    int commentCount;
    int position;
    final RetrofitCallbacks<ResponseBody> onPostComment = new RetrofitCallbacks<ResponseBody>(this) {

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            super.onResponse(call, response);
            // pb.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                try {
                    final JsonElement jsonElement;
                    jsonElement = new Gson().fromJson(response.body().string(), JsonElement.class);
                    JsonObject root = jsonElement.getAsJsonObject();
                    CommentsOnPost.DataBean newComment = new Gson().fromJson(root.getAsJsonObject("Data"), CommentsOnPost.DataBean.class);
                    newComment.setUserImage(App.getInstance().getUser().getUserImage());
                    newComment.setFullName(App.getInstance().getUser().getFullName());

                    Date date = new Date(Calendar.getInstance().getTimeInMillis() - TimeZone.getDefault().getRawOffset());
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    String dateFormatted = formatter.format(date);

                    newComment.setCreateDate(dateFormatted);
                    // adapter.addCommentModel(newComment);
                    //  commentsAdapter.addCommentModel(newComment);
                    etNewCommentBody.setText("");
                   // commentCount++;
                    tvHelpCommentCount.setText("" + commentCount);
                    tv_feedCommentCount2.setText("" + commentCount);
                    CommentsAdapter.changeCommentcount = ActivityNeedHelpFullView.this;

                    getComments();
                    Toast.makeText(ActivityNeedHelpFullView.this, "Comment posted successfully", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_help_full_view);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        ButterKnife.bind(this);
        // chances of context not being an activity is very low, but better to check.
        // owner = (context instanceof Activity) ? (Activity) context : null;

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().hide();
        }
        position = getIntent().getIntExtra("POSITION", 1);
        Views =  getIntent().getIntExtra("Views", 1);
        model = getIntent()
                .getParcelableExtra(Constants.PARCELABLE_HELP_OBJECT);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent i = new Intent(ActivityNeedHelpFullView.this, ActivityNeedHelpList.class);
                //  startActivity(i);
                CommentsAdapter.changeCommentcount = null;
               /* if (msetComment != null) {
                    msetComment.setCommentInModel(commentCount, position);
                }*/
                finish();
            }
        });
        if (model != null) {
            tvHelpFvContent.setText(model.getDescription());
            tv_feedViewCount.setText(""+ Views);
          //  tv_feedCommentCount2.setText(""+ model.getCommentCounts());
            if (model.getAdviseBuySaleType() == AdvisePostList.AdviseBuySaleType.ADVISE) {
                tvHelpFvRequestTitle.setText(model.getUserName() + "- want advise " + model.getHelpTitle());
            } else if (model.getAdviseBuySaleType() == AdvisePostList.AdviseBuySaleType.BUY) {
                tvHelpFvRequestTitle.setText(model.getUserName() + "- want to buy " + model.getHelpTitle());
            } else if (model.getAdviseBuySaleType() == AdvisePostList.AdviseBuySaleType.SALE) {
                tvHelpFvRequestTitle.setText(model.getUserName() + "- want to sale " + model.getHelpTitle());

            }
            tvHelpFvUserLocation.setText(model.getPlace());
            if (model.getPlace() != null) {
                tvHelpFvUserLocation.setVisibility(View.VISIBLE);
            } else {
                tvHelpFvUserLocation.setVisibility(View.INVISIBLE);

            }
            tvHelpFvTime.setText(DateUtils.getRelativeDateTimeString(this,
                    Utils.convertDateToMills(model.getCreateDatetime()),
                    DateUtils.SECOND_IN_MILLIS,
                    DateUtils.WEEK_IN_MILLIS,
                    0));
            try {
                final String time = Utils.month[Integer.valueOf(model.getPostMonth())] +
                        " " + model.getPostDay() + " at " + model.getPostHour() + ":" + model.getPostMinute();
                tvHelpFvTime.setText(time);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            Glide.with(this)
                    .load(model.getUserImage()).asBitmap()
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    // .crossFade()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivHelpFvUserPic.setImageBitmap(resource);
                        }
                    });

            if (model.getImages() != null) {
                if (model.getImages().size() > 0) {
                    Glide.with(this)
                            .load(model.getImages().get(0).getImagestr()).asBitmap()
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            //.crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    ivHelpFvImage.setImageBitmap(resource);
                                }
                            });
                    ivHelpFvImage.setVisibility(View.VISIBLE);
                }
            } else {
                tv_helpFvContent_select.setText(model.getDescription());
                tvHelpFvContent.setVisibility(View.VISIBLE);
                tvHelpFvContent.setVisibility(View.GONE);
            }
        }
        System.out.println("!! Is Liked " + model.isIsLiked());
        tv_feedLikeCount_help.setText("0");
        API.getInstance().getStatusPostLikesList(model.getHelpId(), model.getUserID(),
                new RetrofitCallbacks<LikeListResponse>(this) {

                    @Override
                    public void onResponse(Call<LikeListResponse> call, Response<LikeListResponse> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            tv_feedLikeCount_help.setText(String.valueOf(response.body().getData().size()));
                            for (int i = 0; i < response.body().getData().size(); i++) {
                                if (App.getInstance().getUser().getId().equalsIgnoreCase(response.body().getData().get(i).getUserID())) {
                                    tv_likehelp.setText("UnLike");
                                }
                            }
                            //likeListResponse = response.body();
                            //tvLikeListCount.setText(likeListResponse.getData().size() + "");

                            //setupRecyclerView();

                        }
                    }
                });

        System.out.println("!! Like Count " + model.getLikeCounts());
        commentsAdapter = new CommentsAdapter(this);

        rvHelpComments.setLayoutManager(new MyLinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false, Utils.getScreenHeight(this)));
        rvHelpComments.setHasFixedSize(true);
        rvHelpComments.setAdapter(commentsAdapter);

        rvHelpComments.setNestedScrollingEnabled(false);
        tvHelpCommentCount.setText("" + model.getCommentCounts());
        commentCount = model.getCommentCounts();
        getComments();
        //  getLikeofHelp(model);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if (msetComment != null) {
            msetComment.setCommentInModel(commentCount, position);
        }*/
    }

    private void getLikeofHelp(final AdvisePostList.DataBean model) {
        API.getInstance().getStatusPostLikesList(model.getHelpId(), model.getUserID(),
                new RetrofitCallbacks<LikeListResponse>(this) {

                    @Override
                    public void onResponse(Call<LikeListResponse> call, Response<LikeListResponse> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            if (response.body().getResponseStatus() == 1) {
                                //   tv_feedLikeCount_help.setText(String.valueOf(response.body().getData().size()));
                                // tv_feedLikeCount_advice.setText();
                            }
                   /*         likeListResponse = response.body();
                            tvLikeListCount.setText(likeListResponse.getData().size() + "");

                            setupRecyclerView();*/

                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        System.out.println("Hello");
        if(data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            for (Image singleImage : images) {
                postImageAsComment(singleImage);
            }
        }

        //  super.onActivityResult(requestCode, resultCode, data);
    }

    public void postImageAsComment(Image image) {
        Log.i(TAG, "Posting image as comment");
        final CommentOnPost newComment = new CommentOnPost();
        newComment.setComment(etNewCommentBody.getText().toString());
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(model.getHelpId());
        newComment.setImagestr(Utils.convertImageFileToBase64(image.getPath()));

        API.getInstance().commentOnPost(newComment, onPostComment);
    }

    @OnClick(R.id.iv_addImageToNewComment)
    public void imagePostInComment() {
        ImagePicker.create(this)
                .single() // single mode
                .folderMode(false)
                .showCamera(true) // show camera or not (true by default)
                .start(REQUEST_POST_IMAGE_AS_COMMENT);
    }

    private void getComments() {
        pb_commentLoding.setVisibility(View.VISIBLE);

        RetrofitCallbacks<CommentsOnPost> onGetCommentList = new RetrofitCallbacks<CommentsOnPost>(this) {

            @Override
            public void onResponse(Call<CommentsOnPost> call, Response<CommentsOnPost> response) {
                super.onResponse(call, response);
                pb_commentLoding.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    replycount =0;

                    for(int i =0 ;i< response.body().getData().size();i++){
                        if(response.body().getData().get(i).getCommentReplyDetail() != null) {
                            replycount = replycount + response.body().getData().get(i).getCommentReplyDetail().size();
                        }
                    }
                    commentsAdapter.clear();
                    for (CommentsOnPost.DataBean singleComment : response.body().getData()) {
                        commentsAdapter.addCommentModel(singleComment);
                        //    adapter.addCommentModel(singleComment);
                    }
                    //  tvCommentCount.setText(adapter.getItemCount() + "");
                    replycount = replycount + commentsAdapter.getItemCount();
                    commentCount = replycount;
                       tvHelpCommentCount.setText(""+commentCount);
                    tv_feedCommentCount2.setText(""+commentCount);
                    commentsAdapter.notifyDataSetChanged();
                }
            }
        };

        API.getInstance().getCommentListOfPost(model.getHelpId(), App.getInstance().getUser().getId(), onGetCommentList);


    }

    @OnClick(R.id.ll_postLikeAndShareContainer_help)
    public void likeThisFeedPostcount() {
        final LikeListDialog lld = new LikeListDialog(this, model.getHelpId());
        lld.show();
    }

    @OnClick(R.id.tv_likehelp)
    public void likeThisFeedPost() {

        LikeUnlikePost like = new LikeUnlikePost();
        like.setStatusID(model.getHelpId());
        count = tv_feedLikeCount_help.getText().toString();
        int tempcount = Integer.parseInt(count);
        if (tv_likehelp.getText().toString().equalsIgnoreCase("Like")) {
            like.setIsLike(true);
            tempcount++;
            tv_feedLikeCount_help.setText(String.valueOf(tempcount));

            isLike = true;
        } else {
            like.setIsLike(false);
            tempcount--;
            tv_feedLikeCount_help.setText(String.valueOf(tempcount));
            isLike = false;
        }

        like.setUserID(App.getInstance().getUser().getId());

        like.setCreateDate(Calendar.getInstance().getTimeInMillis() + "");
        like.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");
        API.getInstance().likeOnPost(like, new RetrofitCallbacks<LikeUnlikeResponse>(this) {

            @Override
            public void onResponse(Call<LikeUnlikeResponse> call, Response<LikeUnlikeResponse> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    Log.i(TAG, "SUCCESSFULLY LIKED");
                    if (isLike) {
                        tv_likehelp.setText("UnLike");


                    } else {
                        tv_likehelp.setText("Like");
                    }
                    setlikeCount(Integer.parseInt(tv_feedLikeCount_help.getText().toString()));

                } else {

                    try {
                        Log.i(TAG, "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeUnlikeResponse> call, Throwable t) {
                super.onFailure(call, t);
                if (!call.isCanceled()) {
                    // setLikeCounts(post.getLikeCounts());
                }
            }
        });
    }

    public void setlikeCount(int count) {
        if (isLike) {
            count++;

        } else {
            count--;
        }
        //  tv_feedLikeCount_help.setText(count);
    }

    @OnClick(R.id.iv_postNewComment_help)
    public void postingNewComment() {
        if (etNewCommentBody.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter comment!", Toast.LENGTH_SHORT).show();
            return;
        }

        final String commentBody = etNewCommentBody.getText().toString();

        etNewCommentBody.setText("");
        final CommentOnPost newComment = new CommentOnPost();
        newComment.setComment(commentBody);
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(model.getHelpId());
        newComment.setImagestr("");
        // tvCommentCount.setText(count);

        API.getInstance().commentOnPost(newComment, onPostComment);
        Utils.hideKeyboard(this,this.getCurrentFocus());

    }

    @Override
    public void OnUpdateCommentCount() {
        commentCount++;
        tvHelpCommentCount.setText("" + commentCount);
        tv_feedCommentCount2.setText(""+commentCount);
    }

    public interface updatecomment {
        void setCommentInModel(int count, int position);
    }
}
