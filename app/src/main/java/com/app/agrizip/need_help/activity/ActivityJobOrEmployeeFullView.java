package com.app.agrizip.need_help.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.post.post_comments.adapter.CommentsAdapter;
import com.app.agrizip.post.post_like_list.dialog.LikeListDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.StringLoader;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import apimodels.HelpListComments;
import apimodels.LikeListResponse;
import apimodels.LikeUnlikeJob;
import apimodels.LikeUnlikePost;
import apimodels.LikeUnlikeResponse;
import apimodels.RequireEmployeeList;
import apimodels.RequireJobList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.MyLinearLayoutManager;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import viewholders.VHSingleJobDetails;

public class ActivityJobOrEmployeeFullView extends AppCompatActivity implements CommentsAdapter.changeCommentcount {

    private static final String TAG = App.APP_TAG + ActivityNeedHelpFullView.class.getSimpleName();

    @BindView(R.id.iv_helpFvUserPic)
    ImageView ivHelpFvUserPic;

    @BindView(R.id.tv_helpFvRequestTitle)
    TextView tvHelpFvRequestTitle;

    @BindView(R.id.tv_helpFvTime)
    TextView tvHelpFvTime;

    @BindView(R.id.pb_commentLoding)
    RelativeLayout pb_commentLoding;



    @BindView(R.id.tv_helpFvUserLocation)
    TextView tvHelpFvUserLocation;

    @BindView(R.id.rv_helpComments)
    RecyclerView rvHelpComments;

    @BindView(R.id.tv_helpCommentCount)
    TextView tvHelpCommentCount;

    @BindView(R.id.tv_job_full_screeen_like)
    TextView tv_job_full_screeen_like;
    @BindView(R.id.tv_feedLikeCount)
    TextView tv_feedLikeCount;

    @BindView(R.id.tv_feedViewCount)
    TextView tv_feedViewCount;

    @BindView(R.id.tv_feedcommentcount3)
    TextView tv_feedcommentcount3;


    @BindView(R.id.iv_postNewComment_job)
    ImageView ivPostNewComment;

    @BindView(R.id.et_newCommentBody)
    EditText etNewCommentBody;

    @BindView(R.id.iv_addImageToNewComment)
    ImageView ivAddImageToNewComment;

    @BindView(R.id.rv_jobOrEmployeeDetails)
    RecyclerView rvJobOrEmployeeDetails;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.toolbar1)
    Toolbar toolbar1;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    CommentsAdapter commentsAdapter;

    RequireJobList.DataBean singleJobRequest;

    RequireEmployeeList.DataBean singleEmployeeRequest;

    JobOrEmployeeDetailsAdapter jobOrEmployeeDetailsAdapter;

    ArrayList<String> detailsKey = new ArrayList<>();
    ArrayList<String> detailsValue = new ArrayList<>();
    String userId;

    boolean isLike = true;
    String count;
    int count1;
    int position,viewCount = 0;

    public static setJobCount mjobcount;

    public static final int REQUEST_POST_IMAGE_AS_COMMENT = 3542;


    int replycount = 0;

    final RetrofitCallbacks<ResponseBody> onPostComment = new RetrofitCallbacks<ResponseBody>(this) {

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            super.onResponse(call, response);
            // pb.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                try {
                    final JsonElement jsonElement;
                    jsonElement = new Gson().fromJson(response.body().string(), JsonElement.class);
                    JsonObject root = jsonElement.getAsJsonObject();
                    CommentsOnPost.DataBean newComment = new Gson().fromJson(root.getAsJsonObject("Data"), CommentsOnPost.DataBean.class);
                    newComment.setUserImage(App.getInstance().getUser().getUserImage());
                    newComment.setFullName(App.getInstance().getUser().getFullName());

                    Date date = new Date(Calendar.getInstance().getTimeInMillis() - TimeZone.getDefault().getRawOffset());
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    String dateFormatted = formatter.format(date);
                    System.out.println("Hello");
                    newComment.setCreateDate(dateFormatted);
                    // adapter.addCommentModel(newComment);
                    //  commentsAdapter.addCommentModel(newComment);
                    etNewCommentBody.setText("");
                   //   tvHelpCommentCount.setText( );
                      //  count1++;
                    //tvHelpCommentCount.setText(""+count1);
                    //tv_feedcommentcount3.setText(""+count1);

                    getComments();
                    Toast.makeText(ActivityJobOrEmployeeFullView.this, "Comment posted successfully", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_or_employee_full_view);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        ButterKnife.bind(this);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent i = new Intent(ActivityJobOrEmployeeFullView.this, ActivityNeedHelpList.class);
                //  startActivity(i);
               /* if(mjobcount != null){
                    mjobcount.OnsetnewCommentCount(count1,position);
                }*/
                finish();
            }
        });
        final boolean needHelp = getIntent().getBooleanExtra(Constants.NEED_JOB, false);
        position = getIntent().getIntExtra("POSITION",1);
        if (needHelp) {
            toolbar_title.setText("Need Job");
            singleJobRequest = getIntent()
                    .getParcelableExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT);
        } else {
            singleEmployeeRequest = getIntent()
                    .getParcelableExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT);
        }

        jobOrEmployeeDetailsAdapter = new JobOrEmployeeDetailsAdapter();

        rvJobOrEmployeeDetails.setLayoutManager(new LinearLayoutManager(this));
        rvJobOrEmployeeDetails.setHasFixedSize(true);
        rvJobOrEmployeeDetails.setAdapter(jobOrEmployeeDetailsAdapter);

        rvJobOrEmployeeDetails.setNestedScrollingEnabled(false);
        tv_feedLikeCount.setText("0");
        if (singleJobRequest != null) {
            tvHelpFvUserLocation.setText(singleJobRequest.getCity() + ", " +
                    singleJobRequest.getState());

            //tvHelpFvTime.setText(singleJobRequest.getCreateDate());
            tvHelpFvTime.setText(DateUtils.getRelativeDateTimeString(this,
                    Utils.convertDateToMills(singleJobRequest.getCreateDate()),
                    DateUtils.SECOND_IN_MILLIS,
                    DateUtils.WEEK_IN_MILLIS,
                    0));
            Glide.with(this)
                    .load(singleJobRequest.getUserImage())
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)

                    .into(ivHelpFvUserPic);

            tvHelpFvRequestTitle.setText(singleJobRequest.getUserName() + " -Require " +
                    singleJobRequest.getJobDuration() +
                    " job in " + singleJobRequest.getJobTitle());

            jobOrEmployeeDetailsAdapter.addKeyValue("Name: ", singleJobRequest.getUserName());
            jobOrEmployeeDetailsAdapter.addKeyValue("Job Title: ", singleJobRequest.getJobTitle());
            jobOrEmployeeDetailsAdapter.addKeyValue("Email ID: ", singleJobRequest.getEmail());
            jobOrEmployeeDetailsAdapter.addKeyValue("Mobile No: ", singleJobRequest.getMobile());
            jobOrEmployeeDetailsAdapter.addKeyValue("Education: ", singleJobRequest.getEducation());
            jobOrEmployeeDetailsAdapter.addKeyValue("Address: ", singleJobRequest.getCity() + ", " +
                    singleJobRequest.getState());

        } else if (singleEmployeeRequest != null) {
            toolbar_title.setText("Need Employee");
            tvHelpFvUserLocation.setText(singleEmployeeRequest.getCity() + ", " +
                    singleEmployeeRequest.getState());

           /* tvHelpFvTime.setText
                    (singleEmployeeRequest.getCreateDate());*/
            tvHelpFvTime.setText(DateUtils.getRelativeDateTimeString(this,
                    Utils.convertDateToMills(singleEmployeeRequest.getCreateDate()),
                    DateUtils.SECOND_IN_MILLIS,
                    DateUtils.WEEK_IN_MILLIS,
                    0));

            Glide.with(this)
                    .load(singleEmployeeRequest.getUserImage())
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)

                    .into(ivHelpFvUserPic);

            tvHelpFvRequestTitle.setText(singleEmployeeRequest.getUserName() + " -Require " +
                    singleEmployeeRequest.getJobDuration() +
                    " Employee");

            jobOrEmployeeDetailsAdapter.addKeyValue("Company Name: ", singleEmployeeRequest.getCompanyName());
            jobOrEmployeeDetailsAdapter.addKeyValue("Name: ", singleEmployeeRequest.getUserName());
            jobOrEmployeeDetailsAdapter.addKeyValue("Job Title: ", singleEmployeeRequest.getJobTitle());
            jobOrEmployeeDetailsAdapter.addKeyValue("Vacancy: ", singleEmployeeRequest.getVacancy());
            jobOrEmployeeDetailsAdapter.addKeyValue("Experience: ", singleEmployeeRequest.getExperience());
            jobOrEmployeeDetailsAdapter.addKeyValue("Email ID: ", singleEmployeeRequest.getEmail());
            jobOrEmployeeDetailsAdapter.addKeyValue("Mobile No: ", singleEmployeeRequest.getMobile());
            jobOrEmployeeDetailsAdapter.addKeyValue("Education: ", singleEmployeeRequest.getEducation());
            jobOrEmployeeDetailsAdapter.addKeyValue("Address: ", singleEmployeeRequest.getCity() + ", " +
                    singleEmployeeRequest.getState());
        }

        commentsAdapter = new CommentsAdapter(this);
        CommentsAdapter.changeCommentcount = ActivityJobOrEmployeeFullView.this;

        rvHelpComments.setLayoutManager(new MyLinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false, Utils.getScreenHeight(this)));
        rvHelpComments.setHasFixedSize(true);
        rvHelpComments.setAdapter(commentsAdapter);

        rvHelpComments.setNestedScrollingEnabled(false);
        userId = App.getInstance().getUser().getId();
        if(singleEmployeeRequest != null){
   // tvHelpCommentCount.setText(""+singleEmployeeRequest.getCommentCounts());
            count1 =singleEmployeeRequest.getCommentCounts();
            tv_feedViewCount.setText(""+singleEmployeeRequest.getPostViews());
       //     tv_feedcommentcount3.setText(""+singleEmployeeRequest.getCommentCounts());
            }else{
         //   tvHelpCommentCount.setText(""+singleJobRequest.getCommentCounts());
            tv_feedViewCount.setText(""+singleJobRequest.getPostViews());
          //  tv_feedcommentcount3.setText(""+singleJobRequest.getCommentCounts());
            count1 =singleJobRequest.getCommentCounts();
        }


        getComments();
        getLikeonJob();

    }

    @OnClick(R.id.ll_postLikeAndShareContainer)
    public void likeThisFeedPostcount() {
        final String id = singleJobRequest != null ? singleJobRequest.getJobId() :
                singleEmployeeRequest.getJobId();
        final LikeListDialog lld = new LikeListDialog(this, id);
        lld.show();
    }

    @OnClick(R.id.iv_addImageToNewComment)
    public void postImageIncomment(){
        ImagePicker.create(this)
                .single() // single mode
                .folderMode(false)
                .showCamera(true) // show camera or not (true by default)
                .start(REQUEST_POST_IMAGE_AS_COMMENT);
    }
    @OnClick(R.id.iv_postNewComment_job)
    public void postCommentOnJob() {
        if (etNewCommentBody.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter comment!", Toast.LENGTH_SHORT).show();
            return;
        }
        String jobId = null;
        if (singleEmployeeRequest != null) {
            jobId = singleEmployeeRequest.getJobId();

        } else {
            jobId = singleJobRequest.getJobId();
        }
        final String commentBody = etNewCommentBody.getText().toString();

        etNewCommentBody.setText("");
        final CommentOnPost newComment = new CommentOnPost();
        newComment.setComment(commentBody);
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(jobId);
        newComment.setImagestr("");
        API.getInstance().commentOnPost(newComment, onPostComment);
        Utils.hideKeyboard(this,this.getCurrentFocus());


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
     /*  if(mjobcount != null){
            mjobcount.OnsetnewCommentCount(count1,position);
        }*/
    }

    @OnClick(R.id.tv_job_full_screeen_like)
    public void likeJob() {
        final String id = singleJobRequest != null ? singleJobRequest.getJobId() :
                singleEmployeeRequest.getJobId();
        LikeUnlikePost like = new LikeUnlikePost();
        like.setStatusID(id);
        count = tv_feedLikeCount.getText().toString();
        int tempcount = Integer.parseInt(count);
        if (tv_job_full_screeen_like.getText().toString().equalsIgnoreCase("Like")) {
            like.setIsLike(true);
            tempcount++;
            tv_feedLikeCount.setText(String.valueOf(tempcount));

            isLike = true;
        } else {
            like.setIsLike(false);
            tempcount--;
            tv_feedLikeCount.setText(String.valueOf(tempcount));
            isLike = false;
        }

        like.setUserID(App.getInstance().getUser().getId());

        like.setCreateDate(Calendar.getInstance().getTimeInMillis() + "");
        like.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");
        API.getInstance().likeOnPost(like, new RetrofitCallbacks<LikeUnlikeResponse>(this) {

            @Override
            public void onResponse(Call<LikeUnlikeResponse> call, Response<LikeUnlikeResponse> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    Log.i(TAG, "SUCCESSFULLY LIKED");
                    if (isLike) {
                        tv_job_full_screeen_like.setText("UnLike");


                    } else {
                        tv_job_full_screeen_like.setText("Like");
                    }
                    //setlikeCount(Integer.parseInt(tv_feedLikeCount_help.getText().toString()));

                } else {

                    try {
                        Log.i(TAG, "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeUnlikeResponse> call, Throwable t) {
                super.onFailure(call, t);
                if (!call.isCanceled()) {
                    // setLikeCounts(post.getLikeCounts());
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(data != null){
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            for (Image singleImage : images) {
                postImageAsComment(singleImage);
            }
        }
        //super.onActivityResult(requestCode, resultCode, data);
    }

    public void postImageAsComment(Image image) {
        Log.i(TAG, "Posting image as comment");
        String jobId;
        if (singleEmployeeRequest != null) {
            jobId = singleEmployeeRequest.getJobId();

        } else {
            jobId = singleJobRequest.getJobId();
        }
        final CommentOnPost newComment = new CommentOnPost();
        newComment.setComment(etNewCommentBody.getText().toString());
        newComment.setIsComment(true);
        newComment.setUserID(App.getInstance().getUser().getId());
        newComment.setStatusID(jobId);
        newComment.setImagestr(Utils.convertImageFileToBase64(image.getPath()));

        API.getInstance().commentOnPost(newComment, onPostComment);
    }

    private void getComments() {
        pb_commentLoding.setVisibility(View.VISIBLE);
        final String id = singleJobRequest != null ? singleJobRequest.getJobId() :
                singleEmployeeRequest.getJobId();


        RetrofitCallbacks<CommentsOnPost> onGetCommentList = new RetrofitCallbacks<CommentsOnPost>(this) {

            @Override
            public void onResponse(Call<CommentsOnPost> call, Response<CommentsOnPost> response) {
                super.onResponse(call, response);
                pb_commentLoding.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    replycount = 0;
                    for(int i =0;i< response.body().getData().size();i++){
                    if(response.body().getData().get(i).getCommentReplyDetail() != null){
                        replycount = replycount + response.body().getData().get(i).getCommentReplyDetail().size();
                    }
                    }

                    commentsAdapter.clear();
                    for (CommentsOnPost.DataBean singleComment : response.body().getData()) {
                        commentsAdapter.addCommentModel(singleComment);
                        //    adapter.addCommentModel(singleComment);
                    }
                    count1 = replycount + commentsAdapter.getItemCount();
                    tvHelpCommentCount.setText(""+count1);
                    tv_feedcommentcount3.setText(""+count1);
                      commentsAdapter.notifyDataSetChanged();
                }
            }
        };

        API.getInstance().getCommentListOfPost(id, userId, onGetCommentList);

        /*  final RetrofitCallbacks<HelpListComments> retrofitCallback =
                new RetrofitCallbacks<HelpListComments>(this) {
                    @Override
                    public void onResponse(Call<HelpListComments> call, Response<HelpListComments> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            for (HelpListComments.DataBean comment : response.body().getData()) {
                                Log.i(TAG, "ADDING HELP POST COMMENT");
                                commentsAdapter.addCommentModel(comment);
                            }
                            tvHelpCommentCount.setText(commentsAdapter.getItemCount() + "");
                            commentsAdapter.notifyDataSetChanged();
                        }
                    }
                };


        API.getInstance().getJobPostCommentsList(id, retrofitCallback);*/

    }

    public void getLikeonJob() {
        final String id = singleJobRequest != null ? singleJobRequest.getJobId() :
                singleEmployeeRequest.getJobId();
        API.getInstance().getStatusPostLikesList(id, App.getInstance().getUser().getId(),
                new RetrofitCallbacks<LikeListResponse>(this) {

                    @Override
                    public void onResponse(Call<LikeListResponse> call, Response<LikeListResponse> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            tv_feedLikeCount.setText(String.valueOf(response.body().getData().size()));
                            for (int i = 0; i < response.body().getData().size(); i++) {
                                if (App.getInstance().getUser().getId().equalsIgnoreCase(response.body().getData().get(i).getUserID())) {
                                    tv_job_full_screeen_like.setText("UnLike");
                                }
                            }
                            //likeListResponse = response.body();
                            //tvLikeListCount.setText(likeListResponse.getData().size() + "");

                            //setupRecyclerView();

                        }
                    }
                });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CommentsAdapter.changeCommentcount = null;
    }

    @Override
    public void OnUpdateCommentCount() {
        count1++;
        tvHelpCommentCount.setText(""+count1);
        tv_feedcommentcount3.setText(""+count1);
    }


    class JobOrEmployeeDetailsAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return VHSingleJobDetails.create(ActivityJobOrEmployeeFullView.this, parent);
        }

        public void addKeyValue(String key, String value) {
            detailsKey.add(key);
            detailsValue.add(value);
            notifyItemInserted(detailsKey.size());
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            VHSingleJobDetails.bind((VHSingleJobDetails) holder, detailsKey.get(position),
                    detailsValue.get(position));
        }

        @Override
        public int getItemCount() {
            return detailsKey.size();
        }
    }

    public interface setJobCount{
        void OnsetnewCommentCount(int count,int position);
    }

}
