package com.app.agrizip.need_help.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import apimodels.AdvisePostList;
import apimodels.UserSearchResponse;
import viewholders.VHSingleAdviceBuySale;

/**
 * Created by Alpesh on 4/7/2017.
 */

public class SearchHelpListAdapter extends RecyclerView.Adapter
{
    final Context context;
    List<AdvisePostList.DataBean> modelList;

    public SearchHelpListAdapter(Context context) {
        this.context = context;
        modelList = new LinkedList<>();
    }
    public void addSearchedUser(AdvisePostList.DataBean searchedUser)
    {
        modelList.add(searchedUser);
        notifyItemInserted(modelList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return VHSingleAdviceBuySale.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        VHSingleAdviceBuySale.bind((VHSingleAdviceBuySale) holder,
                modelList.get(position));

    }
public void clear(){
    modelList.clear();
    notifyDataSetChanged();
}

    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
