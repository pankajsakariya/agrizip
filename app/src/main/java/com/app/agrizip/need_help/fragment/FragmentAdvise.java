package com.app.agrizip.need_help.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.activity.ActivityNeedHelpFullView;
import com.app.agrizip.need_help.adapter.HelpListAdapter;
import com.app.agrizip.need_help.adapter.SearchHelpListAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AdvisePostList;
import apimodels.RequireJobList;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/17/2017 at 5:58 PM.
 */

public class FragmentAdvise extends Fragment implements AdvisePostList.HideHelpListener,TextWatcher,ActivityNeedHelpFullView.updatecomment
{

    @BindView(R.id.rv_advise)
    RecyclerView rvAdvise;
    @BindView(R.id.rv_searchAdvise)
    RecyclerView rv_searchAdvise;

    HelpListAdapter adviseListAdapter,adviseListAdapter1;
    SearchHelpListAdapter searchHelpListAdapter;
    @BindView(R.id.ll_loadingJobs)
    LinearLayout llLoadingJobs;
    private Context context;
    @BindView(R.id.tv_buyApiResponse)
    TextView tv_buyApiResponse;
    @BindView(R.id.et_searchAdvice)
    EditText etsearchAdvice;

    Call<ResponseBody> call;
    public static FragmentAdvise newInstance(final Context context) {
        FragmentAdvise fragmentAdvise = new FragmentAdvise();
        fragmentAdvise.context = context;
        return fragmentAdvise;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_advise, container, false);
        ButterKnife.bind(this, view);

        adviseListAdapter = new HelpListAdapter(getActivity());
        adviseListAdapter1 = new HelpListAdapter(getActivity());
        searchHelpListAdapter = new SearchHelpListAdapter(getActivity());
        etsearchAdvice.addTextChangedListener(FragmentAdvise.this);
        rvAdvise.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvAdvise.setHasFixedSize(true);
        rvAdvise.setAdapter(adviseListAdapter);
        rv_searchAdvise.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_searchAdvise.setHasFixedSize(true);
        rv_searchAdvise.setAdapter(adviseListAdapter1);
        ActivityNeedHelpFullView.msetComment = this;


        getAdviseList();


        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            ActivityNeedHelpFullView.msetComment = this;
        }
    }

    public void hideHelp(AdvisePostList.DataBean singleJobRequest){

        System.out.println("hello " + singleJobRequest);
       /*HelpListAdapter adviseListAdapter = new HelpListAdapter(getActivity());
       adviseListAdapter.removeHelp(singleJobRequest);*/
    }


    public void getAdviseList() {
        final RetrofitCallbacks<AdvisePostList> callback =
                new RetrofitCallbacks<AdvisePostList>(getActivity()) {

                    @Override
                    public void onResponse(Call<AdvisePostList> call, Response<AdvisePostList> response) {
                        super.onResponse(call, response);
                        llLoadingJobs.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            for (AdvisePostList.DataBean singleAdvice : response.body().getData()) {
                                if (response.body().getData().size() > 0) {
                                    tv_buyApiResponse.setVisibility(View.GONE);
                                }
                                singleAdvice.setHideHelpListener(FragmentAdvise.this);
                                singleAdvice.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.ADVISE);
                                adviseListAdapter.addAdviseListItem(singleAdvice);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdvisePostList> call, Throwable t) {
                        super.onFailure(call, t);
                        llLoadingJobs.setVisibility(View.GONE);
                    }
                };

        API.getInstance().getAdvicePostList(App.getInstance().getUser().getId(), 1, callback);
    }

    @Override
    public void onhideHelp(AdvisePostList.DataBean singleHelp,int position,int type) {
        if(type == 1){
            adviseListAdapter.removeHelp(singleHelp,position);
        }else{
            adviseListAdapter1.removeHelp(singleHelp,position);
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().length() > 3) {
            searchAdvice(charSequence.toString());
        }
        if (charSequence.toString().length() == 0) {
            rv_searchAdvise.setVisibility(View.GONE);
            rvAdvise.setVisibility(View.VISIBLE);
            tv_buyApiResponse.setVisibility(View.GONE);


        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


    private void searchAdvice(String keyword) {
        String userid = App.getInstance().getUser().getId();
        Context context = getActivity();
        API.getInstance().getSearchPostAdviceList(userid,keyword,1, new RetrofitCallbacks<AdvisePostList>(context) {

            @Override
            public void onResponse(Call<AdvisePostList> call, Response<AdvisePostList> response) {
                super.onResponse(call, response);

                if (response.isSuccessful()) {
                    adviseListAdapter1.clear();
                  //  searchHelpListAdapter.clear();
                    if(response.body().getData().size() > 0){
                        tv_buyApiResponse.setVisibility(View.GONE);
                        rvAdvise.setVisibility(View.GONE);
                        rv_searchAdvise.setVisibility(View.VISIBLE);
                        for (AdvisePostList.DataBean singleAdvice : response.body().getData()) {

                            // singleAdvice.setHideHelpListener(FragmentAdvise.this);
                            singleAdvice.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.ADVISE);
                            //adviseListAdapter.addAdviseListItem(singleAdvice);
                            adviseListAdapter1.addAdviseListItem(singleAdvice);
                        }
                    }else{
                        tv_buyApiResponse.setVisibility(View.VISIBLE);
                        rvAdvise.setVisibility(View.GONE);
                        rv_searchAdvise.setVisibility(View.GONE);
                    }
                   /* adviseListAdapter.clear();
                    for (AdvisePostList.DataBean singleAdvice : response.body().getData()) {
                        if (response.body().getData().size() > 0) {
                            tv_buyApiResponse.setVisibility(View.GONE);
                        }
                       // singleAdvice.setHideHelpListener(FragmentAdvise.this);
                        singleAdvice.setAdviseBuySaleType(AdvisePostList.AdviseBuySaleType.ADVISE);
                        adviseListAdapter.addAdviseListItem(singleAdvice);
                    }*/
                }

            }

            @Override
            public void onFailure(Call<AdvisePostList> call, Throwable t) {
                super.onFailure(call, t);
            }
        });

    }

    @Override
    public void setCommentInModel(int count,int position) {
        adviseListAdapter.changeCommnetCount(position,count);
    }
}
