package com.app.agrizip.need_help.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import apimodels.AdvisePostList;
import apimodels.BuyPostList;
import apimodels.RequireEmployeeList;
import apimodels.RequireJobList;
import apimodels.SalePostList;
import viewholders.VHSingleAdviceBuySale;
import viewholders.VHSingleJobOrEmployeeRequest;

import static com.app.agrizip.need_help.adapter.HelpListComponent.ADVICE_BUY_SALE;
import static com.app.agrizip.need_help.adapter.HelpListComponent.REQUIRE_EMPLOYEE;
import static com.app.agrizip.need_help.adapter.HelpListComponent.REQUIRE_JOB;

/**
 * Created by rutvik on 1/16/2017 at 6:15 PM.
 */

public class HelpListAdapter extends RecyclerView.Adapter
{

    private final List<HelpListComponent> helpListComponentList;

    private final Context context;
    private final List<String> idList;

    public HelpListAdapter(Context context)
    {
        this.context = context;
        helpListComponentList = new ArrayList<>();
        idList = new LinkedList<>();
    }

    public HelpListComponent addRequireJob(final RequireJobList.DataBean singleJobRequest)
    {
        HelpListComponent helpListComponent = new HelpListComponent(singleJobRequest,
                REQUIRE_JOB);
        helpListComponentList.add(helpListComponent);
        notifyItemInserted(helpListComponentList.size());
        return helpListComponent;
    }

    public HelpListComponent addRequireEmployee(final RequireEmployeeList.DataBean singleEmployeeRequest)
    {
        HelpListComponent helpListComponent = new HelpListComponent(singleEmployeeRequest,
                REQUIRE_EMPLOYEE);
        helpListComponentList.add(helpListComponent);
        notifyItemInserted(helpListComponentList.size());
        return helpListComponent;
    }
    public void removeHelp(final AdvisePostList.DataBean singleJobRequest,int position){
       // singleJobRequest.getHelpId();
     /*   int index = idList.indexOf(singleJobRequest.getHelpId());
        idList.remove(singleJobRequest.getHelpId());
        helpListComponentList.remove(singleJobRequest.getHelpId());
        notifyItemRemoved(index);*/

       helpListComponentList.remove(2);

        notifyItemRemoved(position);

        //final int index = helpListComponentList.indexOf(singleJobRequest);

    }
    public  void removeJobEmployeePost(int position,int isjob){

        helpListComponentList.remove(isjob);
        notifyItemRemoved(position);
    }

    public HelpListComponent addSaleListItem(final SalePostList.DataBean salePost)
    {
        HelpListComponent helpListComponent = new HelpListComponent(salePost,
                ADVICE_BUY_SALE);
        helpListComponentList.add(helpListComponent);
        notifyItemInserted(helpListComponentList.size());
        return helpListComponent;
    }

    public HelpListComponent addBuyListItem(final BuyPostList.DataBean buyPost)
    {
        HelpListComponent helpListComponent = new HelpListComponent(buyPost,
                ADVICE_BUY_SALE);
        helpListComponentList.add(helpListComponent);
        notifyItemInserted(helpListComponentList.size());
        return helpListComponent;
    }

    public void changeJobCommentCount(int position,int count){

        RequireJobList.DataBean requreJob=(RequireJobList.DataBean) helpListComponentList.get(position).getModel();
        requreJob.setCommentCounts(count);
        notifyItemChanged(position);
    }

    public void changeEmployeeCommentCount(int position,int count){

        RequireEmployeeList.DataBean requreJob= (RequireEmployeeList.DataBean) helpListComponentList.get(position).getModel();
        requreJob.setCommentCounts(count);
        notifyItemChanged(position);
    }
    public void changeCommnetCount(int position,int count){
        AdvisePostList.DataBean helpdata = (AdvisePostList.DataBean) helpListComponentList.get(position).getModel();
        helpdata.setCommentCounts(count);
        notifyItemChanged(position);
    }

    public HelpListComponent addAdviseListItem(final AdvisePostList.DataBean advisePost)
    {
        HelpListComponent helpListComponent = new HelpListComponent(advisePost,
                ADVICE_BUY_SALE);
        helpListComponentList.add(helpListComponent);
        notifyItemInserted(helpListComponentList.size());
        return helpListComponent;
    }

    @Override
    public int getItemViewType(int position)
    {
        return helpListComponentList.get(position).viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        switch (viewType)
        {
            case REQUIRE_JOB:
                return VHSingleJobOrEmployeeRequest.create(context, parent, REQUIRE_JOB);

            case REQUIRE_EMPLOYEE:
                return VHSingleJobOrEmployeeRequest.create(context, parent, REQUIRE_EMPLOYEE);

            case ADVICE_BUY_SALE:
                return VHSingleAdviceBuySale.create(context, parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        switch (getItemViewType(position))
        {
            case REQUIRE_JOB:
                VHSingleJobOrEmployeeRequest.bind((VHSingleJobOrEmployeeRequest) holder,
                        (RequireJobList.DataBean) helpListComponentList.get(position).getModel());
                break;

            case REQUIRE_EMPLOYEE:
                VHSingleJobOrEmployeeRequest.bind((VHSingleJobOrEmployeeRequest) holder,
                        (RequireEmployeeList.DataBean) helpListComponentList.get(position).getModel());
                break;

            case ADVICE_BUY_SALE:
                VHSingleAdviceBuySale.bind((VHSingleAdviceBuySale) holder,
                        (AdvisePostList.DataBean) helpListComponentList.get(position).getModel());
                break;
        }
    }

    public void hideHelp(String helpId){

    }

    public void clear(){
        helpListComponentList.clear();
      //  idList.clear();
       notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        return helpListComponentList.size();
    }
}
