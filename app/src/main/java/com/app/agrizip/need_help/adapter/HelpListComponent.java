package com.app.agrizip.need_help.adapter;

/**
 * Created by rutvik on 1/16/2017 at 6:16 PM.
 */

public class HelpListComponent<T>
{

    public static final int REQUIRE_JOB = 0;
    public static final int REQUIRE_EMPLOYEE = 1;
    public static final int ADVICE_BUY_SALE = 2;

    final int viewType;

    final T model;

    HelpListComponent(T model, int viewType)
    {
        this.model = model;
        this.viewType = viewType;
    }

    public int getViewType()
    {
        return viewType;
    }

    public T getModel()
    {
        return model;
    }
}
