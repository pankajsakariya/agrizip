package com.app.agrizip.need_help.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.activity.ActivityJobOrEmployeeFullView;
import com.app.agrizip.need_help.adapter.HelpListAdapter;
import com.google.android.gms.vision.text.Text;

import api.API;
import api.RetrofitCallbacks;
import apimodels.RequireEmployeeList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/16/2017 at 6:03 PM.
 */

public class FragmentRequireEmployee extends Fragment implements RequireEmployeeList.HideEmployeeListener,ActivityJobOrEmployeeFullView.setJobCount{

    @BindView(R.id.rv_employee)
    RecyclerView rvEmployee;
    @BindView(R.id.ll_loadingEmployee)
    LinearLayout llLoadingEmployee;
    private Context context;
    private HelpListAdapter employeeListAdapter;

    @BindView(R.id.tv_buyApiResponse)
    TextView tv_buyApiResponse;

    public static FragmentRequireEmployee newInstance(Context context) {
        FragmentRequireEmployee fragmentRequireEmployee = new FragmentRequireEmployee();
        fragmentRequireEmployee.context = context;
        return fragmentRequireEmployee;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_require_employee, container, false);

        ButterKnife.bind(this, view);

        employeeListAdapter = new HelpListAdapter(getActivity());

        rvEmployee.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvEmployee.setHasFixedSize(true);
        rvEmployee.setAdapter(employeeListAdapter);
      //  ActivityJobOrEmployeeFullView.mjobcount = this;
        getRequireEmployeeList();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            ActivityJobOrEmployeeFullView.mjobcount = this;
        }
    }

    private void getRequireEmployeeList() {
        final RetrofitCallbacks<RequireEmployeeList> onGetJobListCallback =
                new RetrofitCallbacks<RequireEmployeeList>(getActivity()) {
                    @Override
                    public void onResponse(Call<RequireEmployeeList> call, Response<RequireEmployeeList> response) {
                        super.onResponse(call, response);
                        llLoadingEmployee.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            for (RequireEmployeeList.DataBean singleEmployeeRequest : response.body().getData()) {
                                if (response.body().getData().size() > 0) {
                                    tv_buyApiResponse.setVisibility(View.GONE);
                                }
                                singleEmployeeRequest.setHideEmployeeListener(FragmentRequireEmployee.this);
                                employeeListAdapter.addRequireEmployee(singleEmployeeRequest);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RequireEmployeeList> call, Throwable t) {
                        super.onFailure(call, t);
                        llLoadingEmployee.setVisibility(View.GONE);
                    }
                };

        API.getInstance().getRequireEmployeeList(App.getInstance().getUser().getId(), 1, onGetJobListCallback);
    }

    @Override
    public void OnHideEmployee(int position) {
        employeeListAdapter.removeJobEmployeePost(position,1);
    }

    @Override
    public void OnsetnewCommentCount(int count, int position) {
        employeeListAdapter.changeEmployeeCommentCount(position,count);
    }
}
