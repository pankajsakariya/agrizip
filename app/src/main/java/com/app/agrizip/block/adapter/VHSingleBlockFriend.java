package com.app.agrizip.block.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import api.API;
import api.RetrofitCallbacks;
import apimodels.BlockUser;
import apimodels.FriendList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by murtuzanalawala on 4/18/17.
 */

public class VHSingleBlockFriend extends RecyclerView.ViewHolder {
    final Context context;
    @BindView(R.id.iv_SingleblockFriendImage)
    PorterShapeImageView iv_SingleblockFriendImage;
    @BindView(R.id.tv_singleblockFriendName)
    TextView tv_singleblockFriendName;
    @BindView(R.id.btn_unblock)
    Button btn_unblock;
    FriendList.DataBean thisFriend;

    int position;

    public VHSingleBlockFriend(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }
    public static VHSingleBlockFriend create(final Context context, final ViewGroup parent) {
        return new VHSingleBlockFriend(context, LayoutInflater.from(context)
                .inflate(R.layout.single_block_friend_row, parent, false));
    }
    public static void bind(final VHSingleBlockFriend vh, FriendList.DataBean thisFriend, int position) {

        vh.thisFriend = thisFriend;
        vh.position = position;

        vh.tv_singleblockFriendName.setText(thisFriend.getFullName());

        Glide.with(vh.context)
                .load(thisFriend.getFriendImage())
                .asBitmap()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true)
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)

                .into(new SimpleTarget<Bitmap>() {

            @Override
            public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                // TODO Auto-generated method stub
                vh.iv_SingleblockFriendImage.setImageBitmap(arg0);
            }
        });

    }

    @OnClick(R.id.btn_unblock)
    public void clickOnUnblock(){

        new AlertDialog.Builder(context)
                .setTitle("Unblock")
                .setMessage("Are you sure you want to UnBlock this Person?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("UnBlock", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        // unfollowUser();
                        // int position = getPosition();

                        UnBlock();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                }).show();

    }

    private void UnBlock(){

        final RetrofitCallbacks<ResponseBody> onUnBlock = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    if (thisFriend.getFriendActionListener() != null) {
                        Constants.UPDATE_FLLOWCOUNT = true;
                        thisFriend.getFriendActionListener().onBlockFriend(thisFriend);
                     //   btnSingleFriendExtraOptions.setText("Unblock");
                    }
                } else {
                    Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        };

        final BlockUser blockUser = new BlockUser();
        blockUser.setBlockUserID(thisFriend.getFriendId());
        blockUser.setUserID(App.getInstance().getUser().getId());
        blockUser.setBlockMessage("This user is Unblock");


        API.getInstance().unBlockUser(blockUser, onUnBlock);


    }


}
