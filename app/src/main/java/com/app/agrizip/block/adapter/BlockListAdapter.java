package com.app.agrizip.block.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.app.agrizip.friends.friend_list.adapter.VHSingleFriend;

import java.util.ArrayList;
import java.util.List;

import apimodels.FriendList;

/**
 * Created by murtuzanalawala on 4/18/17.
 */

public class BlockListAdapter extends RecyclerView.Adapter {

    final Context context;

    final List<FriendList.DataBean> blockList;

    public BlockListAdapter(final Context context)
    {
        this.context = context;
        blockList = new ArrayList<>();
    }

    public void addSingleFriend(FriendList.DataBean singleFriend)
    {
        blockList.add(singleFriend);
        notifyItemInserted(blockList.size());
    }

    public void removeFriend(FriendList.DataBean friend)
    {
        final int index = blockList.indexOf(friend);
        blockList.remove(index);
        notifyItemRemoved(index);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return VHSingleBlockFriend.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        VHSingleBlockFriend.bind((VHSingleBlockFriend) holder, blockList.get(position), position);
    }

    public void clear()
    {
        blockList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return blockList.size();
    }
}
