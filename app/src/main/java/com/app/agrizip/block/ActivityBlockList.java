package com.app.agrizip.block;

import android.graphics.drawable.Drawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.block.adapter.BlockListAdapter;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FriendList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityBlockList extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        FriendList.FriendActionListener {

    @BindView(R.id.rv_blockList)
    RecyclerView rvBlockList;

    @BindView(R.id.srl_refreshBlockList)
    SwipeRefreshLayout srlRefreshBlockList;
    @BindView(R.id.fl_emptyPendingRequest)
    FrameLayout fl_emptyPendingRequest;
    boolean busyLoadingData = false;

    int pageNo = 1;
   BlockListAdapter adapter;

    int pastVisibleItems, visibleItemCount, totalItemCount;

    LinearLayoutManager llm;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static String userId;
    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_list);
        ButterKnife.bind(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText(R.string.block);
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }


        adapter = new BlockListAdapter(this);
        llm = new LinearLayoutManager(this);

        rvBlockList.setLayoutManager(llm);
        rvBlockList.setHasFixedSize(true);
        rvBlockList.setAdapter(adapter);
        rvBlockList.setItemAnimator(new DefaultItemAnimator());

        rvBlockList.addOnScrollListener(new OnScrollRecyclerView());
        srlRefreshBlockList.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshBlockList.setOnRefreshListener(this);

            getBlockList();


    }

    public void getBlockList(){
        userId = App.getInstance().getUser().getId();
        final RetrofitCallbacks<FriendList> onGetFriendList =
                new RetrofitCallbacks<FriendList>(this) {

                    @Override
                    public void onResponse(Call<FriendList> call, Response<FriendList> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            busyLoadingData = false;
                            if (adapter.getItemCount() > 0 && pageNo == 1) {
                                adapter.clear();
                            }
                            for (FriendList.DataBean singleFriend : response.body().getData()) {
                                singleFriend.setFriendActionListener(ActivityBlockList.this);

                                adapter.addSingleFriend(singleFriend);
                            }
                            if (adapter.getItemCount() > 0) {
                                fl_emptyPendingRequest.setVisibility(View.GONE);
                            }
                            if (srlRefreshBlockList.isRefreshing()) {
                                srlRefreshBlockList.setRefreshing(false);
                            }
                        }
                    }
                };


        if (!busyLoadingData) {
            busyLoadingData = true;

            API.getInstance().getBlockList(userId, pageNo,
                    onGetFriendList);

        }


    }

    @Override
    public void onRefresh() {
        if (!busyLoadingData) {
            pageNo = 1;
            getBlockList();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUnfriend(FriendList.DataBean friend) {

    }

    @Override
    public void onUnfollow(FriendList.DataBean friend) {

    }

    @Override
    public void onBlockFriend(FriendList.DataBean blockedFriend) {
        adapter.removeFriend(blockedFriend);
        if(adapter.getItemCount() == 0){
            fl_emptyPendingRequest.setVisibility(View.VISIBLE);
        }
    }
    class OnScrollRecyclerView extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (!busyLoadingData) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        pageNo++;
                        getBlockList();
                    }
                }
            }
        }
    }
}
