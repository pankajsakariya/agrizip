package com.app.agrizip.stories.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apimodels.StoryList;
import viewholders.VHStoryListItem;

/**
 * Created by rutvik on 2/13/2017 at 4:27 PM.
 */

public class StoryListAdapter extends RecyclerView.Adapter
{

    private final Context context;

    private final List<StoryList> storyList;

    public StoryListAdapter(Context context)
    {
        this.context = context;
        storyList = new ArrayList<>();
    }

    public void addStory(StoryList singleStory)
    {
        boolean contains = false;
        if (storyList.size() > 0)
        {
            for (StoryList s : storyList)
            {
                if (s.getId() == singleStory.getId())
                {
                    contains = true;
                }
            }
        }
        if (!contains)
        {
            storyList.add(singleStory);
            notifyItemInserted(storyList.size());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHStoryListItem.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHStoryListItem.bind((VHStoryListItem) holder, storyList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return storyList.size();
    }
}
