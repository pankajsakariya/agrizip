package com.app.agrizip.stories.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import apimodels.SingleStory;
import apimodels.StoryList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityFullStory extends AppCompatActivity
{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.iv_storyImage)
    PorterShapeImageView ivStoryImage;

    @BindView(R.id.tv_storyTitle)
    TextView tvStoryTitle;

    @BindView(R.id.tv_storyDescriptionLong)
    TextView tvStoryDescriptionLong;

    @BindView(R.id.tv_storySources)
    TextView tvStorySources;

    @BindView(R.id.tv_storyEditorAndDate)
    TextView tvStoryEditorAndDate;

    @BindView(R.id.tv_storyTime)
    TextView tvStoryTime;

    @BindView(R.id.ll_shareStory)
    LinearLayout llShareStory;

    StoryList singleStoryDetails;

    SingleStory singleStory;

    Retrofit retrofit;

    Call<SingleStory> call;
    int flag = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_story);

        ButterKnife.bind(this);

        if (toolbar != null)
        {
            tvToolbarTitle.setText(R.string.full_story);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null)
            {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        flag = getIntent().getIntExtra("FLAG",1);
        if(flag==1) {
        singleStoryDetails = getIntent().getParcelableExtra(Constants.SINGLE_STORY);
        }else {
            String id = getIntent().getStringExtra("STORY_ID");
            singleStoryDetails.setId(Integer.parseInt(id));
            retrofit = StoryListActivity.prepareRetrofit();
        }
        if (singleStoryDetails != null)
        {
            tvStoryTitle.setText(singleStoryDetails.getTitle());
            String FromDate= Utils.changeDateFormat(singleStoryDetails.getDate(),"yyyy-MM-dd","MMMM dd,yyyy");
            tvStoryEditorAndDate.setText("By " + singleStoryDetails.getAuthor() +
                    " | " + FromDate);
            String FromTime= Utils.changeDateFormat(singleStoryDetails.getTime(),"HH:mm:ss","hh:mm a");
            tvStoryTime.setText(FromTime);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                SpannableStringBuilder builder = new SpannableStringBuilder();

                String red = singleStoryDetails.getShortDescription();
                SpannableString redSpannable= new SpannableString(red);
                redSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.wallet_holo_blue_light)), 0, red.length(), 0);
                builder.append(redSpannable);

                tvStoryDescriptionLong
                        .setText(red);


                tvStoryDescriptionLong
                        .setText(Html.fromHtml(singleStoryDetails.getShortDescription(), Html.FROM_HTML_MODE_COMPACT));
            } else
            {
                tvStoryDescriptionLong
                        .setText(Html.fromHtml(singleStoryDetails.getShortDescription()));
            }

            Glide.with(this)
                    .load(singleStoryDetails.getImage())
                    .into(ivStoryImage);

            retrofit = StoryListActivity.prepareRetrofit();

        }

        if (retrofit != null)
        {
            call = retrofit.create(StoryListActivity.StoryAPI.class)
                    .getFullStory(singleStoryDetails.getId());

            call.enqueue(new Callback<SingleStory>()
            {
                @Override
                public void onResponse(Call<SingleStory> call, Response<SingleStory> response)
                {
                    if (response.isSuccessful())
                    {
                        singleStory = response.body();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        {
                            tvStoryDescriptionLong
                                    .setText(Html.fromHtml(singleStory.getLongDescription(), Html.FROM_HTML_MODE_COMPACT));
                        } else
                        {
                            tvStoryDescriptionLong
                                    .setText(Html.fromHtml(singleStory.getLongDescription()));
                        }

                        tvStorySources.setText("Source: " + singleStory.getLink());
                    } else
                    {
                        Toast.makeText(ActivityFullStory.this,
                                "Something went wrong please try again later.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SingleStory> call, Throwable t)
                {
                    Toast.makeText(ActivityFullStory.this,
                            "Something went wrong please try again later.", Toast.LENGTH_SHORT).show();
                }
            });
        }


        llShareStory.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, singleStoryDetails.getTitle());
                i.putExtra(Intent.EXTRA_TEXT, singleStory.getLink());
               // startActivity(Intent.createChooser(i, "Share URL"));
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop()
    {
        if (call != null)
        {
            call.cancel();
        }
        super.onStop();
    }
}
