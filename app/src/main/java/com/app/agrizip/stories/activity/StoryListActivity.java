package com.app.agrizip.stories.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.stories.adapter.StoryListAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import apimodels.SingleStory;
import apimodels.StoryList;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static api.ApiClient.provideCacheInterceptor;
import static api.ApiClient.provideOfflineCacheInterceptor;

public class StoryListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = App.APP_TAG + StoryListActivity.class.getSimpleName();
    static Retrofit retrofit = null;
    @BindView(R.id.rv_storyList)
    RecyclerView rvStoryList;
    StoryListAdapter adapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;
    @BindView(R.id.ll_loadingStories)
    LinearLayout llLoadingStories;
    @BindView(R.id.srl_refreshStories)
    SwipeRefreshLayout srlRefreshStories;

    Call<List<StoryList>> call;

    public static Retrofit prepareRetrofit() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()

                .addNetworkInterceptor(provideCacheInterceptor())
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(provideOfflineCacheInterceptor())
                .build();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl("http://www.agrizip.com/wp-json/wp/v2/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }

        return retrofit;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_list);

        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                tvToolbarTitle.setText("Story");
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        rvStoryList.setLayoutManager(new LinearLayoutManager(this));

        rvStoryList.setHasFixedSize(true);

        adapter = new StoryListAdapter(this);

        rvStoryList.setAdapter(adapter);
        srlRefreshStories.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshStories.setOnRefreshListener(this);

        prepareRetrofit();

        getStoryList();

    }

    private void getStoryList() {

        if (call != null) {
            call.cancel();
        }

        call = retrofit.create(StoryAPI.class).getStoryList();
        call.enqueue(new Callback<List<StoryList>>() {
            @Override
            public void onResponse(Call<List<StoryList>> call, Response<List<StoryList>> response) {
                llLoadingStories.setVisibility(View.GONE);
                if (srlRefreshStories.isRefreshing()) {
                    srlRefreshStories.setRefreshing(false);
                }
                if (response.isSuccessful()) {
                    for (StoryList story : response.body()) {
                        adapter.addStory(story);
                    }
                } else {
                    Toast.makeText(StoryListActivity.this, "Something went wrong please try again later.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<StoryList>> call, Throwable t) {
                llLoadingStories.setVisibility(View.GONE);
                if (srlRefreshStories.isRefreshing()) {
                    srlRefreshStories.setRefreshing(false);
                }
                Toast.makeText(StoryListActivity.this, "Something went wrong please try again later.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        getStoryList();
    }

    @Override
    protected void onStop() {
        if (call != null) {
            call.cancel();
        }
        super.onStop();
    }

    public interface StoryAPI {
        @GET("posts")
        Call<List<StoryList>> getStoryList();

        @GET("http://www.agrizip.com/wp-json/wp/v2/posts/{id}")
        Call<SingleStory> getFullStory(@Path("id") int id);
    }
}
