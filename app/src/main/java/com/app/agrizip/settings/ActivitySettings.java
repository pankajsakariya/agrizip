package com.app.agrizip.settings;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;

import java.util.ArrayList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AppSettingModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import viewholders.VHSingleSettingsItem;
import viewmodels.SingleSettingsItemModel;

public class ActivitySettings extends AppCompatActivity
{

    @BindView(R.id.rv_settings)
    RecyclerView rvSettings;

    List<SingleSettingsItemModel> settingsItemModelList = new ArrayList<>();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    SettingsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null)
            {
                if (tvToolbarTitle != null)
                {
                    tvToolbarTitle.setText(R.string.app_setting);
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        adapter = new SettingsAdapter();

        rvSettings.setLayoutManager(new LinearLayoutManager(this));
        rvSettings.setHasFixedSize(true);

        rvSettings.setAdapter(adapter);

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Privacy Settings", null, false,
                null, new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Open privacy settings activity
                startActivity(new Intent(ActivitySettings.this,
                        ActivityPrivacySettings.class));
            }
        }));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Vibrate",
                "Vibrate on incoming notifications", true,
                new SingleSettingsItemModel.SettingsChangedListener(ActivitySettings.this, "vibrate")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);
                        //Perform Vibration setting logic here
                    }
                }, null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Power LED",
                "Flash LED on incoming notifications", true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "power_led")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);
                        //Perform Vibration setting logic here
                    }
                },
                null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Sounds",
                "Play sound on incomming notifications",
                true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "sounds")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);
                        //Perform Play sound on incomming notifications
                    }
                }, null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Notification Tone1",
                "Set your notification ringtone",
                false,
                null,
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Open window to select notification sound/ring tone
                        final String notificationsound = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getString(Constants.CHOSEN_NOTIFICATION_TONE, "");
                        Uri uri = null;
                        if(notificationsound.isEmpty()){
                            uri = null;
                        }else{
                            uri = Uri.parse(notificationsound);
                        }

                        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, uri);
                        startActivityForResult(intent, Constants.SELECT_NOTIFICATION_SOUND);
                    }
                }));


        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Wall Post",
                null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "wall_post")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);

                        AppSettingModel appSettingModel = new AppSettingModel();
                        appSettingModel.setUserId(App.getInstance().getUser().getId());
                        boolean checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("comments",true);
                        appSettingModel.setComments(checkisOn);
                        appSettingModel.setWallPost(isOn);
                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("photo_tag",true);
                        appSettingModel.setPhotoTag(checkisOn);

                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("friend_request",true);

                        appSettingModel.setFriendrequest(checkisOn);

                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("messages",true);
                        appSettingModel.setMessages(checkisOn);

                        ChangeAppSetting(appSettingModel);

                    }
                },
                null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Messages",
                null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "messages")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);

                        AppSettingModel appSettingModel = new AppSettingModel();
                        appSettingModel.setUserId(App.getInstance().getUser().getId());
                        boolean checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("wall_post",true);
                        appSettingModel.setWallPost(checkisOn);
                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("photo_tag",true);
                        appSettingModel.setPhotoTag(checkisOn);

                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("friend_request",true);

                        appSettingModel.setFriendrequest(checkisOn);

                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("comments",true);
                        appSettingModel.setMessages(isOn);
                        appSettingModel.setComments(checkisOn);


                        ChangeAppSetting(appSettingModel);

                    }
                },
                null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Comments",
                null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "comments")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);
                        AppSettingModel appSettingModel = new AppSettingModel();
                        appSettingModel.setUserId(App.getInstance().getUser().getId());
                        boolean checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("wall_post",true);
                        appSettingModel.setWallPost(checkisOn);
                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("photo_tag",true);
                        appSettingModel.setPhotoTag(checkisOn);

                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("friend_request",true);
                        appSettingModel.setComments(isOn);
                        appSettingModel.setFriendrequest(checkisOn);

                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("messages",true);
                        appSettingModel.setMessages(checkisOn);



                        ChangeAppSetting(appSettingModel);



                    }
                },
                null));
        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Friend Request",
                null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "friend_request")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);

                            AppSettingModel appSettingModel = new AppSettingModel();
                            appSettingModel.setUserId(App.getInstance().getUser().getId());
                            boolean checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                    .getBoolean("wall_post",true);
                            appSettingModel.setWallPost(checkisOn);
                            checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                    .getBoolean("photo_tag",true);
                            appSettingModel.setPhotoTag(checkisOn);
                            appSettingModel.setFriendrequest(isOn);
                            checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                    .getBoolean("comments",true);
                            appSettingModel.setComments(checkisOn);


                            checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                    .getBoolean("messages",true);
                            appSettingModel.setMessages(checkisOn);



                            ChangeAppSetting(appSettingModel);



                    }
                },
                null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Photo Tag",
                null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "photo_tag")
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
                    {
                        super.onCheckedChanged(compoundButton, isOn);

                            AppSettingModel appSettingModel = new AppSettingModel();
                            appSettingModel.setUserId(App.getInstance().getUser().getId());
                            boolean checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                    .getBoolean("wall_post",true);
                            appSettingModel.setWallPost(checkisOn);
                            checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                    .getBoolean("friend_request",true);

                            appSettingModel.setFriendrequest(checkisOn);
                            checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                    .getBoolean("comments",true);
                        appSettingModel.setComments(checkisOn);


                        checkisOn = PreferenceManager.getDefaultSharedPreferences(ActivitySettings.this)
                                .getBoolean("messages",true);
                        appSettingModel.setMessages(checkisOn);


                            appSettingModel.setPhotoTag(isOn);
                            ChangeAppSetting(appSettingModel);


                    }
                },
                null));


        //adapter.addSettingsItem(new SingleSettingsItemModel(this, "Wall Post", null, true,
          //      new SingleSettingsItemModel.SettingsChangedListener(this, "wall_post"), null));

      //  adapter.addSettingsItem(new SingleSettingsItemModel(this, "Messages", null, true,
        //        new SingleSettingsItemModel.SettingsChangedListener(this, "messages"), null));

       // adapter.addSettingsItem(new SingleSettingsItemModel(this, "Comments", null, true,
      //          new SingleSettingsItemModel.SettingsChangedListener(this, "comments"), null));

      //  adapter.addSettingsItem(new SingleSettingsItemModel(this, "Friend Request", null, true,
        //        new SingleSettingsItemModel.SettingsChangedListener(this, "friend_request"), null));

       // adapter.addSettingsItem(new SingleSettingsItemModel(this, "Photo Tag", null, true,
        //        new SingleSettingsItemModel.SettingsChangedListener(this, "photo_tag"), null));

      /*  adapter.addSettingsItem(new SingleSettingsItemModel(this, "Event Invites", null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "event_invites"), null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Application Requests", null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "application_request"), null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Groups", null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "groups"), null));

        adapter.addSettingsItem(new SingleSettingsItemModel(this, "Place Tips", null, true,
                new SingleSettingsItemModel.SettingsChangedListener(this, "place_trips"), null));
*/
    }
    public  void ChangeAppSetting(AppSettingModel appSettingModel){
        final RetrofitCallbacks<ResponseBody> onchangeSetting =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if(response.isSuccessful()){

                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        super.onFailure(call, t);

                    }
                };

        API.getInstance().appSetting(appSettingModel,onchangeSetting);

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent)
    {
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.SELECT_NOTIFICATION_SOUND)
        {
            Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (uri != null)
            {
                PreferenceManager.getDefaultSharedPreferences(this)
                        .edit()
                        .putString(Constants.CHOSEN_NOTIFICATION_TONE, uri.toString())
                        .apply();
                Toast.makeText(this, "Notification Tone Changed", Toast.LENGTH_SHORT).show();
            } else
            {
                PreferenceManager.getDefaultSharedPreferences(this)
                        .edit()
                        .putString(Constants.CHOSEN_NOTIFICATION_TONE, null)
                        .apply();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class SettingsAdapter extends RecyclerView.Adapter
    {

        public void addSettingsItem(SingleSettingsItemModel model)
        {
            settingsItemModelList.add(model);
            notifyItemInserted(settingsItemModelList.size());
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            return VHSingleSettingsItem.create(ActivitySettings.this, parent);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            VHSingleSettingsItem.bind((VHSingleSettingsItem) holder, settingsItemModelList.get(position));
        }

        @Override
        public int getItemCount()
        {
            return settingsItemModelList.size();
        }
    }

}
