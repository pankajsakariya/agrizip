package com.app.agrizip.settings;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;

import java.util.HashMap;

import api.API;
import api.RetrofitCallbacks;
import apimodels.ChangeUserPrivacy;
import apimodels.UserPrivacySettings;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityPrivacySettings extends AppCompatActivity
{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.btn_save_privacy)
    Button btn_save_privacy;


    UserPrivacySettings userPrivacySettings;

   // ChangeUserPrivacy changeUserPrivacy;

    @BindView(R.id.rb_futurePostEveryone)
    RadioButton rbFuturePostEveryone;

    @BindView(R.id.rb_futurePostFriends)
    RadioButton rbFuturePostFriends;

    @BindView(R.id.rb_emailIdEveryone)
    RadioButton rbEmailIdEveryone;

    @BindView(R.id.rb_emailIdFriend)
    RadioButton rbEmailIdFriend;

    @BindView(R.id.rb_emailIdOnlyMe)
    RadioButton rbEmailIdOnlyMe;

    @BindView(R.id.rb_mobileNoEveryone)
    RadioButton rbMobileNoEveryone;

    @BindView(R.id.rb_mobileNoFriends)
    RadioButton rbMobileNoFriends;

    @BindView(R.id.rb_mobileNoOnlyMe)
    RadioButton rbMobileNoOnlyMe;

    @BindView(R.id.rb_birthDateEveryone)
    RadioButton rbBirthDateEveryone;

    @BindView(R.id.rb_birthDateFriend)
    RadioButton rbBirthDateFriend;

    @BindView(R.id.rb_birthDateOnlyMe)
    RadioButton rbBirthDateOnlyMe;

    ProgressDialog pd;

    ChangeUserPrivacy changeUserPrivacy;

    HashMap<String,String> privacyId = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_settings);

        ButterKnife.bind(this);
        changeUserPrivacy = new ChangeUserPrivacy();

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null)
            {
                if (tvToolbarTitle != null)
                {
                    tvToolbarTitle.setText("Privacy Setting");
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        radioButtonCheck();

        getPrivacySettings();

    }
    public void radioButtonCheck(){

    }

    @OnClick(R.id.btn_save_privacy)
    public void savePrivacySetting(){
        changeUserPrivacy.setUserId(App.getInstance().getUser().getId());
         if(rbFuturePostEveryone.isChecked()){
             changeUserPrivacy.setWhoseefuturepost(privacyId.get("Public"));
         }else{
            changeUserPrivacy.setWhoseefuturepost(privacyId.get("Friends"));
         }
        if(rbMobileNoEveryone.isChecked()){
            changeUserPrivacy.setWhoseemobile(privacyId.get("Public"));

        }else if(rbMobileNoFriends.isChecked()){
            changeUserPrivacy.setWhoseemobile(privacyId.get("Friends"));

        }else{
            changeUserPrivacy.setWhoseemobile(privacyId.get("Only Me"));

        }

        if(rbEmailIdFriend.isChecked()){
            changeUserPrivacy.setWhoseeemail(privacyId.get("Friends"));

        }else if(rbEmailIdEveryone.isChecked()){
            changeUserPrivacy.setWhoseeemail(privacyId.get("Public"));

        }else{
            changeUserPrivacy.setWhoseeemail(privacyId.get("Only Me"));

        }

        if(rbBirthDateFriend.isChecked()){
            changeUserPrivacy.setWhoseeDOB(privacyId.get("Friends"));

        }else if(rbEmailIdEveryone.isChecked()){
            changeUserPrivacy.setWhoseeDOB(privacyId.get("Public"));

        }else{
            changeUserPrivacy.setWhoseeDOB(privacyId.get("Only Me"));

        }

       final RetrofitCallbacks<ResponseBody> setPrivacy = new RetrofitCallbacks<ResponseBody>(this){
           @Override
           public void onResponse(Call call, Response response) {
               super.onResponse(call, response);
               if(response.isSuccessful()){
                   Toast.makeText(ActivityPrivacySettings.this,"Privacy Setting Change",Toast.LENGTH_LONG).show();
               finish();
               }
           }

           @Override
           public void onFailure(Call call, Throwable t) {
               super.onFailure(call, t);
               Toast.makeText(ActivityPrivacySettings.this,"Something Wrong on Server.",Toast.LENGTH_LONG).show();

           }
       };

        API.getInstance().changePrivacySettings(changeUserPrivacy,setPrivacy);
    }

    private void getPrivacySettings()
    {

        pd = ProgressDialog.show(this, "Please Wait...", "Getting your privacy details",
                true, true, new DialogInterface.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialogInterface)
                    {
                        finish();
                    }
                });

        pd.setCanceledOnTouchOutside(false);

        final RetrofitCallbacks<UserPrivacySettings> onGetPrivacySettings =
                new RetrofitCallbacks<UserPrivacySettings>(this)
                {
                    @Override
                    public void onResponse(Call<UserPrivacySettings> call, Response<UserPrivacySettings> response)
                    {
                        super.onResponse(call, response);
                        if (pd.isShowing())
                        {
                            pd.dismiss();
                        }
                        if (response.isSuccessful())
                        {
                            userPrivacySettings = response.body();
                            for(int i =0 ;i< response.body().getData().getPrivacylist().size() ;i ++){
                                privacyId.put(response.body().getData().getPrivacylist().get(i).getPrivacyName(),response.body().getData().getPrivacylist().get(i).getPrivacyId());
                            }

                            for (UserPrivacySettings.DataBean.PrivacylistBean privacy :
                                    userPrivacySettings.getData().getPrivacylist())
                            {
                                if (response.body().getData().getWhoseeDOB()
                                        .equals(privacy.getPrivacyId()))
                                {
                                    if (privacy.getPrivacyName().equals("Friends"))
                                    {
                                        rbBirthDateFriend.setChecked(true);
                                    } else if (privacy.getPrivacyName().equals("Only Me"))
                                    {
                                        rbBirthDateOnlyMe.setChecked(true);
                                    } else if (privacy.getPrivacyName().equals("Public"))
                                    {
                                        rbBirthDateEveryone.setChecked(true);
                                    }
                                }
                            }


                            for (UserPrivacySettings.DataBean.PrivacylistBean privacy :
                                    userPrivacySettings.getData().getPrivacylist())
                            {
                                if (response.body().getData().getWhoseeemail()
                                        .equals(privacy.getPrivacyId()))
                                {
                                    if (privacy.getPrivacyName().equals("Friends"))
                                    {
                                        rbEmailIdFriend.setChecked(true);
                                    } else if (privacy.getPrivacyName().equals("Only Me"))
                                    {
                                        rbEmailIdOnlyMe.setChecked(true);
                                    } else if (privacy.getPrivacyName().equals("Public"))
                                    {
                                        rbEmailIdEveryone.setChecked(true);
                                    }
                                }
                            }


                            for (UserPrivacySettings.DataBean.PrivacylistBean privacy :
                                    userPrivacySettings.getData().getPrivacylist())
                            {
                                if (response.body().getData().getWhoseefuturepost()
                                        .equals(privacy.getPrivacyId()))
                                {
                                    if (privacy.getPrivacyName().equals("Friends"))
                                    {
                                        rbFuturePostFriends.setChecked(true);
                                    } else if (privacy.getPrivacyName().equals("Public"))
                                    {
                                        rbFuturePostEveryone.setChecked(true);
                                    }
                                }
                            }

                            for (UserPrivacySettings.DataBean.PrivacylistBean privacy :
                                    userPrivacySettings.getData().getPrivacylist())
                            {
                                if (response.body().getData().getWhoseemobile()
                                        .equals(privacy.getPrivacyId()))
                                {
                                    if (privacy.getPrivacyName().equals("Friends"))
                                    {
                                        rbMobileNoFriends.setChecked(true);
                                    } else if (privacy.getPrivacyName().equals("Only Me"))
                                    {
                                        rbMobileNoOnlyMe.setChecked(true);
                                    } else if (privacy.getPrivacyName().equals("Public"))
                                    {
                                        rbMobileNoEveryone.setChecked(true);
                                    }
                                }
                            }
                        } else
                        {
                            Toast.makeText(ActivityPrivacySettings.this, "something went wrong please try later", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserPrivacySettings> call, Throwable t)
                    {
                        super.onFailure(call, t);
                        if (pd.isShowing())
                        {
                            pd.dismiss();
                        }
                    }
                };

        API.getInstance().getUserPrivacyDetails(App.getInstance().getUser().getId(), onGetPrivacySettings);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
