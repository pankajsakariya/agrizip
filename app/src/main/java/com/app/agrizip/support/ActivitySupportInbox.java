package com.app.agrizip.support;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CreateSupport;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class ActivitySupportInbox extends AppCompatActivity implements Validator.ValidationListener
{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @NotEmpty
    @BindView(R.id.et_supportSubject)
    EditText etSupportSubject;

    @NotEmpty
    @BindView(R.id.et_supportDescription)
    EditText etSupportDescription;

    @BindView(R.id.btn_sendSupport)
    Button btnSendSupport;

    Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_inbox);

        ButterKnife.bind(this);

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null)
            {
                if (tvToolbarTitle != null)
                {
                    tvToolbarTitle.setText(R.string.suppert_inbox);
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        validator = new Validator(this);
        validator.setValidationListener(this);

        btnSendSupport.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                validator.validate();
            }
        });

    }

    @Override
    public void onValidationSucceeded()
    {
        promptUserForSendingSupport();
    }

    private void promptUserForSendingSupport()
    {

        new AlertDialog.Builder(this)
                .setTitle("Support")
                .setTitle("Are you sure you want to send support?")
                .setPositiveButton("SEND", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        sendSupport();
                    }
                }).setNegativeButton("CANCEL", null)
                .show();

    }

    private void sendSupport()
    {

        final CreateSupport support = new CreateSupport();
        support.setSubject(etSupportSubject.getText().toString());
        support.setDesciption(etSupportDescription.getText().toString());

        final RetrofitCallbacks callback = new RetrofitCallbacks(this)
        {

            @Override
            public void onResponse(Call call, Response response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    Toast.makeText(ActivitySupportInbox.this, "Support submited successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        };

        API.getInstance().createSupport(support, callback);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors)
    {
        for (ValidationError error : errors)
        {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText)
            {
                ((EditText) view).setError(message);
            } else
            {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
