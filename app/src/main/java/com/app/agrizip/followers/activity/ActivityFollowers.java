package com.app.agrizip.followers.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.followers.adapter.FollowersListAdapter;
import com.app.agrizip.profile.ActivityUserProfile;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FollowersList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityFollowers extends AppCompatActivity implements FollowersList.FollowerActionListener {

    boolean busyLoadingData = false;

    int pageNo = 1;
    public static int FlowCountpos, FlowCount;
    int pastVisibleItems, visibleItemCount, totalItemCount;

    LinearLayoutManager llm;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.rv_followers)
    RecyclerView rvFollowers;

    @BindView(R.id.ll_pleaseWait)
    LinearLayout llPleaseWait;

    @BindView(R.id.fl_emptyPendingRequest)
    FrameLayout fl_emptyPendingRequest;
    FollowersListAdapter adapter;


    final RetrofitCallbacks<FollowersList> onGetFollowingListCallback =
            new RetrofitCallbacks<FollowersList>(this) {
                @Override
                public void onResponse(Call<FollowersList> call, Response<FollowersList> response) {
                    super.onResponse(call, response);
                    llPleaseWait.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        for (FollowersList.DataBean singleFollower : response.body().getData()) {
                            singleFollower.setFriendunfollowActionListener(ActivityFollowers.this);
                            adapter.addFollower(singleFollower);
                        }
                        if (adapter.getItemCount() > 0) {
                            fl_emptyPendingRequest.setVisibility(View.GONE);
                        }

                    } else {
                        Toast.makeText(ActivityFollowers.this, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<FollowersList> call, Throwable t) {
                    super.onFailure(call, t);
                    llPleaseWait.setVisibility(View.GONE);
                }
            };
    public static String userId;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers);

        userId = getIntent().getStringExtra(Constants.USER_ID);
        FlowCountpos = getIntent().getIntExtra("POSITION", 0);
        FlowCount = getIntent().getIntExtra("FOLLOW_COUNT", 0);
        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        llm = new LinearLayoutManager(this);

        rvFollowers.setLayoutManager(llm);
        rvFollowers.setHasFixedSize(true);
        rvFollowers.addOnScrollListener(new OnScrollRecyclerView());

        adapter = new FollowersListAdapter(this);

        rvFollowers.setAdapter(adapter);

        if (getIntent().getBooleanExtra(Constants.IS_SEEING_FOLLOWERS, false)) {
            getFollowers();
            if (tvToolbarTitle != null) {
                tvToolbarTitle.setText(R.string.following);
                
            }
        } else {
            getFollowList();
            if (tvToolbarTitle != null) {
                tvToolbarTitle.setText(R.string.follower);
            }
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void getFollowList() {
        llPleaseWait.setVisibility(View.VISIBLE);

        API.getInstance().getFollowlist(userId,
                pageNo, onGetFollowingListCallback);

    }

    private void getFollowers() {
        llPleaseWait.setVisibility(View.VISIBLE);

        API.getInstance().getFollowinglist(userId,
                pageNo, onGetFollowingListCallback);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if(Constants.UPDATE_FLLOWCOUNT){
            Intent intent = new Intent(ActivityFollowers.this, ActivityUserProfile.class);
            intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
            intent.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
                Constants.UPDATE_FLLOWCOUNT = false;
            }else{
                finish();
            }


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if(Constants.UPDATE_FLLOWCOUNT){
            Intent intent = new Intent(ActivityFollowers.this, ActivityUserProfile.class);
            intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
            intent.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Constants.UPDATE_FLLOWCOUNT = false;
        }else{
            finish();
        }
    }

    @Override
    public void onUnfollow(FollowersList.DataBean UnFollowfriend, int position) {
        adapter.removeFollower(UnFollowfriend, position);
        if(adapter.getItemCount() == 0){
            fl_emptyPendingRequest.setVisibility(View.VISIBLE);
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("ActivityFollowers Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    class OnScrollRecyclerView extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
                if (busyLoadingData) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        busyLoadingData = false;
                        pageNo++;
                        getFollowers();
                    }
                }
            }
        }
    }
}
