package com.app.agrizip.followers.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import apimodels.FollowersList;
import viewholders.VHSingleFollower;

/**
 * Created by rutvik on 1/18/2017 at 6:55 PM.
 */

public class FollowersListAdapter extends RecyclerView.Adapter
{

    private final Context context;

    private final List<FollowersList.DataBean> followerList;

    public FollowersListAdapter(final Context context)
    {
        this.context = context;
        followerList = new ArrayList<>();
    }
    public void removeFollower(FollowersList.DataBean followerFriend,int position){
        final int index = followerList.indexOf(followerFriend);
        followerList.remove(index);
        notifyItemRemoved(index);
       // System.out.println("Hello" + position);
       // notifyItemRemoved(index);
    }

    public void addFollower(FollowersList.DataBean singleFollower)
    {
        followerList.add(singleFollower);
        notifyItemInserted(followerList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingleFollower.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSingleFollower.bind((VHSingleFollower) holder,
                (FollowersList.DataBean) followerList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return followerList.size();
    }
}
