package com.app.agrizip.album.photos_list;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.agrizip.R;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import apimodels.GetAlbums;
import apimodels.PostImageList;
import extras.Constants;

/**
 * Created by macmini2 on 16/07/17.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    final Context context;

    final ArrayList<PostImageList.DataBean> photolist;
    private LayoutInflater inflater;
    public PhotoAdapter(Context context) {
        this.context = context;
        photolist = new ArrayList<>();
        inflater = LayoutInflater.from(context);

    }



    public void addsinglePhoto(PostImageList.DataBean singlePhoto) {
        photolist.add(singlePhoto);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.raw_single_image, parent, false);
        PhotoAdapter.ViewHolder holder = new PhotoAdapter.ViewHolder(view);
        return holder;


    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(photolist.get(position).getImagePath().length() > 0) {
            Picasso.with(context)
                    .load(photolist.get(position).getImagePath())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)

                    .into(holder.iv_photo);
        }
        holder.iv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ActivityFullPhotoView.class);
                i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, photolist);
                i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, position);
                i.putExtra("ScrollON", true);
                i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);
                Constants.COME_FROM_HOME = 0;
                context.startActivity(i);
            }
        });
/*
        Intent i = new Intent(context, ActivityFullPhotoView.class);
        i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, photosBeanArrayList);
        i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, position);
        i.putExtra("ScrollON", true);
        i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);
        Constants.COME_FROM_HOME = 0;
        startActivity(i);
        */
    }

    @Override
    public int getItemCount() {
        return photolist.size();
    }
    public void clear() {
        photolist.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_photo;
      public ViewHolder(View itemView) {
          super(itemView);
          iv_photo = (ImageView)itemView.findViewById(R.id.iv_singleAlbumPhotoGridItem);
      }
  }




}