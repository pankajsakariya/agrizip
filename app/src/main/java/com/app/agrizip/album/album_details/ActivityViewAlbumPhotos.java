package com.app.agrizip.album.album_details;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.album_create.ActivityCreateAlbum;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.app.agrizip.app.App;
import com.felipecsl.asymmetricgridview.library.Utils;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.util.ArrayList;

import adapters.DefaultListAdapter;
import api.API;
import api.RetrofitCallbacks;
import apimodels.GetAlbums;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import services.AlbumImageUploadingService;

public class ActivityViewAlbumPhotos extends AppCompatActivity {

    public static final int REQUEST_CODE_PICKER = 1342;

    private static final String TAG = App.APP_TAG + ActivityViewAlbumPhotos.class.getSimpleName();
    final Handler mHandler = new Handler();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbarTitle2)
    TextView tvToolbarTitle;
    @BindView(R.id.arv_albumPhotos)
    AsymmetricGridView arvAlbumPhotos;
    ArrayList<GetAlbums.DataBean.PhotosBean> photosBeanArrayList;
    GetAlbums.DataBean.PhotosBean newphotoadd = new GetAlbums.DataBean.PhotosBean();
    String albumName;
    AlertDialog promptPrivacyDialog;
    ImagePicker imagePicker;
    @BindView(R.id.srl_refreshAlbums)
    SwipeRefreshLayout srlRefreshAlbums;
    ArrayList<Image> selectedImages = new ArrayList<>();
    AsymmetricGridViewAdapter asymmetricAdapter;
    GetAlbums.DataBean.PhotosBean removedItem;
    DefaultListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_album_photos);

        ButterKnife.bind(this);

        albumName = getIntent().getStringExtra(Constants.ALBUM_NAME);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }

        imagePicker = ImagePicker.create(this);

        imagePicker.folderMode(false) // folder mode (false by default)
                .imageTitle("Tap to select") // image selection title
                .multi() // multi mode (default mode)
                .limit(20) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .origin(selectedImages); // original selected images, used in multi mode


        photosBeanArrayList = getIntent().getParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST);

        Log.i(TAG, "photosBeanArrayList SIZE: " + photosBeanArrayList.size());

        if (tvToolbarTitle != null && albumName != null) {
            tvToolbarTitle.setText(albumName);
        }


        photosBeanArrayList = (ArrayList<GetAlbums.DataBean.PhotosBean>) extras.Utils.moarItems(photosBeanArrayList);

        // Choose your own preferred column width
        arvAlbumPhotos.setRequestedColumnWidth(Utils.dpToPx(this, 120));

        // initialize your items array

        adapter = new DefaultListAdapter(this, photosBeanArrayList);

         asymmetricAdapter =
                new AsymmetricGridViewAdapter<>(this, arvAlbumPhotos, adapter);

        arvAlbumPhotos.setAdapter(asymmetricAdapter);


        arvAlbumPhotos.setAllowReordering(false);

        arvAlbumPhotos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(ActivityViewAlbumPhotos.this, ActivityFullPhotoView.class);
                i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, photosBeanArrayList);
                i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, position);
                i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, true);
                Constants.COME_FROM_HOME = 0;
                startActivity(i);
            }
        });

        arvAlbumPhotos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                showPhotoOptions(photosBeanArrayList.get(i).getPhotoID());
                return true;
            }
        });
        srlRefreshAlbums.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

    }

    private void resetAdapter() {
        if (removedItem != null) {
            photosBeanArrayList.remove(photosBeanArrayList.indexOf(removedItem));
        }

        DefaultListAdapter adapter = new DefaultListAdapter(this, photosBeanArrayList);

        AsymmetricGridViewAdapter asymmetricAdapter =
                new AsymmetricGridViewAdapter<>(this, arvAlbumPhotos, adapter);

        arvAlbumPhotos.setAdapter(asymmetricAdapter);
    }

    private void showPhotoOptions(final String photoId) {

        final ArrayList<String> options = new ArrayList<>();
        options.add("Hide Photo");
        options.add("Delete Photo");

        final ArrayAdapter<String> adapter
                = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(ActivityViewAlbumPhotos.this).inflate(
                            android.R.layout.simple_list_item_1, parent, false);
                }
                TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

                tv.setText(options.get(position));

                return convertView;

            }
        };

        promptPrivacyDialog = new AlertDialog.Builder(this)
                .setTitle("Photo Options")
                .setSingleChoiceItems(adapter, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            hidePhoto(photoId);
                        } else if (i == 1) {
                            deletePhoto(photoId);
                        }
                        dialogInterface.dismiss();
                    }
                })
                .show();

    }

    private void deletePhoto(final String photoId) {
        new AlertDialog.Builder(this)
                .setTitle("Delete photo")
                .setMessage("Are you sure you want to delete this photo?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        API.getInstance().deletePhotoFromAlbum(photoId,
                                new RetrofitCallbacks<ResponseBody>(ActivityViewAlbumPhotos.this) {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        super.onResponse(call, response);
                                        if (response.isSuccessful()) {

                                            for (GetAlbums.DataBean.PhotosBean pb : photosBeanArrayList) {
                                                if (pb.getPhotoID().equals(photoId)) {
                                                    removedItem = pb;
                                                }
                                            }

                                            mHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    resetAdapter();
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        super.onFailure(call, t);
                                    }
                                });
                    }
                }).setNegativeButton("Cancel", null)
                .show();
    }

    private void hidePhoto(final String photoId) {
        new AlertDialog.Builder(this)
                .setTitle("Hide photo")
                .setMessage("Are you sure you want to hide this photo?")
                .setPositiveButton("Hide", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        API.getInstance().hidePhotoFromAlbum(photoId,
                                new RetrofitCallbacks<ResponseBody>(ActivityViewAlbumPhotos.this) {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        super.onResponse(call, response);
                                        if (response.isSuccessful()) {
                                            Toast.makeText(ActivityViewAlbumPhotos.this,
                                                    "Photo now hidden", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        super.onFailure(call, t);
                                    }
                                });
                    }
                }).setNegativeButton("Cancel", null)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_view_album_photos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_editAlbum) {
            imagePicker.start(ActivityCreateAlbum.REQUEST_CODE_PICKER);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {

            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            this.selectedImages.clear();
            this.selectedImages.addAll(images);
            for(int i =0 ;i<images.size();i++){
                newphotoadd = new GetAlbums.DataBean.PhotosBean();
                newphotoadd.setAlbumID("1");
                newphotoadd.setPhotoID("1");
                newphotoadd.setImagestr(images.get(i).getPath());
                photosBeanArrayList.add(newphotoadd);
            }

            images.clear();
            adapter = new DefaultListAdapter(this, photosBeanArrayList);

            asymmetricAdapter =
                    new AsymmetricGridViewAdapter<>(this, arvAlbumPhotos, adapter);

            arvAlbumPhotos.setAdapter(asymmetricAdapter);
            promptUserForUploadingPhotosToAlbum();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void promptUserForUploadingPhotosToAlbum() {

        new AlertDialog.Builder(this)
                .setTitle("Upload Images to Album")
                .setMessage("Are you sure you want to upload " + selectedImages.size() +
                        " Images to " + albumName + " Album?")
                .setPositiveButton("Upload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startImageUploadingService();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityViewAlbumPhotos.this.selectedImages.clear();
                    }
                })
                .show();

    }

    private void startImageUploadingService() {
        Intent uploadAlbumPhotoService =
                new Intent(ActivityViewAlbumPhotos.this, AlbumImageUploadingService.class);

        final ArrayList<String> selectedImagePaths = new ArrayList<>();

        for (Image image : selectedImages) {
            selectedImagePaths.add(image.getPath());
        }

        uploadAlbumPhotoService
                .putStringArrayListExtra(Constants.BASE_64_IMAGE_STRING_LIST,
                        selectedImagePaths);

        uploadAlbumPhotoService.putExtra(Constants.ALBUM_ID,
                photosBeanArrayList.get(0).getAlbumID());

        uploadAlbumPhotoService.putExtra(Constants.ALBUM_NAME,
                albumName);

        startService(uploadAlbumPhotoService);

        Toast.makeText(this, "Started uploading images to album.", Toast.LENGTH_SHORT).show();
            Constants.ALBUM_EDIT = true;
    }

}
