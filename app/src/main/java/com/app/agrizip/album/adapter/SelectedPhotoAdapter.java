package com.app.agrizip.album.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.nguyenhoanglam.imagepicker.activity.ImagePicker;

import java.util.ArrayList;

import extras.Constants;
import viewholders.VHSingleAlbumPhoto;

/**
 * Created by rutvik on 1/5/2017 at 12:14 PM.
 */

public class SelectedPhotoAdapter extends RecyclerView.Adapter
{

    final Context context;

    final ArrayList<String> selectedImages;

    final ImagePicker imagePicker;

    public SelectedPhotoAdapter(Context context, ImagePicker imagePicker)
    {
        this.context = context;
        this.imagePicker = imagePicker;
        selectedImages = new ArrayList<>();
    }

    public void addAddImageButton()
    {
        selectedImages.add(0, Constants.ADD_NEW_PHOTO_TO_ALBUM);
        notifyItemInserted(0);
    }

    public void addSelectedImage(String selectedImagePath)
    {
        selectedImages.add(1, selectedImagePath);
        notifyItemInserted(selectedImages.size());
    }

    public void clear()
    {
        for (int i = selectedImages.size() - 1; i > 0; i--)
        {
            selectedImages.remove(i);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingleAlbumPhoto.create(context, parent, imagePicker);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSingleAlbumPhoto.bind((VHSingleAlbumPhoto) holder, selectedImages.get(position));
    }

    @Override
    public int getItemCount()
    {
        return selectedImages.size();
    }
}
