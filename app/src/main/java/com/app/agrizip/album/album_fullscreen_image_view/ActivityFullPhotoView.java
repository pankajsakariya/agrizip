package com.app.agrizip.album.album_fullscreen_image_view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.adapter.CustomImageLoadAdapter;
import com.app.agrizip.album.adapter.PhotoPagerAdapter;
import com.app.agrizip.album.album_details.ActivityViewAlbumPhotos;
import com.app.agrizip.app.App;
import com.app.agrizip.friends.friend_request.FragmentRequests;
import com.app.agrizip.friends.friend_tag.ActivityTagFriends;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.home.fragment.FragmentChats;
import com.app.agrizip.post.post_comments.dailog.CommentsDialog;
import com.app.agrizip.post.post_like_list.dialog.LikeListDialog;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.squareup.picasso.Picasso;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.GetAlbums;
import apimodels.GetSinglePostDetails;
import apimodels.LikeUnlikePost;
import apimodels.LikeUnlikeResponse;
import apimodels.PostImageList;
import apimodels.SharePost;
import apimodels.SinglePost;
import apimodels.UserPrivacyDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.measite.minidns.record.A;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import views.SingleFullViewPhoto;

public class ActivityFullPhotoView extends AppCompatActivity implements MenuBuilder.Callback,CommentsDialog.CommentCountChangeListener {

    private static final String TAG = App.APP_TAG + ActivityViewAlbumPhotos.class.getSimpleName();

    @BindView(R.id.vp_fullPhotoView)
    ViewPager vpFullPhotoView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_pv_makeProfilePicture)
    Button btnMakeProfilePicture;

    @BindView(R.id.iv_pv_userProfilePic)
    ImageView ivUserProfilePic;

    @BindView(R.id.tv_pv_views)
    TextView tvViews;

    @BindView(R.id.tv_pv_comment)
    TextView tvComment;

    @BindView(R.id.tv_pv_like)
    TextView like;

    @BindView(R.id.tv_pv_likeCount)
    TextView tvLikeCount;

    @BindView(R.id.tv_pv_shareCount)
    TextView tvShareCount;

    @BindView(R.id.tv_pv_location)
    TextView tvLocation;

    @BindView(R.id.rl_topContentContainer)
    RelativeLayout rlTopContentContainer;

    @BindView(R.id.ll_photoSocialOptionsContainer)
    LinearLayout llPhotoSocialOptionsContainer;

    @BindView(R.id.rl_countContainer)
    RelativeLayout rlCountContainer;

    @BindView(R.id.tv_pv_userName)
    TextView tvUserName;

    @BindView(R.id.tv_pv_date)
    TextView tvDate;

    @BindView(R.id.tv_post_content)
    TextView tv_post_content;

    @BindView(R.id.pb_loadimage)
    ProgressBar pb_loadimage;



    @BindView(R.id.iv_menu)
    ImageView iv_menu;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    public static String commentCount = "0";
    public String shareCount ="0";
    public String likeCount = "0";

    MenuPopupHelper photoExtraOptions1,photoExtraOptions2,photoExtraOptions3;
    MenuBuilder menuBuilder;
    MenuBuilder menuBuilder2;
    MenuBuilder menuBuilder3;
   public static File dir,getAbsulutePath;


    String filePath;
    ArrayList<GetAlbums.DataBean.PhotosBean> albumPhotosBeanList;

    ArrayList<PostImageList.DataBean> postImagesList;
    CommentsDialog commentsDialog;
    PopupMenu photoExtraOptions;
    int pagerViewIndex = 0;
    boolean isSeeingAlbumPhotos;
    Call<GetSinglePostDetails> apiCall;
    Call<LikeUnlikeResponse> likeApiCall;
    SinglePost post;
    int tempLike;
    private PhotoPagerAdapter adapter = null;
    String PostuserId;
    final ArrayList<String> taggedFriendIds = new ArrayList<>();
    final ArrayList<String> taggedFriendNames = new ArrayList<>();

    private CustomImageLoadAdapter adapter1;

    boolean ScrollON=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_full_photo_view);

        ButterKnife.bind(this);

        isSeeingAlbumPhotos =
                getIntent().getBooleanExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);

        if (toolbar != null) {
            if (isSeeingAlbumPhotos) {
                iv_menu.setVisibility(View.GONE);
            } else {
                setSupportActionBar(toolbar);
                if (getSupportActionBar() != null) {
                    // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    //final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                    // getSupportActionBar().setHomeAsUpIndicator(upArrow);
                    PostuserId = getIntent().getStringExtra("PostUserId");
                    getSupportActionBar().setTitle("");
                }
            }
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.COME_FROM_HOME == 1) {
                    Constants.COME_FROM_HOME =0;
                    Intent intent = new Intent(ActivityFullPhotoView.this, ActivityHome.class);
                    intent.putExtra("COMMENT_COUNT", Integer.parseInt(commentCount));
                    intent.putExtra("LIKE_COUNT", Integer.parseInt(tvLikeCount.getText().toString()));
                    intent.putExtra("SHARE_COUNT", Integer.parseInt(tvShareCount.getText().toString()));

                    setResult(RESULT_OK, intent);
                }
                else if(Constants.COME_FROM_HOME == 2) {
                    Constants.COME_FROM_HOME =0;
                    Intent intent = new Intent(ActivityFullPhotoView.this, ActivityUserProfile.class);
                    intent.putExtra("COMMENT_COUNT", Integer.parseInt(commentCount));
                    intent.putExtra("LIKE_COUNT", Integer.parseInt(tvLikeCount.getText().toString()));
                    intent.putExtra("SHARE_COUNT", Integer.parseInt(tvShareCount.getText().toString()));

                    setResult(RESULT_OK, intent);
                }

                finish();
            }
        });
        menuBuilder = new MenuBuilder(this);
        menuBuilder2 = new MenuBuilder(this);
        menuBuilder3 = new MenuBuilder(this);
        new MenuInflater(this).inflate(R.menu.popup_photo_options, menuBuilder);
        new MenuInflater(this).inflate(R.menu.popup_photo_option2, menuBuilder2);
        new MenuInflater(this).inflate(R.menu.popup_photo_option3, menuBuilder3);

        photoExtraOptions3 = new MenuPopupHelper(this,menuBuilder3,iv_menu);
        photoExtraOptions3.setForceShowIcon(true);
        menuBuilder3.setCallback(this);

        photoExtraOptions2 = new MenuPopupHelper(this,menuBuilder2,iv_menu);
        photoExtraOptions2.setForceShowIcon(true);
        menuBuilder2.setCallback(this);

        photoExtraOptions1 = new MenuPopupHelper(this, menuBuilder, iv_menu);
        photoExtraOptions1.setForceShowIcon(true);
        menuBuilder.setCallback(this);


        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemTitle = "";
                if(post.isNotification()){
                    itemTitle ="Turn off Notification";
                }else{
                    itemTitle ="Start Notification";
                }
                if(App.getInstance().getUser().getId().equalsIgnoreCase(post.getUserID())){
                    menuBuilder2.getItem(3).setTitle(itemTitle);
                    photoExtraOptions2.show();
                }
                else{
                    if (post.getTagDetails() != null) {
                        for (int i = 0; i < post.getTagDetails().size(); i++) {
                            if (App.getInstance().getUser().getId().equalsIgnoreCase(post.getTagDetails().get(i).getTagId())) {
                              menuBuilder.getItem(3).setTitle(itemTitle);
                                photoExtraOptions1.show();
                            } else {
                                menuBuilder3.getItem(2).setTitle(itemTitle);
                                photoExtraOptions3.show();
                                //   photoExtraOptions.getMenu().getItem(3).setVisible(false);
                            }
                        }
                    }else{
                        menuBuilder3.getItem(2).setTitle(itemTitle);
                        photoExtraOptions3.show();
                    }
                }
            }
        });



        tvComment.setText(commentCount);
        tvShareCount.setText(shareCount);
        tvLikeCount.setText(likeCount);
        pagerViewIndex = getIntent().getIntExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, 0);


       // adapter1 = new CustomImageLoadAdapter(this,imagePath);
        adapter = new PhotoPagerAdapter();

        vpFullPhotoView.setAdapter(adapter);


        if (isSeeingAlbumPhotos) {
            rlCountContainer.setVisibility(View.GONE);
            rlTopContentContainer.setVisibility(View.GONE);
            llPhotoSocialOptionsContainer.setVisibility(View.INVISIBLE);
            tv_post_content.setVisibility(View.GONE);

            albumPhotosBeanList = getIntent().getParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST);
            for (GetAlbums.DataBean.PhotosBean photo : albumPhotosBeanList) {
                addView(new SingleFullViewPhoto(this, photo.getImagestr()));
            }
        } else {

            postImagesList = getIntent().getParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST);
            ScrollON = getIntent().getBooleanExtra("ScrollON",false);
            for (PostImageList.DataBean photo : postImagesList) {
                addView(new SingleFullViewPhoto(this, photo.getImagePath()));
            }
            getPostDetails(postImagesList
                    .get(pagerViewIndex).getStatusId());
        }


        /*if(App.getInstance().getUser().getId().equalsIgnoreCase(post.getUserID())){
            photoExtraOptions.getMenu().getItem(0).setVisible(false);
        }*/
        vpFullPhotoView.setCurrentItem(pagerViewIndex);

        vpFullPhotoView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.i(TAG, "onPageScrolled POSITION: " + position);
                if(ScrollON){
                    System.out.println("Hello");
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.i(TAG, "onPageSelected POSITION: " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.i(TAG, "onPageScrollStateChanged STATE: " + state);

                if (!isSeeingAlbumPhotos) {
                    if (state == ViewPager.SCROLL_STATE_IDLE) {
                        if(ScrollON){
                             getPostDetails(postImagesList
                                    .get(vpFullPhotoView.getCurrentItem()).getStatusId());

                        }
                           }
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {


            if (data != null) {
                ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                for (Image singleImage : images) {
                    commentsDialog.postImageAsComment(singleImage);
                    //  postImageAsComment(singleImage);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  super.onActivityResult(requestCode, resultCode, data);
    }

    private void getPostDetails(String statusId) {

        pb_loadimage.setVisibility(View.VISIBLE);

        if (apiCall != null) {
            apiCall.cancel();
        }

        final RetrofitCallbacks<GetSinglePostDetails> onGetPostDetailsCallback =
                new RetrofitCallbacks<GetSinglePostDetails>(this) {

                    @Override
                    public void onResponse(Call<GetSinglePostDetails> call, Response<GetSinglePostDetails> response) {
                        super.onResponse(call, response);
                        pb_loadimage.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            post = response.body().getData().get(0);
                            if (App.getInstance().getUser().getId().equalsIgnoreCase(post.getUserID())) {
                                if(post.getTagDetails() != null){
                                    for(int i= 0 ; i<post.getTagDetails().size();i++){
                                        taggedFriendIds.add(post.getTagDetails().get(i).getTagFriendId());
                                        taggedFriendNames.add(post.getTagDetails().get(i).getFullName());
                                    }
                                }
                                photoExtraOptions.getMenu().getItem(3).setVisible(false);

                                btnMakeProfilePicture.setVisibility(View.VISIBLE);
                            } else {
                                btnMakeProfilePicture.setVisibility(View.GONE);
                                if(photoExtraOptions.getMenu() == null){

                                }else{
                                    photoExtraOptions.getMenu().getItem(0).setVisible(false);

                                }
                                if (post.getTagDetails() != null) {
                                    for (int i = 0; i < post.getTagDetails().size(); i++) {
                                        if (App.getInstance().getUser().getId().equalsIgnoreCase(post.getTagDetails().get(i).getTagId())) {

                                        } else {
                                            photoExtraOptions.getMenu().getItem(3).setVisible(false);
                                        }
                                    }
                                } else {
                                    photoExtraOptions.getMenu().getItem(3).setVisible(false);
                                }

                            }
                            setupDataIntoView(response.body().getData().get(0));
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<GetSinglePostDetails> call, Throwable t) {
                        super.onFailure(call, t);
                        pb_loadimage.setVisibility(View.GONE);
                    }
                };


        apiCall = API.getInstance().getStatusPostById(statusId, App.getInstance().getUser().getId(), onGetPostDetailsCallback);

    }


    public void promptUserForSelectingPostPrivacy() {
        final List<UserPrivacyDetails.DataBean> privacyList =
                App.getInstance().getUser().getUserPrivacyDetails().getData();

        final ArrayAdapter<UserPrivacyDetails.DataBean> privacyAdapter =
                new ArrayAdapter<UserPrivacyDetails.DataBean>(this, android.R.layout.simple_list_item_1, privacyList) {

                    @NonNull
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {
                            convertView = LayoutInflater.from(ActivityFullPhotoView.this).inflate(
                                    android.R.layout.simple_list_item_1, parent, false);
                        }
                        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

                        tv.setText(privacyList.get(position).getUserTypeName());

                        return convertView;

                    }
                };

        if (App.getInstance().getUser().getUserPrivacyDetails() == null) {
            return;
        }
        LayoutInflater inflater = this.getLayoutInflater();
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView tvtitle = (TextView)titleView.findViewById(R.id.tv_title);
        tvtitle.setText("Select Post Privacy");

        new AlertDialog.Builder(this)
                .setCustomTitle(titleView)
                .setTitle("Select Post Privacy")
                .setSingleChoiceItems(privacyAdapter, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        changePostPrivacy(privacyAdapter.getItem(i).getId());
                        dialogInterface.dismiss();
                    }
                })
                .show();

    }

    private void changePostPrivacy(String privacyId) {
        final RetrofitCallbacks<ResponseBody> onChangePostPrivactyCallback =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            Toast.makeText(ActivityFullPhotoView.this, "Post privacy changed", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ActivityFullPhotoView.this, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().editPostPrivacy(post.getStatusId(), privacyId, onChangePostPrivactyCallback);
    }

    private void setupDataIntoView(SinglePost post) {

        tvUserName.setText(post.getUserName());
        tvLocation.setText(post.getPlace() != null ? post.getPlace() : "");
        tvLocation.setVisibility(post.getPlace() == null ? View.INVISIBLE : View.VISIBLE);
        tvShareCount.setText(post.getShareCounts() + "");
        tv_post_content.setText(post.getStatus());
      /*  String convertdate = Utils.changeDateFormat(post.getCreateDate(),"yyyy-MM-dd T HH:mm:ss.SSSZ","MMM dd");
        String convertdate1 = Utils.changeDateFormat(post.getCreateDate(),"yyyy-MM-dd T HH':mm:ss.SSSZ","hh:mm a");
        tvDate.setText(convertdate +" at "+convertdate1);*/
        String tempDate = null, tempDate1 = "";
        if (post.getCreateDate().contains("T")) {
            tempDate = Utils.changeDateFormat(post.getCreateDate(), "yyyy-MM-dd'T'HH:mm:ss", "MMM dd");
            tempDate1 = Utils.changeDateFormat(post.getCreateDate(), "yyyy-MM-dd'T'HH:mm:ss", "hh:mm a");
            tvDate.setText(tempDate + " at " + tempDate1);
            /*tvDate.setText(DateUtils.getRelativeDateTimeString(this,
                    Utils.convertDateToMills(post.getCreateDate()),
                    DateUtils.SECOND_IN_MILLIS,
                    DateUtils.WEEK_IN_MILLIS,
                    0));
*/
        } else {
            tempDate = post.getCreateDate();
            tvDate.setText(tempDate);
        }


        setLikeCounts(post.getLikeCounts());
        if(post.getSharedOwner().length() > 0){
            tvUserName.setText(post.getSharedOwner());

            Glide.with(this)
                    .load(post.getSharedOwnerImage()).asBitmap()
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivUserProfilePic.setImageBitmap(resource);
                        }
                    });
        }else{
            Glide.with(this)
                    .load(post.getUserImage()).asBitmap()
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivUserProfilePic.setImageBitmap(resource);
                        }
                    });

        }

        /*Glide.with(this)
                .load(post.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(ivUserProfilePic);*/
        commentCount = String.valueOf(post.getCommentCounts());

        tvComment.setText("Comment " + commentCount);

    }

    @Override
    public void onBackPressed() {
        boolean updateAdapter = false;


        //  super.onBackPressed();
        if(Constants.COME_FROM_HOME == 1) {

            Intent intent = new Intent(ActivityFullPhotoView.this, ActivityHome.class);
            intent.putExtra("COMMENT_COUNT", Integer.parseInt(commentCount));
            intent.putExtra("LIKE_COUNT", Integer.parseInt(tvLikeCount.getText().toString()));
            intent.putExtra("SHARE_COUNT", Integer.parseInt(tvShareCount.getText().toString()));

            setResult(RESULT_OK, intent);
        }
        else if(Constants.COME_FROM_HOME == 2) {

            Intent intent = new Intent(ActivityFullPhotoView.this, ActivityUserProfile.class);
            intent.putExtra("COMMENT_COUNT", Integer.parseInt(commentCount));
            intent.putExtra("LIKE_COUNT", Integer.parseInt(tvLikeCount.getText().toString()));
            intent.putExtra("SHARE_COUNT", Integer.parseInt(tvShareCount.getText().toString()));

            setResult(RESULT_OK, intent);
        }

        finish();

        /*Toast.makeText(this,"Hello My Friends123",Toast.LENGTH_LONG).show();
        tvComment.setText("Comment" + commentCount);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Toast.makeText(this,"Hello My Friends",Toast.LENGTH_LONG).show();
        //  tvComment.setText("Comment" + commentCount);
    }

    // Here's what the app should do to add a view to the ViewPager.
    public void addView(View newPage) {
        adapter.addView(newPage);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(App.getInstance().getUser().getId().equalsIgnoreCase(PostuserId)){
            getMenuInflater().inflate(R.menu.activity_full_photo_vew_menu, menu);
            photoExtraOptions = new PopupMenu(this, findViewById(R.id.fl_anchor));
            photoExtraOptions.getMenuInflater().inflate(R.menu.popup_photo_options,
                    photoExtraOptions.getMenu());
        }else{

        }
        photoExtraOptions = new PopupMenu(this, findViewById(R.id.fl_anchor));
        photoExtraOptions.getMenuInflater().inflate(R.menu.popup_photo_options,
                photoExtraOptions.getMenu());
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_bookmark) {
         //   Toast.makeText(this, "Bookmark", Toast.LENGTH_SHORT).show();
            Intent tagFriend = new Intent(ActivityFullPhotoView.this, ActivityTagFriends.class);
            tagFriend.putExtra("COME_FROM_POST","0");
            tagFriend.putExtra(Constants.TAG_FRIEND_LIST, taggedFriendIds);
            tagFriend.putExtra(Constants.TAG_FRIEND_NAME_LIST, taggedFriendNames);

            tagFriend.putExtra("POST_ID",post.getStatusId());
            startActivity(tagFriend);
        } /*else if (item.getItemId() == R.id.action_photoOptions) {
            photoExtraOptions.show();
            photoExtraOptions.setOnMenuItemClickListener(
                    new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getItemId() == R.id.action_editPrivacy) {
                                promptUserForSelectingPostPrivacy();
                            } else if (item.getItemId() == R.id.action_savePhoto) {

                                Glide
                                        .with(getApplicationContext())
                                        .load(post.getImages().get(0).getImagestr())
                                        .asBitmap()
                                        .toBytes(Bitmap.CompressFormat.JPEG, 80)
                                        .into(new SimpleTarget<byte[]>() {
                                            @Override
                                            public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                                                new AsyncTask<Void, Void, Void>() {
                                                    @Override
                                                    protected Void doInBackground(Void... params) {
                                                        File sdcard = Environment.getExternalStorageDirectory();
                                                        File file = new File(sdcard + "/AgriZip/" + Calendar.getInstance().getTimeInMillis() + ".jpg");
                                                        File dir = file.getParentFile();
                                                        try {
                                                            if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                                                                throw new IOException("Cannot ensure parent directory for file " + file);
                                                            }
                                                            BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(file));
                                                            s.write(resource);
                                                            s.flush();
                                                            s.close();

                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                        return null;
                                                    }
                                                }.execute();
                                            }
                                        });
                                Toast.makeText(ActivityFullPhotoView.this, "Saved to Gallery", Toast.LENGTH_LONG).show();

                            } else if (item.getItemId() == R.id.action_sendViaChat) {

                            } else if (item.getItemId() == R.id.action_removeTag) {
                                removeTagFromPost();
                            }

                            return false;
                        }
                    }
            );


        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }*/
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tv_pv_userName)
    public void onClick(View view) {
        Intent i = new Intent(this, ActivityUserProfile.class);
        i.putExtra(Constants.USER_ID, post.getUserID());
        if (post.getUserID().equals(App.getInstance().getUser().getId())) {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
        } else {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
        }

        if (App.getInstance().profileNavigationList.size() > 0) {
            if (!App.getInstance().profileNavigationList.get(App.getInstance()
                    .profileNavigationList.size() - 1).equals(post.getUserID())) {
                this.startActivity(i);
            }
        } else {
            this.startActivity(i);
        }
    }

    @OnClick(R.id.iv_pv_userProfilePic)
    public void onClick1(View view) {
        Intent i = new Intent(this, ActivityUserProfile.class);
        i.putExtra(Constants.USER_ID, post.getUserID());
        if (post.getUserID().equals(App.getInstance().getUser().getId())) {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
        } else {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
        }

        if (App.getInstance().profileNavigationList.size() > 0) {
            if (!App.getInstance().profileNavigationList.get(App.getInstance()
                    .profileNavigationList.size() - 1).equals(post.getUserID())) {
                this.startActivity(i);
            }
        } else {
            this.startActivity(i);
        }
    }

    @OnClick(R.id.tv_pv_share)
    public void sharePost() {
        shareStatusPost();
    }

    @OnClick(R.id.iv_pv_share)
    public void sharePost1() {
        shareStatusPost();
    }

    @OnClick(R.id.btn_pv_makeProfilePicture)
    public void makeProfilePicture() {
        System.out.println("http Image in Constant : " + Constants.USERPROFILE_IMAGE);
        promptForChangingProfilePicture();
    }

    private void promptForChangingProfilePicture() {

        new AlertDialog.Builder(this)
                .setTitle("Change profile picture")
                .setMessage("Use this Image as Profile picture?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        makeCurrentPhotoAsProfilePicture();
                    }
                })
                .setNegativeButton("CANCEL", null)
                .show();

    }

    private void shareStatusPost() {

        final SharePost sharePost = new SharePost();

        sharePost.setStatusId(post.getStatusId());
        sharePost.setMessage(post.getMessage());
        sharePost.setPlace(post.getPlace());
        sharePost.setUserID(App.getInstance().getUser().getId());
        sharePost.setShareId(post.getStatusId());
        sharePost.setStatus(post.getStatus());
        sharePost.setFeelingName(post.getFeelingName());
        sharePost.setFeelingValue(post.getFeelingValue());
        sharePost.setPostYear(post.getPostYear());
        sharePost.setPostMonth(post.getPostMonth());
        sharePost.setPostDay(post.getPostDay());
        sharePost.setPostHour(post.getPostHour());
        sharePost.setPostMinute(post.getPostMinute());
        sharePost.setHelp(post.isHelp());
        sharePost.setNews(post.isNews());
        sharePost.setVisibleTo(post.getVisibleTo());
        sharePost.setCreateDate(post.getCreateDate());
        sharePost.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");
        sharePost.setIsDelete(false);


        final RetrofitCallbacks<ResponseBody> onSharePostCallback =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            post.setShareCounts(post.getShareCounts() + 1);
                            setShareCount();
                            Toast.makeText(ActivityFullPhotoView.this, "Post Shared Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().sharePost(sharePost, onSharePostCallback);

    }

    private void setShareCount() {
        if (post.getShareCounts() > 0) {
            tvShareCount.setText(post.getShareCounts() + "");
            //rlPostShareContainer.setVisibility(View.VISIBLE);
        } else {
            tvShareCount.setText("0");
            // rlPostShareContainer.setVisibility(View.GONE);
        }
    }

    private void removeTagFromPost() {
        final RetrofitCallbacks<ResponseBody> removeTagFromPost =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            Toast.makeText(ActivityFullPhotoView.this,
                                    "Removed From this Post", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().removeTagFromPost(post.getStatusId(), App.getInstance().getUser().getId(), removeTagFromPost);

    }

    private void makeCurrentPhotoAsProfilePicture() {

            Constants.USERPROFILE_IMAGE = postImagesList.get(vpFullPhotoView.getCurrentItem()).getImagestr();


        if(post.getSharedOwner().length() > 0) {

        }else{



            Glide.with(this)
                    .load( Constants.USERPROFILE_IMAGE).asBitmap()
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivUserProfilePic.setImageBitmap(resource);
                        }
                    });
        }

        final RetrofitCallbacks<ResponseBody> onMakeProfilePictureResponse =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            Toast.makeText(ActivityFullPhotoView.this,
                                    "Profile Picture changed successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().makeProfilePicture(App.getInstance().getUser().getId(),
                postImagesList
                        .get(vpFullPhotoView.getCurrentItem()).getImageId(), onMakeProfilePictureResponse);
    }


    @OnClick(R.id.tv_pv_comment)
    public void showCommentDialog() {
        commentsDialog = new CommentsDialog(this, post.getStatusId(),String.valueOf(post.getCommentCounts()));
        commentsDialog.mListenerCount = this;
        commentsDialog.show();
    }


    @OnClick(R.id.iv_pv_comment)
    public void showCommentDialog1() {
        commentsDialog = new CommentsDialog(this, post.getStatusId(),String.valueOf(post.getCommentCounts()));
        commentsDialog.mListenerCount = this;
        commentsDialog.show();
    }

    @OnClick(R.id.tv_pv_likeCount)
    public void showLikeList() {
        final LikeListDialog likeListDialog = new LikeListDialog(this, post.getStatusId());
        likeListDialog.show();
    }


    @OnClick(R.id.tv_pv_like)
    public void likeThisPost() {
        likethisPost();
    }

    @OnClick(R.id.iv_pv_like)
    public void likeThisPost1() {
        likethisPost();
    }

    public void likethisPost() {

        if (likeApiCall != null) {
            likeApiCall.cancel();
        }

        LikeUnlikePost like = new LikeUnlikePost();

        if (post.isIsLiked()) {
            setLikeCounts(post.getLikeCounts() - 1);
            like.setIsLike(false);
        } else {
            setLikeCounts(tempLike + 1);
            like.setIsLike(true);
        }

        like.setStatusID(post.getStatusId());

        /*if (post.getUserID().equals(App.getInstance().getUser().getId()))
        {
            like.setUserID(post.getUserProfileDetailsInfo().getUserId());
        } else
        {
            like.setUserID(post.getUserID());
        }*/

        like.setUserID(App.getInstance().getUser().getId());

        like.setCreateDate(Calendar.getInstance().getTimeInMillis() + "");
        like.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        Log.i(TAG, "SENDING: " + new Gson().toJson(like));

        likeApiCall = API.getInstance().likeOnPost(like, new RetrofitCallbacks<LikeUnlikeResponse>(this) {

            @Override
            public void onResponse(Call<LikeUnlikeResponse> call, Response<LikeUnlikeResponse> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    Log.i(TAG, "SUCCESSFULLY LIKED");
                    post.setIsLiked(response.body().getData().isIsLike());
                    if (post.isIsLiked()) {
                        post.setLikeCounts(post.getLikeCounts() + 1);
                    } else {
                        post.setLikeCounts(post.getLikeCounts() - 1);
                        tempLike = post.getLikeCounts();
                    }

                    setLikeCounts(post.getLikeCounts());
                } else {
                    post.setLikeCounts(post.getLikeCounts());
                    try {
                        Log.i(TAG, "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeUnlikeResponse> call, Throwable t) {
                super.onFailure(call, t);
                if (!call.isCanceled()) {
                    setLikeCounts(post.getLikeCounts());
                }
            }
        });
    }


    private void setLikeCounts(int count) {
        if (count > 0) {
            tvLikeCount.setText(count + "");
        } else {
            tvLikeCount.setText("0");
        }
    }


    @Override
    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
        Log.i(TAG, "ON MENU ITEM CLICK");

        switch (item.getItemId()) {

            case R.id.action_editPrivacy:
                promptUserForSelectingPostPrivacy();
                break;

            case R.id.action_savePhoto:

                saveImage(0);

                Toast.makeText(ActivityFullPhotoView.this, "Saved to Gallery", Toast.LENGTH_LONG).show();



                break;

            case R.id.action_sendViaChat:
                saveImage(1);




              //  ActivityHome feed = (ActivityHome) this;
               // FragmentManager fragmentManager = feed.getSupportFragmentManager();
                //feed.showFragment(fragmentChats, 3);


                break;

            case R.id.action_removeTag:
                removeTagFromPost();
                break;

            case R.id.action_turnOffNotification:
                TurnOnOffNotification();
                break;
        }
        return true;
    }

    public void updategallery(String filepath,int code){

        MediaScannerConnection.scanFile(this,
                new String[] { filepath }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        //now visible in gallery
                    }
                }
        );
        if(code == 1){
            Intent intent = new Intent(ActivityFullPhotoView.this, ActivityHome.class);
            intent.putExtra("COMMENT_COUNT", Integer.parseInt(commentCount));
            intent.putExtra("LIKE_COUNT", Integer.parseInt(tvLikeCount.getText().toString()));
            intent.putExtra("SHARE_COUNT", Integer.parseInt(tvShareCount.getText().toString()));
            intent.putExtra("chatScreen",true);
            intent.putExtra("AttechedImagePath",postImagesList
                    .get(vpFullPhotoView.getCurrentItem()).getImagePath());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onMenuModeChange(MenuBuilder menu) {

    }
    public  void saveImage(final int code){
        Glide
                .with(getApplicationContext())
                .load(postImagesList
                        .get(vpFullPhotoView.getCurrentItem()).getImagePath())
                .asBitmap()
                .toBytes(Bitmap.CompressFormat.JPEG, 80)
                .into(new SimpleTarget<byte[]>() {
                    @Override
                    public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {
                                File sdcard = Environment.getExternalStorageDirectory();
                                File file = new File(sdcard + "/AgriZip/" + Calendar.getInstance().getTimeInMillis() + ".jpg");
                                getAbsulutePath = file;
                                dir = file.getParentFile();
                                filePath = file.toString();
                                try {
                                    if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                                        throw new IOException("Cannot ensure parent directory for file " + file);
                                    }
                                    BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(file));
                                    s.write(resource);
                                    s.flush();
                                    s.close();
                                    updategallery(filePath,code);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        }.execute();
                    }
                });

    }

    @Override
    public void OnChangeCommentCount(int count, int position) {
        commentCount = String.valueOf(count);

        tvComment.setText("Comment " + count);
    }

    public void TurnOnOffNotification(){

        final RetrofitCallbacks<ResponseBody> TurnOnOffNotification =
                new RetrofitCallbacks<ResponseBody>(this) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            if(post.isNotification()){
                                Toast.makeText(ActivityFullPhotoView.this,
                                        "Notification Off for this post", Toast.LENGTH_SHORT).show();
                                post.setNotification(false);
                            }else{
                                post.setNotification(true);
                                Toast.makeText(ActivityFullPhotoView.this,
                                        "Notification On for this post", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                };

        API.getInstance().TurnOnOffNotification(post.getStatusId(),post.getUserID(),TurnOnOffNotification);


    }
}
