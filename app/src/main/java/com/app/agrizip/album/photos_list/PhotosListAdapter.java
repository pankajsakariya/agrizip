package com.app.agrizip.album.photos_list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import adapters.DemoAdapter2;
import apimodels.PostImageList;
import moaritem.MoarItem;

/**
 * Created by rutvik on 1/11/2017 at 7:14 PM.
 */

public class PhotosListAdapter extends ArrayAdapter<PostImageList.DataBean> implements DemoAdapter2
{

    private final LayoutInflater layoutInflater;

    private final Context context;



    public PhotosListAdapter(Context context, List<PostImageList.DataBean> items)
    {
        super(context, 0, items);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent)
    {
        View v;

        MoarItem item = getItem(position);
        boolean isRegular = getItemViewType(position) == 0;

        if (convertView == null)
        {
            v = layoutInflater.inflate(R.layout.single_album_photo_grid_item, parent, false);
        } else
        {
            v = convertView;
        }

        ImageView imageView = (ImageView) v.findViewById(R.id.iv_singleAlbumPhotoGridItem);

        /*TextView tv = (TextView) v.findViewById(R.id.textview_odd);
        tv.setText(position + "");*/

        if(item.getImagestr().length() > 0) {

            Picasso.with(context)
                    .load(item.getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)

                    .into(imageView);
        }

        return v;
    }

    @Override
    public int getViewTypeCount()
    {
        return 2;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position % 2 == 0 ? 1 : 0;
    }

    public void appendItems(List<PostImageList.DataBean> newItems)
    {
        addAll(newItems);
        notifyDataSetChanged();
    }

    public void setItems(List<PostImageList.DataBean> moreItems)
    {
        clear();
        appendItems(moreItems);
    }

}
