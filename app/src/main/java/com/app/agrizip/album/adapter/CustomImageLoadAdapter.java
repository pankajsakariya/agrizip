package com.app.agrizip.album.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by pankajsakariya on 16/06/17.
 */

public class CustomImageLoadAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<String> imagePath;

    public CustomImageLoadAdapter(Context context ,ArrayList<String> imagePath){
        this.mContext = context;
        this.imagePath = imagePath;
    }

    @Override
    public int getCount() {
        return imagePath.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {


        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {

        ImageView mImageView = new ImageView(mContext);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(mContext)
                .load(imagePath.get(i))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .into(mImageView);
        ((ViewPager) container).addView(mImageView, 0);



        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}
