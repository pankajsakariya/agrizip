package com.app.agrizip.album.album_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import apimodels.GetAlbums;
import viewholders.VHSingleAlbum;

/**
 * Created by rutvik on 1/5/2017 at 3:26 PM.
 */

public class AlbumAdapter extends RecyclerView.Adapter
{

    final Context context;

    final List<GetAlbums.DataBean> albumList;

    public AlbumAdapter(Context context)
    {
        this.context = context;
        albumList = new LinkedList<>();
    }

    public void addSingleAlbum(GetAlbums.DataBean singleAlbum)
    {
        if(singleAlbum.getPhotos().size() > 0){
            albumList.add(singleAlbum);
//            notifyItemInserted(albumList.size());
        }
        notifyDataSetChanged();

    }

    public void clear()
    {
        albumList.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return VHSingleAlbum.create(context, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        VHSingleAlbum.bind((VHSingleAlbum) holder, albumList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return albumList.size();
    }
}
