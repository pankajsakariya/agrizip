package com.app.agrizip.album.album_create;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CreatePhotoAlbum;
import apimodels.GetAlbums;
import butterknife.BindView;
import butterknife.ButterKnife;
import dialogs.CustomDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/3/2017 at 5:12 PM.
 */

public class RenameAlbum extends CustomDialog implements Validator.ValidationListener
{

    private final CreatePhotoAlbum renamedAlbum;
    @NotEmpty
    @BindView(R.id.et_newAlbumName)
    EditText etNewAlbumName;
    Validator validator;
    private GetAlbums.DataBean singleAlbum;
    private OnAlbumRenameListener listener;

    public RenameAlbum(Context context, GetAlbums.DataBean singleAlbum, OnAlbumRenameListener listener)
    {
        super(context, null, null, null, null);

        this.singleAlbum = singleAlbum;

        this.listener = listener;

        renamedAlbum = new CreatePhotoAlbum();
        renamedAlbum.setAlbumVisibleTo(singleAlbum.getAlbumVisibleTo());
        renamedAlbum.setAlbumID(singleAlbum.getAlbumID());
        renamedAlbum.setAlbumDescription(singleAlbum.getAlbumDescription());
        renamedAlbum.setUserId(App.getInstance().getUser().getId());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_rename_album);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);


        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        etNewAlbumName.setText(singleAlbum.getAlbumName());

        tvDialogEditPrivacy.setOnClickListener(this);
        tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

        btnDialogSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                validator.validate();
            }
        });

        btnDialogCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

    }

    @Override
    public void onChangePrivacy(String visibilityId)
    {
        renamedAlbum.setAlbumVisibleTo(visibilityId);
    }


    private void updateAlbumName(final String newName)
    {
        renamedAlbum.setAlbumName(newName);

        API.getInstance().renamePhotoAlbum(renamedAlbum, new RetrofitCallbacks<ResponseBody>(context)
        {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                super.onResponse(call, response);
                if (response.isSuccessful())
                {
                    listener.onAlbumRenamed(newName);
                    Toast.makeText(context, "Album name changed successfully", Toast.LENGTH_SHORT).show();
                    dismiss();
                } else
                {
                    Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public void onValidationSucceeded()
    {
        if (etNewAlbumName.getText().toString().trim().equals(singleAlbum.getAlbumName()))
        {
            dismiss();
            return;
        }
        updateAlbumName(etNewAlbumName.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors)
    {
        for (ValidationError error : errors)
        {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText)
            {
                ((EditText) view).setError(message);
            } else
            {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }


    public interface OnAlbumRenameListener
    {
        void onAlbumRenamed(String name);
    }

}
