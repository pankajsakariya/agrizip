package com.app.agrizip.album.album_list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.ActivityAlbum;
import com.app.agrizip.album.album_create.ActivityCreateAlbum;
import com.app.agrizip.app.App;

import api.API;
import api.RetrofitCallbacks;
import apimodels.GetAlbums;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import interfaces.UpdateAlubm;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/11/2017 at 4:09 PM.
 */

public class FragmentAlbums extends Fragment implements SwipeRefreshLayout.OnRefreshListener,ActivityCreateAlbum.AlbumcrateListner {

    @BindView(R.id.srl_refreshAlbums)
    SwipeRefreshLayout srlRefreshAlbums;

    @BindView(R.id.rv_albums)
    RecyclerView rvAlbums;

    @BindView(R.id.rl_createAlbum)
    RelativeLayout rlCreateAlbum;

    @BindView(R.id.fl_loading)
    FrameLayout flLoading;

    AlbumAdapter adapter;
    int pastVisibleItems, visibleItemCount, totalItemCount;


    Activity activity;

  //  boolean busyLoading = false;

    String userId;
    LinearLayoutManager llm;
    int pageNo = 1;
    public FragmentAlbums() {

    }

    public static FragmentAlbums newInstance(String userId) {
        FragmentAlbums fragmentAlbums = new FragmentAlbums();
        fragmentAlbums.userId = userId;
        return fragmentAlbums;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums, container, false);

        ButterKnife.bind(this, view);
         try {
             if (!userId.equals(App.getInstance().getUser().getId())) {
                 rlCreateAlbum.setVisibility(View.GONE);
             }
         }catch (Exception ex)
         {
             ex.printStackTrace();
         }
        llm = new LinearLayoutManager(getActivity());

        rvAlbums.setLayoutManager(llm);
        rvAlbums.setHasFixedSize(true);
        activity = getActivity();
        adapter = new AlbumAdapter(getActivity());

        rvAlbums.setAdapter(adapter);

        rlCreateAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ActivityCreateAlbum.class);

                getActivity().startActivityForResult(intent,123);
                //startActivity(new Intent(getActivity(),
                  //      ActivityCreateAlbum.class));
            }
        });

        getAlbums();
        srlRefreshAlbums.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshAlbums.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == getActivity().RESULT_OK){
            System.out.println("Hello");
         ActivityAlbum.new_album_flag= data.getIntExtra("IS_CREATE_NEW",0);


        }
       // getToast();



    }





    public void getAlbums() {
//        if (!busyLoading) {
//            busyLoading = true;


            final RetrofitCallbacks<GetAlbums> onGetAlbumCallback =
                    new RetrofitCallbacks<GetAlbums>(getActivity()) {

                        @Override
                        public void onResponse(Call<GetAlbums> call, Response<GetAlbums> response) {
                            super.onResponse(call, response);
                            //busyLoading = false;
                            flLoading.setVisibility(View.GONE);
                            if (srlRefreshAlbums.isRefreshing()) {
                                srlRefreshAlbums.setRefreshing(false);
                            }
                            if (response.isSuccessful()) {
                                adapter.clear();
                                for (GetAlbums.DataBean singleAlbum : response.body().getData()) {
                                    adapter.addSingleAlbum(singleAlbum);
                                }

                            }
                        }


                        @Override
                        public void onFailure(Call<GetAlbums> call, Throwable t) {
                            super.onFailure(call, t);
                            flLoading.setVisibility(View.GONE);
                            //busyLoading = false;
                            if (srlRefreshAlbums.isRefreshing()) {
                                srlRefreshAlbums.setRefreshing(false);
                            }
                        }
                    };

            API.getInstance().getPhotoAlbumList(userId, onGetAlbumCallback);
       // }


    }

    class OnScrollRecyclerView extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisibleItems = llm.findFirstVisibleItemPosition();
              //  if (!busyLoadingData) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        pageNo++;
                        getAlbums();
                        //getFriendList();
                  //  }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(Constants.ALBUM_EDIT){
            Constants.ALBUM_EDIT = false;
            albumupdate();
        }

        if(ActivityAlbum.new_album_flag == 1 ){
            albumupdate();
             }


    }

    private void albumupdate(){
        CountDownTimer timer;
        timer = new CountDownTimer(3000, 20) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try{

                    System.out.println("!! Hello");
                    flLoading.setVisibility(View.VISIBLE);
                    getAlbums();
                    //getNewActiveRide(context);
                }catch(Exception e){
                    //  Log.e("Error", "Error: " + e.toString());
                }
            }
        }.start();

    }


    @Override
    public void onRefresh() {
        getAlbums();
    }

    @Override
    public void OncrateAlbum() {
        getAlbums();
    }
}
