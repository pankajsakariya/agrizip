package com.app.agrizip.album.photos_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.felipecsl.asymmetricgridview.library.Utils;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import api.API;
import api.RetrofitCallbacks;
import apimodels.PostImageList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/11/2017 at 4:09 PM.
 */

public class FragmentPhotos extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

  //  @BindView(R.id.agv_photos)
  //  AsymmetricGridView agvPhotos;

    @BindView(R.id.srl_refreshPhotos)
    SwipeRefreshLayout srlRefreshPhotos;

    @BindView(R.id.fl_loadingPhotos)
    FrameLayout flLoadingPhotos;

    @BindView(R.id.tv_apiResponse)
    TextView tvApiResponse;

    @BindView(R.id.rv_photos)
    RecyclerView rv_photos;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    String userId;
    ArrayList<PostImageList.DataBean> photosBeanArrayList = new ArrayList<>();
    ArrayList<PostImageList.DataBean> photosBeanArrayListTemp = new ArrayList<>();
    private int pageNo = 1;
    private boolean busyLoading = false;
    PhotosListAdapter adapter;
    PhotoAdapter adapter1;
    //LinearLayoutManager llm;
    StaggeredGridLayoutManager llm;

    public FragmentPhotos() {

    }

    public static FragmentPhotos newInstance(String userId) {
        FragmentPhotos fragmentPhotos = new FragmentPhotos();
        fragmentPhotos.userId = userId;
        return fragmentPhotos;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photos, container, false);

        ButterKnife.bind(this, view);

        llm = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);

        rv_photos.setHasFixedSize(true);

        rv_photos.setLayoutManager(llm);

        //rv_photos.setItemAnimator(new DefaultItemAnimator());


        adapter1 = new PhotoAdapter(getActivity());
        rv_photos.setAdapter(adapter1);
        rv_photos.addOnScrollListener(new OnScrollRecyclerView());
        srlRefreshPhotos.setColorSchemeResources(R.color.new_color, R.color.new_color, R.color.new_color);

        srlRefreshPhotos.setOnRefreshListener(this);

     //   pagging();
        getPhotoList();



        return view;
    }

    private void getPhotoList() {

        busyLoading = true;
     if(pageNo == 1){
         adapter1.clear();
     }

        final RetrofitCallbacks<PostImageList> onGetPhotosCallback =
                new RetrofitCallbacks<PostImageList>(getActivity()) {

                    @Override
                    public void onResponse(Call<PostImageList> call, Response<PostImageList> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            flLoadingPhotos.setVisibility(View.GONE);
                            if (response.body().getData().size() > 0) {
                                tvApiResponse.setVisibility(View.GONE);
                                flLoadingPhotos.setVisibility(View.GONE);
                                    photosBeanArrayList.clear();
                                    for (PostImageList.DataBean photo : response.body().getData()) {
                                      //  photosBeanArrayList.add(photo);
                                        adapter1.addsinglePhoto(photo);
                                    }
                                   //createAndSetAdapter();
                            } else {
                                if (pageNo == 1) {
                                    photosBeanArrayList.clear();
                                    tvApiResponse.setVisibility(View.VISIBLE);
                                    tvApiResponse.setText(response.body().getMessage());
                                }

                            }

                        } else {
                            Toast.makeText(getActivity(), "something went wrong try again later", Toast.LENGTH_SHORT).show();
                        }

                        busyLoading = false;
                        if (srlRefreshPhotos.isRefreshing()) {
                            srlRefreshPhotos.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<PostImageList> call, Throwable t) {
                        super.onFailure(call, t);
                        busyLoading = false;
                        if (srlRefreshPhotos.isRefreshing()) {
                            srlRefreshPhotos.setRefreshing(false);
                        }
                    }
                };

        API.getInstance().getAllPhotosList(userId, pageNo, onGetPhotosCallback);

    }
/*
    public void pagging() {
        agvPhotos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                return false;
            }
        });
        agvPhotos.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount > 0) {

                    if (!busyLoading) {
                        if ((firstVisibleItem + visibleItemCount) >= totalItemCount) {
                            pageNo++;
                            getPhotoList();
                          //  agvPhotos.setf
                        }
                    }
                }
            }
        });
    }*/

   /* class OnScrollRecyclerView implements AsymmetricGridView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleCount, int totalCount) {


            if (totalCount > 0) {

                if (!busyLoading) {
                    if ((firstVisibleItem + visibleCount) >= totalItemCount) {
                        pageNo++;
                        getPhotoList();
                    }
                }


            }
        }


        //super.onScrolled(recyclerView, dx, dy);
    }*/

    private void createAndSetAdapter1() {
        photosBeanArrayList = (ArrayList<PostImageList.DataBean>) extras.Utils.moarItems(photosBeanArrayList);
        adapter = new PhotosListAdapter(getActivity(), photosBeanArrayList);
       // rv_photos.setAdapter(adapter);
    }
    /*
    private void createAndSetAdapter() {
        if(pageNo==1){

            photosBeanArrayList = (ArrayList<PostImageList.DataBean>) extras.Utils.moarItems(photosBeanArrayList);

            try {
                if (agvPhotos != null) {
                    // Choose your own preferred column width
                    agvPhotos.setRequestedColumnWidth(Utils.dpToPx(getActivity(), 120));

                    // initialize your items array


                    adapter = new PhotosListAdapter(getActivity(), photosBeanArrayList);




                    AsymmetricGridViewAdapter asymmetricAdapter =
                            new AsymmetricGridViewAdapter<>(getActivity(), agvPhotos, adapter);



                    // agvPhotos.setLayoutManager(llm);
                    // agvPhotos.smoothScrollToPosition(4);


                    agvPhotos.setAdapter(asymmetricAdapter);

                    // agvPhotos.setAllowReordering(true);
                    if(pageNo ==1){
                        agvPhotos.setStackFromBottom(false);
                    }else{
                        agvPhotos.smoothScrollToPositionFromTop(5,0);

                    }


                    // agvPhotos.scrollToPosition(mSkipTo)
                    //  agvPhotos.scrollTo();

                    // agvPhotos.smoothScrollToPosition(4);
                    agvPhotos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            Intent i = new Intent(getActivity(), ActivityFullPhotoView.class);
                            i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, photosBeanArrayList);
                            i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, position);
                            i.putExtra("ScrollON", true);
                            i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);
                            Constants.COME_FROM_HOME = 0;
                            startActivity(i);
                        }
                    });
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else
        {
            Toast.makeText(getContext(),"Hi in else",Toast.LENGTH_SHORT).show();
            adapter.appendItems(photosBeanArrayListTemp);


        }


    }
    */

    @Override
    public void onRefresh() {
        if (!busyLoading) {
            getPhotoList();
        }
    }

    class OnScrollRecyclerView extends RecyclerView.OnScrollListener
    {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            if (dy > 0)
            {
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                // pastVisibleItems = llm.findFirstVisibleItemPosition();

                int[] firstVisibleItems = null;
                firstVisibleItems = llm.findFirstVisibleItemPositions(firstVisibleItems);
                if(firstVisibleItems != null && firstVisibleItems.length > 0) {
                    pastVisibleItems = firstVisibleItems[0];
                }

                if (!busyLoading)
                {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount)
                    {
                        pageNo++;
                        //adapter.addProgressBar();
                        getPhotoList();
                    }
                }
            }
        }
    }
}
