package com.app.agrizip.album.album_create;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.ActivityAlbum;
import com.app.agrizip.album.adapter.SelectedPhotoAdapter;
import com.app.agrizip.app.App;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CreatePhotoAlbum;
import apimodels.OnCreatedPhotoAlbum;
import apimodels.UserPrivacyDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Response;
import services.AlbumImageUploadingService;

public class ActivityCreateAlbum extends AppCompatActivity implements DialogInterface.OnClickListener {

    public static final int REQUEST_CODE_PICKER = 1342;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbarTitle2)
    TextView tvToolbarTitle;

    @BindView(R.id.et_albumTitle)
    EditText etAlbumTitle;
    @BindView(R.id.et_albumDescription)
    EditText etAlbumDescription;
    @BindView(R.id.rv_albumSelectedImages)
    RecyclerView rvAlbumSelectedImages;
    @BindView(R.id.tv_dialogEditPrivacy)
    TextView tvDialogEditPrivacy;
    List<UserPrivacyDetails.DataBean> privacyList;
    AlertDialog promptPrivacyDialog;
    ArrayAdapter<UserPrivacyDetails.DataBean> privacyAdapter;
    String visibilityId;
    View titleView;
    ImagePicker imagePicker;

    public static AlbumcrateListner  mAlbumlistner;


    ArrayList<Image> selectedImages = new ArrayList<>();
    final RetrofitCallbacks<OnCreatedPhotoAlbum> onCreateAlbumCallback =
            new RetrofitCallbacks<OnCreatedPhotoAlbum>(this) {
                @Override
                public void onResponse(Call<OnCreatedPhotoAlbum> call, Response<OnCreatedPhotoAlbum> response) {
                    super.onResponse(call, response);
                    if (response.isSuccessful()) {
                        Toast.makeText(ActivityCreateAlbum.this, "Album created successfully", Toast.LENGTH_SHORT).show();


                        if (selectedImages.size() > 0) {
                            Intent uploadAlbumPhotoService =
                                    new Intent(ActivityCreateAlbum.this, AlbumImageUploadingService.class);

                            final ArrayList<String> selectedImagePaths = new ArrayList<>();

                            for (Image image : selectedImages) {
                                selectedImagePaths.add(image.getPath());
                            }

                            uploadAlbumPhotoService
                                    .putStringArrayListExtra(Constants.BASE_64_IMAGE_STRING_LIST,
                                            selectedImagePaths);

                            uploadAlbumPhotoService.putExtra(Constants.ALBUM_ID,
                                    response.body().getData());

                            uploadAlbumPhotoService.putExtra(Constants.ALBUM_NAME,
                                    etAlbumTitle.getText().toString());

                            startService(uploadAlbumPhotoService);
                        }

                        Intent intent = new Intent(ActivityCreateAlbum.this,ActivityAlbum.class);
                        intent.putExtra("IS_CREATE_NEW",1);
                        setResult(RESULT_OK,intent);
                        finish();

                       // ActivityCreateAlbum.this.finish();
                    } else {
                        Toast.makeText(ActivityCreateAlbum.this, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                    }
                }
            };
    SelectedPhotoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_album);

        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                if (tvToolbarTitle != null) {
                    tvToolbarTitle.setText("Create Album");
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);


            }
        }



        rvAlbumSelectedImages.setLayoutManager(new GridLayoutManager(this,
                Utils.calculateNoOfColumns(this)));

        rvAlbumSelectedImages.setHasFixedSize(true);

        imagePicker = ImagePicker.create(this);

        imagePicker.folderMode(true) // folder mode (false by default)
                .imageTitle("Tap to select") // image selection title
                .multi() // multi mode (default mode)
                .limit(20) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .origin(selectedImages); // original selected images, used in multi mode

        adapter = new SelectedPhotoAdapter(this, imagePicker);

        rvAlbumSelectedImages.setAdapter(adapter);

        adapter.addAddImageButton();

        tvDialogEditPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view!=null){
                    LayoutInflater inflater = getLayoutInflater();
                    titleView = inflater.inflate(R.layout.custom_dialog_photo,null);
                    TextView tv_title = (TextView) titleView.findViewById(R.id.tv_title);
                    tv_title.setText("Select Post Privacy");
                promptPrivacyDialog = new AlertDialog.Builder(ActivityCreateAlbum.this)
                        .setTitle("")
                        .setCustomTitle(titleView)
                        .setSingleChoiceItems(privacyAdapter, 0, ActivityCreateAlbum.this)
                        .show();
            }}
        });

        selectedImages = new ArrayList<>();


        preparePrivacyAdapter();

    }

    public void preparePrivacyAdapter() {
        if (App.getInstance().getUser().getUserPrivacyDetails() != null) {
            privacyList = App.getInstance().getUser().getUserPrivacyDetails().getData();
        }

        if (privacyList != null) {
            if (privacyList.size() > 0) {

                //tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());
                privacyAdapter
                        = new ArrayAdapter<UserPrivacyDetails.DataBean>(this,
                        android.R.layout.simple_list_item_1, privacyList) {

                    @NonNull
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {
                            convertView = LayoutInflater.from(ActivityCreateAlbum.this).inflate(
                                    android.R.layout.simple_list_item_1, parent, false);
                        }
                        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

                        tv.setText(privacyList.get(position).getUserTypeName());

                        return convertView;

                    }
                };

            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_createAlbum:
                if (!etAlbumTitle.getText().toString().isEmpty()) {
                    saveAlbum(etAlbumTitle.getText().toString(), etAlbumDescription.getText().toString());
                } else {
                    Toast.makeText(this, "Please provide album title and add photos to create album", Toast.LENGTH_SHORT).show();
                    return true;
                }
                break;

            case android.R.id.home:
                promptUserForExit();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void promptUserForExit() {
        final String albumTitle = etAlbumTitle.getText().toString();
        final String albumDescription = etAlbumDescription.getText().toString();

        if (!albumTitle.isEmpty()) {
            new AlertDialog.Builder(this)
                    .setTitle("Alert")
                    .setMessage("Album not saved")
                    .setPositiveButton("Save Album", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            saveAlbum(albumTitle, albumDescription);
                        }
                    })
                    .setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCreateAlbum.this.finish();
                        }
                    })
                    .show();
        } else {
            Intent intent = new Intent(ActivityCreateAlbum.this,ActivityAlbum.class);
            intent.putExtra("IS_CREATE_NEW",0);
            setResult(RESULT_OK,intent);
            finish();
        }

    }

    private void saveAlbum(String albumName, String description) {
        if (selectedImages.size() > 0) {
            final CreatePhotoAlbum createPhotoAlbum = new CreatePhotoAlbum();
            createPhotoAlbum.setAlbumDescription(description);
            createPhotoAlbum.setAlbumName(albumName);
            createPhotoAlbum.setUserId(App.getInstance().getUser().getId());
            createPhotoAlbum.setAlbumVisibleTo(visibilityId);

            API.getInstance().createPhotoAlbum(createPhotoAlbum, onCreateAlbumCallback);
        } else {
            Toast.makeText(this, "Please add images to create album", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        tvDialogEditPrivacy.setText(privacyAdapter.getItem(i).getUserTypeName());
        visibilityId = privacyAdapter.getItem(i).getId();
        if (promptPrivacyDialog.isShowing()) {
            promptPrivacyDialog.dismiss();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            this.selectedImages.clear();
            adapter.clear();
            this.selectedImages.addAll(images);

            for (Image image : selectedImages) {
                adapter.addSelectedImage(image.getPath());
            }

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public interface AlbumcrateListner{
        void OncrateAlbum();
    }
}
