package com.app.agrizip.start.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.agrizip.R;

import java.util.ArrayList;
import java.util.List;

import apimodels.DistrictList;

/**
 * Created by rutvik on 11/21/2016 at 4:25 PM.
 */

public class DistrictListAdapter extends BaseAdapter
{

   // final DistrictList districtList;
    final Context context;
    List<DistrictList.DataBean> district=new ArrayList<>();
    public DistrictListAdapter(Context context,List<DistrictList.DataBean> district)
    {
       // this.districtList = districtList;
        this.context = context;
        this.district=district;
    }

    @Override
    public int getCount()
    {
        return district.size();
    }

    @Override
    public Object getItem(int i)
    {
        return district.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return district.get(i).hashCode();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup)
    {

        DistrictViewHolder districtViewHolder;

        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.simple_list_item, viewGroup, false);
            districtViewHolder = new DistrictViewHolder(convertView);
            convertView.setTag(districtViewHolder);
        } else
        {
            districtViewHolder = (DistrictViewHolder) convertView.getTag();
        }

        districtViewHolder.tvItem.setText(district.get(i).getDistName());

        return convertView;
    }

    private class DistrictViewHolder
    {
        TextView tvItem;

        public DistrictViewHolder(View item)
        {
            tvItem = (TextView) item.findViewById(R.id.tv_item);
        }
    }


}
