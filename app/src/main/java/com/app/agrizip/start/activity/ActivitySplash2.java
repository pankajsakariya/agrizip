package com.app.agrizip.start.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.app.agrizip.R;
import com.app.agrizip.home.activity.ActivityHome;

import extras.Constants;

public class ActivitySplash2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                isUserAuthorized();
            }
        }, 3000);

    }

    private void isUserAuthorized() {

        final String userId = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(Constants.USER_ID, "");

        if (userId.isEmpty()) {
            final Intent i = new Intent(this, ActivitySplash.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else {
            final Intent i = new Intent(this, ActivityHome.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

}
