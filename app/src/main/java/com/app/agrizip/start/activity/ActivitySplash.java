package com.app.agrizip.start.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivitySplash extends AppCompatActivity implements PermissionListener
{

    private static final String TAG = App.APP_TAG + ActivitySplash.class.getSimpleName();
    TextView tv_terms_condition;
    private boolean isPermissionsGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        tv_terms_condition=(TextView)findViewById(R.id.tv_terms_condition);
        new TedPermission(this)
                .setPermissionListener(this)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.CAMERA)
                .check();
            tv_terms_condition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(),"Terms And Condition ",Toast.LENGTH_SHORT).show();
                }
            });
    }

    @OnClick(R.id.btn_agreeAndContinue)
    void startSignUpActivity()
    {
        if (isPermissionsGranted) {

            boolean haveConnectedWifi = false;
            boolean haveConnectedMobile = false;

            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }

            if(haveConnectedMobile || haveConnectedWifi) {
                Intent i = new Intent(ActivitySplash.this, ActivitySignUp.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Please Check Internet Connection",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onPermissionGranted()
    {
        isPermissionsGranted = true;
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions)
    {
        Toast.makeText(this, "Please grant permissions to use this app", Toast.LENGTH_SHORT).show();
    }
}
