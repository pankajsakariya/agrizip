package com.app.agrizip.start.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.start.adapter.CountryListAdapter;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.CountryList;
import apimodels.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Response;

public class ActivitySignUp extends AppCompatActivity implements Validator.ValidationListener {

    private TextView tv_countryCode;
    private static final String TAG = App.APP_TAG + ActivitySignUp.class.getSimpleName();
    private Context context;

    @NotEmpty(message = "Mobile No cannot be empty")
    @Length(min = 10, message = "Mobile No should be of 10 digits", trim = true)
    @BindView(R.id.et_mobileNo)
    EditText etMobileNumber;
    @BindView(R.id.sp_countryName)
    AppCompatSpinner spCountry;

    @BindView(R.id.lly_spinner)
    LinearLayout lly_spinner;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<String> clist=new ArrayList<>();
    String authToken;
    String CountryCode;
    Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            }
        }
        tv_countryCode = (TextView) findViewById(R.id.tv_countryCode);
        getCountries();
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                Log.e("arg22", "" + arg2);
                if (arg2 == 0) {

                    tv_countryCode.setText("+91");
                } else {
                    tv_countryCode.setText("+1");
                }
                if(clist.size()==0)
                {
                    CountryCode="8F68693B-E1CA-4217-BAB3-1522CB6792DA";
                }
                else {
                    try {
                        CountryCode = clist.get(arg2);
                        Log.d("countryIDDD==", CountryCode);
                    }catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                }

                //
                // String code = countryList.getData().get(arg2).getCountryCode();
                      /*  if (spCountry.getSelectedItem().equals("India")) {
                            tv_countryCode.setText("+91");
                        } else {
                            tv_countryCode.setText("+1");
                        }*/
//                        String code2 = countryList.getData().get(arg2).getCountryCode();
                //        Log.e("code=====", "" + code2);
                //  Toast.makeText(getBaseContext(), countryList.getData().get(arg2).getCountryCode(), Toast.LENGTH_LONG).show();
                //Log.e("Code===", "" + );
                //String cntrycode = countryList.getData().get(arg2).getCountryName();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        validator = new Validator(this);

        validator.setValidationListener(this);

    }

    private void getCountries() {


        final CountryList countryList = new CountryList();
        final CountryList.DataBean countryListDataBean = new CountryList.DataBean();
        countryListDataBean.setCountryCode("+91");
        countryListDataBean.setCountryName("India");
        countryListDataBean.setCountryId("8F68693B-E1CA-4217-BAB3-1522CB6792DA");
//        countryListDataBean.setCountryCode("+1");
//        countryListDataBean.setCountryName("USA");
//        countryListDataBean.setCountryId("353fd5a5-dcd2-455b-a350-0dfe37edb2a2");

        final List<CountryList.DataBean> list = new ArrayList<CountryList.DataBean>();
        list.add(countryListDataBean);
        countryList.setData(list);

        Utils.showProgressDialog(ActivitySignUp.this, "Please Wait...", "Connecting to server...", true, false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                ActivitySignUp.this.finish();
            }
        });

        API.getInstance().getCountryList(new RetrofitCallbacks<CountryList>(this) {

            @Override
            public void onResponse(Call<CountryList> call, final Response<CountryList> response) {
                Utils.hideProgressDialog();
                super.onResponse(call, response);
                Log.e("contrylist", "" + call);
                if (response.isSuccessful()) {

                    for(CountryList.DataBean Country: response.body().getData())
                    {
                        clist.add(Country.getCountryId());
                    }
                    //spCountry.setAdapter(new CountryListAdapter(ActivitySignUp.this, response.body()));
                    spCountry.setAdapter(new CountryListAdapter(ActivitySignUp.this, response.body()));
                } else {
                    spCountry.setAdapter(new CountryListAdapter(ActivitySignUp.this, countryList));
                }
                tv_countryCode.setText(countryListDataBean.getCountryCode());

            }

            @Override
            public void onFailure(Call<CountryList> call, Throwable t) {
                super.onFailure(call, t);

                spCountry.setAdapter(new CountryListAdapter(ActivitySignUp.this, countryList));

                Utils.hideProgressDialog();
            }
        });

    }


    @OnClick(R.id.btn_signUp)
    void trySignUp() {
        validator.validate();
    }


    @Override
    public void onValidationSucceeded() {
        if (etMobileNumber.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.please_enter_mobile_no, Toast.LENGTH_SHORT).show();
            return;
        }

        if (etMobileNumber.getText().toString().length() < 10) {
            Toast.makeText(this, "Mobile number must be of 10 digits", Toast.LENGTH_SHORT).show();
            return;
        }

        if (etMobileNumber.getText().toString().trim().length() == 10) {

            Utils.hideKeyboard(ActivitySignUp.this, ActivitySignUp.this.getCurrentFocus());

            Utils.showProgressDialog(ActivitySignUp.this, "Please Wait...", "Connecting to server...", true, false, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    ActivitySignUp.this.finish();
                }
            });

            API.getInstance().newMobileRegistration(this,etMobileNumber.getText().toString().trim(),CountryCode, new RetrofitCallbacks<UserInfo>(ActivitySignUp.this) {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                    super.onResponse(call, response);

                    Log.i(TAG, "Headers Authtoken: " + response.headers().get(Constants.AUTH_TOKEN));

                    authToken = response.headers().get(Constants.AUTH_TOKEN);

                    Utils.hideProgressDialog();

                    //if(response.body().getData().isAlreadyUser()){
                    if(response.body().getData().getIsAlreadyUser() == 2){

                    /*  if(response.body().getData().getOTP().length() > 0){
                          Intent i = new Intent(ActivitySignUp.this, ActivityVerifyOtp.class);
                          i.putExtra(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim());
                          i.putExtra("CountryCode", CountryCode);
                          i.putExtra("isAlreadyUser",response.body().getData().getIsAlreadyUser());
                          startActivity(i);
                      }else{*/

                          ((App) getApplication()).setUser(response.body().getData());
                          Intent i = new Intent(ActivitySignUp.this, ActivityHome.class);
                          i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                          startActivity(i);
                          PreferenceManager.getDefaultSharedPreferences(ActivitySignUp.this)
                                  .edit()
                                  .putString(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim())
                                  .apply();

                      //}
                        /*  if(!response.body().getData().getOTP().equals(null)||  !response.body().getData().getOTP().equals("")){
                            Intent i = new Intent(ActivitySignUp.this, ActivityVerifyOtp.class);
                            i.putExtra(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim());
                            i.putExtra("CountryCode", CountryCode);
                            i.putExtra("isAlreadyUser",response.body().getData().isAlreadyUser());
                            startActivity(i);
                        }else if(response.body().getData().getOTP().equals(null)||  response.body().getData().getOTP().equals("")){
                            ((App) getApplication()).setUser(response.body().getData());
                            Intent i = new Intent(ActivitySignUp.this, ActivityHome.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            PreferenceManager.getDefaultSharedPreferences(ActivitySignUp.this)
                                    .edit()
                                    .putString(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim())
                                    .apply();
                        }*/
                    }
                    else  if(response.body().getData().getIsAlreadyUser() == 1){
                        PreferenceManager.getDefaultSharedPreferences(ActivitySignUp.this)
                                .edit()
                                .putString(Constants.INCOMPLETE_PROFILE_MOBILE_NO, etMobileNumber.getText().toString().trim())
                                .apply();

                        Intent i = new Intent(ActivitySignUp.this, ActivitySetupProfile.class);
                        i.putExtra(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim());
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }else if(response.body().getData().getIsAlreadyUser() == 0)
                    {
                        PreferenceManager.getDefaultSharedPreferences(ActivitySignUp.this)
                                .edit()
                                .putString(Constants.INCOMPLETE_PROFILE_MOBILE_NO, etMobileNumber.getText().toString().trim())
                                .apply();

                        Intent i = new Intent(ActivitySignUp.this, ActivityVerifyOtp.class);
                        i.putExtra(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim());
                        i.putExtra("CountryCode", CountryCode);
                        startActivity(i);
                    }

                 /*   if (response.body() == null) {
                        Intent i = new Intent(ActivitySignUp.this, ActivityVerifyOtp.class);
                        i.putExtra(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim());
                        i.putExtra("CountryCode", CountryCode);
                        startActivity(i);
                    }
                    else {
                        ((App) getApplication()).setUser(response.body().getData());
                        Intent i = new Intent(ActivitySignUp.this, ActivityHome.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        PreferenceManager.getDefaultSharedPreferences(ActivitySignUp.this)
                                .edit()
                                .putString(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim())
                                .apply();
                    }*/

                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    Log.i(TAG, t.toString());
                    Log.i(TAG, call.toString());
                    //  Toast.makeText(ActivitySignUp.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    Utils.hideProgressDialog();
                }
            });

            /*API.getInstance().userLoginVerify(etMobileNumber.getText().toString().trim(), new RetrofitCallbacks<UserInfo>(ActivitySignUp.this) {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                    super.onResponse(call, response);

                    Log.i(TAG, "Headers Authtoken: " + response.headers().get(Constants.AUTH_TOKEN));

                    authToken = response.headers().get(Constants.AUTH_TOKEN);

                    Utils.hideProgressDialog();

                    if (response.body() == null) {
                        Intent i = new Intent(ActivitySignUp.this, ActivityVerifyOtp.class);
                        i.putExtra(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim());
                        i.putExtra("CountryCode", CountryCode);
                        startActivity(i);
                    }
                    else {
                        ((App) getApplication()).setUser(response.body().getData());
                        Intent i = new Intent(ActivitySignUp.this, ActivityHome.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        PreferenceManager.getDefaultSharedPreferences(ActivitySignUp.this)
                                .edit()
                                .putString(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim())
                                .apply();
                    }

                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    Log.i(TAG, t.toString());
                    Log.i(TAG, call.toString());
                  //  Toast.makeText(ActivitySignUp.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    Utils.hideProgressDialog();
                }
            });
            */




          /*  API.getInstance().newMobileRegistration(ActivitySignUp.this,
                    etMobileNumber.getText().toString(), new OnNewMobileRegistrationCallback(ActivitySignUp.this));*/
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //finish();
        return super.onOptionsItemSelected(item);
    }

    /**@Override public void finish()
    {
    Intent i = new Intent(this, ActivitySplash.class);
    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(i);
    }*/
    /*class OnNewMobileRegistrationCallback extends RetrofitCallbacks<NewMobileRegistration>
    {
        public OnNewMobileRegistrationCallback(Context context)
        {
            super(context);
        }

        @Override
        @CallSuper
        public void onResponse(Call<NewMobileRegistration> call, Response<NewMobileRegistration> response)
        {
            super.onResponse(call, response);
            Log.i(TAG, "onResponse: NewMobileRegistration");
            //            Log.i(TAG, "OTP: " + response.body().getData().getOTP());
           *//* Log.i(TAG, "RESPONSE: " + response.body().getData().getResponse());
            Log.i(TAG, "MOBILE: " + response.body().getData().getMobile());
            Log.i(TAG, "DEVICE ID: " + response.body().getData().getDeviceID());*//*

            //etVerifyOtp.setText(response.body().getData().getOTP());
            Log.i(TAG, "Headers Authtoken: " + response.headers().get(Constants.AUTH_TOKEN));

            authToken = response.headers().get(Constants.AUTH_TOKEN);
            if(response.body().getData().getOTP()!=null)
            {
                Intent i = new Intent(ActivitySignUp.this, ActivityVerifyOtp.class);
                i.putExtra(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim());
                startActivity(i);
            }
            else{
                ((App) getApplication()).setUser(response.body().getData());
                Intent i = new Intent(ActivitySignUp.this, ActivityHome.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                PreferenceManager.getDefaultSharedPreferences(ActivitySignUp.this)
                        .edit()
                        .putString(Constants.MOBILE_NO, etMobileNumber.getText().toString().trim())
                        .apply();
            }
            Utils.hideProgressDialog();



        }

        @Override
        public void onFailure(Call<NewMobileRegistration> call, Throwable t)
        {
            Log.i(TAG, "ERROR ON NewMobileRegistration");
            Log.i(TAG, "call.toString(): " + call.toString());
            Log.i(TAG, "t.toString(): " + t.toString());

            Utils.hideProgressDialog();

            Toast.makeText(ActivitySignUp.this, "Something went wrong while generating OTP", Toast.LENGTH_SHORT).show();
        }
    }*/

}
