package com.app.agrizip.start.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.home.activity.ActivityHome;

import api.API;
import api.RetrofitCallbacks;
import apimodels.NewMobileRegistration;
import apimodels.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Response;

public class ActivityVerifyOtp extends AppCompatActivity
{

    private static final String TAG = App.APP_TAG + ActivityVerifyOtp.class.getSimpleName();

    String mobileNo;
    String CountryCode;

    int isAlreadyUser =0;
    @BindView(R.id.tv_verifyNo)
    TextView tvMobileNumberToVerify;
    @BindView(R.id.et_verifyOtp)
    EditText etVerifyOtp;
    @BindView(R.id.btn_resendOtp)
    Button btnResendOtp;
    @BindView(R.id.btn_verifyOtp)
    Button btnVerifyOtp;

    @BindView(R.id.tv_infome)
    TextView tv_infome;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tv_secondsRemaining)
    TextView tvSecondsRemaining;

    @BindView(R.id.tv_waitingForOTP)
    TextView tvWaitingForOTP;

    @BindView(R.id.ll_resendOtpBtnContainer)
    LinearLayout llResendOtpBtnContainer;

    OnGetOtpReceiver onGetOtpReceiver;
    boolean optFlag=true;
    CountDownTimer cdt;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        ButterKnife.bind(this);

        if (mToolbar != null)
        {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left_white_48dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        mobileNo = getIntent().getStringExtra(Constants.MOBILE_NO);
        //isAlreadyUser = getIntent().getBooleanExtra("isAlreadyUser",false);
        isAlreadyUser = getIntent().getIntExtra("isAlreadyUser",0);
        CountryCode = getIntent().getStringExtra("CountryCode");

        btnResendOtp.setOnClickListener(new ResendOtp());

        llResendOtpBtnContainer.setVisibility(View.GONE);
        btnResendOtp.setEnabled(false);

        btnVerifyOtp.setOnClickListener(new VerifyOtp());

        if (mobileNo != null)
        {
            if (!mobileNo.isEmpty())
            {
                tvMobileNumberToVerify.setText("+91 " + mobileNo);
            }
        }
        tv_infome.setVisibility(View.VISIBLE);

        Utils.showProgressDialog(this, "Please Wait...", "Waiting for OTP...", true, false,
                new DialogInterface.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialogInterface)
                    {
                        ActivityVerifyOtp.this.finish();
                    }
                });
        Log.d("CountryInTop=",this.getIntent().getStringExtra("CountryCode"));

        Utils.hideProgressDialog();

        setTimer();
       /* API.getInstance().newMobileRegistration(ActivityVerifyOtp.this,
                mobileNo,this.getIntent().getStringExtra("CountryCode"), new OnNewMobileRegistrationCallback(ActivityVerifyOtp.this));
*/
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if (onGetOtpReceiver == null)
        {
            onGetOtpReceiver = new OnGetOtpReceiver();
        }
        registerReceiver(onGetOtpReceiver, new IntentFilter(Constants.ON_RECEIVE_OTP_BROADCAST));
    }

    @Override
    protected void onStop()
    {
        if (onGetOtpReceiver != null)
        {
            unregisterReceiver(onGetOtpReceiver);
        }
        super.onStop();
    }

    private void setupUserProfile()
    {

        Intent i = new Intent(this, ActivitySetupProfile.class);
        i.putExtra(Constants.MOBILE_NO, mobileNo);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTimer()
    {
        cdt = new CountDownTimer(40000, 1000)
        {
            public void onTick(long millisUntilFinished)
            {
                tvSecondsRemaining.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish()
            {
                tvSecondsRemaining.setText("");
                llResendOtpBtnContainer.setVisibility(View.VISIBLE);
                btnResendOtp.setEnabled(true);

                tvWaitingForOTP.setText("Try resending OTP");
            }
        }.start();
    }

    class ResendOtp implements View.OnClickListener
    {



        @Override
        public void onClick(View view)
        {

            Utils.showProgressDialog(ActivityVerifyOtp.this, "Please Wait...", "Resending OTP...", true, false,
                    new DialogInterface.OnCancelListener()
                    {
                        @Override
                        public void onCancel(DialogInterface dialogInterface)
                        {
                            ActivityVerifyOtp.this.finish();

                        }
                    });

            API.getInstance().resendOTP(ActivityVerifyOtp.this, mobileNo,CountryCode,new RetrofitCallbacks<NewMobileRegistration>(ActivityVerifyOtp.this)
            {
                @Override
                public void onResponse(Call<NewMobileRegistration> call, Response<NewMobileRegistration> response)
                {
                    super.onResponse(call, response);

                    //etVerifyOtp.setText(response.body().getData().getOTP());
                    llResendOtpBtnContainer.setVisibility(View.GONE);
                    setTimer();

                    Utils.hideProgressDialog();

                }

                @Override
                public void onFailure(Call<NewMobileRegistration> call, Throwable t)
                {
                    Log.i(TAG, "ERROR ON resendOTP");
                    Log.i(TAG, "call.toString(): " + call.toString());
                    Log.i(TAG, "t.toString(): " + t.toString());

                    Toast.makeText(ActivityVerifyOtp.this, "Something went wrong while resending OTP", Toast.LENGTH_SHORT).show();

                    Utils.hideProgressDialog();
                }
            });
        }
    }

    class OtpVerificationCallback extends RetrofitCallbacks<UserInfo>
    {

        public OtpVerificationCallback(Context context)
        {
            super(context);
        }

        @Override
        @CallSuper
        public void onResponse(Call<UserInfo> call, Response<UserInfo> response)
        {
            super.onResponse(call, response);
            /**Log.i(TAG, "onResponse: verifyOTP");
             Log.i(TAG, "OTP: " + response.body().getData().getOTP());
             Log.i(TAG, "RESPONSE: " + response.body().getData().getResponse());
             Log.i(TAG, "MOBILE: " + response.body().getData().getMobile());
             Log.i(TAG, "DEVICE ID: " + response.body().getData().getDeviceID());*/

            Utils.hideProgressDialog();

            //Toast.makeText(ActivityVerifyOtp.this, response.body().getData().getMobile() + " Registered Successfully", Toast.LENGTH_SHORT).show();
            if (response.isSuccessful())
            {

                if(response.body().getData().getIsAlreadyUser() == 2){

                    ((App) getApplication()).setUser(response.body().getData());
                    Intent i = new Intent(ActivityVerifyOtp.this, ActivityHome.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    PreferenceManager.getDefaultSharedPreferences(ActivityVerifyOtp.this)
                            .edit()
                            .putString(Constants.MOBILE_NO, mobileNo)
                            .apply();
                }else{
                    setupUserProfile();
                }

            } else
            {
                Toast.makeText(ActivityVerifyOtp.this, "OTP verification failed", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailure(Call<UserInfo> call, Throwable t)
        {
            Log.i(TAG, "ERROR ON Verifying OTP");
            Log.i(TAG, "call.toString(): " + call.toString());
            Log.i(TAG, "t.toString(): " + t.toString());

            Toast.makeText(ActivityVerifyOtp.this, "OTP verification failed", Toast.LENGTH_SHORT).show();

            Utils.hideProgressDialog();
        }
    }

    class OnNewMobileRegistrationCallback extends RetrofitCallbacks<NewMobileRegistration>
    {
        public OnNewMobileRegistrationCallback(Context context)
        {
            super(context);
        }

        @Override
        @CallSuper
        public void onResponse(Call<NewMobileRegistration> call, Response<NewMobileRegistration> response)
        {
            super.onResponse(call, response);
            Log.i(TAG, "onResponse: NewMobileRegistration");
            //            Log.i(TAG, "OTP: " + response.body().getData().getOTP());
           /* Log.i(TAG, "RESPONSE: " + response.body().getData().getResponse());
            Log.i(TAG, "MOBILE: " + response.body().getData().getMobile());
            Log.i(TAG, "DEVICE ID: " + response.body().getData().getDeviceID());*/

            //etVerifyOtp.setText(response.body().getData().getOTP());

            Utils.hideProgressDialog();

            setTimer();

        }

        @Override
        public void onFailure(Call<NewMobileRegistration> call, Throwable t)
        {
            Log.i(TAG, "ERROR ON NewMobileRegistration");
            Log.i(TAG, "call.toString(): " + call.toString());
            Log.i(TAG, "t.toString(): " + t.toString());

            Utils.hideProgressDialog();

            Toast.makeText(ActivityVerifyOtp.this, "Something went wrong while generating OTP", Toast.LENGTH_SHORT).show();
        }
    }

    class VerifyOtp implements View.OnClickListener
    {

        @Override
        public void onClick(View view)
        {

            if (etVerifyOtp.getText().toString().isEmpty())
            {
                Toast.makeText(ActivityVerifyOtp.this, "Please enter OTP", Toast.LENGTH_SHORT).show();
                return;
            }

            if (etVerifyOtp.getText().toString().length() < 6)
            {
                Toast.makeText(ActivityVerifyOtp.this, "OTP must be of 6 digits", Toast.LENGTH_SHORT).show();
                return;
            }

            Utils.showProgressDialog(ActivityVerifyOtp.this, "Please Wait...", "Verifying OTP...", true, false,
                    new DialogInterface.OnCancelListener()
                    {
                        @Override
                        public void onCancel(DialogInterface dialogInterface)
                        {
                            ActivityVerifyOtp.this.finish();
                        }
                    });

            API.getInstance().verifyOTP(ActivityVerifyOtp.this, mobileNo, etVerifyOtp.getText().toString().trim(),
                    new OtpVerificationCallback(ActivityVerifyOtp.this));
        }
    }

    public class OnGetOtpReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String otpNumber = intent.getStringExtra(Constants.RECEIVED_OTP_NUMBER);

            if (otpNumber != null)
            {
                if (!otpNumber.isEmpty())
                {
                    if (cdt != null)
                    {
                        cdt.cancel();
                        tvSecondsRemaining.setText("");
                        tvWaitingForOTP.setText("");
                    }
                    etVerifyOtp.setText(otpNumber);
                    new VerifyOtp().onClick(null);
                }
            }
        }
    }

}
