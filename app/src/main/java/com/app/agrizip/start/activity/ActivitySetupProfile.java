package com.app.agrizip.start.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.start.adapter.DistrictListAdapter;
import com.app.agrizip.start.adapter.StateListAdapter;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import api.API;
import api.RetrofitCallbacks;
import apimodels.DistrictList;
import apimodels.SetupProfile;
import apimodels.StateList;
import apimodels.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import retrofit2.Call;
import retrofit2.Response;

import static extras.Constants.CHOOSE_IMAGE_INTENT;

public class ActivitySetupProfile extends AppCompatActivity implements DatePickerDialog.OnDateSetListener
{

    private static final String TAG = App.APP_TAG + ActivitySetupProfile.class.getSimpleName();

    String imagePath;
    private List<StateList.DataBean> stateListSetup = new ArrayList<>();

    @BindView(R.id.iv_setupProfilePic)
    PorterShapeImageView ivProfilePic;

    String mobileNo, selectedLanguage = "English", selectedState, selectedDistrict;

    @BindView(R.id.et_userFullName)
    EditText etUserFullName;

    @BindView(R.id.tv_setBirthDate)
    TextView tvSetBirthDate;

    @BindView(R.id.rg_dialogEditGender)
    RadioGroup rgDialogEditGender;

    /*@BindView(R.id.sp_language)
    AppCompatSpinner spLanguage;*/

    @BindView(R.id.sp_state)
    AppCompatSpinner spState;

    @BindView(R.id.sp_district)
    AppCompatSpinner spDistrict;
    int statePosition=0;
    int distrctPosition=0;

    String selectedDob, selectedGender = "Male";

    Calendar selectedDate = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_profile);

        ButterKnife.bind(this);

        mobileNo = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(Constants.INCOMPLETE_PROFILE_MOBILE_NO, null);

        if (mobileNo == null)
        {
            PreferenceManager.getDefaultSharedPreferences(this)
                    .edit()
                    .putString(Constants.INCOMPLETE_PROFILE_MOBILE_NO,
                            getIntent().getStringExtra(Constants.MOBILE_NO))
                    .apply();
        }


        mobileNo = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(Constants.INCOMPLETE_PROFILE_MOBILE_NO, "");

        List<String> languageList = new ArrayList<>();
        languageList.add("English");
        languageList.add("Hindi");
        languageList.add("Gujarati");

        /*spLanguage.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                languageList));*/

        /*spLanguage.post(new Runnable()
        {
            @Override
            public void run()
            {
                spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                    {
                        selectedLanguage = adapterView.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView)
                    {

                    }
                });
            }
        });*/


        ivProfilePic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //showImagePopup();
                openImagePicker();
            }
        });

        findViewById(R.id.btn_letsGoAgrizip).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                if (etUserFullName.getText().toString().trim().isEmpty())
                {

                    Toast.makeText(ActivitySetupProfile.this,
                            "Full Name is required", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(statePosition==0)
                {
                    Toast.makeText(ActivitySetupProfile.this,
                            "Please Select Your State", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(tvSetBirthDate.getText().equals("")||tvSetBirthDate.getText().equals(null))
                {
                    Toast.makeText(ActivitySetupProfile.this,
                            "Please Enter Your Birthday", Toast.LENGTH_SHORT).show();
                    return;
                }
                new PostUserDetails(etUserFullName.getText().toString().trim())
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }

        });

        rgDialogEditGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i)
            {
                if (i == R.id.rb_dialogGenderMale)
                {
                    selectedGender = "Male";
                } else if (i == R.id.rb_dialogGenderFemale)
                {
                    selectedGender = "Female";
                }

            }
        });
        final Calendar now = Calendar.getInstance();
        tvSetBirthDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ActivitySetupProfile.this,
                        selectedDate.get(Calendar.YEAR),
                        selectedDate.get(Calendar.MONTH),
                        selectedDate.get(Calendar.DAY_OF_MONTH)

                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
                dpd.setMaxDate(now);
            }
        });

        Glide.with(this)
                .load(R.drawable.user_default)
                .into(ivProfilePic);

        getStateList();

    }

    private void getStateList()
    {

        API.getInstance().getStateList(new RetrofitCallbacks<StateList>(this)
        {

            @Override
            public void onResponse(Call<StateList> call, final Response<StateList> response)
            {
                super.onResponse(call, response);
                if (response.code() == HttpURLConnection.HTTP_OK)
                {
                    try {
                        StateList.DataBean firstState=new StateList.DataBean();
                        firstState.setStateId("1");
                        firstState.setStateName("Select Your State");
                        firstState.setIsDelete(false);



                        stateListSetup.add(firstState);
                        for (StateList.DataBean state : response.body().getData()) {
                            stateListSetup.add(state);
                        }
                        StateListAdapter stateList = new StateListAdapter(ActivitySetupProfile.this, stateListSetup);

                        spState.setAdapter(stateList);
                    }catch(Exception ex){

                }



                    spState.setOnItemSelectedListener(new OnStateChanged());

                }
            }

            @Override
            public void onFailure(Call<StateList> call, Throwable t)
            {
                super.onFailure(call, t);
            }
        });
    }


    /**
     * Showing Image Picker
     */
    public void showImagePopup()
    {

        // File System.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of file system options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Profile Picture");
        startActivityForResult(chooserIntent, CHOOSE_IMAGE_INTENT);
    }
    private void openImagePicker() {

        ImagePicker.create(this)
                .folderMode(false) // folder mode (false by default)
                .folderTitle("Select Photos") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .multi() // multi mode (default mode)
                .limit(1) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .start(CHOOSE_IMAGE_INTENT); // start image picker activity with request code


    }
    /***
     * OnResult of Image Picked
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
       // super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CHOOSE_IMAGE_INTENT){

            if(data != null){
                final ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                 imagePath = images.get(0).getPath();
                Glide.with(this).load(new File(imagePath))
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .into(ivProfilePic);

            }
        }


       /* if (resultCode == RESULT_OK && requestCode == CHOOSE_IMAGE_INTENT)
        {
            if (data == null)
            {
                return;
            }
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null)
            {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);

                Glide.with(this).load(new File(imagePath))
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .into(ivProfilePic);

                cursor.close();

            }
        }
*/

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
    {
        tvSetBirthDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, monthOfYear);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        selectedDate = c;

    }

    private class PostUserDetails extends AsyncTask<Void, Void, Void>
    {
        final String fullName;

        PostUserDetails(final String fullName)
        {
            this.fullName = fullName;
        }

        @Override
        protected void onPreExecute()
        {
            Utils.showProgressDialog(ActivitySetupProfile.this, "Please Wait...", "Setting up your account...", false, false, null);
        }

        @Override
        protected Void doInBackground(Void... voids)
        {

            SetupProfile sp = new SetupProfile();

            if (imagePath != null)
            {
                sp.setUserImage(Utils.convertImageFileToBase64(imagePath));
            } else
            {
                sp.setUserImage(Utils
                        .getEncoded64ImageStringFromBitmap(BitmapFactory
                                .decodeResource(getResources(), R.drawable.user_default)));
            }
            sp.setDeviceID(Utils.getAndroidDeviceID(ActivitySetupProfile.this));
            sp.setInstanceID(Utils.getFcmDeviceToken(ActivitySetupProfile.this));
            sp.setMobile(mobileNo);
            sp.setName(fullName);
            sp.setState(selectedState);
            sp.setDistrict(selectedDistrict);
            sp.setDob(tvSetBirthDate.getText().toString());
            sp.setGender(selectedGender);


            API.getInstance().setupProfile(sp, new RetrofitCallbacks<UserInfo>(ActivitySetupProfile.this)
            {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response)
                {
                    super.onResponse(call, response);
                    if (response != null)
                    {
                        Log.i(TAG, "RESPONSE CODE: " + response.code());
                        if (response.isSuccessful())
                        {
                            if (response.body().getResponseStatus() == 1)
                            {
                                PreferenceManager.getDefaultSharedPreferences(ActivitySetupProfile.this)
                                        .edit()
                                        .putBoolean(Constants.IS_PROFILE_SET, true)
                                        .apply();

                                Utils.hideProgressDialog();
                                ((App) getApplication()).setUser(response.body().getData());
                                Intent activityHome = new Intent(ActivitySetupProfile.this, ActivityHome.class);
                                activityHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(activityHome);
                            } else
                            {
                                Utils.hideProgressDialog();
                            }

                        } else
                        {
                            Utils.hideProgressDialog();
                            try
                            {
                                System.out.println(response.errorBody().string());
                            } catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                        }

                    } else
                    {
                        Log.i(TAG, "RESPONSE IS NULLLLL");
                    }
                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t)
                {
                    super.onFailure(call, t);
                    Utils.hideProgressDialog();
                }
            });

            return null;
        }
    }

    class OnStateChanged implements AdapterView.OnItemSelectedListener
    {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            statePosition = i;
            Log.d("statePosition=", statePosition + "");


            selectedState = stateListSetup.get(i).getStateId();
            if (statePosition == 0) {

                DistrictList.DataBean firstDistrict = new DistrictList.DataBean();
                firstDistrict.setStateId("1");
                firstDistrict.setIsDelete(false);
                firstDistrict.setDistName("Select Your District");
                firstDistrict.setDistrictId("1");
                List<DistrictList.DataBean> DistrictListSetup = new ArrayList<>();
                DistrictListSetup.add(firstDistrict);
                DistrictListAdapter districtList = new DistrictListAdapter(ActivitySetupProfile.this, DistrictListSetup);
                spDistrict.setAdapter(districtList);




            } else {

                API.getInstance().getDistrictList(stateListSetup.get(i).getStateId(), new RetrofitCallbacks<DistrictList>(ActivitySetupProfile.this) {

                    @Override
                    public void onResponse(Call<DistrictList> call, Response<DistrictList> response) {
                        super.onResponse(call, response);
                        if (response.code() == HttpURLConnection.HTTP_OK) {


                            try {
                          /*  DistrictList.DataBean firstDistrict=new DistrictList.DataBean();
                            firstDistrict.setStateId("1");
                            firstDistrict.setIsDelete(false);
                            firstDistrict.setDistName("Select Your District");
                            firstDistrict.setDistrictId("1");
                            List<DistrictList.DataBean> DistrictListSetup = new ArrayList<>();
*/

                                //  DistrictListSetup.add(firstDistrict);
                                List<DistrictList.DataBean> DistrictListSetup = new ArrayList<>();
                                for (DistrictList.DataBean district : response.body().getData()) {
                                    DistrictListSetup.add(district);
                                }
                                DistrictListAdapter districtList = new DistrictListAdapter(ActivitySetupProfile.this, DistrictListSetup);

                                spDistrict.setAdapter(districtList);
                            } catch (Exception ex) {

                            }


                            spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    distrctPosition = i;

                                    selectedDistrict = ((DistrictList.DataBean) adapterView.getSelectedItem()).getDistrictId();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    }

                    @Override
                    public void onFailure(Call<DistrictList> call, Throwable t) {
                        super.onFailure(call, t);
                    }
                });
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView)
        {

        }
    }

}
