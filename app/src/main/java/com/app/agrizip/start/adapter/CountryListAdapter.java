package com.app.agrizip.start.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.agrizip.R;

import apimodels.CountryList;

/**
 * Created by rutvik on 11/21/2016 at 7:53 PM.
 */

public class CountryListAdapter extends BaseAdapter
{

    final CountryList CountryList;
    final Context context;

    public CountryListAdapter(Context context, CountryList CountryList)
    {
        this.CountryList = CountryList;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return CountryList.getData().size();
    }

    @Override
    public Object getItem(int i)
    {
        return CountryList.getData().get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return CountryList.getData().get(i).hashCode();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup)
    {
        CountryViewHolder countryViewHolder;

        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.simple_list_item, viewGroup, false);
            countryViewHolder = new CountryViewHolder(convertView);
            convertView.setTag(countryViewHolder);
        } else
        {
            countryViewHolder = (CountryViewHolder) convertView.getTag();
        }

        countryViewHolder.tvItem.setText(CountryList.getData().get(i).getCountryName());

        return convertView;
    }

    private class CountryViewHolder
    {
        TextView tvItem;

        public CountryViewHolder(View item)
        {
            tvItem = (TextView) item.findViewById(R.id.tv_item);
        }
    }

}