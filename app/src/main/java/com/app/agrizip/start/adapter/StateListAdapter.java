package com.app.agrizip.start.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.agrizip.R;

import java.util.ArrayList;
import java.util.List;

import apimodels.StateList;

/**
 * Created by rutvik on 11/21/2016 at 4:21 PM.
 */

public class StateListAdapter extends BaseAdapter
{

    //final StateList.DataBean stateList;
    final Context context;
     List<StateList.DataBean> state=new ArrayList<>();
    public StateListAdapter(Context context,List<StateList.DataBean> state)
    {
       // this.stateList = stateList;
        this.context = context;
        this.state=state;
    }


    @Override
    public int getCount()
    {
        return state.size();
    }

    @Override
    public Object getItem(int i)
    {
        return state.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return state.get(i).hashCode();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup)
    {
        StateViewHolder stateViewHolder;

        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.simple_list_item, viewGroup, false);
            stateViewHolder = new StateViewHolder(convertView);
            convertView.setTag(stateViewHolder);
        } else
        {
            stateViewHolder = (StateViewHolder) convertView.getTag();
        }

        stateViewHolder.tvItem.setText(state.get(i).getStateName());

        return convertView;
    }

    private class StateViewHolder
    {
        TextView tvItem;

        public StateViewHolder(View item)
        {
            tvItem = (TextView) item.findViewById(R.id.tv_item);
        }
    }

}
