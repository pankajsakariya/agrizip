package views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import apimodels.CommentsOnPost;
import extras.Utils;

/**
 * Created by pankajsakariya on 29/06/17.
 */

public class AdapterReplyComment extends  RecyclerView.Adapter<AdapterReplyComment.ViewHolder> {

    private Context context;



    public static int upcommingPosition = 0 ;

    private LayoutInflater inflater;
    ArrayList<CommentsOnPost.DataBean.CommentReplyDetailBean> replycommentlist;


    public AdapterReplyComment(Context context){
        this.context = context;
        this.replycommentlist = new ArrayList<>();

        inflater = LayoutInflater.from(context);
    }

    public AdapterReplyComment(Context context, ArrayList<CommentsOnPost.DataBean.CommentReplyDetailBean> replycommentlist){
        this.context = context;
        this.replycommentlist = replycommentlist;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.single_comment_reply_row, parent, false);
        AdapterReplyComment.ViewHolder holder = new AdapterReplyComment.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_name.setText(replycommentlist.get(position).getReplyUserName());
     //   holder.tv_time.setText(replycommentlist.get(position).getReplyDate());
        holder.tv_time.setText(DateUtils.getRelativeDateTimeString(context,
                Utils.convertDateToMills(replycommentlist.get(position).getReplyDate()),
                DateUtils.SECOND_IN_MILLIS,
                DateUtils.WEEK_IN_MILLIS,
                0));
        holder.tv_replycontaint.setText(replycommentlist.get(position).getReplyComment());
        Glide.with(context).load(replycommentlist.get(position).getReplyUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(holder.iv_image);
       // holder.iv_image.s
    }

    public void addReplyComment(CommentsOnPost.DataBean.CommentReplyDetailBean replyComment){
        replycommentlist.add(replyComment);
        notifyItemInserted(replycommentlist.size());
    }

    public void clear(){
        replycommentlist.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return replycommentlist.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_name, tv_time, tv_replycontaint;
        private ImageView iv_image;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView)itemView.findViewById(R.id.tv_commentReplyUserName);
            tv_replycontaint  = (TextView)itemView.findViewById(R.id.tv_commentReplyContent);
            tv_time  = (TextView)itemView.findViewById(R.id.tv_commentReplyTime);
            iv_image = (ImageView)itemView.findViewById(R.id.iv_commentReplyUserImage);

        }
    }
}
