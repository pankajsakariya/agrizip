package views;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;

import api.API;
import api.RetrofitCallbacks;
import apimodels.Comment;
import apimodels.CommentOnPost;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 12/19/2016 at 5:20 PM.
 */

public class SingleCommentReplyRow extends FrameLayout {
    final Context context;
    final Comment.CommentReply replyDetailBean;
    Comment reply;

    @BindView(R.id.tv_commentReplyUserName)
    TextView tvCommentReplyUserName;

    @BindView(R.id.tv_commentReplyContent)
    TextView tvCommentReplyContent;

    @BindView(R.id.tv_commentReplyTime)
    TextView tvCommentReplyTime;

    @BindView(R.id.iv_commentReplyUserImage)
    ImageView ivCommentReplyUserImage;

    @BindView(R.id.tv_deleteCommentReply)
    TextView tvDeleteCommentReply;

    @BindView(R.id.tv_editCommentReply)
    TextView tvEditCommentReply;

    public SingleCommentReplyRow(Context context, Comment.CommentReply replyDetailBean) {
        super(context);
        this.context = context;

        this.replyDetailBean = replyDetailBean;

        View view = inflate(context, R.layout.single_comment_reply_row, null);

        addView(view);

        ButterKnife.bind(this);

        tvCommentReplyContent.setText(replyDetailBean.getReplyComment());
        tvCommentReplyUserName.setText(replyDetailBean.getReplyUserName());

        tvCommentReplyTime.setText(DateUtils.getRelativeDateTimeString(context,
                Utils.convertDateToMills(replyDetailBean.getReplyDate()),
                DateUtils.SECOND_IN_MILLIS,
                DateUtils.WEEK_IN_MILLIS,
                0));

      /*  if (replyDetailBean.getReplyUserID().equals(App.getInstance().getUser().getId())) {
            tvDeleteCommentReply.setVisibility(VISIBLE);
            tvEditCommentReply.setVisibility(VISIBLE);

            tvDeleteCommentReply.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    promptUserForDeletingReply();
                }
            });
        } else {
            tvDeleteCommentReply.setVisibility(GONE);
            tvEditCommentReply.setVisibility(GONE);
        }*/

        Glide.with(context).load(replyDetailBean.getReplyUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(ivCommentReplyUserImage);

    }


    @OnClick(R.id.tv_deleteCommentReply)
    public void promptUserForDeletingReply() {
        new AlertDialog.Builder(context)
                .setTitle(R.string.delete_reply)
                .setMessage(R.string.prompt_delete_reply_msg)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (replyDetailBean.getCommentReplyOperationsListener() != null) {
                            replyDetailBean.getCommentReplyOperationsListener().
                                    onRemoveCommentReply(replyDetailBean);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }


    @OnClick(R.id.tv_editCommentReply)
    public void EditReply() {
        final EditText etEditedReplyContent = new EditText(context);
        etEditedReplyContent.setText(replyDetailBean.getReplyComment());

        new AlertDialog.Builder(context)
                .setTitle("Edit Reply")
                .setView(etEditedReplyContent)
                .setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        editReply(etEditedReplyContent.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    private void editReply(final String commentBodyReply) {
        final RetrofitCallbacks<ResponseBody> onEditReplyCallback = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    reply.setComment(commentBodyReply);
                    tvCommentReplyContent.setText(commentBodyReply);
                }

            }
        };
        CommentOnPost editedComment = new CommentOnPost();
        editedComment.setComment(commentBodyReply);
        API.getInstance().editCommentOnPost(editedComment, onEditReplyCallback);

    }

}
