package views;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import api.API;
import api.RetrofitCallbacks;
import apimodels.Comment;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import apimodels.HelpListComments;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 12/16/2016 at 12:32 PM.
 */

public class SingleCommentRow extends FrameLayout
        implements Comment.CommentReplyOperationsListener {

    final Context context;
    final Map<String, SingleCommentReplyRow> commentRepliesMap;
    @BindView(R.id.iv_commentUserImage)
    ImageView ivCommentUserImage;
    @BindView(R.id.tv_commentUserName)
    TextView tvCommentUserName;
    @BindView(R.id.tv_commentContent)
    TextView tvCommentContent;
    @BindView(R.id.tv_commentLikeCount)
    TextView tvCommentLikeCount;
    @BindView(R.id.tv_commentLike)
    TextView tvCommentLike;
    @BindView(R.id.tv_commentReply)
    TextView tvCommentReply;
    @BindView(R.id.tv_commentTime)
    TextView tvCommentTime;
    @BindView(R.id.ll_commentReplyContainer)
    LinearLayout llCommentReplyContainer;
    @BindView(R.id.tv_deleteComment)
    TextView tvDeleteComment;
    @BindView(R.id.tv_editComment)
    TextView tvEditComment;
    @BindView(R.id.iv_commentImage)
    ImageView ivCommentImage;
    Comment singleComment;
  @BindView(R.id.rv_replyComment)
  RecyclerView rv_replyComment;
    AdapterReplyComment adapterReplyComment;
    ArrayList<CommentsOnPost.DataBean.CommentReplyDetailBean> replycommentlist;

    public SingleCommentRow(Context context) {
        super(context);

        this.context = context;

        commentRepliesMap = new HashMap<>();

        View view = inflate(context, R.layout.single_comment_row, null);

        addView(view);

        ButterKnife.bind(this);
        rv_replyComment.setLayoutManager(new LinearLayoutManager(context));
        rv_replyComment.setHasFixedSize(true);
        adapterReplyComment = new AdapterReplyComment(context);
        rv_replyComment.setAdapter(adapterReplyComment);


    }


    public void setModel(final Comment singleComment) {
        this.singleComment = singleComment;
        Glide.with(context).load(singleComment.getUserImage()).asBitmap()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)

                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        ivCommentUserImage.setImageBitmap(resource);
                    }
                });
        tvCommentContent.setText(singleComment.getComment());
        tvCommentUserName.setText(singleComment.getFullName());
        tvCommentTime.setText(DateUtils.getRelativeDateTimeString(context,
                Utils.convertDateToMills(singleComment.getCreateDate()),
                DateUtils.SECOND_IN_MILLIS,
                DateUtils.WEEK_IN_MILLIS,
                0));

        if (singleComment.getImagestr() != null) {
            if (!singleComment.getImagestr().isEmpty()) {
                ivCommentImage.setVisibility(VISIBLE);
                Picasso.with(context)
                        .load(singleComment.getImagestr())
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)

                        .into(ivCommentImage);
            } else {
                ivCommentImage.setVisibility(GONE);
            }
        } else {
            ivCommentImage.setVisibility(GONE);
        }

        if (singleComment.isLiked()) {
            tvCommentLike.setText("Unlike");
        } else {
            tvCommentLike.setText("Like");
        }

        if (singleComment.getType() == Comment.Type.POST) {
            if (singleComment.getCommentReplyDetailForPost() != null) {
                if (singleComment.getCommentReplyDetailForPost().size() > 0) {
                    llCommentReplyContainer.setVisibility(VISIBLE);
                    replycommentlist = new ArrayList<>();
                    adapterReplyComment.clear();

                    for (int i = 0; i < singleComment.getCommentReplyDetailForPost().size(); i++) {
                        CommentsOnPost.DataBean.CommentReplyDetailBean reply =
                                singleComment.getCommentReplyDetailForPost().get(i);
                        reply.setCommentReplyOperationsListener(this);
                        SingleCommentReplyRow scrr = commentRepliesMap.get(reply.getReplyID());
                        if (scrr == null) {
                            final SingleCommentReplyRow singleCommentReplyRow =
                                    new SingleCommentReplyRow(context, reply);
                            commentRepliesMap.put(reply.getReplyID(), singleCommentReplyRow);
                        //    llCommentReplyContainer.removeAllViews();
                            //replycommentlist.add(reply);
                            adapterReplyComment.addReplyComment(reply);
                           // llCommentReplyContainer.addView(singleCommentReplyRow);
                        } else {
                          //  llCommentReplyContainer.removeAllViews();
                          //  llCommentReplyContainer.addView(scrr);
                            adapterReplyComment.addReplyComment(reply);
                        }
                    }
                   // adapterReplyComment = new AdapterReplyComment(context,replycommentlist);
                    //rv_replyComment.setAdapter(adapterReplyComment);
                } else {
                    llCommentReplyContainer.setVisibility(GONE);
                }
            } else {
                llCommentReplyContainer.setVisibility(GONE);
            }
        } else if (singleComment.getType() == Comment.Type.HELP) {
            if (singleComment.getCommentReplyDetailForHelp() != null) {
                if (singleComment.getCommentReplyDetailForHelp().size() > 0) {

                    llCommentReplyContainer.setVisibility(VISIBLE);

                    for (int i = 0; i < singleComment.getCommentReplyDetailForHelp().size(); i++) {
                        HelpListComments.DataBean.CommentReplyDetailBean reply =
                                (HelpListComments.DataBean.CommentReplyDetailBean) singleComment.getCommentReplyDetailForHelp().get(i);
                        reply.setCommentReplyOperationsListener(this);
                        final SingleCommentReplyRow singleCommentReplyRow = new SingleCommentReplyRow(context, reply);
                        commentRepliesMap.put(reply.getReplyID(), singleCommentReplyRow);
                        llCommentReplyContainer.addView(singleCommentReplyRow);
                        //replycommentlist.add(reply);
                    }
                } else {
                    llCommentReplyContainer.setVisibility(GONE);
                }
            } else {
                llCommentReplyContainer.setVisibility(GONE);
            }

        } else {
            commentRepliesMap.clear();
            llCommentReplyContainer.removeAllViews();
            llCommentReplyContainer.setVisibility(GONE);
        }


        tvCommentLikeCount.setText(singleComment.getCommentLikeCount() + "");
        tvCommentReply.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showReplyBox();
            }
        });

        tvCommentLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (singleComment.getCommentOperationsListener() != null) {
                    singleComment.getCommentOperationsListener().onLikeUnlikeComment(singleComment);
                }
            }
        });

        if (singleComment.getUserID().equals(App.getInstance().getUser().getId())) {
            //tvEditComment.setVisibility(VISIBLE);
            tvEditComment.setVisibility(GONE);
          //  tvDeleteComment.setVisibility(VISIBLE);
            tvDeleteComment.setVisibility(GONE);


            tvDeleteComment.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    promptUserForDeletingComment();
                }
            });

        } else {
            tvEditComment.setVisibility(GONE);
            tvDeleteComment.setVisibility(GONE);
        }

    }

    private void showReplyBox() {
        View v = inflate(context, R.layout.single_reply_box, null);

        final ImageView ivUserImage = (ImageView) v.findViewById(R.id.iv_commentReplyUserImage);
        final TextView tvCommentReplyContent = (TextView) v.findViewById(R.id.tv_commentReplyCommentContent);
        final TextView tvCommentReplyUserName = (TextView) v.findViewById(R.id.tv_commentReplyUserName);
        final EditText etCommentReplyContent = (EditText) v.findViewById(R.id.et_replyContent);

        Glide.with(context).load(singleComment.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .crossFade()
                .into(ivUserImage);

        tvCommentReplyContent.setText(tvCommentContent.getText());
        tvCommentReplyUserName.setText(tvCommentUserName.getText());

        new AlertDialog.Builder(context)
                .setView(v)
                .setPositiveButton("Reply", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (singleComment.getCommentOperationsListener() != null) {
                            if (etCommentReplyContent.getText().toString().trim().isEmpty()) {
                                Toast.makeText(context, "Enter a reply", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Date date = new Date(Calendar.getInstance().getTimeInMillis() - TimeZone.getDefault().getRawOffset());
                            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
                            String dateFormatted = formatter.format(date);

                            final CommentsOnPost.DataBean.CommentReplyDetailBean
                                    reply = new CommentsOnPost.DataBean.CommentReplyDetailBean();
                            reply.setReplyComment(etCommentReplyContent.getText().toString());
                            reply.setReplyDate(dateFormatted);
                            reply.setReplyUserName(App.getInstance().getUser().getFullName());
                            reply.setReplyUserID(App.getInstance().getUser().getId());
                            reply.setReplyUserImage(App.getInstance().getUser().getUserImage());

                            singleComment.getCommentOperationsListener().onReplied(singleComment, reply);

                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();

    }

    private void promptUserForDeletingComment() {
        new AlertDialog.Builder(context)
                .setTitle("Delete Comment?")
                .setMessage("Are you sure you want to delete this comment?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (singleComment.getCommentOperationsListener() != null) {
                            singleComment.getCommentOperationsListener().onDeleteComment(singleComment);
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    @OnClick(R.id.tv_editComment)
    public void showEditCommentBox() {

        final EditText etEditedComment = new EditText(context);
        etEditedComment.setText(singleComment.getComment());

        new AlertDialog.Builder(context)
                .setTitle(R.string.edit_comment)
                .setView(etEditedComment)
                .setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (etEditedComment.getText().toString().trim().isEmpty()) {
                            Toast.makeText(context, "Please enter comment", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        editComment(etEditedComment.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();

    }


    public void editComment(final String commentBody) {
        final RetrofitCallbacks<ResponseBody> onEditCommentCallback = new RetrofitCallbacks<ResponseBody>(context) {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    singleComment.setComment(commentBody);
                    tvCommentContent.setText(commentBody);
                } else {
                    Toast.makeText(context, R.string.try_again_later, Toast.LENGTH_SHORT).show();
                }
            }
        };

        CommentOnPost editedComment = new CommentOnPost();
        editedComment.setComment(commentBody);
        editedComment.setId(singleComment.getStatusDetailID());
        editedComment.setUserID(singleComment.getUserID());
        editedComment.setIsComment(true);

        API.getInstance().editCommentOnPost(editedComment, onEditCommentCallback);

    }


    @Override
    public void onRemoveCommentReply(Comment.CommentReply mReply) {
        if (singleComment.getType() == Comment.Type.POST) {
            final CommentsOnPost.DataBean.CommentReplyDetailBean reply =
                    (CommentsOnPost.DataBean.CommentReplyDetailBean) mReply;

            RetrofitCallbacks<ResponseBody> onDeleteCommentReply = new RetrofitCallbacks<ResponseBody>(context) {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    super.onResponse(call, response);
                    if (response.isSuccessful()) {
                        singleComment.getCommentReplyDetailForPost().remove(reply);
                        llCommentReplyContainer.removeView(commentRepliesMap.remove(reply.getReplyID()));
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.try_again_later), Toast.LENGTH_SHORT).show();
                    }
                }
            };

            API.getInstance().removeReplyOnComment(reply.getReplyID(), onDeleteCommentReply);
        }
    }
}
