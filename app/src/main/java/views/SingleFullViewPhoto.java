package views;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

/**
 * Created by rutvik on 1/5/2017 at 5:58 PM.
 */

public class SingleFullViewPhoto extends FrameLayout
{
    public SingleFullViewPhoto(Context context, String image)
    {
        super(context);

        LinearLayout v0 = (LinearLayout) inflate(context, R.layout.full_view_photo, null);

       // View v = inflate(context, R.layout.full_view_photo, null);

        addView(v0);


        /*this.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
*/
        final ImageView ivFullPhoto = (ImageView) findViewById(R.id.iv_fullImage);

        if(image.length() >0) {

            Picasso.with(context)
                    .load(image)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)

                    .into(ivFullPhoto);
        }

    }
}
