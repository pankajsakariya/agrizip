package views;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;

import butterknife.BindView;
import butterknife.ButterKnife;
import viewmodels.SingleDetailModel;

/**
 * Created by rutvik on 12/7/2016 at 6:56 PM.
 */

public class SingleDetailView extends FrameLayout
{

    private static final String TAG = App.APP_TAG + SingleDetailView.class.getSimpleName();

    @BindView(R.id.tv_singleDetailValue)
    public TextView tvSingleDetailValue;
    @BindView(R.id.iv_singleDetailEditIcon)
    ImageView ivSingleDetailEditIcon;
    @BindView(R.id.iv_singleDetailIcon)
    ImageView ivSingleDetailIcon;
    @BindView(R.id.ll_singleDetail)
    LinearLayout ll_singleDetail;

    @BindView(R.id.view_singleDetailSeparator)
    View viewSingleDetailSeparator;
    private SingleDetailModel singleDetailModel;

    public SingleDetailView(Context context)
    {
        super(context);

        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        View v = LayoutInflater.from(context).inflate(R.layout.single_detail, null, false);

        this.addView(v);

        ButterKnife.bind(this);

        ivSingleDetailEditIcon.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (singleDetailModel.getEditSingleDetailListener() != null)
                {
                    singleDetailModel.getEditSingleDetailListener()
                            .onEditDetail(singleDetailModel.getId(), singleDetailModel.getTextValue(),
                                    SingleDetailView.this);
                }
            }
        });

        ivSingleDetailIcon.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (singleDetailModel.isDetailVisible() != 3)
                {
                    if (singleDetailModel.isDetailVisible() == 1)
                    {
                        ivSingleDetailIcon.setImageResource(R.drawable.ic_remove_circle_outline_green_500_24dp);
                        singleDetailModel.setDetailVisible(2);
                    } else
                    {
                        ivSingleDetailIcon.setImageResource(R.drawable.ic_check_circle_green_500_24dp);
                        singleDetailModel.setDetailVisible(1);
                    }
                    if (singleDetailModel.getEditSingleDetailListener() != null)
                    {
                        singleDetailModel.getEditSingleDetailListener()
                                .onEditVisibility(singleDetailModel.getId(),
                                        SingleDetailView.this);
                    }
                }
            }
        });

    }

    public SingleDetailModel getSingleDetailModel()
    {
        return singleDetailModel;
    }

    public void setSingleDetailModel(SingleDetailModel singleDetailModel)
    {
        if (this.singleDetailModel == null)
        {
            this.singleDetailModel = singleDetailModel;
            setDetailValueTest(singleDetailModel.getTextValue());
        }
    }

    /*private void setIcon(Bitmap bitmap)
    {
        ivSingleDetailIcon.setImageBitmap(bitmap);
    }

    private void setIcon(String url)
    {
        Glide.with(getContext()).load(url).into(ivSingleDetailIcon);
    }*/

    private void setDetailValueTest(String textValue)
    {
        if (textValue.equalsIgnoreCase(""))
        {
            ll_singleDetail.setVisibility(GONE);
        } else
        {
            ll_singleDetail.setVisibility(VISIBLE);
            tvSingleDetailValue.setText(textValue);
            if (singleDetailModel.isDetailVisible() == 3)
            {
                ivSingleDetailIcon.setImageResource(singleDetailModel.getResourceId());
            } else if (singleDetailModel.isDetailVisible() == 1)
            {
                ivSingleDetailIcon.setImageResource(R.drawable.ic_check_circle_green_500_24dp);
            } else if (singleDetailModel.isDetailVisible() == 2)
            {
                ivSingleDetailIcon.setImageResource(R.drawable.ic_remove_circle_outline_green_500_24dp);
            }


            if (singleDetailModel.isLast())
            {
                viewSingleDetailSeparator.setVisibility(GONE);
            } else
            {
                viewSingleDetailSeparator.setVisibility(VISIBLE);
            }

            if (singleDetailModel.isReadOnly())
            {
                Log.i(TAG, singleDetailModel.getTextValue() + " IS READ ONLY");
                ivSingleDetailEditIcon.setVisibility(GONE);
            } else
            {
                Log.i(TAG, singleDetailModel.getTextValue() + " IS EDITABLE");
                ivSingleDetailEditIcon.setVisibility(VISIBLE);
            }


        }


    }

}
