package views;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.album.ActivityAlbum;
import com.app.agrizip.app.App;
import com.app.agrizip.profile.my_friend_list_adapter.SingleSmallFriendView;
import com.app.agrizip.profile.update_profile.UpdateProfileActivity;
import com.bumptech.glide.Glide;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import apimodels.UserProfileDetailsInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import viewmodels.SingleDetailModel;
import viewmodels.SingleProfileDetailModel;

/**
 * Created by rutvik on 12/7/2016 at 7:17 PM.
 */

public class SingleProfileDetailView extends LinearLayout {
    private static final String TAG = App.APP_TAG + SingleProfileDetailView.class.getSimpleName();
    final Context context;
    @BindView(R.id.iv_singleProfileDetailIcon)
    ImageView ivSingleProfileDetailIcon;
    @BindView(R.id.tv_singleProfileDetailTitle)
    TextView tvSingleProfileDetailTitle;
    @BindView(R.id.ll_profileDetailsDetailsContainer)
    LinearLayout llProfileDetailsContainer;
    @BindView(R.id.iv_editProfileDetails)
    ImageView ivEditProfileDetails;
    @BindView(R.id.ll_addNewDetail)
    LinearLayout llAddNewDetail;
    @BindView(R.id.tv_addNewDetailLabel)
    TextView tvAddNewDetailLabel;
    @BindView(R.id.ll_showFriendAndPhotoContainer)
    LinearLayout llShowFriendAndPhotoContainer;
    @BindView(R.id.ll_friend_photo)
    LinearLayout ll_friend_photo;

    @BindView(R.id.iv_fourImageViewFirst)
    ImageView iv_fourImageViewFirst;

    @BindView(R.id.iv_fourImageViewSecond)
    ImageView iv_fourImageViewSecond;

    @BindView(R.id.iv_fourImageViewthird)
    ImageView iv_fourImageViewthird;

    @BindView(R.id.iv_fourImageViewfourth)
    ImageView iv_fourImageViewfourth;
    /*@BindView(R.id.ll_userFriends)
    LinearLayout llUserFriends;*/
    @BindView(R.id.ll_userPhotos)
    LinearLayout llUserPhotos;

    @BindView(R.id.rl_moreThanFourImageView)
    RelativeLayout rl_moreThanFourImageView;

    @BindView(R.id.tv_moreThanFourImageCount)
    TextView tvMoreThanFourImageCount;
    List<SingleSmallFriendView> singleSmallFriendViews;
    SingleProfileDetailModel singleProfileDetailModel;

    Map<String, SingleDetailView> singleDetailViewList = new HashMap<>();

    List<String> companyNames = new LinkedList<>();

    public SingleProfileDetailView(final Context context) {
        super(context);

        this.context = context;

        this.setOrientation(VERTICAL);

        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        View v = LayoutInflater.from(context)
                .inflate(R.layout.single_profile_detail, null, false);

        this.addView(v);

        ButterKnife.bind(this);

        llAddNewDetail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (singleProfileDetailModel.getAddSingleDetailListener() != null) {
                    singleProfileDetailModel.getAddSingleDetailListener()
                            .onAddDetailClicked(SingleProfileDetailView.this);
                }
            }
        });

        llUserPhotos.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ActivityAlbum.class);
                i.putExtra(Constants.USER_ID,
                        ((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                                .getUserProfileDetailsInfo()).getUserId());
                context.startActivity(i);
            }
        });

    }

    @OnClick(R.id.iv_editProfileDetails)
    public void editUserProfileDetails() {
        Intent i = new Intent(getContext(), UpdateProfileActivity.class);
        i.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
        i.putExtra("COME_FROM", "BIGPROFILE_SCREEN");
        getContext().startActivity(i);

    }

    public void addNewSingleDetail(SingleDetailModel singleDetailModel) {
        singleProfileDetailModel.addSingleDetailModel(singleDetailModel);
        setSingleProfileDetailModel(singleProfileDetailModel);
    }

    public TextView getTvAddNewDetailLabel() {
        return tvAddNewDetailLabel;

    }

    public SingleProfileDetailModel getSingleProfileDetailModel() {
        return singleProfileDetailModel;
    }

    public void setSingleProfileDetailModel(final SingleProfileDetailModel singleProfileDetailModel) {
        this.singleProfileDetailModel = singleProfileDetailModel;
        tvSingleProfileDetailTitle.setText(singleProfileDetailModel.getTitle());
            try{
        if (singleProfileDetailModel.isShowFriendsAndPhotos()) {
            llShowFriendAndPhotoContainer.setVisibility(VISIBLE);

            singleSmallFriendViews = new ArrayList<>();

            if (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                    .getUserProfileDetailsInfo()).getPhoto().size() > 0) {

                for (UserProfileDetailsInfo.DataBean.PhotoBean friendsBean :
                        ((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                                .getUserProfileDetailsInfo()).getPhoto()) {

                    if (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                            .getUserProfileDetailsInfo()).getPhoto().size() == 1) {
                        iv_fourImageViewFirst.setVisibility(VISIBLE);
                        iv_fourImageViewSecond.setVisibility(GONE);
                        iv_fourImageViewthird.setVisibility(GONE);
                        iv_fourImageViewfourth.setVisibility(GONE);
                        rl_moreThanFourImageView.setVisibility(GONE);
                        Glide.with(context)
                                .load((((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(0).getImagePath()))
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewFirst);
                    } else if (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                            .getUserProfileDetailsInfo()).getPhoto().size() == 2) {
                        iv_fourImageViewFirst.setVisibility(VISIBLE);
                        iv_fourImageViewSecond.setVisibility(VISIBLE);
                        iv_fourImageViewthird.setVisibility(GONE);
                        iv_fourImageViewfourth.setVisibility(GONE);
                        rl_moreThanFourImageView.setVisibility(GONE);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(0).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewFirst);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(1).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewSecond);
                    } else if (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                            .getUserProfileDetailsInfo()).getPhoto().size() == 3) {
                        iv_fourImageViewFirst.setVisibility(VISIBLE);
                        iv_fourImageViewSecond.setVisibility(VISIBLE);
                        iv_fourImageViewthird.setVisibility(VISIBLE);
                        iv_fourImageViewfourth.setVisibility(GONE);
                        rl_moreThanFourImageView.setVisibility(GONE);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(0).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewFirst);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(1).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewSecond);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(2).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewthird);
                    } else if (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                            .getUserProfileDetailsInfo()).getPhoto().size() == 4) {
                        iv_fourImageViewFirst.setVisibility(VISIBLE);
                        iv_fourImageViewSecond.setVisibility(VISIBLE);
                        iv_fourImageViewthird.setVisibility(VISIBLE);
                        iv_fourImageViewfourth.setVisibility(VISIBLE);
                        rl_moreThanFourImageView.setVisibility(INVISIBLE);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(0).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewFirst);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(1).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewSecond);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(2).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewthird);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(3).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewfourth);
                    } else if (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                            .getUserProfileDetailsInfo()).getPhoto().size() > 4) {
                        iv_fourImageViewFirst.setVisibility(VISIBLE);
                        iv_fourImageViewSecond.setVisibility(VISIBLE);
                        iv_fourImageViewthird.setVisibility(VISIBLE);
                        iv_fourImageViewfourth.setVisibility(VISIBLE);
                        rl_moreThanFourImageView.setVisibility(VISIBLE);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(0).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewFirst);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(1).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewSecond);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(2).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewthird);
                        Glide.with(context)
                                .load(((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().get(3).getImagePath())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .crossFade()
                                .into(iv_fourImageViewfourth);
                        // tvMoreThanFourImageCount.setText("+" + (friendsBean.getImagePath()));
                        int totalphoto = ((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getTotalPhotoCount();
                        totalphoto = totalphoto -4;
                        tvMoreThanFourImageCount.setText("+" +String.valueOf(totalphoto));
                      //  tvMoreThanFourImageCount.setText("+" + (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel.getUserProfileDetailsInfo()).getPhoto().size()));
                    }

                }


           /* if (((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                    .getUserProfileDetailsInfo()).getPhoto().size() > 0) {

                //Make layout visible
                for (UserProfileDetailsInfo.DataBean.PhotoBean friendsBean :
                        ((UserProfileDetailsInfo.DataBean) singleProfileDetailModel
                                .getUserProfileDetailsInfo()).getPhoto()) {

                    Log.i(TAG, "ADDING SINGLE SMALL PHOTO: " + friendsBean.getImagePath());

                    SingleSmallFriendView singleSmallFriendView =
                            new SingleSmallFriendView(context, friendsBean);
                    singleSmallFriendViews.add(singleSmallFriendView);
                    llUserPhotos.addView(singleSmallFriendView);
                }
            } else {
                //Hide Layout
                llShowFriendAndPhotoContainer.setVisibility(GONE);
            }*/
            } else {
                llShowFriendAndPhotoContainer.setVisibility(GONE);
            }
        } else {
            //Hide Layout
            llShowFriendAndPhotoContainer.setVisibility(GONE);
        }

        llProfileDetailsContainer.removeAllViews();
        for (int i = 0; i < singleProfileDetailModel.getSingleDetailModelList().size(); i++) {
            SingleDetailModel singleDetailModel = singleProfileDetailModel.getSingleDetailModelList().get(i);
            //DONT USE //singleDetailModel.setEditSingleDetailListener(this);
            if (i == singleProfileDetailModel.getSingleDetailModelList().size() - 1) {
                singleDetailModel.setLast(true);  // Notify list to last item
            } else {
                singleDetailModel.setLast(false);  // Notify list to last item
            }

            final SingleDetailView view = singleDetailViewList.get(singleDetailModel.getId());
            LinearLayout ll = new LinearLayout(getContext());
            ll.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            ll.setOrientation(VERTICAL);

            if (view == null) {
                final SingleDetailView sdv = new SingleDetailView(getContext());
                sdv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                sdv.setSingleDetailModel(singleDetailModel);
                if (sdv.getParent() != null) {
                    ((ViewGroup) sdv.getParent()).removeView(sdv); // <- fix + view Remove
                }
                singleProfileDetailModel.addSingleDetailModel(singleDetailModel);
                ll.addView(sdv);
                singleDetailViewList.put(singleDetailModel.getId(), sdv);
                llProfileDetailsContainer.addView(ll);
            } else {
                view.setSingleDetailModel(singleDetailModel);
                view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                if (view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view); // <- fix + view Remove
                }
                ll.addView(view);
                //singleDetailViewList.put(singleDetailModel.getId(), view);
                llProfileDetailsContainer.addView(ll);
            }

        }

        // ADDNEW Detail item visible / invisible
        if (singleProfileDetailModel.getAddSingleDetailListener() != null) {
            llAddNewDetail.setVisibility(VISIBLE);
            tvAddNewDetailLabel.setText(singleProfileDetailModel.getAddDetailLabel());
        } else {
            llAddNewDetail.setVisibility(GONE);
        }

        if (singleProfileDetailModel.isProfileDetailReadOnly()) {
            Log.i(TAG, "SINGLE PROFILE DETAILS IS READ ONLY");
            ivEditProfileDetails.setVisibility(GONE);
        } else {
            Log.i(TAG, "SINGLE PROFILE DETAILS IS EDITABLE");
            ivEditProfileDetails.setVisibility(VISIBLE);
        }

        if (singleProfileDetailModel.getIconUrl() != null) {
            Glide.with(getContext()).load(singleProfileDetailModel.getIconUrl())
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    .crossFade()
                    .into(ivSingleProfileDetailIcon);
        } else {
            ivSingleProfileDetailIcon.setImageResource(singleProfileDetailModel.getResourceId());
        }

            }catch (Exception e){
            e.printStackTrace();
            }
    }
}
