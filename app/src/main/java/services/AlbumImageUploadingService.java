package services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.app.agrizip.R;
import com.app.agrizip.app.App;

import java.io.IOException;
import java.util.ArrayList;

import api.API;
import api.RetrofitCallbacks;
import apimodels.UploadPhotoInAlbum;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/5/2017 at 12:01 PM.
 */

public class AlbumImageUploadingService extends IntentService
{

    private static final String TAG = App.APP_TAG + StatusImageUploadingService.class.getSimpleName();

    ArrayList<String> imagePathList;

    String albumId;

    String albumName;

    UploadAlbumImageCallbacks uploadAlbumImageCallbacks;

    int totalImages;

    public AlbumImageUploadingService()
    {
        super("AlbumImageUploadingService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        imagePathList = intent.getStringArrayListExtra(Constants.BASE_64_IMAGE_STRING_LIST);

        albumId = intent.getStringExtra(Constants.ALBUM_ID);

        albumName = intent.getStringExtra(Constants.ALBUM_NAME);


        if (albumId != null && imagePathList != null)
        {
            totalImages = imagePathList.size();
            Log.i(TAG, "UPLOADING IMAGES FOR ALBUM ID: " + albumId);
            tryUploadingImages();
        }
    }

    private void tryUploadingImages()
    {
        Log.i(TAG, "tryUploadingImages: IMAGE LIST SIZE: " + imagePathList.size());

        if (imagePathList.size() > 0)
        {
            final String count = (totalImages - imagePathList.size()) + 1 + "";

            final String contentText = "Uploading Image to " + albumName + " :" + count + " of " + totalImages;

            addNotification(contentText, true);

            uploadAlbumImageCallbacks =
                    new UploadAlbumImageCallbacks(imagePathList.get(0), this);

            new AlbumImageUploadingService.UploadStatusImage(imagePathList.get(0), uploadAlbumImageCallbacks)
                    .startUploading();

        } else
        {
            addNotification("Images uploaded successfully!", false);
            stopSelf();
        }
    }

    private void addNotification(String contentText, boolean isOngoing)
    {
        Notification.Builder m_notificationBuilder = new Notification.Builder(this)
                .setContentTitle("Agrizip Album upload")
                .setContentText(contentText)
                .setOngoing(isOngoing)
                .setSmallIcon(R.drawable.ic_camera_alt_black_18dp);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // send the notification
        notificationManager.notify(Constants.ALBUM_IMAGE_UPLOAD_SERVICE, m_notificationBuilder.build());

    }

    public class UploadAlbumImageCallbacks extends RetrofitCallbacks<ResponseBody>
    {

        final String base64ImageString;

        public UploadAlbumImageCallbacks(final String base64ImageString, Context context)
        {
            super(context);
            this.base64ImageString = base64ImageString;
        }

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
        {
            super.onResponse(call, response);
            if (response.isSuccessful())
            {
                try
                {
                    System.out.println(response.body().string());
                    imagePathList.remove(base64ImageString);
                    System.out.print("WAITING FOR 1.5 SECONDS BEFORE UPLOADING NEXT IMAGE");
                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            tryUploadingImages();
                        }
                    }, 1500);

                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t)
        {
            super.onFailure(call, t);
            addNotification("Failed to upload images.", false);
        }
    }

    class UploadStatusImage
    {

        final AlbumImageUploadingService.UploadAlbumImageCallbacks callbacks;
        final String imagePath;

        UploadStatusImage(final String imagePath,
                          final AlbumImageUploadingService.UploadAlbumImageCallbacks callbacks)
        {
            this.imagePath = imagePath;
            this.callbacks = callbacks;
        }

        /**
         *
         */
        private void startUploading()
        {
            final UploadPhotoInAlbum photo = new UploadPhotoInAlbum();
            Log.i(TAG, "IMAGE PATH: " + imagePath);
            photo.setAlbumID(albumId);
            photo.setImagestr(Utils.convertImageFileToBase64(imagePath));
            API.getInstance().uploadPhotoInAlbum(photo, callbacks);
        }

    }

}
