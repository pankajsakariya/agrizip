package services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;

import java.io.IOException;
import java.util.ArrayList;

import api.API;
import api.RetrofitCallbacks;
import apimodels.UploadStatusImageData;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 11/29/2016 at 11:12 AM.
 */

public class StatusImageUploadingService extends IntentService
{
    private static final String TAG = App.APP_TAG + StatusImageUploadingService.class.getSimpleName();

    ArrayList<String> imagePathList;

    String statusId;

    UploadStatusImageCallbacks uploadStatusImageCallbacks;

    int totalImages;

    public StatusImageUploadingService()
    {
        super("StatusImageUploadingService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        imagePathList = intent.getStringArrayListExtra(Constants.BASE_64_IMAGE_STRING_LIST);

        statusId = intent.getStringExtra(Constants.STATUS_ID);

        if (statusId != null && imagePathList != null)
        {
            totalImages = imagePathList.size();
            Log.i(TAG, "UPLOADING IMAGES FOR STATUS ID: " + statusId);
            tryUploadingImages();
        }
    }

    private void tryUploadingImages()
    {
        Log.i(TAG, "tryUploadingImages: IMAGE LIST SIZE: " + imagePathList.size());

        if (imagePathList.size() > 0)
        {
            final String count = (totalImages - imagePathList.size()) + 1 + "";

            final String contentText = "Uploading Image " + count + " of " + totalImages;

            addNotification(contentText, true);

            uploadStatusImageCallbacks =
                    new UploadStatusImageCallbacks(imagePathList.get(0), this);

            new UploadStatusImage(imagePathList.get(0), uploadStatusImageCallbacks)
                    .startUploading();

        } else
        {
            addNotification("Images uploaded successfully!", false);
            final Intent i = new Intent(Constants.ON_POST_PHOTO_UPLOADED_SUCCESSFULLY);
            i.putExtra(Constants.POST_ID, statusId);
            sendBroadcast(i);
            stopSelf();
        }
    }

    private void addNotification(String contentText, boolean isOngoing)
    {
        Notification.Builder m_notificationBuilder = new Notification.Builder(this)
                .setContentTitle("Agrizip")
                .setContentText(contentText)
                .setOngoing(isOngoing)
                .setSmallIcon(R.drawable.ic_camera_alt_black_18dp);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // send the notification
        notificationManager.notify(1066, m_notificationBuilder.build());

    }

    public class UploadStatusImageCallbacks extends RetrofitCallbacks<ResponseBody>
    {

        final String base64ImageString;

        public UploadStatusImageCallbacks(final String base64ImageString, Context context)
        {
            super(context);
            this.base64ImageString = base64ImageString;
        }

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
        {
            super.onResponse(call, response);
            if (response.isSuccessful())
            {
                try
                {
                    System.out.println(response.body().string());
                    imagePathList.remove(base64ImageString);
                    System.out.print("WAITING FOR 1.5 SECONDS BEFORE UPLOADING NEXT IMAGE");
                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            tryUploadingImages();
                        }
                    }, 1500);

                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t)
        {
            super.onFailure(call, t);
            addNotification("Failed to upload images.", false);
        }
    }

    class UploadStatusImage
    {

        final UploadStatusImageCallbacks callbacks;
        final String imagePath;

        UploadStatusImage(final String imagePath, final UploadStatusImageCallbacks callbacks)
        {
            this.imagePath = imagePath;
            this.callbacks = callbacks;
        }

        /**
         *
         */
        private void startUploading()
        {
            try {
                final UploadStatusImageData data = new UploadStatusImageData();
                Log.i(TAG, "IMAGE PATH: " + imagePath);
                data.setImagestr(Utils.convertImageFileToBase64(imagePath));
                data.setIsDelete(false);
                data.setSort(1);
                data.setStatusID(statusId);
                API.getInstance().uploadStatusImage(data, callbacks);

            }catch (OutOfMemoryError e1){
                e1.printStackTrace();
                imagePathList.remove(imagePath);
                tryUploadingImages();
            }
            catch (Exception e){
                e.printStackTrace();
            }
             }

    }

}
