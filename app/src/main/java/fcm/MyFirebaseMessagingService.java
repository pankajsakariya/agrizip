package fcm;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.chat.activity.ActivityChatDialog;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;
import java.util.Map;

import apimodels.NotificationGetPostModel;
import extras.Constants;
import extras.Utils;

/**
 * Created by rutvik on 12/30/2016 at 3:13 PM.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService
{
    private static final String TAG = App.APP_TAG + MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage message)
    {

        /*if (ActivityLifecycle.getInstance().isBackground()) {
            showNotification(message);
        }*/

        String from = message.getFrom();

        // Check if message contains a notification Data.
        if (message.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + message.getData().toString());

            if(message.getData().containsKey("NotificationType") == false && message.getData().containsKey("message") == true && message.getData().get("message").contains("new messages") ) {
                Log.i(TAG + "getBody", message.getData().toString());
                try
                {
                    NotificationGetPostModel objNotificationGetPostModel = new NotificationGetPostModel();
                    objNotificationGetPostModel.setMessage(message.getData().get("message").toString());
                    objNotificationGetPostModel.setUser_id(Integer.valueOf(message.getData().get("user_id").toString()));
                    objNotificationGetPostModel.setNotificationType(Constants.NotificationType.ChatNewMessage);
                    if(isChatWithNotificationUser(objNotificationGetPostModel.getUser_id()) == false)
                    {
                        new NotificationHandler(this, message).showNotification(0,objNotificationGetPostModel);
                    }
                }
                catch (Exception e)
                {
                    Log.i(TAG + "Exception", e.toString());
                }


            }
            else if(message.getData().containsKey("NotificationType") == true && Integer.valueOf(message.getData().get("NotificationType")) == Constants.NotificationType.ChatNewMessage)
            {
                Log.i(TAG + "getBody", message.getData().toString());

                try
                {
                    NotificationGetPostModel objNotificationGetPostModel =  new Gson().fromJson(message.getData().toString(),NotificationGetPostModel.class);
                    if(isChatWithNotificationUser(objNotificationGetPostModel.getUser_id()) == false)
                    {
                        new NotificationHandler(this, message).showNotification(0,objNotificationGetPostModel);
                        //new NotificationHandler(this, message).showNotification(0,objNotificationGetPostModel);

                    }
                }
                catch (Exception e)
                {
                    Log.i(TAG + "Exception", e.toString());
                }

            }
            else
            {
                new NotificationHandler(this, message).showNotification(message.getData().toString(),message.getNotification().getTitle());
            }
        }

        // Check if message contains a notification payload.
        if (message.getNotification() != null) {
            Log.i(TAG + "notification", message.getNotification().getBody());
            NotificationGetPostModel objNotificationGetPostModel =  new Gson().fromJson(message.getData().toString(),NotificationGetPostModel.class);
           // new NotificationHandler(this, message).showNotification(0,objNotificationGetPostModel);

             new NotificationHandler(this, message).showNotification(message.getNotification().getBody(),message.getNotification().getTitle());

            //new NotificationHandler(this,message).showNotification();
        }
    }

    public Boolean isChatWithNotificationUser(int UserId)
    {

        if(ActivityChatDialog.chatRecivedUserId == UserId)
        {
            return  true;
        }
        return  false;
    }




}
