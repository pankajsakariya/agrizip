package fcm;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.app.agrizip.app.App;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import extras.Constants;
import extras.Utils;

/**
 * Created by rutvik on 12/30/2016 at 3:18 PM.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = App.APP_TAG + MyFirebaseInstanceIDService.class.getSimpleName();
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    @Override
    public void onTokenRefresh()
    {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG, "Refreshed token: " + refreshedToken);

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        // Notify UI that registration has completed, so the progress indicator can be hidden.

    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }


    private void storeRegIdInPref(String token) {
        PreferenceManager.getDefaultSharedPreferences(App.getInstance())
                .edit()
                .putString(Constants.FCM_DEVICE_TOKEN, token)
                .apply();


    }

    public static String getFcmInstanceId()
    {
        return FirebaseInstanceId.getInstance().getToken();
    }

}
