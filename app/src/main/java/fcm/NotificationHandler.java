package fcm;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.chat.activity.ActivityChatDialog;
import com.app.agrizip.friends.friend_list.ActivityFriendList;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.need_help.activity.ActivityNeedHelpList;
import com.app.agrizip.stories.activity.ActivityFullStory;
import com.app.agrizip.stories.activity.StoryListActivity;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import apimodels.NotificationGetPostModel;
import extras.Constants;
import extras.Utils;

/**
 * Created by rutvik on 2/22/2017 at 5:55 PM.
 */

public class NotificationHandler
{

    private static final String TAG = App.APP_TAG + NotificationHandler.class.getSimpleName();

    Context context;

    RemoteMessage remoteMessage;

    JSONObject data;

    JSONObject extra;
    String title = "AgriZip";

    public NotificationHandler(Context context, RemoteMessage remoteMessage)
    {
        this.context = context;
        this.remoteMessage = remoteMessage;
        /*try
        {
            this.data = new JSONObject(remoteMessage.getData().get("data"));
            this.extra = new JSONObject(remoteMessage.getData().get("extra"));

        } catch (JSONException e)
        {
            e.printStackTrace();
        }*/
    }

    public NotificationHandler(Context context)
    {
        this.context = context;
    }

    public void handleNotification()
    {

        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.d(TAG, "key, " + key + " value " + value);
        }

        try
        {
            switch (data.get("type").toString())
            {

            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void showNotification(String messageBody,String notificationTitle) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.notification_icon_1);

        title = notificationTitle;
        if(title == null){
            title="Agrizip";
        }
        //Intent intent = new Intent(context, ActivityHome.class);
        Intent intent =  createIntent1(title);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = null;
        final String notificationsound = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.CHOSEN_NOTIFICATION_TONE, "");

        if(notificationsound.isEmpty()){
            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        }else {
            defaultSoundUri = Uri.parse(notificationsound);
        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setLargeIcon(bitmap)
                .setSound(defaultSoundUri)


                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    public Intent createIntent1(String title)
    {
        Intent intent = null;
        if(title.equalsIgnoreCase("Comment on post"))
        {
            intent = new Intent(context, ActivityHome.class);
        }
        else if(title.equalsIgnoreCase("Tag you into post"))
        {
            intent = new Intent(context, ActivityHome.class);
        }
        else if(title.equalsIgnoreCase("Help Post created successfully")) {

            intent = new Intent(context, ActivityNeedHelpList.class);

        }
        else if(title.equalsIgnoreCase("Wall Post created Successfully")) {

            intent = new Intent(context, ActivityHome.class);

        }
        else if(title.equalsIgnoreCase("Welcome to Agrizip")) {

            intent = new Intent(context, ActivityHome.class);

        }
        else if(title.equalsIgnoreCase("Sent friend request")) {
            // SHOW PENDING REQUEST FRAGMENT
            intent = new Intent(context, ActivityHome.class);

         }
        else if(title.equalsIgnoreCase("Accept friend request")) {

            intent = new Intent(context, ActivityFriendList.class);

        }
        else if(title.equalsIgnoreCase("Agrizip posted new blog")) {

            intent = new Intent(context, StoryListActivity.class);

        }
        else if(title.equalsIgnoreCase("Agrizip")){
            intent = new Intent(context, ActivityHome.class);
        }else{
            intent = new Intent(context, ActivityHome.class);
        }
        return intent;
    }
    private static int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notification_icon_1  : R.drawable.ic_app_logo;
    }

    public void showNotification(final int id, NotificationGetPostModel objNotificationGetPostModel)
    {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.notification_icon_1);

        //  Intent intent =  createIntent1(objNotificationGetPostModel.);
        Intent intent =  createIntent(objNotificationGetPostModel.getNotificationType(),objNotificationGetPostModel);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, id /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(title)
                .setContentText(objNotificationGetPostModel.getMessage())
                .setAutoCancel(true)
                .setLargeIcon(bitmap)

                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
    }




    public Intent createIntent(int notificationType,NotificationGetPostModel objNotificationGetPostModel)
    {
        Intent intent = null;

        if(Constants.NotificationType.BlogPost == notificationType)
        {
          //  title = context.getResources().getString(R.string.pushtitleBlogPost);

            title = "Agrizip posted new blog";
            Log.i(TAG, String.valueOf(notificationType));
            intent = new Intent(context, StoryListActivity.class);
        }
        else if(Constants.NotificationType.FriendReqAccept == notificationType)
        {
            //title = context.getResources().getString(R.string.pushtitleFriendReqAccept);
            title = "Accept friend request";
            Log.i(TAG, String.valueOf(notificationType));
            intent = new Intent(context, ActivityFriendList.class);
        }
        else if(Constants.NotificationType.ChatNewMessage == notificationType) {
            title = context.getResources().getString(R.string.pushtitleChatNewMessage);
            //intent = new Intent(context, ActivityHome.class);
            intent = ActivityChatDialog.startForNotificationResult(context,objNotificationGetPostModel.getUser_id());
            Log.i(TAG, String.valueOf(notificationType));
        }
        else if(Constants.NotificationType.HelpPost == notificationType) {
           // title = context.getResources().getString(R.string.pushtitleHelpPost);
            title ="Help Post created successfully";
            intent = new Intent(context, ActivityNeedHelpList.class);
            Log.i(TAG, String.valueOf(notificationType));
        }
        else if(Constants.NotificationType.SharePost == notificationType) {
            title = context.getResources().getString(R.string.pushtitleSharePost);
            intent = new Intent(context, ActivityHome.class);
            Log.i(TAG, String.valueOf(notificationType));
        }
        return intent;
    }

    private void showBigNotification(final int id, final String title, final String message)
    {
        Intent intent = new Intent(context, ActivityHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, id /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
    }
    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + context.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(context, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
