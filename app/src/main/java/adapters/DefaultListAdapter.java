package adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;

import java.util.List;

import apimodels.GetAlbums;
import moaritem.MoarItem;

/**
 * Created by rutvik on 1/10/2017 at 1:53 PM.
 */

public class DefaultListAdapter extends ArrayAdapter<GetAlbums.DataBean.PhotosBean> implements DemoAdapter
{

    private final LayoutInflater layoutInflater;

    private final Context context;

    public DefaultListAdapter(Context context, List<GetAlbums.DataBean.PhotosBean> items)
    {
        super(context, 0, items);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent)
    {
        View v;

        MoarItem item = getItem(position);
        boolean isRegular = getItemViewType(position) == 0;

        if (convertView == null)
        {
            v = layoutInflater.inflate(R.layout.single_album_photo_grid_item, parent, false);
        } else
        {
            v = convertView;
        }

        ImageView imageView = (ImageView) v.findViewById(R.id.iv_singleAlbumPhotoGridItem);

        /*TextView tv = (TextView) v.findViewById(R.id.textview_odd);
        tv.setText(position + "");*/

        Glide.with(context)
                .load(item.getImagestr())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .into(imageView);

        return v;
    }

    @Override
    public int getViewTypeCount()
    {
        return 2;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position % 2 == 0 ? 1 : 0;
    }

    public void appendItems(List<GetAlbums.DataBean.PhotosBean> newItems)
    {
        addAll(newItems);
        notifyDataSetChanged();
    }

    public void setItems(List<GetAlbums.DataBean.PhotosBean> moreItems)
    {
        clear();
        appendItems(moreItems);
    }

}
