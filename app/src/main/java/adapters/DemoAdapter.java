package adapters;

import android.widget.ListAdapter;

import java.util.List;

import apimodels.GetAlbums;

/**
 * Created by rutvik on 1/10/2017 at 1:54 PM.
 */

public interface DemoAdapter extends ListAdapter
{

    void appendItems(List<GetAlbums.DataBean.PhotosBean> newItems);

    void setItems(List<GetAlbums.DataBean.PhotosBean> moreItems);
}
