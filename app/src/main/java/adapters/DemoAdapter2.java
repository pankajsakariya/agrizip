package adapters;

import android.widget.ListAdapter;

import java.util.List;

import apimodels.PostImageList;

/**
 * Created by rutvik on 1/11/2017 at 7:14 PM.
 */

public interface DemoAdapter2 extends ListAdapter
{

    void appendItems(List<PostImageList.DataBean> newItems);

    void setItems(List<PostImageList.DataBean> moreItems);
}
