package viewmodels;

import android.util.Log;

import com.app.agrizip.app.App;

import views.SingleDetailView;

/**
 * Created by rutvik on 12/7/2016 at 7:26 PM.
 */

public class SingleDetailModel
{
    private static final String TAG = App.APP_TAG + SingleDetailView.class.getSimpleName();

    private final String id;

    private String textValue;

    private String iconUrl;
    public String Editkey;
    private int resourceId;

    private boolean isLast = false;

    private boolean isReadOnly = true;

    private EditSingleDetailListener editSingleDetailListener;

    //1-VISIBLE | 2-INVISIBLE | 3-N/A
    private int isDetailVisible = 1;

    public SingleDetailModel(String id, String textValue, String iconUrl)
    {
        this.textValue = textValue;
        this.iconUrl = iconUrl;
        this.id = id;
    }

    public SingleDetailModel(String id, String textValue, int resourceId)
    {
        this.textValue = textValue;
        this.resourceId = resourceId;
        this.id = id;
    }
    public SingleDetailModel(String id,String Editkey, String textValue, String iconUrl)
    {
        this.textValue = textValue;
        this.iconUrl = iconUrl;
        this.id = id;
        this.Editkey=Editkey;
    }

    public int isDetailVisible()
    {
        return isDetailVisible;
    }

    public void setDetailVisible(int detailVisible)
    {
        isDetailVisible = detailVisible;
    }

    public EditSingleDetailListener getEditSingleDetailListener()
    {
        if (editSingleDetailListener == null)
        {
            Log.i(TAG, "LISTENER IS NULL FOR: " + getTextValue());
        }
        return editSingleDetailListener;
    }

    public void setEditSingleDetailListener(EditSingleDetailListener listener)
    {
        this.editSingleDetailListener = listener;
    }

    public String getTextValue()
    {
        return textValue;
    }

    public void setTextValue(String textValue)
    {
        this.textValue = textValue;
    }

    public String getIconUrl()
    {
        return iconUrl;
    }

    public int getResourceId()
    {
        return resourceId;
    }

    public boolean isLast()
    {
        return isLast;
    }

    public void setLast(boolean last)
    {
        isLast = last;
    }

    public boolean isReadOnly()
    {
        return isReadOnly;
    }

    public void setReadOnly(boolean readOnly)
    {
        isReadOnly = readOnly;
    }

    public String getId()
    {
        return id;
    }

    public interface EditSingleDetailListener
    {
        public void onEditDetail(String id, String value, SingleDetailView singleDetailView);

        public void onEditVisibility(String id, SingleDetailView singleDetailView);
    }
}
