package viewmodels;

/**
 * Created by rutvik on 11/22/2016 at 11:03 AM.
 */

public class BigProfileViewWithDetails
{

    final String userProfilePic, userFullName, userLocation;

    public BigProfileViewWithDetails(final String userProfilePic, final String userFullName, final String userLocation)
    {
        this.userProfilePic = userProfilePic;
        this.userFullName = userFullName;
        this.userLocation = userLocation;
    }

    public String getUserProfilePic()
    {
        return userProfilePic;
    }

    public String getUserFullName()
    {
        return userFullName;
    }

    public String getUserLocation()
    {
        return userLocation;
    }
}
