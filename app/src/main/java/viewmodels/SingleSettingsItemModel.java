package viewmodels;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.CallSuper;
import android.view.View;
import android.widget.CompoundButton;

import com.app.agrizip.app.App;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AppSettingModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/21/2017 at 4:51 PM.
 */

public class SingleSettingsItemModel
{

    private final String title;
    private final Context context;
    SettingsChangedListener settingsChangedListener;
    View.OnClickListener onClickListener;
    private boolean isOn;
    private String subTitle;
    private boolean showSwitch;

    public SingleSettingsItemModel(final Context context,
                                   final String title,
                                   final String subTitle,
                                   final boolean showSwitch,
                                   SettingsChangedListener listener,
                                   View.OnClickListener onClickListener)
    {
        this.settingsChangedListener = listener;
        this.context = context;
        this.title = title;
        this.subTitle = subTitle;
        this.showSwitch = showSwitch;
        if (showSwitch)
        {
            isOn = PreferenceManager.getDefaultSharedPreferences(context)
                    .getBoolean(listener.getSettingName(), isOn);
        }
        this.onClickListener = onClickListener;
    }

    public String getTitle()
    {
        return title;
    }

    public String getSubTitle()
    {
        return subTitle;
    }

    public void setSubTitle(String subTitle)
    {
        this.subTitle = subTitle;
    }

    public boolean isOn()
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(settingsChangedListener.getSettingName(), isOn);
    }

    public boolean isShowSwitch()
    {
        return showSwitch;
    }

    public void setShowSwitch(boolean showSwitch)
    {
        this.showSwitch = showSwitch;
    }

    public SettingsChangedListener getSettingsChangedListener()
    {
        return settingsChangedListener;
    }

    public View.OnClickListener getOnClickListener()
    {
        return onClickListener;
    }

    public static class SettingsChangedListener implements CompoundButton.OnCheckedChangeListener{
        private final Context context;
        private final String settingName;

        public SettingsChangedListener(final Context context, final String settingName)
        {
            this.context = context;
            this.settingName = settingName;
        }

        @CallSuper
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isOn)
        {
           // Toast.makeText(context, "CHECK CHANGED", Toast.LENGTH_SHORT).show();
            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit()
                    .putBoolean(settingName, isOn)
                    .apply();



        }

        public String getSettingName()
        {
            return settingName;
        }
    }





}
