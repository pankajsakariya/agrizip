package viewmodels;

import android.content.Context;
import android.util.Log;

import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditEducation;
import com.app.agrizip.profile.update_profile.update_profile_dialogs.EditWork;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import apimodels.UserProfileDetailGetForEdit;
import apimodels.UserProfileDetailsInfo;
import apimodels.UserProfileInformation;
import views.SingleDetailView;
import views.SingleProfileDetailView;

/**
 * Created by rutvik on 12/8/2016 at 1:06 PM.
 */

public class SingleProfileDetailModel implements SingleDetailModel.EditSingleDetailListener
{

    final List<SingleDetailModel> singleDetailModelList;

    final String title;
    final boolean isProfileDetailReadOnly;

    final UserProfileInformation userProfileDetailsInfo;
    final Context context;
    UserProfileDetailGetForEdit userProfileDetailGetForEdit;

    String iconUrl;
    int resourceId;
    boolean isDetailReadOnly = false;
    AddSingleDetailListener addSingleDetailListener;
    String addDetailLabel;
    boolean showFriendsAndPhotos = false;
    List<UserProfileDetailsInfo.DataBean.FriendsBean> friendsBeanList;

    public SingleProfileDetailModel(final Context context, final String title, final String iconUrl,
                                    final boolean isProfileDetailReadOnly,
                                    final UserProfileInformation userProfileDetailsInfo)
    {
        this.context = context;
        this.title = title;
        this.iconUrl = iconUrl;
        this.isProfileDetailReadOnly = isProfileDetailReadOnly;
        this.userProfileDetailsInfo = userProfileDetailsInfo;
        singleDetailModelList = new LinkedList<>();
    }

    public SingleProfileDetailModel(final Context context, final String title, int resourceId,
                                    final boolean isProfileDetailReadOnly,
                                    final UserProfileInformation userProfileDetailsInfo)
    {
        this.context = context;
        this.title = title;
        this.resourceId = resourceId;
        this.isProfileDetailReadOnly = isProfileDetailReadOnly;
        this.userProfileDetailsInfo = userProfileDetailsInfo;
        singleDetailModelList = new LinkedList<>();
    }

    public UserProfileDetailGetForEdit getUserProfileDetailGetForEdit()
    {
        return userProfileDetailGetForEdit;
    }

    public void setUserProfileDetailGetForEdit(UserProfileDetailGetForEdit userProfileDetailGetForEdit)
    {
        this.userProfileDetailGetForEdit = userProfileDetailGetForEdit;
    }

    public List<SingleDetailModel> getSingleDetailModelList()
    {
        return singleDetailModelList;
    }

    public void addSingleDetailModel(SingleDetailModel singleDetailModel)
    {
        singleDetailModelList.add(singleDetailModel);
    }

    public String getTitle()
    {
        return title;
    }

    public String getIconUrl()
    {
        return iconUrl;
    }

    public int getResourceId()
    {
        return resourceId;
    }

    public boolean isProfileDetailReadOnly()
    {
        return isProfileDetailReadOnly;
    }

    public boolean isDetailReadOnly()
    {
        return isDetailReadOnly;
    }

    public void setDetailReadOnly(boolean detailReadOnly)
    {
        isDetailReadOnly = detailReadOnly;
    }

    public UserProfileInformation getUserProfileDetailsInfo()
    {
        return userProfileDetailsInfo;
    }

    public AddSingleDetailListener getAddSingleDetailListener()
    {
        return addSingleDetailListener;
    }

    public void setAddSingleDetailListener(String addDetailLabel, AddSingleDetailListener addSingleDetailListener)
    {
        this.addSingleDetailListener = addSingleDetailListener;
        this.addDetailLabel = addDetailLabel;
    }

    public String getAddDetailLabel()
    {
        return addDetailLabel;
    }

    public boolean isShowFriendsAndPhotos()
    {
        return showFriendsAndPhotos;
    }

    public void setShowFriendsAndPhotos(boolean showFriendsAndPhotos,
                                        List<UserProfileDetailsInfo.DataBean.FriendsBean> friendsBeanList)
    {
        this.showFriendsAndPhotos = showFriendsAndPhotos;
        this.friendsBeanList = friendsBeanList;
    }

    public List<UserProfileDetailsInfo.DataBean.FriendsBean> getFriendsBeanList()
    {
        return friendsBeanList != null ? friendsBeanList :
                new ArrayList<UserProfileDetailsInfo.DataBean.FriendsBean>();
    }

    @Override
    public void onEditDetail(String id, String value, SingleDetailView singleDetailView)
    {

            Log.d("tesingid",id);
            String EditTag="default";


                if(singleDetailView.getSingleDetailModel().Editkey!=null) {
                    EditTag = singleDetailView.getSingleDetailModel().Editkey;
                }else {
                    EditTag=id;
                }


//        Log.d("testingProfile",id+singleDetailView);

//        Log.d("!!!!!testingProfile",id);

//        Log.d("!!!!testingProfile", singleDetailView.getSingleDetailModel().getTextValue().toString());

      //  Log.d("???Tesing",singleDetailView.getSingleDetailModel().Editkey);
            switch (EditTag) {

                case "Work":
                    EditWork editWork = new EditWork(context, id,
                            userProfileDetailGetForEdit, null, singleDetailView,
                            null);

                    editWork.show();
                    break;
                case "Education":

                    EditEducation editEducation = new EditEducation(context, id,
                            userProfileDetailGetForEdit, null, singleDetailView, null);
                    Log.d("qqqq",editEducation.id);

                    editEducation.show();
                    break;
                default:
                    break;

            }
    }

    @Override
    public void onEditVisibility(String id, SingleDetailView singleDetailView)
    {

    }

    public interface AddSingleDetailListener
    {
        void onAddDetailClicked(SingleProfileDetailView singleProfileDetailView);
    }

}
