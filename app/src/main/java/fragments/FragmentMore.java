package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.agrizip.R;

/**
 * Created by Rakshit on 18-11-2016 at 17:54.
 */

public class FragmentMore extends Fragment {
    public static FragmentMore newInstance(int index) {
        FragmentMore fragment = new FragmentMore();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_more, container, false);

        return rootView;
    }
}
