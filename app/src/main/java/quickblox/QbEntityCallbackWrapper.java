package quickblox;

import android.os.Bundle;

import com.quickblox.core.QBEntityCallback;

/**
 * Created by rutvik on 1/25/2017 at 3:46 PM.
 */

public class QbEntityCallbackWrapper<T> extends QbEntityCallbackTwoTypeWrapper<T, T>
{
    public QbEntityCallbackWrapper(QBEntityCallback<T> callback)
    {
        super(callback);
    }

    @Override
    public void onSuccess(T t, Bundle bundle)
    {
        onSuccessInMainThread(t, bundle);
    }
}