package quickblox;

/**
 * Created by rutvik on 1/27/2017 at 6:03 PM.
 */

public interface PaginationHistoryListener
{
    void downloadMore();
}
