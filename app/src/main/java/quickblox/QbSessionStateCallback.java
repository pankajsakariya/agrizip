package quickblox;

/**
 * Created by rutvik on 1/25/2017 at 4:34 PM.
 */

public interface QbSessionStateCallback
{

    void onSessionCreated(boolean success);
}
