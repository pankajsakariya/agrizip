package quickblox;

import android.os.Bundle;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

/**
 * Created by rutvik on 1/25/2017 at 3:53 PM.
 */

public class QbEntityCallbackImpl<T> implements QBEntityCallback<T>
{

    public QbEntityCallbackImpl()
    {
    }

    @Override
    public void onSuccess(T result, Bundle bundle)
    {

    }

    @Override
    public void onError(QBResponseException e)
    {

    }
}
