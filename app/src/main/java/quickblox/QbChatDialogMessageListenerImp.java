package quickblox;

import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.model.QBChatMessage;

/**
 * Created by rutvik on 1/25/2017 at 3:19 PM.
 */

public class QbChatDialogMessageListenerImp implements QBChatDialogMessageListener
{
    public QbChatDialogMessageListenerImp()
    {
    }

    @Override
    public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer)
    {

    }

    @Override
    public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer)
    {

    }
}
