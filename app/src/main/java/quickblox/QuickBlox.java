package quickblox;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

/**
 * Created by rutvik on 1/25/2017 at 11:55 AM.
 */

public class QuickBlox
{

    public static void registerNewUser(final String userId, final String password,
                                       final String fullName,
                                       QBEntityCallback<QBUser> callback)
    {
        final QBUser user = new QBUser(userId, password);
        user.setFullName(fullName);
        QBUsers.signUp(user).performAsync(callback);
    }

    public static void loginToQuickBlox(final String userId, final String password,
                                        QBCallbacks.onLoginCallback callback)
    {
        final QBUser user = new QBUser(userId, password);
        QBUsers.signIn(user).performAsync(callback);
    }
}
