package quickblox;

public interface MimeType {

    String IMAGE_MIME = "image/*";
    String STREAM_MIME = "application/octet-stream";
}
