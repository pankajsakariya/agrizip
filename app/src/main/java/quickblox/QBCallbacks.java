package quickblox;

import android.os.Bundle;
import android.support.annotation.CallSuper;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import java.net.HttpURLConnection;

/**
 * Created by rutvik on 1/25/2017 at 12:31 PM.
 */

public class QBCallbacks
{


    public static class onLoginCallback implements QBEntityCallback<QBUser>
    {

        final LoginListener loginListener;

        public onLoginCallback(LoginListener loginListener)
        {
            this.loginListener = loginListener;
        }

        @CallSuper
        @Override
        public void onSuccess(QBUser user, Bundle bundle)
        {
            loginListener.loginSuccessful(user, bundle);
        }

        @CallSuper
        @Override
        public void onError(QBResponseException e)
        {
            if (e.getHttpStatusCode() == HttpURLConnection.HTTP_UNAUTHORIZED &&
                    e.getMessage().equals("Unauthorized"))
            {
                loginListener.unauthorized(e);
            }
        }

        public interface LoginListener
        {
            void unauthorized(QBResponseException e);

            void loginSuccessful(QBUser user, Bundle bundle);
        }

    }

}
