package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 1/17/2017 at 6:39 PM.
 */

public class SalePostList
{

    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"HelpId":"b3ca384e-13f3-45c7-ba1b-cfbb6ccb1a10","UserID":"c7e5d577-3328-4a86-97e5-0192b9d352a0","UserName":"sachin","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","IsCheckIn":false,"CheckInID":"00000000-0000-0000-0000-000000000000","HelpTitle":"test","Description":"Tst","FeelingName":"","FeelingValue":"","PostYear":"2017","PostMonth":"01","PostDay":"16","PostHour":"18","PostMinute":"24","Help":true,"HelpType":3,"IsLiked":false,"LikeCounts":0,"CommentCounts":0,"CreateDatetime":null,"Message":null,"Place":null,"Images":null,"TagDetails":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * HelpId : b3ca384e-13f3-45c7-ba1b-cfbb6ccb1a10
         * UserID : c7e5d577-3328-4a86-97e5-0192b9d352a0
         * UserName : sachin
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg
         * IsCheckIn : false
         * CheckInID : 00000000-0000-0000-0000-000000000000
         * HelpTitle : test
         * Description : Tst
         * FeelingName :
         * FeelingValue :
         * PostYear : 2017
         * PostMonth : 01
         * PostDay : 16
         * PostHour : 18
         * PostMinute : 24
         * Help : true
         * HelpType : 3
         * IsLiked : false
         * LikeCounts : 0
         * CommentCounts : 0
         * CreateDatetime : null
         * Message : null
         * Place : null
         * Images : null
         * TagDetails : null
         */

        @SerializedName("HelpId")
        private String HelpId;
        @SerializedName("UserID")
        private String UserID;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("IsCheckIn")
        private boolean IsCheckIn;
        @SerializedName("CheckInID")
        private String CheckInID;
        @SerializedName("HelpTitle")
        private String HelpTitle;
        @SerializedName("Description")
        private String Description;
        @SerializedName("FeelingName")
        private String FeelingName;
        @SerializedName("FeelingValue")
        private String FeelingValue;
        @SerializedName("PostYear")
        private String PostYear;
        @SerializedName("PostMonth")
        private String PostMonth;
        @SerializedName("PostDay")
        private String PostDay;
        @SerializedName("PostHour")
        private String PostHour;
        @SerializedName("PostMinute")
        private String PostMinute;
        @SerializedName("Help")
        private boolean Help;
        @SerializedName("HelpType")
        private int HelpType;
        @SerializedName("IsLiked")
        private boolean IsLiked;
        @SerializedName("LikeCounts")
        private int LikeCounts;
        @SerializedName("CommentCounts")
        private int CommentCounts;
        @SerializedName("CreateDatetime")
        private Object CreateDatetime;
        @SerializedName("Message")
        private Object Message;
        @SerializedName("Place")
        private Object Place;
        @SerializedName("Images")
        private Object Images;
        @SerializedName("TagDetails")
        private Object TagDetails;

        public String getHelpId()
        {
            return HelpId;
        }

        public void setHelpId(String HelpId)
        {
            this.HelpId = HelpId;
        }

        public String getUserID()
        {
            return UserID;
        }

        public void setUserID(String UserID)
        {
            this.UserID = UserID;
        }

        public String getUserName()
        {
            return UserName;
        }

        public void setUserName(String UserName)
        {
            this.UserName = UserName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public boolean isIsCheckIn()
        {
            return IsCheckIn;
        }

        public void setIsCheckIn(boolean IsCheckIn)
        {
            this.IsCheckIn = IsCheckIn;
        }

        public String getCheckInID()
        {
            return CheckInID;
        }

        public void setCheckInID(String CheckInID)
        {
            this.CheckInID = CheckInID;
        }

        public String getHelpTitle()
        {
            return HelpTitle;
        }

        public void setHelpTitle(String HelpTitle)
        {
            this.HelpTitle = HelpTitle;
        }

        public String getDescription()
        {
            return Description;
        }

        public void setDescription(String Description)
        {
            this.Description = Description;
        }

        public String getFeelingName()
        {
            return FeelingName;
        }

        public void setFeelingName(String FeelingName)
        {
            this.FeelingName = FeelingName;
        }

        public String getFeelingValue()
        {
            return FeelingValue;
        }

        public void setFeelingValue(String FeelingValue)
        {
            this.FeelingValue = FeelingValue;
        }

        public String getPostYear()
        {
            return PostYear;
        }

        public void setPostYear(String PostYear)
        {
            this.PostYear = PostYear;
        }

        public String getPostMonth()
        {
            return PostMonth;
        }

        public void setPostMonth(String PostMonth)
        {
            this.PostMonth = PostMonth;
        }

        public String getPostDay()
        {
            return PostDay;
        }

        public void setPostDay(String PostDay)
        {
            this.PostDay = PostDay;
        }

        public String getPostHour()
        {
            return PostHour;
        }

        public void setPostHour(String PostHour)
        {
            this.PostHour = PostHour;
        }

        public String getPostMinute()
        {
            return PostMinute;
        }

        public void setPostMinute(String PostMinute)
        {
            this.PostMinute = PostMinute;
        }

        public boolean isHelp()
        {
            return Help;
        }

        public void setHelp(boolean Help)
        {
            this.Help = Help;
        }

        public int getHelpType()
        {
            return HelpType;
        }

        public void setHelpType(int HelpType)
        {
            this.HelpType = HelpType;
        }

        public boolean isIsLiked()
        {
            return IsLiked;
        }

        public void setIsLiked(boolean IsLiked)
        {
            this.IsLiked = IsLiked;
        }

        public int getLikeCounts()
        {
            return LikeCounts;
        }

        public void setLikeCounts(int LikeCounts)
        {
            this.LikeCounts = LikeCounts;
        }

        public int getCommentCounts()
        {
            return CommentCounts;
        }

        public void setCommentCounts(int CommentCounts)
        {
            this.CommentCounts = CommentCounts;
        }

        public Object getCreateDatetime()
        {
            return CreateDatetime;
        }

        public void setCreateDatetime(Object CreateDatetime)
        {
            this.CreateDatetime = CreateDatetime;
        }

        public Object getMessage()
        {
            return Message;
        }

        public void setMessage(Object Message)
        {
            this.Message = Message;
        }

        public Object getPlace()
        {
            return Place;
        }

        public void setPlace(Object Place)
        {
            this.Place = Place;
        }

        public Object getImages()
        {
            return Images;
        }

        public void setImages(Object Images)
        {
            this.Images = Images;
        }

        public Object getTagDetails()
        {
            return TagDetails;
        }

        public void setTagDetails(Object TagDetails)
        {
            this.TagDetails = TagDetails;
        }
    }
}
