package apimodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alpesh on 4/13/2017.
 */

public class FriendTagId {
    @SerializedName("StatusId")
    @Expose
    private String statusId;

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<FriendUserID> getFriendUserID() {
        return friendUserID;
    }

    public void setFriendUserID(List<FriendUserID> friendUserID) {
        this.friendUserID = friendUserID;
    }

    @SerializedName("UserID")
    @Expose
    private String userID;

    @SerializedName("FriendUserID")
    @Expose
    private List<FriendUserID> friendUserID = null;
    public static class FriendUserID{
        public String getFriendUserID() {
            return friendUserID;
        }

        public void setFriendUserID(String friendUserID) {
            this.friendUserID = friendUserID;
        }

        @SerializedName("FriendUserID")
        @Expose
        private String friendUserID;
    }
}
