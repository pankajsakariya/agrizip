package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 12/16/2016 at 4:29 PM.
 */

public class CommentsOnPost
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"StatusID":"7fbb63dc-8700-44ed-9325-8b3b1d84b045","StatusDetailID":"78263a6b-07c6-4d5b-8834-9f34bcf533ba","UserID":"abb92a99-37ef-430b-8796-01880b462083","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","Comment":"ok","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636177596124924993.jpeg","CreateDate":"2016-12-19T10:23:32.493","CommentLikeCount":0,"IsFriend":0,"CommentReplyDetail":[{"ReplyID":"8c1dee43-28a4-4d89-bde4-a3eae2e627fd","ReplyUserID":"abb92a99-37ef-430b-8796-01880b462083","ReplyUserName":"Rutvik Mehta","ReplyUserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","ReplyComment":"I am a reply :)","ReplyImage":"http://social.vinayakpharmaceuticals.com/UserImage/636177640730326752.jpeg","ReplyDate":"2016-12-19T11:37:00"}]},{"StatusID":"7fbb63dc-8700-44ed-9325-8b3b1d84b045","StatusDetailID":"d55c188e-0aa5-4a37-beb8-5b7303cf70a0","UserID":"abb92a99-37ef-430b-8796-01880b462083","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","Comment":"hey der","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636177596289350709.jpeg","CreateDate":"2016-12-19T10:23:48.937","CommentLikeCount":0,"IsFriend":0,"CommentReplyDetail":null},{"StatusID":"7fbb63dc-8700-44ed-9325-8b3b1d84b045","StatusDetailID":"a9ee29d8-8c30-41c9-a337-6caa752d84a0","UserID":"abb92a99-37ef-430b-8796-01880b462083","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","Comment":"this is mt comment","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636177596514617166.jpeg","CreateDate":"2016-12-19T10:24:11.463","CommentLikeCount":0,"IsFriend":0,"CommentReplyDetail":null},{"StatusID":"7fbb63dc-8700-44ed-9325-8b3b1d84b045","StatusDetailID":"6b4827d5-33d4-45df-b02b-dc593b5813de","UserID":"abb92a99-37ef-430b-8796-01880b462083","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","Comment":"the intended solely those of you to the intended we have been working in which you can get it done today I was thinking that we are in a few weeks back I have to go through it all in one place to live with me on Twitter follow us on Facebook platform pumps and motors with the intended recipient you are not the intended recipient you are not the intended only one who is in the intended only for use with a few weeks ago I was","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636177597306013727.jpeg","CreateDate":"2016-12-19T10:25:30.6","CommentLikeCount":0,"IsFriend":0,"CommentReplyDetail":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;


    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }


    public static class DataBean implements Comment
    {
        /**
         * StatusID : 7fbb63dc-8700-44ed-9325-8b3b1d84b045
         * StatusDetailID : 78263a6b-07c6-4d5b-8834-9f34bcf533ba
         * UserID : abb92a99-37ef-430b-8796-01880b462083
         * FullName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg
         * Comment : ok
         * Imagestr : http://social.vinayakpharmaceuticals.com/UserImage/636177596124924993.jpeg
         * CreateDate : 2016-12-19T10:23:32.493
         * CommentLikeCount : 0
         * IsFriend : 0
         * CommentReplyDetail : [{"ReplyID":"8c1dee43-28a4-4d89-bde4-a3eae2e627fd","ReplyUserID":"abb92a99-37ef-430b-8796-01880b462083","ReplyUserName":"Rutvik Mehta","ReplyUserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","ReplyComment":"I am a reply :)","ReplyImage":"http://social.vinayakpharmaceuticals.com/UserImage/636177640730326752.jpeg","ReplyDate":"2016-12-19T11:37:00"}]
         */

        @SerializedName("StatusID")
        private String StatusID;
        @SerializedName("StatusDetailID")
        private String StatusDetailID;
        @SerializedName("UserID")
        private String UserID;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("Comment")
        private String Comment;
        @SerializedName("Imagestr")
        private String Imagestr;
        @SerializedName("CreateDate")
        private String CreateDate;
        @SerializedName("CommentLikeCount")
        private int CommentLikeCount;
        @SerializedName("IsLiked")
        private boolean IsLiked;
        @SerializedName("IsFriend")
        private int IsFriend;
        @SerializedName("CommentReplyDetail")
        private List<CommentReplyDetailBean> CommentReplyDetail;
        private CommentOperationsListener commentOperationsListener;

        private Type commentType;

        public boolean isLiked()
        {
            return IsLiked;
        }

        public void setLiked(boolean liked)
        {
            IsLiked = liked;
        }

        public String getStatusID()
        {
            return StatusID;
        }

        public void setStatusID(String StatusID)
        {
            this.StatusID = StatusID;
        }

        @Override
        public List<CommentReplyDetailBean> getCommentReplyDetailForPost()
        {
            return CommentReplyDetail;
        }

        @Override
        public void setCommentReplyDetailForPost(List<CommentReplyDetailBean> commentReplyList)
        {
            this.CommentReplyDetail = commentReplyList;
        }

        @Override
        public List<HelpListComments.DataBean.CommentReplyDetailBean> getCommentReplyDetailForHelp()
        {
            //IGNORE
            return null;
        }

        @Override
        public void setCommentReplyDetailForHelp(List<HelpListComments.DataBean.CommentReplyDetailBean> commentReplyList)
        {
            //IGNORE
        }

        public String getStatusDetailID()
        {
            return StatusDetailID;
        }

        public void setStatusDetailID(String StatusDetailID)
        {
            this.StatusDetailID = StatusDetailID;
        }

        public String getUserID()
        {
            return UserID;
        }

        public void setUserID(String UserID)
        {
            this.UserID = UserID;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        @Override
        public Type getType()
        {
            return Type.POST;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getComment()
        {
            return Comment;
        }

        public void setComment(String Comment)
        {
            this.Comment = Comment;
        }

        public String getImagestr()
        {
            return Imagestr;
        }

        public void setImagestr(String Imagestr)
        {
            this.Imagestr = Imagestr;
        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public int getCommentLikeCount()
        {
            return CommentLikeCount;
        }

        public void setCommentLikeCount(int CommentLikeCount)
        {
            this.CommentLikeCount = CommentLikeCount;
        }

        public int getIsFriend()
        {
            return IsFriend;
        }

        public void setIsFriend(int IsFriend)
        {
            this.IsFriend = IsFriend;
        }

        public List<CommentsOnPost.DataBean.CommentReplyDetailBean> getCommentReplyDetail()
        {
            return CommentReplyDetail;
        }

        public CommentOperationsListener getCommentOperationsListener()
        {
            return commentOperationsListener;
        }

        public void setCommentOperationsListener(CommentOperationsListener commentOperationsListener)
        {
            this.commentOperationsListener = commentOperationsListener;
        }

        public static class CommentReplyDetailBean implements CommentReply
        {
            /**
             * ReplyID : 8c1dee43-28a4-4d89-bde4-a3eae2e627fd
             * ReplyUserID : abb92a99-37ef-430b-8796-01880b462083
             * ReplyUserName : Rutvik Mehta
             * ReplyUserImage : http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg
             * ReplyComment : I am a reply :)
             * ReplyImage : http://social.vinayakpharmaceuticals.com/UserImage/636177640730326752.jpeg
             * ReplyDate : 2016-12-19T11:37:00
             */


            CommentReplyOperationsListener commentReplyOperationsListener;
            @SerializedName("ReplyID")
            private String ReplyID;
            @SerializedName("ReplyUserID")
            private String ReplyUserID;
            @SerializedName("ReplyUserName")
            private String ReplyUserName;
            @SerializedName("ReplyUserImage")
            private String ReplyUserImage;
            @SerializedName("ReplyComment")
            private String ReplyComment;
            @SerializedName("ReplyImage")
            private String ReplyImage;
            @SerializedName("ReplyDate")
            private String ReplyDate;

            public CommentReplyOperationsListener getCommentReplyOperationsListener()
            {
                return commentReplyOperationsListener;
            }

            public void setCommentReplyOperationsListener(CommentReplyOperationsListener commentReplyOperationsListener)
            {
                this.commentReplyOperationsListener = commentReplyOperationsListener;
            }

            public String getReplyID()
            {
                return ReplyID;
            }

            public void setReplyID(String ReplyID)
            {
                this.ReplyID = ReplyID;
            }

            public String getReplyUserID()
            {
                return ReplyUserID;
            }

            public void setReplyUserID(String ReplyUserID)
            {
                this.ReplyUserID = ReplyUserID;
            }

            public String getReplyUserName()
            {
                return ReplyUserName;
            }

            public void setReplyUserName(String ReplyUserName)
            {
                this.ReplyUserName = ReplyUserName;
            }

            public String getReplyUserImage()
            {
                return ReplyUserImage;
            }

            public void setReplyUserImage(String ReplyUserImage)
            {
                this.ReplyUserImage = ReplyUserImage;
            }

            public String getReplyComment()
            {
                return ReplyComment;
            }

            public void setReplyComment(String ReplyComment)
            {
                this.ReplyComment = ReplyComment;
            }

            public String getReplyImage()
            {
                return ReplyImage;
            }

            public void setReplyImage(String ReplyImage)
            {
                this.ReplyImage = ReplyImage;
            }

            public String getReplyDate()
            {
                return ReplyDate;
            }

            public void setReplyDate(String ReplyDate)
            {
                this.ReplyDate = ReplyDate;
            }
        }


    }
}
