package apimodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by murtuzanalawala on 4/25/17.
 */


public class AppSettingModel {

    @SerializedName("SettingId")
    @Expose
    private String settingId;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("WallPost")
    @Expose
    private Boolean wallPost;
    @SerializedName("Messages")
    @Expose
    private Boolean messages;
    @SerializedName("Comments")
    @Expose
    private Boolean comments;
    @SerializedName("Friendrequest")
    @Expose
    private Boolean friendrequest;
    @SerializedName("PhotoTag")
    @Expose
    private Boolean photoTag;
    @SerializedName("EventInvites")
    @Expose
    private Boolean eventInvites;
    @SerializedName("AppRequest")
    @Expose
    private Boolean appRequest;
    @SerializedName("Groups")
    @Expose
    private Boolean groups;
    @SerializedName("PlaceTips")
    @Expose
    private Boolean placeTips;

    public String getSettingId() {
        return settingId;
    }

    public void setSettingId(String settingId) {
        this.settingId = settingId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getWallPost() {
        return wallPost;
    }

    public void setWallPost(Boolean wallPost) {
        this.wallPost = wallPost;
    }

    public Boolean getMessages() {
        return messages;
    }

    public void setMessages(Boolean messages) {
        this.messages = messages;
    }

    public Boolean getComments() {
        return comments;
    }

    public void setComments(Boolean comments) {
        this.comments = comments;
    }

    public Boolean getFriendrequest() {
        return friendrequest;
    }

    public void setFriendrequest(Boolean friendrequest) {
        this.friendrequest = friendrequest;
    }

    public Boolean getPhotoTag() {
        return photoTag;
    }

    public void setPhotoTag(Boolean photoTag) {
        this.photoTag = photoTag;
    }

    public Boolean getEventInvites() {
        return eventInvites;
    }

    public void setEventInvites(Boolean eventInvites) {
        this.eventInvites = eventInvites;
    }

    public Boolean getAppRequest() {
        return appRequest;
    }

    public void setAppRequest(Boolean appRequest) {
        this.appRequest = appRequest;
    }

    public Boolean getGroups() {
        return groups;
    }

    public void setGroups(Boolean groups) {
        this.groups = groups;
    }

    public Boolean getPlaceTips() {
        return placeTips;
    }

    public void setPlaceTips(Boolean placeTips) {
        this.placeTips = placeTips;
    }

}

