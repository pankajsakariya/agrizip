package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/23/2017 at 6:07 PM.
 */

public class CreateSupport
{


    /**
     * SupportId : 00000000-0000-0000-0000-000000000000
     * UserId : af2d428d-951a-4ac2-b419-566fb95693b7
     * Subject : sample string 2
     * Desciption : sample string 3
     * CreateDate : 2017-01-18T16:26:06.5645825+05:30
     * IsDelete : false
     */

    @SerializedName("SupportId")
    private String SupportId;
    @SerializedName("UserId")
    private String UserId;
    @SerializedName("Subject")
    private String Subject;
    @SerializedName("Desciption")
    private String Desciption;
    @SerializedName("CreateDate")
    private String CreateDate;
    @SerializedName("IsDelete")
    private boolean IsDelete;

    public String getSupportId()
    {
        return SupportId;
    }

    public void setSupportId(String SupportId)
    {
        this.SupportId = SupportId;
    }

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getSubject()
    {
        return Subject;
    }

    public void setSubject(String Subject)
    {
        this.Subject = Subject;
    }

    public String getDesciption()
    {
        return Desciption;
    }

    public void setDesciption(String Desciption)
    {
        this.Desciption = Desciption;
    }

    public String getCreateDate()
    {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate)
    {
        this.CreateDate = CreateDate;
    }

    public boolean isIsDelete()
    {
        return IsDelete;
    }

    public void setIsDelete(boolean IsDelete)
    {
        this.IsDelete = IsDelete;
    }
}
