package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 1/23/2017 at 6:37 PM.
 */

public class UserPrivacySettings
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : {"Privacylist":[{"PrivacyId":"4be8a1a6-cbd3-41d0-8959-2248daf0d72d","PrivacyName":"Friends"},{"PrivacyId":"bcfb2fef-17f0-4d16-86b4-ab0a2141449b","PrivacyName":"Only Me"},{"PrivacyId":"e19750dd-8bf4-4c73-bbc3-feb014dc0f65","PrivacyName":"Public"}],"UserId":"bb8ea28e-32c8-43ed-b8c1-d8166fa9d10c","Whoseefuturepost":"e19750dd-8bf4-4c73-bbc3-feb014dc0f65","Whoseemobile":"4be8a1a6-cbd3-41d0-8959-2248daf0d72d","Whoseeemail":"4be8a1a6-cbd3-41d0-8959-2248daf0d72d","WhoseeDOB":"4be8a1a6-cbd3-41d0-8959-2248daf0d72d"}
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private DataBean Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public DataBean getData()
    {
        return Data;
    }

    public void setData(DataBean Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * Privacylist : [{"PrivacyId":"4be8a1a6-cbd3-41d0-8959-2248daf0d72d","PrivacyName":"Friends"},{"PrivacyId":"bcfb2fef-17f0-4d16-86b4-ab0a2141449b","PrivacyName":"Only Me"},{"PrivacyId":"e19750dd-8bf4-4c73-bbc3-feb014dc0f65","PrivacyName":"Public"}]
         * UserId : bb8ea28e-32c8-43ed-b8c1-d8166fa9d10c
         * Whoseefuturepost : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
         * Whoseemobile : 4be8a1a6-cbd3-41d0-8959-2248daf0d72d
         * Whoseeemail : 4be8a1a6-cbd3-41d0-8959-2248daf0d72d
         * WhoseeDOB : 4be8a1a6-cbd3-41d0-8959-2248daf0d72d
         */

        @SerializedName("UserId")
        private String UserId;
        @SerializedName("Whoseefuturepost")
        private String Whoseefuturepost;
        @SerializedName("Whoseemobile")
        private String Whoseemobile;
        @SerializedName("Whoseeemail")
        private String Whoseeemail;
        @SerializedName("WhoseeDOB")
        private String WhoseeDOB;
        @SerializedName("Privacylist")
        private List<PrivacylistBean> Privacylist;

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getWhoseefuturepost()
        {
            return Whoseefuturepost;
        }

        public void setWhoseefuturepost(String Whoseefuturepost)
        {
            this.Whoseefuturepost = Whoseefuturepost;
        }

        public String getWhoseemobile()
        {
            return Whoseemobile;
        }

        public void setWhoseemobile(String Whoseemobile)
        {
            this.Whoseemobile = Whoseemobile;
        }

        public String getWhoseeemail()
        {
            return Whoseeemail;
        }

        public void setWhoseeemail(String Whoseeemail)
        {
            this.Whoseeemail = Whoseeemail;
        }

        public String getWhoseeDOB()
        {
            return WhoseeDOB;
        }

        public void setWhoseeDOB(String WhoseeDOB)
        {
            this.WhoseeDOB = WhoseeDOB;
        }

        public List<PrivacylistBean> getPrivacylist()
        {
            return Privacylist;
        }

        public void setPrivacylist(List<PrivacylistBean> Privacylist)
        {
            this.Privacylist = Privacylist;
        }

        public static class PrivacylistBean
        {
            /**
             * PrivacyId : 4be8a1a6-cbd3-41d0-8959-2248daf0d72d
             * PrivacyName : Friends
             */

            @SerializedName("PrivacyId")
            private String PrivacyId;
            @SerializedName("PrivacyName")
            private String PrivacyName;

            public String getPrivacyId()
            {
                return PrivacyId;
            }

            public void setPrivacyId(String PrivacyId)
            {
                this.PrivacyId = PrivacyId;
            }

            public String getPrivacyName()
            {
                return PrivacyName;
            }

            public void setPrivacyName(String PrivacyName)
            {
                this.PrivacyName = PrivacyName;
            }
        }
    }
}
