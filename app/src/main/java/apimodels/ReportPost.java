package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/24/2017 at 4:16 PM.
 */

public class ReportPost
{


    /**
     * ID : 00000000-0000-0000-0000-000000000000
     * UserId : 1134d07a-5780-4a0c-b4d8-f2a6f433e3e7
     * PostId : df530273-e1db-4d57-863c-17e215c9b571
     * ReportTitle : Report title
     * ReportDetail : write descrption
     * Createdate : 2017-01-24T15:18:16.6321478+05:30
     * IsPostRemove : false
     */

    @SerializedName("ID")
    private String ID;
    @SerializedName("UserId")
    private String UserId;
    @SerializedName("PostId")
    private String PostId;
    @SerializedName("ReportTitle")
    private String ReportTitle;
    @SerializedName("ReportDetail")
    private String ReportDetail;
    @SerializedName("Createdate")
    private String Createdate;
    @SerializedName("IsPostRemove")
    private boolean IsPostRemove;

    public String getID()
    {
        return ID;
    }

    public void setID(String ID)
    {
        this.ID = ID;
    }

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getPostId()
    {
        return PostId;
    }

    public void setPostId(String PostId)
    {
        this.PostId = PostId;
    }

    public String getReportTitle()
    {
        return ReportTitle;
    }

    public void setReportTitle(String ReportTitle)
    {
        this.ReportTitle = ReportTitle;
    }

    public String getReportDetail()
    {
        return ReportDetail;
    }

    public void setReportDetail(String ReportDetail)
    {
        this.ReportDetail = ReportDetail;
    }

    public String getCreatedate()
    {
        return Createdate;
    }

    public void setCreatedate(String Createdate)
    {
        this.Createdate = Createdate;
    }

    public boolean isIsPostRemove()
    {
        return IsPostRemove;
    }

    public void setIsPostRemove(boolean IsPostRemove)
    {
        this.IsPostRemove = IsPostRemove;
    }
}
