package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 1/20/2017 at 1:23 PM.
 */

public class HelpListComments
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"JobId":"47671186-45b0-4c71-a99f-b64e6604ed65","JobDetailID":"67c27df8-f614-466c-b37e-b955544247cc","UserID":"c7e5d577-3328-4a86-97e5-0192b9d352a0","FullName":"sachin","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","Comment":"tesy","Imagestr":"","CreateDate":"2017-01-18T05:47:25.823","CommentLikeCount":0,"IsLiked":false,"IsFriend":0,"CommentReplyDetail":[{"ReplyID":"cf8cfb51-10f4-4ff6-9e0b-82b79b619bab","ReplyUserID":"c7e5d577-3328-4a86-97e5-0192b9d352a0","ReplyUserName":"sachin","ReplyUserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","ReplyComment":"sachin","ReplyImage":"","ReplyDate":"2017-01-18T12:52:00"}]},{"JobId":"47671186-45b0-4c71-a99f-b64e6604ed65","JobDetailID":"fb0a57e6-971f-41f0-bced-4180d8c43be6","UserID":"c7e5d577-3328-4a86-97e5-0192b9d352a0","FullName":"sachin","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","Comment":"test","Imagestr":"","CreateDate":"2017-01-18T05:47:59.707","CommentLikeCount":0,"IsLiked":false,"IsFriend":0,"CommentReplyDetail":null},{"JobId":"47671186-45b0-4c71-a99f-b64e6604ed65","JobDetailID":"3ad24464-9ef9-4a4d-a7e5-1eb5e12877d1","UserID":"c7e5d577-3328-4a86-97e5-0192b9d352a0","FullName":"sachin","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","Comment":"test1","Imagestr":"","CreateDate":"2017-01-18T12:48:23.917","CommentLikeCount":0,"IsLiked":false,"IsFriend":0,"CommentReplyDetail":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean implements Comment
    {
        public CommentOperationsListener commentOperationsListener;
        /**
         * JobId : 47671186-45b0-4c71-a99f-b64e6604ed65
         * JobDetailID : 67c27df8-f614-466c-b37e-b955544247cc
         * UserID : c7e5d577-3328-4a86-97e5-0192b9d352a0
         * FullName : sachin
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg
         * Comment : tesy
         * Imagestr :
         * CreateDate : 2017-01-18T05:47:25.823
         * CommentLikeCount : 0
         * IsLiked : false
         * IsFriend : 0
         * CommentReplyDetail : [{"ReplyID":"cf8cfb51-10f4-4ff6-9e0b-82b79b619bab","ReplyUserID":"c7e5d577-3328-4a86-97e5-0192b9d352a0","ReplyUserName":"sachin","ReplyUserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","ReplyComment":"sachin","ReplyImage":"","ReplyDate":"2017-01-18T12:52:00"}]
         */

        @SerializedName("JobId")
        private String JobId;
        @SerializedName("JobDetailID")
        private String JobDetailID;
        @SerializedName("UserID")
        private String UserID;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("Comment")
        private String Comment;
        @SerializedName("Imagestr")
        private String Imagestr;
        @SerializedName("CreateDate")
        private String CreateDate;
        @SerializedName("CommentLikeCount")
        private int CommentLikeCount;
        @SerializedName("IsLiked")
        private boolean IsLiked;
        @SerializedName("IsFriend")
        private int IsFriend;
        @SerializedName("CommentReplyDetail")
        private List<CommentReplyDetailBean> CommentReplyDetail;

        public String getJobId()
        {
            return JobId;
        }

        public void setJobId(String JobId)
        {
            this.JobId = JobId;
        }

        public String getJobDetailID()
        {
            return JobDetailID;
        }

        public void setJobDetailID(String JobDetailID)
        {
            this.JobDetailID = JobDetailID;
        }

        public String getUserID()
        {
            return UserID;
        }

        public void setUserID(String UserID)
        {
            this.UserID = UserID;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        @Override
        public Type getType()
        {
            return Type.HELP;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getComment()
        {
            return Comment;
        }

        public void setComment(String Comment)
        {
            this.Comment = Comment;
        }

        @Override
        public String getStatusDetailID()
        {
            return JobDetailID;
        }

        public String getImagestr()
        {
            return Imagestr;
        }

        public void setImagestr(String Imagestr)
        {
            this.Imagestr = Imagestr;
        }

        @Override
        public boolean isLiked()
        {
            return false;
        }

        @Override
        public void setLiked(boolean liked)
        {

        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public int getCommentLikeCount()
        {
            return CommentLikeCount;
        }

        public void setCommentLikeCount(int CommentLikeCount)
        {
            this.CommentLikeCount = CommentLikeCount;
        }

        @Override
        public String getStatusID()
        {
            return JobId;
        }

        @Override
        public List<CommentsOnPost.DataBean.CommentReplyDetailBean> getCommentReplyDetailForPost()
        {
            //IGNORE
            return null;
        }

        @Override
        public void setCommentReplyDetailForPost(List<CommentsOnPost.DataBean.CommentReplyDetailBean> commentReplyList)
        {
            //IGONER
        }

        @Override
        public List<CommentReplyDetailBean> getCommentReplyDetailForHelp()
        {
            return CommentReplyDetail;
        }

        @Override
        public void setCommentReplyDetailForHelp(List<CommentReplyDetailBean> commentReplyList)
        {
            this.CommentReplyDetail = commentReplyList;
        }

        public boolean isIsLiked()
        {
            return IsLiked;
        }

        public void setIsLiked(boolean IsLiked)
        {
            this.IsLiked = IsLiked;
        }

        public int getIsFriend()
        {
            return IsFriend;
        }

        public void setIsFriend(int IsFriend)
        {
            this.IsFriend = IsFriend;
        }

        public List<CommentReplyDetailBean> getCommentReplyDetail()
        {
            return CommentReplyDetail;
        }

        public void setCommentReplyDetail(List<CommentReplyDetailBean> CommentReplyDetail)
        {
            this.CommentReplyDetail = CommentReplyDetail;
        }

        @Override
        public CommentOperationsListener getCommentOperationsListener()
        {
            return commentOperationsListener;
        }

        @Override
        public void setCommentOperationsListener(CommentOperationsListener commentOperationsListener)
        {
            this.commentOperationsListener = commentOperationsListener;
        }

        public static class CommentReplyDetailBean implements CommentReply
        {
            /**
             * ReplyID : cf8cfb51-10f4-4ff6-9e0b-82b79b619bab
             * ReplyUserID : c7e5d577-3328-4a86-97e5-0192b9d352a0
             * ReplyUserName : sachin
             * ReplyUserImage : http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg
             * ReplyComment : sachin
             * ReplyImage :
             * ReplyDate : 2017-01-18T12:52:00
             */

            @SerializedName("ReplyID")
            private String ReplyID;
            @SerializedName("ReplyUserID")
            private String ReplyUserID;
            @SerializedName("ReplyUserName")
            private String ReplyUserName;
            @SerializedName("ReplyUserImage")
            private String ReplyUserImage;
            @SerializedName("ReplyComment")
            private String ReplyComment;
            @SerializedName("ReplyImage")
            private String ReplyImage;
            @SerializedName("ReplyDate")
            private String ReplyDate;

            private CommentReplyOperationsListener commentReplyOperationsListener;

            @Override
            public CommentReplyOperationsListener getCommentReplyOperationsListener()
            {
                return commentReplyOperationsListener;
            }

            @Override
            public void setCommentReplyOperationsListener(CommentReplyOperationsListener commentReplyOperationsListener)
            {
                this.commentReplyOperationsListener = commentReplyOperationsListener;
            }

            public String getReplyID()
            {
                return ReplyID;
            }

            public void setReplyID(String ReplyID)
            {
                this.ReplyID = ReplyID;
            }

            public String getReplyUserID()
            {
                return ReplyUserID;
            }

            public void setReplyUserID(String ReplyUserID)
            {
                this.ReplyUserID = ReplyUserID;
            }

            public String getReplyUserName()
            {
                return ReplyUserName;
            }

            public void setReplyUserName(String ReplyUserName)
            {
                this.ReplyUserName = ReplyUserName;
            }

            public String getReplyUserImage()
            {
                return ReplyUserImage;
            }

            public void setReplyUserImage(String ReplyUserImage)
            {
                this.ReplyUserImage = ReplyUserImage;
            }

            public String getReplyComment()
            {
                return ReplyComment;
            }

            public void setReplyComment(String ReplyComment)
            {
                this.ReplyComment = ReplyComment;
            }

            public String getReplyImage()
            {
                return ReplyImage;
            }

            public void setReplyImage(String ReplyImage)
            {
                this.ReplyImage = ReplyImage;
            }

            public String getReplyDate()
            {
                return ReplyDate;
            }

            public void setReplyDate(String ReplyDate)
            {
                this.ReplyDate = ReplyDate;
            }
        }
    }
}
