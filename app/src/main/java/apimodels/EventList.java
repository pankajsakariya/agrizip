package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 1/18/2017 at 3:28 PM.
 */

public class EventList
{

    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"EventId":"00000000-0000-0000-0000-000000000000","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","UserName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","EventImage":"http://social.vinayakpharmaceuticals.com/UserImage/636203500492416392.jpeg","EventName":"Hug day","Host":"Hug boy","ContactPerson":"9409210488","Date":"20-1-2017","Time":"6:30 AM","Location":"Hug hall","Address":"Hug street","Description":"celebrating hug day and I will be in the intended recipient you are not the intended recipient you are not the intended only one who are interested and I will send you a lot to do you think you are interested in the future please unsubscribe me from friends and family members to attend the","CreateDate":null,"UpdateDate":null,"IsDelete":false}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * EventId : 00000000-0000-0000-0000-000000000000
         * UserId : cd4e1de6-fecd-4a27-acad-b1b244adca93
         * UserName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg
         * EventImage : http://social.vinayakpharmaceuticals.com/UserImage/636203500492416392.jpeg
         * EventName : Hug day
         * Host : Hug boy
         * ContactPerson : 9409210488
         * Date : 20-1-2017
         * Time : 6:30 AM
         * Location : Hug hall
         * Address : Hug street
         * Description : celebrating hug day and I will be in the intended recipient you are not the intended recipient you are not the intended only one who are interested and I will send you a lot to do you think you are interested in the future please unsubscribe me from friends and family members to attend the
         * CreateDate : null
         * UpdateDate : null
         * IsDelete : false
         */

        @SerializedName("EventId")
        private String EventId;
        @SerializedName("UserId")
        private String UserId;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("EventImage")
        private String EventImage;
        @SerializedName("EventName")
        private String EventName;
        @SerializedName("Host")
        private String Host;
        @SerializedName("ContactPerson")
        private String ContactPerson;
        @SerializedName("Date")
        private String Date;
        @SerializedName("Time")
        private String Time;
        @SerializedName("Location")
        private String Location;
        @SerializedName("Address")
        private String Address;
        @SerializedName("Description")
        private String Description;
        @SerializedName("CreateDate")
        private Object CreateDate;
        @SerializedName("UpdateDate")
        private Object UpdateDate;
        @SerializedName("IsDelete")
        private boolean IsDelete;

        public String getEventId()
        {
            return EventId;
        }

        public void setEventId(String EventId)
        {
            this.EventId = EventId;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getUserName()
        {
            return UserName;
        }

        public void setUserName(String UserName)
        {
            this.UserName = UserName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getEventImage()
        {
            return EventImage;
        }

        public void setEventImage(String EventImage)
        {
            this.EventImage = EventImage;
        }

        public String getEventName()
        {
            return EventName;
        }

        public void setEventName(String EventName)
        {
            this.EventName = EventName;
        }

        public String getHost()
        {
            return Host;
        }

        public void setHost(String Host)
        {
            this.Host = Host;
        }

        public String getContactPerson()
        {
            return ContactPerson;
        }

        public void setContactPerson(String ContactPerson)
        {
            this.ContactPerson = ContactPerson;
        }

        public String getDate()
        {
            return Date;
        }

        public void setDate(String Date)
        {
            this.Date = Date;
        }

        public String getTime()
        {
            return Time;
        }

        public void setTime(String Time)
        {
            this.Time = Time;
        }

        public String getLocation()
        {
            return Location;
        }

        public void setLocation(String Location)
        {
            this.Location = Location;
        }

        public String getAddress()
        {
            return Address;
        }

        public void setAddress(String Address)
        {
            this.Address = Address;
        }

        public String getDescription()
        {
            return Description;
        }

        public void setDescription(String Description)
        {
            this.Description = Description;
        }

        public Object getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(Object CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public Object getUpdateDate()
        {
            return UpdateDate;
        }

        public void setUpdateDate(Object UpdateDate)
        {
            this.UpdateDate = UpdateDate;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }
    }
}
