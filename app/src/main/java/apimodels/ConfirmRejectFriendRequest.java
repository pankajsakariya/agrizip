package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 11/29/2016 at 5:48 PM.
 */

public class ConfirmRejectFriendRequest
{


    /**
     * Id : 386b1cc6-0803-40d0-aa64-bdced4a7275a
     * UserId : c6683359-fefe-4dff-8312-996ab23c6386
     * FriendUserId : 87d9dc69-bd1f-4cb5-af51-f355cc0804ab
     */

    @SerializedName("Id")
    private String Id;
    @SerializedName("UserId")
    private String UserId;
    @SerializedName("FriendUserId")
    private String FriendUserId;

    public String getId()
    {
        return Id;
    }

    public void setId(String Id)
    {
        this.Id = Id;
    }

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getFriendUserId()
    {
        return FriendUserId;
    }

    public void setFriendUserId(String FriendUserId)
    {
        this.FriendUserId = FriendUserId;
    }
}
