package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/4/2017 at 12:43 PM.
 */

public class CreatePhotoAlbum
{


    /**
     * UserId : 089080dd-5cf6-410b-9b3a-cf6c5d8e9d71
     * AlbumID : 00000000-0000-0000-0000-000000000000 //no need
     * AlbumName : Villege Trip
     * AlbumDescription : Enjoying
     * AlbumVisibleTo : 07e03b2e-f073-4764-b214-2ff01f690ab8
     */

    @SerializedName("UserId")
    private String UserId;
    @SerializedName("AlbumID")
    private String AlbumID;
    @SerializedName("AlbumName")
    private String AlbumName;
    @SerializedName("AlbumDescription")
    private String AlbumDescription;
    @SerializedName("AlbumVisibleTo")
    private String AlbumVisibleTo;

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getAlbumID()
    {
        return AlbumID;
    }

    public void setAlbumID(String AlbumID)
    {
        this.AlbumID = AlbumID;
    }

    public String getAlbumName()
    {
        return AlbumName;
    }

    public void setAlbumName(String AlbumName)
    {
        this.AlbumName = AlbumName;
    }

    public String getAlbumDescription()
    {
        return AlbumDescription;
    }

    public void setAlbumDescription(String AlbumDescription)
    {
        this.AlbumDescription = AlbumDescription;
    }

    public String getAlbumVisibleTo()
    {
        return AlbumVisibleTo;
    }

    public void setAlbumVisibleTo(String AlbumVisibleTo)
    {
        this.AlbumVisibleTo = AlbumVisibleTo;
    }
}
