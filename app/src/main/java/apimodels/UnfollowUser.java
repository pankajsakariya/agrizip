package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/3/2017 at 6:02 PM.
 */

public class UnfollowUser
{


    /**
     * FollowupId : 00000000-0000-0000-0000-000000000000
     * UserId : 227b0e88-631b-45ef-9792-313140683355
     * FollowUserId : d714d23c-bed8-465f-8e80-1579937bd5b7
     * CreateDate : 2017-01-03T12:18:04.5761173+05:30
     * UpdateDate : 2017-01-03T12:18:04.5761173+05:30
     * IsFollow : true
     */

    @SerializedName("FollowupId")
    private String FollowupId;
    @SerializedName("UserId")
    private String UserId;
    @SerializedName("FollowUserId")
    private String FollowUserId;
    @SerializedName("CreateDate")
    private String CreateDate;
    @SerializedName("UpdateDate")
    private String UpdateDate;
    @SerializedName("IsFollow")
    private boolean IsFollow;

    public String getFollowupId()
    {
        return FollowupId;
    }

    public void setFollowupId(String FollowupId)
    {
        this.FollowupId = FollowupId;
    }

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getFollowUserId()
    {
        return FollowUserId;
    }

    public void setFollowUserId(String FollowUserId)
    {
        this.FollowUserId = FollowUserId;
    }

    public String getCreateDate()
    {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate)
    {
        this.CreateDate = CreateDate;
    }

    public String getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(String UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public boolean isIsFollow()
    {
        return IsFollow;
    }

    public void setIsFollow(boolean IsFollow)
    {
        this.IsFollow = IsFollow;
    }
}
