package apimodels;

import com.google.gson.annotations.SerializedName;
import com.quickblox.users.model.QBUser;

/**
 * Created by rutvik on 11/21/2016 at 5:28 PM.
 */

public class UserInfo
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : {"Id":"174e4d9b-b273-4312-ae81-ff9e7bf9071b","FullName":"Avani Patel","DeviceID":"aa9eb1e543fc6403","MobileNumber":"1234567890","UserImage":"/9j/4AAQSkZJRgABAQEASABIAAD/4QCORXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAZCgAwAEAAAAAQAAAfQAAAAAAAD/2wBDAAEBAQEBAQIBAQIDAgICAwUDAwMDBQYFBQUFBQYHBgYGBgYGBwcHBwcHBwcICAgICAgKCgoKCgsLCwsLCwsLCwv/2wBDAQICAgMDAwUDAwULCAYICwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwv/wAARCAH0AZADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3.....................","State":"37e5cd0c-c8af-44d9-913a-dcc88ea8355a","District":"158cb588-fcf1-4f14-afcc-b7f1261ec427","Language":"English","Dob":null,"Gender":null,"EmailAddress":null,"WhoSeeMyStuff":null,"WhoContactMe":null,"LookMeByEmail":null,"LookMeByMobile":null,"AddToMyTimeline":null,"WhoSeeTaggedTimeline":null,"FolowMe":null,"PostComment":null,"ProfileInfo":null,"CreateDate":"2016-11-21T17:10:44.483","UpdateDate":"2016-11-21T17:13:26.4119425+05:30","LastLoginDate":null,"IsApproved":true,"FailedPasswordAttemptCount":0,"MobilePIN":"509577","IsActive":true,"IsDelete":false}
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private User Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public User getData()
    {
        return Data;
    }

    public void User(User Data)
    {
        this.Data = Data;
    }

    public interface UserEventCallbacks
    {
        void onStatusPostSuccessful(SinglePost singlePost);

        void onStatusPostEdit(SinglePost singlePost);

        void onStatusDeleted(String statusId);

    }

    public static class User
    {

        UserPrivacyDetails userPrivacyDetails;
        private UserEventCallbacks userEventCallbacks;
        /**
         * Id : abb92a99-37ef-430b-8796-01880b462083
         * FullName : Rutvik Mehta
         * DeviceID : aa9eb1e543fc6403
         * MobileNumber : 9409210488
         * UserImage : C:\Inetpub\vhosts\vinayakpharmaceuticals.com\social.vinayakpharmaceuticals.com\UserImage\636154152743906912.jpeg
         * Country : 8f68693b-e1ca-4217-bab3-1522cb6792da
         * State : e0cfbdfe-8560-4980-a0cd-ced2862ff133
         * StateName : null
         * District : 10f0c29f-c790-417c-9550-59655816fdf8
         * DistrictName : null
         * Language : null
         * Dob : null
         * Gender : null
         * EmailAddress : null
         * WhoSeeMyStuff : 1
         * WhoContactMe : 1
         * LookMeByEmail : 1
         * LookMeByMobile : 1
         * AddToMyTimeline : 1
         * WhoSeeTaggedTimeline : 1
         * FollowMe : 1
         * PostComment : 1
         * ProfileInfo : 1
         * CreateDate : 2016-11-22T12:40:43.627
         * UpdateDate : 2016-11-22T12:41:14.407
         * LastLoginDate : null
         * IsApproved : true
         * FailedPasswordAttemptCount : 0
         * MobilePIN : 129823
         * IsActive : true
         * IsDelete : false
         */

        private boolean showNeedHelpInWhatsOnYourMind;
        @SerializedName("Id")
        private String Id;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("DeviceID")
        private String DeviceID;
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @SerializedName("InstanceID")
        private String instanceID;

        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("Country")
        private String Country;
        @SerializedName("State")
        private String State;
        @SerializedName("StateName")
        private Object StateName;
        @SerializedName("District")
        private String District;
        @SerializedName("DistrictName")
        private Object DistrictName;
        @SerializedName("Language")
        private Object Language;
        @SerializedName("Dob")
        private Object Dob;
        @SerializedName("Gender")
        private Object Gender;
        @SerializedName("EmailAddress")
        private Object EmailAddress;
        @SerializedName("WhoSeeMyStuff")
        private String WhoSeeMyStuff;
        @SerializedName("WhoContactMe")
        private String WhoContactMe;
        @SerializedName("LookMeByEmail")
        private String LookMeByEmail;
        @SerializedName("LookMeByMobile")
        private String LookMeByMobile;
        @SerializedName("AddToMyTimeline")
        private String AddToMyTimeline;
        @SerializedName("WhoSeeTaggedTimeline")
        private String WhoSeeTaggedTimeline;
        @SerializedName("FollowMe")
        private String FollowMe;
        @SerializedName("PostComment")
        private String PostComment;
        @SerializedName("ProfileInfo")
        private String ProfileInfo;
        @SerializedName("CreateDate")
        private String CreateDate;
        @SerializedName("UpdateDate")
        private String UpdateDate;
        @SerializedName("LastLoginDate")
        private Object LastLoginDate;
        @SerializedName("IsApproved")
        private boolean IsApproved;
        @SerializedName("FailedPasswordAttemptCount")
        private int FailedPasswordAttemptCount;
        @SerializedName("MobilePIN")
        private String MobilePIN;
        @SerializedName("IsActive")
        private boolean IsActive;
        @SerializedName("IsDelete")
        private boolean IsDelete;
        @SerializedName("OTP")
        private String OTP;



        public String getOTP() {
            return OTP;
        }

        public void setOTP(String OTP) {
            this.OTP = OTP;
        }


        public int getIsAlreadyUser() {
            return isAlreadyUser;
        }

        public void setIsAlreadyUser(int isAlreadyUser) {
            this.isAlreadyUser = isAlreadyUser;
        }

        @SerializedName("isAlreadyUser")
        private int isAlreadyUser;

        private QBUser quickBloxUser;

        public boolean isShowNeedHelpInWhatsOnYourMind()
        {
            return showNeedHelpInWhatsOnYourMind;
        }

        public void setShowNeedHelpInWhatsOnYourMind(boolean showNeedHelpInWhatsOnYourMind)
        {
            this.showNeedHelpInWhatsOnYourMind = showNeedHelpInWhatsOnYourMind;
        }

        public QBUser getQuickBloxUser()
        {
            return quickBloxUser;
        }

        public void setQuickBloxUser(QBUser quickBloxUser)
        {
            this.quickBloxUser = quickBloxUser;
        }

        public UserPrivacyDetails getUserPrivacyDetails()
        {
            return userPrivacyDetails;
        }

        public void setUserPrivacyDetails(UserPrivacyDetails userPrivacyDetails)
        {
            this.userPrivacyDetails = userPrivacyDetails;
        }

        public String getId()
        {
            return Id;
        }

        public void setId(String Id)
        {
            this.Id = Id;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getDeviceID()
        {
            return DeviceID;
        }

        public void setDeviceID(String DeviceID)
        {
            this.DeviceID = DeviceID;
        }

        public String getMobileNumber()
        {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber)
        {
            this.MobileNumber = MobileNumber;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getCountry()
        {
            return Country;
        }

        public void setCountry(String Country)
        {
            this.Country = Country;
        }

        public String getState()
        {
            return State;
        }

        public void setState(String State)
        {
            this.State = State;
        }

        public Object getStateName()
        {
            return StateName;
        }

        public void setStateName(Object StateName)
        {
            this.StateName = StateName;
        }

        public String getDistrict()
        {
            return District;
        }

        public void setDistrict(String District)
        {
            this.District = District;
        }

        public Object getDistrictName()
        {
            return DistrictName;
        }

        public void setDistrictName(Object DistrictName)
        {
            this.DistrictName = DistrictName;
        }

        public Object getLanguage()
        {
            return Language;
        }

        public void setLanguage(Object Language)
        {
            this.Language = Language;
        }

        public Object getDob()
        {
            return Dob;
        }

        public void setDob(Object Dob)
        {
            this.Dob = Dob;
        }

        public Object getGender()
        {
            return Gender;
        }

        public void setGender(Object Gender)
        {
            this.Gender = Gender;
        }

        public Object getEmailAddress()
        {
            return EmailAddress;
        }

        public void setEmailAddress(Object EmailAddress)
        {
            this.EmailAddress = EmailAddress;
        }

        public String getWhoSeeMyStuff()
        {
            return WhoSeeMyStuff;
        }

        public void setWhoSeeMyStuff(String WhoSeeMyStuff)
        {
            this.WhoSeeMyStuff = WhoSeeMyStuff;
        }

        public String getWhoContactMe()
        {
            return WhoContactMe;
        }

        public void setWhoContactMe(String WhoContactMe)
        {
            this.WhoContactMe = WhoContactMe;
        }

        public String getLookMeByEmail()
        {
            return LookMeByEmail;
        }

        public void setLookMeByEmail(String LookMeByEmail)
        {
            this.LookMeByEmail = LookMeByEmail;
        }

        public String getLookMeByMobile()
        {
            return LookMeByMobile;
        }

        public void setLookMeByMobile(String LookMeByMobile)
        {
            this.LookMeByMobile = LookMeByMobile;
        }

        public String getAddToMyTimeline()
        {
            return AddToMyTimeline;
        }

        public void setAddToMyTimeline(String AddToMyTimeline)
        {
            this.AddToMyTimeline = AddToMyTimeline;
        }

        public String getWhoSeeTaggedTimeline()
        {
            return WhoSeeTaggedTimeline;
        }

        public void setWhoSeeTaggedTimeline(String WhoSeeTaggedTimeline)
        {
            this.WhoSeeTaggedTimeline = WhoSeeTaggedTimeline;
        }

        public String getFollowMe()
        {
            return FollowMe;
        }

        public void setFollowMe(String FollowMe)
        {
            this.FollowMe = FollowMe;
        }

        public String getPostComment()
        {
            return PostComment;
        }

        public void setPostComment(String PostComment)
        {
            this.PostComment = PostComment;
        }

        public String getProfileInfo()
        {
            return ProfileInfo;
        }

        public void setProfileInfo(String ProfileInfo)
        {
            this.ProfileInfo = ProfileInfo;
        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public String getUpdateDate()
        {
            return UpdateDate;
        }

        public void setUpdateDate(String UpdateDate)
        {
            this.UpdateDate = UpdateDate;
        }

        public Object getLastLoginDate()
        {
            return LastLoginDate;
        }

        public void setLastLoginDate(Object LastLoginDate)
        {
            this.LastLoginDate = LastLoginDate;
        }

        public boolean isIsApproved()
        {
            return IsApproved;
        }

        public void setIsApproved(boolean IsApproved)
        {
            this.IsApproved = IsApproved;
        }

        public int getFailedPasswordAttemptCount()
        {
            return FailedPasswordAttemptCount;
        }

        public void setFailedPasswordAttemptCount(int FailedPasswordAttemptCount)
        {
            this.FailedPasswordAttemptCount = FailedPasswordAttemptCount;
        }

        public String getMobilePIN()
        {
            return MobilePIN;
        }

        public void setMobilePIN(String MobilePIN)
        {
            this.MobilePIN = MobilePIN;
        }

        public boolean isIsActive()
        {
            return IsActive;
        }

        public void setIsActive(boolean IsActive)
        {
            this.IsActive = IsActive;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }

        public UserEventCallbacks getUserEventCallbacks()
        {
            return userEventCallbacks;
        }

        public void setUserEventCallbacks(UserEventCallbacks userEventCallbacks)
        {
            this.userEventCallbacks = userEventCallbacks;
        }
    }

}
