package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 2/21/2017 at 3:23 PM.
 */

public class EditedPost
{

    /**
     * StatusId : 0491d9a4-1d8a-4a17-aecb-a894539291df
     * Status : say something about post
     */

    @SerializedName("StatusId")
    private String StatusId;
    @SerializedName("Status")
    private String Status;

    public String getStatusId()
    {
        return StatusId;
    }

    public void setStatusId(String StatusId)
    {
        this.StatusId = StatusId;
    }

    public String getStatus()
    {
        return Status;
    }

    public void setStatus(String Status)
    {
        this.Status = Status;
    }
}
