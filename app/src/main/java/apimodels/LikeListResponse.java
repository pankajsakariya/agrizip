package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 2/11/2017 at 6:05 PM.
 */

public class LikeListResponse
{

    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"StatusID":null,"StatusDetailID":null,"ShareID":null,"ShareDetailID":null,"UserID":"7f47b9c3-2f1e-424d-9947-e5896bc75a63","FullName":"","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636221789040573456.jpeg","Comment":null,"Imagestr":null,"CreateDate":null,"CommentLikeCount":0,"IsLiked":false,"IsFriend":1,"CommentReplyDetail":null},{"StatusID":null,"StatusDetailID":null,"ShareID":null,"ShareDetailID":null,"UserID":"7f47b9c3-2f1e-424d-9947-e5896bc75a63","FullName":"","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636221789040573456.jpeg","Comment":null,"Imagestr":null,"CreateDate":null,"CommentLikeCount":0,"IsLiked":false,"IsFriend":1,"CommentReplyDetail":null},{"StatusID":null,"StatusDetailID":null,"ShareID":null,"ShareDetailID":null,"UserID":"7f47b9c3-2f1e-424d-9947-e5896bc75a63","FullName":"","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636221789040573456.jpeg","Comment":null,"Imagestr":null,"CreateDate":null,"CommentLikeCount":0,"IsLiked":false,"IsFriend":1,"CommentReplyDetail":null},{"StatusID":null,"StatusDetailID":null,"ShareID":null,"ShareDetailID":null,"UserID":"7f47b9c3-2f1e-424d-9947-e5896bc75a63","FullName":"","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636221789040573456.jpeg","Comment":null,"Imagestr":null,"CreateDate":null,"CommentLikeCount":0,"IsLiked":false,"IsFriend":1,"CommentReplyDetail":null},{"StatusID":null,"StatusDetailID":null,"ShareID":null,"ShareDetailID":null,"UserID":"7f47b9c3-2f1e-424d-9947-e5896bc75a63","FullName":"","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636221789040573456.jpeg","Comment":null,"Imagestr":null,"CreateDate":null,"CommentLikeCount":0,"IsLiked":false,"IsFriend":1,"CommentReplyDetail":null},{"StatusID":null,"StatusDetailID":null,"ShareID":null,"ShareDetailID":null,"UserID":"7f47b9c3-2f1e-424d-9947-e5896bc75a63","FullName":"","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636221789040573456.jpeg","Comment":null,"Imagestr":null,"CreateDate":null,"CommentLikeCount":0,"IsLiked":false,"IsFriend":1,"CommentReplyDetail":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * StatusID : null
         * StatusDetailID : null
         * ShareID : null
         * ShareDetailID : null
         * UserID : 7f47b9c3-2f1e-424d-9947-e5896bc75a63
         * FullName :
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636221789040573456.jpeg
         * Comment : null
         * Imagestr : null
         * CreateDate : null
         * CommentLikeCount : 0
         * IsLiked : false
         * IsFriend : 1
         * CommentReplyDetail : null
         */

        @SerializedName("StatusID")
        private Object StatusID;
        @SerializedName("StatusDetailID")
        private Object StatusDetailID;
        @SerializedName("ShareID")
        private Object ShareID;
        @SerializedName("ShareDetailID")
        private Object ShareDetailID;
        @SerializedName("UserID")
        private String UserID;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("Comment")
        private Object Comment;
        @SerializedName("Imagestr")
        private Object Imagestr;
        @SerializedName("CreateDate")
        private Object CreateDate;
        @SerializedName("CommentLikeCount")
        private int CommentLikeCount;
        @SerializedName("IsLiked")
        private boolean IsLiked;
        @SerializedName("IsFriend")
        private int IsFriend;

        @SerializedName("MutualFriendCount")
        private int MutualFriendCount;
        @SerializedName("CommentReplyDetail")
        private Object CommentReplyDetail;

        public Object getStatusID()
        {
            return StatusID;
        }

        public void setStatusID(Object StatusID)
        {
            this.StatusID = StatusID;
        }

        public Object getStatusDetailID()
        {
            return StatusDetailID;
        }

        public void setStatusDetailID(Object StatusDetailID)
        {
            this.StatusDetailID = StatusDetailID;
        }

        public int getMutualFriendCount() {
            return MutualFriendCount;
        }

        public void setMutualFriendCount(int mutualFriendCount) {
            MutualFriendCount = mutualFriendCount;
        }


        public Object getShareID()
        {
            return ShareID;
        }

        public void setShareID(Object ShareID)
        {
            this.ShareID = ShareID;
        }

        public Object getShareDetailID()
        {
            return ShareDetailID;
        }

        public void setShareDetailID(Object ShareDetailID)
        {
            this.ShareDetailID = ShareDetailID;
        }

        public String getUserID()
        {
            return UserID;
        }

        public void setUserID(String UserID)
        {
            this.UserID = UserID;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public Object getComment()
        {
            return Comment;
        }

        public void setComment(Object Comment)
        {
            this.Comment = Comment;
        }

        public Object getImagestr()
        {
            return Imagestr;
        }

        public void setImagestr(Object Imagestr)
        {
            this.Imagestr = Imagestr;
        }

        public Object getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(Object CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public int getCommentLikeCount()
        {
            return CommentLikeCount;
        }

        public void setCommentLikeCount(int CommentLikeCount)
        {
            this.CommentLikeCount = CommentLikeCount;
        }

        public boolean isIsLiked()
        {
            return IsLiked;
        }

        public void setIsLiked(boolean IsLiked)
        {
            this.IsLiked = IsLiked;
        }

        public int getIsFriend()
        {
            return IsFriend;
        }

        public void setIsFriend(int IsFriend)
        {
            this.IsFriend = IsFriend;
        }

        public Object getCommentReplyDetail()
        {
            return CommentReplyDetail;
        }

        public void setCommentReplyDetail(Object CommentReplyDetail)
        {
            this.CommentReplyDetail = CommentReplyDetail;
        }
    }
}
