package apimodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 1/17/2017 at 4:27 PM.
 */

public class RequireEmployeeList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"JobId":"1b685af2-ccdc-4c70-825c-653258827049","UserId":"c7e5d577-3328-4a86-97e5-0192b9d352a0","UserName":"sachin","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","CompanyName":"Srashtasoft","JobTitle":"IOS","Vacancy":"2","Experience":"2 Years","Email":"sachin@gmail.com","Mobile":"8980622116","Education":"BE","JobDuration":"Full Time","Street":"JodhPur","City":"Ahmedabad","District":"Ahmedabad","State":"Gujarat","ZipCode":"382350","Resume":"","LikeCounts":0,"CommentCounts":0,"IsLiked":false,"IsWantJob":false,"IsWantEmployee":true,"CreateDate":"2017-01-16T10:13:25.283"}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public interface HideEmployeeListener{
        void OnHideEmployee(int position);
    }

    public static class DataBean implements Parcelable
    {
        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>()
        {
            @Override
            public DataBean createFromParcel(Parcel source)
            {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size)
            {
                return new DataBean[size];
            }
        };

        public HideEmployeeListener getHideEmployeeListener() {
            return hideEmployeeListener;
        }

        public void setHideEmployeeListener(HideEmployeeListener hideEmployeeListener) {
            this.hideEmployeeListener = hideEmployeeListener;
        }

        /**
         * JobId : 1b685af2-ccdc-4c70-825c-653258827049
         * UserId : c7e5d577-3328-4a86-97e5-0192b9d352a0
         * UserName : sachin
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg
         * CompanyName : Srashtasoft
         * JobTitle : IOS
         * Vacancy : 2
         * Experience : 2 Years
         * Email : sachin@gmail.com
         * Mobile : 8980622116
         * Education : BE
         * JobDuration : Full Time
         * Street : JodhPur
         * City : Ahmedabad
         * District : Ahmedabad
         * State : Gujarat
         * ZipCode : 382350
         * Resume :
         * LikeCounts : 0
         * CommentCounts : 0
         * IsLiked : false
         * IsWantJob : false
         * IsWantEmployee : true
         * CreateDate : 2017-01-16T10:13:25.283
         */
        private HideEmployeeListener hideEmployeeListener;
        @SerializedName("JobId")
        private String JobId;
        @SerializedName("UserId")
        private String UserId;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("CompanyName")
        private String CompanyName;
        @SerializedName("JobTitle")
        private String JobTitle;
        @SerializedName("Vacancy")
        private String Vacancy;
        @SerializedName("Experience")
        private String Experience;
        @SerializedName("Email")
        private String Email;
        @SerializedName("Mobile")
        private String Mobile;
        @SerializedName("Education")
        private String Education;
        @SerializedName("JobDuration")
        private String JobDuration;
        @SerializedName("Street")
        private String Street;
        @SerializedName("City")
        private String City;
        @SerializedName("District")
        private String District;
        @SerializedName("State")
        private String State;
        @SerializedName("ZipCode")
        private String ZipCode;
        @SerializedName("Resume")
        private String Resume;
        @SerializedName("LikeCounts")
        private int LikeCounts;
        @SerializedName("CommentCounts")
        private int CommentCounts;
        @SerializedName("IsLiked")
        private boolean IsLiked;
        @SerializedName("IsWantJob")
        private boolean IsWantJob;
        @SerializedName("IsWantEmployee")
        private boolean IsWantEmployee;
        @SerializedName("CreateDate")
        private String CreateDate;

        public int getPostViews() {
            return PostViews;
        }

        public void setPostViews(int postViews) {
            PostViews = postViews;
        }

        @SerializedName("PostViews")
        private int PostViews;


        public DataBean()
        {
        }

        protected DataBean(Parcel in)
        {
            this.JobId = in.readString();
            this.UserId = in.readString();
            this.UserName = in.readString();
            this.UserImage = in.readString();
            this.CompanyName = in.readString();
            this.JobTitle = in.readString();
            this.Vacancy = in.readString();
            this.Experience = in.readString();
            this.Email = in.readString();
            this.Mobile = in.readString();
            this.Education = in.readString();
            this.JobDuration = in.readString();
            this.Street = in.readString();
            this.City = in.readString();
            this.District = in.readString();
            this.State = in.readString();
            this.ZipCode = in.readString();
            this.Resume = in.readString();
            this.LikeCounts = in.readInt();
            this.CommentCounts = in.readInt();
            this.IsLiked = in.readByte() != 0;
            this.IsWantJob = in.readByte() != 0;
            this.IsWantEmployee = in.readByte() != 0;
            this.CreateDate = in.readString();
            this.PostViews = in.readInt();
        }

        public String getJobId()
        {
            return JobId;
        }

        public void setJobId(String JobId)
        {
            this.JobId = JobId;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getUserName()
        {
            return UserName;
        }

        public void setUserName(String UserName)
        {
            this.UserName = UserName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getCompanyName()
        {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName)
        {
            this.CompanyName = CompanyName;
        }

        public String getJobTitle()
        {
            return JobTitle;
        }

        public void setJobTitle(String JobTitle)
        {
            this.JobTitle = JobTitle;
        }

        public String getVacancy()
        {
            return Vacancy;
        }

        public void setVacancy(String Vacancy)
        {
            this.Vacancy = Vacancy;
        }

        public String getExperience()
        {
            return Experience;
        }

        public void setExperience(String Experience)
        {
            this.Experience = Experience;
        }

        public String getEmail()
        {
            return Email;
        }

        public void setEmail(String Email)
        {
            this.Email = Email;
        }

        public String getMobile()
        {
            return Mobile;
        }

        public void setMobile(String Mobile)
        {
            this.Mobile = Mobile;
        }

        public String getEducation()
        {
            return Education;
        }

        public void setEducation(String Education)
        {
            this.Education = Education;
        }

        public String getJobDuration()
        {
            return JobDuration;
        }

        public void setJobDuration(String JobDuration)
        {
            this.JobDuration = JobDuration;
        }

        public String getStreet()
        {
            return Street;
        }

        public void setStreet(String Street)
        {
            this.Street = Street;
        }

        public String getCity()
        {
            return City;
        }

        public void setCity(String City)
        {
            this.City = City;
        }

        public String getDistrict()
        {
            return District;
        }

        public void setDistrict(String District)
        {
            this.District = District;
        }

        public String getState()
        {
            return State;
        }

        public void setState(String State)
        {
            this.State = State;
        }

        public String getZipCode()
        {
            return ZipCode;
        }

        public void setZipCode(String ZipCode)
        {
            this.ZipCode = ZipCode;
        }

        public String getResume()
        {
            return Resume;
        }

        public void setResume(String Resume)
        {
            this.Resume = Resume;
        }

        public int getLikeCounts()
        {
            return LikeCounts;
        }

        public void setLikeCounts(int LikeCounts)
        {
            this.LikeCounts = LikeCounts;
        }

        public int getCommentCounts()
        {
            return CommentCounts;
        }

        public void setCommentCounts(int CommentCounts)
        {
            this.CommentCounts = CommentCounts;
        }

        public boolean isIsLiked()
        {
            return IsLiked;
        }

        public void setIsLiked(boolean IsLiked)
        {
            this.IsLiked = IsLiked;
        }

        public boolean isIsWantJob()
        {
            return IsWantJob;
        }

        public void setIsWantJob(boolean IsWantJob)
        {
            this.IsWantJob = IsWantJob;
        }

        public boolean isIsWantEmployee()
        {
            return IsWantEmployee;
        }

        public void setIsWantEmployee(boolean IsWantEmployee)
        {
            this.IsWantEmployee = IsWantEmployee;
        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeString(this.JobId);
            dest.writeString(this.UserId);
            dest.writeString(this.UserName);
            dest.writeString(this.UserImage);
            dest.writeString(this.CompanyName);
            dest.writeString(this.JobTitle);
            dest.writeString(this.Vacancy);
            dest.writeString(this.Experience);
            dest.writeString(this.Email);
            dest.writeString(this.Mobile);
            dest.writeString(this.Education);
            dest.writeString(this.JobDuration);
            dest.writeString(this.Street);
            dest.writeString(this.City);
            dest.writeString(this.District);
            dest.writeString(this.State);
            dest.writeString(this.ZipCode);
            dest.writeString(this.Resume);
            dest.writeInt(this.LikeCounts);
            dest.writeInt(this.CommentCounts);
            dest.writeByte(this.IsLiked ? (byte) 1 : (byte) 0);
            dest.writeByte(this.IsWantJob ? (byte) 1 : (byte) 0);
            dest.writeByte(this.IsWantEmployee ? (byte) 1 : (byte) 0);
            dest.writeString(this.CreateDate);
            dest.writeInt(this.PostViews);
        }
    }
}
