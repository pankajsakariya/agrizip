package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 11/21/2016 at 1:15 PM.
 */

public class SetupProfile
{


    /**
     * UserImage : Base64 Format
     * DeviceID : 12345678912345
     * Mobile : 9845784545
     * Name : John
     * State : 480c1a2a-ae7e-48cf-9103-899c69a76895
     * District : 81388d1e-d873-48bb-bf10-e12529ad5687
     * Gender : Male
     * Dob : 2016-12-23
     * InstanceID : fADHYCZl43w:APA91bF4_gLVK-ajmRdPGH3avAcpByUEKRqT3N-p1zu6vSWgnmgr-keOtYbx6LHeavw_kXrEmQD85QixnjpIRRVOTEjW0O4Lb6rSYyTN
     */

    @SerializedName("UserImage")
    private String UserImage;
    @SerializedName("DeviceID")
    private String DeviceID;
    @SerializedName("Mobile")
    private String Mobile;
    @SerializedName("Name")
    private String Name;
    @SerializedName("State")
    private String State;
    @SerializedName("District")
    private String District;
    @SerializedName("Gender")
    private String Gender;
    @SerializedName("Dob")
    private String Dob;
    @SerializedName("InstanceID")
    private String InstanceID;

    public String getUserImage()
    {
        return UserImage;
    }

    public void setUserImage(String UserImage)
    {
        this.UserImage = UserImage;
    }

    public String getDeviceID()
    {
        return DeviceID;
    }

    public void setDeviceID(String DeviceID)
    {
        this.DeviceID = DeviceID;
    }

    public String getMobile()
    {
        return Mobile;
    }

    public void setMobile(String Mobile)
    {
        this.Mobile = Mobile;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String Name)
    {
        this.Name = Name;
    }

    public String getState()
    {
        return State;
    }

    public void setState(String State)
    {
        this.State = State;
    }

    public String getDistrict()
    {
        return District;
    }

    public void setDistrict(String District)
    {
        this.District = District;
    }

    public String getGender()
    {
        return Gender;
    }

    public void setGender(String Gender)
    {
        this.Gender = Gender;
    }

    public String getDob()
    {
        return Dob;
    }

    public void setDob(String Dob)
    {
        this.Dob = Dob;
    }

    public String getInstanceID()
    {
        return InstanceID;
    }

    public void setInstanceID(String InstanceID)
    {
        this.InstanceID = InstanceID;
    }
}
