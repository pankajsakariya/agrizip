package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 11/29/2016 at 6:28 PM.
 */

public class PendingRequests
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : [{"Id":"ff8b821e-5791-4338-8a65-db6c9c0204da","UserId":"abb92a99-37ef-430b-8796-01880b462083","FriendUserId":"abb92a99-37ef-430b-8796-01880b462083","CreatedDateTime":"0001-01-01T00:00:00","RemoveDateTime":"0001-01-01T00:00:00","IsConfirm":false,"FullName":"Rutvik Mehta","UserImage":"/9j/4AAQSk"},{"Id":"ff8b821e-5791-4338-8a65-db6c9c0204da","UserId":"abb92a99-37ef-430b-8796-01880b462083","FriendUserId":"abb92a99-37ef-430b-8796-01880b462083","CreatedDateTime":"0001-01-01T00:00:00","RemoveDateTime":"0001-01-01T00:00:00","IsConfirm":false,"FullName":"Rutvik Mehta","UserImage":"/9j/4AAQSk"}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * Id : ff8b821e-5791-4338-8a65-db6c9c0204da
         * UserId : abb92a99-37ef-430b-8796-01880b462083
         * FriendUserId : abb92a99-37ef-430b-8796-01880b462083
         * CreatedDateTime : 0001-01-01T00:00:00
         * RemoveDateTime : 0001-01-01T00:00:00
         * IsConfirm : false
         * FullName : Rutvik Mehta
         * UserImage : /9j/4AAQSk
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("UserId")
        private String UserId;
        @SerializedName("FriendUserId")
        private String FriendUserId;
        @SerializedName("CreatedDateTime")
        private String CreatedDateTime;
        @SerializedName("RemoveDateTime")
        private String RemoveDateTime;
        @SerializedName("IsConfirm")
        private boolean IsConfirm;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;

        public int getMutualFriendCount() {
            return MutualFriendCount;
        }

        public void setMutualFriendCount(int mutualFriendCount) {
            MutualFriendCount = mutualFriendCount;
        }

        @SerializedName("MutualFriendCount")
        private int MutualFriendCount;


        public String getId()
        {
            return Id;
        }

        public void setId(String Id)
        {
            this.Id = Id;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getFriendUserId()
        {
            return FriendUserId;
        }

        public void setFriendUserId(String FriendUserId)
        {
            this.FriendUserId = FriendUserId;
        }

        public String getCreatedDateTime()
        {
            return CreatedDateTime;
        }

        public void setCreatedDateTime(String CreatedDateTime)
        {
            this.CreatedDateTime = CreatedDateTime;
        }

        public String getRemoveDateTime()
        {
            return RemoveDateTime;
        }

        public void setRemoveDateTime(String RemoveDateTime)
        {
            this.RemoveDateTime = RemoveDateTime;
        }

        public boolean isIsConfirm()
        {
            return IsConfirm;
        }

        public void setIsConfirm(boolean IsConfirm)
        {
            this.IsConfirm = IsConfirm;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }
    }
}
