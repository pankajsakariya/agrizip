package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 1/18/2017 at 6:44 PM.
 */

public class FollowersList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"FollowupId":"8ae8b1d2-b474-4b27-888f-6e32c84cced7","FollowUserId":"9c8bbb89-7063-450c-9b47-e616e71ffc86","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","CreateDate":"2017-01-12T06:49:55.12"},{"FollowupId":"80d10c02-e91a-4409-ad15-7826c866bd92","FollowUserId":"c7e5d577-3328-4a86-97e5-0192b9d352a0","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","CreateDate":"2017-01-10T10:08:17.003"}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }
    public interface FollowerActionListener
    {


        void onUnfollow(FollowersList.DataBean UnFollowfriend,int position);


    }


    public static class DataBean
    {


        public FollowerActionListener getFriendunfollowActionListener() {
            return friendunfollowActionListener;
        }

        public void setFriendunfollowActionListener(FollowerActionListener friendunfollowActionListener) {
            this.friendunfollowActionListener = friendunfollowActionListener;
        }

        /**
         * FollowupId : 8ae8b1d2-b474-4b27-888f-6e32c84cced7
         * FollowUserId : 9c8bbb89-7063-450c-9b47-e616e71ffc86
         * FullName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg
         * CreateDate : 2017-01-12T06:49:55.12
         */
        private FollowerActionListener friendunfollowActionListener;
        @SerializedName("FollowupId")
        private String FollowupId;
        @SerializedName("FollowUserId")
        private String FollowUserId;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("CreateDate")
        private String CreateDate;

        public String getFollowupId()
        {
            return FollowupId;
        }

        public void setFollowupId(String FollowupId)
        {
            this.FollowupId = FollowupId;
        }

        public String getFollowUserId()
        {
            return FollowUserId;
        }

        public void setFollowUserId(String FollowUserId)
        {
            this.FollowUserId = FollowUserId;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }
    }
}
