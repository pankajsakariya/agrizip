package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 11/28/2016 at 5:36 PM.
 */

public class UserSearchResponse
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"Id":"abb92a99-37ef-430b-8796-01880b462083","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","IsFriend":"Add Friend","mutualfriendcount":2,"MutualFriend":[{"FriendUserId":"6b81db86-3d14-4bd9-9d16-def858f58094","MutualFriend":"Pankaj"},{"FriendUserId":"d9b959f6-1f37-427f-b693-70b4a187d3f3","MutualFriend":"Rakshit Sathvara"}]}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * Id : abb92a99-37ef-430b-8796-01880b462083
         * FullName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg
         * IsFriend : Add Friend
         * mutualfriendcount : 2
         * MutualFriend : [{"FriendUserId":"6b81db86-3d14-4bd9-9d16-def858f58094","MutualFriend":"Pankaj"},{"FriendUserId":"d9b959f6-1f37-427f-b693-70b4a187d3f3","MutualFriend":"Rakshit Sathvara"}]
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("IsFriend")
        private String IsFriend;
        @SerializedName("mutualfriendcount")
        private int mutualfriendcount;
        @SerializedName("MutualFriend")
        private List<MutualFriendBean> MutualFriend;

        public String getId()
        {
            return Id;
        }

        public void setId(String Id)
        {
            this.Id = Id;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getIsFriend()
        {
            return IsFriend;
        }

        public void setIsFriend(String IsFriend)
        {
            this.IsFriend = IsFriend;
        }

        public int getMutualfriendcount()
        {
            return mutualfriendcount;
        }

        public void setMutualfriendcount(int mutualfriendcount)
        {
            this.mutualfriendcount = mutualfriendcount;
        }

        public List<MutualFriendBean> getMutualFriend()
        {
            return MutualFriend;
        }

        public void setMutualFriend(List<MutualFriendBean> MutualFriend)
        {
            this.MutualFriend = MutualFriend;
        }

        public static class MutualFriendBean
        {
            /**
             * FriendUserId : 6b81db86-3d14-4bd9-9d16-def858f58094
             * MutualFriend : Pankaj
             */

            @SerializedName("FriendUserId")
            private String FriendUserId;
            @SerializedName("MutualFriend")
            private String MutualFriend;

            public String getFriendUserId()
            {
                return FriendUserId;
            }

            public void setFriendUserId(String FriendUserId)
            {
                this.FriendUserId = FriendUserId;
            }

            public String getMutualFriend()
            {
                return MutualFriend;
            }

            public void setMutualFriend(String MutualFriend)
            {
                this.MutualFriend = MutualFriend;
            }
        }
    }
}