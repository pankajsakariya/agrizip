package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 12/27/2016 at 11:54 AM.
 */

public class UpdateUserProfileDetails
{
    /**
     * UserId : 75e32ba2-34b3-4309-8e6b-48a19850e4a9
     * EmailAddress : manoj.mistry@metizsoft.com
     * EmailVisibleTo : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
     * MobileNumber : 9724977397
     * MobileVisibleTo : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
     * Dob : 1992-07-20
     * DOBVisibleTo : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
     * Gender : Male
     * GenderVisibleTo : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
     * CityId : 507ec09e-9eae-4ddc-b069-965d07a44c73
     * City : Ahmedabad
     * CityVisibleTo : f983a431-a8ef-4127-9e22-3aaecab3bf63
     * IsCityShow : true
     * HometownId : 675203a9-5cf5-40e8-b04b-38cf30a9d4ea
     * Hometown : Surat
     * HometownVisibleTo : a9370d84-36c6-49cf-9799-9b8f125f5b50
     * IsHometownShow : true
     * RelationshipId : 79808fde-77fd-4372-b28e-5317484de752
     * RelationshipStatus : Single
     * RelationshipVisibleTo : 5da749f6-bc9b-47f3-9b88-de8c82bb0839
     * IsRelationshipShow : true
     * FollowUpCount : 1
     * IsFollowUpShow : true
     * Work : [{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Company":"Metizsoft","Position":".net developer","City":"Ahmedabad","Description":"write description","TimePeriodFrom":"2016-12-23T18:31:16.0168635+05:30","TimePeriodTo":"2016-12-23T18:31:16.0168635+05:30","WorkVisibleTo":"b44f8a4e-41dc-46af-ad85-1b5b11599268","IsWorkShow":true},{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Company":"Metizsoft","Position":".net developer","City":"Ahmedabad","Description":"write description","TimePeriodFrom":"2016-12-23T18:31:16.0168635+05:30","TimePeriodTo":"2016-12-23T18:31:16.0168635+05:30","WorkVisibleTo":"b44f8a4e-41dc-46af-ad85-1b5b11599268","IsWorkShow":true}]
     * Education : [{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","City":"Ahmedabad","TimePeriodFrom":"2016-12-23T18:31:16.0168635+05:30","TimePeriodTo":"2016-12-23T18:31:16.0168635+05:30","Graduated":true,"Description":"Description","IsSchool":true,"IsCollege":true,"EducationVisibleTo":"9472d723-54cd-4efe-b83c-d4f17aa539e5","IsEducationShow":true},{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","City":"Ahmedabad","TimePeriodFrom":"2016-12-23T18:31:16.0168635+05:30","TimePeriodTo":"2016-12-23T18:31:16.0168635+05:30","Graduated":true,"Description":"Description","IsSchool":true,"IsCollege":true,"EducationVisibleTo":"9472d723-54cd-4efe-b83c-d4f17aa539e5","IsEducationShow":true}]
     */

    @SerializedName("UserId")
    private String UserId;
    @SerializedName("EmailAddress")
    private String EmailAddress;
    @SerializedName("EmailVisibleTo")
    private String EmailVisibleTo;
    @SerializedName("MobileNumber")
    private String MobileNumber;
    @SerializedName("MobileVisibleTo")
    private String MobileVisibleTo;
    @SerializedName("Dob")
    private String Dob;
    @SerializedName("DOBVisibleTo")
    private String DOBVisibleTo;
    @SerializedName("Gender")
    private String Gender;
    @SerializedName("GenderVisibleTo")
    private String GenderVisibleTo;
    @SerializedName("CityId")
    private String CityId;
    @SerializedName("City")
    private String City;
    @SerializedName("CityVisibleTo")
    private String CityVisibleTo;
    @SerializedName("IsCityShow")
    private boolean IsCityShow;
    @SerializedName("HometownId")
    private String HometownId;
    @SerializedName("Hometown")
    private String Hometown;
    @SerializedName("HometownVisibleTo")
    private String HometownVisibleTo;
    @SerializedName("IsHometownShow")
    private boolean IsHometownShow;
    @SerializedName("RelationshipId")
    private String RelationshipId;
    @SerializedName("RelationshipStatus")
    private String RelationshipStatus;
    @SerializedName("RelationshipVisibleTo")
    private String RelationshipVisibleTo;
    @SerializedName("IsRelationshipShow")
    private boolean IsRelationshipShow;
    @SerializedName("FollowUpCount")
    private int FollowUpCount;
    @SerializedName("IsFollowUpShow")
    private boolean IsFollowUpShow;
    @SerializedName("Work")
    private List<WorkBean> Work;
    @SerializedName("Education")
    private List<EducationBean> Education;

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getEmailAddress()
    {
        return EmailAddress;
    }

    public void setEmailAddress(String EmailAddress)
    {
        this.EmailAddress = EmailAddress;
    }

    public String getEmailVisibleTo()
    {
        return EmailVisibleTo;
    }

    public void setEmailVisibleTo(String EmailVisibleTo)
    {
        this.EmailVisibleTo = EmailVisibleTo;
    }

    public String getMobileNumber()
    {
        return MobileNumber;
    }

    public void setMobileNumber(String MobileNumber)
    {
        this.MobileNumber = MobileNumber;
    }

    public String getMobileVisibleTo()
    {
        return MobileVisibleTo;
    }

    public void setMobileVisibleTo(String MobileVisibleTo)
    {
        this.MobileVisibleTo = MobileVisibleTo;
    }

    public String getDob()
    {
        return Dob;
    }

    public void setDob(String Dob)
    {
        this.Dob = Dob;
    }

    public String getDOBVisibleTo()
    {
        return DOBVisibleTo;
    }

    public void setDOBVisibleTo(String DOBVisibleTo)
    {
        this.DOBVisibleTo = DOBVisibleTo;
    }

    public String getGender()
    {
        return Gender;
    }

    public void setGender(String Gender)
    {
        this.Gender = Gender;
    }

    public String getGenderVisibleTo()
    {
        return GenderVisibleTo;
    }

    public void setGenderVisibleTo(String GenderVisibleTo)
    {
        this.GenderVisibleTo = GenderVisibleTo;
    }

    public String getCityId()
    {
        return CityId;
    }

    public void setCityId(String CityId)
    {
        this.CityId = CityId;
    }

    public String getCity()
    {
        return City;
    }

    public void setCity(String City)
    {
        this.City = City;
    }

    public String getCityVisibleTo()
    {
        return CityVisibleTo;
    }

    public void setCityVisibleTo(String CityVisibleTo)
    {
        this.CityVisibleTo = CityVisibleTo;
    }

    public boolean isIsCityShow()
    {
        return IsCityShow;
    }

    public void setIsCityShow(boolean IsCityShow)
    {
        this.IsCityShow = IsCityShow;
    }

    public String getHometownId()
    {
        return HometownId;
    }

    public void setHometownId(String HometownId)
    {
        this.HometownId = HometownId;
    }

    public String getHometown()
    {
        return Hometown;
    }

    public void setHometown(String Hometown)
    {
        this.Hometown = Hometown;
    }

    public String getHometownVisibleTo()
    {
        return HometownVisibleTo;
    }

    public void setHometownVisibleTo(String HometownVisibleTo)
    {
        this.HometownVisibleTo = HometownVisibleTo;
    }

    public boolean isIsHometownShow()
    {
        return IsHometownShow;
    }

    public void setIsHometownShow(boolean IsHometownShow)
    {
        this.IsHometownShow = IsHometownShow;
    }

    public String getRelationshipId()
    {
        return RelationshipId;
    }

    public void setRelationshipId(String RelationshipId)
    {
        this.RelationshipId = RelationshipId;
    }

    public String getRelationshipStatus()
    {
        return RelationshipStatus;
    }

    public void setRelationshipStatus(String RelationshipStatus)
    {
        this.RelationshipStatus = RelationshipStatus;
    }

    public String getRelationshipVisibleTo()
    {
        return RelationshipVisibleTo;
    }

    public void setRelationshipVisibleTo(String RelationshipVisibleTo)
    {
        this.RelationshipVisibleTo = RelationshipVisibleTo;
    }

    public boolean isIsRelationshipShow()
    {
        return IsRelationshipShow;
    }

    public void setIsRelationshipShow(boolean IsRelationshipShow)
    {
        this.IsRelationshipShow = IsRelationshipShow;
    }

    public int getFollowUpCount()
    {
        return FollowUpCount;
    }

    public void setFollowUpCount(int FollowUpCount)
    {
        this.FollowUpCount = FollowUpCount;
    }

    public boolean isIsFollowUpShow()
    {
        return IsFollowUpShow;
    }

    public void setIsFollowUpShow(boolean IsFollowUpShow)
    {
        this.IsFollowUpShow = IsFollowUpShow;
    }

    public List<WorkBean> getWork()
    {
        return Work;
    }

    public void setWork(List<WorkBean> Work)
    {
        this.Work = Work;
    }

    public List<EducationBean> getEducation()
    {
        return Education;
    }

    public void setEducation(List<EducationBean> Education)
    {
        this.Education = Education;
    }

    public static class WorkBean
    {
        /**
         * WorkId : 6fb609c0-60d5-4c17-85b4-87bf233cec40
         * Company : Metizsoft
         * Position : .net developer
         * City : Ahmedabad
         * Description : write description
         * TimePeriodFrom : 2016-12-23T18:31:16.0168635+05:30
         * TimePeriodTo : 2016-12-23T18:31:16.0168635+05:30
         * WorkVisibleTo : b44f8a4e-41dc-46af-ad85-1b5b11599268
         * IsWorkShow : true
         */

        @SerializedName("WorkId")
        private String WorkId;
        @SerializedName("Company")
        private String Company;
        @SerializedName("Position")
        private String Position;
        @SerializedName("City")
        private String City;
        @SerializedName("Description")
        private String Description;
        @SerializedName("TimePeriodFrom")
        private String TimePeriodFrom;
        @SerializedName("TimePeriodTo")
        private String TimePeriodTo;
        @SerializedName("WorkVisibleTo")
        private String WorkVisibleTo;
        @SerializedName("IsWorkShow")
        private boolean IsWorkShow;

        public String getWorkId()
        {
            return WorkId;
        }

        public void setWorkId(String WorkId)
        {
            this.WorkId = WorkId;
        }

        public String getCompany()
        {
            return Company;
        }

        public void setCompany(String Company)
        {
            this.Company = Company;
        }

        public String getPosition()
        {
            return Position;
        }

        public void setPosition(String Position)
        {
            this.Position = Position;
        }

        public String getCity()
        {
            return City;
        }

        public void setCity(String City)
        {
            this.City = City;
        }

        public String getDescription()
        {
            return Description;
        }

        public void setDescription(String Description)
        {
            this.Description = Description;
        }

        public String getTimePeriodFrom()
        {
            return TimePeriodFrom;
        }

        public void setTimePeriodFrom(String TimePeriodFrom)
        {
            this.TimePeriodFrom = TimePeriodFrom;
        }

        public String getTimePeriodTo()
        {
            return TimePeriodTo;
        }

        public void setTimePeriodTo(String TimePeriodTo)
        {
            this.TimePeriodTo = TimePeriodTo;
        }

        public String getWorkVisibleTo()
        {
            return WorkVisibleTo;
        }

        public void setWorkVisibleTo(String WorkVisibleTo)
        {
            this.WorkVisibleTo = WorkVisibleTo;
        }

        public boolean isIsWorkShow()
        {
            return IsWorkShow;
        }

        public void setIsWorkShow(boolean IsWorkShow)
        {
            this.IsWorkShow = IsWorkShow;
        }
    }

    public static class EducationBean
    {
        /**
         * EducationId : 29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311
         * InstituteName : GTU
         * City : Ahmedabad
         * TimePeriodFrom : 2016-12-23T18:31:16.0168635+05:30
         * TimePeriodTo : 2016-12-23T18:31:16.0168635+05:30
         * Graduated : true
         * Description : Description
         * IsSchool : true
         * IsCollege : true
         * EducationVisibleTo : 9472d723-54cd-4efe-b83c-d4f17aa539e5
         * IsEducationShow : true
         */

        @SerializedName("EducationId")
        private String EducationId;
        @SerializedName("InstituteName")
        private String InstituteName;
        @SerializedName("City")
        private String City;
        @SerializedName("TimePeriodFrom")
        private String TimePeriodFrom;
        @SerializedName("TimePeriodTo")
        private String TimePeriodTo;
        @SerializedName("Graduated")
        private boolean Graduated;
        @SerializedName("Description")
        private String Description;
        @SerializedName("IsSchool")
        private boolean IsSchool;
        @SerializedName("IsCollege")
        private boolean IsCollege;
        @SerializedName("EducationVisibleTo")
        private String EducationVisibleTo;
        @SerializedName("IsEducationShow")
        private boolean IsEducationShow;

        public String getEducationId()
        {
            return EducationId;
        }

        public void setEducationId(String EducationId)
        {
            this.EducationId = EducationId;
        }

        public String getInstituteName()
        {
            return InstituteName;
        }

        public void setInstituteName(String InstituteName)
        {
            this.InstituteName = InstituteName;
        }

        public String getCity()
        {
            return City;
        }

        public void setCity(String City)
        {
            this.City = City;
        }

        public String getTimePeriodFrom()
        {
            return TimePeriodFrom;
        }

        public void setTimePeriodFrom(String TimePeriodFrom)
        {
            this.TimePeriodFrom = TimePeriodFrom;
        }

        public String getTimePeriodTo()
        {
            return TimePeriodTo;
        }

        public void setTimePeriodTo(String TimePeriodTo)
        {
            this.TimePeriodTo = TimePeriodTo;
        }

        public boolean isGraduated()
        {
            return Graduated;
        }

        public void setGraduated(boolean Graduated)
        {
            this.Graduated = Graduated;
        }

        public String getDescription()
        {
            return Description;
        }

        public void setDescription(String Description)
        {
            this.Description = Description;
        }

        public boolean isIsSchool()
        {
            return IsSchool;
        }

        public void setIsSchool(boolean IsSchool)
        {
            this.IsSchool = IsSchool;
        }

        public boolean isIsCollege()
        {
            return IsCollege;
        }

        public void setIsCollege(boolean IsCollege)
        {
            this.IsCollege = IsCollege;
        }

        public String getEducationVisibleTo()
        {
            return EducationVisibleTo;
        }

        public void setEducationVisibleTo(String EducationVisibleTo)
        {
            this.EducationVisibleTo = EducationVisibleTo;
        }

        public boolean isIsEducationShow()
        {
            return IsEducationShow;
        }

        public void setIsEducationShow(boolean IsEducationShow)
        {
            this.IsEducationShow = IsEducationShow;
        }
    }

    /**
     *
     *
     * {
     "UserId": "75e32ba2-34b3-4309-8e6b-48a19850e4a9", // Pass UserId
     "EmailAddress": "manoj.mistry@metizsoft.com", // Pass email address
     "EmailVisibleTo": "e19750dd-8bf4-4c73-bbc3-feb014dc0f65", //pass privacyId
     "MobileNumber": "9724977397", // pass mobile number
     "MobileVisibleTo": "e19750dd-8bf4-4c73-bbc3-feb014dc0f65",//pass privacyId
     "Dob": "1992-07-20", // pass birthdate
     "DOBVisibleTo": "e19750dd-8bf4-4c73-bbc3-feb014dc0f65",//pass privacyId
     "Gender": "Male", // Pass Male or Female
     "GenderVisibleTo": "e19750dd-8bf4-4c73-bbc3-feb014dc0f65",//pass privacyId
     "TotalFriendsCounts": "10", // no need to pass default as it is
     "Rating": 4, // no need to pass default as it is
     "CityId": "507ec09e-9eae-4ddc-b069-965d07a44c73", // Pass City Id
     "City": "Ahmedabad", // Pass Current City
     "CityVisibleTo": "f983a431-a8ef-4127-9e22-3aaecab3bf63", // Pass privacy Id
     "IsCityShow": true, // Pass true/false based on user check
     "HometownId": "675203a9-5cf5-40e8-b04b-38cf30a9d4ea", // Pass Hometown Id
     "Hometown": "Surat",  // Pass Hometown place
     "HometownVisibleTo": "a9370d84-36c6-49cf-9799-9b8f125f5b50",//Pass privacyId

     "IsHometownShow": true, // Pass true/false based on user check
     "RelationshipId": "79808fde-77fd-4372-b28e-5317484de752", // Pass relationship Id
     "RelationshipStatus": "Single", // Pass relationship Status
     "RelationshipVisibleTo": "5da749f6-bc9b-47f3-9b88-de8c82bb0839",// Pass privacyId
     "IsRelationshipShow": true, // Pass true/false based on user check
     "FollowUpCount": 1, // No need to pass default 1
     "IsFollowUpShow": true  // Pass true/false based on user check
     "Work": [
     {
     "WorkId": "6fb609c0-60d5-4c17-85b4-87bf233cec40", // Pass work Id
     "Company": "Metizsoft", // work company
     "Position": ".net developer", // work position
     "City": "Ahmedabad", // City Name
     "Description": "write description", // description
     "TimePeriodFrom": "2016-12-23T18:31:16.0168635+05:30",// work startdate
     "TimePeriodTo": "2016-12-23T18:31:16.0168635+05:30", // work leavedate
     "WorkVisibleTo": "b44f8a4e-41dc-46af-ad85-1b5b11599268",//pass privacyId
     "IsWorkShow": true // Pass true/false based on user check

     },
     {
     "WorkId": "6fb609c0-60d5-4c17-85b4-87bf233cec40", // Pass work Id
     "Company": "Metizsoft", // work company
     "Position": ".net developer", // work position
     "City": "Ahmedabad", // City Name
     "Description": "write description", // description
     "TimePeriodFrom": "2016-12-23T18:31:16.0168635+05:30",// work startdate
     "TimePeriodTo": "2016-12-23T18:31:16.0168635+05:30", // work leavedate
     "WorkVisibleTo": "b44f8a4e-41dc-46af-ad85-1b5b11599268",//pass privacyId
     "IsWorkShow": true // Pass true/false based on user check
     }
     ],

     "Education": [
     {
     "EducationId": "29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311",//Pass Education Id
     "InstituteName": "GTU", // Pass School or college name
     "City": "Ahmedabad", // // pass education city name
     "TimePeriodFrom": "2016-12-23T18:31:16.0168635+05:30",// education startdate
     "TimePeriodTo": "2016-12-23T18:31:16.0168635+05:30",// education completedate
     "Graduated": true, // pass true false based on check
     "Description": "Description",
     "IsSchool": true, // no need to pass default true
     "IsCollege": true, // no need to pass default true
     "EducationVisibleTo": "9472d723-54cd-4efe-b83c-d4f17aa539e5",
     "IsEducationShow": true // Pass true/false based on user check
     },
     {
     "EducationId": "29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311", // Pass Education Id
     "InstituteName": "GTU", // Pass School or college name
     "City": "Ahmedabad", // // pass education city name
     "TimePeriodFrom": "2016-12-23T18:31:16.0168635+05:30",// education startdate
     "TimePeriodTo": "2016-12-23T18:31:16.0168635+05:30",// education completedate
     "Graduated": true, // pass true false based on check
     "Description": "Description",
     "IsSchool": true, // no need to pass default true
     "IsCollege": true, // no need to pass default true
     "EducationVisibleTo": "9472d723-54cd-4efe-b83c-d4f17aa539e5",
     "IsEducationShow": true // Pass true/false based on user check
     }
     ]
     }
     *
     */


}
