package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 2/23/2017 at 5:44 PM.
 */

public class CommonResponse
{

    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : Not Found
     * UpdateDate : null
     * Data : null
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private Object Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public Object getData()
    {
        return Data;
    }

    public void setData(Object Data)
    {
        this.Data = Data;
    }
}
