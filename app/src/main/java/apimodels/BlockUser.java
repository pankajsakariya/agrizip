package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/3/2017 at 5:58 PM.
 */

public class BlockUser
{


    /**
     * Id : 00000000-0000-0000-0000-000000000000
     * UserID : 9ff68b87-130c-4950-a317-ab3dc6712b29
     * BlockUserID : 9396ce45-f26b-49a5-826e-b65ddcdb9031
     * BlockMessage : write block message
     */

    @SerializedName("Id")
    private String Id;
    @SerializedName("UserID")
    private String UserID;
    @SerializedName("BlockUserID")
    private String BlockUserID;
    @SerializedName("BlockMessage")
    private String BlockMessage;

    public String getId()
    {
        return Id;
    }

    public void setId(String Id)
    {
        this.Id = Id;
    }

    public String getUserID()
    {
        return UserID;
    }

    public void setUserID(String UserID)
    {
        this.UserID = UserID;
    }

    public String getBlockUserID()
    {
        return BlockUserID;
    }

    public void setBlockUserID(String BlockUserID)
    {
        this.BlockUserID = BlockUserID;
    }

    public String getBlockMessage()
    {
        return BlockMessage;
    }

    public void setBlockMessage(String BlockMessage)
    {
        this.BlockMessage = BlockMessage;
    }
}
