package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 11/24/2016 at 12:40 PM.
 */

public class StatusPostList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : [{"StatusId":"770a2ce3-b839-4b0b-8641-4dc13a59dfb7","UserID":"abb92a99-37ef-430b-8796-01880b462083","CheckInID":"00000000-0000-0000-0000-000000000000","Status":"hello its Thursday today","TagTo":"","FeelingName":"","FeelingValue":"","PostYear":"","PostMonth":"","PostDay":"","PostHour":"","PostMinute":"","Help":false,"News":false,"VisibleTo":"","CreateDate":"2016-11-24T11:30:56.13","UpdateDate":"2016-11-24T11:30:56.13","IsDelete":false,"UserName":"Rutvik Mehta","SharedOwner":"","LikeCounts":0,"CommentCounts":0,"ShareCounts":0,"CreateDatetime":null,"Message":null,"Place":null,"Images":null},{"StatusId":"47c6584c-9ce4-4bb4-b9d3-59eac4b2a24a","UserID":"abb92a99-37ef-430b-8796-01880b462083","CheckInID":"00000000-0000-0000-0000-000000000000","Status":"it will be Friday tomorrow","TagTo":"","FeelingName":"","FeelingValue":"","PostYear":"","PostMonth":"","PostDay":"","PostHour":"","PostMinute":"","Help":false,"News":false,"VisibleTo":"","CreateDate":"2016-11-24T11:34:01.053","UpdateDate":"2016-11-24T11:34:01.053","IsDelete":false,"UserName":"Rutvik Mehta","SharedOwner":"","LikeCounts":0,"CommentCounts":0,"ShareCounts":0,"CreateDatetime":null,"Message":null,"Place":null,"Images":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<SinglePost> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public List<SinglePost> getData()
    {
        return Data;
    }

    public void setData(List<SinglePost> Data)
    {
        this.Data = Data;
    }

}
