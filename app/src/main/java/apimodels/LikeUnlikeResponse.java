package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 11/28/2016 at 1:55 PM.
 */

public class LikeUnlikeResponse
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : {"Id":"7078d704-1a5e-4677-800d-3d898d8c5414","StatusID":"8f0d790b-0f91-4d00-b612-105123b0f924","UserID":"abb92a99-37ef-430b-8796-01880b462083","IsLike":true,"IsComment":false,"Comment":null,"Imagestr":null,"CreateDate":null,"UpdateDate":null,"StatusDetailID":null}
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private DataBean Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public DataBean getData()
    {
        return Data;
    }

    public void setData(DataBean Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * Id : 7078d704-1a5e-4677-800d-3d898d8c5414
         * StatusID : 8f0d790b-0f91-4d00-b612-105123b0f924
         * UserID : abb92a99-37ef-430b-8796-01880b462083
         * IsLike : true
         * IsComment : false
         * Comment : null
         * Imagestr : null
         * CreateDate : null
         * UpdateDate : null
         * StatusDetailID : null
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("StatusID")
        private String StatusID;
        @SerializedName("UserID")
        private String UserID;
        @SerializedName("IsLike")
        private boolean IsLike;
        @SerializedName("IsComment")
        private boolean IsComment;
        @SerializedName("Comment")
        private Object Comment;
        @SerializedName("Imagestr")
        private Object Imagestr;
        @SerializedName("CreateDate")
        private Object CreateDate;
        @SerializedName("UpdateDate")
        private Object UpdateDate;
        @SerializedName("StatusDetailID")
        private Object StatusDetailID;

        public String getId()
        {
            return Id;
        }

        public void setId(String Id)
        {
            this.Id = Id;
        }

        public String getStatusID()
        {
            return StatusID;
        }

        public void setStatusID(String StatusID)
        {
            this.StatusID = StatusID;
        }

        public String getUserID()
        {
            return UserID;
        }

        public void setUserID(String UserID)
        {
            this.UserID = UserID;
        }

        public boolean isIsLike()
        {
            return IsLike;
        }

        public void setIsLike(boolean IsLike)
        {
            this.IsLike = IsLike;
        }

        public boolean isIsComment()
        {
            return IsComment;
        }

        public void setIsComment(boolean IsComment)
        {
            this.IsComment = IsComment;
        }

        public Object getComment()
        {
            return Comment;
        }

        public void setComment(Object Comment)
        {
            this.Comment = Comment;
        }

        public Object getImagestr()
        {
            return Imagestr;
        }

        public void setImagestr(Object Imagestr)
        {
            this.Imagestr = Imagestr;
        }

        public Object getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(Object CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public Object getUpdateDate()
        {
            return UpdateDate;
        }

        public void setUpdateDate(Object UpdateDate)
        {
            this.UpdateDate = UpdateDate;
        }

        public Object getStatusDetailID()
        {
            return StatusDetailID;
        }

        public void setStatusDetailID(Object StatusDetailID)
        {
            this.StatusDetailID = StatusDetailID;
        }
    }
}
