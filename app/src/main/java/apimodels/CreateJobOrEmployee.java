package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/13/2017 at 6:41 PM.
 */

public class CreateJobOrEmployee
{


    /**
     * JobId : 00000000-0000-0000-0000-000000000000
     * UserId : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
     * CompanyName :
     * JobTitle : asp.net web developer
     * Vacancy :
     * Experience :
     * Email : xyz@gmail.com
     * Mobile : 1234567890
     * Education : MCA
     * JobDuration : Part time/ Full time
     * Street : Navrangpura
     * City : Ahmedabad
     * District : Ahmedabad
     * State : Gujarat
     * ZipCode : 380009
     * Resume : Resume image base 64 format
     * IsWantJob : true
     * IsWantEmployee : false
     * CreateDate : 2017-01-05T11:03:34.1331546+05:30
     * IsDelete : false
     */

    @SerializedName("JobId")
    private String JobId;
    @SerializedName("UserId")
    private String UserId;
    @SerializedName("CompanyName")
    private String CompanyName;
    @SerializedName("JobTitle")
    private String JobTitle;
    @SerializedName("Vacancy")
    private String Vacancy;
    @SerializedName("Experience")
    private String Experience;
    @SerializedName("Email")
    private String Email;
    @SerializedName("Mobile")
    private String Mobile;
    @SerializedName("Education")
    private String Education;
    @SerializedName("JobDuration")
    private String JobDuration;
    @SerializedName("Street")
    private String Street;
    @SerializedName("City")
    private String City;
    @SerializedName("District")
    private String District;
    @SerializedName("State")
    private String State;
    @SerializedName("ZipCode")
    private String ZipCode;
    @SerializedName("Resume")
    private String Resume;
    @SerializedName("IsWantJob")
    private boolean IsWantJob;
    @SerializedName("IsWantEmployee")
    private boolean IsWantEmployee;
    @SerializedName("CreateDate")
    private String CreateDate;
    @SerializedName("IsDelete")
    private boolean IsDelete;

    public String getJobId()
    {
        return JobId;
    }

    public void setJobId(String JobId)
    {
        this.JobId = JobId;
    }

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getCompanyName()
    {
        return CompanyName;
    }

    public void setCompanyName(String CompanyName)
    {
        this.CompanyName = CompanyName;
    }

    public String getJobTitle()
    {
        return JobTitle;
    }

    public void setJobTitle(String JobTitle)
    {
        this.JobTitle = JobTitle;
    }

    public String getVacancy()
    {
        return Vacancy;
    }

    public void setVacancy(String Vacancy)
    {
        this.Vacancy = Vacancy;
    }

    public String getExperience()
    {
        return Experience;
    }

    public void setExperience(String Experience)
    {
        this.Experience = Experience;
    }

    public String getEmail()
    {
        return Email;
    }

    public void setEmail(String Email)
    {
        this.Email = Email;
    }

    public String getMobile()
    {
        return Mobile;
    }

    public void setMobile(String Mobile)
    {
        this.Mobile = Mobile;
    }

    public String getEducation()
    {
        return Education;
    }

    public void setEducation(String Education)
    {
        this.Education = Education;
    }

    public String getJobDuration()
    {
        return JobDuration;
    }

    public void setJobDuration(String JobDuration)
    {
        this.JobDuration = JobDuration;
    }

    public String getStreet()
    {
        return Street;
    }

    public void setStreet(String Street)
    {
        this.Street = Street;
    }

    public String getCity()
    {
        return City;
    }

    public void setCity(String City)
    {
        this.City = City;
    }

    public String getDistrict()
    {
        return District;
    }

    public void setDistrict(String District)
    {
        this.District = District;
    }

    public String getState()
    {
        return State;
    }

    public void setState(String State)
    {
        this.State = State;
    }

    public String getZipCode()
    {
        return ZipCode;
    }

    public void setZipCode(String ZipCode)
    {
        this.ZipCode = ZipCode;
    }

    public String getResume()
    {
        return Resume;
    }

    public void setResume(String Resume)
    {
        this.Resume = Resume;
    }

    public boolean isIsWantJob()
    {
        return IsWantJob;
    }

    public void setIsWantJob(boolean IsWantJob)
    {
        this.IsWantJob = IsWantJob;
    }

    public boolean isIsWantEmployee()
    {
        return IsWantEmployee;
    }

    public void setIsWantEmployee(boolean IsWantEmployee)
    {
        this.IsWantEmployee = IsWantEmployee;
    }

    public String getCreateDate()
    {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate)
    {
        this.CreateDate = CreateDate;
    }

    public boolean isIsDelete()
    {
        return IsDelete;
    }

    public void setIsDelete(boolean IsDelete)
    {
        this.IsDelete = IsDelete;
    }
}
