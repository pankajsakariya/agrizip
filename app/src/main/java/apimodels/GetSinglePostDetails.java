package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 2/27/2017 at 6:02 PM.
 */

public class GetSinglePostDetails
{

    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"StatusId":"b3dd6f1d-a00b-4763-9e75-a89f4fc8a347","UserID":"cd4e1de6-fecd-4a27-acad-b1b244adca93","IsCheckIn":false,"CheckInID":"00000000-0000-0000-0000-000000000000","Status":"The Facebook platform pumps and motors and they are not the intended recipient you are not the intended recipient you are not the intended recipient you","FeelingName":"","FeelingValue":"","PostYear":"","PostMonth":"","PostDay":"","PostHour":"","PostMinute":"","News":false,"VisibleTo":"e19750dd-8bf4-4c73-bbc3-feb014dc0f65","CreateDate":"2017-02-23T16:31:12.767","UpdateDate":"2017-02-23T16:31:12.767","IsDelete":false,"UserName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233817834214986.jpeg","SharedOwnerId":"00000000-0000-0000-0000-000000000000","SharedOwner":"","SharedOwnerImage":"","LikeCounts":0,"CommentCounts":0,"ShareCounts":0,"CreateDatetime":null,"Message":null,"Place":null,"IsLiked":false,"Images":[{"StatusImageId":"41147ED3-A272-4D4E-B1AB-DE50BD4CC098","StatusID":"b3dd6f1d-a00b-4763-9e75-a89f4fc8a347","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636234840758385450.jpeg"}],"TagDetails":null,"Help":false,"HelpType":0,"HelpTitle":"","IsJobPost":false,"IsEventPost":false,"JobDetails":null,"EventsDetails":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<SinglePost> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<SinglePost> getData()
    {
        return Data;
    }

    public void setData(List<SinglePost> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * StatusId : b3dd6f1d-a00b-4763-9e75-a89f4fc8a347
         * UserID : cd4e1de6-fecd-4a27-acad-b1b244adca93
         * IsCheckIn : false
         * CheckInID : 00000000-0000-0000-0000-000000000000
         * Status : The Facebook platform pumps and motors and they are not the intended recipient you are not the intended recipient you are not the intended recipient you
         * FeelingName :
         * FeelingValue :
         * PostYear :
         * PostMonth :
         * PostDay :
         * PostHour :
         * PostMinute :
         * News : false
         * VisibleTo : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
         * CreateDate : 2017-02-23T16:31:12.767
         * UpdateDate : 2017-02-23T16:31:12.767
         * IsDelete : false
         * UserName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636233817834214986.jpeg
         * SharedOwnerId : 00000000-0000-0000-0000-000000000000
         * SharedOwner :
         * SharedOwnerImage :
         * LikeCounts : 0
         * CommentCounts : 0
         * ShareCounts : 0
         * CreateDatetime : null
         * Message : null
         * Place : null
         * IsLiked : false
         * Images : [{"StatusImageId":"41147ED3-A272-4D4E-B1AB-DE50BD4CC098","StatusID":"b3dd6f1d-a00b-4763-9e75-a89f4fc8a347","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636234840758385450.jpeg"}]
         * TagDetails : null
         * Help : false
         * HelpType : 0
         * HelpTitle :
         * IsJobPost : false
         * IsEventPost : false
         * JobDetails : null
         * EventsDetails : null
         */

        @SerializedName("PostYear")
        private String PostYear;
        @SerializedName("PostMonth")
        private String PostMonth;
        @SerializedName("PostDay")
        private String PostDay;
        @SerializedName("PostHour")
        private String PostHour;
        @SerializedName("PostMinute")
        private String PostMinute;
        @SerializedName("News")
        private boolean News;
        @SerializedName("VisibleTo")
        private String VisibleTo;
        @SerializedName("CreateDate")
        private String CreateDate;
        @SerializedName("UpdateDate")
        private String UpdateDate;
        @SerializedName("IsDelete")
        private boolean IsDelete;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("SharedOwnerId")
        private String SharedOwnerId;
        @SerializedName("SharedOwner")
        private String SharedOwner;
        @SerializedName("SharedOwnerImage")
        private String SharedOwnerImage;
        @SerializedName("LikeCounts")
        private int LikeCounts;
        @SerializedName("CommentCounts")
        private int CommentCounts;
        @SerializedName("ShareCounts")
        private int ShareCounts;
        @SerializedName("IsLiked")
        private boolean IsLiked;
        @SerializedName("TagDetails")
        private Object TagDetails;
        @SerializedName("Help")
        private boolean Help;
        @SerializedName("HelpType")
        private int HelpType;
        @SerializedName("HelpTitle")
        private String HelpTitle;
        @SerializedName("IsJobPost")
        private boolean IsJobPost;
        @SerializedName("IsEventPost")
        private boolean IsEventPost;
        @SerializedName("Images")
        private List<ImagesBean> Images;

        public String getPostYear()
        {
            return PostYear;
        }

        public void setPostYear(String PostYear)
        {
            this.PostYear = PostYear;
        }

        public String getPostMonth()
        {
            return PostMonth;
        }

        public void setPostMonth(String PostMonth)
        {
            this.PostMonth = PostMonth;
        }

        public String getPostDay()
        {
            return PostDay;
        }

        public void setPostDay(String PostDay)
        {
            this.PostDay = PostDay;
        }

        public String getPostHour()
        {
            return PostHour;
        }

        public void setPostHour(String PostHour)
        {
            this.PostHour = PostHour;
        }

        public String getPostMinute()
        {
            return PostMinute;
        }

        public void setPostMinute(String PostMinute)
        {
            this.PostMinute = PostMinute;
        }

        public boolean isNews()
        {
            return News;
        }

        public void setNews(boolean News)
        {
            this.News = News;
        }

        public String getVisibleTo()
        {
            return VisibleTo;
        }

        public void setVisibleTo(String VisibleTo)
        {
            this.VisibleTo = VisibleTo;
        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public String getUpdateDate()
        {
            return UpdateDate;
        }

        public void setUpdateDate(String UpdateDate)
        {
            this.UpdateDate = UpdateDate;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }

        public String getUserName()
        {
            return UserName;
        }

        public void setUserName(String UserName)
        {
            this.UserName = UserName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getSharedOwnerId()
        {
            return SharedOwnerId;
        }

        public void setSharedOwnerId(String SharedOwnerId)
        {
            this.SharedOwnerId = SharedOwnerId;
        }

        public String getSharedOwner()
        {
            return SharedOwner;
        }

        public void setSharedOwner(String SharedOwner)
        {
            this.SharedOwner = SharedOwner;
        }

        public String getSharedOwnerImage()
        {
            return SharedOwnerImage;
        }

        public void setSharedOwnerImage(String SharedOwnerImage)
        {
            this.SharedOwnerImage = SharedOwnerImage;
        }

        public int getLikeCounts()
        {
            return LikeCounts;
        }

        public void setLikeCounts(int LikeCounts)
        {
            this.LikeCounts = LikeCounts;
        }

        public int getCommentCounts()
        {
            return CommentCounts;
        }

        public void setCommentCounts(int CommentCounts)
        {
            this.CommentCounts = CommentCounts;
        }

        public int getShareCounts()
        {
            return ShareCounts;
        }

        public void setShareCounts(int ShareCounts)
        {
            this.ShareCounts = ShareCounts;
        }

        public boolean isIsLiked()
        {
            return IsLiked;
        }

        public void setIsLiked(boolean IsLiked)
        {
            this.IsLiked = IsLiked;
        }

        public Object getTagDetails()
        {
            return TagDetails;
        }

        public void setTagDetails(Object TagDetails)
        {
            this.TagDetails = TagDetails;
        }

        public boolean isHelp()
        {
            return Help;
        }

        public void setHelp(boolean Help)
        {
            this.Help = Help;
        }

        public int getHelpType()
        {
            return HelpType;
        }

        public void setHelpType(int HelpType)
        {
            this.HelpType = HelpType;
        }

        public String getHelpTitle()
        {
            return HelpTitle;
        }

        public void setHelpTitle(String HelpTitle)
        {
            this.HelpTitle = HelpTitle;
        }

        public boolean isIsJobPost()
        {
            return IsJobPost;
        }

        public void setIsJobPost(boolean IsJobPost)
        {
            this.IsJobPost = IsJobPost;
        }

        public boolean isIsEventPost()
        {
            return IsEventPost;
        }

        public void setIsEventPost(boolean IsEventPost)
        {
            this.IsEventPost = IsEventPost;
        }

        public List<ImagesBean> getImages()
        {
            return Images;
        }

        public void setImages(List<ImagesBean> Images)
        {
            this.Images = Images;
        }

        public static class ImagesBean
        {
            /**
             * StatusImageId : 41147ED3-A272-4D4E-B1AB-DE50BD4CC098
             * StatusID : b3dd6f1d-a00b-4763-9e75-a89f4fc8a347
             * Imagestr : http://social.vinayakpharmaceuticals.com/UserImage/636234840758385450.jpeg
             */

            @SerializedName("StatusImageId")
            private String StatusImageId;
            @SerializedName("StatusID")
            private String StatusID;
            @SerializedName("Imagestr")
            private String Imagestr;

            public String getStatusImageId()
            {
                return StatusImageId;
            }

            public void setStatusImageId(String StatusImageId)
            {
                this.StatusImageId = StatusImageId;
            }

            public String getStatusID()
            {
                return StatusID;
            }

            public void setStatusID(String StatusID)
            {
                this.StatusID = StatusID;
            }

            public String getImagestr()
            {
                return Imagestr;
            }

            public void setImagestr(String Imagestr)
            {
                this.Imagestr = Imagestr;
            }
        }
    }
}
