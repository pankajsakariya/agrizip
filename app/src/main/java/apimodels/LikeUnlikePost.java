package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 11/25/2016 at 4:37 PM.
 */

public class LikeUnlikePost
{


    /**
     * Id : 00000000-0000-0000-0000-000000000000
     * StatusID : a80be270-e8e0-4f9b-8cfa-a978fbe48a02
     * UserID : 92a9fa1a-9665-4ce0-9725-7c620df094bc
     * IsLike : true
     * IsComment : false
     * Comment :
     * Imagestr :
     * CreateDate : 2016-11-23T17:29:32.9131884+05:30
     * UpdateDate : 2016-11-23T17:29:32.9131884+05:30
     * StatusDetailID : 00000000-0000-0000-0000-000000000000
     */

    @SerializedName("Id")
    private String Id;
    @SerializedName("StatusID")
    private String StatusID;
    @SerializedName("UserID")
    private String UserID;
    @SerializedName("IsLike")
    private boolean IsLike;
    @SerializedName("IsComment")
    private boolean IsComment;
    @SerializedName("Comment")
    private String Comment;
    @SerializedName("Imagestr")
    private String Imagestr;
    @SerializedName("CreateDate")
    private String CreateDate;
    @SerializedName("UpdateDate")
    private String UpdateDate;
    @SerializedName("StatusDetailID")
    private String StatusDetailID;

    public String getId()
    {
        return Id;
    }

    public void setId(String Id)
    {
        this.Id = Id;
    }

    public String getStatusID()
    {
        return StatusID;
    }

    public void setStatusID(String StatusID)
    {
        this.StatusID = StatusID;
    }

    public String getUserID()
    {
        return UserID;
    }

    public void setUserID(String UserID)
    {
        this.UserID = UserID;
    }

    public boolean isIsLike()
    {
        return IsLike;
    }

    public void setIsLike(boolean IsLike)
    {
        this.IsLike = IsLike;
    }

    public boolean isIsComment()
    {
        return IsComment;
    }

    public void setIsComment(boolean IsComment)
    {
        this.IsComment = IsComment;
    }

    public String getComment()
    {
        return Comment;
    }

    public void setComment(String Comment)
    {
        this.Comment = Comment;
    }

    public String getImagestr()
    {
        return Imagestr;
    }

    public void setImagestr(String Imagestr)
    {
        this.Imagestr = Imagestr;
    }

    public String getCreateDate()
    {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate)
    {
        this.CreateDate = CreateDate;
    }

    public String getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(String UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public String getStatusDetailID()
    {
        return StatusDetailID;
    }

    public void setStatusDetailID(String StatusDetailID)
    {
        this.StatusDetailID = StatusDetailID;
    }
}
