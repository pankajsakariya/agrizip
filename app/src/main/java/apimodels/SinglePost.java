package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 11/22/2016 at 4:11 PM.
 */

public class SinglePost
{


    /**
     * IsCheckIn : true
     * StatusId : e7069731-1882-4732-860a-bba0de99e0c6
     * UserID : fb408304-edee-466e-bb3f-557594907b15
     * CreateDatetime : 2016-11-22T15:30:40.5359602+05:30
     * Message : sample string 5
     * Place : sample string 6
     * CheckInID : 95ab15fc-3ab7-497a-ad84-8a59273e663d
     * Status : sample string 8
     * TagTo : sample string 9
     * FeelingName : sample string 10
     * FeelingValue : sample string 11
     * PostYear : sample string 12
     * PostMonth : sample string 13
     * PostDay : sample string 14
     * PostHour : sample string 15
     * PostMinute : sample string 16
     * Help : true
     * News : true
     * VisibleTo : sample string 19
     * CreateDate : 2016-11-22T15:30:40.5359602+05:30
     * UpdateDate : 2016-11-22T15:30:40.5359602+05:30
     * IsDelete : true
     * Images : [{"Id":"b1317b3b-4092-4da9-913f-5882340a14ac","StatusImageId":"sample string 2","StatusID":"36972abe-e6ca-4cad-93a5-23be70785ee0","Imagestr":"sample string 3","Sort":4,"IsDelete":true,"UserID":"b7619f04-7be5-4ea6-80c3-9f054f20bc1d","IsLike":true,"IsComment":true,"Comment":"sample string 5","CreateDate":"2016-11-22T15:30:40.5359602+05:30","UpdateDate":"2016-11-22T15:30:40.5359602+05:30"},{"Id":"b1317b3b-4092-4da9-913f-5882340a14ac","StatusImageId":"sample string 2","StatusID":"36972abe-e6ca-4cad-93a5-23be70785ee0","Imagestr":"sample string 3","Sort":4,"IsDelete":true,"UserID":"b7619f04-7be5-4ea6-80c3-9f054f20bc1d","IsLike":true,"IsComment":true,"Comment":"sample string 5","CreateDate":"2016-11-22T15:30:40.5359602+05:30","UpdateDate":"2016-11-22T15:30:40.5359602+05:30"},{"Id":"b1317b3b-4092-4da9-913f-5882340a14ac","StatusImageId":"sample string 2","StatusID":"36972abe-e6ca-4cad-93a5-23be70785ee0","Imagestr":"sample string 3","Sort":4,"IsDelete":true,"UserID":"b7619f04-7be5-4ea6-80c3-9f054f20bc1d","IsLike":true,"IsComment":true,"Comment":"sample string 5","CreateDate":"2016-11-22T15:30:40.5359602+05:30","UpdateDate":"2016-11-22T15:30:40.5359602+05:30"}]
     * Tagpeople : [{"TagId":"f5fd2b04-c1fd-47e8-9fae-515538255e3a","StatusID":"a943bd72-c69b-483a-b0dc-ccc33496ce96","TaggedBy":"307a4800-3778-4c06-bb01-989c19cbc056","TaggedTo":"ef54fb08-6512-46a0-a6bb-1e7db0917d06","IsDelete":true},{"TagId":"f5fd2b04-c1fd-47e8-9fae-515538255e3a","StatusID":"a943bd72-c69b-483a-b0dc-ccc33496ce96","TaggedBy":"307a4800-3778-4c06-bb01-989c19cbc056","TaggedTo":"ef54fb08-6512-46a0-a6bb-1e7db0917d06","IsDelete":true},{"TagId":"f5fd2b04-c1fd-47e8-9fae-515538255e3a","StatusID":"a943bd72-c69b-483a-b0dc-ccc33496ce96","TaggedBy":"307a4800-3778-4c06-bb01-989c19cbc056","TaggedTo":"ef54fb08-6512-46a0-a6bb-1e7db0917d06","IsDelete":true}]
     */


    private PostCallbacksListener postCallbacksListener;

    @SerializedName("IsCheckIn")
    private boolean IsCheckIn;
    @SerializedName("StatusId")
    private String StatusId;
    @SerializedName("UserID")
    private String UserID;
    @SerializedName("CreateDatetime")
    private String CreateDatetime;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Place")
    private String Place;
    @SerializedName("CheckInID")
    private String CheckInID;
    @SerializedName("Status")
    private String Status;
    @SerializedName("TagTo")
    private String TagTo;
    @SerializedName("FeelingName")
    private String FeelingName;
    @SerializedName("FeelingValue")
    private String FeelingValue;
    @SerializedName("PostYear")
    private String PostYear;
    @SerializedName("PostMonth")
    private String PostMonth;
    @SerializedName("PostDay")
    private String PostDay;
    @SerializedName("PostHour")
    private String PostHour;
    @SerializedName("PostMinute")
    private String PostMinute;
    @SerializedName("Help")
    private boolean Help;
    @SerializedName("News")
    private boolean News;
    @SerializedName("Notification")
    private boolean Notification;
    @SerializedName("VisibleTo")
    private String VisibleTo;
    @SerializedName("CreateDate")
    private String CreateDate;
    @SerializedName("UpdateDate")
    private String UpdateDate;
    @SerializedName("IsDelete")
    private boolean IsDelete;
    @SerializedName("Images")
    private List<ImagesBean> Images;
    @SerializedName("Tagpeople")
    private List<TagpeopleBean> Tagpeople;
    @SerializedName("UserProfileDetailsInfo")
    private UserProfileDetailsInfo.DataBean userProfileDetailsInfo;
    @SerializedName("UserName")
    private String UserName;
    @SerializedName("SharedOwner")
    private String SharedOwner;
    @SerializedName("LikeCounts")
    private int LikeCounts;
    @SerializedName("CommentCounts")
    private int CommentCounts;
    @SerializedName("ShareCounts")
    private int ShareCounts;

    @SerializedName("UserImage")
    private String UserImage;
    /**
     * IsLiked : true
     */

    @SerializedName("IsLiked")
    private boolean IsLiked;
    /**
     * HelpType : 1
     * HelpTitle : Advice Title
     */
    @SerializedName("PostViews")
    private int PostViews;

    public int getPostViews() {
        return PostViews;
    }

    public void setPostViews(int postViews) {
        PostViews = postViews;
    }
    @SerializedName("HelpType")
    private int HelpType;   //
    @SerializedName("HelpTitle")
    private String HelpTitle;
    /**
     * SharedOwnerId : 00000000-0000-0000-0000-000000000000
     * SharedOwnerImage :
     * CreateDatetime : null
     * Message : null
     * Place : null
     */

    @SerializedName("SharedOwnerId")
    private String SharedOwnerId;
    @SerializedName("SharedOwnerImage")
    private String SharedOwnerImage;

    public boolean isFollow() {
        return IsFollow;
    }

    public void setFollow(boolean follow) {
        IsFollow = follow;
    }

    @SerializedName("IsFollow")
    private boolean IsFollow;


    /**
     * CreateDatetime : null
     * Message : null
     * Place : null
     * TagDetails : null
     * IsJobPost : false
     * IsEventPost : false
     * JobDetails : {"JobId":"00000000-0000-0000-0000-000000000000","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","UserName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","CompanyName":"","JobTitle":"","Vacancy":"","Experience":"","Email":"","Mobile":"","Education":"","JobDuration":"","Street":"","City":"","District":"","State":"","ZipCode":"","Resume":"","LikeCounts":0,"CommentCounts":0,"IsLiked":false,"IsWantJob":false,"IsWantEmployee":false,"CreateDate":"2014-10-10T00:00:00"}
     * EventsDetails : {"EventId":"00000000-0000-0000-0000-000000000000","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","UserName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","EventImage":"","EventName":"","Host":"","ContactPerson":"","Date":"2014-10-10T00:00:00","Time":"","Location":"","Address":"","Description":"","CreateDate":"2014-10-10T00:00:00"}
     */
    @SerializedName("TagDetails")
    private List<TagDetails> TagDetails;
    @SerializedName("IsJobPost")
    private boolean IsJobPost;
    @SerializedName("IsEventPost")
    private boolean IsEventPost;
    @SerializedName("JobDetails")
    private JobDetailsBean JobDetails;
    @SerializedName("EventsDetails")
    private EventsDetailsBean EventsDetails;

    public String getUserImage()
    {
        return UserImage;
    }

    public void setUserImage(String userImage)
    {
        UserImage = userImage;
    }

    public PostCallbacksListener getPostCallbacksListener()
    {
        return postCallbacksListener;
    }

    public void setPostCallbacksListener(PostCallbacksListener postCallbacksListener)
    {
        this.postCallbacksListener = postCallbacksListener;
    }
    public boolean isNotification() {
        return Notification;
    }

    public void setNotification(boolean notification) {
        Notification = notification;
    }
    public UserProfileDetailsInfo.DataBean getUserProfileDetailsInfo()
    {
        return userProfileDetailsInfo;
    }

    public void setUserProfileDetailsInfo(UserProfileDetailsInfo.DataBean userProfileDetailsInfo)
    {
        this.userProfileDetailsInfo = userProfileDetailsInfo;
    }

    public boolean isIsCheckIn()
    {
        return IsCheckIn;
    }

    public void setIsCheckIn(boolean IsCheckIn)
    {
        this.IsCheckIn = IsCheckIn;
    }

    public String getStatusId()
    {
        return StatusId;
    }

    public void setStatusId(String StatusId)
    {
        this.StatusId = StatusId;
    }

    public String getUserID()
    {
        return UserID;
    }

    public void setUserID(String UserID)
    {
        this.UserID = UserID;
    }

    public String getCreateDatetime()
    {
        return CreateDatetime;
    }

    public void setCreateDatetime(String CreateDatetime)
    {
        this.CreateDatetime = CreateDatetime;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public String getPlace()
    {
        return Place;
    }

    public void setPlace(String Place)
    {
        this.Place = Place;
    }

    public String getCheckInID()
    {
        return CheckInID;
    }

    public void setCheckInID(String CheckInID)
    {
        this.CheckInID = CheckInID;
    }

    public String getStatus()
    {
        return Status;
    }

    public void setStatus(String Status)
    {
        this.Status = Status;
    }

    public String getTagTo()
    {
        return TagTo;
    }

    public void setTagTo(String TagTo)
    {
        this.TagTo = TagTo;
    }

    public String getFeelingName()
    {
        return FeelingName;
    }

    public void setFeelingName(String FeelingName)
    {
        this.FeelingName = FeelingName;
    }

    public String getFeelingValue()
    {
        return FeelingValue;
    }

    public void setFeelingValue(String FeelingValue)
    {
        this.FeelingValue = FeelingValue;
    }

    public String getPostYear()
    {
        return PostYear;
    }

    public void setPostYear(String PostYear)
    {
        this.PostYear = PostYear;
    }

    public String getPostMonth()
    {
        return PostMonth;
    }

    public void setPostMonth(String PostMonth)
    {
        this.PostMonth = PostMonth;
    }

    public String getPostDay()
    {
        return PostDay;
    }

    public void setPostDay(String PostDay)
    {
        this.PostDay = PostDay;
    }

    public String getPostHour()
    {
        return PostHour;
    }

    public void setPostHour(String PostHour)
    {
        this.PostHour = PostHour;
    }

    public String getPostMinute()
    {
        return PostMinute;
    }

    public void setPostMinute(String PostMinute)
    {
        this.PostMinute = PostMinute;
    }

    public boolean isHelp()
    {
        return Help;
    }

    public void setHelp(boolean Help)
    {
        this.Help = Help;
    }

    public boolean isNews()
    {
        return News;
    }

    public void setNews(boolean News)
    {
        this.News = News;
    }

    public String getVisibleTo()
    {
        return VisibleTo;
    }

    public void setVisibleTo(String VisibleTo)
    {
        this.VisibleTo = VisibleTo;
    }

    public String getCreateDate()
    {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate)
    {
        this.CreateDate = CreateDate;
    }

    public String getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(String UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public boolean isIsDelete()
    {
        return IsDelete;
    }

    public void setIsDelete(boolean IsDelete)
    {
        this.IsDelete = IsDelete;
    }

    public List<ImagesBean> getImages()
    {
        return Images;
    }

    public void setImages(List<ImagesBean> Images)
    {
        this.Images = Images;
    }

    public List<TagpeopleBean> getTagpeople()
    {
        return Tagpeople;
    }

    public void setTagpeople(List<TagpeopleBean> Tagpeople)
    {
        this.Tagpeople = Tagpeople;
    }

    public String getUserName()
    {
        return UserName;
    }

    public void setUserName(String UserName)
    {
        this.UserName = UserName;
    }

    public String getSharedOwner()
    {
        return SharedOwner;
    }

    public void setSharedOwner(String SharedOwner)
    {
        this.SharedOwner = SharedOwner;
    }

    public int getLikeCounts()
    {
        return LikeCounts;
    }

    public void setLikeCounts(int LikeCounts)
    {
        this.LikeCounts = LikeCounts;
    }

    public int getCommentCounts()
    {
        return CommentCounts;
    }

    public void setCommentCounts(int CommentCounts)
    {
        this.CommentCounts = CommentCounts;
    }

    public int getShareCounts()
    {
        return ShareCounts;
    }

    public void setShareCounts(int ShareCounts)
    {
        this.ShareCounts = ShareCounts;
    }

    public boolean isIsLiked()
    {
        return IsLiked;
    }

    public void setIsLiked(boolean IsLiked)
    {
        this.IsLiked = IsLiked;
    }

    public int getHelpType()
    {
        return HelpType;
    }

    public void setHelpType(int HelpType)
    {
        this.HelpType = HelpType;
    }

    public String getHelpTitle()
    {
        return HelpTitle;
    }

    public void setHelpTitle(String HelpTitle)
    {
        this.HelpTitle = HelpTitle;
    }

    public String getSharedOwnerId()
    {
        return SharedOwnerId;
    }

    public void setSharedOwnerId(String SharedOwnerId)
    {
        this.SharedOwnerId = SharedOwnerId;
    }

    public String getSharedOwnerImage()
    {
        return SharedOwnerImage;
    }

    public void setSharedOwnerImage(String SharedOwnerImage)
    {
        this.SharedOwnerImage = SharedOwnerImage;
    }

    public List<TagDetails> getTagDetails()
    {
        return TagDetails;
    }

    public void setTagDetails(List<TagDetails> TagDetails)
    {
        this.TagDetails = TagDetails;
    }

    public boolean isIsJobPost()
    {
        return IsJobPost;
    }

    public void setIsJobPost(boolean IsJobPost)
    {
        this.IsJobPost = IsJobPost;
    }

    public boolean isIsEventPost()
    {
        return IsEventPost;
    }

    public void setIsEventPost(boolean IsEventPost)
    {
        this.IsEventPost = IsEventPost;
    }

    public JobDetailsBean getJobDetails()
    {
        return JobDetails;
    }

    public void setJobDetails(JobDetailsBean JobDetails)
    {
        this.JobDetails = JobDetails;
    }

    public EventsDetailsBean getEventsDetails()
    {
        return EventsDetails;
    }

    public void setEventsDetails(EventsDetailsBean EventsDetails)
    {
        this.EventsDetails = EventsDetails;
    }

    public interface PostCallbacksListener
    {
        void onClickComments(String postId,int count);
       // void onStatusPostSuccessful(SinglePost singlePost);
    }

    public static class ImagesBean
    {
        /**
         * Id : b1317b3b-4092-4da9-913f-5882340a14ac
         * StatusImageId : sample string 2
         * StatusID : 36972abe-e6ca-4cad-93a5-23be70785ee0
         * Imagestr : sample string 3
         * Sort : 4
         * IsDelete : true
         * UserID : b7619f04-7be5-4ea6-80c3-9f054f20bc1d
         * IsLike : true
         * IsComment : true
         * Comment : sample string 5
         * CreateDate : 2016-11-22T15:30:40.5359602+05:30
         * UpdateDate : 2016-11-22T15:30:40.5359602+05:30
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("StatusImageId")
        private String StatusImageId;
        @SerializedName("StatusID")
        private String StatusID;
        @SerializedName("Imagestr")
        private String Imagestr;
        @SerializedName("Sort")
        private int Sort;
        @SerializedName("IsDelete")
        private boolean IsDelete;
        @SerializedName("UserID")
        private String UserID;
        @SerializedName("IsLike")
        private boolean IsLike;
        @SerializedName("IsComment")
        private boolean IsComment;
        @SerializedName("Comment")
        private String Comment;
        @SerializedName("CreateDate")
        private String CreateDate;
        @SerializedName("UpdateDate")
        private String UpdateDate;





        public String getId()
        {
            return Id;
        }

        public void setId(String Id)
        {
            this.Id = Id;
        }

        public String getStatusImageId()
        {
            return StatusImageId;
        }

        public void setStatusImageId(String StatusImageId)
        {
            this.StatusImageId = StatusImageId;
        }

        public String getStatusID()
        {
            return StatusID;
        }

        public void setStatusID(String StatusID)
        {
            this.StatusID = StatusID;
        }

        public String getImagestr()
        {
            return Imagestr;
        }

        public void setImagestr(String Imagestr)
        {
            this.Imagestr = Imagestr;
        }

        public int getSort()
        {
            return Sort;
        }

        public void setSort(int Sort)
        {
            this.Sort = Sort;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }

        public String getUserID()
        {
            return UserID;
        }

        public void setUserID(String UserID)
        {
            this.UserID = UserID;
        }

        public boolean isIsLike()
        {
            return IsLike;
        }

        public void setIsLike(boolean IsLike)
        {
            this.IsLike = IsLike;
        }

        public boolean isIsComment()
        {
            return IsComment;
        }

        public void setIsComment(boolean IsComment)
        {
            this.IsComment = IsComment;
        }

        public String getComment()
        {
            return Comment;
        }

        public void setComment(String Comment)
        {
            this.Comment = Comment;
        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public String getUpdateDate()
        {
            return UpdateDate;
        }

        public void setUpdateDate(String UpdateDate)
        {
            this.UpdateDate = UpdateDate;
        }
    }

    public static class TagpeopleBean
    {
        /**
         * TagId : f5fd2b04-c1fd-47e8-9fae-515538255e3a
         * StatusID : a943bd72-c69b-483a-b0dc-ccc33496ce96
         * TaggedBy : 307a4800-3778-4c06-bb01-989c19cbc056
         * TaggedTo : ef54fb08-6512-46a0-a6bb-1e7db0917d06
         * IsDelete : true
         */

        @SerializedName("TagId")
        private String TagId;
        @SerializedName("StatusID")
        private String StatusID;
        @SerializedName("TaggedBy")
        private String TaggedBy;


        @SerializedName("TaggedName")
        private String TaggedName;
        @SerializedName("TaggedTo")
        private String TaggedTo;
        @SerializedName("IsDelete")
        private boolean IsDelete;

        public String getTaggedName() {
            return TaggedName;
        }

        public void setTaggedName(String taggedName) {
            TaggedName = taggedName;
        }

        public String getTagId()
        {
            return TagId;
        }

        public void setTagId(String TagId)
        {
            this.TagId = TagId;
        }

        public String getStatusID()
        {
            return StatusID;
        }

        public void setStatusID(String StatusID)
        {
            this.StatusID = StatusID;
        }

        public String getTaggedBy()
        {
            return TaggedBy;
        }

        public void setTaggedBy(String TaggedBy)
        {
            this.TaggedBy = TaggedBy;
        }

        public void setTaggedTo(String TaggedTo)
        {
            this.TaggedTo = TaggedTo;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }
    }

    public static class JobDetailsBean
    {
        /**
         * JobId : 00000000-0000-0000-0000-000000000000
         * UserId : cd4e1de6-fecd-4a27-acad-b1b244adca93
         * UserName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg
         * CompanyName :
         * JobTitle :
         * Vacancy :
         * Experience :
         * Email :
         * Mobile :
         * Education :
         * JobDuration :
         * Street :
         * City :
         * District :
         * State :
         * ZipCode :
         * Resume :
         * LikeCounts : 0
         * CommentCounts : 0
         * IsLiked : false
         * IsWantJob : false
         * IsWantEmployee : false
         * CreateDate : 2014-10-10T00:00:00
         */

        @SerializedName("JobId")
        private String JobId;
        @SerializedName("UserId")
        private String UserId;
        @SerializedName("UserName")
        private String UserNameX;
        @SerializedName("UserImage")
        private String UserImageX;
        @SerializedName("CompanyName")
        private String CompanyName;
        @SerializedName("JobTitle")
        private String JobTitle;
        @SerializedName("Vacancy")
        private String Vacancy;
        @SerializedName("Experience")
        private String Experience;
        @SerializedName("Email")
        private String Email;
        @SerializedName("Mobile")
        private String Mobile;
        @SerializedName("Education")
        private String Education;
        @SerializedName("JobDuration")
        private String JobDuration;
        @SerializedName("Street")
        private String Street;
        @SerializedName("City")
        private String City;
        @SerializedName("District")
        private String District;
        @SerializedName("State")
        private String State;
        @SerializedName("ZipCode")
        private String ZipCode;
        @SerializedName("Resume")
        private String Resume;
        @SerializedName("LikeCounts")
        private int LikeCountsX;
        @SerializedName("CommentCounts")
        private int CommentCountsX;
        @SerializedName("IsLiked")
        private boolean IsLikedX;
        @SerializedName("IsWantJob")
        private boolean IsWantJob;
        @SerializedName("IsWantEmployee")
        private boolean IsWantEmployee;
        @SerializedName("CreateDate")
        private String CreateDateX;

        public String getJobId()
        {
            return JobId;
        }

        public void setJobId(String JobId)
        {
            this.JobId = JobId;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getUserNameX()
        {
            return UserNameX;
        }

        public void setUserNameX(String UserNameX)
        {
            this.UserNameX = UserNameX;
        }

        public String getUserImageX()
        {
            return UserImageX;
        }

        public void setUserImageX(String UserImageX)
        {
            this.UserImageX = UserImageX;
        }

        public String getCompanyName()
        {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName)
        {
            this.CompanyName = CompanyName;
        }

        public String getJobTitle()
        {
            return JobTitle;
        }

        public void setJobTitle(String JobTitle)
        {
            this.JobTitle = JobTitle;
        }

        public String getVacancy()
        {
            return Vacancy;
        }

        public void setVacancy(String Vacancy)
        {
            this.Vacancy = Vacancy;
        }

        public String getExperience()
        {
            return Experience;
        }

        public void setExperience(String Experience)
        {
            this.Experience = Experience;
        }

        public String getEmail()
        {
            return Email;
        }

        public void setEmail(String Email)
        {
            this.Email = Email;
        }

        public String getMobile()
        {
            return Mobile;
        }

        public void setMobile(String Mobile)
        {
            this.Mobile = Mobile;
        }

        public String getEducation()
        {
            return Education;
        }

        public void setEducation(String Education)
        {
            this.Education = Education;
        }

        public String getJobDuration()
        {
            return JobDuration;
        }

        public void setJobDuration(String JobDuration)
        {
            this.JobDuration = JobDuration;
        }

        public String getStreet()
        {
            return Street;
        }

        public void setStreet(String Street)
        {
            this.Street = Street;
        }

        public String getCity()
        {
            return City;
        }

        public void setCity(String City)
        {
            this.City = City;
        }

        public String getDistrict()
        {
            return District;
        }

        public void setDistrict(String District)
        {
            this.District = District;
        }

        public String getState()
        {
            return State;
        }

        public void setState(String State)
        {
            this.State = State;
        }

        public String getZipCode()
        {
            return ZipCode;
        }

        public void setZipCode(String ZipCode)
        {
            this.ZipCode = ZipCode;
        }

        public String getResume()
        {
            return Resume;
        }

        public void setResume(String Resume)
        {
            this.Resume = Resume;
        }

        public int getLikeCountsX()
        {
            return LikeCountsX;
        }

        public void setLikeCountsX(int LikeCountsX)
        {
            this.LikeCountsX = LikeCountsX;
        }

        public int getCommentCountsX()
        {
            return CommentCountsX;
        }

        public void setCommentCountsX(int CommentCountsX)
        {
            this.CommentCountsX = CommentCountsX;
        }

        public boolean isIsLikedX()
        {
            return IsLikedX;
        }

        public void setIsLikedX(boolean IsLikedX)
        {
            this.IsLikedX = IsLikedX;
        }

        public boolean isIsWantJob()
        {
            return IsWantJob;
        }

        public void setIsWantJob(boolean IsWantJob)
        {
            this.IsWantJob = IsWantJob;
        }

        public boolean isIsWantEmployee()
        {
            return IsWantEmployee;
        }

        public void setIsWantEmployee(boolean IsWantEmployee)
        {
            this.IsWantEmployee = IsWantEmployee;
        }

        public String getCreateDateX()
        {
            return CreateDateX;
        }

        public void setCreateDateX(String CreateDateX)
        {
            this.CreateDateX = CreateDateX;
        }
    }

    public static class EventsDetailsBean
    {
        /**
         * EventId : 00000000-0000-0000-0000-000000000000
         * UserId : cd4e1de6-fecd-4a27-acad-b1b244adca93
         * UserName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg
         * EventImage :
         * EventName :
         * Host :
         * ContactPerson :
         * Date : 2014-10-10T00:00:00
         * Time :
         * Location :
         * Address :
         * Description :
         * CreateDate : 2014-10-10T00:00:00
         */

        @SerializedName("EventId")
        private String EventId;
        @SerializedName("UserId")
        private String UserId;
        @SerializedName("UserName")
        private String UserNameX;
        @SerializedName("UserImage")
        private String UserImageX;
        @SerializedName("EventImage")
        private String EventImage;
        @SerializedName("EventName")
        private String EventName;
        @SerializedName("Host")
        private String Host;
        @SerializedName("ContactPerson")
        private String ContactPerson;
        @SerializedName("Date")
        private String Date;
        @SerializedName("Time")
        private String Time;
        @SerializedName("Location")
        private String Location;
        @SerializedName("Address")
        private String Address;
        @SerializedName("Description")
        private String Description;
        @SerializedName("CreateDate")
        private String CreateDateX;

        public int getCommentCounts() {
            return CommentCounts;
        }

        public void setCommentCounts(int commentCounts) {
            CommentCounts = commentCounts;
        }

        @SerializedName("CommentCounts")
        private int CommentCounts;



        public String getEventId()
        {
            return EventId;
        }

        public void setEventId(String EventId)
        {
            this.EventId = EventId;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getUserNameX()
        {
            return UserNameX;
        }

        public void setUserNameX(String UserNameX)
        {
            this.UserNameX = UserNameX;
        }

        public String getUserImageX()
        {
            return UserImageX;
        }

        public void setUserImageX(String UserImageX)
        {
            this.UserImageX = UserImageX;
        }

        public String getEventImage()
        {
            return EventImage;
        }

        public void setEventImage(String EventImage)
        {
            this.EventImage = EventImage;
        }

        public String getEventName()
        {
            return EventName;
        }

        public void setEventName(String EventName)
        {
            this.EventName = EventName;
        }

        public String getHost()
        {
            return Host;
        }

        public void setHost(String Host)
        {
            this.Host = Host;
        }

        public String getContactPerson()
        {
            return ContactPerson;
        }

        public void setContactPerson(String ContactPerson)
        {
            this.ContactPerson = ContactPerson;
        }

        public String getDate()
        {
            return Date;
        }

        public void setDate(String Date)
        {
            this.Date = Date;
        }

        public String getTime()
        {
            return Time;
        }

        public void setTime(String Time)
        {
            this.Time = Time;
        }

        public String getLocation()
        {
            return Location;
        }

        public void setLocation(String Location)
        {
            this.Location = Location;
        }

        public String getAddress()
        {
            return Address;
        }

        public void setAddress(String Address)
        {
            this.Address = Address;
        }

        public String getDescription()
        {
            return Description;
        }

        public void setDescription(String Description)
        {
            this.Description = Description;
        }

        public String getCreateDateX()
        {
            return CreateDateX;
        }

        public void setCreateDateX(String CreateDateX)
        {
            this.CreateDateX = CreateDateX;
        }
    }

    public static class TagDetails
    {

        /**
         * TagId : a9d3e710-a918-40b3-b329-282edb11ad64
         * TagFriendId : bb8ea28e-32c8-43ed-b8c1-d8166fa9d10c
         * FullName : gokul hirani
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636216588997269543.jpeg
         */

        @SerializedName("TagId")
        private String TagId;
        @SerializedName("TagFriendId")
        private String TagFriendId;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;

        public String getTagId()
        {
            return TagId;
        }

        public void setTagId(String TagId)
        {
            this.TagId = TagId;
        }

        public String getTagFriendId()
        {
            return TagFriendId;
        }

        public void setTagFriendId(String TagFriendId)
        {
            this.TagFriendId = TagFriendId;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }
    }
}
