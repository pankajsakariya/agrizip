package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/4/2017 at 12:51 PM.
 */

public class UploadPhotoInAlbum
{


    /**
     * PhotoID : 00000000-0000-0000-0000-000000000000
     * AlbumID : aa1cd0d6-3d67-45d5-bd5e-911b8cc04857
     * Imagestr : sample string 3
     */

    @SerializedName("PhotoID")
    private String PhotoID;
    @SerializedName("AlbumID")
    private String AlbumID;
    @SerializedName("Imagestr")
    private String Imagestr;

    public String getPhotoID()
    {
        return PhotoID;
    }

    public void setPhotoID(String PhotoID)
    {
        this.PhotoID = PhotoID;
    }

    public String getAlbumID()
    {
        return AlbumID;
    }

    public void setAlbumID(String AlbumID)
    {
        this.AlbumID = AlbumID;
    }

    public String getImagestr()
    {
        return Imagestr;
    }

    public void setImagestr(String Imagestr)
    {
        this.Imagestr = Imagestr;
    }
}
