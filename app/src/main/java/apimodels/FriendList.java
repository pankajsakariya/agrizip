package apimodels;

import com.google.gson.annotations.SerializedName;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.users.model.QBUser;

import java.util.List;

/**
 * Created by rutvik on 12/1/2016 at 12:06 PM.
 */

public class FriendList
{

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public interface FriendActionListener
    {
        void onUnfriend(FriendList.DataBean friend);

        void onUnfollow(FriendList.DataBean friend);

        void onBlockFriend(FriendList.DataBean blockedFriend);
    }


    public static class DataBean
    {
        /**
         * Id : 00000000-0000-0000-0000-000000000000
         * UserId : 00000000-0000-0000-0000-000000000000
         * FriendUserId : abb92a99-37ef-430b-8796-01880b462083
         * CreatedDateTime : 0001-01-01T00:00:00
         * RemoveDateTime : 0001-01-01T00:00:00
         * IsConfirm : false
         * FullName : Rutvik Mehta
         * UserImage :
         */


        @SerializedName("Id")
        private String Id;
        @SerializedName("UserId")
        private String UserId;
        @SerializedName("FriendUserId")
        private String FriendUserId;
        @SerializedName("CreatedDateTime")
        private String CreatedDateTime;
        @SerializedName("RemoveDateTime")
        private String RemoveDateTime;
        @SerializedName("IsConfirm")
        private boolean IsConfirm;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("isChecked")
        private boolean isChecked = false;




        @SerializedName("FriendId")
        private String FriendId;

        @SerializedName("FriendImage")
        private String FriendImage;

        public String getFriendImage() {
            return FriendImage;
        }

        public void setFriendImage(String friendImage) {
            FriendImage = friendImage;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        @SerializedName("Status")
        private String Status;

        private FriendActionListener friendActionListener;
        public String getFriendId() {
            return FriendId;
        }

        public void setFriendId(String friendId) {
            FriendId = friendId;
        }

        /**
         * MobileNumber : 8980622116
         * IsFollow : false
         */

        @SerializedName("MobileNumber")
        private String MobileNumber;
        @SerializedName("IsFollow")
        private boolean IsFollow;
         @SerializedName("MutualFriendCount")
        private int MutualFriendCount;

        private ChatOperationsListener chatOperationsListener;
        private OnPresenceChangeListener onPresenceChangeListener;

        public ChatOperationsListener getChatOperationsListener()
        {
            return chatOperationsListener;
        }

        public void setChatOperationsListener(ChatOperationsListener chatOperationsListener)
        {
            this.chatOperationsListener = chatOperationsListener;
        }
        public int getMutualFriendCount() {
            return MutualFriendCount;
        }

        public void setMutualFriendCount(int mutualFriendCount) {
            MutualFriendCount = mutualFriendCount;
        }

        public FriendActionListener getFriendActionListener()
        {
            return friendActionListener;
        }

        public void setFriendActionListener(FriendActionListener friendActionListener)
        {
            this.friendActionListener = friendActionListener;
        }

        public OnPresenceChangeListener getOnPresenceChangeListener()
        {
            return onPresenceChangeListener;
        }

        public void setOnPresenceChangeListener(OnPresenceChangeListener onPresenceChangeListener)
        {
            this.onPresenceChangeListener = onPresenceChangeListener;
        }

        public boolean getIsChecked()
        {
            return isChecked;
        }

        public void setIsChecked(boolean isChecked)
        {
            this.isChecked = isChecked;
        }

        public String getId()
        {
            return Id;
        }

        public void setId(String Id)
        {
            this.Id = Id;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getFriendUserId()
        {
            return FriendUserId;
        }

        public void setFriendUserId(String FriendUserId)
        {
            this.FriendUserId = FriendUserId;
        }

        public String getCreatedDateTime()
        {
            return CreatedDateTime;
        }

        public void setCreatedDateTime(String CreatedDateTime)
        {
            this.CreatedDateTime = CreatedDateTime;
        }

        public String getRemoveDateTime()
        {
            return RemoveDateTime;
        }

        public void setRemoveDateTime(String RemoveDateTime)
        {
            this.RemoveDateTime = RemoveDateTime;
        }

        public boolean isIsConfirm()
        {
            return IsConfirm;
        }

        public void setIsConfirm(boolean IsConfirm)
        {
            this.IsConfirm = IsConfirm;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getMobileNumber()
        {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber)
        {
            this.MobileNumber = MobileNumber;
        }

        public boolean isIsFollow()
        {
            return IsFollow;
        }

        public void setIsFollow(boolean IsFollow)
        {
            this.IsFollow = IsFollow;
        }


        public interface ChatOperationsListener
        {
            void initChat(QBUser qbUser, FriendList.DataBean user);
        }

        public interface OnPresenceChangeListener
        {
            void onPresenceChanged(QBPresence qbPresence);
        }

    }

}
