package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/18/2017 at 11:29 AM.
 */

public class CreateEvent
{


    /**
     * EventId : 00000000-0000-0000-0000-000000000000
     * UserId : 54b86048-fec0-4428-81a4-abe30b7a11fc
     * EventImage : sample string 2
     * EventName : sample string 3
     * Host : sample string 4
     * ContactPerson : sample string 5
     * Date : sample string 6
     * Time : sample string 7
     * Location : sample string 8
     * Address : sample string 9
     * Description : sample string 10
     * CreateDate : 2017-01-17T16:35:21.1646543+05:30
     * UpdateDate : 2017-01-17T16:35:21.1646543+05:30
     * IsDelete : false
     */

    @SerializedName("EventId")
    private String EventId;
    @SerializedName("UserId")
    private String UserId;
    @SerializedName("EventImage")
    private String EventImage;
    @SerializedName("EventName")
    private String EventName;
    @SerializedName("Host")
    private String Host;
    @SerializedName("ContactPerson")
    private String ContactPerson;
    @SerializedName("Date")
    private String Date;
    @SerializedName("Time")
    private String Time;
    @SerializedName("Location")
    private String Location;
    @SerializedName("Address")
    private String Address;
    @SerializedName("Description")
    private String Description;
    @SerializedName("CreateDate")
    private String CreateDate;
    @SerializedName("UpdateDate")
    private String UpdateDate;
    @SerializedName("IsDelete")
    private boolean IsDelete;
    /**
     * Latitude : 36.227521
     * Longitude : -115.83171
     */

    @SerializedName("Latitude")
    private double Latitude;
    @SerializedName("Longitude")
    private double Longitude;

    public String getEventId()
    {
        return EventId;
    }

    public void setEventId(String EventId)
    {
        this.EventId = EventId;
    }

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getEventImage()
    {
        return EventImage;
    }

    public void setEventImage(String EventImage)
    {
        this.EventImage = EventImage;
    }

    public String getEventName()
    {
        return EventName;
    }

    public void setEventName(String EventName)
    {
        this.EventName = EventName;
    }

    public String getHost()
    {
        return Host;
    }

    public void setHost(String Host)
    {
        this.Host = Host;
    }

    public String getContactPerson()
    {
        return ContactPerson;
    }

    public void setContactPerson(String ContactPerson)
    {
        this.ContactPerson = ContactPerson;
    }

    public String getDate()
    {
        return Date;
    }

    public void setDate(String Date)
    {
        this.Date = Date;
    }

    public String getTime()
    {
        return Time;
    }

    public void setTime(String Time)
    {
        this.Time = Time;
    }

    public String getLocation()
    {
        return Location;
    }

    public void setLocation(String Location)
    {
        this.Location = Location;
    }

    public String getAddress()
    {
        return Address;
    }

    public void setAddress(String Address)
    {
        this.Address = Address;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String Description)
    {
        this.Description = Description;
    }

    public String getCreateDate()
    {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate)
    {
        this.CreateDate = CreateDate;
    }

    public String getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(String UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public boolean isIsDelete()
    {
        return IsDelete;
    }

    public void setIsDelete(boolean IsDelete)
    {
        this.IsDelete = IsDelete;
    }

    public double getLatitude()
    {
        return Latitude;
    }

    public void setLatitude(double Latitude)
    {
        this.Latitude = Latitude;
    }

    public double getLongitude()
    {
        return Longitude;
    }

    public void setLongitude(double Longitude)
    {
        this.Longitude = Longitude;
    }
}
