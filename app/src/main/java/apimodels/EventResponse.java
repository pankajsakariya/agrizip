package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by murtuzanalawala on 4/21/17.
 */

public class EventResponse {



        @SerializedName("ResponseStatus")

        private Integer responseStatus;
        @SerializedName("ErrorMessage")

        private Object errorMessage;
        @SerializedName("Message")

        private String message;
        @SerializedName("UpdateDate")

        private Object updateDate;
        @SerializedName("Data")

        private Data data;

        public Integer getResponseStatus() {
            return responseStatus;
        }

        public void setResponseStatus(Integer responseStatus) {
            this.responseStatus = responseStatus;
        }

        public Object getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(Object errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(Object updateDate) {
            this.updateDate = updateDate;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    public class Data{
        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        @SerializedName("EventId")

        private String eventId;
        @SerializedName("UserId")

        private String userId;
        @SerializedName("EventImage")

        private String eventImage;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        @SerializedName("Address")

        private String address;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @SerializedName("Description")

        private String description;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        @SerializedName("UserName")

        private String userName;


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEventImage() {
            return eventImage;
        }

        public void setEventImage(String eventImage) {
            this.eventImage = eventImage;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getContactPerson() {
            return contactPerson;
        }

        public void setContactPerson(String contactPerson) {
            this.contactPerson = contactPerson;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        @SerializedName("EventName")

        private String eventName;
        @SerializedName("Host")

        private String host;
        @SerializedName("ContactPerson")

        private String contactPerson;
        @SerializedName("Date")

        private String date;
        @SerializedName("Time")

        private String time;
        @SerializedName("Location")

        private String location;
        @SerializedName("Latitude")

        private Double latitude;
        @SerializedName("Longitude")

        private Double longitude;

    }


}
