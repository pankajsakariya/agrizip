package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 11/21/2016 at 7:52 PM.
 */

public class CountryList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : [{"CountryId":"8f68693b-e1ca-4217-bab3-1522cb6792da","CountryName":"India","CountryCode":"+91","IsDelete":false}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * CountryId : 8f68693b-e1ca-4217-bab3-1522cb6792da
         * CountryName : India
         * CountryCode : +91
         * IsDelete : false
         */

        @SerializedName("CountryId")
        private String CountryId;
        @SerializedName("CountryName")
        private String CountryName;
        @SerializedName("CountryCode")
        private String CountryCode;
        @SerializedName("IsDelete")
        private boolean IsDelete;

        public String getCountryId()
        {
            return CountryId;
        }

        public void setCountryId(String CountryId)
        {
            this.CountryId = CountryId;
        }

        public String getCountryName()
        {
            return CountryName;
        }

        public void setCountryName(String CountryName)
        {
            this.CountryName = CountryName;
        }

        public String getCountryCode()
        {
            return CountryCode;
        }

        public void setCountryCode(String CountryCode)
        {
            this.CountryCode = CountryCode;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }
    }
}
