package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 11/17/2016 at 3:57 PM.
 */

public class NewMobileRegistration
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : {"Mobile":null,"DeviceID":null,"OTP":"555007","Response":"Success"}
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private DataBean Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public DataBean getData()
    {
        return Data;
    }

    public void setData(DataBean Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * Mobile : null
         * DeviceID : null
         * OTP : 555007
         * Response : Success
         */

        @SerializedName("Mobile")
        private Object Mobile;
        @SerializedName("DeviceID")
        private Object DeviceID;
        @SerializedName("OTP")
        private String OTP;
        @SerializedName("Response")
        private String Response;

        public Object getMobile()
        {
            return Mobile;
        }

        public void setMobile(Object Mobile)
        {
            this.Mobile = Mobile;
        }

        public Object getDeviceID()
        {
            return DeviceID;
        }

        public void setDeviceID(Object DeviceID)
        {
            this.DeviceID = DeviceID;
        }

        public String getOTP()
        {
            return OTP;
        }

        public void setOTP(String OTP)
        {
            this.OTP = OTP;
        }

        public String getResponse()
        {
            return Response;
        }

        public void setResponse(String Response)
        {
            this.Response = Response;
        }
    }
}
