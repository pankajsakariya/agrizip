package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 11/21/2016 at 4:11 PM.
 */

public class DistrictList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : [{"DistrictId":"3ea9f186-78ef-4722-8267-0267ea0a5073","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Bharuch","IsDelete":false},{"DistrictId":"d0b4a605-640d-48dc-ad29-0bace989a9fe","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Chhota Udepur","IsDelete":false},{"DistrictId":"a34e94fe-a201-4fa5-9f53-1b3c1f5b6219","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Kachchh","IsDelete":false},{"DistrictId":"a58ef38d-c6fd-4370-9f13-2e3f180e8437","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Banaskantha (Palanpur)","IsDelete":false},{"DistrictId":"15ee373d-bf6b-4d89-8470-2fffc9af6f1a","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Morbi","IsDelete":false},{"DistrictId":"5d66a973-d43d-46a6-9aa3-3ed8bb81578d","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Valsad","IsDelete":false},{"DistrictId":"f5940e82-a45f-4195-9afa-40eef8c16d76","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Jamnagar","IsDelete":false},{"DistrictId":"748f9c44-0cbd-45ee-9425-4466f2fd5555","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Junagadh","IsDelete":false},{"DistrictId":"0988a000-5e8c-4f84-ba0d-4558ef9c4b49","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Narmada (Rajpipla)","IsDelete":false},{"DistrictId":"f4a9dff3-91af-45ef-80c8-4bef0ab4132e","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Tapi (Vyara)","IsDelete":false},{"DistrictId":"ba978bff-f66c-4465-912b-4c29395e1a7f","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Amreli","IsDelete":false},{"DistrictId":"113df981-110d-4364-9240-4f8a16f595f6","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Devbhoomi Dwarka","IsDelete":false},{"DistrictId":"6e4addb6-48b5-45e3-b7e8-5155f0626c90","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Gir Somnath","IsDelete":false},{"DistrictId":"ac0ec64e-dbca-400c-bb70-53f9a339de1e","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Botad","IsDelete":false},{"DistrictId":"10f0c29f-c790-417c-9550-59655816fdf8","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Ahmedabad","IsDelete":false},{"DistrictId":"3887c40e-df8a-44ab-b1ea-7977d2798eac","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Mahisagar","IsDelete":false},{"DistrictId":"092d4da6-6fe7-4a9f-aa98-7ccac0656e51","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Vadodara","IsDelete":false},{"DistrictId":"6b171f35-09b3-4796-8117-9d036ef5d27f","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Bhavnagar","IsDelete":false},{"DistrictId":"cbe90681-3ac5-4ce3-bede-a324b9f88a4c","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Kheda (Nadiad)","IsDelete":false},{"DistrictId":"8f3560ca-56a9-4ca6-ba1b-a9b49765b96e","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Gandhinagar","IsDelete":false},{"DistrictId":"fbf5e975-3887-48fb-9974-b11a825b007b","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Sabarkantha (Himmatnagar)","IsDelete":false},{"DistrictId":"2d004579-728b-442e-8dab-b7051d837c45","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Mehsana","IsDelete":false},{"DistrictId":"cf560ef5-059c-45db-af81-c18df100da91","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Surat","IsDelete":false},{"DistrictId":"ba0cb3f5-1de3-4a5e-b4b6-c3959986be6b","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Rajkot","IsDelete":false},{"DistrictId":"3f449a42-31f3-46b0-bb73-c9624ea29a4b","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Dahod","IsDelete":false},{"DistrictId":"3fb5677d-8a87-4ad4-860b-cffd3718bf73","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Anand","IsDelete":false},{"DistrictId":"950a25a0-4681-4b9a-aabe-d64760c91f08","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Panchmahal (Godhra)","IsDelete":false},{"DistrictId":"c375f77f-62b3-4ca4-bd79-e3a2b15539c1","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Surendranagar","IsDelete":false},{"DistrictId":"155c5de2-9c42-4f28-aa5d-e963fc578ab4","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Navsari","IsDelete":false},{"DistrictId":"f55684bb-2035-4754-ae30-f21dfd23e4e5","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Patan","IsDelete":false},{"DistrictId":"70927542-cd25-4878-aa07-f90e62467522","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Dangs (Ahwa)","IsDelete":false},{"DistrictId":"19bc0c44-87c5-4a16-83a7-fb4ccfc78183","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Aravalli","IsDelete":false},{"DistrictId":"52f88eec-9275-4363-90bf-ff57d955b699","StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","DistName":"Porbandar","IsDelete":false}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * DistrictId : 3ea9f186-78ef-4722-8267-0267ea0a5073
         * StateId : e0cfbdfe-8560-4980-a0cd-ced2862ff133
         * DistName : Bharuch
         * IsDelete : false
         */

        @SerializedName("DistrictId")
        private String DistrictId;
        @SerializedName("StateId")
        private String StateId;
        @SerializedName("DistName")
        private String DistName;
        @SerializedName("IsDelete")
        private boolean IsDelete;

        public String getDistrictId()
        {
            return DistrictId;
        }

        public void setDistrictId(String DistrictId)
        {
            this.DistrictId = DistrictId;
        }

        public String getStateId()
        {
            return StateId;
        }

        public void setStateId(String StateId)
        {
            this.StateId = StateId;
        }

        public String getDistName()
        {
            return DistName;
        }

        public void setDistName(String DistName)
        {
            this.DistName = DistName;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }
    }
}
