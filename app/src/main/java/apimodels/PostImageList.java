package apimodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import moaritem.MoarItem;

/**
 * Created by rutvik on 1/11/2017 at 5:15 PM.
 */

public class PostImageList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"ImageId":"f6850af1-ba62-4a82-bf7e-354ff444cc80","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468422829528.jpeg"},{"ImageId":"c89daeb1-6735-4d44-b127-38ce0f0b04d5","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468394745946.jpeg"},{"ImageId":"170f3f09-5f7b-4d63-b4ba-40865c97be3a","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468539384334.jpeg"},{"ImageId":"51ca1d2f-2f2a-47af-aa31-4563107eaaab","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468603732537.jpeg"},{"ImageId":"11255301-17a1-4960-b45a-8c4d839d62fa","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468451153178.jpeg"},{"ImageId":"51daf6d8-040f-46b0-8b11-91f0baf66a62","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468515061275.jpeg"},{"ImageId":"d0341c77-4d4b-4d58-a46a-b1674711311d","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468330837817.jpeg"},{"ImageId":"f87f3cbc-9643-46ab-8c49-c503a75cedee","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468362431842.jpeg"},{"ImageId":"0a89a238-a82f-465a-893e-d05600fe81da","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468486167574.jpeg"},{"ImageId":"5152c5e6-1d20-4bbf-80f4-f2f6017c8e70","StatusId":"84e931d6-f59b-422e-a2e7-54172b8fd76d","ImagePath":"http://social.vinayakpharmaceuticals.com/UserImage/636196468571898482.jpeg"}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean extends MoarItem implements Parcelable
    {
        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>()
        {
            @Override
            public DataBean createFromParcel(Parcel source)
            {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size)
            {
                return new DataBean[size];
            }
        };
        /**
         * ImageId : f6850af1-ba62-4a82-bf7e-354ff444cc80
         * StatusId : 84e931d6-f59b-422e-a2e7-54172b8fd76d
         * ImagePath : http://social.vinayakpharmaceuticals.com/UserImage/636196468422829528.jpeg
         */

        @SerializedName("ImageId")
        private String ImageId;
        @SerializedName("StatusId")
        private String StatusId;
        @SerializedName("ImagePath")
        private String ImagePath;
        private int colSpan, rowSpan, position;

        public DataBean()
        {
        }

        protected DataBean(Parcel in)
        {
            this.ImageId = in.readString();
            this.StatusId = in.readString();
            this.ImagePath = in.readString();
            this.colSpan = in.readInt();
            this.rowSpan = in.readInt();
            this.position = in.readInt();
        }

        public int getColSpan()
        {
            return colSpan;
        }

        public void setColSpan(int colSpan)
        {
            this.colSpan = colSpan;
        }

        @Override
        public int getColumnSpan()
        {
            return colSpan;
        }

        @Override
        public int getRowSpan()
        {
            return rowSpan;
        }

        public void setRowSpan(int rowSpan)
        {
            this.rowSpan = rowSpan;
        }

        public int getPosition()
        {
            return position;
        }

        public void setPosition(int position)
        {
            this.position = position;
        }

        @Override
        public String getImagestr()
        {
            return getImagePath();
        }

        public String getImageId()
        {
            return ImageId;
        }

        public void setImageId(String ImageId)
        {
            this.ImageId = ImageId;
        }

        public String getStatusId()
        {
            return StatusId;
        }

        public void setStatusId(String StatusId)
        {
            this.StatusId = StatusId;
        }

        public String getImagePath()
        {
            return ImagePath;
        }

        public void setImagePath(String ImagePath)
        {
            this.ImagePath = ImagePath;
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeString(this.ImageId);
            dest.writeString(this.StatusId);
            dest.writeString(this.ImagePath);
            dest.writeInt(this.colSpan);
            dest.writeInt(this.rowSpan);
            dest.writeInt(this.position);
        }
    }
}
