package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by murtuzanalawala on 4/21/17.
 */

public class JobResponse {


    @SerializedName("ResponseStatus")

    private Integer responseStatus;
    @SerializedName("ErrorMessage")

    private Object errorMessage;
    @SerializedName("Message")

    private String message;
    @SerializedName("UpdateDate")

    private Object updateDate;
    @SerializedName("Data")

    private Data data;

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }



    public class Data{

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        @SerializedName("JobId")

        private String jobId;
    }

}
