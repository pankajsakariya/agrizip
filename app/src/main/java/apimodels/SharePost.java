package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 2/11/2017 at 2:11 PM.
 */

public class SharePost
{


    /**
     * IsCheckIn : false
     * Message : write message
     * Place : user enter place name
     * CreateDatetime : 2016-11-23T18:52:07.073876+05:30
     * ShareId : 1b00e996-5dbd-4744-84cc-6e3ba634c173
     * UserID : 6d3abfa5-bfd1-4a79-b9db-05d0348e1579
     * CheckInID : caa946cd-7a56-421e-8a20-9f764a94cf63
     * StatusId : 0fc7b6a9-64f8-4c88-8394-acbe1ba4716b
     * Status : Hello good morning
     * TagTo :
     * FeelingName : awesome
     * FeelingValue : :)
     * PostYear : 2016
     * PostMonth : November
     * PostDay : 22
     * PostHour : 11
     * PostMinute : 53
     * Help : false
     * News : false
     * VisibleTo : e19750dd-8bf4-4c73-bbc3-feb014dc0f65
     * CreateDate : 2016-11-22T11:42:17.3804268+05:30
     * UpdateDate : 2016-11-22T11:42:17.3804268+05:30
     * IsDelete : false
     * Tagpeople : [{"TagId":"00000000-0000-0000-0000-000000000000","ShareID":"00000000-0000-0000-0000-000000000000","TaggedBy":"5476BD6B-8CDA-4F95-A203-B2FD8FC46861","TaggedTo":"5A5EF7B1-3362-4AD2-875C-F578E5CBA562","IsDelete":false}]
     */

    @SerializedName("IsCheckIn")
    private boolean IsCheckIn;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Place")
    private String Place;
    @SerializedName("CreateDatetime")
    private String CreateDatetime;
    @SerializedName("ShareId")
    private String ShareId;
    @SerializedName("UserID")
    private String UserID;
    @SerializedName("CheckInID")
    private String CheckInID;
    @SerializedName("StatusId")
    private String StatusId;
    @SerializedName("Status")
    private String Status;
    @SerializedName("TagTo")
    private String TagTo;
    @SerializedName("FeelingName")
    private String FeelingName;
    @SerializedName("FeelingValue")
    private String FeelingValue;
    @SerializedName("PostYear")
    private String PostYear;
    @SerializedName("PostMonth")
    private String PostMonth;
    @SerializedName("PostDay")
    private String PostDay;
    @SerializedName("PostHour")
    private String PostHour;
    @SerializedName("PostMinute")
    private String PostMinute;
    @SerializedName("Help")
    private boolean Help;
    @SerializedName("News")
    private boolean News;
    @SerializedName("VisibleTo")
    private String VisibleTo;
    @SerializedName("CreateDate")
    private String CreateDate;
    @SerializedName("UpdateDate")
    private String UpdateDate;
    @SerializedName("IsDelete")
    private boolean IsDelete;
    @SerializedName("Tagpeople")
    private List<TagpeopleBean> Tagpeople;

    public boolean isIsCheckIn()
    {
        return IsCheckIn;
    }

    public void setIsCheckIn(boolean IsCheckIn)
    {
        this.IsCheckIn = IsCheckIn;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public String getPlace()
    {
        return Place;
    }

    public void setPlace(String Place)
    {
        this.Place = Place;
    }

    public String getCreateDatetime()
    {
        return CreateDatetime;
    }

    public void setCreateDatetime(String CreateDatetime)
    {
        this.CreateDatetime = CreateDatetime;
    }

    public String getShareId()
    {
        return ShareId;
    }

    public void setShareId(String ShareId)
    {
        this.ShareId = ShareId;
    }

    public String getUserID()
    {
        return UserID;
    }

    public void setUserID(String UserID)
    {
        this.UserID = UserID;
    }

    public String getCheckInID()
    {
        return CheckInID;
    }

    public void setCheckInID(String CheckInID)
    {
        this.CheckInID = CheckInID;
    }

    public String getStatusId()
    {
        return StatusId;
    }

    public void setStatusId(String StatusId)
    {
        this.StatusId = StatusId;
    }

    public String getStatus()
    {
        return Status;
    }

    public void setStatus(String Status)
    {
        this.Status = Status;
    }

    public String getTagTo()
    {
        return TagTo;
    }

    public void setTagTo(String TagTo)
    {
        this.TagTo = TagTo;
    }

    public String getFeelingName()
    {
        return FeelingName;
    }

    public void setFeelingName(String FeelingName)
    {
        this.FeelingName = FeelingName;
    }

    public String getFeelingValue()
    {
        return FeelingValue;
    }

    public void setFeelingValue(String FeelingValue)
    {
        this.FeelingValue = FeelingValue;
    }

    public String getPostYear()
    {
        return PostYear;
    }

    public void setPostYear(String PostYear)
    {
        this.PostYear = PostYear;
    }

    public String getPostMonth()
    {
        return PostMonth;
    }

    public void setPostMonth(String PostMonth)
    {
        this.PostMonth = PostMonth;
    }

    public String getPostDay()
    {
        return PostDay;
    }

    public void setPostDay(String PostDay)
    {
        this.PostDay = PostDay;
    }

    public String getPostHour()
    {
        return PostHour;
    }

    public void setPostHour(String PostHour)
    {
        this.PostHour = PostHour;
    }

    public String getPostMinute()
    {
        return PostMinute;
    }

    public void setPostMinute(String PostMinute)
    {
        this.PostMinute = PostMinute;
    }

    public boolean isHelp()
    {
        return Help;
    }

    public void setHelp(boolean Help)
    {
        this.Help = Help;
    }

    public boolean isNews()
    {
        return News;
    }

    public void setNews(boolean News)
    {
        this.News = News;
    }

    public String getVisibleTo()
    {
        return VisibleTo;
    }

    public void setVisibleTo(String VisibleTo)
    {
        this.VisibleTo = VisibleTo;
    }

    public String getCreateDate()
    {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate)
    {
        this.CreateDate = CreateDate;
    }

    public String getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(String UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public boolean isIsDelete()
    {
        return IsDelete;
    }

    public void setIsDelete(boolean IsDelete)
    {
        this.IsDelete = IsDelete;
    }

    public List<TagpeopleBean> getTagpeople()
    {
        return Tagpeople;
    }

    public void setTagpeople(List<TagpeopleBean> Tagpeople)
    {
        this.Tagpeople = Tagpeople;
    }

    public static class TagpeopleBean
    {
        /**
         * TagId : 00000000-0000-0000-0000-000000000000
         * ShareID : 00000000-0000-0000-0000-000000000000
         * TaggedBy : 5476BD6B-8CDA-4F95-A203-B2FD8FC46861
         * TaggedTo : 5A5EF7B1-3362-4AD2-875C-F578E5CBA562
         * IsDelete : false
         */

        @SerializedName("TagId")
        private String TagId;
        @SerializedName("ShareID")
        private String ShareID;
        @SerializedName("TaggedBy")
        private String TaggedBy;
        @SerializedName("TaggedTo")
        private String TaggedTo;
        @SerializedName("IsDelete")
        private boolean IsDelete;

        public String getTagId()
        {
            return TagId;
        }

        public void setTagId(String TagId)
        {
            this.TagId = TagId;
        }

        public String getShareID()
        {
            return ShareID;
        }

        public void setShareID(String ShareID)
        {
            this.ShareID = ShareID;
        }

        public String getTaggedBy()
        {
            return TaggedBy;
        }

        public void setTaggedBy(String TaggedBy)
        {
            this.TaggedBy = TaggedBy;
        }

        public String getTaggedTo()
        {
            return TaggedTo;
        }

        public void setTaggedTo(String TaggedTo)
        {
            this.TaggedTo = TaggedTo;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }
    }
}
