package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 2/13/2017 at 3:39 PM.
 */

public class SingleStory
{

    /**
     * id : 1467
     * title : Success Story: Pomegranate farmer awarded &#8220;Padm Shri&#8221;
     * link : http://www.agrizip.com/success-story-pomegranate-farmer-nominated-padm-shri-award/
     * image : http://www.agrizip.com/wp-content/uploads/2017/01/success-story-pomegranate-farmer-nominated-padm-shri-award.jpg
     * short_description : <p>Genabhai Dargabhai Patel who is known as Anar Dada is going to get Padmashri award this year. Genabhai is divyang farmer from Sarkari Golia village of Banaskantha district in North Gujarat. He transformed drought-hit Banaskantha district of North Gujarat into major pomegranate producer. This year’s Padma Shri selectee Genabhai Patel, a 52 year Divyang farmer [&hellip;]</p>

     * long_description : <p>Genabhai Dargabhai Patel who is known as Anar Dada is going to get Padmashri award this year. Genabhai is divyang farmer from Sarkari Golia village of Banaskantha district in North Gujarat. He transformed drought-hit Banaskantha district of North Gujarat into major pomegranate producer.</p>
     <p>This year’s Padma Shri selectee Genabhai Patel, a 52 year Divyang farmer from Banaskantha district in Gujarat, today said that he was so overwhelmed with joy on getting a phone call from Delhi informing him that his name has been included in Padma Awardees list, that he virtually became speechless for a few minutes.</p>
     <p>Genabhai, popularly known as ‘Anar Dada’ in and around his village Sarkari Golia under Deesa Taluka for his laudable contribution to pomegranate cultivation, had also got a mention in Prime Minister Narendra Modi’s speech in Bansakantha recently.</p>
     <p>Genabhai reportedly took to horticulture after being inspired from the “Krishi Mahotsav” organised by Modi when he was the CM in 2003-04 and had started pomegranate cultivation using drip irrigation systems.  He had started with five hectares of cultivation. Today, his pomegranate crop is covered in 12 hectares.</p>
     <p>Despite being handicapped with both legs he successfully experimented with pomegranate cultivation in an area notorious for scanty availability of water and rain. He used scientific techniques, drip irrigation and even CCTV’s he used.</p>
     <p>Genabhai had awarded around 17 awards till date among them 4 are National Awards. He had inspire lots of farmers to horticultural farming. He had guided farmers from plantation to fruit harvesting who had visited his farm with pure intention results in huge plantation of pomegranate crop in North Gujarat, Saurastra as well as near by districts of Rajasthan state. He had kept Farm visitor&#8217;s book where there are around 35000 visitors whom he guided for profitable Anar cultivation where water availability is low.</p>
     <p>Genabhai Dargabhai Patel is farmer who &#8220;Goes Beyond Possible&#8221; with his inability and has established a inspiration mark among the farming communities.</p>

     * date : 2017-01-26
     * time : 07:46:18
     * author : Editor
     */

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("link")
    private String link;
    @SerializedName("image")
    private String image;
    @SerializedName("short_description")
    private String shortDescription;
    @SerializedName("long_description")
    private String longDescription;
    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;
    @SerializedName("author")
    private String author;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getLink()
    {
        return link;
    }

    public void setLink(String link)
    {
        this.link = link;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription()
    {
        return longDescription;
    }

    public void setLongDescription(String longDescription)
    {
        this.longDescription = longDescription;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }
}
