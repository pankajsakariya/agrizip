package apimodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alpesh on 4/5/2017.
 */

public class JobHideResponse {


        @SerializedName("ResponseStatus")
        @Expose
        private Integer responseStatus;
        @SerializedName("ErrorMessage")
        @Expose
        private Object errorMessage;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("UpdateDate")
        @Expose
        private Object updateDate;
 /*       @SerializedName("Data")
        @Expose
        private Data data;
*/
        public Integer getResponseStatus() {
            return responseStatus;
        }

        public void setResponseStatus(Integer responseStatus) {
            this.responseStatus = responseStatus;
        }

        public Object getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(Object errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(Object updateDate) {
            this.updateDate = updateDate;
        }

  /*      public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }*/

    }

