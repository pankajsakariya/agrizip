package apimodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 2/3/2017 at 11:29 AM.
 */

public class StoryList implements Parcelable
{


    public static final Parcelable.Creator<StoryList> CREATOR = new Parcelable.Creator<StoryList>()
    {
        @Override
        public StoryList createFromParcel(Parcel source)
        {
            return new StoryList(source);
        }

        @Override
        public StoryList[] newArray(int size)
        {
            return new StoryList[size];
        }
    };
    /**
     * id : 1467
     * title : Success Story: Pomegranate farmer awarded &#8220;Padm Shri&#8221;
     * image : http://www.agrizip.com/wp-content/uploads/2017/01/success-story-pomegranate-farmer-nominated-padm-shri-award.jpg
     * short_description : <p>Genabhai Dargabhai Patel who is known as Anar Dada is going to get Padmashri award this year. Genabhai is divyang farmer from Sarkari Golia village of Banaskantha district in North Gujarat. He transformed drought-hit Banaskantha district of North Gujarat into major pomegranate producer. This year’s Padma Shri selectee Genabhai Patel, a 52 year Divyang farmer [&hellip;]</p>
     * <p>
     * date : 2017-01-26
     * time : 07:46:18
     * author : Editor
     */

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("short_description")
    private String shortDescription;
    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;
    @SerializedName("author")
    private String author;

    public StoryList()
    {
    }

    protected StoryList(Parcel in)
    {
        this.id = in.readInt();
        this.title = in.readString();
        this.image = in.readString();
        this.shortDescription = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.author = in.readString();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.image);
        dest.writeString(this.shortDescription);
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeString(this.author);
    }
}
