package apimodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import moaritem.MoarItem;

/**
 * Created by rutvik on 1/4/2017 at 7:44 PM.
 */

public class GetAlbums
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"UserId":"abb92a99-37ef-430b-8796-01880b462083","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","AlbumName":"10 images","AlbumDescription":"hello","AlbumVisibleTo":"00000000-0000-0000-0000-000000000000","Photos":[{"PhotoID":"afb3ce45-fbb1-4ed9-92c9-1974ed51e3b4","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281456244772.jpeg"},{"PhotoID":"15541113-464e-4f92-b8b6-434b11813379","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281503650813.jpeg"},{"PhotoID":"11866c49-9278-4b93-9929-48edf891d909","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281356922164.jpeg"},{"PhotoID":"9c4fd8f4-7188-4d10-903a-542f71bbb21b","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281623115959.jpeg"},{"PhotoID":"fc9b930c-b0c5-45e7-b361-65775c0af093","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281481438000.jpeg"},{"PhotoID":"42a75bf2-ff9c-4e1d-a700-6837fcdd59d3","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281587601527.jpeg"},{"PhotoID":"3cf6edbe-b2e1-4642-82f5-98d3c2c1e8f4","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281391226612.jpeg"},{"PhotoID":"dc4edb95-4069-4318-97c4-9b7845af5d51","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281524933474.jpeg"},{"PhotoID":"24f0b1f2-12a9-4aad-9756-aa38dcd10e5c","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281555957509.jpeg"},{"PhotoID":"6b6ea489-62d3-48eb-add3-ca7941809ec2","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281430961537.jpeg"}]}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * UserId : abb92a99-37ef-430b-8796-01880b462083
         * AlbumID : 09db2d5a-2005-4ddb-b3b7-7d65664b90cb
         * AlbumName : 10 images
         * AlbumDescription : hello
         * AlbumVisibleTo : 00000000-0000-0000-0000-000000000000
         * Photos : [{"PhotoID":"afb3ce45-fbb1-4ed9-92c9-1974ed51e3b4","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281456244772.jpeg"},{"PhotoID":"15541113-464e-4f92-b8b6-434b11813379","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281503650813.jpeg"},{"PhotoID":"11866c49-9278-4b93-9929-48edf891d909","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281356922164.jpeg"},{"PhotoID":"9c4fd8f4-7188-4d10-903a-542f71bbb21b","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281623115959.jpeg"},{"PhotoID":"fc9b930c-b0c5-45e7-b361-65775c0af093","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281481438000.jpeg"},{"PhotoID":"42a75bf2-ff9c-4e1d-a700-6837fcdd59d3","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281587601527.jpeg"},{"PhotoID":"3cf6edbe-b2e1-4642-82f5-98d3c2c1e8f4","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281391226612.jpeg"},{"PhotoID":"dc4edb95-4069-4318-97c4-9b7845af5d51","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281524933474.jpeg"},{"PhotoID":"24f0b1f2-12a9-4aad-9756-aa38dcd10e5c","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281555957509.jpeg"},{"PhotoID":"6b6ea489-62d3-48eb-add3-ca7941809ec2","AlbumID":"09db2d5a-2005-4ddb-b3b7-7d65664b90cb","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636192281430961537.jpeg"}]
         */

        @SerializedName("UserId")
        private String UserId;
        @SerializedName("AlbumID")
        private String AlbumID;
        @SerializedName("AlbumName")
        private String AlbumName;
        @SerializedName("AlbumDescription")
        private String AlbumDescription;

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String createDate) {
            CreateDate = createDate;
        }

        @SerializedName("CreateDate")
        private String CreateDate;
        @SerializedName("AlbumVisibleTo")
        private String AlbumVisibleTo;
        @SerializedName("Photos")
        private List<PhotosBean> Photos;

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getAlbumID()
        {
            return AlbumID;
        }

        public void setAlbumID(String AlbumID)
        {
            this.AlbumID = AlbumID;
        }

        public String getAlbumName()
        {
            return AlbumName;
        }

        public void setAlbumName(String AlbumName)
        {
            this.AlbumName = AlbumName;
        }

        public String getAlbumDescription()
        {
            return AlbumDescription;
        }

        public void setAlbumDescription(String AlbumDescription)
        {
            this.AlbumDescription = AlbumDescription;
        }

        public String getAlbumVisibleTo()
        {
            return AlbumVisibleTo;
        }

        public void setAlbumVisibleTo(String AlbumVisibleTo)
        {
            this.AlbumVisibleTo = AlbumVisibleTo;
        }

        public List<PhotosBean> getPhotos()
        {
            if (Photos == null)
            {
                return new ArrayList<PhotosBean>();
            }
            return Photos;
        }

        public void setPhotos(List<PhotosBean> Photos)
        {
            this.Photos = Photos;
        }

        public static class PhotosBean extends MoarItem implements Parcelable
        {
            public static final Creator<PhotosBean> CREATOR = new Creator<PhotosBean>()
            {
                @Override
                public PhotosBean createFromParcel(Parcel source)
                {
                    return new PhotosBean(source);
                }

                @Override
                public PhotosBean[] newArray(int size)
                {
                    return new PhotosBean[size];
                }
            };
            /**
             * PhotoID : afb3ce45-fbb1-4ed9-92c9-1974ed51e3b4
             * AlbumID : 09db2d5a-2005-4ddb-b3b7-7d65664b90cb
             * Imagestr : http://social.vinayakpharmaceuticals.com/UserImage/636192281456244772.jpeg
             */

            @SerializedName("PhotoID")
            private String PhotoID;
            @SerializedName("AlbumID")
            private String AlbumID;
            @SerializedName("Imagestr")
            private String Imagestr;
            private int colSpan, rowSpan, position;
            private boolean isInEditMode, isInDeleteMode;

            public PhotosBean()
            {
            }

            protected PhotosBean(Parcel in)
            {
                this.PhotoID = in.readString();
                this.AlbumID = in.readString();
                this.Imagestr = in.readString();
                this.colSpan = in.readInt();
                this.rowSpan = in.readInt();
                this.position = in.readInt();
                this.isInEditMode = in.readByte() != 0;
                this.isInDeleteMode = in.readByte() != 0;
            }

            public int getColSpan()
            {
                return colSpan;
            }

            public void setColSpan(int colSpan)
            {
                this.colSpan = colSpan;
            }

            public int getPosition()
            {
                return position;
            }

            public void setPosition(int position)
            {
                this.position = position;
            }

            public String getPhotoID()
            {
                return PhotoID;
            }

            public void setPhotoID(String PhotoID)
            {
                this.PhotoID = PhotoID;
            }

            public String getAlbumID()
            {
                return AlbumID;
            }

            public void setAlbumID(String AlbumID)
            {
                this.AlbumID = AlbumID;
            }

            public String getImagestr()
            {
                return Imagestr;
            }

            public void setImagestr(String Imagestr)
            {
                this.Imagestr = Imagestr;
            }

            @Override
            public int getColumnSpan()
            {
                return colSpan;
            }

            @Override
            public int getRowSpan()
            {
                return rowSpan;
            }

            public void setRowSpan(int rowSpan)
            {
                this.rowSpan = rowSpan;
            }

            @Override
            public int describeContents()
            {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags)
            {
                super.writeToParcel(dest, flags);
                dest.writeString(this.PhotoID);
                dest.writeString(this.AlbumID);
                dest.writeString(this.Imagestr);
                dest.writeInt(this.colSpan);
                dest.writeInt(this.rowSpan);
                dest.writeInt(this.position);
                dest.writeByte(this.isInEditMode ? (byte) 1 : (byte) 0);
                dest.writeByte(this.isInDeleteMode ? (byte) 1 : (byte) 0);
            }
        }
    }
}
