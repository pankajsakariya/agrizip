package apimodels;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import extras.Utils;

/**
 * Created by macmini2 on 17/03/17.
 */

public class NotificationGetPostModel implements Serializable {

    @SerializedName("user_id")
    private int user_id;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    @SerializedName("message")
    private String Message;

    @SerializedName("userContactNumber")
    private String UserContactNumber;

    @SerializedName("NotificationId")
    @Expose
    private Integer notificationId;
    @SerializedName("InstanceId")
    @Expose
    private String instanceId;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("UserImage")
    @Expose
    private String userImage;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("DataId")
    @Expose
    private String dataId;
    @SerializedName("IsRead")
    @Expose
    private Boolean isRead;
    @SerializedName("NotificationType")

    private Integer NotificationType;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("BlogId")
    @Expose
    private String blogId;
    @SerializedName("Notification")
    @Expose
    private Integer notification;
    @SerializedName("Topic")
    @Expose
    private Object topic;

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public void setNotificationType(Integer notificationType) {
        this.NotificationType = notificationType;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public Integer getNotification() {
        return notification;
    }

    public void setNotification(Integer notification) {
        this.notification = notification;
    }

    public Object getTopic() {
        return topic;
    }

    public void setTopic(Object topic) {
        this.topic = topic;
    }






    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getNotificationType() {
        return NotificationType;
    }

    public void setNotificationType(int notificationType) {
        NotificationType = notificationType;
    }

    public String getUserContactNumber() {
        return UserContactNumber;
    }

    public void setUserContactNumber(String userContactNumber) {
        UserContactNumber = userContactNumber;
    }


}