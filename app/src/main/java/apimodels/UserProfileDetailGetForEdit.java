package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rutvik on 12/14/2016 at 11:10 AM.
 */

public class UserProfileDetailGetForEdit
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : {"UserId":"75e32ba2-34b3-4309-8e6b-48a19850e4a9","FullName":"","UserImage":"","Status":"","CountryName":"","StateName":"","DistrictName":"","EmailAddress":"abc@xyz.com","MobileNumber":"1111111111","Dob":"1992-07-04T12:10:15.4927462+05:30","Gender":"Male","CityId":"507ec09e-9eae-4ddc-b069-965d07a44c73","City":"Ahmedabad","IsCityShow":true,"HometownId":"675203a9-5cf5-40e8-b04b-38cf30a9d4ea","Hometown":"Surat","IsHometownShow":true,"RelationshipId":"79808fde-77fd-4372-b28e-5317484de752","RelationshipStatus":"Single","IsRelationshipShow":true,"FollowUpCount":10,"IsFollowUpShow":true,"Work":[{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Position":".net developer","Company":"Metizsoft","IsWorkShow":true},{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Position":".net developer","Company":"Metizsoft","IsWorkShow":true}],"Education":[{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","IsSchool":true,"IsCollege":true,"IsEducationShow":true},{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","IsSchool":true,"IsCollege":true,"IsEducationShow":true}],"Followup":[{"FollowupId":"1529c8f6-d66e-4925-b254-7de187841533","FollowUser":"3079595c-e101-497e-92ae-dc1c15779b41"},{"FollowupId":"1529c8f6-d66e-4925-b254-7de187841533","FollowUser":"3079595c-e101-497e-92ae-dc1c15779b41"}]}
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private DataBean Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public DataBean getData()
    {
        return Data;
    }

    public void setData(DataBean Data)
    {
        this.Data = Data;
    }

    public static class DataBean implements UserProfileInformation
    {
        /**
         * UserId : 75e32ba2-34b3-4309-8e6b-48a19850e4a9
         * FullName :
         * UserImage :
         * Status :
         * CountryName :
         * StateName :
         * DistrictName :
         * EmailAddress : abc@xyz.com
         * MobileNumber : 1111111111
         * Dob : 1992-07-04T12:10:15.4927462+05:30
         * Gender : Male
         * CityId : 507ec09e-9eae-4ddc-b069-965d07a44c73
         * City : Ahmedabad
         * IsCityShow : true
         * HometownId : 675203a9-5cf5-40e8-b04b-38cf30a9d4ea
         * Hometown : Surat
         * IsHometownShow : true
         * RelationshipId : 79808fde-77fd-4372-b28e-5317484de752
         * RelationshipStatus : Single
         * IsRelationshipShow : true
         * FollowUpCount : 10
         * IsFollowUpShow : true
         * Work : [{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Position":".net developer","Company":"Metizsoft","IsWorkShow":true},{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Position":".net developer","Company":"Metizsoft","IsWorkShow":true}]
         * Education : [{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","IsSchool":true,"IsCollege":true,"IsEducationShow":true},{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","IsSchool":true,"IsCollege":true,"IsEducationShow":true}]
         * Followup : [{"FollowupId":"1529c8f6-d66e-4925-b254-7de187841533","FollowUser":"3079595c-e101-497e-92ae-dc1c15779b41"},{"FollowupId":"1529c8f6-d66e-4925-b254-7de187841533","FollowUser":"3079595c-e101-497e-92ae-dc1c15779b41"}]
         */

        @SerializedName("UserId")
        private String UserId;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("Status")
        private String Status;
        @SerializedName("CountryName")
        private String CountryName;
        @SerializedName("StateName")
        private String StateName;
        @SerializedName("DistrictName")
        private String DistrictName;
        @SerializedName("EmailAddress")
        private String EmailAddress;
        @SerializedName("EmailVisibleTo")
        private String EmailVisibleTo;
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @SerializedName("MobileVisibleTo")
        private String MobileVisibleTo;
        @SerializedName("Dob")
        private String Dob;
        @SerializedName("DOBVisibleTo")
        private String DOBVisibleTo;
        @SerializedName("Gender")
        private String Gender;
        @SerializedName("GenderVisibleTo")
        private String GenderVisibleTo;
        @SerializedName("CityId")
        private String CityId;
        @SerializedName("City")
        private String City;
        @SerializedName("CityVisibleTo")
        private String CityVisibleTo;
        @SerializedName("IsCityShow")
        private boolean IsCityShow;
        @SerializedName("HometownId")
        private String HometownId;
        @SerializedName("Hometown")
        private String Hometown;
        @SerializedName("HometownVisibleTo")
        private String HometownVisibleTo;
        @SerializedName("IsHometownShow")
        private boolean IsHometownShow;
        @SerializedName("RelationshipId")
        private String RelationshipId;
        @SerializedName("RelationshipStatus")
        private String RelationshipStatus;
        @SerializedName("RelationshipVisibleTo")
        private String RelationshipVisibleTo;
        @SerializedName("IsRelationshipShow")
        private boolean IsRelationshipShow;
        @SerializedName("FollowUpCount")
        private int FollowUpCount;
        @SerializedName("IsFollowUpShow")
        private boolean IsFollowUpShow;
        @SerializedName("Work")
        private List<WorkBean> Work = new ArrayList<>();
        @SerializedName("Education")
        private List<EducationBean> Education = new ArrayList<>();
        @SerializedName("Followup")
        private List<FollowupBean> Followup = new ArrayList<>();

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String fullName)
        {
            FullName = fullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String userImage)
        {
            UserImage = userImage;
        }

        public String getStatus()
        {
            return Status;
        }

        public void setStatus(String status)
        {
            Status = status;
        }

        public String getCountryName()
        {
            return CountryName;
        }

        public void setCountryName(String countryName)
        {
            CountryName = countryName;
        }

        public String getStateName()
        {
            return StateName;
        }

        public void setStateName(String stateName)
        {
            StateName = stateName;
        }

        public String getDistrictName()
        {
            return DistrictName;
        }

        public void setDistrictName(String districtName)
        {
            DistrictName = districtName;
        }

        public boolean isCityShow()
        {
            return IsCityShow;
        }

        public void setCityShow(boolean cityShow)
        {
            IsCityShow = cityShow;
        }

        public boolean isHometownShow()
        {
            return IsHometownShow;
        }

        public void setHometownShow(boolean hometownShow)
        {
            IsHometownShow = hometownShow;
        }

        public boolean isRelationshipShow()
        {
            return IsRelationshipShow;
        }

        public void setRelationshipShow(boolean relationshipShow)
        {
            IsRelationshipShow = relationshipShow;
        }

        public boolean isFollowUpShow()
        {
            return IsFollowUpShow;
        }

        public void setFollowUpShow(boolean followUpShow)
        {
            IsFollowUpShow = followUpShow;
        }

        public String getEmailAddress()
        {
            return EmailAddress;
        }

        public void setEmailAddress(String EmailAddress)
        {
            this.EmailAddress = EmailAddress;
        }

        public String getEmailVisibleTo()
        {
            return EmailVisibleTo;
        }

        public void setEmailVisibleTo(String EmailVisibleTo)
        {
            this.EmailVisibleTo = EmailVisibleTo;
        }

        public String getMobileNumber()
        {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber)
        {
            this.MobileNumber = MobileNumber;
        }

        public String getMobileVisibleTo()
        {
            return MobileVisibleTo;
        }

        public void setMobileVisibleTo(String MobileVisibleTo)
        {
            this.MobileVisibleTo = MobileVisibleTo;
        }

        public String getDob()
        {
            return Dob;
        }

        public void setDob(String Dob)
        {
            this.Dob = Dob;
        }

        public String getDOBVisibleTo()
        {
            return DOBVisibleTo;
        }

        public void setDOBVisibleTo(String DOBVisibleTo)
        {
            this.DOBVisibleTo = DOBVisibleTo;
        }

        public String getGender()
        {
            return Gender;
        }

        public void setGender(String Gender)
        {
            this.Gender = Gender;
        }

        public String getGenderVisibleTo()
        {
            return GenderVisibleTo;
        }

        public void setGenderVisibleTo(String GenderVisibleTo)
        {
            this.GenderVisibleTo = GenderVisibleTo;
        }

        public String getCityId()
        {
            return CityId;
        }

        public void setCityId(String CityId)
        {
            this.CityId = CityId;
        }

        public String getCity()
        {
            return City;
        }

        public void setCity(String City)
        {
            this.City = City;
        }

        public String getCityVisibleTo()
        {
            return CityVisibleTo;
        }

        public void setCityVisibleTo(String CityVisibleTo)
        {
            this.CityVisibleTo = CityVisibleTo;
        }

        public boolean isIsCityShow()
        {
            return IsCityShow;
        }

        public void setIsCityShow(boolean IsCityShow)
        {
            this.IsCityShow = IsCityShow;
        }

        public String getHometownId()
        {
            return HometownId;
        }

        public void setHometownId(String HometownId)
        {
            this.HometownId = HometownId;
        }

        public String getHometown()
        {
            return Hometown;
        }

        public void setHometown(String Hometown)
        {
            this.Hometown = Hometown;
        }

        public String getHometownVisibleTo()
        {
            return HometownVisibleTo;
        }

        public void setHometownVisibleTo(String HometownVisibleTo)
        {
            this.HometownVisibleTo = HometownVisibleTo;
        }

        public boolean isIsHometownShow()
        {
            return IsHometownShow;
        }

        public void setIsHometownShow(boolean IsHometownShow)
        {
            this.IsHometownShow = IsHometownShow;
        }

        public String getRelationshipId()
        {
            return RelationshipId;
        }

        public void setRelationshipId(String RelationshipId)
        {
            this.RelationshipId = RelationshipId;
        }

        public String getRelationshipStatus()
        {
            return RelationshipStatus;
        }

        public void setRelationshipStatus(String RelationshipStatus)
        {
            this.RelationshipStatus = RelationshipStatus;
        }

        public String getRelationshipVisibleTo()
        {
            return RelationshipVisibleTo;
        }

        public void setRelationshipVisibleTo(String RelationshipVisibleTo)
        {
            this.RelationshipVisibleTo = RelationshipVisibleTo;
        }

        public boolean isIsRelationshipShow()
        {
            return IsRelationshipShow;
        }

        public void setIsRelationshipShow(boolean IsRelationshipShow)
        {
            this.IsRelationshipShow = IsRelationshipShow;
        }

        public int getFollowUpCount()
        {
            return FollowUpCount;
        }

        public void setFollowUpCount(int FollowUpCount)
        {
            this.FollowUpCount = FollowUpCount;
        }

        public boolean isIsFollowUpShow()
        {
            return IsFollowUpShow;
        }

        public void setIsFollowUpShow(boolean IsFollowUpShow)
        {
            this.IsFollowUpShow = IsFollowUpShow;
        }

        public List<WorkBean> getWork()
        {
            if (Work == null)
            {
                Work = new ArrayList<>();
            }
            return Work;
        }

        public void setWork(List<WorkBean> Work)
        {
            this.Work = Work;
        }

        public List<EducationBean> getEducation()
        {
            if (Education == null)
            {
                Education = new ArrayList<>();
            }
            return Education;
        }

        public void setEducation(List<EducationBean> Education)
        {
            this.Education = Education;
        }

        public List<FollowupBean> getFollowup()
        {
            return Followup;
        }

        public void setFollowup(List<FollowupBean> followup)
        {
            Followup = followup;
        }

        public static class WorkBean
        {
            /**
             * WorkId : 6fb609c0-60d5-4c17-85b4-87bf233cec40
             * Company : Metizsoft
             * Position : .net developer
             * City : Ahmedabad
             * Description : write description
             * TimePeriodFrom : 2016-12-23T18:31:16.0168635+05:30
             * TimePeriodTo : 2016-12-23T18:31:16.0168635+05:30
             * WorkVisibleTo : b44f8a4e-41dc-46af-ad85-1b5b11599268
             * IsWorkShow : true
             */

            @SerializedName("WorkId")
            private String WorkId;
            @SerializedName("Company")
            private String Company;
            @SerializedName("Position")
            private String Position;
            @SerializedName("City")
            private String City;
            @SerializedName("Description")
            private String Description;
            @SerializedName("TimePeriodFrom")
            private String TimePeriodFrom;
            @SerializedName("TimePeriodTo")
            private String TimePeriodTo;
            @SerializedName("WorkVisibleTo")
            private String WorkVisibleTo;
            @SerializedName("IsWorkShow")
            private boolean IsWorkShow;

            public String getWorkId()
            {
                return WorkId;
            }

            public void setWorkId(String WorkId)
            {
                this.WorkId = WorkId;
            }

            public String getCompany()
            {
                return Company;
            }

            public void setCompany(String Company)
            {
                this.Company = Company;
            }

            public String getPosition()
            {
                return Position;
            }

            public void setPosition(String Position)
            {
                this.Position = Position;
            }

            public String getCity()
            {
                return City;
            }

            public void setCity(String City)
            {
                this.City = City;
            }

            public String getDescription()
            {
                return Description;
            }

            public void setDescription(String Description)
            {
                this.Description = Description;
            }

            public String getTimePeriodFrom()
            {
                return TimePeriodFrom;
            }

            public void setTimePeriodFrom(String TimePeriodFrom)
            {
                this.TimePeriodFrom = TimePeriodFrom;
            }

            public String getTimePeriodTo()
            {
                return TimePeriodTo;
            }

            public void setTimePeriodTo(String TimePeriodTo)
            {
                this.TimePeriodTo = TimePeriodTo;
            }

            public String getWorkVisibleTo()
            {
                return WorkVisibleTo;
            }

            public void setWorkVisibleTo(String WorkVisibleTo)
            {
                this.WorkVisibleTo = WorkVisibleTo;
            }

            public boolean isIsWorkShow()
            {
                return IsWorkShow;
            }

            public void setIsWorkShow(boolean IsWorkShow)
            {
                this.IsWorkShow = IsWorkShow;
            }
        }

        public static class EducationBean
        {
            /**
             * EducationId : 29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311
             * InstituteName : GTU
             * City : Ahmedabad
             * TimePeriodFrom : 2016-12-23T18:31:16.0168635+05:30
             * TimePeriodTo : 2016-12-23T18:31:16.0168635+05:30
             * Graduated : true
             * Description : Description
             * IsSchool : true
             * IsCollege : true
             * EducationVisibleTo : 9472d723-54cd-4efe-b83c-d4f17aa539e5
             * IsEducationShow : true
             */

            @SerializedName("EducationId")
            private String EducationId;
            @SerializedName("InstituteName")
            private String InstituteName;
            @SerializedName("City")
            private String City;
            @SerializedName("TimePeriodFrom")
            private String TimePeriodFrom;
            @SerializedName("TimePeriodTo")
            private String TimePeriodTo;
            @SerializedName("Graduated")
            private boolean Graduated;
            @SerializedName("Description")
            private String Description;
            @SerializedName("IsSchool")
            private boolean IsSchool;
            @SerializedName("IsCollege")
            private boolean IsCollege;
            @SerializedName("EducationVisibleTo")
            private String EducationVisibleTo;
            @SerializedName("IsEducationShow")
            private boolean IsEducationShow;

            public String getEducationId()
            {
                return EducationId;
            }

            public void setEducationId(String EducationId)
            {
                this.EducationId = EducationId;
            }

            public String getInstituteName()
            {
                return InstituteName;
            }

            public void setInstituteName(String InstituteName)
            {
                this.InstituteName = InstituteName;
            }

            public String getCity()
            {
                return City;
            }

            public void setCity(String City)
            {
                this.City = City;
            }

            public String getTimePeriodFrom()
            {
                return TimePeriodFrom;
            }

            public void setTimePeriodFrom(String TimePeriodFrom)
            {
                this.TimePeriodFrom = TimePeriodFrom;
            }

            public String getTimePeriodTo()
            {
                return TimePeriodTo;
            }

            public void setTimePeriodTo(String TimePeriodTo)
            {
                this.TimePeriodTo = TimePeriodTo;
            }

            public boolean isGraduated()
            {
                return Graduated;
            }

            public void setGraduated(boolean Graduated)
            {
                this.Graduated = Graduated;
            }

            public String getDescription()
            {
                return Description;
            }

            public void setDescription(String Description)
            {
                this.Description = Description;
            }

            public boolean isIsSchool()
            {
                return IsSchool;
            }

            public void setIsSchool(boolean IsSchool)
            {
                this.IsSchool = IsSchool;
            }

            public boolean isIsCollege()
            {
                return IsCollege;
            }

            public void setIsCollege(boolean IsCollege)
            {
                this.IsCollege = IsCollege;
            }

            public String getEducationVisibleTo()
            {
                return EducationVisibleTo;
            }

            public void setEducationVisibleTo(String EducationVisibleTo)
            {
                this.EducationVisibleTo = EducationVisibleTo;
            }

            public boolean isIsEducationShow()
            {
                return IsEducationShow;
            }

            public void setIsEducationShow(boolean IsEducationShow)
            {
                this.IsEducationShow = IsEducationShow;
            }
        }

        public static class FollowupBean
        {
            /**
             * FollowupId : 1529c8f6-d66e-4925-b254-7de187841533
             * FollowUser : 3079595c-e101-497e-92ae-dc1c15779b41
             */

            @SerializedName("FollowupId")
            private String FollowupId;
            @SerializedName("FollowUser")
            private String FollowUser;

            public String getFollowupId()
            {
                return FollowupId;
            }

            public void setFollowupId(String FollowupId)
            {
                this.FollowupId = FollowupId;
            }

            public String getFollowUser()
            {
                return FollowUser;
            }

            public void setFollowUser(String FollowUser)
            {
                this.FollowUser = FollowUser;
            }
        }
    }
}
