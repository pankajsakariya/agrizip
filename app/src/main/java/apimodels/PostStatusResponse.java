package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 11/22/2016 at 5:58 PM.
 */

public class PostStatusResponse
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : {"IsCheckIn":false,"StatusId":"0b130064-dfe2-4f61-97f9-87e92ea9c38a","UserID":"5a8e7b6c-6a1e-444a-b28a-12d521a71da1","CreateDatetime":"0001-01-01T00:00:00","Message":"sample string 5","Place":"sample string 6","CheckInID":"6ca8e10a-af52-49b7-b659-20d4a7a2aa57","Status":"sample string 8","TagTo":"sample string 9","FeelingName":"sample string 10","FeelingValue":"sample string 11","PostYear":"sample string 12","PostMonth":"sample string 13","PostDay":"sample string 14","PostHour":"sample string 15","PostMinute":"sample string 16","Help":true,"News":true,"VisibleTo":"sample string 19","CreateDate":"2016-11-22T16:38:02.0712482+05:30","UpdateDate":"2016-11-22T16:38:02.0712482+05:30","IsDelete":true,"Images":[],"Tagpeople":[]}
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private SinglePost Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public SinglePost getData()
    {
        return Data;
    }

    public void setData(SinglePost Data)
    {
        this.Data = Data;
    }

}
