package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 12/21/2016 at 12:28 PM.
 */

public class UserPrivacyDetails
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"Id":"4be8a1a6-cbd3-41d0-8959-2248daf0d72d","UserTypeName":"Friends","WhoSeeMyStuff":null,"WhoContactMe":null,"LookMeByEmail":null,"LookMeByMobile":null,"AddToMyTimeline":null,"WhoSeeTaggedTimeline":null,"FollowMe":null,"PostComment":null,"ProfileInfo":null,"Privacy":null},{"Id":"bcfb2fef-17f0-4d16-86b4-ab0a2141449b","UserTypeName":"Only Me","WhoSeeMyStuff":null,"WhoContactMe":null,"LookMeByEmail":null,"LookMeByMobile":null,"AddToMyTimeline":null,"WhoSeeTaggedTimeline":null,"FollowMe":null,"PostComment":null,"ProfileInfo":null,"Privacy":null},{"Id":"e19750dd-8bf4-4c73-bbc3-feb014dc0f65","UserTypeName":"Public","WhoSeeMyStuff":null,"WhoContactMe":null,"LookMeByEmail":null,"LookMeByMobile":null,"AddToMyTimeline":null,"WhoSeeTaggedTimeline":null,"FollowMe":null,"PostComment":null,"ProfileInfo":null,"Privacy":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * Id : 4be8a1a6-cbd3-41d0-8959-2248daf0d72d
         * UserTypeName : Friends
         * WhoSeeMyStuff : null
         * WhoContactMe : null
         * LookMeByEmail : null
         * LookMeByMobile : null
         * AddToMyTimeline : null
         * WhoSeeTaggedTimeline : null
         * FollowMe : null
         * PostComment : null
         * ProfileInfo : null
         * Privacy : null
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("UserTypeName")
        private String UserTypeName;
        @SerializedName("WhoSeeMyStuff")
        private Object WhoSeeMyStuff;
        @SerializedName("WhoContactMe")
        private Object WhoContactMe;
        @SerializedName("LookMeByEmail")
        private Object LookMeByEmail;
        @SerializedName("LookMeByMobile")
        private Object LookMeByMobile;
        @SerializedName("AddToMyTimeline")
        private Object AddToMyTimeline;
        @SerializedName("WhoSeeTaggedTimeline")
        private Object WhoSeeTaggedTimeline;
        @SerializedName("FollowMe")
        private Object FollowMe;
        @SerializedName("PostComment")
        private Object PostComment;
        @SerializedName("ProfileInfo")
        private Object ProfileInfo;
        @SerializedName("Privacy")
        private Object Privacy;

        public String getId()
        {
            return Id;
        }

        public void setId(String Id)
        {
            this.Id = Id;
        }

        public String getUserTypeName()
        {
            return UserTypeName;
        }

        public void setUserTypeName(String UserTypeName)
        {
            this.UserTypeName = UserTypeName;
        }

        public Object getWhoSeeMyStuff()
        {
            return WhoSeeMyStuff;
        }

        public void setWhoSeeMyStuff(Object WhoSeeMyStuff)
        {
            this.WhoSeeMyStuff = WhoSeeMyStuff;
        }

        public Object getWhoContactMe()
        {
            return WhoContactMe;
        }

        public void setWhoContactMe(Object WhoContactMe)
        {
            this.WhoContactMe = WhoContactMe;
        }

        public Object getLookMeByEmail()
        {
            return LookMeByEmail;
        }

        public void setLookMeByEmail(Object LookMeByEmail)
        {
            this.LookMeByEmail = LookMeByEmail;
        }

        public Object getLookMeByMobile()
        {
            return LookMeByMobile;
        }

        public void setLookMeByMobile(Object LookMeByMobile)
        {
            this.LookMeByMobile = LookMeByMobile;
        }

        public Object getAddToMyTimeline()
        {
            return AddToMyTimeline;
        }

        public void setAddToMyTimeline(Object AddToMyTimeline)
        {
            this.AddToMyTimeline = AddToMyTimeline;
        }

        public Object getWhoSeeTaggedTimeline()
        {
            return WhoSeeTaggedTimeline;
        }

        public void setWhoSeeTaggedTimeline(Object WhoSeeTaggedTimeline)
        {
            this.WhoSeeTaggedTimeline = WhoSeeTaggedTimeline;
        }

        public Object getFollowMe()
        {
            return FollowMe;
        }

        public void setFollowMe(Object FollowMe)
        {
            this.FollowMe = FollowMe;
        }

        public Object getPostComment()
        {
            return PostComment;
        }

        public void setPostComment(Object PostComment)
        {
            this.PostComment = PostComment;
        }

        public Object getProfileInfo()
        {
            return ProfileInfo;
        }

        public void setProfileInfo(Object ProfileInfo)
        {
            this.ProfileInfo = ProfileInfo;
        }

        public Object getPrivacy()
        {
            return Privacy;
        }

        public void setPrivacy(Object Privacy)
        {
            this.Privacy = Privacy;
        }
    }
}
