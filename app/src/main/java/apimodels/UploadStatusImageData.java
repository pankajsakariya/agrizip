package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 11/29/2016 at 12:22 PM.
 */

public class UploadStatusImageData
{


    /**
     * Id : 00000000-0000-0000-0000-000000000000
     * StatusID : 00000000-0000-0000-0000-000000000000
     * Imagestr : 9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAINAjQDASIAAhEBAxEB/8QAGwAAAwADAQEAAAAAAAAAAAAAAAECAwUGBAf/xAAaAQEBAAMBAQAAAAAAAAAAAAAAA…..
     * Sort : 1
     * IsDelete : false
     */

    @SerializedName("Id")
    private String Id;
    @SerializedName("StatusID")
    private String StatusID;
    @SerializedName("Imagestr")
    private String Imagestr;
    @SerializedName("Sort")
    private int Sort;
    @SerializedName("IsDelete")
    private boolean IsDelete;

    public String getId()
    {
        return Id;
    }

    public void setId(String Id)
    {
        this.Id = Id;
    }

    public String getStatusID()
    {
        return StatusID;
    }

    public void setStatusID(String StatusID)
    {
        this.StatusID = StatusID;
    }

    public String getImagestr()
    {
        return Imagestr;
    }

    public void setImagestr(String Imagestr)
    {
        this.Imagestr = Imagestr;
    }

    public int getSort()
    {
        return Sort;
    }

    public void setSort(int Sort)
    {
        this.Sort = Sort;
    }

    public boolean isIsDelete()
    {
        return IsDelete;
    }

    public void setIsDelete(boolean IsDelete)
    {
        this.IsDelete = IsDelete;
    }
}
