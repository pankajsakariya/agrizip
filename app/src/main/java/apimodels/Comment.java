package apimodels;

import java.util.List;

/**
 * Created by rutvik on 1/20/2017 at 1:32 PM.
 */

public interface Comment
{

    Type getType();

    String getUserImage();

    String getComment();

    void setComment(String comment);

    String getFullName();

    String getCreateDate();

    String getImagestr();

    int getCommentLikeCount();

    void setCommentLikeCount(int count);

    String getUserID();

    boolean isLiked();

    void setLiked(boolean liked);

    String getStatusID();

    List<CommentsOnPost.DataBean.CommentReplyDetailBean> getCommentReplyDetailForPost();

    void setCommentReplyDetailForPost(List<CommentsOnPost.DataBean.CommentReplyDetailBean> commentReplyList);

    List<HelpListComments.DataBean.CommentReplyDetailBean> getCommentReplyDetailForHelp();

    void setCommentReplyDetailForHelp(List<HelpListComments.DataBean.CommentReplyDetailBean> commentReplyList);

    CommentOperationsListener getCommentOperationsListener();

    void setCommentOperationsListener(CommentOperationsListener commentOperationsListener);

    String getStatusDetailID();

    enum Type
    {
        POST,
        HELP
    }

    interface CommentReply
    {
        CommentReplyOperationsListener getCommentReplyOperationsListener();

        void setCommentReplyOperationsListener(CommentReplyOperationsListener listener);

        String getReplyID();

        String getReplyComment();

        String getReplyUserName();

        String getReplyDate();

        String getReplyUserImage();

        String getReplyUserID();
    }

    public interface CommentReplyOperationsListener
    {
        void onRemoveCommentReply(CommentReply reply);
    }

    public interface CommentOperationsListener
    {
        void onDeleteComment(Comment model);

        void onLikeUnlikeComment(Comment model);

        void onReplied(Comment singleComment, CommentReply model);
    }

}
