package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/31/2017 at 6:20 PM.
 */

public class UpdateProfilePic
{

    /**
     * UserId : ff972b65-4e28-4b13-ae2e-87893d3e58d0
     * Imagestr : sample string 2
     */

    @SerializedName("UserId")
    private String UserId;
    @SerializedName("Imagestr")
    private String Imagestr;

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getImagestr()
    {
        return Imagestr;
    }

    public void setImagestr(String Imagestr)
    {
        this.Imagestr = Imagestr;
    }


    public static class UpdatedProfilePicDetails
    {

        /**
         * ResponseStatus : 1
         * ErrorMessage : null
         * Message : successful
         * UpdateDate : null
         * Data : http://social.vinayakpharmaceuticals.com/UserImage/636214853711750630.jpeg
         */

        @SerializedName("ResponseStatus")
        private int ResponseStatus;
        @SerializedName("ErrorMessage")
        private Object ErrorMessage;
        @SerializedName("Message")
        private String Message;
        @SerializedName("UpdateDate")
        private Object UpdateDate;
        @SerializedName("Data")
        private String Data;

        public int getResponseStatus()
        {
            return ResponseStatus;
        }

        public void setResponseStatus(int ResponseStatus)
        {
            this.ResponseStatus = ResponseStatus;
        }

        public Object getErrorMessage()
        {
            return ErrorMessage;
        }

        public void setErrorMessage(Object ErrorMessage)
        {
            this.ErrorMessage = ErrorMessage;
        }

        public String getMessage()
        {
            return Message;
        }

        public void setMessage(String Message)
        {
            this.Message = Message;
        }

        public Object getUpdateDate()
        {
            return UpdateDate;
        }

        public void setUpdateDate(Object UpdateDate)
        {
            this.UpdateDate = UpdateDate;
        }

        public String getData()
        {
            return Data;
        }

        public void setData(String Data)
        {
            this.Data = Data;
        }
    }

}
