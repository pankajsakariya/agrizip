package apimodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alpesh on 3/15/2017.
 */

public class LikeUnlikeJob {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("JobId")
    @Expose
    private String jobId;
    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("IsLike")
    @Expose
    private Boolean isLike;
    @SerializedName("IsComment")
    @Expose
    private Boolean isComment;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("Imagestr")
    @Expose
    private String imagestr;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("UpdateDate")
    @Expose
    private String updateDate;
    @SerializedName("JobDetailId")
    @Expose
    private String jobDetailId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Boolean getIsLike() {
        return isLike;
    }

    public void setIsLike(Boolean isLike) {
        this.isLike = isLike;
    }

    public Boolean getIsComment() {
        return isComment;
    }

    public void setIsComment(Boolean isComment) {
        this.isComment = isComment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImagestr() {
        return imagestr;
    }

    public void setImagestr(String imagestr) {
        this.imagestr = imagestr;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getJobDetailId() {
        return jobDetailId;
    }

    public void setJobDetailId(String jobDetailId) {
        this.jobDetailId = jobDetailId;
    }

}
