package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 11/21/2016 at 3:14 PM.
 */

public class StateList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * Data : [{"StateId":"5e099a6e-909f-497c-a1ab-06d7430bb1d0","StateName":"Chhattisgarh","IsDelete":false},{"StateId":"21b2db79-81a9-4db2-be0a-087b9986b744","StateName":"West Bengal","IsDelete":false},{"StateId":"584ecfe2-56d6-4921-9992-0b0a9f849b17","StateName":"Jammu and Kashmir","IsDelete":false},{"StateId":"64aa6451-73b4-4e59-868e-1f65bf908cfd","StateName":"Puducherry","IsDelete":false},{"StateId":"5d21bc48-1131-427d-8e3a-243fd3101bce","StateName":"Daman and Diu","IsDelete":false},{"StateId":"b7b8cecb-947f-4730-80da-340a0f02ff1a","StateName":"Rajasthan","IsDelete":false},{"StateId":"9ee0e840-5186-44f3-b048-3bebddffd61a","StateName":"Telangana","IsDelete":false},{"StateId":"fccf62a4-ad40-425b-b72e-3cfdce718502","StateName":"Nagaland","IsDelete":false},{"StateId":"2388177e-17ad-4b94-bcf9-4c2901e53c1e","StateName":"Delhi","IsDelete":false},{"StateId":"f7a78c43-aa44-4a10-a23e-51f076bc0424","StateName":"Jharkhand","IsDelete":false},{"StateId":"832e5d3c-c4df-41be-a0d9-537874fbd1f7","StateName":"Sikkim","IsDelete":false},{"StateId":"27c3b512-678a-474c-8d2b-54077b2b1234","StateName":"Himachal Pradesh","IsDelete":false},{"StateId":"91c1ada8-c643-4054-88e4-592de2bf6dae","StateName":"Odisha","IsDelete":false},{"StateId":"ebf90229-5269-4fe3-8fbc-5b08cdee817a","StateName":"Goa","IsDelete":false},{"StateId":"20283422-5e91-4c52-ab80-64db25664cd2","StateName":"Manipur","IsDelete":false},{"StateId":"f66f2450-d807-4f0d-a601-67483832ed58","StateName":"Bihar","IsDelete":false},{"StateId":"35de8977-7245-464e-9f0e-6a85ada1c502","StateName":"Uttar Pradesh","IsDelete":false},{"StateId":"b8cc6c40-ad3d-44be-8463-6f7b4a942ac3","StateName":"Arunachal Pradesh","IsDelete":false},{"StateId":"a51ecfa2-f319-491a-92d9-70f379a237c9","StateName":"Madhya Pradesh","IsDelete":false},{"StateId":"b97a149d-6c55-4834-a128-715f712a96a2","StateName":"Mizoram","IsDelete":false},{"StateId":"27cab68d-0282-4ea2-9e82-77875b7f97ab","StateName":"Tamil Nadu","IsDelete":false},{"StateId":"0fd17b2b-406b-4b90-a215-9232a68e5327","StateName":"Dadra and Nagar Haveli","IsDelete":false},{"StateId":"a67dfe3f-7584-44e4-a8cf-95eeef758e16","StateName":"Karnataka","IsDelete":false},{"StateId":"79ce4516-7562-4d2e-bfac-a906a54eb786","StateName":"Uttarakhand","IsDelete":false},{"StateId":"fea69e40-4dad-496c-ae7d-abb02d7bd472","StateName":"Lakshadweep","IsDelete":false},{"StateId":"05944a87-79b9-42a5-af58-b87d5121781e","StateName":"Tripura","IsDelete":false},{"StateId":"76e92b05-2ace-401a-9733-c074b53c2bf3","StateName":"Punjab","IsDelete":false},{"StateId":"a155d4bb-374f-439f-a912-c0bde26a0079","StateName":"Maharashtra","IsDelete":false},{"StateId":"4b9b0f6d-6d3c-457b-ab3c-c15daea6e74b","StateName":"Assam","IsDelete":false},{"StateId":"e0cfbdfe-8560-4980-a0cd-ced2862ff133","StateName":"Gujarat","IsDelete":false},{"StateId":"2a8495e7-92b4-46db-a46f-cf78bc63a5c5","StateName":"Andhra Pradesh","IsDelete":false},{"StateId":"66f7a7b6-739e-4980-b789-d5adef75ef53","StateName":"Meghalaya","IsDelete":false},{"StateId":"d13ce697-f754-4f06-92fb-e5b6b13432eb","StateName":"Haryana","IsDelete":false},{"StateId":"bc24a763-3548-46aa-9e82-ea3d37f569d3","StateName":"Andaman and Nicobar Island","IsDelete":false},{"StateId":"34ca0c55-8e76-404f-8ba9-ee2e700798c8","StateName":"Chandigarh","IsDelete":false},{"StateId":"be1d5d23-9a13-4e75-9454-fb1ca19f4194","StateName":"Kerala","IsDelete":false}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * StateId : 5e099a6e-909f-497c-a1ab-06d7430bb1d0
         * StateName : Chhattisgarh
         * IsDelete : false
         */

        @SerializedName("StateId")
        private String StateId;
        @SerializedName("StateName")
        private String StateName;
        @SerializedName("IsDelete")
        private boolean IsDelete;

        public String getStateId()
        {
            return StateId;
        }

        public void setStateId(String StateId)
        {
            this.StateId = StateId;
        }

        public String getStateName()
        {
            return StateName;
        }

        public void setStateName(String StateName)
        {
            this.StateName = StateName;
        }

        public boolean isIsDelete()
        {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete)
        {
            this.IsDelete = IsDelete;
        }
    }
}
