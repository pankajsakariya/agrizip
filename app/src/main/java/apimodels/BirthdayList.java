package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by S-PC on 15/03/2017.
 */

public class BirthdayList {



    @SerializedName("ResponseStatus")
    private Integer responseStatus;
    @SerializedName("ErrorMessage")
    private Object errorMessage;
    @SerializedName("Message")
    private String message;
    @SerializedName("UpdateDate")
    private Object updateDate;
    @SerializedName("Data")

    private List<DataBean> data;

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }












        public class DataBean {


            @SerializedName("FriendUserId")
            private String friendUserId;
            @SerializedName("FullName")
            private String fullName;
            @SerializedName("UserImage")
            private String userImage;
            @SerializedName("Dob")
            private String dob;


            @SerializedName("PostID")
            private String PostID;
            @SerializedName("IsPostImage")
            private boolean IsPostImage;

            public String getPostID() {return PostID; }


            public void setPostID(String postID) {PostID = postID; }


            public boolean isPostImage() {return IsPostImage; }


            public void setPostImage(boolean postImage) {IsPostImage = postImage; }




            public String getFriendUserId() {
                return friendUserId;
            }

            public void setFriendUserId(String friendUserId) {
                this.friendUserId = friendUserId;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getUserImage() {
                return userImage;
            }

            public void setUserImage(String userImage) {
                this.userImage = userImage;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }
        }
}
