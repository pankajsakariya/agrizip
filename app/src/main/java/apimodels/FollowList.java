package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 1/18/2017 at 7:24 PM.
 */

public class FollowList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"FollowupId":"1d4b2e27-aa69-484d-b76e-2e4f9999b5b1","FollowUserId":"3cc42b15-5607-4f69-8f2c-630aa5b1cff1","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","CreateDate":"2017-01-12T07:53:28.957"},{"FollowupId":"2c422d44-0e3e-4a7d-8818-e6923520d375","FollowUserId":"c7e5d577-3328-4a86-97e5-0192b9d352a0","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","CreateDate":"2017-01-12T07:47:38.717"},{"FollowupId":"ae70cd16-135b-4e8c-9184-b00d46c26a6a","FollowUserId":"9c8bbb89-7063-450c-9b47-e616e71ffc86","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","CreateDate":"2017-01-12T07:29:53.07"},{"FollowupId":"0539944a-3697-4818-a75a-1313eb6de4cb","FollowUserId":"164e9273-891d-4c0e-afe7-af385d37cfde","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg","CreateDate":"2017-01-06T14:21:20.05"}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * FollowupId : 1d4b2e27-aa69-484d-b76e-2e4f9999b5b1
         * FollowUserId : 3cc42b15-5607-4f69-8f2c-630aa5b1cff1
         * FullName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636193251812992234.jpeg
         * CreateDate : 2017-01-12T07:53:28.957
         */

        @SerializedName("FollowupId")
        private String FollowupId;
        @SerializedName("FollowUserId")
        private String FollowUserId;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("CreateDate")
        private String CreateDate;

        public String getFollowupId()
        {
            return FollowupId;
        }

        public void setFollowupId(String FollowupId)
        {
            this.FollowupId = FollowupId;
        }

        public String getFollowUserId()
        {
            return FollowUserId;
        }

        public void setFollowUserId(String FollowUserId)
        {
            this.FollowUserId = FollowUserId;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }
    }
}
