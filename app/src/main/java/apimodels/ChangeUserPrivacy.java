package apimodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rutvik on 1/23/2017 at 6:11 PM.
 */

public class ChangeUserPrivacy
{


    /**
     * UserId : fad83ed4-d5a2-4329-90bb-cb27c51ff884
     * Whoseefuturepost : 556494e9-362e-4c44-ac96-cebf4940a51c
     * Whoseemobile : 70ddc59f-67cf-469d-a041-79fd92ee3d62
     * Whoseeemail : 0f58f719-d63b-442c-a9d3-4248cebbc1ba
     * WhoseeDOB : 3e60256d-1cce-4dee-b46b-bdb878d476a7
     */

    @SerializedName("UserId")
    private String UserId;
    @SerializedName("Whoseefuturepost")
    private String Whoseefuturepost;
    @SerializedName("Whoseemobile")
    private String Whoseemobile;
    @SerializedName("Whoseeemail")
    private String Whoseeemail;
    @SerializedName("WhoseeDOB")
    private String WhoseeDOB;

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String UserId)
    {
        this.UserId = UserId;
    }

    public String getWhoseefuturepost()
    {
        return Whoseefuturepost;
    }

    public void setWhoseefuturepost(String Whoseefuturepost)
    {
        this.Whoseefuturepost = Whoseefuturepost;
    }

    public String getWhoseemobile()
    {
        return Whoseemobile;
    }

    public void setWhoseemobile(String Whoseemobile)
    {
        this.Whoseemobile = Whoseemobile;
    }

    public String getWhoseeemail()
    {
        return Whoseeemail;
    }

    public void setWhoseeemail(String Whoseeemail)
    {
        this.Whoseeemail = Whoseeemail;
    }

    public String getWhoseeDOB()
    {
        return WhoseeDOB;
    }

    public void setWhoseeDOB(String WhoseeDOB)
    {
        this.WhoseeDOB = WhoseeDOB;
    }
}
