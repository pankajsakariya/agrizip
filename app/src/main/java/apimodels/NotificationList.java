package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rutvik on 2/22/2017 at 3:40 PM.
 */

public class NotificationList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"NotificationId":7701,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta recently liked post.","DataId":"607c9b4c-422e-43eb-ad37-ec29d65f56aa","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T14:07:44.76","BlogId":"","Notification":0,"Topic":null},{"NotificationId":7700,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta recently commented on post.","DataId":"607c9b4c-422e-43eb-ad37-ec29d65f56aa","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T14:07:42","BlogId":"","Notification":0,"Topic":null},{"NotificationId":7695,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Agrizip added new blog","DataId":"5685b519-56d5-422e-93f2-8ddff0db2fb1","IsRead":false,"NotificationType":6,"CreateDate":"2017-02-21T11:16:19.6","BlogId":"1489","Notification":0,"Topic":null},{"NotificationId":7681,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Agrizip added new blog","DataId":"464e08ae-2706-4bfe-a129-cf2ae290f896","IsRead":false,"NotificationType":6,"CreateDate":"2017-02-21T10:11:31.93","BlogId":"1486","Notification":0,"Topic":null},{"NotificationId":7671,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta recently liked post.","DataId":"60587cfc-ee08-4536-8a9e-b29af4569bd7","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T09:52:36.613","BlogId":"","Notification":0,"Topic":null},{"NotificationId":7670,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta recently commented on post.","DataId":"ab4bc31b-7506-45da-899d-0526ba2e99be","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T08:59:20.59","BlogId":"","Notification":0,"Topic":null},{"NotificationId":7669,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta,Rutvik Mehta and 1 other people recently liked post.","DataId":"ab4bc31b-7506-45da-899d-0526ba2e99be","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T08:59:04.447","BlogId":"","Notification":0,"Topic":null},{"NotificationId":7668,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta,Rutvik Mehta and 1 other people recently liked post.","DataId":"ab4bc31b-7506-45da-899d-0526ba2e99be","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T08:59:04.257","BlogId":"","Notification":0,"Topic":null},{"NotificationId":7667,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta,Rutvik Mehta and 1 other people recently liked post.","DataId":"ab4bc31b-7506-45da-899d-0526ba2e99be","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T08:59:04.21","BlogId":"","Notification":0,"Topic":null},{"NotificationId":7666,"InstanceId":"ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve","UserId":"cd4e1de6-fecd-4a27-acad-b1b244adca93","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg","Message":"Rutvik Mehta,Rutvik Mehta recently liked post.","DataId":"ab4bc31b-7506-45da-899d-0526ba2e99be","IsRead":false,"NotificationType":1,"CreateDate":"2017-02-21T08:58:50.797","BlogId":"","Notification":0,"Topic":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public static class DataBean
    {
        /**
         * NotificationId : 7701
         * InstanceId : ftfuvi3dkV8:APA91bF_diwkZoaLEepcUklRzL68VuOTdRc0MQGqSHJSEgU7P13Kcy6OKNn_ZVVLUV-5CaOKh7dSKwISainpDnjf-TBNF4qhqzOY7lq9jWY2tZvpsdIl873CNBNN7T7L9aCSLTGTtyve
         * UserId : cd4e1de6-fecd-4a27-acad-b1b244adca93
         * FullName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636233025860570938.jpeg
         * Message : Rutvik Mehta recently liked post.
         * DataId : 607c9b4c-422e-43eb-ad37-ec29d65f56aa
         * IsRead : false
         * NotificationType : 1
         * CreateDate : 2017-02-21T14:07:44.76
         * BlogId :
         * Notification : 0
         * Topic : null
         */

        @SerializedName("NotificationId")
        private int NotificationId;
        @SerializedName("InstanceId")
        private String InstanceId;
        @SerializedName("UserId")
        private String UserId;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("Message")
        private String Message;
        @SerializedName("DataId")
        private String DataId;
        @SerializedName("IsRead")
        private boolean IsRead;
         @SerializedName("IsPostImage")
        private boolean IsPostImage;
        @SerializedName("NotificationType")
        private int NotificationType;
        @SerializedName("CreateDate")
        private String CreateDate;
        @SerializedName("BlogId")
        private String BlogId;
        @SerializedName("Notification")
        private int Notification;
        @SerializedName("Topic")
        private Object Topic;

        public int getNotificationId()
        {
            return NotificationId;
        }

        public void setNotificationId(int NotificationId)
        {
            this.NotificationId = NotificationId;
        }

        public String getInstanceId()
        {
            return InstanceId;
        }

        public void setInstanceId(String InstanceId)
        {
            this.InstanceId = InstanceId;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getMessage()
        {
            return Message;
        }

        public void setMessage(String Message)
        {
            this.Message = Message;
        }

        public String getDataId()
        {
            return DataId;
        }

        public void setDataId(String DataId)
        {
            this.DataId = DataId;
        }

        public boolean isIsRead()
        {
            return IsRead;
        }

        public void setIsRead(boolean IsRead)
        {
            this.IsRead = IsRead;
        }

        public int getNotificationType()
        {
            return NotificationType;
        }

        public void setNotificationType(int NotificationType)
        {
            this.NotificationType = NotificationType;
        }
        public boolean isPostImage() {
            return IsPostImage;
        }

        public void setPostImage(boolean postImage) {
            IsPostImage = postImage;
        }


        public String getCreateDate()
        {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate)
        {
            this.CreateDate = CreateDate;
        }

        public String getBlogId()
        {
            return BlogId;
        }

        public void setBlogId(String BlogId)
        {
            this.BlogId = BlogId;
        }

        public int getNotification()
        {
            return Notification;
        }

        public void setNotification(int Notification)
        {
            this.Notification = Notification;
        }

        public Object getTopic()
        {
            return Topic;
        }

        public void setTopic(Object Topic)
        {
            this.Topic = Topic;
        }
    }
}
