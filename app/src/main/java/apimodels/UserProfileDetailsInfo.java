package apimodels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rutvik on 12/10/2016 at 12:34 PM.
 */

public class UserProfileDetailsInfo
{

    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : {"UserId":"abb92a99-37ef-430b-8796-01880b462083","FullName":"Rutvik Mehta","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg","Status":"Developing Agrizip :)","CountryName":"India","StateName":"Gujarat","DistrictName":"Ahmedabad","EmailAddress":"","MobileNumber":"9409210488","Dob":"2014-10-10T00:00:00","Gender":"","TotalFriendsCounts":"3","Rating":4,"CityId":"00000000-0000-0000-0000-000000000000","City":"","IsCityShow":false,"HometownId":"00000000-0000-0000-0000-000000000000","Hometown":"","IsHometownShow":false,"RelationshipId":"00000000-0000-0000-0000-000000000000","RelationshipStatus":"","IsRelationshipShow":false,"FollowUpCount":2,"IsShowFollower":true,"Work":[{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Position":".net developer","Company":"Metizsoft","IsWorkShow":true},{"WorkId":"6fb609c0-60d5-4c17-85b4-87bf233cec40","Position":".net developer","Company":"Metizsoft","IsWorkShow":true}],"Education":[{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","IsSchool":true,"IsCollege":true,"IsEducationShow":true},{"EducationId":"29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311","InstituteName":"GTU","IsSchool":true,"IsCollege":true,"IsEducationShow":true}],"Followup":[{"FollowupId":"5bdc7cd9-0310-4da0-bbd2-0b3e28a51469","FollowUser":"6b81db86-3d14-4bd9-9d16-def858f58094"},{"FollowupId":"5817b4a3-955a-49dd-bc88-3d430dfdb51d","FollowUser":"abb92a99-37ef-430b-8796-01880b462083"}],"Friends":[{"FriendUserId":"6b81db86-3d14-4bd9-9d16-def858f58094","FullName":"Pankaj","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636167199900120944.jpeg"},{"FriendUserId":"d9b959f6-1f37-427f-b693-70b4a187d3f3","FullName":"Rakshit Sathvara","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636166347965013628.jpeg"},{"FriendUserId":"7892c771-68af-4954-8a1b-02121f89fb75","FullName":"Neel Mistry","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636168157191277793.jpeg"}]}
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private DataBean Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public DataBean getData()
    {
        return Data;
    }

    public void setData(DataBean Data)
    {
        this.Data = Data;
    }
    public interface  OnUpdateFolowerCount{

        void onChangeFollowerCount(int count,int position);
    }

    public OnUpdateFolowerCount getOnUpdateFolowerCount() {
        return onUpdateFolowerCount;
    }

    public void setOnUpdateFolowerCount(OnUpdateFolowerCount onUpdateFolowerCount) {
        this.onUpdateFolowerCount = onUpdateFolowerCount;
    }

    private OnUpdateFolowerCount onUpdateFolowerCount;

    public static class DataBean implements UserProfileInformation
    {



        @SerializedName("Work")
        private List<WorkBean> Work;
        @SerializedName("Education")
        private List<EducationBean> Education;
        @SerializedName("Followup")
        private List<FollowupBean> Followup;
        @SerializedName("Friends")
        private List<FriendsBean> Friends;
        @SerializedName("Photo")
        private List<PhotoBean> Photo;
        /**
         * UserId : abb92a99-37ef-430b-8796-01880b462083
         * FullName : Rutvik Mehta
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636154152743906912.jpeg
         * Status : bdbdhdhdbdhd
         * CountryName : India
         * StateName : Gujarat
         * DistrictName : Ahmedabad
         * EmailAddress : rutvik1061992@gmail.com
         * EmailVisibleTo : null
         * MobileNumber : 9409210488
         * MobileVisibleTo : null
         * Dob : 1982-10-06T00:00:00
         * DOBVisibleTo : null
         * Gender : Female
         * GenderVisibleTo : null
         * TotalFriendsCounts : 3
         * Rating : 4.25
         * CityId : 98e1726b-fd9e-4759-bb23-5c9a32f3a827
         * City : Ahmedabad
         * CityVisibleTo : 00000000-0000-0000-0000-000000000000
         * IsCityShow : true
         * HometownId : 874a2b50-6a01-4158-ba48-56f6f73cd390
         * Hometown :
         * HometownVisibleTo : 00000000-0000-0000-0000-000000000000
         * IsHometownShow : true
         * RelationshipId : 00000000-0000-0000-0000-000000000000
         * RelationshipStatus :
         * RelationshipVisibleTo : 00000000-0000-0000-0000-000000000000
         * IsRelationshipShow : false
         * FollowCount : 7
         * FollowingCount : 6
         */

        @SerializedName("UserId")
        private String UserId;
        @SerializedName("FullName")
        private String FullName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("Status")
        private String Status;
        @SerializedName("CountryName")
        private String CountryName;
        @SerializedName("StateName")
        private String StateName;
        @SerializedName("DistrictName")
        private String DistrictName;
        @SerializedName("EmailAddress")
        private String EmailAddress;
        @SerializedName("EmailVisibleTo")
        private Object EmailVisibleTo;
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @SerializedName("MobileVisibleTo")
        private Object MobileVisibleTo;
        @SerializedName("Dob")
        private String Dob;
        @SerializedName("DOBVisibleTo")
        private Object DOBVisibleTo;
        @SerializedName("Gender")
        private String Gender;
        @SerializedName("GenderVisibleTo")
        private Object GenderVisibleTo;
        @SerializedName("TotalFriendsCounts")
        private String TotalFriendsCounts;
        @SerializedName("Rating")
        private double Rating;
        @SerializedName("CityId")
        private String CityId;
        @SerializedName("City")
        private String City;
        @SerializedName("CityVisibleTo")
        private String CityVisibleTo;
        @SerializedName("IsCityShow")
        private boolean IsCityShow;
        @SerializedName("HometownId")
        private String HometownId;
        @SerializedName("Hometown")
        private String Hometown;
        @SerializedName("HometownVisibleTo")
        private String HometownVisibleTo;
        @SerializedName("IsHometownShow")
        private boolean IsHometownShow;
        @SerializedName("RelationshipId")
        private String RelationshipId;
        @SerializedName("RelationshipStatus")
        private String RelationshipStatus;
        @SerializedName("RelationshipVisibleTo")
        private String RelationshipVisibleTo;
        @SerializedName("IsRelationshipShow")
        private boolean IsRelationshipShow;
        @SerializedName("FollowCount")
        private String FollowCount;
        @SerializedName("FollowingCount")
        private String FollowingCount;

        public int getIsFriend() {
            return IsFriend;
        }

        public void setIsFriend(int isFriend) {
            IsFriend = isFriend;
        }

        @SerializedName("IsFriend")
        private int IsFriend;

        public int getTotalPhotoCount() {
            return TotalPhotoCount;
        }

        public void setTotalPhotoCount(int totalPhotoCount) {
            TotalPhotoCount = totalPhotoCount;
        }

        @SerializedName("TotalPhotoCount")
        private int TotalPhotoCount;
        public List<WorkBean> getWork()
        {
            if (Work == null)
            {
                Work = new ArrayList<>();
            }
            return Work;
        }

        public void setWork(List<WorkBean> Work)
        {
            this.Work = Work;
        }

        public List<EducationBean> getEducation()
        {
            return Education;
        }

        public void setEducation(List<EducationBean> Education)
        {
            this.Education = Education;
        }

        public List<FollowupBean> getFollowup()
        {
            return Followup;
        }

        public void setFollowup(List<FollowupBean> Followup)
        {
            this.Followup = Followup;
        }

        public List<FriendsBean> getFriends()
        {
            return Friends;
        }

        public void setFriends(List<FriendsBean> Friends)
        {
            this.Friends = Friends;
        }

        public String getUserId()
        {
            return UserId;
        }

        public void setUserId(String UserId)
        {
            this.UserId = UserId;
        }

        public String getFullName()
        {
            return FullName;
        }

        public void setFullName(String FullName)
        {
            this.FullName = FullName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public String getStatus()
        {
            return Status;
        }

        public void setStatus(String Status)
        {
            this.Status = Status;
        }

        public String getCountryName()
        {
            return CountryName;
        }

        public void setCountryName(String CountryName)
        {
            this.CountryName = CountryName;
        }

        public String getStateName()
        {
            return StateName;
        }

        public void setStateName(String StateName)
        {
            this.StateName = StateName;
        }

        public String getDistrictName()
        {
            return DistrictName;
        }

        public void setDistrictName(String DistrictName)
        {
            this.DistrictName = DistrictName;
        }

        public String getEmailAddress()
        {
            return EmailAddress;
        }

        public void setEmailAddress(String EmailAddress)
        {
            this.EmailAddress = EmailAddress;
        }

        public Object getEmailVisibleTo()
        {
            return EmailVisibleTo;
        }

        public void setEmailVisibleTo(Object EmailVisibleTo)
        {
            this.EmailVisibleTo = EmailVisibleTo;
        }

        public String getMobileNumber()
        {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber)
        {
            this.MobileNumber = MobileNumber;
        }

        public Object getMobileVisibleTo()
        {
            return MobileVisibleTo;
        }

        public void setMobileVisibleTo(Object MobileVisibleTo)
        {
            this.MobileVisibleTo = MobileVisibleTo;
        }

        public String getDob()
        {
            return Dob;
        }

        public void setDob(String Dob)
        {
            this.Dob = Dob;
        }

        public Object getDOBVisibleTo()
        {
            return DOBVisibleTo;
        }

        public void setDOBVisibleTo(Object DOBVisibleTo)
        {
            this.DOBVisibleTo = DOBVisibleTo;
        }

        public String getGender()
        {
            return Gender;
        }

        public void setGender(String Gender)
        {
            this.Gender = Gender;
        }

        public Object getGenderVisibleTo()
        {
            return GenderVisibleTo;
        }

        public void setGenderVisibleTo(Object GenderVisibleTo)
        {
            this.GenderVisibleTo = GenderVisibleTo;
        }

        public String getTotalFriendsCounts()
        {
            return TotalFriendsCounts;
        }

        public void setTotalFriendsCounts(String TotalFriendsCounts)
        {
            this.TotalFriendsCounts = TotalFriendsCounts;
        }

        public double getRating()
        {
            return Rating;
        }

        public void setRating(double Rating)
        {
            this.Rating = Rating;
        }

        public String getCityId()
        {
            return CityId;
        }

        public void setCityId(String CityId)
        {
            this.CityId = CityId;
        }

        public String getCity()
        {
            return City;
        }

        public void setCity(String City)
        {
            this.City = City;
        }

        public String getCityVisibleTo()
        {
            return CityVisibleTo;
        }

        public void setCityVisibleTo(String CityVisibleTo)
        {
            this.CityVisibleTo = CityVisibleTo;
        }

        public boolean isIsCityShow()
        {
            return IsCityShow;
        }

        public void setIsCityShow(boolean IsCityShow)
        {
            this.IsCityShow = IsCityShow;
        }

        public String getHometownId()
        {
            return HometownId;
        }

        public void setHometownId(String HometownId)
        {
            this.HometownId = HometownId;
        }

        public String getHometown()
        {
            return Hometown;
        }

        public void setHometown(String Hometown)
        {
            this.Hometown = Hometown;
        }

        public String getHometownVisibleTo()
        {
            return HometownVisibleTo;
        }

        public void setHometownVisibleTo(String HometownVisibleTo)
        {
            this.HometownVisibleTo = HometownVisibleTo;
        }

        public boolean isIsHometownShow()
        {
            return IsHometownShow;
        }

        public void setIsHometownShow(boolean IsHometownShow)
        {
            this.IsHometownShow = IsHometownShow;
        }

        public String getRelationshipId()
        {
            return RelationshipId;
        }

        public void setRelationshipId(String RelationshipId)
        {
            this.RelationshipId = RelationshipId;
        }

        public String getRelationshipStatus()
        {
            return RelationshipStatus;
        }

        public void setRelationshipStatus(String RelationshipStatus)
        {
            this.RelationshipStatus = RelationshipStatus;
        }

        public String getRelationshipVisibleTo()
        {
            return RelationshipVisibleTo;
        }

        public void setRelationshipVisibleTo(String RelationshipVisibleTo)
        {
            this.RelationshipVisibleTo = RelationshipVisibleTo;
        }

        public boolean isIsRelationshipShow()
        {
            return IsRelationshipShow;
        }

        public void setIsRelationshipShow(boolean IsRelationshipShow)
        {
            this.IsRelationshipShow = IsRelationshipShow;
        }

        public String getFollowCount()
        {
            return FollowCount;
        }

        public void setFollowCount(String FollowCount)
        {
            this.FollowCount = FollowCount;
        }

        public String getFollowingCount()
        {
            return FollowingCount;
        }

        public void setFollowingCount(String FollowingCount)
        {
            this.FollowingCount = FollowingCount;
        }

        public List<PhotoBean> getPhoto()
        {
           if (Photo == null)
            {
                return new ArrayList<PhotoBean>();
            }
            return Photo;
        }

        public void setPhoto(List<PhotoBean> Photo)
        {
            this.Photo = Photo;
        }

        public static class WorkBean
        {
            /**
             * WorkId : 6fb609c0-60d5-4c17-85b4-87bf233cec40
             * Position : .net developer
             * Company : Metizsoft
             * IsWorkShow : true
             */

            @SerializedName("WorkId")
            private String WorkId;
            @SerializedName("Position")
            private String Position;
            @SerializedName("Company")
            private String Company;
            @SerializedName("IsWorkShow")
            private boolean IsWorkShow;

            public String getWorkId()
            {
                return WorkId;
            }

            public void setWorkId(String WorkId)
            {
                this.WorkId = WorkId;
            }

            public String getPosition()
            {
                return Position;
            }

            public void setPosition(String Position)
            {
                this.Position = Position;
            }

            public String getCompany()
            {
                return Company;
            }

            public void setCompany(String Company)
            {
                this.Company = Company;
            }

            public boolean isIsWorkShow()
            {
                return IsWorkShow;
            }

            public void setIsWorkShow(boolean IsWorkShow)
            {
                this.IsWorkShow = IsWorkShow;
            }
        }

        public static class EducationBean
        {
            /**
             * EducationId : 29d69f7f-f86c-46e6-b5fb-e5fbfb3f4311
             * InstituteName : GTU
             * IsSchool : true
             * IsCollege : true
             * IsEducationShow : true
             */

            @SerializedName("EducationId")
            private String EducationId;
            @SerializedName("InstituteName")
            private String InstituteName;
            @SerializedName("IsSchool")
            private boolean IsSchool;
            @SerializedName("IsCollege")
            private boolean IsCollege;
            @SerializedName("IsEducationShow")
            private boolean IsEducationShow;

            public String getEducationId()
            {
                return EducationId;
            }

            public void setEducationId(String EducationId)
            {
                this.EducationId = EducationId;
            }

            public String getInstituteName()
            {
                return InstituteName;
            }

            public void setInstituteName(String InstituteName)
            {
                this.InstituteName = InstituteName;
            }

            public boolean isIsSchool()
            {
                return IsSchool;
            }

            public void setIsSchool(boolean IsSchool)
            {
                this.IsSchool = IsSchool;
            }

            public boolean isIsCollege()
            {
                return IsCollege;
            }

            public void setIsCollege(boolean IsCollege)
            {
                this.IsCollege = IsCollege;
            }

            public boolean isIsEducationShow()
            {
                return IsEducationShow;
            }

            public void setIsEducationShow(boolean IsEducationShow)
            {
                this.IsEducationShow = IsEducationShow;
            }
        }

        public static class FollowupBean
        {
            /**
             * FollowupId : 5bdc7cd9-0310-4da0-bbd2-0b3e28a51469
             * FollowUser : 6b81db86-3d14-4bd9-9d16-def858f58094
             */

            @SerializedName("FollowupId")
            private String FollowupId;
            @SerializedName("FollowUser")
            private String FollowUser;

            public String getFollowupId()
            {
                return FollowupId;
            }

            public void setFollowupId(String FollowupId)
            {
                this.FollowupId = FollowupId;
            }

            public String getFollowUser()
            {
                return FollowUser;
            }

            public void setFollowUser(String FollowUser)
            {
                this.FollowUser = FollowUser;
            }
        }

        public static class FriendsBean
        {
            /**
             * FriendUserId : 6b81db86-3d14-4bd9-9d16-def858f58094
             * FullName : Pankaj
             * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636167199900120944.jpeg
             */

            @SerializedName("FriendUserId")
            private String FriendUserId;
            @SerializedName("FullName")
            private String FullName;
            @SerializedName("UserImage")
            private String UserImage;

            private OnClickSmallFriendIconListener onClickSmallFriendIconListener;

            public String getFriendUserId()
            {
                return FriendUserId;
            }

            public void setFriendUserId(String FriendUserId)
            {
                this.FriendUserId = FriendUserId;
            }

            public String getFullName()
            {
                return FullName;
            }

            public void setFullName(String FullName)
            {
                this.FullName = FullName;
            }

            public String getUserImage()
            {
                return UserImage;
            }

            public void setUserImage(String UserImage)
            {
                this.UserImage = UserImage;
            }

            public OnClickSmallFriendIconListener getOnClickSmallFriendIconListener()
            {
                return onClickSmallFriendIconListener;
            }

            public void setOnClickSmallFriendIconListener(OnClickSmallFriendIconListener onClickSmallFriendIconListener)
            {
                this.onClickSmallFriendIconListener = onClickSmallFriendIconListener;
            }

            public interface OnClickSmallFriendIconListener
            {
                void onClickSmallFriend();
            }



        }

        public static class PhotoBean
        {
            /**
             * ImageId : d341bf7b-e498-4109-ae79-19952a166a80
             * ImagePath : http://social.vinayakpharmaceuticals.com/UserImage/636179482805271487.jpeg
             */

            @SerializedName("ImageId")
            private String ImageId;
            @SerializedName("ImagePath")
            private String ImagePath;

            public String getImageId()
            {
                return ImageId;
            }

            public void setImageId(String ImageId)
            {
                this.ImageId = ImageId;
            }

            public String getImagePath()
            {
                return ImagePath;
            }

            public void setImagePath(String ImagePath)
            {
                this.ImagePath = ImagePath;
            }

        }
    }
}
