package apimodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rutvik on 1/17/2017 at 6:40 PM.
 */

public class AdvisePostList
{


    /**
     * ResponseStatus : 1
     * ErrorMessage : null
     * Message : successful
     * UpdateDate : null
     * Data : [{"HelpId":"47671186-45b0-4c71-a99f-b64e6604ed65","UserID":"c7e5d577-3328-4a86-97e5-0192b9d352a0","UserName":"sachin","UserImage":"http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg","IsCheckIn":true,"CheckInID":"e128ad85-33b5-4005-a31c-977962b3fc71","HelpTitle":"test","Description":"Nothing...","FeelingName":"","FeelingValue":"","PostYear":"2017","PostMonth":"01","PostDay":"16","PostHour":"18","PostMinute":"03","Help":true,"HelpType":1,"IsLiked":false,"LikeCounts":0,"CommentCounts":0,"CreateDatetime":"2017-01-16T12:33:06.377","Message":"test","Place":"Ahmedabad, India","Images":[{"ImageId":"2E8D8898-3A12-42EC-B729-CF1C449211AF","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636201865936164717.jpeg"}],"TagDetails":null}]
     */

    @SerializedName("ResponseStatus")
    private int ResponseStatus;
    @SerializedName("ErrorMessage")
    private Object ErrorMessage;
    @SerializedName("Message")
    private String Message;
    @SerializedName("UpdateDate")
    private Object UpdateDate;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(int ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public Object getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage)
    {
        this.ErrorMessage = ErrorMessage;
    }

    public String getMessage()
    {
        return Message;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }

    public Object getUpdateDate()
    {
        return UpdateDate;
    }

    public void setUpdateDate(Object UpdateDate)
    {
        this.UpdateDate = UpdateDate;
    }

    public List<DataBean> getData()
    {
        return Data;
    }

    public void setData(List<DataBean> Data)
    {
        this.Data = Data;
    }

    public interface HideHelpListener{

        void onhideHelp(AdvisePostList.DataBean singleHelp,int position,int type);

    }
    public enum AdviseBuySaleType
    {
        ADVISE,
        BUY,
        SALE,
    }

    public static class DataBean implements Parcelable
    {


        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>()
        {
            @Override
            public DataBean createFromParcel(Parcel source)
            {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size)
            {
                return new DataBean[size];
            }
        };
        @SerializedName("AdviseBuySaleType")
        AdviseBuySaleType adviseBuySaleType;



        /**
         * HelpId : 47671186-45b0-4c71-a99f-b64e6604ed65
         * UserID : c7e5d577-3328-4a86-97e5-0192b9d352a0
         * UserName : sachin
         * UserImage : http://social.vinayakpharmaceuticals.com/UserImage/636195793328438442.jpeg
         * IsCheckIn : true
         * CheckInID : e128ad85-33b5-4005-a31c-977962b3fc71
         * HelpTitle : test
         * Description : Nothing...
         * FeelingName :
         * FeelingValue :
         * PostYear : 2017
         * PostMonth : 01
         * PostDay : 16
         * PostHour : 18
         * PostMinute : 03
         * Help : true
         * HelpType : 1
         * IsLiked : false
         * LikeCounts : 0
         * CommentCounts : 0
         * CreateDatetime : 2017-01-16T12:33:06.377
         * Message : test
         * Place : Ahmedabad, India
         * Images : [{"ImageId":"2E8D8898-3A12-42EC-B729-CF1C449211AF","Imagestr":"http://social.vinayakpharmaceuticals.com/UserImage/636201865936164717.jpeg"}]
         * TagDetails : null
         */

     private HideHelpListener hideHelpListener;
        @SerializedName("HelpId")
        private String HelpId;
        @SerializedName("UserID")
        private String UserID;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("IsCheckIn")
        private boolean IsCheckIn;
        @SerializedName("CheckInID")
        private String CheckInID;
        @SerializedName("HelpTitle")
        private String HelpTitle;
        @SerializedName("Description")
        private String Description;
        @SerializedName("FeelingName")
        private String FeelingName;
        @SerializedName("FeelingValue")
        private String FeelingValue;
        @SerializedName("PostYear")
        private String PostYear;
        @SerializedName("PostMonth")
        private String PostMonth;
        @SerializedName("PostDay")
        private String PostDay;
        @SerializedName("PostHour")
        private String PostHour;
        @SerializedName("PostMinute")
        private String PostMinute;
        @SerializedName("Help")
        private boolean Help;
        @SerializedName("HelpType")
        private int HelpType;
        @SerializedName("IsLiked")
        private boolean IsLiked;
        @SerializedName("LikeCounts")
        private int LikeCounts;
        @SerializedName("CommentCounts")
        private int CommentCounts;
        @SerializedName("CreateDatetime")
        private String CreateDatetime;
        @SerializedName("Message")
        private String Message;
        @SerializedName("Place")
        private String Place;
        @SerializedName("PostViews")
        private int PostViews;

        public int getPostViews() {
            return PostViews;
        }

        public void setPostViews(int postViews) {
            PostViews = postViews;
        }


        @SerializedName("TagDetails")
        private Object TagDetails;
        @SerializedName("Images")
        private List<ImagesBean> Images;

        public DataBean()
        {
        }

        public HideHelpListener getHideHelpListener()
        {
            return hideHelpListener;
        }
        public void setHideHelpListener(HideHelpListener hideHelpListener){
            this.hideHelpListener = hideHelpListener;
        }
        protected DataBean(Parcel in)
        {
            int tmpAdviseBuySaleType = in.readInt();
            this.adviseBuySaleType = tmpAdviseBuySaleType == -1 ? null : AdviseBuySaleType.values()[tmpAdviseBuySaleType];
            this.HelpId = in.readString();
            this.UserID = in.readString();
            this.UserName = in.readString();
            this.UserImage = in.readString();
            this.IsCheckIn = in.readByte() != 0;
            this.CheckInID = in.readString();
            this.HelpTitle = in.readString();
            this.Description = in.readString();
            this.FeelingName = in.readString();
            this.FeelingValue = in.readString();
            this.PostYear = in.readString();
            this.PostMonth = in.readString();
            this.PostDay = in.readString();
            this.PostHour = in.readString();
            this.PostMinute = in.readString();
            this.Help = in.readByte() != 0;
            this.HelpType = in.readInt();
            this.IsLiked = in.readByte() != 0;
            this.LikeCounts = in.readInt();
            this.CommentCounts = in.readInt();
            this.CreateDatetime = in.readString();
            this.Message = in.readString();
            this.Place = in.readString();

            this.Images = new ArrayList<ImagesBean>();
            in.readList(this.Images, ImagesBean.class.getClassLoader());

        }

        public AdviseBuySaleType getAdviseBuySaleType()
        {
            return adviseBuySaleType;
        }

        public void setAdviseBuySaleType(AdviseBuySaleType adviseBuySaleType)
        {
            this.adviseBuySaleType = adviseBuySaleType;
        }

        public String getHelpId()
        {
            return HelpId;
        }

        public void setHelpId(String HelpId)
        {
            this.HelpId = HelpId;
        }

        public String getUserID()
        {
            return UserID;
        }

        public void setUserID(String UserID)
        {
            this.UserID = UserID;
        }

        public String getUserName()
        {
            return UserName;
        }

        public void setUserName(String UserName)
        {
            this.UserName = UserName;
        }

        public String getUserImage()
        {
            return UserImage;
        }

        public void setUserImage(String UserImage)
        {
            this.UserImage = UserImage;
        }

        public boolean isIsCheckIn()
        {
            return IsCheckIn;
        }

        public void setIsCheckIn(boolean IsCheckIn)
        {
            this.IsCheckIn = IsCheckIn;
        }

        public String getCheckInID()
        {
            return CheckInID;
        }

        public void setCheckInID(String CheckInID)
        {
            this.CheckInID = CheckInID;
        }

        public String getHelpTitle()
        {
            return HelpTitle;
        }

        public void setHelpTitle(String HelpTitle)
        {
            this.HelpTitle = HelpTitle;
        }

        public String getDescription()
        {
            return Description;
        }

        public void setDescription(String Description)
        {
            this.Description = Description;
        }

        public String getFeelingName()
        {
            return FeelingName;
        }

        public void setFeelingName(String FeelingName)
        {
            this.FeelingName = FeelingName;
        }

        public String getFeelingValue()
        {
            return FeelingValue;
        }

        public void setFeelingValue(String FeelingValue)
        {
            this.FeelingValue = FeelingValue;
        }

        public String getPostYear()
        {
            return PostYear;
        }

        public void setPostYear(String PostYear)
        {
            this.PostYear = PostYear;
        }

        public String getPostMonth()
        {
            return PostMonth;
        }

        public void setPostMonth(String PostMonth)
        {
            this.PostMonth = PostMonth;
        }

        public String getPostDay()
        {
            return PostDay;
        }

        public void setPostDay(String PostDay)
        {
            this.PostDay = PostDay;
        }

        public String getPostHour()
        {
            return PostHour;
        }

        public void setPostHour(String PostHour)
        {
            this.PostHour = PostHour;
        }

        public String getPostMinute()
        {
            return PostMinute;
        }

        public void setPostMinute(String PostMinute)
        {
            this.PostMinute = PostMinute;
        }

        public boolean isHelp()
        {
            return Help;
        }

        public void setHelp(boolean Help)
        {
            this.Help = Help;
        }

        public int getHelpType()
        {
            return HelpType;
        }

        public void setHelpType(int HelpType)
        {
            this.HelpType = HelpType;
        }

        public boolean isIsLiked()
        {
            return IsLiked;
        }

        public void setIsLiked(boolean IsLiked)
        {
            this.IsLiked = IsLiked;
        }

        public int getLikeCounts()
        {
            return LikeCounts;
        }

        public void setLikeCounts(int LikeCounts)
        {
            this.LikeCounts = LikeCounts;
        }

        public int getCommentCounts()
        {
            return CommentCounts;
        }

        public void setCommentCounts(int CommentCounts)
        {
            this.CommentCounts = CommentCounts;
        }

        public String getCreateDatetime()
        {
            return CreateDatetime;
        }

        public void setCreateDatetime(String CreateDatetime)
        {
            this.CreateDatetime = CreateDatetime;
        }

        public String getMessage()
        {
            return Message;
        }

        public void setMessage(String Message)
        {
            this.Message = Message;
        }

        public String getPlace()
        {
            return Place;
        }

        public void setPlace(String Place)
        {
            this.Place = Place;
        }

        public Object getTagDetails()
        {
            return TagDetails;
        }

        public void setTagDetails(Object TagDetails)
        {
            this.TagDetails = TagDetails;
        }

        public List<ImagesBean> getImages()
        {
            return Images;
        }

        public void setImages(List<ImagesBean> Images)
        {
            this.Images = Images;
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeInt(this.adviseBuySaleType == null ? -1 : this.adviseBuySaleType.ordinal());
            dest.writeString(this.HelpId);
            dest.writeString(this.UserID);
            dest.writeString(this.UserName);
            dest.writeString(this.UserImage);
            dest.writeByte(this.IsCheckIn ? (byte) 1 : (byte) 0);
            dest.writeString(this.CheckInID);
            dest.writeString(this.HelpTitle);
            dest.writeString(this.Description);
            dest.writeString(this.FeelingName);
            dest.writeString(this.FeelingValue);
            dest.writeString(this.PostYear);
            dest.writeString(this.PostMonth);
            dest.writeString(this.PostDay);
            dest.writeString(this.PostHour);
            dest.writeString(this.PostMinute);
            dest.writeByte(this.Help ? (byte) 1 : (byte) 0);
            dest.writeInt(this.HelpType);
            dest.writeByte(this.IsLiked ? (byte) 1 : (byte) 0);
            dest.writeInt(this.LikeCounts);

            dest.writeInt(this.CommentCounts);
            dest.writeString(this.CreateDatetime);
            dest.writeString(this.Message);
            dest.writeString(this.Place);
            dest.writeList(this.Images);

        }

        public static class ImagesBean implements Parcelable
        {

            public static final Creator<ImagesBean> CREATOR = new Creator<ImagesBean>()
            {
                @Override
                public ImagesBean createFromParcel(Parcel source)
                {
                    return new ImagesBean(source);
                }

                @Override
                public ImagesBean[] newArray(int size)
                {
                    return new ImagesBean[size];
                }
            };
            /**
             * ImageId : 2E8D8898-3A12-42EC-B729-CF1C449211AF
             * Imagestr : http://social.vinayakpharmaceuticals.com/UserImage/636201865936164717.jpeg
             */

            @SerializedName("ImageId")
            private String ImageId;
            @SerializedName("Imagestr")
            private String Imagestr;

            public ImagesBean()
            {
            }

            protected ImagesBean(Parcel in)
            {
                this.ImageId = in.readString();
                this.Imagestr = in.readString();
            }

            public String getImageId()
            {
                return ImageId;
            }

            public void setImageId(String ImageId)
            {
                this.ImageId = ImageId;
            }

            public String getImagestr()
            {
                return Imagestr;
            }

            public void setImagestr(String Imagestr)
            {
                this.Imagestr = Imagestr;
            }

            @Override
            public int describeContents()
            {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags)
            {
                dest.writeString(this.ImageId);
                dest.writeString(this.Imagestr);
            }
        }
    }

}
