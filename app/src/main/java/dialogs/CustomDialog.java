package dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.home.adapter.MasterAdapter;

import java.util.List;

import apimodels.UserPrivacyDetails;
import apimodels.UserProfileDetailGetForEdit;
import butterknife.BindView;
import views.SingleDetailView;

/**
 * Created by rutvik on 12/26/2016 at 5:36 PM.
 */

public abstract class CustomDialog extends Dialog implements PrivacyChangeListener, View.OnClickListener,
        DialogInterface.OnClickListener
{
    public final UserProfileDetailGetForEdit userProfileDetailGetForEdit;
    public final MasterAdapter adapter;
    public final SingleDetailView singleDetailView;
    public final Context context;
    public final String id;
    @BindView(R.id.btn_dialogSave)
    public Button btnDialogSave;
    @BindView(R.id.btn_dialogCancel)
    public Button btnDialogCancel;
    @BindView(R.id.tv_dialogEditPrivacy)
    public TextView tvDialogEditPrivacy;
    public List<UserPrivacyDetails.DataBean> privacyList;
    public AlertDialog promptPrivacyDialog;
    public ArrayAdapter<UserPrivacyDetails.DataBean> privacyAdapter;

    public CustomDialog(Context context,
                        String id,
                        UserProfileDetailGetForEdit userProfileDetailGetForEdit,
                        SingleDetailView singleDetailView,
                        MasterAdapter adapter)
    {
        super(context);
        this.context = context;
        this.id = id;
        this.userProfileDetailGetForEdit = userProfileDetailGetForEdit;
        this.adapter = adapter;
        this.singleDetailView = singleDetailView;
    }

    @CallSuper
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        preparePrivacyAdapter();
    }

    @CallSuper
    public void preparePrivacyAdapter()
    {
        try {
            privacyList = App.getInstance().getUser().getUserPrivacyDetails().getData();

            if (privacyList != null) {
                if (privacyList.size() > 0) {

                    //tvDialogEditPrivacy.setText(privacyList.get(privacyList.size() - 1).getUserTypeName());

                    privacyAdapter
                            = new ArrayAdapter<UserPrivacyDetails.DataBean>(context,
                            android.R.layout.simple_list_item_1, privacyList) {

                        @NonNull
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            if (convertView == null) {
                                convertView = LayoutInflater.from(context).inflate(
                                        android.R.layout.simple_list_item_1, parent, false);
                            }
                            TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

                            tv.setText(privacyList.get(position).getUserTypeName());

                            return convertView;

                        }
                    };

                }
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }


    }


    @Override
    public void dismiss()
    {
        //adapter.notifyChange(id);
        super.dismiss();
    }

    @CallSuper
    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.tv_dialogEditPrivacy)
        {
            promptPrivacyDialog = new AlertDialog.Builder(context)
                    .setTitle("Select Post Privacy")
                    .setSingleChoiceItems(privacyAdapter, 0, this)
                    .show();
        }
    }

    @CallSuper
    @Override
    public void onClick(DialogInterface dialogInterface, int i)
    {
        tvDialogEditPrivacy.setText(privacyAdapter.getItem(i).getUserTypeName());
        onChangePrivacy(privacyAdapter.getItem(i).getId());
        if (promptPrivacyDialog.isShowing())
        {
            promptPrivacyDialog.dismiss();
        }
    }
}
