package dialogs;

/**
 * Created by rutvik on 12/28/2016 at 12:56 PM.
 */

public interface PrivacyChangeListener
{
    void onChangePrivacy(String visibilityId);
}
