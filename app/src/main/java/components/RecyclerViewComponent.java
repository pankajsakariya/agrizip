package components;

/**
 * Created by rutvik on 11/22/2016 at 10:59 AM.
 */

public class RecyclerViewComponent<T>
{

    public static final int BIG_PROFILE_PIC_WITH_DETAILS = 0;
    public static final int USER_PROFILE_EXTRA_STRIP = 1;
    public static final int WHATS_ON_YOUR_MIND = 2;
    public static final int SINGLE_POST = 3;
    public static final int SINGLE_PROFILE_DETAIL = 4;
    public static final int SAVE_USER_PROFILE_DETAILS_BUTTON = 5;
    public static final int LOADING_PROGRESS_BAR = 6;
    final T model;

    final int viewType;

    public RecyclerViewComponent(final int viewType, final T model)
    {
        this.viewType = viewType;
        this.model = model;
    }

    public T getModel()
    {
        return model;
    }

    public int getViewType()
    {
        return viewType;
    }
}
