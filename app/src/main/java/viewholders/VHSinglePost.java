package viewholders;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.format.DateUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.app.agrizip.app.App;
import com.app.agrizip.event.activity.ViewPostedEvent;
import com.app.agrizip.need_help.activity.ActivityJobOrEmployeeFullView;
import com.app.agrizip.post.dialog.DialogHideDeletePost;
import com.app.agrizip.post.dialog.DialogReportPost;
import com.app.agrizip.post.post_like_list.dialog.LikeListDialog;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.Inflater;

import api.API;
import api.RetrofitCallbacks;
import apimodels.Comment;
import apimodels.EventList;
import apimodels.LikeUnlikePost;
import apimodels.LikeUnlikeResponse;
import apimodels.PostImageList;
import apimodels.RequireEmployeeList;
import apimodels.RequireJobList;
import apimodels.SharePost;
import apimodels.SinglePost;
import apimodels.UnfollowUser;
import apimodels.UserPrivacyDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 11/24/2016 at 12:42 PM.
 */

public class VHSinglePost extends RecyclerView.ViewHolder implements View.OnClickListener,
        MenuBuilder.Callback {

    private static final String TAG = App.APP_TAG + VHSinglePost.class.getSimpleName();
    final Context context;
    @BindView(R.id.tv_commentname)
    TextView tv_commentname;
    @BindView(R.id.tv_commentname1)
    TextView tv_commentname1;

    @BindView(R.id.tv_feedCommentCount)
    TextView tvFeedCommentCount;

    @BindView(R.id.tv_feedCommentCount1)
    TextView tvFeedCommentCount1;
    @BindView(R.id.tv_feedDescription)
    TextView tvFeedDescription;


    @BindView(R.id.tv_sharedpost)
    TextView tv_sharedpost;
    @BindView(R.id.tv_feedTagDetails)
    TextView tvFeedTagDetails;
    @BindView(R.id.tv_feedLikeCount)
    TextView tvFeedLikeCount;
    @BindView(R.id.tv_postLikeStatusPost)
    TextView tvPostLikeStatusPost;
    @BindView(R.id.tv_feedLocation)
    TextView tvFeedLocation;
    @BindView(R.id.tv_feedShareCount)
    TextView tvFeedShareCount;
    @BindView(R.id.tv_feedTime)
    TextView tvFeedTime;
    @BindView(R.id.tv_feedViewCount)
    TextView tvFeedViewCount;
    @BindView(R.id.tv_feedViewCount1)
    TextView tvFeedViewCount1;
    @BindView(R.id.tv_feeUserName)
    TextView tvFeedUserName;
    @BindView(R.id.tv_feedTextStatus)
    TextView tvFeedTextStatus;
    @BindView(R.id.tv_company_name)
    TextView tv_company_name;
    @BindView(R.id.tv_employee_name)
    TextView tv_employee_name;
    @BindView(R.id.tv_job_title)
    TextView tv_job_title;
    @BindView(R.id.tv_vacancy)
    TextView tv_vacancy;
    @BindView(R.id.tv_postShareFeedPost)
    TextView tvPostShareFeedPost;

    @BindView(R.id.tv_singleEventName)
    TextView tv_singleEventName;

    @BindView(R.id.tv_singleEventHost)
    TextView tv_singleEventHost;

    @BindView(R.id.tv_singleEventAddress)
    TextView tv_singleEventAddress;

    @BindView(R.id.tv_singleEventContactPerson)
    TextView tv_singleEventContactPerson;

    @BindView(R.id.tv_singleEventDay)
    TextView tv_singleEventDay;

    @BindView(R.id.tv_singleEventMonth)
    TextView tv_singleEventMonth;

    @BindView(R.id.tv_singleEventYear)
    TextView tv_singleEventYear;
    @BindView(R.id.tv_singleEventTime)
    TextView tv_singleEventTime;

    @BindView(R.id.iv_feedPicture)
    ImageView ivFeedPicture;

    @BindView(R.id.iv_feedUserPic)
    PorterShapeImageView ivFeedUserPic;

    @BindView(R.id.rl_postShareContainer)
    RelativeLayout rlPostShareContainer;

    @BindView(R.id.ll_postLikeAndShareContainer)
    LinearLayout llPostLikeAndShareContainer;

    @BindView(R.id.iv_more)
    ImageView ivMore;

    @BindView(R.id.ll_feedPostWithTwoImages)
    LinearLayout llFeedPostWithTwoImages;
    @BindView(R.id.iv_twoImageViewFirst)
    ImageView ivTwoImageViewFirst;
    @BindView(R.id.iv_twoImageViewSecond)
    ImageView ivTwoImageViewSecond;


    @BindView(R.id.ll_feedPostWithThreeImages)
    LinearLayout llFeedPostWithThreeImages;
    @BindView(R.id.iv_threeImageViewFirst)
    ImageView ivThreeImageViewFirst;
    @BindView(R.id.iv_threeImageViewSecond)
    ImageView ivThreeImageViewSecond;
    @BindView(R.id.iv_threeImageViewThird)
    ImageView ivThreeImageViewThird;


    @BindView(R.id.ll_feedPostWithFourImagesOrMore)
    LinearLayout llFeedPostWithFourImagesOrMore;
    @BindView(R.id.iv_fourImageViewFirst)
    ImageView ivFourImageViewFirst;
    @BindView(R.id.iv_fourImageViewSecond)
    ImageView ivFourImageViewSecond;
    @BindView(R.id.iv_fourImageViewThird)
    ImageView ivFourImageViewThird;
    @BindView(R.id.iv_fourImageViewFourth)
    ImageView ivFourImageViewFourth;


    @BindView(R.id.rl_moreThanFourImageView)
    RelativeLayout rlMoreThanFourImageView;

    @BindView(R.id.tv_moreThanFourImageCount)
    TextView tvMoreThanFourImageCount;

    @BindView(R.id.tv_typeOfPost)
    TextView tvTypeOfPost;

    @BindView(R.id.tv_mobile)
    TextView tv_mobile;

    @BindView(R.id.rl_singleFeedPostContainer)
    RelativeLayout rlSingleFeedPostContainer;

    @BindView(R.id.ll_viewFeedPost)
    LinearLayout llViewFeedPost;

    @BindView(R.id.ll_likeFeedPost)
    LinearLayout llLikeFeedPost;
    @BindView(R.id.ll_likeFeedPost1)
    LinearLayout ll_likeFeedPost1;
    @BindView(R.id.ll_commentOnFeedPost)
    LinearLayout llCommentOnFeedPost;
    @BindView(R.id.ll_commentOnFeedPost1)
    LinearLayout llCommentOnFeedPost1;

    @BindView(R.id.ll_shareFeedPost)
    LinearLayout llShareFeedPost;

    @BindView(R.id.tv_postFeelingName)
    TextView tvPostFeelingName;
    @BindView(R.id.lly_noshare)
    LinearLayout lly_noshare;


    @BindView(R.id.lly_job_need)
    LinearLayout lly_job_need;


    @BindView(R.id.lly_share)
    LinearLayout lly_share;

    @BindView(R.id.lly_employee)
    LinearLayout lly_employee;

    @BindView(R.id.lly_event)
    LinearLayout lly_event;
    @BindView(R.id.tv_readmore)
    TextView tv_readmore;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;
    @BindView(R.id.tv_user_job_title1)
    TextView tv_user_job_title1;

    @BindView(R.id.tv_user_job_Email1)
    TextView tv_user_job_Email1;

    @BindView(R.id.tv_user_job_Mobile1)
    TextView tv_user_job_Mobile1;

    public static String count = "0";

    SinglePost post;

    int tempLike;

    static int imageClick = 0;

    public static int temp_comment_count = 0;

    public static followUnfollowTextChange mFollowtextchange;

    MenuPopupHelper popupForSelf, popupOther, popupForJob;

    MenuBuilder menuBuilder;
    MenuBuilder menuBuilder2;
    MenuBuilder menuBuilder3;
    EventList.DataBean singleEvent;
    Call<LikeUnlikeResponse> likeApiCall;
    LayoutInflater mInflater;
    private VHSinglePost(final Context context, View itemView) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

        ivFeedUserPic.setOnClickListener(this);

        tvFeedUserName.setOnClickListener(this);

        menuBuilder = new MenuBuilder(context);
        menuBuilder2 = new MenuBuilder(context);
        menuBuilder3 = new MenuBuilder(context);

        new MenuInflater(context).inflate(R.menu.user_post_pop_up_menu, menuBuilder);
        new MenuInflater(context).inflate(R.menu.other_post_pop_up_menu, menuBuilder2);
        new MenuInflater(context).inflate(R.menu.user_job_event_post_pop_up_menu, menuBuilder3);

        popupForSelf = new MenuPopupHelper(context, menuBuilder, ivMore);
        popupForSelf.setForceShowIcon(true);
        mInflater = (LayoutInflater) context.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = mInflater.inflate(R.layout.custom_popup_menu, null);

      //  menuBuilder = new MenuPopupHelper(context,)
        popupOther = new MenuPopupHelper(context, menuBuilder2, ivMore);
        popupOther.setForceShowIcon(true);

        popupForJob = new MenuPopupHelper(context, menuBuilder3, ivMore);
        popupForJob.setForceShowIcon(true);


        menuBuilder.setCallback(this);
        menuBuilder2.setCallback(this);
        menuBuilder3.setCallback(this);


        ivMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = "";
                if (post.isNotification()) {
                    title = "Off Notification for this post";
                } else {
                    title = "Start Notification for this post";
                }
                displayPopupWindow(ivMore, title);
                //  initiatePopupWindow();
            }
        });

        tvFeedLikeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Show list of people who liked the post
                final LikeListDialog lld = new LikeListDialog(context, post.getStatusId());
                lld.show();
            }
        });

    }

    public static VHSinglePost create(final Context context, final ViewGroup parent) {
        return new VHSinglePost(context, LayoutInflater.from(context)
                .inflate(R.layout.single_post_feed, parent, false));
    }

    public static void bind(final VHSinglePost vh, final SinglePost model) {

        vh.post = model;
        vh.tempLike = vh.post.getLikeCounts();

        vh.tvFeedDescription.setText("");


        Log.i(TAG, "on bind viewholder STATUS ID: " + vh.post.getStatusId());

        /** In Post List if user have images then this list will setup proper image layout, for Eg: 1 image then 1 imageview  **/

        if (vh.post.getImages() != null) {
            if (vh.post.getImages().size() > 0) {
                if (vh.post.getImages().size() == 1) {
                    vh.ivFeedPicture.setVisibility(View.VISIBLE);
                    vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
                    vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
                    vh.llFeedPostWithFourImagesOrMore.setVisibility(View.GONE);
                    vh.rlMoreThanFourImageView.setVisibility(View.GONE);
                    imageClick = 1;
                    if (vh.post.getImages().get(0).getImagestr().length() > 0) {
                        Picasso.with(vh.context)
                                .load(vh.post.getImages().get(0).getImagestr())
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .into(vh.ivFeedPicture);
                    }


                } else if (vh.post.getImages().size() == 2) {
                    vh.ivFeedPicture.setVisibility(View.GONE);
                    vh.llFeedPostWithTwoImages.setVisibility(View.VISIBLE);
                    vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
                    vh.llFeedPostWithFourImagesOrMore.setVisibility(View.GONE);
                    vh.rlMoreThanFourImageView.setVisibility(View.GONE);
                    imageClick = 2;
                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(0).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivTwoImageViewFirst.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(1).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivTwoImageViewSecond.setImageBitmap(resource);
                                }
                            });
                } else if (vh.post.getImages().size() == 3) {
                    vh.ivFeedPicture.setVisibility(View.GONE);
                    vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
                    vh.llFeedPostWithThreeImages.setVisibility(View.VISIBLE);
                    vh.llFeedPostWithFourImagesOrMore.setVisibility(View.GONE);
                    vh.rlMoreThanFourImageView.setVisibility(View.GONE);
                    imageClick = 3;
                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(0).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            //   .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivThreeImageViewFirst.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(1).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivThreeImageViewSecond.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(2).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivThreeImageViewThird.setImageBitmap(resource);
                                }
                            });
                } else if (vh.post.getImages().size() == 4) {
                    vh.ivFeedPicture.setVisibility(View.GONE);
                    vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
                    vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
                    vh.llFeedPostWithFourImagesOrMore.setVisibility(View.VISIBLE);
                    vh.rlMoreThanFourImageView.setVisibility(View.GONE);
                    imageClick = 4;
                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(0).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            //.crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewFirst.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(1).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewSecond.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(2).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            //.crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewThird.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(3).getImagestr()).asBitmap()
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewFourth.setImageBitmap(resource);
                                }
                            });
                } else if (vh.post.getImages().size() > 4) {
                    vh.ivFeedPicture.setVisibility(View.GONE);
                    vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
                    vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
                    vh.llFeedPostWithFourImagesOrMore.setVisibility(View.VISIBLE);
                    vh.rlMoreThanFourImageView.setVisibility(View.VISIBLE);
                    imageClick = 5;
                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(0).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            //.crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewFirst.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(1).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewSecond.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(2).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewThird.setImageBitmap(resource);
                                }
                            });

                    Glide.with(vh.context)
                            .load(vh.post.getImages().get(3).getImagestr()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            // .crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFourImageViewFourth.setImageBitmap(resource);
                                }
                            });

                    vh.tvMoreThanFourImageCount.setText("+" + (vh.post.getImages().size() - 4));
                }
                vh.lly_share.setVisibility(View.VISIBLE);
                vh.lly_noshare.setVisibility(View.GONE);
            } else {
                vh.lly_share.setVisibility(View.VISIBLE);
                vh.lly_noshare.setVisibility(View.GONE);
                vh.ivFeedPicture.setVisibility(View.GONE);
                vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
                vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
                vh.llFeedPostWithFourImagesOrMore.setVisibility(View.GONE);
                vh.rlMoreThanFourImageView.setVisibility(View.GONE);
            }
        } else {
            vh.ivFeedPicture.setVisibility(View.GONE);
            vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
            vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
            vh.llFeedPostWithFourImagesOrMore.setVisibility(View.GONE);
            vh.rlMoreThanFourImageView.setVisibility(View.GONE);
        }

        /** In Post list if feeling is set then we will get feeling detail */
        vh.tvFeedViewCount.setText(Integer.toString(vh.post.getPostViews()));
        vh.tvFeedViewCount1.setText(Integer.toString(vh.post.getPostViews()));
        if (vh.post.getFeelingName() != null) {
            if (!vh.post.getFeelingName().isEmpty()) {
                vh.tvPostFeelingName.setText("Feeling " + vh.post.getFeelingName() + " ");
            } else {
                vh.tvPostFeelingName.setText("");
            }
        } else {
            vh.tvPostFeelingName.setText("");
        }

        /** If the post is from any need help list. */
        if (model.isHelp()) {
            vh.lly_job_need.setVisibility(View.GONE);
            vh.lly_event.setVisibility(View.GONE);
            // vh.lly_need_job.setVisibility(View.GONE);

            vh.tvTypeOfPost.setVisibility(View.VISIBLE);
            vh.tvFeedDescription.setText("- " + model.getHelpTitle());
            // vh.llShareFeedPost.setVisibility(View.GONE);

            vh.lly_noshare.setVisibility(View.VISIBLE);

            vh.lly_share.setVisibility(View.GONE);
            switch (model.getHelpType()) {

                //Advise
                case 1:
                    vh.tvTypeOfPost.setText(R.string.help_advise);
                    vh.lly_employee.setVisibility(View.GONE);
                    vh.tvTypeOfPost.setBackgroundColor(vh.context.getResources()
                            .getColor(R.color.advise_help));
                    vh.rlSingleFeedPostContainer.setBackground(vh.context.getResources()
                            .getDrawable(R.drawable.advise_help_bg));
                    break;

                //Buy
                case 2:
                    vh.tvTypeOfPost.setText(R.string.help_buy);
                    vh.lly_employee.setVisibility(View.GONE);
                    vh.tvTypeOfPost.setBackgroundColor(vh.context.getResources()
                            .getColor(R.color.buy_help));
                    vh.rlSingleFeedPostContainer.setBackground(vh.context.getResources()
                            .getDrawable(R.drawable.buy_help_bg));
                    break;

                //Sale
                case 3:
                    vh.tvTypeOfPost.setText(R.string.help_sale);
                    vh.lly_employee.setVisibility(View.GONE);
                    vh.tvTypeOfPost.setBackgroundColor(vh.context.getResources()
                            .getColor(R.color.sale_help));
                    vh.rlSingleFeedPostContainer.setBackground(vh.context.getResources()
                            .getDrawable(R.drawable.sale_help_bg));
                    break;

            }
        }
        /** If the post is from any Event list. */
        else if (model.isIsEventPost()) {
            //   vh.llShareFeedPost.setVisibility(View.GONE);
            vh.lly_event.setVisibility(View.VISIBLE);
            // vh.lly_need_job.setVisibility(View.GONE);
            vh.lly_job_need.setVisibility(View.GONE);

            vh.lly_noshare.setVisibility(View.VISIBLE);
            vh.lly_share.setVisibility(View.GONE);
            vh.tvTypeOfPost.setVisibility(View.VISIBLE);
            vh.lly_employee.setVisibility(View.GONE);
            vh.tvFeedDescription.setText("- " + model.getEventsDetails().getEventName() + " At " + vh.post.getEventsDetails().getAddress());
            vh.tvTypeOfPost.setText("Event");
            vh.tvTypeOfPost.setBackgroundColor(vh.context.getResources()
                    .getColor(R.color.advise_help));
            vh.rlSingleFeedPostContainer.setBackground(vh.context.getResources()
                    .getDrawable(R.drawable.advise_help_bg));
            vh.tvFeedLocation.setVisibility(View.VISIBLE);
            vh.tvFeedLocation.setText(vh.post.getPlace());
            vh.tv_singleEventName.setText(vh.post.getEventsDetails().getEventName());
            vh.tv_singleEventHost.setText(vh.post.getEventsDetails().getHost());
            vh.tv_singleEventAddress.setText(vh.post.getEventsDetails().getAddress());
            vh.tv_singleEventContactPerson.setText(vh.post.getEventsDetails().getContactPerson());
            vh.tv_singleEventTime.setText(vh.post.getEventsDetails().getTime());
            vh.tvFeedTextStatus.setText(vh.post.getEventsDetails().getDescription());

            String[] date = new String[3];

            date[0] = "N/A";
            date[1] = "N/A";
            date[2] = "N/A";
//        String tempdate = Utils.convertMillsToDate(Long.valueOf(singleEvent.getDate()));
            if (vh.post.getEventsDetails().getDate().contains("-")) {
                date = vh.post.getEventsDetails().getDate().split("-");
            } else if (vh.post.getEventsDetails().getDate().contains("/")) {
                date = vh.post.getEventsDetails().getDate().split("/");
            }

            if (date[2].contains("T")) {
                date[2] = date[2].substring(0, date[2].split("T").length);
            }

            try {
                String month = Utils.month[Integer.parseInt(date[1]) - 1];
                Log.e("month", "" + month);
                vh.tv_singleEventMonth.setText(month);
                //vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[1])-1]);
            } catch (ArrayIndexOutOfBoundsException e) {
                if (date[1].contains("T")) {
                    date[1] = date[1].substring(0, date[1].split("T").length);
                }
                vh.tv_singleEventDay.setText(date[1]);
                try {
                    String month = Utils.month[Integer.parseInt(date[1]) - 1];
                    vh.tv_singleEventMonth.setText(month);

                    // vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[0]) - 1]);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                    Log.i(TAG, "ERROR HANDLING MONTH");
                }
            }
            vh.tv_singleEventDay.setText(date[2]);
            Log.e("date", "" + date);
            try {
                String month = Utils.month[Integer.parseInt(date[1]) - 1];
                vh.tv_singleEventMonth.setText(month);

//            vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[1])]);
            } catch (ArrayIndexOutOfBoundsException e) {
                vh.tv_singleEventDay.setText(date[1]);
                try {
                    String month = Utils.month[Integer.parseInt(date[1]) - 1];
                    vh.tv_singleEventMonth.setText(month);

                    //vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[0])]);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                    Log.i(TAG, "ERROR HANDLING MONTH");
                }
            }
            vh.tv_singleEventYear.setText(date[0]);


            if (vh.post.getEventsDetails().getEventImage().length() > 0) {
                vh.lly_event.setVisibility(View.VISIBLE);
                // vh.lly_need_job.setVisibility(View.GONE);

                vh.ivFeedPicture.setVisibility(View.VISIBLE);
                vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
                vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
                vh.llFeedPostWithFourImagesOrMore.setVisibility(View.GONE);
                vh.rlMoreThanFourImageView.setVisibility(View.GONE);

                Glide.with(vh.context)
                        .load(vh.post.getEventsDetails().getEventImage()).asBitmap()
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        // .crossFade()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                vh.ivFeedPicture.setImageBitmap(resource);
                            }
                        });
                vh.tv_singleEventName.setText(vh.post.getEventsDetails().getEventName());
                vh.tv_singleEventHost.setText(vh.post.getEventsDetails().getHost());
                vh.tv_singleEventAddress.setText(vh.post.getEventsDetails().getAddress());
                vh.tv_singleEventContactPerson.setText(vh.post.getEventsDetails().getContactPerson());
                vh.tv_singleEventTime.setText(vh.post.getEventsDetails().getTime());
            }

        }
        /** If the post is from any JOb or Want Employee list. */
        else if (model.isIsJobPost()) {
            // vh.llShareFeedPost.setVisibility(View.GONE);
            vh.lly_event.setVisibility(View.GONE);
            vh.lly_noshare.setVisibility(View.VISIBLE);
            vh.lly_share.setVisibility(View.GONE);
            vh.lly_job_need.setVisibility(View.GONE);

            vh.tvTypeOfPost.setVisibility(View.VISIBLE);
            vh.tvFeedDescription.setText("- " + vh.post.getJobDetails().getJobTitle());
            vh.tvFeedLocation.setVisibility(View.VISIBLE);
            vh.lly_employee.setVisibility(View.GONE);
            //  vh.lly_need_job.setVisibility(View.GONE);
            vh.tvFeedLocation.setText(vh.post.getJobDetails().getDistrict());

            if (model.getJobDetails().isIsWantEmployee()) {
                //  vh.lly_need_job.setVisibility(View.GONE);
                vh.lly_job_need.setVisibility(View.GONE);

                vh.lly_event.setVisibility(View.GONE);
                vh.tvTypeOfPost.setText("Need Help For Employee");
                vh.lly_employee.setVisibility(View.VISIBLE);
                vh.tv_company_name.setText(vh.post.getJobDetails().getCompanyName());
                vh.tv_employee_name.setText(vh.post.getUserName());
                vh.tv_job_title.setText(vh.post.getJobDetails().getJobTitle());
                vh.tv_vacancy.setText(vh.post.getJobDetails().getVacancy());
                vh.tv_mobile.setText(vh.post.getJobDetails().getMobile());

            } else {
                vh.tvTypeOfPost.setText("Need Help For Job");
                if (model.getJobDetails().getResume().length() > 0) {
                    // vh.lly_need_job.setVisibility(View.GONE);
                    vh.lly_job_need.setVisibility(View.GONE);

                    vh.ivFeedPicture.setVisibility(View.VISIBLE);
                    vh.lly_event.setVisibility(View.GONE);
                    vh.lly_employee.setVisibility(View.GONE);
                    vh.llFeedPostWithTwoImages.setVisibility(View.GONE);
                    vh.llFeedPostWithThreeImages.setVisibility(View.GONE);
                    vh.llFeedPostWithFourImagesOrMore.setVisibility(View.GONE);
                    vh.rlMoreThanFourImageView.setVisibility(View.GONE);
                    vh.tvFeedLocation.setVisibility(View.VISIBLE);
                    vh.tvFeedLocation.setText(vh.post.getJobDetails().getDistrict());
                    Glide.with(vh.context)
                            .load(vh.post.getJobDetails().getResume()).asBitmap()
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            //.crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFeedPicture.setImageBitmap(resource);
                                }
                            });
                } else {
                    vh.lly_job_need.setVisibility(View.VISIBLE);
                    vh.lly_share.setVisibility(View.GONE);

                    vh.tv_user_name.setText(vh.post.getUserName());
                    vh.tv_user_job_title1.setText(vh.post.getJobDetails().getJobTitle());
                    vh.tv_user_job_Email1.setText(vh.post.getJobDetails().getEmail());
                    vh.tv_user_job_Mobile1.setText(vh.post.getJobDetails().getMobile());

                }
            }
            vh.tvTypeOfPost.setBackgroundColor(vh.context.getResources()
                    .getColor(R.color.job_help));
            vh.rlSingleFeedPostContainer.setBackground(vh.context.getResources()
                    .getDrawable(R.drawable.buy_help_bg));


            //   vh.tvFeedLocation.setVisibility(View.VISIBLE);
            // vh.tvFeedLocation.setText(vh.post.getPlace());
        } else {
            vh.lly_event.setVisibility(View.GONE);
            vh.lly_job_need.setVisibility(View.GONE);

            //   vh.lly_need_job.setVisibility(View.GONE);
            vh.lly_employee.setVisibility(View.GONE);
            vh.tvTypeOfPost.setText("");
            vh.tvTypeOfPost.setVisibility(View.GONE);
            vh.rlSingleFeedPostContainer.setBackgroundColor(Color.WHITE);
        }

        if (vh.post.getStatus() != null) {
            if (vh.post.getStatus().length() > 0) {
                vh.tvFeedTextStatus.setVisibility(View.VISIBLE);
                vh.tvFeedTextStatus.setText(vh.post.getStatus());

            } else {
                vh.tvFeedTextStatus.setVisibility(View.GONE);
            }
        } else {
            vh.tvFeedTextStatus.setVisibility(View.GONE);

        }

        if (vh.post.isIsCheckIn()) {
            vh.tvFeedDescription.setText("Checked in");
            vh.tvFeedLocation.setVisibility(View.VISIBLE);
            vh.tvFeedLocation.setText(vh.post.getPlace());
        } else {
            vh.tvFeedLocation.setVisibility(View.INVISIBLE);
            //vh.tvFeedLocation.setText("");
            // vh.tvFeedLocation.setCompoundDrawables(null,null,null,null);
        }

        if (model.isIsEventPost()) {
            vh.lly_job_need.setVisibility(View.GONE);

            vh.lly_employee.setVisibility(View.GONE);
            vh.lly_event.setVisibility(View.VISIBLE);
            // vh.lly_need_job.setVisibility(View.GONE);

            vh.tvFeedLocation.setVisibility(View.VISIBLE);
            vh.tvFeedLocation.setText(vh.post.getEventsDetails().getLocation());
        }
        if (model.isIsJobPost()) {
            vh.lly_event.setVisibility(View.GONE);
            vh.tvFeedLocation.setVisibility(View.VISIBLE);
            vh.tvFeedLocation.setText(vh.post.getJobDetails().getCity());
        }
        if (vh.post.getImages() != null) {
            if (vh.post.getImages().size() > 0) {
                vh.lly_event.setVisibility(View.GONE);
                vh.lly_employee.setVisibility(View.GONE);
                vh.lly_job_need.setVisibility(View.GONE);


                if (model.isHelp()) {
                    switch (model.getHelpType()) {

                        //Advise
                        case 1:
                            vh.tvFeedDescription.setText("- " + model.getHelpTitle());

                            break;

                        //Buy
                        case 2:
                            vh.tvFeedDescription.setText(" - " + model.getHelpTitle());

                            break;

                        //Sale
                        case 3:
                            vh.tvFeedDescription.setText(" - " + model.getHelpTitle());

                            break;

                    }
                } else {
                    if (model.getImages().size() > 1) {
                        vh.tvFeedDescription.setText("Posted new photos");
                    } else {
                        if (model.getStatus().equalsIgnoreCase("Changed Profile Picture")) {
                            vh.tvFeedDescription.setText(model.getStatus());
                        } else {
                            vh.tvFeedDescription.setText("Posted new photo");
                        }

                    }

                }


            }
        }
        if (vh.post.getSharedOwner().length() > 0) {
            vh.lly_event.setVisibility(View.GONE);
            vh.lly_job_need.setVisibility(View.GONE);
            vh.tv_sharedpost.setVisibility(View.GONE);
            // vh.tv_sharedpost.setText("Shared  post  of ");
            vh.tvFeedUserName.setText(vh.post.getSharedOwner());
            String temp = "Shared  post of <font color="+"black"+"><b>" +vh.post.getUserName()+"</b></font>";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                ;
                vh.tvFeedDescription.setText(Html.fromHtml(temp,Html.FROM_HTML_MODE_COMPACT));
            }else{
                vh.tvFeedDescription.setText(Html.fromHtml(temp));

            }

           // vh.tvFeedDescription.setText("Shared  post of ");
            //vh.tv_feedsharename.setText(vh.post.getUserName());
            // vh.tvFeedDescription.setText(spannableString);

        }


        if (vh.post.getCommentCounts() > 0) {
            count = String.valueOf(vh.post.getCommentCounts());
            vh.tvFeedCommentCount.setText("" +
                    " " + vh.post.getCommentCounts());
            if(vh.post.isIsJobPost()){
                vh.tvFeedCommentCount1.setText("" +
                        " " + vh.post.getJobDetails().getCommentCountsX());

            }
            else if(vh.post.isIsEventPost()){
                vh.tvFeedCommentCount1.setText("" +
                        " " + vh.post.getEventsDetails().getCommentCounts());
            }

            else{
                vh.tvFeedCommentCount1.setText("" +
                        " " + vh.post.getCommentCounts());

            }
            //vh.tvFeedCommentCount.setText(vh.post.getCommentCounts());
            vh.tv_commentname.setText(R.string.comments);
            vh.tv_commentname1.setText(R.string.comments);
        } else {
            vh.tv_commentname.setText(R.string.comments);
            vh.tv_commentname1.setText(R.string.comments);
            //vh.tvFeedCommentCount.setTextColor(R.color.app_font_color);
            // vh.tvFeedCommentCount.setVisibility(View.INVISIBLE);
            if(vh.post.isIsJobPost()){
                vh.tvFeedCommentCount1.setText("" +
                        " " + vh.post.getJobDetails().getCommentCountsX());

            }else if(vh.post.isIsEventPost()){
                vh.tvFeedCommentCount1.setText("" +
                        " " + vh.post.getEventsDetails().getCommentCounts());
            }

            else {
                vh.tvFeedCommentCount1.setText("" +
                        " " + vh.post.getCommentCounts());
            }

                vh.tvFeedCommentCount.setText(Integer.toString(vh.post.getCommentCounts()));
           // vh.tvFeedCommentCount1.setText(Integer.toString(vh.post.getCommentCounts()));
        }

        vh.setLikeCounts(vh.post.getLikeCounts());

        vh.setShareCount();

        if (vh.post.getUserID().equals(App.getInstance().getUser().getId())) {
            vh.tvFeedUserName.setText(vh.post.getUserName());
            if (vh.post.getSharedOwner().length() > 0) {
                vh.tvFeedUserName.setText(vh.post.getSharedOwner());
            }
        } else {
            if (vh.post.getSharedOwner().length() > 0) {
                vh.tvFeedUserName.setText(vh.post.getSharedOwner());
            } else {
                vh.tvFeedUserName.setText(vh.post.getUserName());
            }

        }

        vh.tvFeedTime.setText(DateUtils.getRelativeDateTimeString(vh.context,
                Utils.convertDateToMills(vh.post.getCreateDate()),
                DateUtils.SECOND_IN_MILLIS,
                DateUtils.WEEK_IN_MILLIS,
                0));

        if (vh.post.getUserID().equals(App.getInstance().getUser().getId())) {
            if (vh.post.getUserProfileDetailsInfo().getUserImage() != null) {
                if (!vh.post.getUserProfileDetailsInfo().getUserImage().isEmpty()) {
                    if (Constants.USERPROFILE_IMAGE.length() > 0) {
                        Glide.with(vh.context)
                                .load(Constants.USERPROFILE_IMAGE).asBitmap()
                                .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.user_default)
                                .error(R.drawable.user_default)
                                //.crossFade()
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        vh.ivFeedUserPic.setImageBitmap(resource);
                                    }
                                });
                    } else {
                        Glide.with(vh.context)
                                .load(vh.post.getUserImage()).asBitmap()
                                .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.user_default)
                                .error(R.drawable.user_default)
                                // .crossFade()
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        vh.ivFeedUserPic.setImageBitmap(resource);
                                    }
                                });
                    }
                }
            }
        } else {
            if (vh.post.getUserImage() != null) {
                if (!vh.post.getUserImage().isEmpty()) {
                    Glide.with(vh.context)
                            .load(vh.post.getUserImage()).asBitmap()
                            .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.user_default)
                            .error(R.drawable.user_default)
                            //.crossFade()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    vh.ivFeedUserPic.setImageBitmap(resource);
                                }
                            });
                }
            }
        }

        if (vh.post.getSharedOwner().length() > 0) {
            Glide.with(vh.context)
                    .load(vh.post.getSharedOwnerImage()).asBitmap()
                    .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    // .crossFade()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            vh.ivFeedUserPic.setImageBitmap(resource);
                        }
                    });

        }

        if (vh.post.getTagDetails() != null) {
            if (vh.post.getTagDetails().size() > 0) {
                if (vh.post.getTagDetails().size() == 1) {
                    vh.tvFeedTagDetails.setText(" with " +
                            vh.post.getTagDetails().get(0).getFullName());
                } else if (vh.post.getTagDetails().size() == 2) {
                    vh.tvFeedTagDetails.setText(" with " +
                            vh.post.getTagDetails().get(0).getFullName() + " and "
                            + vh.post.getTagDetails().get(1).getFullName());
                } else {
                    vh.tvFeedTagDetails.setText(" with " +
                            vh.post.getTagDetails().get(0).getFullName() + " and "
                            + (vh.post.getTagDetails().size() - 1) + " other");
                }
            } else {
                vh.tvFeedTagDetails.setText("");
            }
        } else {
            vh.tvFeedTagDetails.setText("");
        }
        if (model.getStatus().equalsIgnoreCase("Changed Profile Picture")) {
            vh.tvFeedTextStatus.setVisibility(View.GONE);
        }

    }

    private void setShareCount() {
        if (post.getShareCounts() > 0) {
            tvFeedShareCount.setText(post.getShareCounts() + "");
            rlPostShareContainer.setVisibility(View.VISIBLE);
        } else {
            tvFeedShareCount.setText("");
            rlPostShareContainer.setVisibility(View.GONE);
        }
    }

    private void showDeleteConfirmationBox() {


        final DialogHideDeletePost hideDeletePost =
                new DialogHideDeletePost(context, post.getStatusId(), DialogHideDeletePost.Purpose.DELETE_POST);

        hideDeletePost.show();


    }


    private void setLikeCounts(int count) {
        if (count > 0) {
            tvFeedLikeCount.setText(count + "");
            llPostLikeAndShareContainer.setVisibility(View.VISIBLE);
        } else {
            tvFeedLikeCount.setText("");
            llPostLikeAndShareContainer.setVisibility(View.GONE);
        }
    }

    /**private void tryLoadingFeedImageAsync()
     {
     try
     {
     new AsyncTask<Void, Void, Bitmap>()
     {
     @Override protected Bitmap doInBackground(Void... voids)
     {
     if (post != null)
     {
     try
     {
     return Utils.getBitmapFromBase64(post.getImages().get(0).getImagestr());
     } catch (NullPointerException e)
     {
     return null;
     }

     }
     return null;
     }

     @Override protected void onPostExecute(Bitmap bitmap)
     {
     if (bitmap != null)
     {
     ivFeedPicture.setImageBitmap(bitmap);
     }
     }
     }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

     } catch (Exception e)
     {
     e.printStackTrace();
     }
     }*/


    /**
     * private void tryLoadingUserPicAsync()
     * {
     * try
     * {
     * new AsyncTask<Void, Void, Bitmap>()
     * {
     *
     * @Override protected Bitmap doInBackground(Void... voids)
     * {
     * return Utils.getBitmapFromBase64(post.getUser().getUserImage());
     * }
     * @Override protected void onPostExecute(Bitmap bitmap)
     * {
     * ivFeedUserPic.setImageBitmap(bitmap);
     * }
     * }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
     * <p>
     * } catch (Exception e)
     * {
     * e.printStackTrace();
     * }
     * }
     */

    private void displayPopupWindow(View anchorView, String title) {

        // IH Own Post
        if (post.getUserID().equals(App.getInstance().getUser().getId())) {
            if (post.isIsEventPost() || post.isIsJobPost()) {
                //popupForSelf.show();
                if (post.getSharedOwner().length() > 0) {

                    menuBuilder2.getItem(3).setTitle(title);

                    popupOther.show();
                } else {
                    menuBuilder3.getItem(3).setTitle(title);
                    popupForJob.show();
                }


            } else {
                if (post.getSharedOwner().length() > 0) {
                    menuBuilder2.getItem(3).setTitle(title);
                    popupOther.show();
                } else {
                    menuBuilder.getItem(4).setTitle(title);
                    popupForSelf.show();
                }

            }

        }
        //IF other User Post
        else {
            if (post.isIsEventPost() || post.isIsJobPost()) {

                menuBuilder2.getItem(3).setTitle(title);
                if (post.isFollow()) {
                    menuBuilder2.getItem(0).setTitle("UnFollow");
                } else {
                    menuBuilder2.getItem(0).setTitle("Follow");
                }
                popupOther.show();
            } else {
                menuBuilder2.getItem(3).setTitle(title);
                if (post.isFollow()) {
                    menuBuilder2.getItem(0).setTitle("UnFollow");
                } else {
                    menuBuilder2.getItem(0).setTitle("Follow");
                }
                popupOther.show();
            }

        }
    }

    private PopupWindow initiatePopupWindow() {
        LayoutInflater mInflater;
        PopupWindow mDropdown = null;
        try {

            mInflater = (LayoutInflater) context.getApplicationContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = mInflater.inflate(R.layout.custom_popup_menu, null);

            //If you want to add any listeners to your textviews, these are two //textviews.
            final TextView itema = (TextView) layout.findViewById(R.id.tv_name);


            final TextView itemb = (TextView) layout.findViewById(R.id.tv_title);


            layout.measure(View.MeasureSpec.UNSPECIFIED,
                    View.MeasureSpec.UNSPECIFIED);
            mDropdown = new PopupWindow(layout, FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT, true);
            mDropdown.showAsDropDown(ivMore, 5, 5);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDropdown;

    }

    @Override
    public void onClick(View view) {
        if (post.getSharedOwner().length() > 0) {
            post.setUserID(post.getSharedOwnerId());
        }
        Intent i = new Intent(context, ActivityUserProfile.class);
        i.putExtra(Constants.USER_ID, post.getUserID());
        if (post.getUserID().equals(App.getInstance().getUser().getId())) {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
        } else {
            i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
        }

        if (App.getInstance().profileNavigationList.size() > 0) {
            if (!App.getInstance().profileNavigationList.get(App.getInstance()
                    .profileNavigationList.size() - 1).equals(post.getUserID())) {
                context.startActivity(i);
            }
        } else {
            context.startActivity(i);
        }
    }

    private void promptUserForEditPost() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // LayoutInflater inflater =context.getApplicationContext().getL;
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView title = (TextView) titleView.findViewById(R.id.tv_title);

        //ImageView iv_seticon = (ImageView)titleView.findViewById(R.id.iv_seticon);
        // iv_seticon.setVisibility(View.VISIBLE);

        title.setText("Edit Post");


        new AlertDialog.Builder(context)
                .setTitle("Edit Post")
                .setCustomTitle(titleView)
                .setIcon(R.drawable.edit1)
                .setMessage("Are you sure you want to edit this post?")

                .setPositiveButton("EDIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intentPost = new Intent(context, ActivityPostToAgrizip.class);
                        intentPost.putExtra(Constants.POST_ID, post.getStatusId());
                        if (post.isHelp()) {
                            intentPost.putExtra(Constants.NEED_HELP, true);

                            switch (post.getHelpType()) {
                                //Advise
                                case 1:

                                    intentPost.putExtra(Constants.HELP_TYPE, Constants.ADVISE);
                                    break;

                                //Buy
                                case 2:
                                    intentPost.putExtra(Constants.HELP_TYPE, Constants.BUY);
                                    break;

                                //Sale
                                case 3:
                                    intentPost.putExtra(Constants.HELP_TYPE, Constants.SELL);
                                    break;

                            }
                        }
                        context.startActivity(intentPost);
                    }
                }).setNegativeButton("CANCEL", null)
                .show();

    }

    private void promptUserToHidePost() {
        final DialogHideDeletePost hideDeletePost =
                new DialogHideDeletePost(context, post.getStatusId(), DialogHideDeletePost.Purpose.HIDE_POST);

        hideDeletePost.show();

    }

    private void promptUserForUnfollow(boolean isFollow) {


        String title = "";
        if (isFollow) {
            title = "Unfollow";
        } else {
            title = "Follow";
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView tvtitle = (TextView) titleView.findViewById(R.id.tv_title);
        tvtitle.setText(title);

        new AlertDialog.Builder(context)
                .setTitle(title)
                .setCustomTitle(titleView)
                .setMessage("Are you sure you want to " + title + " " + post.getUserName() + "?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(title, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        unfollowUser();

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        popupOther.dismiss();
                    }
                }).show();
    }


    private void unfollowUser() {
        final UnfollowUser unfollowUser = new UnfollowUser();

        unfollowUser.setUserId(App.getInstance().getUser().getId());
        unfollowUser.setFollowUserId(post.getUserID());
        if (post.isFollow()) {
            unfollowUser.setIsFollow(false);
        } else {
            unfollowUser.setIsFollow(true);
        }

        unfollowUser.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        final RetrofitCallbacks<ResponseBody> onUnfollowUserCallback =
                new RetrofitCallbacks<ResponseBody>(context) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            if (post.isFollow()) {
                                post.setFollow(false);
                                if (mFollowtextchange != null) {
                                    mFollowtextchange.OnFollowTextChange(false, post.getUserID());
                                }
                                Toast.makeText(context, "You have unfollowed " + post.getUserName(), Toast.LENGTH_SHORT).show();
                            } else {
                                if (mFollowtextchange != null) {
                                    mFollowtextchange.OnFollowTextChange(true, post.getUserID());
                                }
                                post.setFollow(true);
                                Toast.makeText(context, "Now You have followed " + post.getUserName(), Toast.LENGTH_SHORT).show();

                            }


                        }
                    }
                };

        if (post.isFollow()) {
            API.getInstance().unfollowUser(unfollowUser, onUnfollowUserCallback);
        } else {
            API.getInstance().followUser(unfollowUser, onUnfollowUserCallback);
        }


    }


    @OnClick(R.id.ll_commentOnFeedPost)
    public void showCommentsFragment() {
        if (post.getPostCallbacksListener() != null) {
            temp_comment_count = getPosition();
            post.getPostCallbacksListener().onClickComments(post.getStatusId(), post.getCommentCounts());
        }
    }

    @OnClick(R.id.ll_commentOnFeedPost1)
    public void showCommentsFragment1() {
        if (post.getPostCallbacksListener() != null) {
            temp_comment_count = getPosition();
            post.getPostCallbacksListener().onClickComments(post.getStatusId(), post.getCommentCounts());
        }
    }

    public void promptUserForSelectingPostPrivacy() {
        final List<UserPrivacyDetails.DataBean> privacyList =
                App.getInstance().getUser().getUserPrivacyDetails().getData();

        final ArrayAdapter<UserPrivacyDetails.DataBean> privacyAdapter =
                new ArrayAdapter<UserPrivacyDetails.DataBean>(context, android.R.layout.simple_list_item_1, privacyList) {

                    @NonNull
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {
                            convertView = LayoutInflater.from(context).inflate(
                                    android.R.layout.simple_list_item_1, parent, false);
                        }
                        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

                        tv.setText(privacyList.get(position).getUserTypeName());

                        return convertView;

                    }
                };

        if (App.getInstance().getUser().getUserPrivacyDetails() == null) {
            return;
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView title = (TextView) titleView.findViewById(R.id.tv_title);
        title.setText("Select Post Privacy");
        new AlertDialog.Builder(context)
                .setCustomTitle(titleView)
                .setTitle("Select Post Privacy")
                .setIcon(R.drawable.edit_privacy)
                .setSingleChoiceItems(privacyAdapter, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        changePostPrivacy(privacyAdapter.getItem(i).getId());
                        dialogInterface.dismiss();
                    }
                })
                .show();

    }

    private void changePostPrivacy(String privacyId) {
        final RetrofitCallbacks<ResponseBody> onChangePostPrivactyCallback =
                new RetrofitCallbacks<ResponseBody>(context) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            Toast.makeText(context, "Post privacy changed", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Something went wrong please try later", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().editPostPrivacy(post.getStatusId(), privacyId, onChangePostPrivactyCallback);
    }


    @OnClick(R.id.ll_shareFeedPost)
    public void sharePost() {
        promptUserForSharingPost();
    }

    private void promptUserForSharingPost() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog_photo, null);
        TextView tvtitle = (TextView) titleView.findViewById(R.id.tv_title);

        tvtitle.setText("Share Post");
        new AlertDialog.Builder(context)
                .setCustomTitle(titleView)
                .setMessage("Are you sure you want to share this post?")
                .setPositiveButton("SHARE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        shareStatusPost();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();

    }

    private void shareStatusPost() {

        final SharePost sharePost = new SharePost();
        sharePost.setUserID(App.getInstance().getUser().getId());

        sharePost.setStatusId(post.getStatusId());
        sharePost.setMessage(post.getMessage());
        sharePost.setPlace(post.getPlace());
        sharePost.setUserID(App.getInstance().getUser().getId());
        //   sharePost.setShareId(post.getStatusId());
        sharePost.setStatus(post.getStatus());
        sharePost.setFeelingName(post.getFeelingName());
        sharePost.setFeelingValue(post.getFeelingValue());
        sharePost.setPostYear(post.getPostYear());
        sharePost.setPostMonth(post.getPostMonth());
        sharePost.setPostDay(post.getPostDay());
        sharePost.setPostHour(post.getPostHour());
        sharePost.setPostMinute(post.getPostMinute());
        sharePost.setHelp(post.isHelp());
        sharePost.setNews(post.isNews());
        sharePost.setVisibleTo(post.getVisibleTo());
        sharePost.setCreateDate(post.getCreateDate());
        sharePost.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");
        sharePost.setIsDelete(false);


        final RetrofitCallbacks<ResponseBody> onSharePostCallback =
                new RetrofitCallbacks<ResponseBody>(context) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            post.setShareCounts(post.getShareCounts() + 1);
                            setShareCount();
                            Toast.makeText(context, "Post Shared Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

        API.getInstance().sharePost(sharePost, onSharePostCallback);

    }

    @OnClick(R.id.ll_likeFeedPost1)
    public void likeThisFeedPost1() {

        if (likeApiCall != null) {
            likeApiCall.cancel();
        }

        LikeUnlikePost like = new LikeUnlikePost();

        if (post.isIsLiked()) {
            setLikeCounts(post.getLikeCounts() - 1);
            like.setIsLike(false);
        } else {
            setLikeCounts(tempLike + 1);
            like.setIsLike(true);
        }

        like.setStatusID(post.getStatusId());

        /*if (post.getUserID().equals(App.getInstance().getUser().getId()))
        {
            like.setUserID(post.getUserProfileDetailsInfo().getUserId());
        } else
        {
            like.setUserID(post.getUserID());
        }*/

        like.setUserID(App.getInstance().getUser().getId());

        like.setCreateDate(Calendar.getInstance().getTimeInMillis() + "");
        like.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        Log.i(TAG, "SENDING: " + new Gson().toJson(like));

        likeApiCall = API.getInstance().likeOnPost(like, new RetrofitCallbacks<LikeUnlikeResponse>(context) {

            @Override
            public void onResponse(Call<LikeUnlikeResponse> call, Response<LikeUnlikeResponse> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    Log.i(TAG, "SUCCESSFULLY LIKED");
                    post.setIsLiked(response.body().getData().isIsLike());
                    if (post.isIsLiked()) {
                        post.setLikeCounts(post.getLikeCounts() + 1);
                    } else {
                        post.setLikeCounts(post.getLikeCounts() - 1);
                        tempLike = post.getLikeCounts();
                    }

                    setLikeCounts(post.getLikeCounts());
                } else {
                    post.setLikeCounts(post.getLikeCounts());
                    try {
                        Log.i(TAG, "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeUnlikeResponse> call, Throwable t) {
                super.onFailure(call, t);
                if (!call.isCanceled()) {
                    setLikeCounts(post.getLikeCounts());
                }
            }
        });
    }

    @OnClick(R.id.ll_likeFeedPost)
    public void likeThisFeedPost() {

        if (likeApiCall != null) {
            likeApiCall.cancel();
        }

        LikeUnlikePost like = new LikeUnlikePost();

        if (post.isIsLiked()) {
            setLikeCounts(post.getLikeCounts() - 1);
            like.setIsLike(false);
        } else {
            setLikeCounts(tempLike + 1);
            like.setIsLike(true);
        }

        like.setStatusID(post.getStatusId());

        /*if (post.getUserID().equals(App.getInstance().getUser().getId()))
        {
            like.setUserID(post.getUserProfileDetailsInfo().getUserId());
        } else
        {
            like.setUserID(post.getUserID());
        }*/

        like.setUserID(App.getInstance().getUser().getId());

        like.setCreateDate(Calendar.getInstance().getTimeInMillis() + "");
        like.setUpdateDate(Calendar.getInstance().getTimeInMillis() + "");

        Log.i(TAG, "SENDING: " + new Gson().toJson(like));

        likeApiCall = API.getInstance().likeOnPost(like, new RetrofitCallbacks<LikeUnlikeResponse>(context) {

            @Override
            public void onResponse(Call<LikeUnlikeResponse> call, Response<LikeUnlikeResponse> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    Log.i(TAG, "SUCCESSFULLY LIKED");
                    post.setIsLiked(response.body().getData().isIsLike());
                    if (post.isIsLiked()) {
                        post.setLikeCounts(post.getLikeCounts() + 1);
                    } else {
                        post.setLikeCounts(post.getLikeCounts() - 1);
                        tempLike = post.getLikeCounts();
                    }

                    setLikeCounts(post.getLikeCounts());
                } else {
                    post.setLikeCounts(post.getLikeCounts());
                    try {
                        Log.i(TAG, "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeUnlikeResponse> call, Throwable t) {
                super.onFailure(call, t);
                if (!call.isCanceled()) {
                    setLikeCounts(post.getLikeCounts());
                }
            }
        });
    }


    @Override
    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
        Log.i(TAG, "ON MENU ITEM CLICK");


        switch (item.getItemId()) {

            case R.id.action_deletePost:

                showDeleteConfirmationBox();
                break;

            case R.id.action_editPost:
                promptUserForEditPost();
                break;

            case R.id.action_editPostPrivacy:
                promptUserForSelectingPostPrivacy();
                break;

            case R.id.action_hidePost:
                promptUserToHidePost();
                break;

            case R.id.action_hidePostOther:
                promptUserToHidePost();
                break;

            case R.id.action_unfollowPostOther:
                promptUserForUnfollow(post.isFollow());
                break;

            case R.id.action_reportPostOther:
                new DialogReportPost(context, post.getStatusId()).show();
                break;
            case R.id.action_startNotification:
                TurnOnOffNotification();
                break;
            case R.id.action_startNotificationOther:
                TurnOnOffNotification();
                break;


        }
        return true;
    }

    @Override
    public void onMenuModeChange(MenuBuilder menu) {

    }

    public void TurnOnOffNotification() {

        final RetrofitCallbacks<ResponseBody> TurnOnOffNotification =
                new RetrofitCallbacks<ResponseBody>(context) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            if (post.isNotification()) {
                                post.setNotification(false);
                            } else {
                                post.setNotification(true);
                            }


                        }
                    }
                };

        API.getInstance().TurnOnOffNotification(post.getStatusId(), post.getUserID(), TurnOnOffNotification);


    }


    // @OnClick({R.id.ll_feedPostWithFourImagesOrMore, R.id.ll_feedPostWithTwoImages, R.id.ll_feedPostWithThreeImages, R.id.iv_feedPicture, R.id.lly_employee, R.id.lly_event})
    @OnClick({R.id.iv_feedPicture, R.id.lly_employee, R.id.lly_event, R.id.lly_job_need})
    void onClickPostImages() {
        if (post.isIsJobPost() || post.isIsEventPost()) {
            if (post.isIsEventPost()) {

                Intent viewSingleEvent = new Intent(context, ViewPostedEvent.class);
                viewSingleEvent.putExtra("UserImage", post.getEventsDetails().getEventImage());
                viewSingleEvent.putExtra("EventName", post.getEventsDetails().getEventName());
                viewSingleEvent.putExtra("Host", post.getEventsDetails().getHost());
                viewSingleEvent.putExtra("Address", post.getEventsDetails().getAddress());
                viewSingleEvent.putExtra("UserName", post.getUserName());
                viewSingleEvent.putExtra("Contact", post.getEventsDetails().getContactPerson());
                viewSingleEvent.putExtra("Time", post.getEventsDetails().getTime());
                viewSingleEvent.putExtra("Date", post.getEventsDetails().getDate());
                viewSingleEvent.putExtra("Desc", post.getEventsDetails().getDescription());
                //startActivity(viewSingleEvent);
                context.startActivity(viewSingleEvent);
            } else {
                RequireEmployeeList.DataBean singleEmployeeRequest = null;
                RequireJobList.DataBean singleJobRequest = null;

                if (post.getJobDetails().isIsWantJob()) {
                    singleJobRequest = new RequireJobList.DataBean();
                    singleJobRequest.setJobId(post.getJobDetails().getJobId());
                    singleJobRequest.setUserId(post.getJobDetails().getUserId());
                    singleJobRequest.setUserName(post.getUserName());
                    singleJobRequest.setUserImage(post.getUserImage());
                    singleJobRequest.setCompanyName(post.getJobDetails().getCompanyName());
                    singleJobRequest.setJobTitle(post.getJobDetails().getJobTitle());
                    singleJobRequest.setVacancy(post.getJobDetails().getVacancy());
                    singleJobRequest.setExperience(post.getJobDetails().getExperience());
                    singleJobRequest.setEmail(post.getJobDetails().getEmail());
                    singleJobRequest.setMobile(post.getJobDetails().getMobile());
                    singleJobRequest.setEducation(post.getJobDetails().getEducation());
                    singleJobRequest.setJobDuration(post.getJobDetails().getJobDuration());
                    singleJobRequest.setStreet(post.getJobDetails().getStreet());
                    singleJobRequest.setCity(post.getJobDetails().getCity());
                    singleJobRequest.setDistrict(post.getJobDetails().getDistrict());
                    singleJobRequest.setState(post.getJobDetails().getState());
                    singleJobRequest.setZipCode(post.getJobDetails().getZipCode());
                    singleJobRequest.setResume(post.getJobDetails().getResume());
                    singleJobRequest.setLikeCounts(post.getLikeCounts());
                    singleJobRequest.setCommentCounts(post.getCommentCounts());
                    singleJobRequest.setIsLiked(post.isIsLiked());
                    singleJobRequest.setIsWantJob(true);
                    singleJobRequest.setIsWantEmployee(false);
                    singleJobRequest.setCreateDate(post.getCreateDate());

                } else {
                    singleEmployeeRequest = new RequireEmployeeList.DataBean();
                    singleEmployeeRequest.setJobId(post.getJobDetails().getJobId());
                    singleEmployeeRequest.setUserId(post.getJobDetails().getUserId());
                    singleEmployeeRequest.setUserName(post.getUserName());
                    singleEmployeeRequest.setUserImage(post.getUserImage());
                    singleEmployeeRequest.setCompanyName(post.getJobDetails().getCompanyName());
                    singleEmployeeRequest.setJobTitle(post.getJobDetails().getJobTitle());
                    singleEmployeeRequest.setVacancy(post.getJobDetails().getVacancy());
                    singleEmployeeRequest.setExperience(post.getJobDetails().getExperience());
                    singleEmployeeRequest.setEmail(post.getJobDetails().getEmail());
                    singleEmployeeRequest.setMobile(post.getJobDetails().getMobile());
                    singleEmployeeRequest.setEducation(post.getJobDetails().getEducation());
                    singleEmployeeRequest.setJobDuration(post.getJobDetails().getJobDuration());
                    singleEmployeeRequest.setStreet(post.getJobDetails().getStreet());
                    singleEmployeeRequest.setCity(post.getJobDetails().getCity());
                    singleEmployeeRequest.setDistrict(post.getJobDetails().getDistrict());
                    singleEmployeeRequest.setState(post.getJobDetails().getState());
                    singleEmployeeRequest.setZipCode(post.getJobDetails().getZipCode());
                    singleEmployeeRequest.setResume(post.getJobDetails().getResume());
                    singleEmployeeRequest.setLikeCounts(post.getLikeCounts());
                    singleEmployeeRequest.setCommentCounts(post.getCommentCounts());
                    singleEmployeeRequest.setIsLiked(post.isIsLiked());
                    singleEmployeeRequest.setIsWantJob(false);
                    singleEmployeeRequest.setIsWantEmployee(true);
                    singleEmployeeRequest.setCreateDate(post.getCreateDate());
                }

                final Intent fullViewActivity = new Intent(context, ActivityJobOrEmployeeFullView.class);

                if (singleJobRequest != null) {
                    fullViewActivity.putExtra(Constants.NEED_JOB, true);
                    fullViewActivity.putExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT,
                            singleJobRequest);
                } else if (singleEmployeeRequest != null) {
                    fullViewActivity.putExtra(Constants.NEED_JOB, false);
                    fullViewActivity.putExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT,
                            singleEmployeeRequest);
                }

                context.startActivity(fullViewActivity);
            }

        } else {

            ArrayList<PostImageList.DataBean> images = new ArrayList<>();
            for (SinglePost.ImagesBean postImage : post.getImages()) {
                final PostImageList.DataBean d = new PostImageList.DataBean();
                d.setImageId(postImage.getStatusImageId());
                d.setImagePath(postImage.getImagestr());
                d.setStatusId(post.getStatusId());

                images.add(d);
            }
            Intent i = new Intent(context, ActivityFullPhotoView.class);

            i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, images);
            i.putExtra("PostUserId", post.getUserID());
            i.putExtra("CMMENT_COUNT", post.getCommentCounts());

            i.putExtra("LIKE_COUNT", post.getLikeCounts());

            i.putExtra("SHARE_COUNT", post.getShareCounts());

            i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, 0);
            i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);
            temp_comment_count = getPosition();
            if (context.toString().contains("Home")) {
                Constants.COME_FROM_HOME = 1;
            } else {
                Constants.COME_FROM_HOME = 2;
            }
            //context.startActivity(i);
            ((Activity) context).startActivityForResult(i, 444);

        }
    }

    @OnClick(R.id.iv_twoImageViewFirst)
    public void iv_twoImageViewFirstClick() {
        whenClickonFirstImage();

    }

    public void whenClickonFirstImage() {
        ArrayList<PostImageList.DataBean> images = new ArrayList<>();
        for (SinglePost.ImagesBean postImage : post.getImages()) {
            final PostImageList.DataBean d = new PostImageList.DataBean();
            d.setImageId(postImage.getStatusImageId());
            d.setImagePath(postImage.getImagestr());
            d.setStatusId(post.getStatusId());

            images.add(d);
        }
        commonIntentPass(images);
    }

    @OnClick(R.id.iv_twoImageViewSecond)
    public void iv_twoImageViewSecondClick() {
        ArrayList<PostImageList.DataBean> images = new ArrayList<>();
        PostImageList.DataBean d = null;
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(1).getStatusImageId());
        d.setImagePath(post.getImages().get(1).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(0).getStatusImageId());
        d.setImagePath(post.getImages().get(0).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);
        commonIntentPass(images);
       /* for (SinglePost.ImagesBean postImage : post.getImages()) {
            final PostImageList.DataBean d = new PostImageList.DataBean();
            d.setImageId(postImage.getStatusImageId());
            d.setImagePath(postImage.getImagestr());
            d.setStatusId(post.getStatusId());

            images.add(d);
        }*/

    }

    public void commonIntentPass(ArrayList<PostImageList.DataBean> images) {
        Intent i = new Intent(context, ActivityFullPhotoView.class);

        i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, images);
        i.putExtra("PostUserId", post.getUserID());
        i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, 0);
        i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);
        //  i.putExtra("COMMENT_COUNT",post.getCommentCounts());
        i.putExtra("CMMENT_COUNT", post.getCommentCounts());
        i.putExtra("LIKE_COUNT", post.getLikeCounts());
        i.putExtra("SHARE_COUNT", post.getShareCounts());

        if (context.toString().contains("Home")) {
            Constants.COME_FROM_HOME = 1;
        } else {
            Constants.COME_FROM_HOME = 2;
        }
        // context.startActivity(i);
        temp_comment_count = getPosition();
        ((Activity) context).startActivityForResult(i, 444);
    }

    @OnClick(R.id.iv_threeImageViewFirst)
    public void iv_threeImageViewFirstClick() {
        whenClickonFirstImage();
    }

    @OnClick(R.id.iv_threeImageViewSecond)
    public void iv_threeImageViewSecond() {
        ArrayList<PostImageList.DataBean> images = new ArrayList<>();
        PostImageList.DataBean d = null;
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(1).getStatusImageId());
        d.setImagePath(post.getImages().get(1).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(0).getStatusImageId());
        d.setImagePath(post.getImages().get(0).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(2).getStatusImageId());
        d.setImagePath(post.getImages().get(2).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);

        commonIntentPass(images);
    }

    @OnClick(R.id.iv_threeImageViewThird)
    public void iv_threeImageViewThird() {
        ArrayList<PostImageList.DataBean> images = new ArrayList<>();
        PostImageList.DataBean d = null;
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(2).getStatusImageId());
        d.setImagePath(post.getImages().get(2).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(0).getStatusImageId());
        d.setImagePath(post.getImages().get(0).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);
        d = new PostImageList.DataBean();
        d.setImageId(post.getImages().get(1).getStatusImageId());
        d.setImagePath(post.getImages().get(1).getImagestr());
        d.setStatusId(post.getStatusId());
        images.add(d);

        commonIntentPass(images);
    }

    public void tempdata(int imageindex) {
        int imageposition1 = 0, imageposition2 = 0, imageposition3 = 0, imageposition4 = 0;

        ArrayList<PostImageList.DataBean> images = new ArrayList<>();
        if(post.getImages().size() > 4){
         // post.getImages().get(imageindex);


          List<SinglePost.ImagesBean> imgesList = new ArrayList<>();
          imgesList = post.getImages();
         // ArrayList<PostImageList.DataBean> images = new ArrayList<>();
          PostImageList.DataBean d = null;
          d = new PostImageList.DataBean();
          d.setImageId(post.getImages().get(imageindex).getStatusImageId());
          d.setImagePath(post.getImages().get(imageindex).getImagestr());
          d.setStatusId(post.getStatusId());
          images.add(d);
         // imgesList.remove(imageindex);
          for(int i=0;i<imgesList.size();i++){
              if(i == imageindex){

              }else {
                  d = new PostImageList.DataBean();
                  d.setImageId(imgesList.get(i).getStatusImageId());
                  d.setImagePath(imgesList.get(i).getImagestr());
                  d.setStatusId(post.getStatusId());

                  images.add(d);
              }
          }
         // images = post.getImages();


      }else {
          if (imageindex == 1) {
              imageposition1 = 1;
              imageposition2 = 2;
              imageposition3 = 3;
              imageposition4 = 0;
          }
          if (imageindex == 2) {
              imageposition1 = 2;
              imageposition2 = 3;
              imageposition3 = 1;
              imageposition4 = 0;
          }
          if (imageindex == 3) {
              imageposition1 = 3;
              imageposition2 = 2;
              imageposition3 = 1;
              imageposition4 = 0;
          }


          PostImageList.DataBean d = null;
          d = new PostImageList.DataBean();
          d.setImageId(post.getImages().get(imageposition1).getStatusImageId());
          d.setImagePath(post.getImages().get(imageposition1).getImagestr());
          d.setStatusId(post.getStatusId());
          images.add(d);
          d = new PostImageList.DataBean();
          d.setImageId(post.getImages().get(imageposition2).getStatusImageId());
          d.setImagePath(post.getImages().get(imageposition2).getImagestr());
          d.setStatusId(post.getStatusId());
          images.add(d);
          d = new PostImageList.DataBean();
          d.setImageId(post.getImages().get(imageposition3).getStatusImageId());
          d.setImagePath(post.getImages().get(imageposition3).getImagestr());
          d.setStatusId(post.getStatusId());
          images.add(d);
          d = new PostImageList.DataBean();
          d.setImageId(post.getImages().get(imageposition4).getStatusImageId());
          d.setImagePath(post.getImages().get(imageposition4).getImagestr());
          d.setStatusId(post.getStatusId());
          images.add(d);
      }

        commonIntentPass(images);

    }

    @OnClick(R.id.iv_fourImageViewFirst)
    public void iv_fourImageViewFirst() {
        whenClickonFirstImage();
    }

    @OnClick(R.id.iv_fourImageViewSecond)
    public void iv_fourImageViewSecond() {
        tempdata(1);
    }

    @OnClick(R.id.iv_fourImageViewThird)
    public void iv_fourImageViewThird() {
        tempdata(2);
    }

    @OnClick(R.id.iv_fourImageViewFourth)
    public void iv_fourImageViewFourth() {
        tempdata(3);
    }

    public interface followUnfollowTextChange {
        void OnFollowTextChange(boolean isFollow, String UserId);
    }

}


