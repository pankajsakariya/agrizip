package viewholders;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.stories.activity.ActivityFullStory;

import apimodels.StoryList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;

/**
 * Created by rutvik on 2/13/2017 at 4:13 PM.
 */

public class VHStoryListItem extends RecyclerView.ViewHolder
{

    final Context context;

    @BindView(R.id.tv_storyTitle)
    TextView tvStoryTitle;

    @BindView(R.id.tv_storyEditorAndDate)
    TextView tvStoryEditorAndDate;

    @BindView(R.id.tv_storyDescription)
    TextView tvStoryDescription;

    @BindView(R.id.tv_read_more)
    TextView tv_read_more;

    @BindView(R.id.tv_storyTime)
    TextView tvStoryTime;

    StoryList model;

    public VHStoryListItem(final Context context, View itemView)
    {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (model != null)
                {

                    Intent storyFullView = new Intent(context, ActivityFullStory.class);
                    storyFullView.putExtra(Constants.SINGLE_STORY, model);
                    storyFullView.putExtra("FLAG",1);
                    context.startActivity(storyFullView);
                }
            }
        });

    }

    public static VHStoryListItem create(final Context context, final ViewGroup parent)
    {
        return new VHStoryListItem(context, LayoutInflater.from(context)
                .inflate(R.layout.single_story_list_item, parent, false));
    }

    public static void bind(final VHStoryListItem vh, StoryList model)
    {
        vh.tv_read_more.setText("[Read Full Story...]");
        String readmore=vh.tv_read_more.getText().toString();
        vh.model = model;
        vh.tvStoryTitle.setText(model.getTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            vh.tvStoryDescription.setText(Html.fromHtml(model.getShortDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else
        {
            vh.tvStoryDescription.setText(Html.fromHtml(model.getShortDescription()));
        }
        String FromTime= Utils.changeDateFormat(model.getTime(),"HH:mm:ss","hh:mm a");
        vh.tvStoryTime.setText(FromTime);
       String FromDate= Utils.changeDateFormat(model.getDate(),"yyyy-MM-dd","MMMM dd,yyyy");
        vh.tvStoryEditorAndDate.setText("By " + model.getAuthor() + " | " + FromDate);
    }


}
