package viewholders;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.event.activity.ViewPostedEvent;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;
import java.util.List;

import apimodels.EventList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Utils;

/**
 * Created by rutvik on 1/18/2017 at 1:05 PM.
 */

public class VHSingelEventItem extends RecyclerView.ViewHolder {

    private static final String TAG = App.APP_TAG + VHSingelEventItem.class.getSimpleName();
    final Context context;

    @BindView(R.id.iv_singleEventCoverPic)
    ImageView ivSingleEventCoverPic;

    @BindView(R.id.tv_singleEventHost)
    TextView tvSingleEventHost;

    @BindView(R.id.tv_singleEventDay)
    TextView tvSingleEventDay;

    @BindView(R.id.tv_singleEventMonth)
    TextView tvSingleEventMonth;

    @BindView(R.id.tv_singleEventYear)
    TextView tvSingleEventYear;

    @BindView(R.id.tv_singleEventTime)
    TextView tvSingleEventTime;

    @BindView(R.id.tv_singleEventName)
    TextView tvSingleEventName;

    @BindView(R.id.tv_singleEventAddress)
    TextView tvSingleEventAddress;

    @BindView(R.id.tv_singleEventContactPerson)
    TextView tvSingleEventNContactPerson;

    @BindView(R.id.ll_singleEventContainer)
    LinearLayout ll_singleEventContainer;

    EventList.DataBean singleEvent;
    List<EventList.DataBean> event=new ArrayList<>();

    public VHSingelEventItem(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;



    }

    public static VHSingelEventItem create(final Context context, final ViewGroup parent) {
        return new VHSingelEventItem(context, LayoutInflater.from(context)
                .inflate(R.layout.single_event_row_item, parent, false));
    }

    public static void bind(final VHSingelEventItem vh, final EventList.DataBean singleEvent) {



        vh.singleEvent = singleEvent;

       vh.event.add(singleEvent);

        Glide.with(vh.context)
                .load(singleEvent.getEventImage()).asBitmap()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
               // .crossFade()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        vh.ivSingleEventCoverPic.setImageBitmap(resource);
                    }
                });

        vh.tvSingleEventName.setText(singleEvent.getEventName());
        vh.tvSingleEventHost.setText(singleEvent.getHost());
        vh.tvSingleEventAddress.setText(singleEvent.getAddress());

        String[] date = new String[3];

        date[0] = "N/A";
        date[1] = "N/A";
        date[2] = "N/A";
//        String tempdate = Utils.convertMillsToDate(Long.valueOf(singleEvent.getDate()));
        if (singleEvent.getDate().contains("-")) {
            date = singleEvent.getDate().split("-");
        } else if (singleEvent.getDate().contains("/")) {
            date = singleEvent.getDate().split("/");
        }

        if (date[2].contains("T")) {
            date[2] = date[2].substring(0, date[2].split("T").length);
        }

        try {
            String month = Utils.month[Integer.parseInt(date[1]) - 1];
            Log.e("month", "" + month);
            vh.tvSingleEventMonth.setText(month);
            //vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[1])-1]);
        } catch (ArrayIndexOutOfBoundsException e) {
            if (date[1].contains("T")) {
                date[1] = date[1].substring(0, date[1].split("T").length);
            }
            vh.tvSingleEventDay.setText(date[1]);
            try {
                String month = Utils.month[Integer.parseInt(date[1]) - 1];
                vh.tvSingleEventMonth.setText(month);

                // vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[0]) - 1]);
            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.printStackTrace();
                Log.i(TAG, "ERROR HANDLING MONTH");
            }
        }
        vh.tvSingleEventDay.setText(date[2]);
        Log.e("date", "" + date);
        try {
            String month = Utils.month[Integer.parseInt(date[1]) - 1];
            vh.tvSingleEventMonth.setText(month);

//            vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[1])]);
        } catch (ArrayIndexOutOfBoundsException e) {
            vh.tvSingleEventDay.setText(date[1]);
            try {
                String month = Utils.month[Integer.parseInt(date[1]) - 1];
                vh.tvSingleEventMonth.setText(month);

                //vh.tvSingleEventMonth.setText(Utils.month[Integer.parseInt(date[0])]);
            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.printStackTrace();
                Log.i(TAG, "ERROR HANDLING MONTH");
            }
        }
        vh.tvSingleEventYear.setText(date[0]);


        vh.tvSingleEventTime.setText(singleEvent.getTime());

        vh.tvSingleEventNContactPerson.setText(singleEvent.getUserName() + "- +91" +
                singleEvent.getContactPerson());

       vh.ll_singleEventContainer.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //clickOnEventContainer(singleEvent);

                        System.out.println("Hello " + singleEvent.getEventName());
                        Intent viewSingleEvent=new Intent(vh.context, ViewPostedEvent.class);
                        viewSingleEvent.putExtra("UserImage",singleEvent.getEventImage());
                        viewSingleEvent.putExtra("EventName", singleEvent.getEventName());
                        viewSingleEvent.putExtra("Host", singleEvent.getHost());
                        viewSingleEvent.putExtra("Address", singleEvent.getAddress());
                        viewSingleEvent.putExtra("UserName",singleEvent.getUserName());
                        viewSingleEvent.putExtra("Contact", singleEvent.getContactPerson());
                        viewSingleEvent.putExtra("Time", singleEvent.getTime());
                        viewSingleEvent.putExtra("Date", singleEvent.getDate());
                        viewSingleEvent.putExtra("Desc", singleEvent.getDescription());
                        vh.context.startActivity(viewSingleEvent);


                    }
                }
        );


    }


    public  void clickOnEventContainer(EventList.DataBean singleEvent){





        Intent viewSingleEvent=new Intent(context, ViewPostedEvent.class);
        viewSingleEvent.putExtra("UserImage",singleEvent.getEventImage());
        viewSingleEvent.putExtra("EventName", singleEvent.getEventName());
        viewSingleEvent.putExtra("Host", singleEvent.getHost());
        viewSingleEvent.putExtra("Address", singleEvent.getAddress());
        viewSingleEvent.putExtra("UserName",singleEvent.getUserName());
        viewSingleEvent.putExtra("Contact", singleEvent.getContactPerson());
        viewSingleEvent.putExtra("Time", singleEvent.getTime());
        viewSingleEvent.putExtra("Date", singleEvent.getDate());
        viewSingleEvent.putExtra("Desc", singleEvent.getDescription());
        context.startActivity(viewSingleEvent);



    }
}
