package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.agrizip.R;
import com.app.agrizip.album.album_create.ActivityCreateAlbum;
import com.bumptech.glide.Glide;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;

/**
 * Created by rutvik on 1/5/2017 at 12:18 PM.
 */

public class VHSingleAlbumPhoto extends RecyclerView.ViewHolder implements View.OnClickListener
{

    final Context context;
    final ImagePicker imagePicker;
    @BindView(R.id.iv_singleAlbumPhoto)
    ImageView ivSingleAlbumPhoto;
    String filePath;

    private VHSingleAlbumPhoto(Context context, View itemView, ImagePicker imagePicker)
    {
        super(itemView);
        this.context = context;
        this.imagePicker = imagePicker;
        ButterKnife.bind(this, itemView);

        ivSingleAlbumPhoto.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                view.setAlpha(.7f);
                return false;
            }
        });

        ivSingleAlbumPhoto.setOnClickListener(this);

    }

    public static VHSingleAlbumPhoto create(final Context context, final ViewGroup parent, ImagePicker imagePicker)
    {
        return new VHSingleAlbumPhoto(context, LayoutInflater.from(context)
                .inflate(R.layout.single_album_photo_row_item, parent, false), imagePicker);
    }

    public static void bind(final VHSingleAlbumPhoto vh, final String filePath)
    {
        vh.filePath = filePath;
        if (filePath.equals(Constants.ADD_NEW_PHOTO_TO_ALBUM))
        {
            vh.ivSingleAlbumPhoto.setImageResource(R.drawable.click_to_add_photo);
        } else
        {
            Glide.with(vh.context)
                    .load(filePath)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivSingleAlbumPhoto);
        }
    }


    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.iv_singleAlbumPhoto)
        {
            if (filePath.equals(Constants.ADD_NEW_PHOTO_TO_ALBUM))
            {
                imagePicker.start(ActivityCreateAlbum.REQUEST_CODE_PICKER);
            }
        }
    }
}
