package viewholders;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;

import java.io.IOException;

import api.API;
import api.RetrofitCallbacks;
import apimodels.UserProfileDetailsInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import jp.wasabeef.glide.transformations.BlurTransformation;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 11/22/2016 at 11:03 AM.
 */

public class VHBigProfileViewWithDetails extends RecyclerView.ViewHolder
{
    final Context context;

    UserProfileDetailsInfo.DataBean model;

    @BindView(R.id.tv_userLocation)
    TextView tvUserLocation;

    @BindView(R.id.tv_userFullName)
    TextView tvUserFullName;

    @BindView(R.id.iv_profilePicBig)
    PorterShapeImageView ivUserProfilePic;

    @BindView(R.id.iv_userPicBigBlurred)
    ImageView ivUserProfilePicBlurred;

    @BindView(R.id.iv_editStatus)
    ImageView ivEditStatus;

    @BindView(R.id.et_status)
    EditText tvStatus;

    @BindView(R.id.iv_profileAction)
    ImageView ivProfileAction;

    @BindView(R.id.iv_cancelQuieckReq)
    ImageView ivcancelQuieckReq;

    @BindView(R.id.iv_sendQuieckReq)
    ImageView ivsendQuieckReq;

    @BindView(R.id.pb_loadingStatus)
    ProgressBar pbLoadingStatus;

    @BindView(R.id.rb_userRating)
    RatingBar rbUserRating;

    @BindView(R.id.iv_changeProfilePic)
    ImageView ivChangeProfilePic;

    private VHBigProfileViewWithDetails(final Context context, View itemView)
    {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

        ivEditStatus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ShowEditStatusBox();
            }
        });

    }

    public static VHBigProfileViewWithDetails create(final Context context, final ViewGroup parent)
    {
        return new VHBigProfileViewWithDetails(context, LayoutInflater.from(context)
                .inflate(R.layout.big_profile_view_with_details, parent, false));
    }

    public static void bind(final VHBigProfileViewWithDetails vh, final UserProfileDetailsInfo.DataBean model)
    {
        if (vh.model == null)
        {
            vh.model = model;
        }

        if (model.getUserId().equals(App.getInstance().getUser().getId()))
        {
            vh.ivChangeProfilePic.setVisibility(View.VISIBLE);
        } else
        {
            vh.ivChangeProfilePic.setVisibility(View.GONE);
        }

        vh.rbUserRating.setRating((float) Math.round((model.getRating() / model.getRating() * 100) * 10) / 10);

        if (!model.getUserId().equals(App.getInstance().getUser().getId()))
        {
            if(model.getIsFriend() == 3){
                // Show Add Friend icon for Sending Request
                vh.ivProfileAction.setVisibility(View.GONE);
                vh.ivcancelQuieckReq.setVisibility(View.GONE);
                vh.ivsendQuieckReq.setVisibility(View.VISIBLE);
                vh.ivEditStatus.setVisibility(View.GONE);
            }
            if(model.getIsFriend() == 2){
                // Show Cnacel Request icon  for Cancel Request
                vh.ivEditStatus.setVisibility(View.GONE);
                vh.ivProfileAction.setVisibility(View.GONE);
                vh.ivcancelQuieckReq.setVisibility(View.VISIBLE);
                vh.ivsendQuieckReq.setVisibility(View.GONE);
            }
            if(model.getIsFriend() == 4){
                // Show Right Sign  icon  for this User is Friend
                vh.ivEditStatus.setVisibility(View.GONE);
                vh.ivProfileAction.setVisibility(View.VISIBLE);
                vh.ivcancelQuieckReq.setVisibility(View.GONE);
                vh.ivsendQuieckReq.setVisibility(View.GONE);
            }

        } else
        {
            // For Self Profile no need to show any icon
            vh.ivEditStatus.setVisibility(View.VISIBLE);
            vh.ivProfileAction.setVisibility(View.GONE);
            vh.ivcancelQuieckReq.setVisibility(View.GONE);
            vh.ivsendQuieckReq.setVisibility(View.GONE);
        }

        if(model.isIsCityShow()){

                vh.tvUserLocation.setText("Lives in " + model.getCity() + ", " + model.getCountryName());



        }
        else{
            if(model.getUserId().equalsIgnoreCase(App.getInstance().getUser().getId())){
                vh.tvUserLocation.setVisibility(View.GONE);
            }else{
                vh.tvUserLocation.setVisibility(View.INVISIBLE);
            }

        }
    //    vh.tvUserLocation.setText("Lives in " + model.getDistrictName() + ", " + model.getCountryName());

        vh.tvUserFullName.setText(model.getFullName());

        vh.tvStatus.setText(model.getStatus());

        try
        {
            if(App.getInstance().getUser().getId().equalsIgnoreCase(model.getUserId())){
            if(Constants.USERPROFILE_IMAGE.length() >0){
                Glide.with(vh.context).load(Constants.USERPROFILE_IMAGE).asBitmap()
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                       // .crossFade()
                        .skipMemoryCache(true)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                vh.ivUserProfilePic.setImageBitmap(resource);
                            }
                        });

                Glide.with(vh.context).load(Constants.USERPROFILE_IMAGE)
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .skipMemoryCache(true)
                        .bitmapTransform(new BlurTransformation(vh.context,5))
                        .into(vh.ivUserProfilePicBlurred);

            }else{
                Glide.with(vh.context).load(model.getUserImage()).asBitmap()
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        //.crossFade()
                        .skipMemoryCache(true)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                vh.ivUserProfilePic.setImageBitmap(resource);
                            }
                        });

                Glide.with(vh.context).load(model.getUserImage())
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .skipMemoryCache(true)
                        .bitmapTransform(new BlurTransformation(vh.context,5))
                        .into(vh.ivUserProfilePicBlurred);
            }
            }else{
                Glide.with(vh.context).load(model.getUserImage()).asBitmap()
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                       // .crossFade()
                        .skipMemoryCache(true)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                vh.ivUserProfilePic.setImageBitmap(resource);
                            }
                        });

                Glide.with(vh.context).load(model.getUserImage())
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .skipMemoryCache(true)
                        .bitmapTransform(new BlurTransformation(vh.context,5))
                        .into(vh.ivUserProfilePicBlurred);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    private void ShowEditStatusBox()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Change Status");

        // Set up the input
        final EditText input = new EditText(context);
        input.setText(tvStatus.getText());
        input.setHint("Enter status here");
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                updateStatus(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private void updateStatus(final String status)
    {
        pbLoadingStatus.setVisibility(View.VISIBLE);
        API.getInstance().updateUserStatus(App.getInstance().getUser().getId(),
                status, new RetrofitCallbacks<ResponseBody>(context)
                {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
                    {
                        super.onResponse(call, response);
                        if (response.isSuccessful())
                        {
                            tvStatus.setText(status);
                            model.setStatus(status);
                            pbLoadingStatus.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t)
                    {
                        super.onFailure(call, t);
                        pbLoadingStatus.setVisibility(View.GONE);
                    }
                });
    }

    @OnClick(R.id.iv_changeProfilePic)
    public void changeProfilePicture()
    {
        /*Intent intent = new Intent();
        intent.setType("image*//**//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((ActivityUserProfile) context)
                .startActivityForResult(Intent.createChooser(intent, "Select Picture")
                        , Constants.CHANGE_PROFILE_PIC);
        */

        openImagePicker();
    }
    private void openImagePicker() {

        ImagePicker.create((ActivityUserProfile)context)
                .folderMode(false) // folder mode (false by default)
                .folderTitle("Select Photos") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .multi() // multi mode (default mode)
                .limit(1) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .start(Constants.CHANGE_PROFILE_PIC); // start image picker activity with request code


    }

    @OnClick(R.id.iv_sendQuieckReq)
    public void sendQuickRequest(){
        API.getInstance().sendFriendRequest(App.getInstance().getUser().getId(),model.getUserId(),
                new RetrofitCallbacks<ResponseBody>(context) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            // btn_cancleFriend.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Friend Request Sent", Toast.LENGTH_SHORT).show();
                            ivsendQuieckReq.setVisibility(View.GONE);
                            ivcancelQuieckReq.setVisibility(View.VISIBLE);
                            //   btn_cancleFriend.setText("Cancel Request");
                            try {
                                System.out.println(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        super.onFailure(call, t);
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick(R.id.iv_cancelQuieckReq)
    public void cancelFriendRequest(){
    /*    ivcancelQuieckReq.setVisibility(View.GONE);
        ivsendQuieckReq.setVisibility(View.VISIBLE);
        ivProfileAction.setVisibility(View.GONE);*/

    }
/*TODO: Roshni GLIDE
*
*  Glide.with(vh.context).load(model.getUserImage())
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .skipMemoryCache(true)
                        .into(vh.ivUserProfilePic);

                Glide.with(vh.context).load(model.getUserImage())
                        .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.user_default)
                        .error(R.drawable.user_default)
                        .crossFade()
                        .skipMemoryCache(true)
                        .bitmapTransform(new BlurTransformation(vh.context,5))
                        .into(vh.ivUserProfilePicBlurred);*/
}
