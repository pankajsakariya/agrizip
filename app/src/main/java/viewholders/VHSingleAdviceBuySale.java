package viewholders;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.activity.ActivityNeedHelpFullView;
import com.app.agrizip.need_help.fragment.FragmentAdvise;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.squareup.picasso.Picasso;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AdvisePostList;
import apimodels.JobHideResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/17/2017 at 5:13 PM.
 */

public class VHSingleAdviceBuySale extends RecyclerView.ViewHolder {

    final Context context;

    AdvisePostList.DataBean model;

    @BindView(R.id.iv_adviceBuySaleImage)
    ImageView ivAdviceBuySaleImage;

    @BindView(R.id.iv_removeAdviceBuySale)
    ImageView iv_removeAdviceBuySale;

    @BindView(R.id.iv_adviceBuySaleUserPic)
    ImageView ivAdviceBuySaleUserPic;

    @BindView(R.id.tv_adviceBuySaleTime)
    TextView tvAdviceBuySaleTime;

    @BindView(R.id.tv_adviceBuySaleUserLocation)
    TextView tvAdviceBuySaleUserLocation;

    @BindView(R.id.tv_adviceBuySaleRequestTitle)
    TextView tvAdviceBuySaleRequestTitle;

    @BindView(R.id.tv_adviceBuySaleContent)
    TextView tvAdviceBuySaleContent;

    @BindView(R.id.iv_adviceBuySaleImage_only)
    ImageView iv_adviceBuySaleImage_only;

    @BindView(R.id.tv_adviceBuySaleContent_select)
    TextView tv_adviceBuySaleContent_select;

    @BindView(R.id.lly_description)
    LinearLayout lly_description;

    public VHSingleAdviceBuySale(final Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(context, ActivityNeedHelpFullView.class);
                i.putExtra(Constants.PARCELABLE_HELP_OBJECT, model);
                i.putExtra("POSITION", getPosition());
                i.putExtra("Views",model.getPostViews());
                context.startActivity(i);
            }
        });

        iv_removeAdviceBuySale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptDeleteAdvice();
            }
        });

    }

    public void promptDeleteAdvice() {
        new AlertDialog.Builder(context)
                .setTitle("Hide Post")
                .setMessage("Are you sure you want to Hide This Post?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Hide", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        // unfollowUser();
                        // int position = getPosition();

                        hidePost();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                }).show();
    }

    public void hidePost() {
        // model.getHelpId()
        RetrofitCallbacks<JobHideResponse> hideHelp = new RetrofitCallbacks<JobHideResponse>(context) {

            @Override
            public void onResponse(Call<JobHideResponse> call, Response<JobHideResponse> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    int code = response.code();
                    System.out.println("Hello ");
                    if (response.body().getResponseStatus() == 1) {

                        if (model.getHideHelpListener() != null) {
                            model.getHideHelpListener().onhideHelp(model, getPosition(), 1);
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<JobHideResponse> call, Throwable t) {
                super.onFailure(call, t);
                Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
            }
        };

        API.getInstance().hideHelp(App.getInstance().getUser().getId(), model.getHelpId(), hideHelp);

    }

    public static VHSingleAdviceBuySale create(final Context context, final ViewGroup parent) {
        return new VHSingleAdviceBuySale(context, LayoutInflater.from(context)
                .inflate(R.layout.single_advice_buy_sale_row_item, parent, false));
    }

    public static void bind(final VHSingleAdviceBuySale vh, AdvisePostList.DataBean model) {
        vh.model = model;

        Picasso.with(vh.context)
                .load(model.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                //.crossFade()
                .into(vh.ivAdviceBuySaleUserPic);

        if (model.getImages() != null) {
            if (model.getImages().size() > 0) {
                Glide.with(vh.context)
                        .load(model.getImages().get(0).getImagestr()).asBitmap()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        // .crossFade()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                vh.ivAdviceBuySaleImage.setImageBitmap(resource);
                            }
                        });
                vh.lly_description.setVisibility(View.VISIBLE);
            }
        } else {
            vh.tv_adviceBuySaleContent_select.setText(model.getDescription());

            vh.tv_adviceBuySaleContent_select.setVisibility(View.VISIBLE);
            vh.lly_description.setVisibility(View.GONE);
        }
        if (model.getImages() != null) {
            if (model.getImages().size() > 0) {
                Picasso.with(vh.context)
                        .load(model.getImages().get(0).getImagestr())
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)

                        .into(vh.iv_adviceBuySaleImage_only);
            }

        }
        vh.tvAdviceBuySaleUserLocation.setText(model.getPlace());

        vh.tvAdviceBuySaleTime.setText(DateUtils.getRelativeDateTimeString(vh.context,
                Utils.convertDateToMills(model.getCreateDatetime()),
                DateUtils.SECOND_IN_MILLIS,
                DateUtils.WEEK_IN_MILLIS,
                0));

        if (model.getDescription() != null) {
            if (!model.getDescription().isEmpty()) {
                vh.tvAdviceBuySaleContent.setText(model.getDescription());
            } else {
                vh.iv_adviceBuySaleImage_only.setVisibility(View.VISIBLE);
                vh.ivAdviceBuySaleImage.setVisibility(View.GONE);
                vh.tvAdviceBuySaleContent.setVisibility(View.GONE);
            }
        }
        if (model.getPlace() != null) {
            vh.tvAdviceBuySaleUserLocation.setVisibility(View.VISIBLE);
        } else {
            vh.tvAdviceBuySaleUserLocation.setVisibility(View.INVISIBLE);

        }
        if (model.getAdviseBuySaleType() == AdvisePostList.AdviseBuySaleType.SALE) {
            vh.tvAdviceBuySaleRequestTitle.setText(model.getUserName() + " -Want to Sale " + model.getHelpTitle());
        } else if (model.getAdviseBuySaleType() == AdvisePostList.AdviseBuySaleType.BUY) {
            vh.tvAdviceBuySaleRequestTitle.setText(model.getUserName() + " -Want to Buy " + model.getHelpTitle());
        } else if (model.getAdviseBuySaleType() == AdvisePostList.AdviseBuySaleType.ADVISE) {
            vh.tvAdviceBuySaleRequestTitle.setText(model.getUserName() + " -Want Advise for " + model.getHelpTitle());
        } else {
            vh.tvAdviceBuySaleRequestTitle.setText("Unknown");
        }

    }


}
