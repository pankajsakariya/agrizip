package viewholders;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.UiThread;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import api.API;
import api.RetrofitCallbacks;
import apimodels.LikeListResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 2/11/2017 at 6:13 PM.
 */

public class VHSIngleLikeListItem extends RecyclerView.ViewHolder {

    final Context context;

    @BindView(R.id.tv_likeListUserName)
    TextView tvLikeListUserName;

    @BindView(R.id.tv_likeListMutualFriends)
    TextView tvLikeListMutualFriends;

    @BindView(R.id.iv_likeListUserImage)
    ImageView ivLikeListUserImage;
    @BindView(R.id.iv_addFriendLikeList)
    ImageButton ivAddFriendLikeList;

    private LikeListResponse.DataBean model;


    public VHSIngleLikeListItem(final Context context, View itemView) {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);

        ivLikeListUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (App.getInstance().getUser().getId().equals(model.getUserID())) {
                    Intent i = new Intent(context, ActivityUserProfile.class);
                    i.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
                    i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
                    context.startActivity(i);
                } else {
                    Intent i = new Intent(context, ActivityUserProfile.class);
                    i.putExtra(Constants.USER_ID, model.getUserID());
                    i.putExtra(Constants.IS_SEEING_SELF_PROFILE, false);
                    context.startActivity(i);
                }
            }
        });

    }

    public static VHSIngleLikeListItem create(final Context context, final ViewGroup parent) {
        return new VHSIngleLikeListItem(context, LayoutInflater.from(context)
                .inflate(R.layout.single_like_list_item, parent, false));
    }

    public static void bind(final VHSIngleLikeListItem vh,
                            final LikeListResponse.DataBean model) {

        vh.model = model;
        vh.tvLikeListUserName.setText(model.getFullName());
        vh.ivLikeListUserImage.setVisibility(View.VISIBLE);
        if (App.getInstance().getUser().getId().equalsIgnoreCase(model.getUserID())) {
            vh.tvLikeListMutualFriends.setText("You");
        } else if (model.getMutualFriendCount() > 0) {
            vh.tvLikeListMutualFriends.setText(model.getMutualFriendCount() + " Mutual Friend");
        } else {
            vh.tvLikeListMutualFriends.setText("No Mutual Friend");
        }

        Glide.with(vh.context).load(model.getUserImage()).asBitmap()
                .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)

                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        vh.ivLikeListUserImage.setImageBitmap(resource);
                    }
                });
        if (App.getInstance().getUser().getId().equals(model.getUserID()) || model.getIsFriend() == 4) {
            vh.ivAddFriendLikeList.setVisibility(View.GONE);

        } else {
            vh.ivAddFriendLikeList.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.iv_addFriendLikeList)
    public void onClick() {
        promptUserForAddingFriend();
    }

    private void promptUserForAddingFriend() {
        new AlertDialog.Builder(context)
                .setTitle("Send Friend Request")
                .setMessage("Send Friend Request to " + model.getFullName() + " ?")
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendFriendRequest();
                    }
                }).show();
    }

    private void sendFriendRequest() {
        API.getInstance().sendFriendRequest(App.getInstance().getUser().getId(),
                model.getUserID(), new RetrofitCallbacks<ResponseBody>(context) {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {
                            toggleAddFriendButton();
                        }
                    }
                });
    }

    @UiThread
    void toggleAddFriendButton() {
        // ivLikeListUserImage.setImageResource(R.drawable.btn_reject_request_normal);
        ivAddFriendLikeList.setImageResource(R.drawable.btn_reject_request_normal);
    }

}
