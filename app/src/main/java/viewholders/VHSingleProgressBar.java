package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.agrizip.R;

/**
 * Created by rutvik on 2/23/2017 at 1:12 PM.
 */

public class VHSingleProgressBar extends RecyclerView.ViewHolder
{

    final Context context;

    public VHSingleProgressBar(Context context, View itemView)
    {
        super(itemView);
        this.context = context;
    }

    public static VHSingleProgressBar create(final Context context, final ViewGroup parent)
    {
        return new VHSingleProgressBar(context, LayoutInflater.from(context)
                .inflate(R.layout.single_progress_bar, parent, false));

    }

    public static void bind(final VHSingleProgressBar vh)
    {

    }


}
