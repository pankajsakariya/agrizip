package viewholders;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.followers.activity.ActivityFollowers;
import com.app.agrizip.post.dialog.DialogFollower;
import com.app.agrizip.post.dialog.DialogHideDeletePost;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.squareup.picasso.Picasso;

import api.API;
import api.RetrofitCallbacks;
import apimodels.FollowersList;
import apimodels.SinglePost;
import apimodels.UnfollowUser;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/10/2017 at 6:34 PM.
 */

public class VHSingleFollower extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener, View.OnClickListener {

    final Context context;
    SinglePost post;
    @BindView(R.id.tv_singleFollowerName)
    TextView tvSingleFollowerName;

    @BindView(R.id.iv_singleFollowerImage)
    ImageView ivSingleFollowerImage;

    @BindView(R.id.btn_singleFollowerExtraOptions)
    Button btnSingleFollowerExtraOptions;

    FollowersList.DataBean singleFollower;

    PopupMenu popup;
    AlertDialog.Builder builder;

    private VHSingleFollower(final Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        popup = new PopupMenu(context, btnSingleFollowerExtraOptions);

       /* popup.getMenuInflater().inflate(R.menu.followers_extra_options, popup.getMenu());

        popup.setOnMenuItemClickListener(this);*/

        btnSingleFollowerExtraOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                popup.show();
                //promptUserForUnfollow();
                if(App.getInstance().getUser().getId().equalsIgnoreCase(ActivityFollowers.userId)){


                    final DialogFollower UnFollowFriend =
                            new DialogFollower(context, singleFollower.getFollowupId(), DialogFollower.Purpose.HIDE_POST,singleFollower,getPosition());

                    UnFollowFriend.show();
                }else{

                }

            }
        });

        this.context = context;
    }

    public static VHSingleFollower create(final Context context, final ViewGroup parent) {
        return new VHSingleFollower(context, LayoutInflater.from(context)
                .inflate(R.layout.single_follower_row_item, parent, false));
    }

    public static void bind(final VHSingleFollower vh, FollowersList.DataBean singleFollower) {
        vh.singleFollower = singleFollower;
        vh.tvSingleFollowerName.setText(singleFollower.getFullName());

        Glide.with(vh.context)
                .load(singleFollower.getUserImage()).asBitmap()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        vh.ivSingleFollowerImage.setImageBitmap(resource);
                    }
                });

    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.action_unfollowFollower) {
          /*  final DialogFollower hideDeletePost =
                    new DialogFollower(context, singleFollower.getFollowupId(), DialogFollower.Purpose.HIDE_POST);

            hideDeletePost.show();*/
            UnfollowUser unfollowUser = new UnfollowUser();
            unfollowUser.setFollowUserId(singleFollower.getFollowUserId());
            unfollowUser.setIsFollow(false);
            unfollowUser.setFollowupId(singleFollower.getFollowupId());
            unfollowUser.setUserId(App.getInstance().getUser().getId());

            API.getInstance().unfollowUser(unfollowUser,
                    new RetrofitCallbacks<ResponseBody>(context) {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            super.onResponse(call, response);
                            if (response.isSuccessful()) {
                                Toast.makeText(context, "Unfollowed Successfully", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            super.onFailure(call, t);
                        }
                    });
        }
        return false;
    }

    private void promptUserForUnfollow() {

        new AlertDialog.Builder(context)
                .setTitle("Unfollow")
                .setMessage("Are You Sure To Unfollow " + singleFollower.getFullName() +"?")
                .setPositiveButton("Unfollow", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        unfollowUser();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        popup.dismiss();
                    }
                }).show();
    }

    private void unfollowUser() {
        UnfollowUser unfollowUser = new UnfollowUser();
        unfollowUser.setFollowUserId(singleFollower.getFollowUserId());
        unfollowUser.setIsFollow(false);
        unfollowUser.setFollowupId(singleFollower.getFollowupId());
        unfollowUser.setUserId(App.getInstance().getUser().getId());

        API.getInstance().unfollowUser(unfollowUser,
                new RetrofitCallbacks<ResponseBody>(context) {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        super.onResponse(call, response);
                        if (response.isSuccessful()) {

                            Toast.makeText(context, "Unfollowed Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        super.onFailure(call, t);
                    }
                });
    }

    @Override
    public void onClick(View v) {
/*
        if (v.getId() == R.id.btn_unfollow) {
            unfollowUser();
        } else if (v.getId() == R.id.btn_cancle) {
            builder.dismiss();
        }*/
    }
}
