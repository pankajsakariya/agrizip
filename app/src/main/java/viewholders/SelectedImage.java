package viewholders;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.LinkedHashMap;
import java.util.Map;

import extras.Constants;

/**
 * Created by rutvik on 11/28/2016 at 11:19 AM.
 */

public class SelectedImage
{
    private static final String TAG = App.APP_TAG + SelectedImage.class.getSimpleName();

    final SelectedImageOperations selectedImageOperations;
    final Context context;
    Map<String, SingleImage> singleImageList = new LinkedHashMap<>();
    LinearLayout ll;

    public SelectedImage(Context context, SelectedImageOperations selectedImageOperations)
    {
        this.context = context;
        ll = new LinearLayout(context);
        this.selectedImageOperations = selectedImageOperations;
    }

    public void addSingleImage(String imagePath, Bitmap bitmap)
    {
        singleImageList.put(imagePath, new SingleImage(bitmap, imagePath));
    }

    public LinearLayout getView()
    {
        return ll;
    }

    public void removeAll()
    {
        ll.removeAllViews();
        selectedImageOperations.onRemoveAllImages();
    }

    private void removeImage(final String imagePath)
    {

        SingleImage si = singleImageList.get(imagePath);
        Log.i(TAG, "REMOVING VIEW AT: " + imagePath);
        if (ll.getChildCount() == 1)
        {
            selectedImageOperations.onRemoveAllImages();
            ll.removeAllViews();
        } else
        {
            selectedImageOperations.removeSelectedImage(imagePath);
            ll.removeView(si.view);
        }

    }

    public interface SelectedImageOperations
    {
        void removeSelectedImage(String imagePath);

        void onRemoveAllImages();
    }

    class SingleImage
    {
        final String imagePath;
        ImageView ivSelectedImage;
        ImageView ivRemove;
        View view;

        public SingleImage(Bitmap bitmap, final String imagePath)
        {
            this.imagePath = imagePath;
            view = LayoutInflater.from(context).inflate(R.layout.single_selected_image, null);
            ivSelectedImage = (ImageView) view.findViewById(R.id.iv_selectedImage);

            Glide.with(context).load(imagePath).asBitmap()
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    // .crossFade()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivSelectedImage.setImageBitmap(resource);
                        }
                    });

           // ivSelectedImage.setImageBitmap(bitmap);
            ivRemove = (ImageView) view.findViewById(R.id.iv_removeSelectedImage);
            ivRemove.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    removeImage(imagePath);
                }
            });

            ll.addView(view);


        }

    }

}
