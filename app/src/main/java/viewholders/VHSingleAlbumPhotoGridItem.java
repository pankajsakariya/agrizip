package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.agrizip.R;
import com.bumptech.glide.Glide;

import apimodels.GetAlbums;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 1/6/2017 at 12:58 PM.
 */

public class VHSingleAlbumPhotoGridItem extends RecyclerView.ViewHolder
{

    final Context context;

    @BindView(R.id.iv_singleAlbumPhotoGridItem)
    ImageView ivSingleAlbumPhotoGridItem;

    GetAlbums.DataBean.PhotosBean photo;

    private VHSingleAlbumPhotoGridItem(Context context, View itemView)
    {
        super(itemView);

        ButterKnife.bind(this, itemView);

        this.context = context;

    }

    public static VHSingleAlbumPhotoGridItem create(Context context, ViewGroup parent)
    {
        return new VHSingleAlbumPhotoGridItem(context, LayoutInflater.from(context)
                .inflate(R.layout.single_album_photo_grid_item, parent, false));
    }

    public static void bind(VHSingleAlbumPhotoGridItem vh, GetAlbums.DataBean.PhotosBean photo)
    {
        vh.photo = photo;
        Glide.with(vh.context)
                .load(photo.getImagestr())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .into(vh.ivSingleAlbumPhotoGridItem);
    }


}
