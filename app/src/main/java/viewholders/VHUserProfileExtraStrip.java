package viewholders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.chat.activity.ActivityChatDialog;
import com.app.agrizip.followers.activity.ActivityFollowers;
import com.app.agrizip.friends.friend_list.ActivityFriendList;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.home.fragment.FragmentChats;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import apimodels.UserProfileDetailsInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;

/**
 * Created by rutvik on 11/22/2016 at 12:09 PM.
 */

public class VHUserProfileExtraStrip extends RecyclerView.ViewHolder
{

    final Context context;
    @BindView(R.id.ll_chatMessage)
    LinearLayout tvChat;
    @BindView(R.id.ll_followers)
    LinearLayout llFollow;
    @BindView(R.id.ll_following)
    LinearLayout llFollowing;
    @BindView(R.id.ll_friends)
    LinearLayout llFriends;
    @BindView(R.id.tv_followers)
    TextView tvFollow;
    @BindView(R.id.tv_friends)
    TextView tvFriends;
    @BindView(R.id.tv_following)
    TextView tvFollowing;

    @BindView(R.id.tv_toggleMiniBankOrChat1)
    TextView tv_toggleMiniBankOrChat1;



    @BindView(R.id.tv_toggleMiniBankOrChat)
    TextView tvToggleMiniBankOrChat;


    UserProfileDetailsInfo.DataBean userInfo;
    ProgressDialog pd;

    public VHUserProfileExtraStrip(final Context context, View itemView)
    {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

        llFriends.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(context, ActivityFriendList.class);
                i.putExtra(Constants.USER_ID, userInfo.getUserId());

                i.putExtra("POSITION",getPosition());
          //      i.putExtra("FRIEND_COUNT",userInfo.getf)
                context.startActivity(i);
            }
        });

        llFollowing.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(context, ActivityFollowers.class);
                i.putExtra(Constants.IS_SEEING_FOLLOWERS, true);
                i.putExtra(Constants.USER_ID, userInfo.getUserId());
                i.putExtra("FOLLOW_COUNT",Integer.parseInt(userInfo.getFollowingCount()));
                i.putExtra("POSITION",getPosition());
                context.startActivity(i);

            }
        });

        llFollow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(context, ActivityFollowers.class);
                i.putExtra(Constants.IS_SEEING_FOLLOWERS, false);
                i.putExtra("POSITION",getPosition());
                i.putExtra("FOLLOW_COUNT",Integer.parseInt(userInfo.getFollowCount()));
                i.putExtra(Constants.USER_ID, userInfo.getUserId());
                context.startActivity(i);
            }
        });


        tvChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userInfo.getUserId().equals(App.getInstance().getUser().getId()))
                {
                    //   vh.tvToggleMiniBankOrChat.setText(R.string.mini_bank);
                } else
                {
                    if(userInfo.getIsFriend() == 4){
                        pd = new ProgressDialog(context);
                        pd.setMessage("Wait...");
                        pd.show();

                        getQuickBloxId(userInfo.getMobileNumber());
                       // Intent intent = ActivityChatDialog.startForNotificationResult(vh.context,25380977);
                       // vh.context.startActivity(intent);

                    }else{

                        Toast.makeText(context,"Send Friend Requiest to this Person.",Toast.LENGTH_LONG).show();
                    }





                }
            }
        });


    }

    public static VHUserProfileExtraStrip create(final Context context, final ViewGroup parent)
    {
        return new VHUserProfileExtraStrip(context, LayoutInflater.from(context)
                .inflate(R.layout.user_profile_extra_strip, parent, false));
    }



    public static void bind(final VHUserProfileExtraStrip vh, final UserProfileDetailsInfo.DataBean userInfo)
    {
        vh.userInfo = userInfo;
        vh.tvFriends.setText(userInfo.getTotalFriendsCounts());
        vh.tvFollow.setText(userInfo.getFollowCount());
        vh.tvFollowing.setText(userInfo.getFollowingCount());

        if (userInfo.getUserId().equals(App.getInstance().getUser().getId()))
        {
           vh.tv_toggleMiniBankOrChat1.setText("Mini");
            vh.tvToggleMiniBankOrChat.setText("Bank");
        } else
        {
            vh.tv_toggleMiniBankOrChat1.setText("Chat");
            vh.tvToggleMiniBankOrChat.setText("Message");
        }
        /*vh.tvChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

    }

    public   void goToChat(int qbUserId){
        pd.dismiss();
        Intent intent = ActivityChatDialog.startForNotificationResult(context,qbUserId);
        intent.putExtra("UserName",userInfo.getFullName());
        context.startActivity(intent);
    }

    public  void getQuickBloxId(String mobile){
        QBUsers.getUserByLogin(mobile).performAsync(new QBEntityCallback<QBUser>()
        {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle)
            {
               // Log.i(TAG, "QB USER FOUND SUCCESSFULLY");

                goToChat(qbUser.getId());
                /*    if (true)
                {
                   // vh.qbUser = qbUser;
                    final QBRoster roster = App.getInstance().getRoster();
                    if (roster != null)
                    {
                        if (roster.contains(qbUser.getId()))
                        {
                            try
                            {
                                roster.subscribe(qbUser.getId());
                            } catch (SmackException.NotConnectedException e)
                            {
                                e.printStackTrace();
                            }
                        } else
                        {
                            try
                            {
                                roster.createEntry(qbUser.getId(), null);
                            } catch (XMPPException e)
                            {
                                e.printStackTrace();
                            } catch (SmackException.NotLoggedInException e)
                            {
                                e.printStackTrace();
                            } catch (SmackException.NotConnectedException e)
                            {
                                e.printStackTrace();
                            } catch (SmackException.NoResponseException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (roster != null)
                    {
                        QBPresence presence = App.getInstance().getRoster().getPresence(qbUser.getId());

                        if (presence == null)
                        {
                            //Log.i(TAG, "PRESENCE: NO USER IN ROSTER");
                        } else
                        {
                            if (presence.getType() == QBPresence.Type.online)
                            {
                              //  vh.tvChatFriendPresence.setTextColor(vh.context.getResources().getColor(android.R.color.holo_green_light));
                                //vh.tvChatFriendPresence.setText("Online");
                            } else
                            {
                               // vh.tvChatFriendPresence.setTextColor(vh.context.getResources().getColor(android.R.color.darker_gray));
                                //vh.tvChatFriendPresence.setText("Offline");
                            }
                        }
                    }

            */    }

            @Override
            public void onError(QBResponseException e) {

            }



        });


    }

}
