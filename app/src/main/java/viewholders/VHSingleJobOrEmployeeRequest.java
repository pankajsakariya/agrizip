package viewholders;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.need_help.activity.ActivityJobOrEmployeeFullView;
import com.app.agrizip.need_help.fragment.FragmentRequireJob;
import com.bumptech.glide.Glide;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AdvisePostList;
import apimodels.JobHideResponse;
import apimodels.RequireEmployeeList;
import apimodels.RequireJobList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rutvik on 1/16/2017 at 6:12 PM.
 */

public class VHSingleJobOrEmployeeRequest extends RecyclerView.ViewHolder
{

    final Context context;

    final int viewType;

    RequireJobList.DataBean singleJobRequest;

    RequireEmployeeList.DataBean singleEmployeeRequest;

    @BindView(R.id.iv_jobUserPic)
    ImageView ivJobUserPic;

    @BindView(R.id.tv_jobRequestTitle)
    TextView tvJobRequestTitle;

    @BindView(R.id.tv_jobUserLocation)
    TextView tvJobUserLocation;

    @BindView(R.id.tv_jobTime)
    TextView tvJobTime;

    @BindView(R.id.iv_removeJob)
    ImageView ivremoveJob;

    @BindView(R.id.tv_jobTitleOrName)
    TextView tvJobTitleOrName;

    @BindView(R.id.tv_jobTitleOrNameValue)
    TextView tvJobTitleOrNameValue;

    @BindView(R.id.tv_jobNameOrCompanyName)
    TextView tvJobNameOrCompanyName;

    @BindView(R.id.tv_jobNameOrCompanyNameValue)
    TextView tvJobNameOrCompanyNameValue;

    @BindView(R.id.tv_jobEmailOrTitle)
    TextView tvJobEmailOrTitle;

    @BindView(R.id.tv_jobEmailOrTitleValue)
    TextView tvJobEmailOrTitleValue;

    @BindView(R.id.tv_jobMobileOrVacancy)
    TextView tvJobMobileOrVacancy;

    @BindView(R.id.tv_jobMobileOrVacancyValue)
    TextView tvJobMobileOrVacancyValue;
    int position;
    boolean isJobPost;
    public VHSingleJobOrEmployeeRequest(final Context context, View itemView, int viewType)
    {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
        this.viewType = viewType;

        itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                final Intent fullViewActivity = new Intent(context, ActivityJobOrEmployeeFullView.class);

                if (singleJobRequest != null)
                {
                    fullViewActivity.putExtra(Constants.NEED_JOB, true);
                    fullViewActivity.putExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT,
                            singleJobRequest);

                } else if (singleEmployeeRequest != null)
                {
                    fullViewActivity.putExtra(Constants.NEED_JOB, false);
                    fullViewActivity.putExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT,
                            singleEmployeeRequest);
                }
                fullViewActivity.putExtra("POSITION",getPosition());
                context.startActivity(fullViewActivity);
            }
        });

        ivremoveJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = getPosition();
                promptDeleteAdvice();
            }
        });

    }
    public  void promptDeleteAdvice(){
        new AlertDialog.Builder(context)
                .setTitle("Hide Post")
                .setMessage("Are you sure you want to Hide This Post?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Hide", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        // unfollowUser();
                        // int position = getPosition();
                        if(singleJobRequest != null){
                            isJobPost = true;
                        }else{
                            isJobPost = false;
                        }


                        hideJobEmployeePost();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                }).show();
    }

    public void hideJobEmployeePost(){
        // model.getHelpId()
        final String id = singleJobRequest != null ? singleJobRequest.getJobId() :
                singleEmployeeRequest.getJobId();


        RetrofitCallbacks<JobHideResponse> hideJobEmployeePost = new RetrofitCallbacks<JobHideResponse>(context) {

            @Override
            public void onResponse(Call<JobHideResponse> call, Response<JobHideResponse> response) {
                super.onResponse(call, response);
                if (response.isSuccessful()) {
                    if(isJobPost) {
                        if (singleJobRequest.getJobEmployeeHidePost() != null) {
                            singleJobRequest.getJobEmployeeHidePost().OnHideJobEmployeePost(getPosition());
                        }
                    }else{
                        if (singleEmployeeRequest.getHideEmployeeListener() != null) {
                            singleEmployeeRequest.getHideEmployeeListener().OnHideEmployee(getPosition());
                        }
                    }
                   // new FragmentRequireJob().removejob(position);
                }
            }

            @Override
            public void onFailure(Call<JobHideResponse> call, Throwable t) {
                super.onFailure(call, t);
                Toast.makeText(context,t.toString(),Toast.LENGTH_LONG).show();
            }
        };

        API.getInstance().hideHelp(App.getInstance().getUser().getId(),id, hideJobEmployeePost);

    }


    public static VHSingleJobOrEmployeeRequest create(final Context context, final ViewGroup parent,
                                                      final int viewType)
    {
        return new VHSingleJobOrEmployeeRequest(context, LayoutInflater.from(context)
                .inflate(R.layout.single_job_row_item, parent, false), viewType);
    }

    public static void bind(final VHSingleJobOrEmployeeRequest vh,
                            RequireJobList.DataBean singleJobRequest)
    {
        vh.singleJobRequest = singleJobRequest;

        Glide.with(vh.context)
                .load(singleJobRequest.getUserImage())
                .into(vh.ivJobUserPic);

        vh.tvJobRequestTitle.setText(singleJobRequest.getUserName() + " -Require " +
                singleJobRequest.getJobDuration() +
                " job in " + singleJobRequest.getJobTitle());

        vh.tvJobNameOrCompanyName.setText("Name: ");
        vh.tvJobTitleOrName.setText("Job Title: ");
        vh.tvJobEmailOrTitle.setText("Email ID: ");
        vh.tvJobMobileOrVacancy.setText("Mobile No: ");

        vh.tvJobNameOrCompanyNameValue.setText(singleJobRequest.getUserName());
        vh.tvJobTitleOrNameValue.setText(singleJobRequest.getJobTitle());
        vh.tvJobEmailOrTitleValue.setText(singleJobRequest.getEmail());
        vh.tvJobMobileOrVacancyValue.setText(singleJobRequest.getMobile());
        vh.tvJobUserLocation.setText(singleJobRequest.getCity() + ", " +
                singleJobRequest.getState());
        vh.tvJobTime.setText(
                DateUtils.getRelativeDateTimeString(vh.context,
                        Utils.convertDateToMills(singleJobRequest.getCreateDate()),
                        DateUtils.SECOND_IN_MILLIS,
                        DateUtils.WEEK_IN_MILLIS,
                        0));

    }

    public static void bind(final VHSingleJobOrEmployeeRequest vh,
                            RequireEmployeeList.DataBean singleEmployeeRequest)
    {
        vh.singleEmployeeRequest = singleEmployeeRequest;

        Glide.with(vh.context)
                .load(singleEmployeeRequest.getUserImage())
                .into(vh.ivJobUserPic);

        vh.tvJobRequestTitle.setText(singleEmployeeRequest.getUserName() + " -Require " +
                singleEmployeeRequest.getJobDuration() +
                " Employee");

        vh.tvJobNameOrCompanyName.setText("Company Name: ");
        vh.tvJobTitleOrName.setText("Name: ");
        vh.tvJobEmailOrTitle.setText("Job Title: ");
        vh.tvJobMobileOrVacancy.setText("Vacancy: ");

        vh.tvJobNameOrCompanyNameValue.setText(singleEmployeeRequest.getCompanyName());
        vh.tvJobTitleOrNameValue.setText(singleEmployeeRequest.getUserName());
        vh.tvJobEmailOrTitleValue.setText(singleEmployeeRequest.getJobTitle());
        vh.tvJobMobileOrVacancyValue.setText(singleEmployeeRequest.getVacancy());
        vh.tvJobUserLocation.setText(singleEmployeeRequest.getCity() + ", " +
                singleEmployeeRequest.getState());
        vh.tvJobTime.setText(
                DateUtils.getRelativeDateTimeString(vh.context,
                        Utils.convertDateToMills(singleEmployeeRequest.getCreateDate()),
                        DateUtils.SECOND_IN_MILLIS,
                        DateUtils.WEEK_IN_MILLIS,
                        0));

    }


}
