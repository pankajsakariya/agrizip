package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import apimodels.Comment;
import views.SingleCommentRow;

/**
 * Created by rutvik on 12/16/2016 at 12:50 PM.
 */

public class VHSingleComment extends RecyclerView.ViewHolder
{

    final Context context;

    final SingleCommentRow singleCommentRow;

    public VHSingleComment(Context context, SingleCommentRow itemView)
    {
        super(itemView);
        this.singleCommentRow = itemView;
        this.context = context;
    }

    public static VHSingleComment create(final Context context, final ViewGroup parent)
    {
        return new VHSingleComment(context, new SingleCommentRow(context));
    }

    public static void bind(final VHSingleComment vh, Comment singleComment)
    {
        vh.singleCommentRow.setModel(singleComment);
    }


}
