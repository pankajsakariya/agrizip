package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.agrizip.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 1/20/2017 at 8:18 PM.
 */

public class VHSingleJobDetails extends RecyclerView.ViewHolder
{

    final Context context;

    String key, value;

    @BindView(R.id.tv_singleJobDetailKey)
    TextView tvSingleJobDetailKey;

    @BindView(R.id.tv_singleJobDetailValue)
    TextView tvSingleJobDetailValue;

    public VHSingleJobDetails(Context context, View itemView)
    {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);
    }

    public static VHSingleJobDetails create(final Context context, final ViewGroup parent)
    {
        return new VHSingleJobDetails(context, LayoutInflater.from(context)
                .inflate(R.layout.single_job_details_row_item, parent, false));
    }

    public static void bind(final VHSingleJobDetails vh, String key, String value)
    {
        vh.key = key;
        vh.value = value;
        vh.tvSingleJobDetailKey.setText(vh.key);
        vh.tvSingleJobDetailValue.setText(vh.value);
    }


}
