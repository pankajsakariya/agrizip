package viewholders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.album_create.RenameAlbum;
import com.app.agrizip.album.album_details.ActivityViewAlbumPhotos;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import apimodels.GetAlbums;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;
import extras.Utils;

/**
 * Created by rutvik on 1/5/2017 at 11:29 AM.
 */

public class VHSingleAlbum extends RecyclerView.ViewHolder {

    final Context context;
    @BindView(R.id.tv_albumName)
    TextView tvAlbumName;
    @BindView(R.id.tv_albumDate)
    TextView tvAlbumDate;
    @BindView(R.id.iv_editAlbumName)
    ImageView ivEditAlbumName;
    @BindView(R.id.iv_oneOne)
    ImageView ivOneOne;
    @BindView(R.id.iv_oneTwo)
    ImageView ivOneTwo;
    @BindView(R.id.iv_twoOne)
    ImageView ivTwoOne;
    @BindView(R.id.iv_threeOne)
    ImageView ivThreeOne;
    @BindView(R.id.iv_threeTwo)
    ImageView ivThreeTwo;

    @BindView(R.id.view_alfa)
    View view_alfa;


    @BindView(R.id.ll_album)
    LinearLayout llAlbum;

    @BindView(R.id.rl_album)
    RelativeLayout rl_album;
    @BindView(R.id.tv_moreThanFourImageCount)
    TextView tv_moreThanFourImageCount;

    @BindView(R.id.rl_moreThanFourImageView)
    RelativeLayout rl_moreThanFourImageView;
    GetAlbums.DataBean singleAlbum;
    public static String dateFormate = "";
    public VHSingleAlbum(final Context context, View itemView) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

        rl_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (App.getInstance().getUser().getId().equalsIgnoreCase(singleAlbum.getUserId())) {


                    if (singleAlbum.getPhotos().size() > 0) {

                        final ArrayList<GetAlbums.DataBean.PhotosBean> photoList =
                                new ArrayList<GetAlbums.DataBean.PhotosBean>();

                        for (GetAlbums.DataBean.PhotosBean photo : singleAlbum.getPhotos()) {
                            photoList.add(photo);
                        }

                        Intent openFullViewImage = new Intent(context, ActivityViewAlbumPhotos.class);
                        openFullViewImage.putExtra(Constants.ALBUM_NAME, singleAlbum.getAlbumName());
                        openFullViewImage.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST,
                                photoList);

                       context.startActivity(openFullViewImage);
                    } else {
                        Toast.makeText(context, "No images in album", Toast.LENGTH_SHORT).show();
                    }
                } else {

                }
            }
        });

        ivEditAlbumName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAlbumName();
            }
        });

    }

    public static VHSingleAlbum create(final Context context, final ViewGroup parent) {
        return new VHSingleAlbum(context, LayoutInflater.from(context)
                .inflate(R.layout.single_album_row_item, parent, false));
    }

    public static void bind(final VHSingleAlbum vh, GetAlbums.DataBean singleAlbum) {
        vh.singleAlbum = singleAlbum;
        String[] date = new String[3];

        date[0] = "N/A";
        date[1] = "N/A";
        date[2] = "N/A";
   /*     if (getDate.contains("-")) {
            date = getDate.split("-");
        } else if (getDate.contains("/")) {
            date = getDate.split("/");
        }

        if (date[2].contains("T")) {
            date[2] = date[2].substring(0, date[2].split("T").length);
        }
*/
       // String dateFormate = Utils.changeDateFormat(singleAlbum.getCreateDate()," yyyy-MM-ddTHH:mm:ss.SSS","dd MM yyyy");
         dateFormate = singleAlbum.getCreateDate().split("T")[0];
         dateFormate = Utils.changeDateFormat(dateFormate,"yyyy-MM-dd","d MMM yy");
        System.out.println("Hello");
        vh.tvAlbumName.setText(singleAlbum.getAlbumName() + "("+ dateFormate+")" );
        if (App.getInstance().getUser().getId().equalsIgnoreCase(singleAlbum.getUserId())) {

        } else {
            vh.ivEditAlbumName.setVisibility(View.GONE);
        }

        if (singleAlbum.getPhotos().size() > 0) {
            vh.ivOneOne.setVisibility(View.VISIBLE);
            vh.ivOneTwo.setVisibility(View.GONE);
            vh.ivTwoOne.setVisibility(View.GONE);
            vh.ivThreeOne.setVisibility(View.GONE);
            vh.ivThreeTwo.setVisibility(View.GONE);
            vh.rl_moreThanFourImageView.setVisibility(View.GONE);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(0).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneOne);
        }

        if (singleAlbum.getPhotos().size() > 1) {
            vh.ivOneOne.setVisibility(View.VISIBLE);
            vh.ivOneTwo.setVisibility(View.VISIBLE);
            vh.ivTwoOne.setVisibility(View.GONE);
            vh.ivThreeOne.setVisibility(View.GONE);
            vh.ivThreeTwo.setVisibility(View.GONE);
            vh.rl_moreThanFourImageView.setVisibility(View.GONE);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(0).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneOne);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(1).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneTwo);
        }

        if (singleAlbum.getPhotos().size() > 2) {
            vh.ivOneOne.setVisibility(View.VISIBLE);
            vh.ivOneTwo.setVisibility(View.VISIBLE);
            vh.ivTwoOne.setVisibility(View.VISIBLE);
            vh.ivThreeOne.setVisibility(View.GONE);
            vh.ivThreeTwo.setVisibility(View.GONE);
            vh.rl_moreThanFourImageView.setVisibility(View.GONE);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(0).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneOne);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(1).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneTwo);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(2).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivTwoOne);
        }

        if (singleAlbum.getPhotos().size() > 3) {
            vh.ivOneOne.setVisibility(View.VISIBLE);
            vh.ivOneTwo.setVisibility(View.VISIBLE);
            vh.ivTwoOne.setVisibility(View.VISIBLE);
            vh.ivThreeOne.setVisibility(View.VISIBLE);
            vh.ivThreeTwo.setVisibility(View.GONE);
            vh.rl_moreThanFourImageView.setVisibility(View.GONE);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(0).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneOne);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(1).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneTwo);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(2).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivTwoOne);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(3).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivThreeOne);
        }

        if (singleAlbum.getPhotos().size() > 4) {
            vh.ivOneOne.setVisibility(View.VISIBLE);
            vh.ivOneTwo.setVisibility(View.VISIBLE);
            vh.ivTwoOne.setVisibility(View.VISIBLE);
            vh.ivThreeOne.setVisibility(View.VISIBLE);
            vh.ivThreeTwo.setVisibility(View.VISIBLE);
            vh.rl_moreThanFourImageView.setVisibility(View.VISIBLE);
            Log.e("sixe=====", "" + singleAlbum.getPhotos().size());
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(0).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneOne);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(1).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivOneTwo);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(2).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivTwoOne);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(3).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivThreeOne);
            Glide.with(vh.context)
                    .load(singleAlbum.getPhotos().get(4).getImagestr())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .into(vh.ivThreeTwo);
            int albumsize = singleAlbum.getPhotos().size() - 5;
            if(albumsize == 0){
                vh.view_alfa.setVisibility(View.GONE);
                vh.tv_moreThanFourImageCount.setVisibility(View.GONE);
            }else{
                vh.tv_moreThanFourImageCount.setText("+" + albumsize);
            }


        }

    }

    private void editAlbumName() {
        new RenameAlbum(context, singleAlbum, new RenameAlbum.OnAlbumRenameListener() {
            @Override
            public void onAlbumRenamed(String name) {
                tvAlbumName.setText(name +"("+ dateFormate+")" );
            }
        }).show();
    }


}
