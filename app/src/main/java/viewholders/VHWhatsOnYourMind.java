package viewholders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.app.agrizip.home.fragment.FragmentNewsFeed;
import com.app.agrizip.post.dialog.DialogNeedHelp;
import com.app.agrizip.post.post_simple.ActivityPostToAgrizip;
import com.app.agrizip.profile.ActivityUserProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import apimodels.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Constants;

/**
 * Created by rutvik on 11/22/2016 at 6:12 PM.
 */

public class VHWhatsOnYourMind extends RecyclerView.ViewHolder
{

    private final Context context;

    @BindView(R.id.iv_profilePicSmallStatus)
    PorterShapeImageView ivUserPic;

    @BindView(R.id.et_editStatusMessage)
    EditText etStatusMessage;

    @BindView(R.id.btn_postPhoto)
    RelativeLayout btnPostPhoto;

    @BindView(R.id.btn_checkin)
    RelativeLayout btnCheckin;

    @BindView(R.id.btn_needHelp)
    RelativeLayout btnNeedHelp;

    UserInfo.User user;

   public  boolean come_from_UserProfile =false;

    public VHWhatsOnYourMind(final Context context, View itemView)
    {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

        ivUserPic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(context, ActivityUserProfile.class);
                i.putExtra(Constants.USER_ID, user.getId());
                i.putExtra(Constants.IS_SEEING_SELF_PROFILE, true);
                if (App.getInstance().profileNavigationList.size() > 0)
                {
                    if (!App.getInstance().profileNavigationList.get(App.getInstance()
                            .profileNavigationList.size() - 1).equals(user.getId()))
                    {
                        context.startActivity(i);
                    }
                } else
                {
                    context.startActivity(i);
                }
            }
        });

        etStatusMessage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {



               // System.out.println( FragmentNewsFeed.adapter.getItemCount());

                if(context.toString().contains("Home")){
                    come_from_UserProfile = false;
                }else{
                    come_from_UserProfile = true;
                }
                if(FragmentNewsFeed.adapter.getItemCount() > 1){

                        Intent i = new Intent(context, ActivityPostToAgrizip.class);
                        i.putExtra("COME_FROM_PROFLE", come_from_UserProfile);
                        context.startActivity(i);


                }else{
                    if(come_from_UserProfile == true){
                        Intent i = new Intent(context, ActivityPostToAgrizip.class);
                        i.putExtra("COME_FROM_PROFLE", come_from_UserProfile);
                        context.startActivity(i);
                    }else{
                        Toast.makeText(context,"Please Wait NewsFeed is Loading. ",Toast.LENGTH_LONG).show();
                    }

                }
             //   context.startActivity(new Intent(context, ActivityPostToAgrizip.class));
            }
        });

        btnCheckin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(context.toString().contains("Home")){
                    come_from_UserProfile = false;
                }else{
                    come_from_UserProfile = true;
                }
                if(FragmentNewsFeed.adapter.getItemCount() > 1) {
                    final Intent postIntent = new Intent(context, ActivityPostToAgrizip.class);
                    postIntent.putExtra(Constants.IS_POST_TYPE_CHECKIN, true);
                    context.startActivity(postIntent);
                }else{
                    if(come_from_UserProfile == true){
                        final Intent postIntent = new Intent(context, ActivityPostToAgrizip.class);
                        postIntent.putExtra(Constants.IS_POST_TYPE_CHECKIN, true);
                        context.startActivity(postIntent);
                    }else{
                        Toast.makeText(context,"Please Wait NewsFeed is Loading. ",Toast.LENGTH_LONG).show();

                    }
                     }
            }
        });

        btnPostPhoto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                    if (context.toString().contains("Home")) {
                        come_from_UserProfile = false;
                    } else {
                        come_from_UserProfile = true;
                    }
                if(FragmentNewsFeed.adapter.getItemCount() > 1) {
                    Intent i = new Intent(context, ActivityPostToAgrizip.class);
                    i.putExtra("COME_FROM_PROFLE", come_from_UserProfile);
                    context.startActivity(i);
                }else {
                    if(come_from_UserProfile == true){
                        Intent i = new Intent(context, ActivityPostToAgrizip.class);
                        i.putExtra("COME_FROM_PROFLE", come_from_UserProfile);
                        context.startActivity(i);
                    }else{
                        Toast.makeText(context,"Please Wait NewsFeed is Loading. ",Toast.LENGTH_LONG).show();

                    }

                }
             //   context.startActivity(new Intent(context, ActivityPostToAgrizip.class));
            }
        });

        btnNeedHelp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                new DialogNeedHelp(context,1).show();
            }
        });



        /*etStatusMessage.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_SEND)
                {
                    tryPostingFeed();
                }
                return false;
            }
        });*/

    }

    public static VHWhatsOnYourMind create(final Context context, final ViewGroup parent)
    {
        return new VHWhatsOnYourMind(context, LayoutInflater.from(context)
                .inflate(R.layout.whats_on_your_mind, parent, false));
    }

    public static void bind(final VHWhatsOnYourMind vh, final UserInfo.User user)
    {

        if (vh.user == null)
        {
            vh.user = user;
        }

        if (user.isShowNeedHelpInWhatsOnYourMind())
        {

            vh.btnNeedHelp.setVisibility(View.VISIBLE);


        } else
        {

            vh.btnNeedHelp.setVisibility(View.GONE);
        }


        if (Constants.USERPROFILE_IMAGE.length() > 0)
        {
            Glide.with(vh.context)
                    .load(Constants.USERPROFILE_IMAGE)
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(vh.ivUserPic);
        } else
        {

            Glide.with(vh.context)
                    .load(user.getUserImage())
                    .placeholder(R.drawable.user_default)
                    .error(R.drawable.user_default)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(vh.ivUserPic);
        }

    }

}
