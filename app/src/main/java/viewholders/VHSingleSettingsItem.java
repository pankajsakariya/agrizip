package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;

import butterknife.BindView;
import butterknife.ButterKnife;
import viewmodels.SingleSettingsItemModel;

/**
 * Created by rutvik on 1/21/2017 at 4:58 PM.
 */

public class VHSingleSettingsItem extends RecyclerView.ViewHolder
{
    private static final String TAG = App.APP_TAG + VHSingleSettingsItem.class.getSimpleName();

    final Context context;

    @BindView(R.id.switch_settings)
    Switch switchSettings;

    @BindView(R.id.tv_settingsTitle)
    TextView tvSettingsTitle;

    @BindView(R.id.tv_settingsSubTitle)
    TextView tvSettingsSubTitle;

    SingleSettingsItemModel model;

    private VHSingleSettingsItem(Context context, View itemView)
    {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);
    }

    public static VHSingleSettingsItem create(final Context context, final ViewGroup parent)
    {
        return new VHSingleSettingsItem(context, LayoutInflater.from(context)
                .inflate(R.layout.single_settings_item, parent, false));
    }

    public static void bind(final VHSingleSettingsItem vh, SingleSettingsItemModel model)
    {
        vh.model = model;
        if (model.getSubTitle() != null)
        {
            vh.tvSettingsSubTitle.setVisibility(View.VISIBLE);
            vh.tvSettingsSubTitle.setText(model.getSubTitle());
        } else
        {
            vh.tvSettingsSubTitle.setVisibility(View.INVISIBLE);
        }

        vh.tvSettingsTitle.setText(model.getTitle());

        if (model.isShowSwitch())
        {
            vh.switchSettings.setChecked(model.isOn());
        } else
        {
            vh.switchSettings.setVisibility(View.GONE);
        }

        if (model.getSettingsChangedListener() != null)
        {
            Log.i(TAG, "bind: getSettingsChangedListener");
            vh.switchSettings.setOnCheckedChangeListener(model.getSettingsChangedListener());
        } else if (model.getOnClickListener() != null)
        {
            vh.itemView.setOnClickListener(model.getOnClickListener());
        }
    }

}
