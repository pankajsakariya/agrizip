package viewholders;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.album.album_fullscreen_image_view.ActivityFullPhotoView;
import com.app.agrizip.app.App;
import com.app.agrizip.event.activity.ViewPostedEvent;
import com.app.agrizip.friends.friend_list.ActivityFriendList;
import com.app.agrizip.friends.friend_request.FragmentRequests;
import com.app.agrizip.home.activity.ActivityHome;
import com.app.agrizip.home.fragment.FragmentNewsFeed;
import com.app.agrizip.need_help.activity.ActivityJobOrEmployeeFullView;
import com.app.agrizip.need_help.activity.ActivityNeedHelpFullView;
import com.app.agrizip.stories.activity.ActivityFullStory;
import com.app.agrizip.stories.activity.StoryListActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;

import api.API;
import api.RetrofitCallbacks;
import apimodels.AdvisePostList;
import apimodels.EventResponse;
import apimodels.FriendList;
import apimodels.GetSinglePostDetails;
import apimodels.NotificationList;
import apimodels.PostImageList;
import apimodels.RequireEmployeeList;
import apimodels.RequireJobList;
import apimodels.SinglePost;
import apimodels.SingleStory;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rutvik on 2/22/2017 at 4:00 PM.
 */

public class VHNotificationItem extends RecyclerView.ViewHolder {
    final Context context;

    @BindView(R.id.ll_notificationContainer)
    LinearLayout ll_notificationContainer;

    @BindView(R.id.tv_notificationMessage)
    TextView tvNotificationMessage;

    @BindView(R.id.iv_notificationUserImage)
    ImageView ivNotificationUserImage;

    @BindView(R.id.tv_notificationTime)
    TextView tvNotificationTime;

    @BindView(R.id.iv_notificationType)
    ImageView ivNotificationType;

    NotificationList.DataBean model;
    SinglePost post;

    public VHNotificationItem(Context context, View itemView) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

    }

    public static VHNotificationItem create(final Context context, final ViewGroup parent) {
        return new VHNotificationItem(context, LayoutInflater.from(context)
                .inflate(R.layout.single_notification_item, parent, false));
    }

    public static void bind(final VHNotificationItem vh, NotificationList.DataBean model) {
        vh.model = model;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            vh.tvNotificationMessage
                    .setText(Html.fromHtml(model.getMessage(), Html.FROM_HTML_MODE_COMPACT));

        }else{
            vh.tvNotificationMessage
                    .setText(Html.fromHtml(model.getMessage()));

        }

    // vh.tvNotificationMessage.setText(Html.model.getMessage());
        vh.tvNotificationTime.setText(DateUtils.getRelativeDateTimeString(vh.context,
                Utils.convertDateToMills(model.getCreateDate()),
                DateUtils.SECOND_IN_MILLIS,
                DateUtils.WEEK_IN_MILLIS,
                0));

        Glide.with(vh.context)
                .load(model.getUserImage()).asBitmap()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                // .crossFade()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        vh.ivNotificationUserImage.setImageBitmap(resource);
                    }
                });

        if (!model.isIsRead()) {
            vh.itemView.setBackgroundColor(Color.parseColor("#EFEFEF"));
        }

    }

    @OnClick(R.id.ll_notificationContainer)
    public void clickonNotification() {

        if (model.getNotificationType() == 1) {
            // WALL POST NOTIFICATION
            if (model.isPostImage()) {

                getUserPost(1, model.getDataId());

            } else {
                FragmentNewsFeed fragmentNewsFeed = FragmentNewsFeed.newInstance(0);
                // Fragment fragment =  FragmentNewsFeed()
                ActivityHome feed = (ActivityHome) context;
                FragmentManager fragmentManager = feed.getSupportFragmentManager();
                feed.showFragment(fragmentNewsFeed, 1);

            }

        }
        if (model.getNotificationType() == 2) {
            // HELP POST NOTIFICATION

            getSingleHelp();
        }
        if (model.getNotificationType() == 3) {
            // JOB POST NOTIFICATION

            //  getUserPost(3,model.getDataId());
            getSingleJobPost(model.getDataId());
        }
        if (model.getNotificationType() == 4) {
            // EVENT POST NOTIFICATION
            //  getUserPost(4,model.getDataId());
            getSingleEventPost(model.getDataId());


        }
        if (model.getNotificationType() == 5) {

            //SENT FRIEND REQ NOTIFICATION
            FragmentRequests fragmentNewsFeed = FragmentRequests.newInstance(1);
            ActivityHome feed = (ActivityHome) context;
            FragmentManager fragmentManager = feed.getSupportFragmentManager();
            feed.showFragment(fragmentNewsFeed, 2);

        }
        if (model.getNotificationType() == 6) {
            // ACCEPT FRIEND REQ  NOTIFICATION
            Intent intent = new Intent(context, ActivityFriendList.class);
            intent.putExtra(Constants.USER_ID, App.getInstance().getUser().getId());
            context.startActivity(intent);


        }
        if (model.getNotificationType() == 7) {
            // BLOG POST NOTIFICATION

            Intent storyFullView = new Intent(context, ActivityFullStory.class);
            //storyFullView.putExtra(Constants.SINGLE_STORY, model);
            storyFullView.putExtra("STORY_ID", model.getDataId());
            storyFullView.putExtra("FLAG", 0);
            context.startActivity(storyFullView);

        }

    }


    public void getSingleEventPost(String postId) {

        final RetrofitCallbacks<EventResponse> onGetPostDetailsCallback =
                new RetrofitCallbacks<EventResponse>(context) {

                    @Override
                    public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                        super.onResponse(call, response);


                        if (response.isSuccessful()) {

                            if (response.body().getResponseStatus() == 1) {
                                if (response.body().getData().getUserId() != null) {
                                    Intent viewSingleEvent = new Intent(context, ViewPostedEvent.class);
                                    viewSingleEvent.putExtra("UserImage", response.body().getData().getEventImage());
                                    viewSingleEvent.putExtra("EventName", response.body().getData().getEventName());
                                    viewSingleEvent.putExtra("Host", response.body().getData().getHost());
                                    viewSingleEvent.putExtra("Address", response.body().getData().getAddress());
                                    viewSingleEvent.putExtra("UserName", response.body().getData().getUserName());
                                    viewSingleEvent.putExtra("Contact", response.body().getData().getContactPerson());
                                    viewSingleEvent.putExtra("Time", response.body().getData().getTime());
                                    viewSingleEvent.putExtra("Date", response.body().getData().getDate());
                                    viewSingleEvent.putExtra("Desc", response.body().getData().getDescription());
                                    context.startActivity(viewSingleEvent);
                                }

                            }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<EventResponse> call, Throwable t) {
                        super.onFailure(call, t);

                    }
                };
        API.getInstance().getEventFromEventId(postId, onGetPostDetailsCallback);

        // API.getInstance().getHelpPostDetails(model.getDataId(),App.getInstance().getUser().getId(),onGetPostDetailsCallback);
    }

    public void getSingleJobPost(String postId) {

        final RetrofitCallbacks<RequireEmployeeList> onGetPostDetailsCallback =
                new RetrofitCallbacks<RequireEmployeeList>(context) {

                    @Override
                    public void onResponse(Call<RequireEmployeeList> call, Response<RequireEmployeeList> response) {
                        super.onResponse(call, response);


                        if (response.isSuccessful()) {

                            if (response.body().getResponseStatus() == 1) {
                                //  AdvisePostList.DataBean model1 = new AdvisePostList();


                                if (response.body().getData().size() > 0) {
                                    RequireEmployeeList.DataBean singleEmployeeRequest = null;
                                    RequireJobList.DataBean singleJobRequest = null;
                                    if (response.body().getData().get(0).isIsWantJob()) {
                                        singleJobRequest = new RequireJobList.DataBean();
                                        singleJobRequest.setJobId(response.body().getData().get(0).getJobId());
                                        singleJobRequest.setUserId(response.body().getData().get(0).getUserId());
                                        singleJobRequest.setUserName(response.body().getData().get(0).getUserName());
                                        singleJobRequest.setUserImage(response.body().getData().get(0).getUserImage());
                                        singleJobRequest.setCompanyName(response.body().getData().get(0).getCompanyName());
                                        singleJobRequest.setJobTitle(response.body().getData().get(0).getJobTitle());
                                        singleJobRequest.setVacancy(response.body().getData().get(0).getVacancy());
                                        singleJobRequest.setExperience(response.body().getData().get(0).getExperience());
                                        singleJobRequest.setEmail(response.body().getData().get(0).getEmail());
                                        singleJobRequest.setMobile(response.body().getData().get(0).getMobile());
                                        singleJobRequest.setEducation(response.body().getData().get(0).getEducation());
                                        singleJobRequest.setJobDuration(response.body().getData().get(0).getJobDuration());
                                        singleJobRequest.setStreet(response.body().getData().get(0).getStreet());
                                        singleJobRequest.setCity(response.body().getData().get(0).getCity());
                                        singleJobRequest.setDistrict(response.body().getData().get(0).getDistrict());
                                        singleJobRequest.setState(response.body().getData().get(0).getState());
                                        singleJobRequest.setZipCode(response.body().getData().get(0).getZipCode());
                                        singleJobRequest.setResume(response.body().getData().get(0).getResume());
                                        singleJobRequest.setLikeCounts(response.body().getData().get(0).getLikeCounts());
                                        singleJobRequest.setCommentCounts(response.body().getData().get(0).getCommentCounts());
                                        singleJobRequest.setIsLiked(response.body().getData().get(0).isIsLiked());
                                        singleJobRequest.setIsWantJob(true);
                                        singleJobRequest.setIsWantEmployee(false);
                                        singleJobRequest.setCreateDate(response.body().getData().get(0).getCreateDate());

                                    } else {

                                        singleEmployeeRequest = new RequireEmployeeList.DataBean();
                                        singleEmployeeRequest.setJobId(response.body().getData().get(0).getJobId());
                                        singleEmployeeRequest.setUserId(response.body().getData().get(0).getUserId());
                                        singleEmployeeRequest.setUserName(response.body().getData().get(0).getUserName());
                                        singleEmployeeRequest.setUserImage(response.body().getData().get(0).getUserImage());
                                        singleEmployeeRequest.setCompanyName(response.body().getData().get(0).getCompanyName());
                                        singleEmployeeRequest.setJobTitle(response.body().getData().get(0).getJobTitle());
                                        singleEmployeeRequest.setVacancy(response.body().getData().get(0).getVacancy());
                                        singleEmployeeRequest.setExperience(response.body().getData().get(0).getExperience());
                                        singleEmployeeRequest.setEmail(response.body().getData().get(0).getEmail());
                                        singleEmployeeRequest.setMobile(response.body().getData().get(0).getMobile());
                                        singleEmployeeRequest.setEducation(response.body().getData().get(0).getEducation());
                                        singleEmployeeRequest.setJobDuration(response.body().getData().get(0).getJobDuration());
                                        singleEmployeeRequest.setStreet(response.body().getData().get(0).getStreet());
                                        singleEmployeeRequest.setCity(response.body().getData().get(0).getCity());
                                        singleEmployeeRequest.setDistrict(response.body().getData().get(0).getDistrict());
                                        singleEmployeeRequest.setState(response.body().getData().get(0).getState());
                                        singleEmployeeRequest.setZipCode(response.body().getData().get(0).getZipCode());
                                        singleEmployeeRequest.setResume(response.body().getData().get(0).getResume());
                                        singleEmployeeRequest.setLikeCounts(response.body().getData().get(0).getLikeCounts());
                                        singleEmployeeRequest.setCommentCounts(response.body().getData().get(0).getCommentCounts());
                                        singleEmployeeRequest.setIsLiked(response.body().getData().get(0).isIsLiked());
                                        singleEmployeeRequest.setIsWantJob(false);
                                        singleEmployeeRequest.setIsWantEmployee(true);
                                        singleEmployeeRequest.setCreateDate(response.body().getData().get(0).getCreateDate());
                                    }
                                    final Intent fullViewActivity = new Intent(context, ActivityJobOrEmployeeFullView.class);
                                    if (singleJobRequest != null) {
                                        fullViewActivity.putExtra(Constants.NEED_JOB, true);
                                        fullViewActivity.putExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT,
                                                singleJobRequest);
                                    } else if (singleEmployeeRequest != null) {
                                        fullViewActivity.putExtra(Constants.NEED_JOB, false);
                                        fullViewActivity.putExtra(Constants.PARCELABLE_JOB_OR_EMPLOYEE_OBJECT,
                                                singleEmployeeRequest);
                                    }

                                    context.startActivity(fullViewActivity);

                                }

                            }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<RequireEmployeeList> call, Throwable t) {
                        super.onFailure(call, t);

                    }
                };

        API.getInstance().getJobOrEmployeePostDetails(postId, App.getInstance().getUser().getId(), onGetPostDetailsCallback);


    }


    public void getUserPost(final int notificationtype, String PostId) {


        final RetrofitCallbacks<GetSinglePostDetails> onGetPostDetailsCallback =
                new RetrofitCallbacks<GetSinglePostDetails>(context) {

                    @Override
                    public void onResponse(Call<GetSinglePostDetails> call, Response<GetSinglePostDetails> response) {
                        super.onResponse(call, response);


                        if (response.isSuccessful()) {

                            if (response.body().getResponseStatus() == 1) {

                                if (notificationtype == 1) {
                                    if (response.body().getData().size() > 0) {

                                        System.out.println("Hello");
                                        ArrayList<PostImageList.DataBean> images = new ArrayList<>();
                                        final PostImageList.DataBean d = new PostImageList.DataBean();
                                        d.setImageId(response.body().getData().get(0).getImages().get(0).getStatusImageId());
                                        d.setImagePath(response.body().getData().get(0).getImages().get(0).getImagestr());
                                        d.setStatusId(response.body().getData().get(0).getImages().get(0).getStatusID());
                                        images.add(d);

                                        Intent i = new Intent(context, ActivityFullPhotoView.class);

                                        i.putParcelableArrayListExtra(Constants.PARCELABLE_PHOTO_LIST, images);
                                        i.putExtra("PostUserId", model.getUserId());
                                        i.putExtra(Constants.ALBUM_SELECTED_IMAGE_INDEX, 0);
                                        i.putExtra(Constants.IS_SEEING_ALBUM_PHOTOS, false);
                                        context.startActivity(i);
                                    }

                                }

                                if (notificationtype == 4) {
                                    if (response.body().getData().size() > 0) {
                                        if (response.body().getData().get(0).isIsEventPost()) {

                                        }

                                    }
                                }

                                if (notificationtype == 3) {


                                }


                            }


                        } else {
                           /* Toast.makeText(ActivityPostToAgrizip.this, "try again later", Toast.LENGTH_SHORT).show();
                            finish();*/
                        }
                    }

                    @Override
                    public void onFailure(Call<GetSinglePostDetails> call, Throwable t) {
                        super.onFailure(call, t);
                    /*    Toast.makeText(ActivityPostToAgrizip.this, "try again later", Toast.LENGTH_SHORT).show();
                        finish();*/
                    }
                };
        API.getInstance().getStatusPostById(PostId, App.getInstance().getUser().getId(), onGetPostDetailsCallback);

    }

    public void getSingleHelp() {

        final RetrofitCallbacks<AdvisePostList> onGetPostDetailsCallback =
                new RetrofitCallbacks<AdvisePostList>(context) {

                    @Override
                    public void onResponse(Call<AdvisePostList> call, Response<AdvisePostList> response) {
                        super.onResponse(call, response);


                        if (response.isSuccessful()) {

                            if (response.body().getResponseStatus() == 1) {
                                //  AdvisePostList.DataBean model1 = new AdvisePostList();


                                Intent i = new Intent(context, ActivityNeedHelpFullView.class);
                                i.putExtra(Constants.PARCELABLE_HELP_OBJECT, response.body().getData().get(0));
                                context.startActivity(i);

                            }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<AdvisePostList> call, Throwable t) {
                        super.onFailure(call, t);

                    }
                };

        API.getInstance().getHelpPostDetails(model.getDataId(), App.getInstance().getUser().getId(), onGetPostDetailsCallback);
    }

    public void getSingleEvent() {


    }

}
