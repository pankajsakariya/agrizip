package viewholders;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.agrizip.R;
import com.app.agrizip.app.App;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import apimodels.FriendList;
import butterknife.BindView;
import butterknife.ButterKnife;
import extras.Utils;

/**
 * Created by rutvik on 1/27/2017 at 2:05 PM.
 */

public class VHSingleChatListUser extends RecyclerView.ViewHolder implements FriendList.DataBean.OnPresenceChangeListener
{

    private static final String TAG = App.APP_TAG + VHSingleChatListUser.class.getSimpleName();

    final Context context;

    FriendList.DataBean user;

    QBUser qbUser;

    @BindView(R.id.iv_chatFriendImage)
    PorterShapeImageView ivChatFriendImage;

    @BindView(R.id.tv_chatFriendName)
    TextView tvChatFriendName;

    @BindView(R.id.tv_chatFriendLastMessage)
    TextView tvChatFriendLastMessage;

    @BindView(R.id.tv_chatFriendPresence)
    TextView tvChatFriendPresence;

    public VHSingleChatListUser(final Context context, View itemView)
    {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (user.getChatOperationsListener() != null)
                {

                    user.getChatOperationsListener().initChat(qbUser, user);
                }
            }
        });

    }



    private int getUnreadMsgCount(QBChatDialog chatDialog){
        Integer unreadMessageCount = chatDialog.getUnreadMessageCount();
        if (unreadMessageCount == null) {
            return 0;
        } else {
            return unreadMessageCount;
        }
    }

    private boolean isLastMessageAttachment(QBChatDialog dialog) {
        String lastMessage = dialog.getLastMessage();
        Integer lastMessageSenderId = dialog.getLastMessageUserId();
        return TextUtils.isEmpty(lastMessage) && lastMessageSenderId != null;
    }

    private String prepareTextLastMessage(QBChatDialog chatDialog){
        if (isLastMessageAttachment(chatDialog)){
            return context.getString(R.string.chat_attachment);
        } else {
            return chatDialog.getLastMessage();
        }
    }

    public static VHSingleChatListUser create(final Context context, final ViewGroup parent)
    {
        return new VHSingleChatListUser(context, LayoutInflater.from(context)
                .inflate(R.layout.single_chat_row_item, parent, false));
    }

    public static void bind(final VHSingleChatListUser vh, final FriendList.DataBean user)
    {
        vh.user = user;

        Picasso.with(vh.context)
                .load(user.getUserImage())
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .into(vh.ivChatFriendImage);

        user.setOnPresenceChangeListener(vh);

        vh.tvChatFriendName.setText(user.getFullName());
        vh.tvChatFriendLastMessage.setText(user.getStatus());

        QBUsers.getUserByLogin(user.getMobileNumber()).performAsync(new QBEntityCallback<QBUser>()
        {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle)
            {
                Log.i(TAG, "QB USER FOUND SUCCESSFULLY");
                if (vh.user != null)
                {
                    vh.qbUser = qbUser;
                    final QBRoster roster = App.getInstance().getRoster();
                    if (roster != null)
                    {
                        if (roster.contains(qbUser.getId()))
                        {
                            try
                            {
                                roster.subscribe(qbUser.getId());
                            } catch (SmackException.NotConnectedException e)
                            {
                                e.printStackTrace();
                            }
                        } else
                        {
                            try
                            {
                                roster.createEntry(qbUser.getId(), null);
                            } catch (XMPPException e)
                            {
                                e.printStackTrace();
                            } catch (SmackException.NotLoggedInException e)
                            {
                                e.printStackTrace();
                            } catch (SmackException.NotConnectedException e)
                            {
                                e.printStackTrace();
                            } catch (SmackException.NoResponseException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (roster != null)
                    {
                        QBPresence presence = App.getInstance().getRoster().getPresence(qbUser.getId());

                        if (presence == null)
                        {
                            Log.i(TAG, "PRESENCE: NO USER IN ROSTER");
                        } else
                        {
                            if (presence.getType() == QBPresence.Type.online)
                            {
                                vh.tvChatFriendPresence.setTextColor(vh.context.getResources().getColor(android.R.color.holo_green_light));
                                vh.tvChatFriendPresence.setText("Online");
                            } else
                            {
                                vh.tvChatFriendPresence.setTextColor(vh.context.getResources().getColor(android.R.color.darker_gray));
                                vh.tvChatFriendPresence.setText("Offline");
                            }
                        }
                    }

                }
            }

            @Override
            public void onError(QBResponseException e)
            {
                Log.i(TAG, "QB USER NOT FOUND");
            }
        });


    }

    @Override
    public void onPresenceChanged(QBPresence qbPresence)
    {
        if (qbUser != null)
        {
            if (qbUser.getId().equals(qbPresence.getUserId()))
            {
                if (qbPresence.getType() == QBPresence.Type.online)
                {
                    tvChatFriendPresence.setTextColor(context.getResources().getColor(android.R.color.holo_green_light));
                    tvChatFriendPresence.setText("Online");
                } else
                {
                    tvChatFriendPresence.setTextColor(context.getResources().getColor(android.R.color.darker_gray));
                    tvChatFriendPresence.setText("Offline");
                }
            }
        }
    }
}
