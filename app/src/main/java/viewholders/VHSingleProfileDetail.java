package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import viewmodels.SingleProfileDetailModel;
import views.SingleProfileDetailView;

/**
 * Created by rutvik on 12/8/2016 at 1:16 PM.
 */

public class VHSingleProfileDetail extends RecyclerView.ViewHolder
{

    final Context context;

    SingleProfileDetailView singleProfileDetailView;

    SingleProfileDetailModel singleProfileDetailModel;

    public VHSingleProfileDetail(Context context, SingleProfileDetailView view)
    {
        super(view);
        singleProfileDetailView = view;
        this.context = context;
    }

    public static VHSingleProfileDetail create(final Context context, final ViewGroup parent)
    {
        return new VHSingleProfileDetail(context, new SingleProfileDetailView(context));
    }

    public static void bind(final VHSingleProfileDetail vh, SingleProfileDetailModel singleProfileDetailModel)
    {
        vh.singleProfileDetailModel = singleProfileDetailModel;

        vh.singleProfileDetailView.setSingleProfileDetailModel(vh.singleProfileDetailModel);

    }


}
