package viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.app.agrizip.R;
import com.app.agrizip.app.App;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rutvik on 12/13/2016 at 7:25 PM.
 */

public class VHSingleSaveProfileButton extends RecyclerView.ViewHolder
{

    private static final String TAG = App.APP_TAG + VHSingleSaveProfileButton.class.getSimpleName();
    final Context context;
    @BindView(R.id.btn_saveUserProfileDetails)
    Button btnSaveUserProfileDetails;
    View.OnClickListener onClickListener;

    public VHSingleSaveProfileButton(final Context context, View itemView)
    {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

        btnSaveUserProfileDetails.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View view)
            {
                //Toast.makeText(context, "CLICKED FROM VH", Toast.LENGTH_SHORT).show();
                if (onClickListener != null)
                {
                    onClickListener.onClick(view);
                } else
                {
                    Toast.makeText(context, "MODEL IS NULL", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public static VHSingleSaveProfileButton create(final Context context, final ViewGroup parent)
    {
        return new VHSingleSaveProfileButton(context, LayoutInflater.from(context)
                .inflate(R.layout.single_save_profile_details, parent, false));
    }

    public static void bind(final VHSingleSaveProfileButton vh, final View.OnClickListener onClickListener)
    {
        Log.i(TAG, "SAVE PROFILE DETAIL BUTTON BINDED");
        vh.onClickListener = onClickListener;
    }


}
