package api;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.CallSuper;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;

import apimodels.CommonResponse;
import apimodels.UserInfo;
import extras.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rutvik on 11/21/2016 at 11:45 AM.
 */

public class RetrofitCallbacks<T> implements Callback<T>
{

    final Context context;

    public RetrofitCallbacks(Context context)
    {
        this.context = context;
    }

    @Override
    @CallSuper
    public void onResponse(Call<T> call, Response<T> response)
    {
        System.out.println("INSIDE ON RESPONSE");
        System.out.println("RESPONSE CODE:" + response.code());

        if (response.isSuccessful())
        {
            if (response.headers() != null)
            {
                if (response.headers().get(Constants.AUTH_TOKEN) != null)
                {
                    if (!response.headers().get(Constants.AUTH_TOKEN).isEmpty())
                    {
                        PreferenceManager.getDefaultSharedPreferences(context)
                                .edit()
                                .putString(Constants.AUTH_TOKEN, response.headers().get(Constants.AUTH_TOKEN))
                                .apply();
                        API.getInstance().setAuthToken(response.headers().get(Constants.AUTH_TOKEN));
                    }
                }
            }
        } else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND)
        {
            try
            {
                final Gson gson = new Gson();

                CommonResponse commonResponse =
                        gson.fromJson(response.errorBody().string(), CommonResponse.class);

                if (commonResponse != null)
                {
                    if (commonResponse.getMessage() != null)
                    {
                        Toast.makeText(context, commonResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        } else if (response.code() == HttpURLConnection.HTTP_INTERNAL_ERROR)
        {
            try
            {
                final Gson gson = new Gson();

                CommonResponse commonResponse =
                        gson.fromJson(response.errorBody().string(), CommonResponse.class);

                if (commonResponse != null)
                {
                    if (commonResponse.getMessage() != null)
                    {
                        Toast.makeText(context, commonResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        } else
        {
            try
            {
                final Gson gson = new Gson();

                CommonResponse commonResponse =
                        gson.fromJson(response.errorBody().string(), CommonResponse.class);

                if (commonResponse != null)
                {
                    if (commonResponse.getMessage() != null)
                    {
                        Toast.makeText(context, commonResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        if (response.code() != HttpURLConnection.HTTP_OK)
        {
            System.out.println(response.errorBody().toString());
        } else if (response.code() == HttpURLConnection.HTTP_OK)
        {
            System.out.println(response.body().toString());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t)
    {
        if (!call.isCanceled())
        {
            if(t  !=null){
                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
            }
            //Toast.makeText(context, "Please Check Internet Connection.", Toast.LENGTH_SHORT).show();

            System.out.println("INSIDE ON FAILURE");
            System.out.println("!! Error :: "+t.toString());
        }
    }

    public void getNewAuthToken()
    {
        final String mobileNo = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.MOBILE_NO, "");
        if (!mobileNo.isEmpty())
        {

            API.getInstance().userLoginVerify(mobileNo, new RetrofitCallbacks<UserInfo>(context)
            {

                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response)
                {
                    super.onResponse(call, response);
                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t)
                {
                    super.onFailure(call, t);
                    Toast.makeText(context, "Cannot get new authtoken", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public String getTokenFromPreferences()
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.AUTH_TOKEN, null);
    }
}
