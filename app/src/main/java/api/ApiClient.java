package api;

import android.util.Log;

import com.app.agrizip.app.App;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static extras.Constants.API_BASE_URL;

/**
 * Created by rutvik on 11/17/2016 at 3:52 PM.
 */

public class ApiClient
{

    private static final String TAG = App.APP_TAG + ApiClient.class.getSimpleName();

    private static final String CACHE_CONTROL = "Cache-Control";

    private static Retrofit retrofit = null;

    private static OkHttpClient okHttpClient;

    public static Retrofit getClient()
    {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient = new OkHttpClient.Builder()
                .cache(provideCache()) // 20 MB
                .addNetworkInterceptor(provideCacheInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(provideOfflineCacheInterceptor())
                .build();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static OkHttpClient getOkHttpClient()
    {
        return okHttpClient;
    }

    public static void setOkHttpClient(OkHttpClient okHttpClient)
    {
        ApiClient.okHttpClient = okHttpClient;
    }

    private static Cache provideCache()
    {
        Cache cache = null;
        try
        {
            cache = new Cache(new File(App.getInstance().getCacheDir(), "http-cache"),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e)
        {
            Log.i(TAG, "Could not create Cache!");
        }
        return cache;
    }

    public static Interceptor provideCacheInterceptor()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept(Chain chain) throws IOException
            {
                Response response = chain.proceed(chain.request());

                // re-write response header to force use of cache
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build();

                return response.newBuilder()
                        .header(CACHE_CONTROL, cacheControl.toString())
                        .build();
            }
        };
    }


    public static Interceptor provideOfflineCacheInterceptor()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept(Chain chain) throws IOException
            {
                Request request = chain.request();

                if (!App.hasNetwork())
                {
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build();

                    request = request.newBuilder()
                            .cacheControl(cacheControl)
                            .build();
                }

                return chain.proceed(request);
            }
        };
    }

}
