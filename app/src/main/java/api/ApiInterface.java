package api;

import org.json.JSONObject;

import apimodels.AdvisePostList;
import apimodels.AppSettingModel;
import apimodels.BirthdayList;
import apimodels.BlockUser;
import apimodels.ChangeUserPrivacy;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import apimodels.ConfirmRejectFriendRequest;
import apimodels.CountryList;
import apimodels.CreateEvent;
import apimodels.CreateJobOrEmployee;
import apimodels.CreatePhotoAlbum;
import apimodels.CreateSupport;
import apimodels.DistrictList;
import apimodels.EditedPost;
import apimodels.EventList;
import apimodels.EventResponse;
import apimodels.FollowersList;
import apimodels.FriendList;
import apimodels.FriendTagId;
import apimodels.GetAlbums;
import apimodels.GetSinglePostDetails;
import apimodels.HelpListComments;
import apimodels.JobHideResponse;
import apimodels.JobResponse;
import apimodels.LikeListResponse;
import apimodels.LikeUnlikeJob;
import apimodels.LikeUnlikePost;
import apimodels.LikeUnlikeResponse;
import apimodels.NewMobileRegistration;
import apimodels.NotificationList;
import apimodels.OnCreatedPhotoAlbum;
import apimodels.PendingRequests;
import apimodels.PostImageList;
import apimodels.PostStatusResponse;
import apimodels.ReportPost;
import apimodels.RequireEmployeeList;
import apimodels.RequireJobList;
import apimodels.SetupProfile;
import apimodels.SharePost;
import apimodels.SinglePost;
import apimodels.StateList;
import apimodels.StatusPostList;
import apimodels.UnfollowUser;
import apimodels.UpdateProfilePic;
import apimodels.UploadPhotoInAlbum;
import apimodels.UploadStatusImageData;
import apimodels.UserInfo;
import apimodels.UserPrivacyDetails;
import apimodels.UserPrivacySettings;
import apimodels.UserProfileDetailGetForEdit;
import apimodels.UserProfileDetailsInfo;
import apimodels.UserSearchResponse;
import extras.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by rutvik on 11/17/2016 at 3:52 PM.
 */

public interface ApiInterface
{

    //MAIN METHOD **********************************************************************************
    //METHOD FOR LOGGING IN
    @GET("Login/UserLoginVerify")
    Call<UserInfo> userLoginVerify(@Query("Mobile") String mobile);


    //UPDATE DEVICE TOKEN FOR PUSH NOTIFICATION*****************************************************
    //Update InstanceID
    @GET("UserDetail/UpdateInstanceID")
    Call<ResponseBody> updateInstanceID(@Query("UserId") String userId,
                                        @Query("InstanceID") String fcmDeviceToken);


    //INITIAL METHODS FOR SETTING UP USER***********************************************************
    //METHOD FOR REGISTERING NEW MOBILE NO
    @GET("MobilePINVerify/VerifyOTP")
    Call<UserInfo> verifyOTP(@Query("Mobile") String mobile,
                                          @Query("DeviceID") String deviceId,
                                          @Query("OTP") String otp,
                                          @Query("InstanceID") String InstanceID,
                                          @Header(Constants.AUTH_TOKEN) String authToken);
    @GET("MobilePINVerify/NewMobileRegistration")
    Call<UserInfo> newMobileRegistraton(@Query("Mobile") String mobile,
                                          @Query("DeviceID") String deviceId,
                                          @Query("CountryId") String CountryId,
                                          @Query("InstanceID") String InstanceID,
                                          @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR RESENDING OTP
    @GET("MobilePINVerify/ResendOTP")
    Call<NewMobileRegistration> resendOTP(@Query("Mobile") String mobile,
                                          @Query("CountryId") String CountryId,
                                          @Query("DeviceID") String deviceId,
                                          @Header(Constants.AUTH_TOKEN) String authToken);

    /**
     * @Multipart
     * @POST("upload.php") Call<ImageUploadResult> uploadImage(@Part MultipartBody.Part file);
     */


    //METHOD FOR SETTING UP USER PROFILE LIKE NAME,PHOTO,STATE,DISTRICT ETC
    @POST("UserDetail/SetupProfile")
    Call<UserInfo> setupProfile(@Body SetupProfile data,
                                @Header(Constants.AUTH_TOKEN) String authToken,
                                @Header("Content-Type") String contentType);


    //STATE, DISTRICT AND COUNTRY RELATED METHODS***************************************************
    //METHOD FOR GETTING ALL STATES
    @GET("State/StateList")
    Call<StateList> getStateList(@Header(Constants.AUTH_TOKEN) String authToken);


    //METHOD FOR GETTING ALL DISTRICTS
    @GET("State/DistrictList")
    Call<DistrictList> getDistrictList(@Query("StateId") String stateId,
                                       @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR GETTING COUNTRIES
    @GET("State/CountryList")
    Call<CountryList> getCountryList();

    //POST RELATED METHODS**************************************************************************
    //METHOD FOR CREATING NEW POST
    @POST("StatusPost/CreateNewStatusPost")
    Call<PostStatusResponse> createNewStatusPost(@Body SinglePost data,
                                                 @Header(Constants.AUTH_TOKEN) String authToken,
                                                 @Header("Content-Type") String contentType);

    //METHOD FOR GETTING FEED POST ON DASHBOARD
    @POST("StatusPost/GetStatusPostList")
    Call<StatusPostList> getStatusPostList(@Query("UserID") String userId,
                                           @Query("PageNo") int pageNo,
                                           @Query("PageSize") int pageSize,
                                           @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/UserStatusPostList")
    Call<StatusPostList> getStatusPostListForUser(@Query("UserID") String userId,
                                                  @Query("PageNo") int pageNo,
                                                  @Query("PageSize") int pageSize,
                                                  @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/GetStatusPostListByStatusId")
    Call<GetSinglePostDetails> getStatusPostById(@Query("StatusId") String statusId,
                                                 @Query("UserID") String userId,
                                                 @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/GetStatusPostListByStatusId")
    Call<SinglePost> getPostById(@Query("StatusId") String statusId,
                                                 @Query("UserID") String userId,
                                                 @Header(Constants.AUTH_TOKEN) String authToken);


    //METHOD FOR LIKING/UN LIKING POST
    @POST("StatusPost/LikeUnlikeOnPost")
    Call<LikeUnlikeResponse> likeOnPost(@Body LikeUnlikePost data,
                                        @Header(Constants.AUTH_TOKEN) String authToken,
                                        @Header("Content-Type") String contentType);

    //METHOD FOR SHARING POST
    @POST("StatusPost/ShareStatusPost")
    Call<ResponseBody> sharePost(@Body SharePost sharePost,
                                 @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR DELETING POST
    @GET("StatusPost/DeleteStatusPost")
    Call<ResponseBody> deletePost(@Query("StatusId") String statusId,
                                  @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR UPLOADING IMAGES OF SPECIFIC STATUS (works in background using intent service)
    @POST("StatusPost/UploadPostImage")
    Call<ResponseBody> uploadStatusImage(@Body UploadStatusImageData imageData,
                                         @Header(Constants.AUTH_TOKEN) String authToken);


    //COMMENT RELATED API METHODS*******************************************************************
    //METHOD FOR COMMENTING ON POST
    @POST("StatusPost/CommentOnPost")
    Call<ResponseBody> commentOnPost(@Body CommentOnPost data,
                                     @Header(Constants.AUTH_TOKEN) String authToken,
                                     @Header("Content-Type") String contentType);

    //METHOD FOR GETTING COMMENTS ON PARTICULAR POST
    @POST("StatusPost/StatusPostCommentsList")
    Call<CommentsOnPost> getCommentListOfPost(@Query("StatusId") String statusId,
                                              @Query("UserID") String userId,
                                              @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR REMOVING COMMENT ON POST
    @GET("StatusPost/RemoveCommentOnPost")
    Call<ResponseBody> removeCommentOnPost(@Query("Id") String commentId,
                                           @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR REMOVING REPLY ON COMMENT
    @GET("StatusPost/RemoveSubCommentOnPost")
    Call<ResponseBody> removeReplyOnComment(@Query("Id") String replyId,
                                            @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR UPDATING COMMENT ON POST
    @POST("StatusPost/UpdateCommentOnPost")
    Call<ResponseBody> editCommentOnPost(@Body CommentOnPost data,
                                         @Header(Constants.AUTH_TOKEN) String authToken,
                                         @Header("Content-Type") String contentType);

    //METHOD FOR LIKING OR UNLIKING COMMENTS ON PARTICULAR POST
    @POST("StatusPost/LikeUnlikeOnPost")
    Call<ResponseBody> likeUnlikeOnComment(@Body CommentOnPost commentOnPost,
                                           @Header(Constants.AUTH_TOKEN) String authToken,
                                           @Header("Content-Type") String contentType);

    //METHOD FOR REPLYING ON COMMENTS OF PARTICULAR POST
    @POST("StatusPost/CommentOnPost")
    Call<ResponseBody> replyOnComment(@Body CommentOnPost commentOnPost,
                                      @Header(Constants.AUTH_TOKEN) String authToken,
                                      @Header("Content-Type") String contentType);

    //AGRIZIP USER/PEOPLE RELATED METHODS***********************************************************
    //METHOD FOR SEARCHING USERS
    @POST("UserDetail/PeopleSearchList")
    Call<UserSearchResponse> searchUser(@Query("UserId") String userId,
                                        @Query("searchkeyword") String keyword,
                                        @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("UserDetail/SelectUserProfileIntro")
    Call<UserProfileDetailsInfo> getUserProfileInfo(@Query("UserID") String userId,
                                                    @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("UserDetail/UpdateUserProfileDetails")
    Call<UserProfileDetailsInfo> updateUserProfileDetails(@Body UserProfileDetailsInfo.DataBean userProfileDetailsInfo,
                                                          @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR SENDING FRIEND REQUEST
    @GET("Friend/AddFriend")
    Call<ResponseBody> sendFriendRequest(@Query("UserID") String userId,
                                         @Query("FriendUserID") String friendUserId,
                                         @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR GETTING PEOPLE YOU MAY KNOW
    @GET("Friend/PeopleYouMayKnowList")
    Call<ResponseBody> getPeopleYouMayKnowList(@Query("UserID") String UserId,
                                               @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR GETTING ALL PENDING FRIEND REQUEST
    @GET("Friend/PendingRequest")
    Call<PendingRequests> getPendingFriendRequest(@Query("UserID") String userId,
                                                  @Query("PageNo") int PageNo,
                                                  @Query("PageSize") int PageSize,
                                                  @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR GETTING User Profile Detail Get for Edit
    @POST("UserDetail/SelectUserProfileDetails")
    Call<UserProfileDetailGetForEdit> getUserProfileDetailForEdit(@Query("UserID") String userId,
                                                                  @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR Update User Profile Detail
    @POST("UserDetail/UpdateUserProfileDetails")
    Call<UserInfo> updateUserProfileDetail(@Body UserProfileDetailGetForEdit.DataBean userProfileDetailGetForEdit,
                                               @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR GETTING ALL FRIENDS OF USER
    @POST("Friend/FriendList")
    Call<FriendList> getFriendList(@Query("UserID") String UserId,
                                   @Query("PageNo") int PageNo,
                                   @Query("PageSize") int PageSize,
                                   @Header(Constants.AUTH_TOKEN) String authToken);


    @POST("BlockSetting/BlockList")
    Call<FriendList> getBlockList(@Query("UserID") String UserId,
                                   @Query("PageNo") int PageNo,
                                   @Query("PageSize") int PageSize,
                                   @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR GETTING ALL FOLLOWERS OF USER
    @POST("Friend/UserFollowinglist")
    Call<FollowersList> getFollowinglist(@Query("UserID") String UserId,
                                         @Query("PageNo") int PageNo,
                                         @Query("PageSize") int PageSize,
                                         @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR GETTING ALL UserFollowlist OF USER
    @POST("Friend/UserFollowlist")
    Call<FollowersList> getFollowlist(@Query("UserID") String UserId,
                                      @Query("PageNo") int PageNo,
                                      @Query("PageSize") int PageSize,
                                      @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR ACCEPTING FRIEND REQUEST
    @POST("Friend/ConfirmToAddFriend")
    Call<ResponseBody> confirmFriendRequest(@Body ConfirmRejectFriendRequest confirmRejectFriendRequest,
                                            @Header(Constants.AUTH_TOKEN) String authToken);

    //METHOD FOR REJECTING FRIEND REQUEST
    @POST("Friend/DeleteRequestFriend")
    Call<ResponseBody> rejectFriendRequest(@Body ConfirmRejectFriendRequest confirmRejectFriendRequest,
                                           @Header(Constants.AUTH_TOKEN) String authToken);

    @GET("UserDetail/UpdateStatus")
    Call<ResponseBody> updateUserStatus(@Query("UserId") String userId,
                                        @Query("Status") String status,
                                        @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/AddTagintoPost")
    Call<ResponseBody> AddTagintoPost(  @Body FriendTagId FriendUserID,
                                        @Header(Constants.AUTH_TOKEN) String authToken);

    //Update User Personal Detail(DON'T USE THIS METHOD)
    @GET("UserDetail/UpdateUserPersonalDetails")
    Call<ResponseBody> updateUserPersonalDetails(@Query("Key") String key,
                                                 @Query("Value") String value,
                                                 @Query("UserId") String userId,
                                                 @Header(Constants.AUTH_TOKEN) String authToken);

    //Update User Personal Detail
    @POST("UserDetail/UpdateUserProfileDetails")
    Call<ResponseBody> updateUserProfileDetails(@Body UserProfileDetailGetForEdit.DataBean data,
                                                @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("BlockSetting/CreateBlockUser")
    Call<ResponseBody> blockUser(@Body BlockUser blockUser,
                                 @Header(Constants.AUTH_TOKEN) String authToken);


    @POST("BlockSetting/UnBlockUser")
    Call<ResponseBody> unBlockUser(@Body BlockUser blockUser,
                                 @Header(Constants.AUTH_TOKEN) String authToken);


    @POST("BlockSetting/UnBlockUser")
    Call<ResponseBody> unblockUser(@Body BlockUser unblockUser,
                                   @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Friend/UnFollowupUser")
    Call<ResponseBody> unfollowUser(@Body UnfollowUser unfollowUser,
                                    @Header(Constants.AUTH_TOKEN) String authToken);
    @POST("Friend/UnFollowupUser")
    Call<FollowersList> unfollowUser_temp(@Body UnfollowUser unfollowUser,
                                    @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Friend/FollowupUser")
    Call<ResponseBody> followUser(@Body UnfollowUser followUser,
                                  @Header(Constants.AUTH_TOKEN) String authToken);

    @GET("Friend/UnFriend")
    Call<ResponseBody> unfriend(@Query("UserId") String userId,
                                @Query("FriendUserId") String friendUserId,
                                @Header(Constants.AUTH_TOKEN) String authToken);

    //GET USER PROFILE PRIVACY DETAILS**************************************************************
    //GET User Profile Privacy
    @GET("UserType/UserTypeForProfilePrivacy")
    Call<UserPrivacyDetails> getUserPrivacyDetails(@Header(Constants.AUTH_TOKEN) String authToken);


    //USER PHOTO ALBUM RELATED METHODS**************************************************************
    //User Photo Album list
    @POST("StatusPost/PhotoAlbumList")
    Call<GetAlbums> getPhotoAlbumList(@Query("UserId") String userId,
                                      @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/AllPhotoList")
    Call<PostImageList> getAllPhotosList(@Query("UserID") String UserId,
                                         @Query("PageNo") int PageNo,
                                         @Query("PageSize") int PageSize,
                                         @Header(Constants.AUTH_TOKEN) String authToken);

    //Create Photo Album
    @POST("StatusPost/CreatePhotoAlbum")
    Call<OnCreatedPhotoAlbum> createPhotoAlbum(@Body CreatePhotoAlbum createPhotoAlbum,
                                               @Header(Constants.AUTH_TOKEN) String authToken);

    //Rename Photo  Album
    @POST("StatusPost/RenamePhotoAlbum")
    Call<ResponseBody> renamePhotoAlbum(@Body CreatePhotoAlbum createPhotoAlbum,
                                        @Header(Constants.AUTH_TOKEN) String authToken);

    //Create Job/CreateJobOrEmployee
    @POST("Job/CreateJob")
    Call<JobResponse> createJobOrEmployee(@Body CreateJobOrEmployee createJobOrEmployee,
                                          @Header(Constants.AUTH_TOKEN) String authToken);


    //Upload Photo in Album
    @POST("StatusPost/UploadPhotoinAlbum")
    Call<ResponseBody> uploadPhotoInAlbum(@Body UploadPhotoInAlbum uploadPhotoInAlbum,
                                          @Header(Constants.AUTH_TOKEN) String authToken);


    //Get Require Job List
    @POST("Job/WantJobList")
    Call<RequireJobList> getRequireJobList(@Query("UserId") String userId,
                                           @Query("PageNo") int pageNo,
                                           @Query("PageSize") int pageSize,
                                           @Header(Constants.AUTH_TOKEN) String authToken);


    //Get Require Job List
    @POST("Job/WantEmployeeList")
    Call<RequireEmployeeList> getRequireEmployeeList(@Query("UserId") String userId,
                                                     @Query("PageNo") int pageNo,
                                                     @Query("PageSize") int pageSize,
                                                     @Header(Constants.AUTH_TOKEN) String authToken);


    //Get Require Job List
    @POST("Job/AdvicePostList")
    Call<AdvisePostList> getAdvicePostList(@Query("UserId") String userId,
                                           @Query("PageNo") int pageNo,
                                           @Query("PageSize") int pageSize,
                                           @Header(Constants.AUTH_TOKEN) String authToken);

    //Get Require Job List
    @POST("Job/BuyPostList")
    Call<AdvisePostList> getBuyPostList(@Query("UserId") String userId,
                                        @Query("PageNo") int pageNo,
                                        @Query("PageSize") int pageSize,
                                        @Header(Constants.AUTH_TOKEN) String authToken);

    //Get Require Job List
    @POST("Job/SalePostList")
    Call<AdvisePostList> getSalePostList(@Query("UserId") String userId,
                                         @Query("PageNo") int pageNo,
                                         @Query("PageSize") int pageSize,
                                         @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Event/CreateEvent")
    Call<EventResponse> createNewEvent(@Body CreateEvent newEvent,
                                       @Header(Constants.AUTH_TOKEN) String authToken);


    @POST("Event/NearbyEventsList")
    Call<EventList> getNearByEventList(@Query("Latitude") double lat,
                                       @Query("Longitude") double lng,
                                       @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Event/EventsList")
    Call<EventList> getEventList(@Query("UserId") String userId,
                                 @Query("PageNo") int pageNo,
                                 @Query("PageSize") int pageSize,
                                 @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Friend/BirthdayList")
    Call<BirthdayList> getBirthdayList(@Query("UserId") String userId,
                                       @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Job/HelpPostDetails")
    Call<AdvisePostList> getHelpPostDetails(@Query("HelpId") String helpId,
                                          @Query("UserId") String userId,
                                          @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Job/JobPostCommentsList")
    Call<HelpListComments> getJobPostCommentsList(@Query("JobId") String jobId,
                                                  @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Job/JobPostDetails")
    Call<RequireEmployeeList> getJobOrEmployeePostDetails(@Query("JobId") String jobId,
                                                   @Query("UserId") String userId,
                                                   @Header(Constants.AUTH_TOKEN) String authToken);
    @POST("Event/EventsListByEventID")
    Call<EventResponse> getEventFromEventId(@Query("EventId") String eventId,

                                                          @Header(Constants.AUTH_TOKEN) String authToken);


    @POST("Support/CreateSupport")
    Call<ResponseBody> createSupport(@Body CreateSupport support,
                                     @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("UserDetail/ChangePrivacySettings")
    Call<ResponseBody> changePrivacySettings(@Body ChangeUserPrivacy privacyDetails,
                                             @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("UserDetail/ChangeAppSettings")
    Call<ResponseBody> ChangeAppSettings(@Body AppSettingModel appSettingModel,
                                             @Header(Constants.AUTH_TOKEN) String authToken);



    @POST("UserDetail/UserPrivacySettings")
    Call<UserPrivacySettings> getUserPrivacySettings(@Query("UserId") String userId,
                                                     @Header(Constants.AUTH_TOKEN) String authToken);

    @GET("StatusPost/EditPostPrivacy")
    Call<ResponseBody> editPostPrivacy(@Query("PostId") String postId,
                                       @Query("PrivacyId") String privacyId,
                                       @Header(Constants.AUTH_TOKEN) String authToken);

    @GET("StatusPost/HideFeedPost")
    Call<ResponseBody> hideFeedPost(@Query("UserId") String userId,
                                    @Query("PostId") String postId,
                                    @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/ChangeProfilePicture")
    Call<UpdateProfilePic.UpdatedProfilePicDetails>
    changeProfilePicture(@Body UpdateProfilePic userProfilePic,
                         @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/ReportFeedPost")
    Call<ResponseBody> reportFeedPost(@Body ReportPost reportPost,
                                      @Header(Constants.AUTH_TOKEN) String authToken);

    @GET("Friend/ScanQRCode")
    Call<ResponseBody> scanQRCode(@Query("UserID") String userId,
                                  @Query("FriendUserID") String friendUserID,
                                  @Header(Constants.AUTH_TOKEN) String authToken);

    @GET("StatusPost/MakeProfilePicture")
    Call<ResponseBody> makeProfilePicture(@Query("UserId") String userId,
                                          @Query("ImageId") String imageId,
                                          @Header(Constants.AUTH_TOKEN) String authToken);


    @POST("StatusPost/StatusPostLikesList")
    Call<LikeListResponse> getStatusPostLikesList(@Query("StatusId") String statusId,
                                                  @Query("UserId") String userId,
                                                  @Header(Constants.AUTH_TOKEN) String authToken);
    @GET("Job/HideHelp")
    Call<JobHideResponse> hidejobPost(@Query("UserId") String userId,
                                      @Query("HelpId") String helpId,
                                      @Header(Constants.AUTH_TOKEN) String authToken);

    @GET("StatusPost/RemoveTagFromPost")
    Call<ResponseBody> removeTagFromPost(@Query("StatusId") String statusId,
                                                  @Query("UserId") String userId,
                                                  @Header(Constants.AUTH_TOKEN) String authToken);
    @GET("StatusPost/TurnOnOffNotification")
    Call<ResponseBody> TurnOnOffNotification(@Query("PostId") String postId,
                                         @Query("UserId") String userId,
                                         @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/UpdateStatusPost")
    Call<ResponseBody> updateFeedPost(@Body EditedPost editedPost,
                                      @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("UserDetail/NotificationByUserId")
    Call<NotificationList> getNotificationByUserId(@Query("UserId") String userId,
                                                   @Query("PageNo") int pageNo,
                                                   @Query("PageSize") int pageSize,
                                                   @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/HidePhotoAlbum")
    Call<ResponseBody> hidePhotoFromAlbum(@Query("PhotoID") String photoId,
                                          @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("StatusPost/DeletePhotoAlbum")
    Call<ResponseBody> deletePhotoFromAlbum(@Query("PhotoID") String photoId,
                                            @Header(Constants.AUTH_TOKEN) String authToken);



    @POST("Job/LikeUnlikeOnJobPost")
    Call<ResponseBody> likeUnlikeOnJobPost(@Body LikeUnlikeJob likeUnlikeJob,
                                           @Header(Constants.AUTH_TOKEN) String authToken);
    @POST("Job/CommentOnJobPost")
    Call<HelpListComments> commentOnJobPost(@Body LikeUnlikeJob likeUnlikeJob,
                                          @Header(Constants.AUTH_TOKEN) String authToken);
    @POST("Job/JobPostLikesList")
    Call<HelpListComments> getJobPostLikeList(@Query("JobId") String jobId,
                                                  @Header(Constants.AUTH_TOKEN) String authToken);
    @POST("Job/SearchAdvicePostList")
      Call<AdvisePostList> searchAdvice(@Query("UserId") String userId,
                                          @Query("PageNo") int pageNo,
                                          @Query("PageSize") int pageSize,
                                          @Query("searchkeyword") String keyword,
                                          @Header(Constants.AUTH_TOKEN) String authToken);

    @POST("Job/SearchBuyPostList")
    Call<AdvisePostList> searchBuy(@Query("UserId") String userId,
                                   @Query("PageNo") int pageNo,
                                      @Query("PageSize") int pageSize,
                                      @Query("searchkeyword") String keyword,
                                      @Header(Constants.AUTH_TOKEN) String authToken);
    @POST("Job/SearchSalePostList")
    Call<AdvisePostList> searchSale(@Query("UserId") String userId,
                                    @Query("PageNo") int pageNo,
                                   @Query("PageSize") int pageSize,
                                   @Query("searchkeyword") String keyword,
                                   @Header(Constants.AUTH_TOKEN) String authToken);


}
