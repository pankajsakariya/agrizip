package api;

import android.content.Context;
import android.util.Log;

import com.app.agrizip.app.App;

import apimodels.AdvisePostList;
import apimodels.AppSettingModel;
import apimodels.BirthdayList;
import apimodels.BlockUser;
import apimodels.ChangeUserPrivacy;
import apimodels.CommentOnPost;
import apimodels.CommentsOnPost;
import apimodels.ConfirmRejectFriendRequest;
import apimodels.CountryList;
import apimodels.CreateEvent;
import apimodels.CreateJobOrEmployee;
import apimodels.CreatePhotoAlbum;
import apimodels.CreateSupport;
import apimodels.DistrictList;
import apimodels.EditedPost;
import apimodels.EventList;
import apimodels.EventResponse;
import apimodels.FollowersList;
import apimodels.FriendList;
import apimodels.FriendTagId;
import apimodels.GetAlbums;
import apimodels.GetSinglePostDetails;
import apimodels.HelpListComments;
import apimodels.JobHideResponse;
import apimodels.JobResponse;
import apimodels.LikeListResponse;
import apimodels.LikeUnlikeJob;
import apimodels.LikeUnlikePost;
import apimodels.LikeUnlikeResponse;
import apimodels.NewMobileRegistration;
import apimodels.NotificationList;
import apimodels.OnCreatedPhotoAlbum;
import apimodels.PendingRequests;
import apimodels.PostImageList;
import apimodels.PostStatusResponse;
import apimodels.ReportPost;
import apimodels.RequireEmployeeList;
import apimodels.RequireJobList;
import apimodels.SetupProfile;
import apimodels.SharePost;
import apimodels.SinglePost;
import apimodels.StateList;
import apimodels.StatusPostList;
import apimodels.UnfollowUser;
import apimodels.UpdateProfilePic;
import apimodels.UploadPhotoInAlbum;
import apimodels.UploadStatusImageData;
import apimodels.UserInfo;
import apimodels.UserPrivacyDetails;
import apimodels.UserPrivacySettings;
import apimodels.UserProfileDetailGetForEdit;
import apimodels.UserProfileDetailsInfo;
import apimodels.UserSearchResponse;
import extras.Constants;
import extras.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by rutvik on 11/17/2016 at 3:45 PM.
 */

public class API
{

    private static final String TAG = App.APP_TAG + API.class.getSimpleName();
    //create an object of SingleObject
    private static API instance = new API();
    private ApiInterface apiService;
    private String authToken = null;

    private API()
    {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    //Get the only object available
    public static API getInstance()
    {
        return instance;
    }

    void setAuthToken(String authToken)
    {
        /*if (authToken != null)
        {
            if (!authToken.isEmpty())
            {
                if (!this.authToken.equals(authToken))
                {
                    this.authToken = authToken;
                }
            }
        }*/
        this.authToken = authToken;
        Log.i(TAG, "SET AUTH-TOKEN: " + this.authToken);
    }

    String getAuthToken(RetrofitCallbacks callback)
    {
        if (authToken == null)
        {
            authToken = callback.getTokenFromPreferences();
            if (authToken == null)
            {
                callback.getNewAuthToken();
            }
        }

        Log.i(TAG, "GET AUTH-TOKEN: " + authToken);
        return authToken;
    }

    public void userLoginVerify(final String mobile, final RetrofitCallbacks<UserInfo> callback)
    {
        Call<UserInfo> call = apiService.userLoginVerify(mobile);
        call.enqueue(callback);
    }

    public void newMobileRegistration(final Context context, final String mobile,String CountyId, final RetrofitCallbacks<UserInfo> callback)
    {
        Call<UserInfo> call = apiService.newMobileRegistraton(mobile,
                Utils.getAndroidDeviceID(context), CountyId,  Utils.getFcmDeviceToken(context),getAuthToken(callback));

        call.enqueue(callback);
    }

    public void verifyOTP(final Context context, final String mobile, final String otp, final RetrofitCallbacks<UserInfo> callback)
    {
        Call<UserInfo> call = apiService.verifyOTP(mobile,
                Utils.getAndroidDeviceID(context), otp,Utils.getFcmDeviceToken(context),getAuthToken(callback));

        call.enqueue(callback);
    }

    public void resendOTP(final Context context, final String mobile,final String countryCode,final RetrofitCallbacks<NewMobileRegistration> callback)
    {
        Call<NewMobileRegistration> call = apiService.resendOTP(mobile,countryCode,
                Utils.getAndroidDeviceID(context), getAuthToken(callback));

        call.enqueue(callback);
    }


    /**
     * public void uploadImage(final MultipartBody.Part body, final RetrofitCallbacks<ImageUploadResult> callback)
     * {
     * Call<ImageUploadResult> call = apiService.uploadImage(body);
     * <p>
     * call.enqueue(callback);
     * }
     */

    public void setupProfile(final SetupProfile data, final RetrofitCallbacks<UserInfo> callback)
    {
        Call<UserInfo> call = apiService.setupProfile(data, getAuthToken(callback), Constants.CONTENT_TYPE_JSON);

        call.enqueue(callback);
    }

    public void getStateList(final RetrofitCallbacks<StateList> callback)
    {
        Call<StateList> call = apiService.getStateList(getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getDistrictList(final String stateId, final RetrofitCallbacks<DistrictList> callback)
    {
        Call<DistrictList> call = apiService.getDistrictList(stateId, getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getCountryList(final RetrofitCallbacks<CountryList> callback)
    {
        Call<CountryList> call = apiService.getCountryList();

        call.enqueue(callback);
    }

    public void createNewStatusPost(final SinglePost singlePost,
                                    final RetrofitCallbacks<PostStatusResponse> callback)
    {
        Call<PostStatusResponse> call = apiService.createNewStatusPost(singlePost, getAuthToken(callback), Constants.CONTENT_TYPE_JSON);

        call.enqueue(callback);
    }
    public void getSearchPostAdviceList(final String userid,final String searchkeyward,
                                  final int pageNo,
                                  final RetrofitCallbacks<AdvisePostList> callback)
    {
        Call<AdvisePostList> call = apiService.searchAdvice(userid,1, Constants.PAGE_SIZE,searchkeyward,
                                                         getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getSearchPostBuyList(final String userid,final String searchkeyward,
                                        final int pageNo,
                                        final RetrofitCallbacks<AdvisePostList> callback)
    {
        Call<AdvisePostList> call = apiService.searchBuy(userid,1, Constants.PAGE_SIZE,searchkeyward,
                getAuthToken(callback));

        call.enqueue(callback);
    }
    public void getSearchPostSaleList(final String userid,final String searchkeyward,
                                     final int pageNo,
                                     final RetrofitCallbacks<AdvisePostList> callback)
    {
        Call<AdvisePostList> call = apiService.searchSale(userid,1, Constants.PAGE_SIZE,searchkeyward,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getStatusPostList(final String userId,
                                  final int pageNo,
                                  final RetrofitCallbacks<StatusPostList> callback)
    {
        Call<StatusPostList> call = apiService.getStatusPostList(userId, pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getStatusPostListForUser(final String userId,
                                         final int pageNo,
                                         final RetrofitCallbacks<StatusPostList> callback)
    {
        Call<StatusPostList> call = apiService.getStatusPostListForUser(userId, pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public Call<GetSinglePostDetails> getStatusPostById(final String statusId,final String userId,
                                                        final RetrofitCallbacks<GetSinglePostDetails> callback)
    {
        Call<GetSinglePostDetails> call = apiService.getStatusPostById(statusId,userId,getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<SinglePost> getPostById(final String statusId,final String userId,
                                                        final RetrofitCallbacks<SinglePost> callback)
    {
        Call<SinglePost> call = apiService.getPostById(statusId,userId,getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }



    public Call<LikeUnlikeResponse> likeOnPost(final LikeUnlikePost likeUnlikePost, final RetrofitCallbacks<LikeUnlikeResponse> callback)
    {
        Call<LikeUnlikeResponse> call = apiService.likeOnPost(likeUnlikePost, getAuthToken(callback), Constants.CONTENT_TYPE_JSON);

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> commentOnPost(final CommentOnPost commentOnPost,
                                            final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.commentOnPost(commentOnPost, getAuthToken(callback),
                Constants.CONTENT_TYPE_JSON);

        call.enqueue(callback);

        return call;
    }

    public void editCommentOnPost(final CommentOnPost commentOnPost,
                                  final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.editCommentOnPost(commentOnPost, getAuthToken(callback),
                Constants.CONTENT_TYPE_JSON);

        call.enqueue(callback);
    }

    public void likeUnlikeOnComment(final CommentOnPost commentOnPost,
                                    final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.likeUnlikeOnComment(commentOnPost,
                getAuthToken(callback), Constants.CONTENT_TYPE_JSON);

        call.enqueue(callback);
    }

    public void replyOnComment(final CommentOnPost commentOnPost,
                               final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.replyOnComment(commentOnPost,
                getAuthToken(callback), Constants.CONTENT_TYPE_JSON);

        call.enqueue(callback);
    }



    public Call<CommentsOnPost> getCommentListOfPost(final String postId,final String userId, final RetrofitCallbacks<CommentsOnPost> callback)
    {
        Call<CommentsOnPost> call = apiService.getCommentListOfPost(postId,userId,getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<JobHideResponse> hideHelp(final String userId, final String helpId, final RetrofitCallbacks<JobHideResponse> callback)
    {
        Call<JobHideResponse> call = apiService.hidejobPost(userId,helpId,getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public void removeCommentOnPost(final String commentId, final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.removeCommentOnPost(commentId, getAuthToken(callback));

        call.enqueue(callback);
    }

    public void removeReplyOnComment(final String replyId, final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.removeReplyOnComment(replyId, getAuthToken(callback));

        call.enqueue(callback);
    }

    public void deletePost(final String statusId, final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.deletePost(statusId, getAuthToken(callback));

        call.enqueue(callback);
    }

    public Call<UserSearchResponse> searchUser(final String userId, final String keyword, final RetrofitCallbacks<UserSearchResponse> callback)
    {
        Call<UserSearchResponse> call = apiService.searchUser(userId, keyword, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<UserProfileDetailsInfo> getUserProfileInfo(final String userId, final RetrofitCallbacks<UserProfileDetailsInfo> callback)
    {
        Call<UserProfileDetailsInfo> call = apiService.getUserProfileInfo(userId, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<UserProfileDetailsInfo> updateUserProfileDetails(final UserProfileDetailsInfo.DataBean userProfileDetailsInfo,
                                                                 final RetrofitCallbacks<UserProfileDetailsInfo> callback)
    {
        Call<UserProfileDetailsInfo> call = apiService.updateUserProfileDetails(userProfileDetailsInfo, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<UserProfileDetailGetForEdit>
    getUserProfileDetailForEdit(final String userId,
                                final RetrofitCallbacks<UserProfileDetailGetForEdit> callback)
    {
        Call<UserProfileDetailGetForEdit> call = apiService.getUserProfileDetailForEdit(userId, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<UserInfo>
    updateUserProfileDetail(final UserProfileDetailGetForEdit.DataBean userProfileDetailGetForEdit,
                            final RetrofitCallbacks<UserInfo> callback)
    {

        Call<UserInfo> call =
                apiService.updateUserProfileDetail(userProfileDetailGetForEdit,
                        getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public void sendFriendRequest(final String userId, final String friendUserId, final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.sendFriendRequest(userId, friendUserId, getAuthToken(callback));

        call.enqueue(callback);
    }

    public void uploadStatusImage(final UploadStatusImageData statusImageData, final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.uploadStatusImage(statusImageData, getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getFriendList(final String userId, final int page, final RetrofitCallbacks<FriendList> callback)
    {
        Call<FriendList> call = apiService.getFriendList(userId, page, Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getBlockList(final String userId, final int page, final RetrofitCallbacks<FriendList> callback)
    {
        Call<FriendList> call = apiService.getBlockList(userId, page, Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getFollowinglist(final String userId, final int page, final RetrofitCallbacks<FollowersList> callback)
    {
        Call<FollowersList> call = apiService.getFollowinglist(userId, page, Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getFollowlist(final String userId, final int page, final RetrofitCallbacks<FollowersList> callback)
    {
        Call<FollowersList> call = apiService.getFollowlist(userId, page, Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public void getPendingFriendRequest(final String userId, final int page, final RetrofitCallbacks<PendingRequests> callback)
    {
        Call<PendingRequests> call = apiService.getPendingFriendRequest(userId,
                page,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public void confirmFriendRequest(final ConfirmRejectFriendRequest confirmRejectFriendRequest,
                                     final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.confirmFriendRequest(confirmRejectFriendRequest, getAuthToken(callback));

        call.enqueue(callback);
    }

    public void rejectFriendRequest(final ConfirmRejectFriendRequest confirmRejectFriendRequest,
                                    final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.rejectFriendRequest(confirmRejectFriendRequest, getAuthToken(callback));

        call.enqueue(callback);
    }

    public void updateUserStatus(final String userId,
                                 final String status,
                                 final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.updateUserStatus(userId, status, getAuthToken(callback));

        call.enqueue(callback);
    }

    public Call<UserPrivacyDetails> getPrivacyDetails(RetrofitCallbacks<UserPrivacyDetails> callback)
    {
        Call<UserPrivacyDetails> call = apiService.getUserPrivacyDetails(getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> updateUserEmail(final String email,
                                              final String userId,
                                              final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.updateUserPersonalDetails(Constants.UserPersonalDetail.Email,
                email, userId, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> updateUserProfileDetails(final UserProfileDetailGetForEdit.DataBean data,
                                                       final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.updateUserProfileDetails(data, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> blockUser(final BlockUser blockedUserData,
                                        final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.blockUser(blockedUserData, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> unBlockUser(final BlockUser blockedUserData,
                                        final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.unBlockUser(blockedUserData, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> unblockUser(final BlockUser unblockedUserData,
                                          final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.unblockUser(unblockedUserData, getAuthToken(callback));

        call.enqueue(callback);
        return call;
    }
    public Call<ResponseBody> AddTagintoPost(final FriendTagId FriendUserID,
                                          final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.AddTagintoPost(FriendUserID, getAuthToken(callback));

        call.enqueue(callback);
        return call;
    }

    public Call<ResponseBody> followUser(final UnfollowUser unfollowUser,
                                           final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.followUser(unfollowUser, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }
    public Call<ResponseBody> unfollowUser(final UnfollowUser unfollowUser,
                                           final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.unfollowUser(unfollowUser, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }
    public Call<FollowersList> unfollowUser_temp(final UnfollowUser unfollowUser,
                                           final RetrofitCallbacks<FollowersList> callback)
    {
        Call<FollowersList> call = apiService.unfollowUser_temp(unfollowUser, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> unfriend(final String userId,
                                       final String friendUserId,
                                       final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.unfriend(userId, friendUserId, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<GetAlbums> getPhotoAlbumList(final String userId,
                                             final RetrofitCallbacks<GetAlbums> callback)
    {
        Call<GetAlbums> call = apiService.getPhotoAlbumList(userId, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public void getAllPhotosList(final String userId, final int page, final RetrofitCallbacks<PostImageList> callback)
    {
        Call<PostImageList> call = apiService.getAllPhotosList(userId,
                page,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);
    }

    public Call<OnCreatedPhotoAlbum> createPhotoAlbum(final CreatePhotoAlbum createPhotoAlbum,
                                                      final RetrofitCallbacks<OnCreatedPhotoAlbum> callback)
    {
        Call<OnCreatedPhotoAlbum> call = apiService.createPhotoAlbum(createPhotoAlbum, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> renamePhotoAlbum(final CreatePhotoAlbum renamedAlbum,
                                               final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.renamePhotoAlbum(renamedAlbum, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> uploadPhotoInAlbum(final UploadPhotoInAlbum uploadPhotoInAlbum,
                                                 final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.uploadPhotoInAlbum(uploadPhotoInAlbum, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<JobResponse> createJobOrEmployee(final CreateJobOrEmployee createJobOrEmployee,
                                                 final RetrofitCallbacks<JobResponse> callback)
    {
        Call<JobResponse> call = apiService.createJobOrEmployee(createJobOrEmployee,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }


    public Call<RequireJobList> getRequireJobList(final String userId,
                                                  final int pageNo,
                                                  final RetrofitCallbacks<RequireJobList> callback)
    {
        Call<RequireJobList> call = apiService.getRequireJobList(userId, pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<RequireEmployeeList> getRequireEmployeeList(final String userId,
                                                            final int pageNo,
                                                            final RetrofitCallbacks<RequireEmployeeList> callback)
    {
        Call<RequireEmployeeList> call = apiService.getRequireEmployeeList(userId, pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<AdvisePostList> getAdvicePostList(final String userId,
                                                  final int pageNo,
                                                  final RetrofitCallbacks<AdvisePostList> callback)
    {
        Call<AdvisePostList> call = apiService.getAdvicePostList(userId, pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<AdvisePostList> getBuyPostList(final String userId,
                                               final int pageNo,
                                               final RetrofitCallbacks<AdvisePostList> callback)
    {
        Call<AdvisePostList> call = apiService.getBuyPostList(userId, pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<AdvisePostList> getSalePostList(final String userId,
                                                final int pageNo,
                                                final RetrofitCallbacks<AdvisePostList> callback)
    {
        Call<AdvisePostList> call = apiService.getSalePostList(userId, pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<EventResponse> createNewEvent(final CreateEvent newEvent,
                                              final RetrofitCallbacks<EventResponse> callback)
    {
        Call<EventResponse> call = apiService.createNewEvent(newEvent,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<EventList> getEventList(final String userId,
                                        final int pageNo,
                                        final RetrofitCallbacks<EventList> callback)
    {
        Call<EventList> call = apiService.getEventList(userId,
                pageNo,
                Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<EventList> getNearByEventList(final double lat,
                                              final double lng,
                                              final RetrofitCallbacks<EventList> callback)
    {
        Call<EventList> call = apiService.getNearByEventList(lat, lng, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<BirthdayList> getBirthdayList(final String userId,
                                              final RetrofitCallbacks<BirthdayList> callback)
    {
        Call<BirthdayList> call = apiService.getBirthdayList(userId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<AdvisePostList> getHelpPostDetails(final String helpId,final String userId,
                                                 final RetrofitCallbacks<AdvisePostList> callback)
    {
        Call<AdvisePostList> call = apiService.getHelpPostDetails(helpId,userId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }


    public Call<HelpListComments> newCommentOnJobPost(final LikeUnlikeJob likeUnlikeJob, final RetrofitCallbacks<HelpListComments> callbacks)
    {
        Call<HelpListComments> call = apiService.commentOnJobPost(likeUnlikeJob, getAuthToken(callbacks));
        call.enqueue(callbacks);
        return call;
    }

    public Call<ResponseBody> likeUnlikeForJob(final LikeUnlikeJob likeUnlikeJob, final RetrofitCallbacks<ResponseBody> callbacks)
    {
        Call<ResponseBody> call = apiService.likeUnlikeOnJobPost(likeUnlikeJob, getAuthToken(callbacks));
        call.enqueue(callbacks);
        return call;
    }

    public Call<HelpListComments> getJobPostCommentsList(final String jobId,
                                                         final RetrofitCallbacks<HelpListComments> callback)
    {
        Call<HelpListComments> call = apiService.getJobPostCommentsList(jobId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<HelpListComments> getJobLikeList(final String jobId,
                                                          final RetrofitCallbacks<HelpListComments> callback)
    {
        Call<HelpListComments> call = apiService.getJobPostLikeList(jobId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<RequireEmployeeList> getJobOrEmployeePostDetails(final String jobId,final String userId,
                                                          final RetrofitCallbacks<RequireEmployeeList> callback)
    {
        Call<RequireEmployeeList> call = apiService.getJobOrEmployeePostDetails(jobId,userId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }
    public Call<EventResponse> getEventFromEventId(final String jobId,
                                                                 final RetrofitCallbacks<EventResponse> callback)
    {
        Call<EventResponse> call = apiService.getEventFromEventId(jobId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }
    public Call<ResponseBody> removeTagFromPost(final String jobId,final String userId,

                                                          final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.removeTagFromPost(jobId,userId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> TurnOnOffNotification(final String postId,final String userId,

                                                final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.TurnOnOffNotification(postId,userId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> createSupport(final CreateSupport support,
                                            final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.createSupport(support,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> changePrivacySettings(final ChangeUserPrivacy privacyDetails,
                                            final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.changePrivacySettings(privacyDetails,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<UserPrivacySettings> getUserPrivacyDetails(final String userId,
                                                           final RetrofitCallbacks<UserPrivacySettings> callback)
    {
        Call<UserPrivacySettings> call = apiService.getUserPrivacySettings(userId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> editPostPrivacy(final String postId,
                                              final String privacyId,
                                              final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.editPostPrivacy(postId, privacyId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> hideFeedPost(final String userId,
                                           final String postId,
                                           final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.hideFeedPost(userId, postId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<UpdateProfilePic.UpdatedProfilePicDetails>
    changeProfilePicture(final UpdateProfilePic updateProfilePic,
                         final RetrofitCallbacks<UpdateProfilePic.UpdatedProfilePicDetails> callback)
    {
        Call<UpdateProfilePic.UpdatedProfilePicDetails> call = apiService.changeProfilePicture(updateProfilePic,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }


    public Call<ResponseBody> reportFeedPost(final ReportPost reportPost,
                                             final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.reportFeedPost(reportPost,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> appSetting(final AppSettingModel appSettingModel,
                                            final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.ChangeAppSettings(appSettingModel,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> scanQrCode(final String userId, final String friendUserId,
                                         final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.scanQRCode(userId, friendUserId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> makeProfilePicture(final String userId, final String imageId,
                                                 final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.makeProfilePicture(userId, imageId, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> sharePost(final SharePost sharePost,
                                        final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.sharePost(sharePost, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<LikeListResponse> getStatusPostLikesList(final String statudId,final String userId,
                                                         final RetrofitCallbacks<LikeListResponse> callback)
    {
        Call<LikeListResponse> call = apiService.getStatusPostLikesList(statudId, userId,getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> updateFeedPost(final EditedPost editedPost,
                                             final RetrofitCallbacks<ResponseBody> callback)
    {
        Call<ResponseBody> call = apiService.updateFeedPost(editedPost, getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<NotificationList> getNotificationByUserId(final String userId,
                                                          final int pageNo,
                                                          final RetrofitCallbacks<NotificationList> callback)
    {
        Call<NotificationList> call = apiService.getNotificationByUserId(userId, pageNo, Constants.PAGE_SIZE,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> hidePhotoFromAlbum(final String photoId,
                                                 final RetrofitCallbacks<ResponseBody> callback)

    {
        Call<ResponseBody> call = apiService.hidePhotoFromAlbum(photoId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

    public Call<ResponseBody> deletePhotoFromAlbum(final String photoId,
                                                   final RetrofitCallbacks<ResponseBody> callback)

    {
        Call<ResponseBody> call = apiService.deletePhotoFromAlbum(photoId,
                getAuthToken(callback));

        call.enqueue(callback);

        return call;
    }

}
