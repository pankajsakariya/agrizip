package extras;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.app.agrizip.R;

import quickblox.ResourceUtils;

/**
 * Created by rutvik on 11/18/2016 at 12:03 PM.
 */

public class Constants
{

    //    public static final String API_BASE_URL = "http://social.vinayakpharmaceuticals.com/api/";
 //   public static final String API_BASE_URL = "http://app.agrizip.net/api/";
   public static final String API_BASE_URL = "http://demo.agrizip.net/api/";

    public static final String PREF_USER = "PREF_USER";

    public static final String PICASSO_TAG = "PICASSO_AGRIZIP";

    public static final String MOBILE_NO = "MOBILE_NO";

    public static final String AUTH_TOKEN = "Authtoken";

    public static final int CHOOSE_IMAGE_INTENT = 1010;

    public static final int REQUEST_CODE_IMAGE_PICKER = 111;

    public static final String CONTENT_TYPE_JSON = "application/json";

    public static final String BASE_64_IMAGE_STRING_LIST = "BASE_64_IMAGE_STRING_LIST";

    public static final String STATUS_ID = "STATUS_ID";

    public static final String USER_ID = "USER_ID";

    public static final int CHECKIN_ACTIVITY = 222;

    public static final String CHECKIN_PLACE = "CHECKIN_PLACE";
    public static final String CHECKIN_LAT = "CHECKIN_LAT";
    public static final String CHECKIN_LNG = "CHECKIN_LNG";


    public static final int TAG_FRIEND_ACTIVITY = 333;

    public static final String TAG_FRIEND_LIST = "TAG_FRIEND_LIST";
    public static final String TAG_FRIEND_NAME_LIST = "TAG_FRIEND_NAME_LIST";
    public static final String IS_SEEING_SELF_PROFILE = "IS_SEEING_SELF_PROFILE";
    public static final int PAGE_SIZE = 10;

    public static final String SMS_ORIGIN = "AGRZIP";
    public static final String OTP_DELIMITER = ")";

    public static final String ON_RECEIVE_OTP_BROADCAST = "com.app.agrizip.ON_RECEIVE_OTP_BROADCAST";

    public static final String RECEIVED_OTP_NUMBER = "RECEIVED_OTP_NUMBER";

    public static final String FCM_DEVICE_TOKEN = "FCM_DEVICE_TOKEN";

    public static final String ALBUM_ID = "ALBUM_ID";

    public static final String ALBUM_NAME = "ALBUM_NAME";

    public static final String ADD_NEW_PHOTO_TO_ALBUM = "ADD_NEW_PHOTO_TO_ALBUM";

    public static final String ALBUM_IMAGES_URL_LIST = "ALBUM_IMAGES_URL_LIST";

    public static final String PARCELABLE_PHOTO_LIST = "PARCELABLE_PHOTO_LIST";

    public static final String ALBUM_SELECTED_IMAGE_INDEX = "ALBUM_SELECTED_IMAGE_INDEX";

    public static final int ALBUM_IMAGE_UPLOAD_SERVICE = 1088;

    public static final String IS_SEEING_ALBUM_PHOTOS = "IS_SEEING_ALBUM_PHOTOS";

    public static final String NEED_HELP = "NEED_HELP";

    public static final String HELP_TYPE = "HELP_TYPE";


    public static final String NEED_JOB = "NEED_JOB";


    public static final int ADVISE = 1;
    public static final int BUY = 2;
    public static final int SELL = 3;

    public static final String PARCELABLE_HELP_OBJECT = "PARCELABLE_HELP_OBJECT";

    public static final String PARCELABLE_JOB_OR_EMPLOYEE_OBJECT = "PARCELABLE_JOB_OR_EMPLOYEE_OBJECT";

    public static final String IS_SEEING_FOLLOWERS = "IS_SEEING_FOLLOWERS";

    public static final int SELECT_NOTIFICATION_SOUND = 2534;

    public static final String CHOSEN_NOTIFICATION_TONE = "CHOSEN_NOTIFICATION_TONE";

    public static final int CHANGE_PROFILE_PIC = 2573;

    public static final String EXTRA_DIALOG_ID = "EXTRA_DIALOG_ID";

    public static final String POST_ID = "POST_ID";

    public static boolean ALBUM_EDIT = false;

    public static boolean DATA_LOADED = false;

    public static final String ON_POST_PHOTO_UPLOADED_SUCCESSFULLY = "ON_POST_PHOTO_UPLOADED_SUCCESSFULLY";

    public static final String CONNECTIVITY_CHANGED = "CONNECTIVITY_CHANGED";
    public static final String IS_PROFILE_SET = "IS_PROFILE_SET";
    public static final String INCOMPLETE_PROFILE_MOBILE_NO = "INCOMPLETE_PROFILE_MOBILE_NO";
    public static final String IS_ACTIVE = "IS_ACTIVE";
    public static final String SINGLE_STORY = "SINGLE_STORY";
    public static final String IS_POST_TYPE_CHECKIN = "IS_POST_TYPE_CHECKIN";
    static final String[] relationshipList =
            new String[]{"Single",
                    "In a relationship",
                    "Engaged",
                    "Married",
                    "In a civil union",
                    "In a domestic partnership",
                    "In an open relationship",
                    "It's complicated",
                    "Separated",
                    "Divorced",
                    "Widowed"};
    public static String USERPROFILE_IMAGE = "";
    public static String USER_NAME = "";
    public static boolean UPDATE_FLLOWCOUNT =false;
    public static int COME_FROM_HOME =0;


    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";



    public static final String SHARED_PREF = "ah_firebase";

    public static ArrayAdapter<String> getRelationshipAdapter(final Context context)
    {
        /*
        Single
        In a relationship
        Engaged
        Married
        In a civil union
        In a domestic partnership
        In an open relationship
        It's complicated
        Separated
        Divorced
        Widowed
         */

        return new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, relationshipList);


    }

    public interface QuickBloxConst
    {

        int PREFERRED_IMAGE_SIZE_PREVIEW = ResourceUtils.getDimen(R.dimen.chat_attachment_preview_size);
        int PREFERRED_IMAGE_SIZE_FULL = ResourceUtils.dpToPx(320);
    }

    public static final class UserType
    {
        public static final String SELF = "1";
        public static final String FRIEND_REQUEST_ALREADY_SENT = "2";
        public static final String NOT_A_FRIEND = "3";
        public static final String FRIEND = "4";
    }

    public static final class UserPersonalDetail
    {
        public static final String Email = "Email";
        public static final String Mobile = "Mobile";
        public static final String DOB = "DOB";
        public static final String Gender = "Gender";
    }

    public static class QuickBlox
    {
        public static final String APP_ID = "52835";
        public static final String AUTH_KEY = "FReg3Vuvv28vz87";
        public static final String AUTH_SECRET = "XZW3sOAupnWyJRw";
        public static final String ACCOUNT_KEY = "iaQJ1xkj7JvfRysqmozY";
        public static final String GCM_SENDER_ID = "761750217637";

    }

    public static class NotificationType {
        public static final int NormalPost = 1;
        public static final int SharePost = 2;
        public static final int FriendReqAccept = 3;
        public static final int JobPost = 4;
        public static final int HelpPost = 5;
        public static final int BlogPost = 6;
        public static final int ChatNewMessage = 7;
        public static final int Comment = 8;
        public static final int TagPost = 9;
    }

}
