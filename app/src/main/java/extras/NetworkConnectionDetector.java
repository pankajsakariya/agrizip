package extras;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.app.agrizip.app.App;

/**
 * Created by ACER on 17-Mar-16 at 9:40 AM.
 */
public class NetworkConnectionDetector extends BroadcastReceiver
{

    private static final String TAG = App.APP_TAG + NetworkConnectionDetector.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent)
    {

        Log.i(TAG, "Network connectivity changed");


    }
}

