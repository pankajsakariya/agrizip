package extras;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.app.agrizip.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import moaritem.MoarItem;

/**
 * Created by rutvik on 11/17/2016 at 3:27 PM.
 */

public class Utils {


    public final static String[] month = {"January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"};
    //Set the radius of the Blur. Supported range 0 < radius <= 25
    private static final float BLUR_RADIUS = 25f;
    static ProgressDialog progressDialog;

    public static final String getAndroidDeviceID(final Context context) {

        final String androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("AGRIZIP", "ANDROID ID: " + androidId);
        return androidId;
    }

    public static String getFcmDeviceToken(final Context context) {

        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.FCM_DEVICE_TOKEN, "");
    }

    public static String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        return Base64.encodeToString(byteFormat, Base64.DEFAULT);
    }

    public static Bitmap getBitmapFromBase64(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString,50, decodedString.length);
    }

    public static String convertImageFileToBase64(final String imagePath) {
        File imagefile = new File(imagePath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String temp ="";
       try{
           Bitmap bm = BitmapFactory.decodeStream(fis);

           ByteArrayOutputStream baos = new ByteArrayOutputStream();

           bm.compress(Bitmap.CompressFormat.JPEG,100, baos);
           byte[] b = baos.toByteArray();
            temp =Base64.encodeToString(b, Base64.DEFAULT);
           return temp ;
        }catch (Exception e){

        e.printStackTrace();
       }

        return temp;
        //return Utils.getEncoded64ImageStringFromBitmap(bm);
    }

    public static Bitmap convertImageToBitmap(final String imagePath) {

        BitmapFactory.Options bfOptions = new BitmapFactory.Options();
        bfOptions.inDither = false;                     //Disable Dithering mode
        bfOptions.inPurgeable = true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
        bfOptions.inInputShareable = true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
        bfOptions.inTempStorage = new byte[64 * 1024];

        File file = new File(imagePath);
        FileInputStream fs = null;
        try {
            fs = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            //TODO do something intelligent
            e.printStackTrace();
        }

        Bitmap bm = null;

        try {
            if (fs != null) {
                bm = BitmapFactory.decodeFileDescriptor(fs.getFD(), null, bfOptions);
            }
        } catch (IOException e) {
            //TODO do something intelligent
            e.printStackTrace();
        } finally {
            if (fs != null) {
                try {
                    fs.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        //bm=BitmapFactory.decodeFile(path, bfOptions); This one causes error: java.lang.OutOfMemoryError: bitmap size exceeds VM budget
       // return bm;
       return compressBitmap(bm);


    }

    public static Bitmap compressBitmap(final Bitmap original) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        original.compress(Bitmap.CompressFormat.JPEG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return decoded;
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showProgressDialog(final Context context, final String title,
                                          final String message, final boolean isCancellable,
                                          final boolean isCancellableOnTouchOutside,
                                          final DialogInterface.OnCancelListener cancelListener) throws IllegalArgumentException {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        if (isCancellable) {
            if (cancelListener == null) {
                throw new IllegalArgumentException("Cancel listener cannot be null for cancellable progress dialog.");
            }
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(cancelListener);

            progressDialog.setCanceledOnTouchOutside(isCancellableOnTouchOutside);
        } else {
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setOnCancelListener(null);
            progressDialog.setCanceledOnTouchOutside(isCancellableOnTouchOutside);
        }
        progressDialog.show();

    }



    public static void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public static Bitmap blur(final Context context, Bitmap image) {
        if (null == image) {
            return null;
        }

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(context);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

        //Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }


    public static long convertDateToMills(String datetime) {

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        inputFormat.setTimeZone(tz);


        if (datetime == null) {
            return Calendar.getInstance().getTimeInMillis();
        }
        if (datetime.equals("")) {
            return Calendar.getInstance().getTimeInMillis();
        }
        try {
            return Long.parseLong(datetime);
        } catch (NumberFormatException e) {
            Date date = null;
            try {
                date = inputFormat.parse(datetime);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return date.getTime() + TimeZone.getDefault().getRawOffset();
        }
    }

    public static String convertMillsToDate(final long timeInMills) {
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMills);
        Date date = calendar.getTime();

        return outputFormat.format(date);
    }


    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 80);
        return noOfColumns;
    }


    public static ArrayList<? extends MoarItem> moarItems(ArrayList<? extends MoarItem> list) {
        int qty = list.size();

        for (int i = 0; i < qty; i++) {
            int colSpan = Math.random() < 0.2f ? 2 : 1;
            // Swap the next 2 lines to have items with variable
            // column/row span.
            // int rowSpan = Math.random() < 0.2f ? 2 : 1;
            int rowSpan = colSpan;
            list.get(i).setColSpan(colSpan);
            list.get(i).setRowSpan(rowSpan);
            list.get(i).setPosition(i);
        }

        return list;
    }

    public static Status isGooglePlayServiceAvailable(Context context) {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);

        if (status == ConnectionResult.SUCCESS) {
            return Status.AVAILABLE;
        } else if (status == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            return Status.UPDATE_REQUIRE;
        } else {
            return Status.UNAVAILABLE;
        }
    }

    public static int getScreenHeight(Context context) {
        int measuredHeight;
        Point size = new Point();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            wm.getDefaultDisplay().getSize(size);
            measuredHeight = size.y;
        } else {
            Display d = wm.getDefaultDisplay();
            measuredHeight = d.getHeight();
        }

        return measuredHeight;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Context inContext, Uri uri) {
        String result;
        Cursor cursor = inContext.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }



    public static enum Status {
        UNAVAILABLE,
        UPDATE_REQUIRE,
        AVAILABLE
    }

    public static String changeDateFormat(String dateM, String inputFormatM, String outputForamtM) {
        String inputPattern = inputFormatM;
        String outputPattern = outputForamtM;
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateM);
            str = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }




}
